package com.onestepmore.caredoc.ui.main.profile.contact;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.databinding.FContactBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialogClickListener;

import javax.inject.Inject;
import javax.inject.Named;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.CS_CLICK_BACK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.CS_COMPLETE_SEND_EMAIL;

public class ContactFragment extends BaseFragment<FContactBinding, ContactViewModel> implements
        ContactNavigator,
        BottomSheetDialogClickListener
{

    private static final int REQUEST_CODE_SEND_EMAIL = 99;

    @Inject
    @Named("ContactFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private ContactViewModel mContactViewModel;

    private String mType = "";

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_contact;
    }

    @Override
    public ContactViewModel getViewModel() {
        mContactViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ContactViewModel.class);
        return mContactViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContactViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fContactToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }
    }

    @Override
    public void onTypeLayoutClick() {
        getViewDataBinding().fContactTypeLayout.requestFocus();

        String [] textList = getResources().getStringArray(R.array.f_contact_type_list);;
        BottomSheetDialog dialog = BottomSheetDialog.getInstance(textList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), "contact_type");
    }

    @Override
    public void onSendMail() {
        String content = getViewDataBinding().fContactTextInput.getText().toString().trim();
        int textLength = content.length();
        if(textLength == 0 || mType.isEmpty()) {
            String title = getString(R.string.warning_msg_contact_failed);
            String desc = "";
            if(mType.isEmpty()){
                desc = getString(R.string.warning_msg_contact_invalid_type);
            }else if(textLength == 0){
                desc = getString(R.string.warning_msg_contact_invalid_content);
            }

            showCommonDialog(title, desc);
            return;
        }

        String subject = getString(R.string.f_contact_title);
        subject += "-" + mType;

        String email = "";
        String name = "";
        User user = getViewModel().getRealm().where(User.class).findFirst();
        if(user != null){
            email = user.getEmail();
            name = user.getName();
        }

        getViewModel().sendEmail(
                name + " <" + email + ">",
                "support@caredoc.kr",
                subject,
                content
        );
    }

    @Override
    public void onSuccessSendMail() {
        AmplitudeManager.getInstance().logEvent(CS_COMPLETE_SEND_EMAIL, AmplitudeParam.Key.TYPE, mType);

        showCommonDialog(
                getString(R.string.f_contact_succeed_title),
                getString(R.string.f_contact_succeed_desc),
                "common_dialog",
                (view) -> BackStackManager.getInstance().onBackPressed(),
                getString(R.string.common_confirm),
                "");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AmplitudeManager.getInstance().logEvent(CS_CLICK_BACK);
                BackStackManager.getInstance().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(String tag, String text) {
        mType = text;
        getViewDataBinding().fContactTypeHint.setText(text);
        getViewDataBinding().fContactTextInput.requestFocus();
    }

    @Override
    public void onClose(String tag) {

    }

    @Override
    public void onInit(String tag) {
        mType = "";
        getViewDataBinding().fContactTypeHint.setText(getResources().getString(R.string.f_contact_type_hint));
    }
}
