package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.LtiFeeSheetTypesObject;
import com.onestepmore.caredoc.data.model.api.object.RatingRelationsObject;
import com.onestepmore.caredoc.data.model.api.object.StaticServiceObject;
import com.onestepmore.caredoc.data.model.realm.Evaluation;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StaticService extends RealmObject implements Parcelable {

    // only client
    public enum SERVICE_TYPE{
        NONE(0),        // 서비스없음
        HOSPITAL(1),    // 진료/입원
        NURSING_HOME(2),    // 시설입소
        NURSING_HOME_WHO_DEMENTIA(3),   // 치매입소
        VISIT_CARE(4),  // 방문요양
        VISIT_BATH(5),  // 방문목욕
        PROTECT_FOR_DAY_AND_NIGHT(6),   // 주야간보호
        PROTECT_FOR_SHORT_TERM(7),      // 단기보호
        VISIT_NURSING(8),   // 방문간호
        WELFARE_TOOL(9),    // 복지용구
        PROTECT_WHO_DEMENTIA_FOR_DAY_AND_NIGHT(10); //치매주야간보호

        private final int id;
        SERVICE_TYPE(int id){
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

    @PrimaryKey
    private int id;
    private String category;
    private String typeName;
    private String description;
    private String warning;
    private int evaluationSourceId;
    private RealmList<StaticLtiFeeSheetType> ltiFeeSheetTypes;
    private RealmList<StaticRating> ratings;

    public StaticService(){}

    protected StaticService(Parcel in) {
        id = in.readInt();
        typeName = in.readString();
        description = in.readString();
        warning = in.readString();
        evaluationSourceId = in.readInt();
        this.ltiFeeSheetTypes = new RealmList<>();
        in.readList(this.ltiFeeSheetTypes , StaticLtiFeeSheetType.class.getClassLoader());
        this.ratings = new RealmList<>();
        in.readList(this.ratings , StaticRating.class.getClassLoader());
    }

    public void setService(StaticServiceObject service){
        this.id = service.getId();
        this.category = service.getCategory();
        this.typeName = service.getTypeName();
        this.description = service.getDescription();
        this.warning = service.getWarning();
        this.evaluationSourceId = service.getEvaluationSourceId();
        this.ltiFeeSheetTypes = new RealmList<>();
        for(LtiFeeSheetTypesObject.Item item : service.getLtiFeeSheetTypesObject().getItems()){
            StaticLtiFeeSheetType type = new StaticLtiFeeSheetType();
            type.setLtiFeeSheetType(service.getId(), item);
            this.ltiFeeSheetTypes.add(type);
        }
        this.ratings = new RealmList<>();
        for(RatingRelationsObject.Item item : service.getRatingRelations().getItems()){
            StaticRating rating = new StaticRating();
            rating.setRating(item);
            this.ratings.add(rating);
        }
    }

    public static final Creator<StaticService> CREATOR = new Creator<StaticService>() {
        @Override
        public StaticService createFromParcel(Parcel in) {
            return new StaticService(in);
        }

        @Override
        public StaticService[] newArray(int size) {
            return new StaticService[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(typeName);
        dest.writeString(description);
        dest.writeString(warning);
        dest.writeInt(evaluationSourceId);
        dest.writeList(ltiFeeSheetTypes);
        dest.writeList(ratings);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SERVICE_TYPE getServiceType() {
        SERVICE_TYPE serviceType = SERVICE_TYPE.NONE;
        for(SERVICE_TYPE type : SERVICE_TYPE.values()){
            if(type.getId() == this.getId()){
                serviceType = type;
                break;
            }
        }

        return serviceType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmList<StaticLtiFeeSheetType> getLtiFeeSheetTypes() {
        return ltiFeeSheetTypes;
    }

    public int getEvaluationSourceId() {
        return evaluationSourceId;
    }

    public String getCategory() {
        return category;
    }

    public String getWarning() {
        return warning;
    }

    public RealmList<StaticRating> getRatings() {
        return ratings;
    }
}
