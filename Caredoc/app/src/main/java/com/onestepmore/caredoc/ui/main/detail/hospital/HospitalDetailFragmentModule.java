package com.onestepmore.caredoc.ui.main.detail.hospital;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class HospitalDetailFragmentModule {
    @Provides
    HospitalDetailViewModel hospitalDetailViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                                    ReviewRepository reviewRepository) {
        return new HospitalDetailViewModel(application, dataSource, schedulerProvider, reviewRepository);
    }

    @Provides
    @Named("HospitalDetailFragment")
    ViewModelProvider.Factory provideHospitalDetailViewModelProvider(HospitalDetailViewModel hospitalDetailViewModel) {
        return new ViewModelProviderFactory<>(hospitalDetailViewModel);
    }
}
