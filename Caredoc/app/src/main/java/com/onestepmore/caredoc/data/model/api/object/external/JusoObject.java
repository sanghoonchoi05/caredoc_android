package com.onestepmore.caredoc.data.model.api.object.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JusoObject {
    @Expose
    @SerializedName("zipNo")
    public String zipNo;            // 우편번호

    @Expose
    @SerializedName("jibunAddr")
    public String jibunAddr;        // 지번 주소

    @Expose
    @SerializedName("roadAddrPart1")
    public String roadAddrPart1;    // 도로명 주소

    @Expose
    @SerializedName("rnMgtSn")
    public String rnMgtSn;      // 도로명 코드

    @Expose
    @SerializedName("bdNm")
    public String bdNm;      // 건물 이름

    @Expose
    @SerializedName("bdMgtSn")
    public String bdMgtSn;      // 관리번호

    @Expose
    @SerializedName("admCd")
    public String admCd;      // 행정구역코드(동코드)

    @Expose
    @SerializedName("buldMnnm")
    public String buldMnnm;      // 건물본번

    @Expose
    @SerializedName("buldSlno")
    public String buldSlno;      // 건물부번

    @Expose
    @SerializedName("udrtYn")
    public String udrtYn;      // 지하여부

    public String getZipNo() {
        return zipNo;
    }

    public String getJibunAddr() {
        return jibunAddr;
    }

    public String getRoadAddrPart1() {
        return roadAddrPart1;
    }

    public String getRnMgtSn() {
        return rnMgtSn;
    }

    public String getBdMgtSn() {
        return bdMgtSn;
    }

    public String getBdNm() {
        return bdNm;
    }

    public String getAdmCd() {
        return admCd;
    }

    public String getBuldMnnm() {
        return buldMnnm;
    }

    public String getBuldSlno() {
        return buldSlno;
    }

    public String getUdrtYn() {
        return udrtYn;
    }
}
