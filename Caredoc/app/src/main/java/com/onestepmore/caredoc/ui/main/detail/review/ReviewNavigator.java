package com.onestepmore.caredoc.ui.main.detail.review;

import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceResponse;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface ReviewNavigator extends BaseNavigator {
    void onLoadReviewService(ReviewFacilityServiceResponse response);
    void onCompleteRemoveReview();
}
