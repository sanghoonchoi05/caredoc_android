package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class LtiFeeSheetTypesObject extends ItemResponse<LtiFeeSheetTypesObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("type_id")
        private int typeId;

        @Expose
        @SerializedName("type_name")
        private String typeName;

        public int getTypeId() {
            return typeId;
        }

        public String getTypeName() {
            return typeName;
        }
    }
}
