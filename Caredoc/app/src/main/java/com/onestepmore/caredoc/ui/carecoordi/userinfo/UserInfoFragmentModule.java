package com.onestepmore.caredoc.ui.carecoordi.userinfo;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class UserInfoFragmentModule {
    @Provides
    UserInfoViewModel userInfoViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new UserInfoViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("UserInfoFragment")
    ViewModelProvider.Factory provideUserInfoViewModelProvider(UserInfoViewModel userInfoViewModel) {
        return new ViewModelProviderFactory<>(userInfoViewModel);
    }
}
