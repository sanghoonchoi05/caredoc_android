package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class EvaluationsMedicalObject extends ItemResponse<EvaluationsMedicalObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("id")
        private long id;

        @Expose
        @SerializedName("total_grade")
        private String totalGrade;

        @Expose
        @SerializedName("year")
        private String year;

        @Expose
        @SerializedName("patient_count_per_doctor")
        private Float patientCountPerDoctor;

        @Expose
        @SerializedName("patient_count_per_nurse")
        private Float patientCountPerNurse;

        @Expose
        @SerializedName("patient_count_per_nusing_staff")
        private Float patientCountPerNusingStaff;

        // 방밖으로 나오기 악화 환자분율_치매환자군(%)
        @Expose
        @SerializedName("leaving_room_dementia_patient")
        private Float leavingRoomDementiaPatient;

        // 방밖으로 나오기 악화 환자분율_치매환자제외군(%)
        @Expose
        @SerializedName("leaving_room_dementia_patient_excluded")
        private Float leavingRoomDementiaPatientExcluded;

        // 일상생활수행능력 감퇴환자 분율_치매환자군(%)
        @Expose
        @SerializedName("dementia_patient")
        private Float dementiaPatient;

        // 일상생활수행능력 감퇴환자 분율_치매환자제외군(%)
        @Expose
        @SerializedName("dementia_patient_excluded")
        private Float dementiaPatientExcluded;

        // 욕창이 악화된 환자분율_고위험군(%)
        @Expose
        @SerializedName("ulcers_worsened_high_risk_group")
        private Float ulcersWorsenedHighRiskGroup;

        // 욕창이 새로 생긴 환자분율_저위험군(%)
        @Expose
        @SerializedName("pressure_ulcers_low_risk_group")
        private Float pressureUlcersLowRiskGroup;

        // 욕창이 새로 생긴 환자분율_고위험군(%)
        @Expose
        @SerializedName("newly_developed_pressure_ulcers_high_risk_group")
        private Float newlyDevelopedPressureUlcersHighRiskGroup;

        // 욕창 개선 환자분율_고위험군(%)
        @Expose
        @SerializedName("improved_pressure_ulcers_high_risk_group")
        private Float improvedPressureUlcersHighRiskGroup;

        public long getId() {
            return id;
        }

        public String getTotalGrade() {
            return totalGrade;
        }

        public String getYear() {
            return year;
        }

        public Float getPatientCountPerDoctor() {
            return patientCountPerDoctor;
        }

        public Float getPatientCountPerNurse() {
            return patientCountPerNurse;
        }

        public Float getPatientCountPerNusingStaff() {
            return patientCountPerNusingStaff;
        }

        public Float getLeavingRoomDementiaPatient() {
            return leavingRoomDementiaPatient;
        }

        public Float getLeavingRoomDementiaPatientExcluded() {
            return leavingRoomDementiaPatientExcluded;
        }

        public Float getDementiaPatient() {
            return dementiaPatient;
        }

        public Float getDementiaPatientExcluded() {
            return dementiaPatientExcluded;
        }

        public Float getUlcersWorsenedHighRiskGroup() {
            return ulcersWorsenedHighRiskGroup;
        }

        public Float getPressureUlcersLowRiskGroup() {
            return pressureUlcersLowRiskGroup;
        }

        public Float getNewlyDevelopedPressureUlcersHighRiskGroup() {
            return newlyDevelopedPressureUlcersHighRiskGroup;
        }

        public Float getImprovedPressureUlcersHighRiskGroup() {
            return improvedPressureUlcersHighRiskGroup;
        }
    }
}
