package com.onestepmore.caredoc.ui.carecoordi.service;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.databinding.FServiceBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

public class ServiceFragment extends BaseCoordiFragment<FServiceBinding, ServiceViewModel> implements
        ServiceNavigator,
        ServiceClickCallback{

    @Inject
    @Named("ServiceFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private ServiceViewModel mServiceViewModel;

    private ServiceAdapter mServiceAdapter;

    private StaticService selectedService = null;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_service;
    }

    @Override
    public ServiceViewModel getViewModel() {
        mServiceViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ServiceViewModel.class);
        return mServiceViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceViewModel.setNavigator(this);

        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setHomeAsUpIndicator(R.drawable.vec_ic_close_white_24dp);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List<StaticService> staticServiceList = getViewModel().getRealm().where(StaticService.class)
                .equalTo("serviceableCode","FC-01")
                .or()
                .equalTo("serviceableCode","FC-02")
                .or()
                .equalTo("serviceableCode","FC-03")
                .findAll();

        mServiceAdapter = new ServiceAdapter(this);
        mServiceAdapter.setServiceList(staticServiceList);
        getViewDataBinding().fServiceRecyclerview.setAdapter(mServiceAdapter);
        getViewDataBinding().fServiceRecyclerview.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_service_item_bottom_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(spacingInPixels, 0, true);
        spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.step_button_height);
        decoration.setSpaceLastVertical(spacingInPixels);
        getViewDataBinding().fServiceRecyclerview.addItemDecoration(decoration);

        //getViewDataBinding().fServiceNextButton.setCommonNavigator(this);
        //getViewDataBinding().fServiceNextButtonAnchor.setCommonNavigator(this);
    }

    @Override
    public void setTitle(int titleResId) {
        UIUtils.setTextIncludeAnnotation(getBaseActivity(), titleResId, getViewDataBinding().fServiceTitle);
    }

    @Override
    public void setStepData(StepData stepData) {
        if(stepData == null){
            return;
        }

        setTitle(CommonUtils.getCareCoordiTitleResId(stepData));
    }

    @Override
    public void onClick(StaticService service) {
        selectedService = service;

        ((CareCoordiActivity)getBaseActivity()).selectStaticService(service);
    }
}
