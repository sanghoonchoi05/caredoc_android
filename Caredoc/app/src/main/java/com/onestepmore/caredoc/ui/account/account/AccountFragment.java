package com.onestepmore.caredoc.ui.account.account;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.data.OAuthLoginState;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.FAccountBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.account.login.LoginFragment;
import com.onestepmore.caredoc.ui.account.register.email.EmailRegisterFragment;
import com.onestepmore.caredoc.ui.account.terms.TermsFragment;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;

import java.lang.ref.WeakReference;
import java.util.Arrays;

import javax.inject.Inject;
import javax.inject.Named;

public class AccountFragment extends BaseFragment<FAccountBinding, AccountViewModel> implements
        AccountNavigator,BaseActivity.OnBaseActivityListener
{
    @Inject
    @Named("AccountFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private AccountViewModel mAccountViewModel;

    private CallbackManager mFBCallbackManager;

    /**
     * client 정보를 넣어준다.
     */
    private static String OAUTH_CLIENT_ID = "elXupdnP5U5wrk5mHxdZ";
    private static String OAUTH_CLIENT_SECRET = "D8TMAOLzUF";
    private static String OAUTH_CLIENT_NAME = "네이버 아이디로 로그인";

    private static OAuthLogin mOAuthLoginInstance;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_account;
    }

    @Override
    public AccountViewModel getViewModel() {
        mAccountViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AccountViewModel.class);
        return mAccountViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAccountViewModel.setNavigator(this);

        mFBCallbackManager = CallbackManager.Factory.create();

        mOAuthLoginInstance = OAuthLogin.getInstance();

        mOAuthLoginInstance.showDevelopersLog(BuildConfig.DEBUG);
        mOAuthLoginInstance.init(getBaseActivity(), OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET, OAUTH_CLIENT_NAME);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mFBCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showEmailRegisterView() {
        BackStackManager.getInstance().addChildFragment(TermsFragment.newInstance(true), R.id.v_app_bar_start_fragment_layout);
    }

    @Override
    public void showLoginView() {
        BackStackManager.getInstance().addChildFragment(new LoginFragment(), R.id.v_app_bar_start_fragment_layout);
    }

    @Override
    public void showFacebookView() {
        BackStackManager.getInstance().addChildFragment(TermsFragment.newInstance(false), R.id.v_app_bar_start_fragment_layout);
    }

    @Override
    public void showNaverView() {
        mOAuthLoginInstance.startOauthLoginActivity(getBaseActivity(), mOAuthLoginHandler );
    }

    @Override
    public void openMainActivity() {
        ((AccountMainActivity)getBaseActivity()).openMainActivity();
    }

    private void onResultNaverLogin(boolean success){
        if (success) {
            OAuthLoginState state = mOAuthLoginInstance.getState(getBaseActivity());
            String accessToken = mOAuthLoginInstance.getAccessToken(getBaseActivity());
            String refreshToken = mOAuthLoginInstance.getRefreshToken(getBaseActivity());
            long expiresAt = mOAuthLoginInstance.getExpiresAt(getBaseActivity());
            String tokenType = mOAuthLoginInstance.getTokenType(getBaseActivity());
            if(state == OAuthLoginState.OK){
                getViewModel().getNaverUserInfo(tokenType + " " + accessToken);
            }

        } else {
            String errorCode = mOAuthLoginInstance.getLastErrorCode(getBaseActivity()).getCode();
            String errorDesc = mOAuthLoginInstance.getLastErrorDesc(getBaseActivity());
            Toast.makeText(getBaseActivity(), errorDesc, Toast.LENGTH_SHORT).show();
        }
    }

    OAuthLoginHandler mOAuthLoginHandler = new NaverLoginHandler(this);
    private static class NaverLoginHandler extends OAuthLoginHandler {
        private final WeakReference<AccountFragment> mFragment;

        public NaverLoginHandler(AccountFragment fragment) {
            mFragment = new WeakReference<AccountFragment>(fragment);
        }
        @Override
        public void run(boolean success) {
            AccountFragment fragment = mFragment.get();
            if(fragment != null){
                fragment.onResultNaverLogin(success);
            }
        }
    };

    @Override
    public void onShow() {
        if(getArguments() != null){
            if(getArguments().getBoolean("agree")){
                /*LoginManager.getInstance().registerCallback(mFBCallbackManager, mAccountViewModel);
                LoginManager.getInstance().logInWithReadPermissions(getBaseActivity(), Arrays.asList("public_profile", "email"));*/
                LoginButton loginButton = new LoginButton(getBaseActivity());
                loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
                loginButton.registerCallback(mFBCallbackManager, mAccountViewModel);
                loginButton.performClick();
            }

            setArguments(null);
        }
    }
}
