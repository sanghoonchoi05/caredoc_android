package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewServiceObject {
    @Expose
    @SerializedName("id")
    private long id;

    @Expose
    @SerializedName("rating")
    private float rating;

    @Expose
    @SerializedName("ratings")
    private RatingsObject ratings;

    @Expose
    @SerializedName("content")
    private String content;

    @Expose
    @SerializedName("reviewable")
    private ReviewableObject reviewable;

    @Expose
    @SerializedName("created_at")
    private String createdAt;

    @Expose
    @SerializedName("updated_at")
    private String updatedAt;

    @Expose
    @SerializedName("is_own")
    private boolean isOwn;

    @Expose
    @SerializedName("is_exposed")
    private boolean isExposed;

    @Expose
    @SerializedName("writer")
    private WriterObject writer;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public void setOwn(boolean own) {
        isOwn = own;
    }

    public WriterObject getWriter() {
        return writer;
    }

    public void setWriter(WriterObject writer) {
        this.writer = writer;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public boolean isExposed() {
        return isExposed;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public float getRating() {
        return rating;
    }

    public RatingsObject getRatings() {
        return ratings;
    }

    public ReviewableObject getReviewable() {
        return reviewable;
    }
}
