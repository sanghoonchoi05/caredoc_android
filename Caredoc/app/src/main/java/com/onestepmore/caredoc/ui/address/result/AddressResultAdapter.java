package com.onestepmore.caredoc.ui.address.result;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Juso;
import com.onestepmore.caredoc.databinding.IAddressResultVerticalFooterBinding;
import com.onestepmore.caredoc.databinding.IJusoBinding;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class AddressResultAdapter extends RealmRecyclerViewAdapter<Juso, AddressResultAdapter.AddressResultViewHolder> {

    private final int FOOTER_VIEW = 1;
    private boolean mShowFooter = true;
    private final AddressResultNavigator mAddressResultNavigator;

    AddressResultAdapter(OrderedRealmCollection<Juso> data, AddressResultNavigator addressResultNavigator) {
        super(data, true);

        mAddressResultNavigator = addressResultNavigator;
    }

    public void setShowFooter(boolean showFooter) {
        this.mShowFooter = showFooter;
        if(getData() != null){
            notifyItemChanged(getData().size());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(mShowFooter && getData() != null && position == getData().size()){
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if(getData() == null || !getData().isValid()){
            return 0;
        }

        int addCount = 0;
        if(mShowFooter){
            addCount = 1;
        }

        return getData().size() + addCount;
    }

    @NonNull
    @Override
    public AddressResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == FOOTER_VIEW) {
            IAddressResultVerticalFooterBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_address_result_vertical_footer, viewGroup, false);

            return new AddressResultViewHolder(binding);
        }

        IJusoBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_juso, viewGroup, false);
        return new AddressResultViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AddressResultViewHolder addressResultViewHolder, int position) {
        OrderedRealmCollection data = getData();
        if (data != null) {
            if(addressResultViewHolder.binding instanceof IJusoBinding){
                IJusoBinding binding = (IJusoBinding)addressResultViewHolder.binding;
                final Juso juso = getData().get(position);
                binding.setJuso(juso);
                binding.setNavigator(mAddressResultNavigator);
            }
            addressResultViewHolder.binding.executePendingBindings();
        }
    }

    static class AddressResultViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        private AddressResultViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
