package com.onestepmore.caredoc.ui.main.taker.add.basic;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerBasicFragmentModule {
    @Provides
    TakerBasicViewModel takerBasicViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerBasicViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerBasicFragment")
    ViewModelProvider.Factory provideTakerBasicViewModelProvider(TakerBasicViewModel takerBasicViewModel) {
        return new ViewModelProviderFactory<>(takerBasicViewModel);
    }
}
