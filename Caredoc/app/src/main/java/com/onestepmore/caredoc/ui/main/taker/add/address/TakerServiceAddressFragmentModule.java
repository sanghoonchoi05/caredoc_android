package com.onestepmore.caredoc.ui.main.taker.add.address;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerServiceAddressFragmentModule {
    @Provides
    TakerServiceAddressViewModel takerServiceAddressViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerServiceAddressViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerServiceAddressFragment")
    ViewModelProvider.Factory provideTakerServiceAddressViewModelProvider(TakerServiceAddressViewModel takerServiceAddressViewModel) {
        return new ViewModelProviderFactory<>(takerServiceAddressViewModel);
    }
}
