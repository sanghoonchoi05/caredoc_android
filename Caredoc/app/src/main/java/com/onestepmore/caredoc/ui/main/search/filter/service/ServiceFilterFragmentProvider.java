package com.onestepmore.caredoc.ui.main.search.filter.service;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ServiceFilterFragmentProvider {
    @ContributesAndroidInjector(modules = ServiceFilterFragmentModule.class)
    abstract ServiceFilterFragment provideServiceFilterFragmentFactory();
}
