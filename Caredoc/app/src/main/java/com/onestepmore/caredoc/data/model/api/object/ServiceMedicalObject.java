package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class ServiceMedicalObject extends ServiceDetailObject{
    @Expose
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public final class Data{
        @Expose
        @SerializedName("key")
        private String key;

        @Expose
        @SerializedName("funeral")
        private int funeral;

        @Expose
        @SerializedName("caring_form")
        private String caringForm;

        @Expose
        @SerializedName("person_per_doctor")
        private String personPerDoctor;
        @Expose
        @SerializedName("person_per_nurse")
        private String personPerNurse;
        @Expose
        @SerializedName("person_per_nursing_staff")
        private String personPerNursingStaff;

        @Expose
        @SerializedName("medical_expenses_avg")
        private float medicalExpensesAvg;

        @Expose
        @SerializedName("person_per_doctor_avg")
        private String personPerDoctorAvg;
        @Expose
        @SerializedName("person_per_nurse_avg")
        private String personPerNurseAvg;
        @Expose
        @SerializedName("person_per_nursing_staff_avg")
        private String personPerNursingStaffAvg;

        @Expose
        @SerializedName("medical_expenses_diff")
        private float medicalExpensesDiff;
        @Expose
        @SerializedName("person_per_doctor_diff")
        private Float personPerDoctorDiff;
        @Expose
        @SerializedName("person_per_nurse_diff")
        private Float personPerNurseDiff;
        @Expose
        @SerializedName("person_per_nursing_staff_diff")
        private Float personPerNursingStaffDiff;

        @Expose
        @SerializedName("first_patient_disease")
        private String firstPatientDisease;
        @Expose
        @SerializedName("second_patient_disease")
        private String secondPatientDisease;
        @Expose
        @SerializedName("third_patient_disease")
        private String thirdPatientDisease;
        @Expose
        @SerializedName("patient_disease")
        private String patientDisease;
        @Expose
        @SerializedName("patient_self_help")
        private String patientSelfHelp;
        @Expose
        @SerializedName("patient_need_help")
        private String patientNeedHelp;
        @Expose
        @SerializedName("patient_need_full_help")
        private String patientNeedFullHelp;

        @Expose
        @SerializedName("medical_expenses")
        private int medicalExpenses;

        @Expose
        @SerializedName("emergency_day")
        private boolean emergencyDay;
        @Expose
        @SerializedName("emergency_day_contact")
        private String emergencyDayContact;
        @Expose
        @SerializedName("emergency_night")
        private boolean emergencyNight;
        @Expose
        @SerializedName("emergency_night_contact")
        private String emergencyNightContact;

        @Expose
        @SerializedName("meal_add_ons")
        private MealAddOnsObject mealAddOnsObject;

        @Expose
        @SerializedName("nursing_add_ons")
        private NursingAddOnsObject nursingAddOnsObject;

        @Expose
        @SerializedName("medical_subjects")
        private MedicalSubjectObject medicalSubjectObject;

        @Expose
        @SerializedName("programs")
        private ProgramsObject programs;

        @Expose
        @SerializedName("subs")
        private SubsObject subsObjects;

        @Expose
        @SerializedName("employees")
        private EmployeesObject employeesObject;

        @Expose
        @SerializedName("time_tables")
        private TimeTablesObject timeTables;

        @Expose
        @SerializedName("equipment")
        private EquipmentObject equipment;

        @Expose
        @SerializedName("evaluations")
        private EvaluationsMedicalObject evaluationsMedicalObject;

        public String getKey() {
            return key;
        }

        public int getFuneral() {
            return funeral;
        }

        public String getCaringForm() {
            return caringForm;
        }

        public String getPersonPerDoctor() {
            return personPerDoctor;
        }

        public String getPersonPerNurse() {
            return personPerNurse;
        }

        public String getPersonPerNursingStaff() {
            return personPerNursingStaff;
        }

        public float getMedicalExpensesAvg() {
            return medicalExpensesAvg;
        }

        public String getPersonPerDoctorAvg() {
            return personPerDoctorAvg;
        }

        public String getPersonPerNurseAvg() {
            return personPerNurseAvg;
        }

        public String getPersonPerNursingStaffAvg() {
            return personPerNursingStaffAvg;
        }

        public float getMedicalExpensesDiff() {
            return medicalExpensesDiff;
        }

        public Float getPersonPerDoctorDiff() {
            return personPerDoctorDiff;
        }

        public Float getPersonPerNurseDiff() {
            return personPerNurseDiff;
        }

        public Float getPersonPerNursingStaffDiff() {
            return personPerNursingStaffDiff;
        }

        public String getFirstPatientDisease() {
            return firstPatientDisease;
        }

        public String getSecondPatientDisease() {
            return secondPatientDisease;
        }

        public String getThirdPatientDisease() {
            return thirdPatientDisease;
        }

        public String getPatientDisease() {
            return patientDisease;
        }

        public String getPatientSelfHelp() {
            return patientSelfHelp;
        }

        public String getPatientNeedHelp() {
            return patientNeedHelp;
        }

        public String getPatientNeedFullHelp() {
            return patientNeedFullHelp;
        }

        public int getMedicalExpenses() {
            return medicalExpenses;
        }

        public boolean getEmergencyDay() {
            return emergencyDay;
        }

        public String getEmergencyDayContact() {
            return emergencyDayContact;
        }

        public boolean getEmergencyNight() {
            return emergencyNight;
        }

        public String getEmergencyNightContact() {
            return emergencyNightContact;
        }

        public MealAddOnsObject getMealAddOnsObject() {
            return mealAddOnsObject;
        }

        public NursingAddOnsObject getNursingAddOnsObject() {
            return nursingAddOnsObject;
        }

        public MedicalSubjectObject getMedicalSubjectObject() {
            return medicalSubjectObject;
        }

        public EmployeesObject getEmployees() {
            return employeesObject;
        }

        public ProgramsObject getPrograms() {
            return programs;
        }

        public SubsObject getSubs() {
            return subsObjects;
        }

        public TimeTablesObject getTimeTables() {
            return timeTables;
        }

        public EvaluationsMedicalObject getEvaluationsMedicalObject() {
            return evaluationsMedicalObject;
        }

        public EquipmentObject getEquipment() {
            return equipment;
        }
    }
}
