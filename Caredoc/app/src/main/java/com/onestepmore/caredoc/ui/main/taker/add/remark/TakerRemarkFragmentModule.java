package com.onestepmore.caredoc.ui.main.taker.add.remark;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerRemarkFragmentModule {
    @Provides
    TakerRemarkViewModel takerRemarkViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerRemarkViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerRemarkFragment")
    ViewModelProvider.Factory provideTakerRemarkViewModelProvider(TakerRemarkViewModel takerRemarkViewModel) {
        return new ViewModelProviderFactory<>(takerRemarkViewModel);
    }
}
