package com.onestepmore.caredoc.firebase;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.onestepmore.caredoc.ui.account.account.AccountFragment;
import com.onestepmore.caredoc.ui.account.login.LoginFragment;
import com.onestepmore.caredoc.ui.account.login.forgot.LoginForgotFragment;
import com.onestepmore.caredoc.ui.account.register.email.EmailRegisterFragment;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiStartActivity;
import com.onestepmore.caredoc.ui.carecoordi.result.ResultFragment;
import com.onestepmore.caredoc.ui.main.detail.FacilityDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.comment.write.WriteCommentActivity;
import com.onestepmore.caredoc.ui.main.detail.review.write.WriteReviewActivity;
import com.onestepmore.caredoc.ui.main.favorite.FavoriteFragment;
import com.onestepmore.caredoc.ui.main.home.HomeFragment;
import com.onestepmore.caredoc.ui.main.profile.ProfileFragment;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.map.SearchMapFragment;
import com.onestepmore.caredoc.ui.splash.SplashActivity;
import com.onestepmore.caredoc.ui.start.StartActivity;

import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.CARECOORDI;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.EMAIL;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.FACILITY;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.LOGIN;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.REGISTER;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.RESET_PASSWORD;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.RESULT_CARECOORDI;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.SPLASH;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.START;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.TAB_BOOKMARK;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.TAB_HOME;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.TAB_PROFILE;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.TAB_SEARCH;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.TAB_SEARCH_MAP;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.WRITE_REVIEW;
import static com.onestepmore.caredoc.firebase.FirebaseEvent.Screen.WRITE_USER_REVIEW;

public class FirebaseEvent {
    public static class Screen{
        public static final String SPLASH = "Splash";
        public static final String START = "Start";
        public static final String REGISTER = "Register";
        public static final String EMAIL = "Email";
        public static final String LOGIN = "Login";
        public static final String RESET_PASSWORD = "Reset_Password";
        public static final String TAB_HOME = "Tab_Home";
        public static final String TAB_SEARCH = "Tab_Search";
        public static final String TAB_SEARCH_MAP = "Tab_Search_Map";
        public static final String TAB_BOOKMARK = "Tab_Bookmark";
        public static final String TAB_PROFILE = "Tab_Profile";
        public static final String FACILITY = "Facility";
        public static final String WRITE_REVIEW = "Write_Review";
        public static final String WRITE_USER_REVIEW = "Write_User_Review";
        public static final String CARECOORDI = "CareCoordi";
        public static final String RESULT_CARECOORDI = "Result_CareCoordi";
        public static final String REQUEST_ESTIMATE = "Request_Estimate";
        public static final String REQUEST_ESTIMATE_SIMPLE = "Request_Estimate_Simple";
        public static final String NEED_LOGIN_POPUP = "Need_Login_Popup";
        public static final String FORCE_UPDATE_POPUP = "Force_Update_Popup";
        public static final String OPTIONAL_UPDATE_NOTI = "Optional_Update_Noti";
    }

    public static class ClickAction{
        public static final String START_CLICK_REGISTER = "Start_Click_Register";
        public static final String START_CLICK_CARECOORDI = "Start_Click_CareCoordi";
        public static final String START_CLICK_HOME = "Start_Click_Home";
        public static final String REGISTER_CLICK_EMAIL = "Register_Click_Email";
        public static final String REGISTER_CLICK_LOGIN = "Register_Click_Login";
        public static final String LOGIN_CLICK_RESET_PASSWORD = "Login_Click_Reset_Password";
        public static final String TAB_HOME_CLICK_SERVICE = "Tab_Home_Click_Service";
        public static final String TAB_HOME_CLICK_CALL = "Tab_Home_Click_Call";
        public static final String TAB_HOME_CLICK_EMAIL = "Tab_Home_Click_Email";
        public static final String TAB_HOME_CLICK_KAKAOPLUS = "Tab_Home_Click_KakaoPlus";
        public static final String TAB_HOME_CLICK_CARE_QUESTION = "Tab_Home_Click_Question";
        public static final String TAB_SEARCH_CLICK_FACILITY = "Tab_Search_Click_Facility";
        public static final String FACILITY_CLICK_CALL = "Facility_Click_Call";
        public static final String FACILITY_CLICK_SHARE = "Facility_Click_Share";
        public static final String FACILITY_CLICK_BOOKMARK_SAVE = "Facility_Click_Bookmark_Save";
        public static final String FACILITY_CLICK_HOMEPAGE = "Facility_Click_Homepage";

        public static final String CARECOORDI_CLICK_START = "CareCoordi_Click_Start";
        public static final String RESULT_CARECOORDI_CLICK_RETRY = "Result_CareCoordi_Click_Retry";
        public static final String NEED_LOGIN_POPUP_CLICK_START = "Need_Login_Popup_Click_Start";
        public static final String FORCE_UPDATE_POPUP_CLICK_UPDATE = "Force_Update_Popup_Click_Update";
        public static final String OPTIONAL_UPDATE_NOTI_CLICK_UPDATE = "Optional_Update_Noti_Click_Update";
    }

    public static class Situation{
        public static final String LOGIN_AUTO_COMPLETE = "Login_Auto_Complete";
        public static final String REGISTER_FACEBOOK_COMPLETE = "Register_Facebook_Complete";
        public static final String REGISTER_NAVER_COMPLETE = "Register_Naver_Complete";
        public static final String REGISTER_KAKAO_COMPLETE = "Register_Kakao_Complete";
        public static final String EMAIL_REGISTER_COMPLETE = "Email_Register_Complete";
        public static final String LOGIN_COMPLETE = "Login_Complete";
        public static final String RESET_PASSWORD_COMPLETE = "Reset_Password_Complete";
        public static final String TAB_PROFILE_LOGOUT_COMPLETE = "Tab_Profile_Logout_Complete";
        public static final String WRITE_REVIEW_COMPLETE = "Write_Review_Complete";
        public static final String WRITE_USER_REVIEW_COMPLETE = "Write_User_Review_Complete";
        public static final String EXIT = "Exit";
    }

    public static class PredefinedEvent{
        public static final String SIGN_UP_COMPLETE = FirebaseAnalytics.Event.SIGN_UP;
        public static final String LOGIN_COMPLETE = FirebaseAnalytics.Event.LOGIN;
        public static final String SEARCH = FirebaseAnalytics.Event.SEARCH;
        public static final String SELECT_CONTENT = FirebaseAnalytics.Event.SELECT_CONTENT;
        public static final String SHARE = FirebaseAnalytics.Event.SHARE;
    }

    public static String getScreenName(BaseActivity baseActivity){
        return getEventName(baseActivity);
    }

    public static String getEventName(BaseActivity activity){
        String screenName = "";
        if(activity instanceof SplashActivity){
            screenName = SPLASH;
        }
        else if(activity instanceof StartActivity){
            screenName = START;
        }
        else if(activity instanceof CareCoordiStartActivity){
            screenName = CARECOORDI;
        }
        else if(activity instanceof WriteCommentActivity){
            screenName = WRITE_REVIEW;
        }
        else if(activity instanceof WriteReviewActivity){
            screenName = WRITE_USER_REVIEW;
        }

        return screenName;
    }

    public static String getScreenName(BaseFragment baseFragment){
        return getEventName(baseFragment);
    }

    public static String getEventName(BaseFragment fragment){
        String screenName = "";
        if(fragment instanceof AccountFragment){
            screenName = REGISTER;
        }
        else if(fragment instanceof LoginFragment){
            screenName = LOGIN;
        }
        else if(fragment instanceof EmailRegisterFragment){
            screenName = EMAIL;
        }
        else if(fragment instanceof LoginForgotFragment){
            screenName = RESET_PASSWORD;
        }
        else if(fragment instanceof HomeFragment){
            screenName = TAB_HOME;
        }
        else if(fragment instanceof SearchFragment){
            screenName = TAB_SEARCH;
        }
        else if(fragment instanceof SearchMapFragment){
            screenName = TAB_SEARCH_MAP;
        }
        else if(fragment instanceof FavoriteFragment){
            screenName = TAB_BOOKMARK;
        }
        else if(fragment instanceof ProfileFragment){
            screenName = TAB_PROFILE;
        }
        else if(fragment instanceof ResultFragment){
            screenName = RESULT_CARECOORDI;
        }
        else if(fragment instanceof FacilityDetailFragment){
            screenName = FACILITY;
        }

        return screenName;
    }
}
