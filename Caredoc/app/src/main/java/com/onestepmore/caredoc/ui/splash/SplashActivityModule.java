package com.onestepmore.caredoc.ui.splash;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class SplashActivityModule {

    @Provides
    SplashViewModel splashViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider, Gson gson) {
        return new SplashViewModel(application, dataSource, schedulerProvider, gson);
    }

    @Provides
    ViewModelProvider.Factory provideSplashViewModelProvider(SplashViewModel splashViewModel) {
        return new ViewModelProviderFactory<>(splashViewModel);
    }
}
