package com.onestepmore.caredoc.ui.main.home;

import android.app.Application;
import android.view.View;

import com.onestepmore.caredoc.data.model.realm.FacilitySearchTemp;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import io.realm.RealmResults;

public class HomeViewModel extends BaseViewModel<HomeNavigator>{

    private FacilityDataSource mFacilityDataSource;

    public FacilityDataSource getFacilityRepository() {
        return mFacilityDataSource;
    }

    public HomeViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider, FacilityDataSource facilityDataSource) {
        super(application, appDataSource, schedulerProvider);

        mFacilityDataSource = facilityDataSource;
    }
    public void onSearch(View view){
        getNavigator().onSearch();
    }
}
