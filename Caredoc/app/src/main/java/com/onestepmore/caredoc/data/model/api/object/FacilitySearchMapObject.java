package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

import java.util.List;

public class FacilitySearchMapObject extends ItemResponse<FacilitySearchMapObject.Item> {
    public enum GROUP_BY{
        SIDO("sido"),
        GUGUN("gugun"),
        DONG("dong"),
        MARKER("marker");

        private String text;
        GROUP_BY(String text){
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

    @Expose
    @SerializedName("next_move")
    private String nextMove;

    @Expose
    @SerializedName("next_zoom")
    private int nextZoom;

    @Expose
    @SerializedName("next_location")
    private String nextLocation;

    @Expose
    @SerializedName("group_by")
    private String groupBy;

    @Expose
    @SerializedName("code")
    private String code;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("count")
    private int count;

    @Expose
    @SerializedName("lng")
    private double lng;

    @Expose
    @SerializedName("lat")
    private double lat;

    public class Item{
        @Expose
        @SerializedName("id")
        private long id;

        @Expose
        @SerializedName("name")
        private String name;

        public String getName() {
            return name;
        }

        public long getId() {
            return id;
        }
    }

    public String getAllName(){
        StringBuilder names = new StringBuilder();
        for(int i=0; i<getItems().size(); i++){
            Item item = getItems().get(i);
            names.append(item.getName());

            if(i != getItems().size()-1){
                names.append("\n");
            }
        }

        return names.toString();
    }

    public String getNextMove() {
        return nextMove;
    }

    public int getNextZoom() {
        return nextZoom;
    }

    public String getNextLocation() {
        return nextLocation;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }
}
