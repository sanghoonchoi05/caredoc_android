package com.onestepmore.caredoc.ui.main.detail;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.object.EvaluationObject;
import com.onestepmore.caredoc.databinding.IFacilityDetailEvaluationBinding;
import com.onestepmore.caredoc.utils.CommonUtils;

import java.lang.ref.WeakReference;
import java.util.List;

public class FacilityEvaluationListAdapter extends RecyclerView.Adapter<FacilityEvaluationListAdapter.FacilityEvaluationListViewHolder> {
    private List<EvaluationObject> mEvaluationList = null;
    private WeakReference<Context> mContext;

    public FacilityEvaluationListAdapter(Context context, List<EvaluationObject> evaluations) {
        this.mContext = new WeakReference<>(context);
        this.mEvaluationList = evaluations;
    }

    @NonNull
    @Override
    public FacilityEvaluationListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFacilityDetailEvaluationBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_detail_evaluation, viewGroup, false);
        return new FacilityEvaluationListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FacilityEvaluationListViewHolder facilityViewHolder, int position) {
        if (mEvaluationList == null || mEvaluationList.size() == 0) {
            facilityViewHolder.binding.setEvaluation(null);
            facilityViewHolder.binding.setGradeIcon(CommonUtils.getOvalDrawableWithGrade(mContext.get(), null, false));
        } else {
            EvaluationObject evaluation = mEvaluationList.get(position);
            facilityViewHolder.binding.setEvaluation(evaluation);
            facilityViewHolder.binding.setGradeIcon(CommonUtils.getOvalDrawableWithGrade(mContext.get(), evaluation.getGrade(), false));
        }

        facilityViewHolder.binding.executePendingBindings();
    }

    private int getColorStateIdList(String grade) {
        int colorStateList = R.color.rect_facility_detail_grade_chart_no_grade_selector;
        if (grade.equals("E") || grade.equals("5")) {
            colorStateList = R.color.rect_facility_detail_grade_chart_grade5_selector;
        } else if (grade.equals("D") || grade.equals("4")) {
            colorStateList = R.color.rect_facility_detail_grade_chart_grade4_selector;
        } else if (grade.equals("C") || grade.equals("3")) {
            colorStateList = R.color.rect_facility_detail_grade_chart_grade3_selector;
        } else if (grade.equals("B") || grade.equals("2")) {
            colorStateList = R.color.rect_facility_detail_grade_chart_grade2_selector;
        } else if (grade.equals("A") || grade.equals("1")) {
            colorStateList = R.color.rect_facility_detail_grade_chart_grade1_selector;
        }

        return colorStateList;
    }

    private int getDrawableId(String grade) {
        int drawableId = R.drawable.rect_facility_detail_grade_chart_no_grade_selector;
        if (grade.equals("E") || grade.equals("5")) {
            drawableId = R.drawable.rect_facility_detail_grade_chart_grade5_selector;
        } else if (grade.equals("D") || grade.equals("4")) {
            drawableId = R.drawable.rect_facility_detail_grade_chart_grade4_selector;
        } else if (grade.equals("C") || grade.equals("3")) {
            drawableId = R.drawable.rect_facility_detail_grade_chart_grade3_selector;
        } else if (grade.equals("B") || grade.equals("2")) {
            drawableId = R.drawable.rect_facility_detail_grade_chart_grade2_selector;
        } else if (grade.equals("A") || grade.equals("1")) {
            drawableId = R.drawable.rect_facility_detail_grade_chart_grade1_selector;
        }
        return drawableId;
    }

    @Override
    public int getItemCount() {
        int count = 1;
        if (mEvaluationList != null && mEvaluationList.size() > 0) {
            count = mEvaluationList.size();
        }
        return count;
    }

    static class FacilityEvaluationListViewHolder extends RecyclerView.ViewHolder {
        final IFacilityDetailEvaluationBinding binding;

        private FacilityEvaluationListViewHolder(IFacilityDetailEvaluationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}