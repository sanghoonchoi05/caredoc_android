package com.onestepmore.caredoc.ui.main.home;

import android.animation.ValueAnimator;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.Suggestion;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.databinding.FHomeBinding;
import com.onestepmore.caredoc.databinding.VHomeMenuBinding;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.profile.ProfileFragment;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.dialog.SearchDialog;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.ui.widgets.EventDialog;
import com.onestepmore.caredoc.ui.widgets.StartPagerSnapHelper;
import com.onestepmore.caredoc.utils.UIUtils;
import com.onestepmore.caredoc.utils.UnitUtils;
import com.zoyi.channel.plugin.android.ChannelIO;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.HOME_CLICK_AUTH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.HOME_CLICK_FACILITY_CATEGORY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.HOME_CLICK_PROFILE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.HOME_CLICK_SEARCH_KEYWORD;
import static com.onestepmore.caredoc.ui.main.search.SearchFragment.SEARCH_CATEGORY_KEY;

public class HomeFragment extends BaseFragment<FHomeBinding, HomeViewModel> implements
        HomeNavigator,
        BaseFragment.OnBackPressedListener,
        SearchDialog.OnSearchListener,
        SearchCategoryClickCallback
{

    @Inject
    @Named("HomeFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private HomeViewModel mHomeViewModel;

    private RecyclerView.SmoothScroller mSmoothScroller;
    private Timer mTimer;
    private int mPosition = 0;
    private VHomeMenuBinding mMenuBinding;

    public static HomeFragment newInstance(String key, long fakeId) {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        args.putLong(key, fakeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_home;
    }

    @Override
    public HomeViewModel getViewModel() {
        mHomeViewModel = ViewModelProviders.of(this, mViewModelFactory).get(HomeViewModel.class);
        return mHomeViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHomeViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home, menu);

        mMenuBinding =  DataBindingUtil.bind(menu.findItem(R.id.home_start).getActionView());
        User user = getViewModel().getRealm().where(User.class).findFirst();
        mMenuBinding.setUser(user);
        mMenuBinding.vHomeMenuLayout.setOnClickListener((v) -> onProfileClick());
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getBaseActivity()).showCSFab(true);

        setSupportToolBar(getViewDataBinding().fHomeToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.home_logo);
        }

        initText();
        initSearchCategory();
        initBannerAdapter();
        initCSCallback();

        Bundle bundle = getArguments();
        long fakeId = 0;
        if(bundle!=null){
            fakeId = bundle.getLong("fakeId");
        }

        if(fakeId > 0){
            ((MainActivity)getBaseActivity()).loadFacilityDetailBaseFromServer(fakeId);
        }
        else{
            checkEventPopup();
        }

        //setTotalFacilityCount();
        //getViewModel().clearFacilityRealm();
        //getViewModel().loadFacilitySearchMainFromServer();
    }

    private void initText(){
        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.v_home_top_bottom_text,
                getViewDataBinding().fHomeTopLayout.vHomeTopMainTextBottom);

        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.v_home_data_source_top_text,
                getViewDataBinding().fHomeDataSourceLayout.vHomeDataSourceTopText);

        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.v_home_cs_top_text,
                getViewDataBinding().fHomeCsLayout.vHomeCsTopText);

        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.v_home_cs_talk_top_text,
                getViewDataBinding().fHomeCsLayout.vHomeCsBottomLeft.vHomeCsBottomTopText);

        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.v_home_cs_call_top_text,
                getViewDataBinding().fHomeCsLayout.vHomeCsBottomRight.vHomeCsBottomTopText);
    }

    /*private void initServiceAdapter(){
        RealmResults<StaticService> staticServices = getViewModel().getRealm().where(StaticService.class).findAll();
        ServiceHorizontalAdapter adapter = new ServiceHorizontalAdapter(staticServices, this);
        getViewDataBinding().fHomeServiceLayout.vHomeServiceRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fHomeServiceLayout.vHomeServiceRecyclerView.setLayoutManager(layoutManager);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_service_horizontal_item_space);
        int spacingFirstLast = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_service_horizontal_first_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, spacingInPixels, true);
        decoration.setSpaceFirstAndLastHorizontal(spacingFirstLast);
        getViewDataBinding().fHomeServiceLayout.vHomeServiceRecyclerView.addItemDecoration(decoration);
    }*/

    private void initSearchCategory(){
        RealmResults<StaticCategory> staticCategories = RealmDataAccessor.getInstance().getStaticCategoryExcludeAll();
        SearchCategoryHorizontalAdapter adapter = new SearchCategoryHorizontalAdapter(staticCategories, this);
        getViewDataBinding().fHomeServiceLayout.vHomeServiceRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fHomeServiceLayout.vHomeServiceRecyclerView.setLayoutManager(layoutManager);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_service_horizontal_item_space);
        int spacingFirstLast = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_service_horizontal_first_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, spacingInPixels, true);
        decoration.setSpaceFirstAndLastHorizontal(spacingFirstLast);
        getViewDataBinding().fHomeServiceLayout.vHomeServiceRecyclerView.addItemDecoration(decoration);
    }

    private void initBannerAdapter(){
        List<String> argsList = new ArrayList<>();
        int count = getViewModel().getDataManager().getTotalFacilityCount();
        argsList.add(UnitUtils.thousandComma(count));
        argsList.add("60만");
        argsList.add("98.8");
        HomeBannerAdapter adapter = new HomeBannerAdapter(getBaseActivity(), argsList);
        getViewDataBinding().fHomeBannerLayout.vHomeBannerRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fHomeBannerLayout.vHomeBannerRecyclerView.setLayoutManager(layoutManager);

        SnapHelper snapHelperCenter = new StartPagerSnapHelper(0);
        snapHelperCenter.attachToRecyclerView(getViewDataBinding().fHomeBannerLayout.vHomeBannerRecyclerView);

        getViewDataBinding().fHomeBannerLayout.vHomeBannerRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int snapPosition = RecyclerView.NO_POSITION;
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if(layoutManager!=null){
                    View snapView = snapHelperCenter.findSnapView(layoutManager);
                    if(snapView!=null){
                        snapPosition = layoutManager.getPosition(snapView);
                    }
                }

                mPosition = snapPosition;
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        mSmoothScroller = new LinearSmoothScroller(getBaseActivity()) {
            @Override protected int getHorizontalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        initBannerTimer();
    }

    private void initBannerTimer(){
        // 타이머 세팅
        mTimer = new Timer();
        mTimer.schedule(new BannerTimer(), 3000, 3000);
    }

    @Override
    public void onClick(StaticCategory category) {
        AmplitudeManager.getInstance().logEvent(HOME_CLICK_FACILITY_CATEGORY, AmplitudeParam.Key.VALUE, category.getText());

        Bundle bundle = new Bundle();
        bundle.putParcelable(SEARCH_CATEGORY_KEY, category);

        ((MainActivity)getBaseActivity()).onNavigationItemSelected(SearchFragment.class.getName(), bundle);
    }

    class BannerTimer extends TimerTask {
        @Override
        public void run() {
            int position = mPosition + 1;
            if(position >= 3){
                position = 0;
            }
            moveToScrollPosition(position);
        }
    }

    private void moveToScrollPosition(int position){
        //getViewDataBinding().fSearchMapRecyclerView.smoothScrollToPosition(i);
        RecyclerView.LayoutManager manager = getViewDataBinding().fHomeBannerLayout.vHomeBannerRecyclerView.getLayoutManager();
        if(mSmoothScroller!= null && manager!=null){
            mSmoothScroller.setTargetPosition(position);
            manager.startSmoothScroll(mSmoothScroller);
        }
    }

    private void initCSCallback(){
        MainActivity activity = (MainActivity)getBaseActivity();
        getViewDataBinding().fHomeCsLayout.vHomeCsBottomLeft.setOnClickListener(activity::onFabKakao);
        getViewDataBinding().fHomeCsLayout.vHomeCsBottomRight.setOnClickListener(activity::onFabCall);
    }

    private void checkEventPopup(){
        if(!getViewModel().getDataManager().isShowEventPopup() && FirebaseManager.getInstance().isNeedShowEventPopup()){
            getViewModel().getDataManager().setShowEventPopup(true);
            showEventPopup();
        }
    }

    private void showEventPopup(){
        EventDialog dialog = EventDialog.newInstance();
        dialog.show(getFragmentManager(), null);
    }

    public void setTotalFacilityCount(){
        int count = getViewModel().getDataManager().getTotalFacilityCount();

        int minCount = 0;
        if(count > 1000){
            minCount = 10000;
        }
        DecimalFormat decimalFormat = UnitUtils.getThousandCommnaFormat();

        ValueAnimator animator = ValueAnimator.ofInt(minCount, count);
        animator.setDuration(1500);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                String countText = decimalFormat.format(animation.getAnimatedValue());

                if(getBaseActivity() == null){
                    getViewDataBinding().fHomeTopLayout.vHomeTopMainTextMiddle.setText(
                            getString(R.string.f_home_main_text_middle_1, countText));
                }
                else{
                    UIUtils.setTextIncludeAnnotation(
                            getBaseActivity(),
                            R.string.f_home_main_text_middle_1,
                            getViewDataBinding().fHomeTopLayout.vHomeTopMainTextMiddle,
                            countText);
                }
            }
        });
        animator.start();
    }

    @Override
    public void onCareCoordiButtonClick() {
        if(getBaseActivity() instanceof MainActivity){
            ((MainActivity)getBaseActivity()).openCareCoordiActivity();
        }
    }

    @Override
    public void onRequestEstimate() {

    }

    @Override
    public void onSearch() {
        AmplitudeManager.getInstance().logEvent(HOME_CLICK_SEARCH_KEYWORD);
        SearchDialog dialog = SearchDialog.getInstance();
        dialog.setOnSearchListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), "");
    }

    @Override
    public void doBack() {
        if(((MainActivity)getBaseActivity()).isFabOpen()){
            ((MainActivity)getBaseActivity()).setFabOpen(false);
            return;
        }

        BackStackManager.getInstance().onBackPressed();
    }

    public void openAccountMainActivity() {
        Intent intent = getBaseActivity().newIntent(AccountMainActivity.class);
        intent.putExtra("logout", false);
        startActivity(intent);
    }

    public void onProfileClick(){
        if(getViewModel().getDataManager().isLoggedIn()){
            AmplitudeManager.getInstance().logEvent(HOME_CLICK_PROFILE);
            ((MainActivity)getBaseActivity()).onNavigationItemSelected(ProfileFragment.class.getName(), null);
        }
        else {
            AmplitudeManager.getInstance().logEvent(HOME_CLICK_AUTH);
            openAccountMainActivity();
        }
    }

    @Override
    public void onAdded() {
        super.onAdded();
    }

    @Override
    public void onShow() {
        super.onShow();

        ((MainActivity)getBaseActivity()).showCSFab(true);

        setSupportToolBar(getViewDataBinding().fHomeToolbar);

        initBannerTimer();

        ((MainActivity)getBaseActivity()).getViewDataBinding().aMainFab.setVisible(true);
        UIUtils.setStatusBarColor(getBaseActivity(), getView(), Color.WHITE, true);
    }

    @Override
    public void onHide(){
        if(mTimer != null){
            mTimer.cancel();
        }

        ((MainActivity)getBaseActivity()).showCSFab(false);
    }

    @Override
    public void onSearch(Suggestion suggestion) {
        if(suggestion == null || getBaseActivity() == null){
            return;
        }

        ((MainActivity)getBaseActivity()).onSearch(suggestion);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mTimer != null){
            mTimer.cancel();
        }
    }
}
