package com.onestepmore.caredoc.ui.account.terms;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.FTermsBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.register.email.EmailRegisterFragment;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.profile.webview.WebViewActivity;

import javax.inject.Inject;
import javax.inject.Named;

public class TermsFragment  extends BaseFragment<FTermsBinding, TermsViewModel> implements
        TermsNavigator
{
    public static TermsFragment newInstance(boolean isEmail){
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEmail", isEmail);
        TermsFragment fragment = new TermsFragment();
        fragment.setArguments(bundle);

        return  fragment;
    }

    @Inject
    @Named("TermsFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TermsViewModel mTermsViewModel;

    private boolean mAgreement = false;
    private boolean mPrivacy = false;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_terms;
    }

    @Override
    public TermsViewModel getViewModel() {
        mTermsViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TermsViewModel.class);
        return mTermsViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTermsViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TermsAdapter adapter = new TermsAdapter();
        adapter.setCallback(this);
        getViewDataBinding().fTermsRecyclerView.setAdapter(adapter);
        getViewDataBinding().fTermsRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        adapter.addAll(getResources().getStringArray(R.array.i_terms_text_list));
    }

    @Override
    public void onTermsCheck(int position, boolean checked) {
        if(position == 0){
            mAgreement = checked;
        }
        else if(position == 1){
            mPrivacy = checked;
        }
    }

    @Override
    public void onTermsClick(int position) {
        String text = getResources().getStringArray(R.array.i_terms_text_list)[position];
        Intent intent = getBaseActivity().newIntent(WebViewActivity.class);
        intent.putExtra("title", text);
        String url = BuildConfig.BASE_URL + "/m/terms/member?webview=1";
        if(position == 0){
        }
        else{
            url = BuildConfig.BASE_URL + "/m/terms/privacy?webview=1";
        }
        intent.putExtra("url", url);
        getBaseActivity().startActivity(intent);
    }

    @Override
    public void onAgree() {
        if(mAgreement && mPrivacy){
            if(getArguments() != null){
                if(getArguments().getBoolean("isEmail")){
                    BackStackManager.getInstance().onBackPressed();
                    BackStackManager.getInstance().addChildFragment(new EmailRegisterFragment(), R.id.v_app_bar_start_fragment_layout);
                }
                else{
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("agree", true);
                    BackStackManager.getInstance().getCurrentBackStack().getRootFragment().setArguments(bundle);
                    BackStackManager.getInstance().onBackPressed();
                }
            }
            else{
                BackStackManager.getInstance().onBackPressed();
            }
        }
        else{
            showCommonDialog(getString(R.string.warning_msg_terms_title), getString(R.string.warning_msg_terms_desc));
        }
    }
}
