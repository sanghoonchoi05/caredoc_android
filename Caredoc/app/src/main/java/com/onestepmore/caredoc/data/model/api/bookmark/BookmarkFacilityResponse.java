package com.onestepmore.caredoc.data.model.api.bookmark;

import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.object.BookmarkObject;

public final class BookmarkFacilityResponse extends ItemResponse<BookmarkObject> {
}
