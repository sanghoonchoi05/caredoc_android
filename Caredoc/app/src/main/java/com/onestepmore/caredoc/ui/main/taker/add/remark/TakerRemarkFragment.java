package com.onestepmore.caredoc.ui.main.taker.add.remark;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTypes;
import com.onestepmore.caredoc.databinding.FTakerRemarkBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.BaseAddTakerFragment;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialogClickListener;
import com.onestepmore.caredoc.utils.CommonUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;

public class TakerRemarkFragment extends BaseAddTakerFragment<FTakerRemarkBinding, TakerRemarkViewModel> implements
        TakerRemarkNavigator,
        BottomSheetDialogClickListener
{
    public static TakerRemarkFragment newInstance(int titleResId, boolean isModify){
        TakerRemarkFragment fragment = new TakerRemarkFragment();
        fragment.setArguments(getBundle(titleResId, isModify));

        return  fragment;
    }

    @Inject
    @Named("TakerRemarkFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerRemarkViewModel mTakerRemarkViewModel;
    private AddTakerRequest.Remarks mCachedRemarks;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_remark;
    }

    @Override
    public TakerRemarkViewModel getViewModel() {
        mTakerRemarkViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerRemarkViewModel.class);
        return mTakerRemarkViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerRemarkViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            setModify(getArguments().getBoolean("isModify"));
        }

        mCachedRemarks = ((AddTakerActivity)getBaseActivity()).getCachedRemarks();
        if(isModify() && mCachedRemarks != null){
            getViewDataBinding().setRemark(mCachedRemarks);
        }

        initTitle(getViewDataBinding().fTakerRemarkTitle);
        setTextObserver();
    }

    private void setTextObserver(){
        rx.Observable<CharSequence> favoriteObserver = RxTextView.textChanges(getViewDataBinding().fTakerRemarkHabitLayout.vCommonTextLayout3FirstText);
        getViewModel().addSubscription(favoriteObserver.skip(1).subscribe((text) -> mCachedRemarks.setHabit(text.toString())));
    }

    @Override
    public void onClick(String tag) {
        String category = getCategory(tag);
        if(CommonUtils.checkNullAndEmpty(category)){
            return;
        }

        RealmResults<StaticTypes> staticTypes = getViewModel().getRealm()
                .where(StaticTypes.class)
                .equalTo("category", category)
                .findAll();

        if(staticTypes != null){
            String [] textList = new String[staticTypes.size()];
            for(int i=0; i<staticTypes.size(); i++){
                textList[i] = staticTypes.get(i).getTypeName();
            }
            showBottomSheetDialog(textList, tag);
        }
    }

    private String getCategory(String tag){
        if(tag.equals(getString(R.string.f_taker_remark_tendency))){
            return StaticTypes.TYPE_ENUM.TENDENCY.toString();
        }
        else if(tag.equals(getString(R.string.f_taker_remark_speak))){
            return StaticTypes.TYPE_ENUM.COMMON_LEVEL.toString();
        }

        return null;
    }

    public void showBottomSheetDialog(String [] textList, String tag){
        BottomSheetDialog dialog = BottomSheetDialog.getInstance(textList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), tag);
    }

    @Override
    public void onClick(String tag, String text) {
        AddTakerActivity addTakerActivity = (AddTakerActivity)getBaseActivity();
        if(addTakerActivity == null){
            return;
        }

        if(tag.equals(getString(R.string.f_taker_remark_tendency))){
            mCachedRemarks.setTendency(addTakerActivity.getTypeCode(text, StaticTypes.TYPE_ENUM.TENDENCY));
            mCachedRemarks.setTendencyStr(text);
        }
        else if(tag.equals(getString(R.string.f_taker_remark_speak))){
            mCachedRemarks.setSpeak(addTakerActivity.getTypeCode(text, StaticTypes.TYPE_ENUM.COMMON_LEVEL));
            mCachedRemarks.setSpeakStr(text);
        }

        getViewDataBinding().setRemark(mCachedRemarks);
    }

    @Override
    public void onClose(String tag) {

    }

    @Override
    public void onInit(String tag) {
        if(tag.equals(getString(R.string.f_taker_remark_tendency))){
            mCachedRemarks.setTendency(null);
            mCachedRemarks.setTendencyStr(null);
        }
        else if(tag.equals(getString(R.string.f_taker_remark_speak))){
            mCachedRemarks.setSpeak(null);
            mCachedRemarks.setSpeakStr(null);
        }

        getViewDataBinding().setRemark(mCachedRemarks);
    }
}
