package com.onestepmore.caredoc.ui.main.favorite.detail;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class FavoriteDetailViewModel extends BaseViewModel<FavoriteDetailNavigator> {
    public FavoriteDetailViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }
}
