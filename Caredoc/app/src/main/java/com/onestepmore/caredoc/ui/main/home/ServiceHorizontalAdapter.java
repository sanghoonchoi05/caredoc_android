package com.onestepmore.caredoc.ui.main.home;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.databinding.IServiceHorizontalBinding;
import com.onestepmore.caredoc.ui.carecoordi.service.ServiceClickCallback;
import com.onestepmore.caredoc.ui.carecoordi.service.ServiceSelectListener;
import com.onestepmore.caredoc.utils.CommonUtils;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class ServiceHorizontalAdapter extends RealmRecyclerViewAdapter<StaticService, ServiceHorizontalAdapter.ServiceHorizontalViewHolder>
{
    @Nullable
    private final ServiceClickCallback mListener;

    public ServiceHorizontalAdapter(@Nullable OrderedRealmCollection<StaticService> data, @Nullable final ServiceClickCallback listener){
        super(data, true);
        mListener = listener;
    }

    @NonNull
    @Override
    public ServiceHorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IServiceHorizontalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_service_horizontal, viewGroup, false);
        return new ServiceHorizontalViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceHorizontalViewHolder bottomSheetDialogViewHolder, int position) {
        OrderedRealmCollection data = getData();
        if (data != null) {
            final StaticService staticService = getData().get(position);
            bottomSheetDialogViewHolder.binding.setIconResourceId(CommonUtils.getServiceIconResourceId(staticService));
            bottomSheetDialogViewHolder.binding.setService(staticService);
            bottomSheetDialogViewHolder.binding.executePendingBindings();
        }
    }

    class ServiceHorizontalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final IServiceHorizontalBinding binding;

        private ServiceHorizontalViewHolder(IServiceHorizontalBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.iServiceHorizontalCard.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mListener!=null){
                mListener.onClick(binding.getService());
            }
        }
    }
}
