package com.onestepmore.caredoc.ui.splash;

import android.app.Application;
import android.databinding.ObservableField;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.facebook.AccessToken;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.onestepmore.caredoc.AppConstants;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.CommonResponse_Deprecated;
import com.onestepmore.caredoc.data.model.api.account.LoginRequest;
import com.onestepmore.caredoc.data.model.api.account.LoginResponse;
import com.onestepmore.caredoc.data.model.api.object.ToolObject;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticLtiGrades;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTool;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTypes;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.firebase.FirebaseParam;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.ui.support.rx.SupportRxObserver;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Response;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SPLASH_COMPLETE_AUTO_LOGIN;

public class SplashViewModel extends BaseViewModel<SplashNavigator> {
    private final Gson mGson;
    private ObservableField<String> versionName = new ObservableField<>();
    @Deprecated
    private ObservableField<Boolean> completeCommonData = new ObservableField<>(false);
    @Deprecated
    private ObservableField<Boolean> completeToolData = new ObservableField<>(false);
    private ObservableField<Boolean> completeAddressData = new ObservableField<>(false);
    private ObservableField<Boolean> completeSubjectData = new ObservableField<>(false);
    private ObservableField<Boolean> completeDiseaseData = new ObservableField<>(false);
    private ObservableField<Boolean> completeStaticData = new ObservableField<>(false);

    private ObservableField<Boolean> completeUserData = new ObservableField<>(false);
    private ObservableField<Boolean> completeTakerData = new ObservableField<>(false);

    public ObservableField<String> getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName.set(versionName);
    }

    public SplashViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider, Gson gson) {
        super(application, appDataSource, schedulerProvider);

        mGson = gson;

        setObserver();
    }

    private void setObserver(){
        rx.Observable<Boolean> addressData = SupportRxObserver.observableFieldBooleanChanges(completeAddressData);
        rx.Observable<Boolean> subjectData = SupportRxObserver.observableFieldBooleanChanges(completeSubjectData);
        rx.Observable<Boolean> diseaseData = SupportRxObserver.observableFieldBooleanChanges(completeDiseaseData);
        rx.Observable<Boolean> staticData = SupportRxObserver.observableFieldBooleanChanges(completeStaticData);

        rx.Observable<Boolean> isFirstComplete = rx.Observable.combineLatest(addressData, subjectData,
                (_addressData, _subjectData) -> _addressData && _subjectData);

        rx.Observable<Boolean> isSecondComplete = rx.Observable.combineLatest(diseaseData, staticData,
                (_diseaseData, _staticData) -> _diseaseData && _staticData);

        rx.Observable<Boolean> isComplete = rx.Observable.combineLatest(isFirstComplete, isSecondComplete,
                (_isFirstComplete, _isSecondComplete) -> _isFirstComplete && _isSecondComplete);

        addSubscription(isComplete.subscribe(this::onComplete));

        rx.Observable<Boolean> userData = SupportRxObserver.observableFieldBooleanChanges(completeUserData);
        addSubscription(userData.subscribe(this::onLoginComplete));
        /*rx.Observable<Boolean> takerData = SupportRxObserver.observableFieldBooleanChanges(completeTakerData);

        rx.Observable<Boolean> isLoginDataComplete = rx.Observable.combineLatest(userData, takerData,
                (_userData, _takerData) -> _userData && _takerData);

        addSubscription(isLoginDataComplete.subscribe(this::onLoginComplete));*/
    }

    public void onComplete(Boolean aBoolean){
        if(aBoolean){
            checkUserHistory();
        }
    }

    public void onLoginComplete(Boolean aBoolean){
        if(aBoolean){
            getNavigator().loadComplete();
        }
    }

    public void saveStaticsData(String categoryAllStr){
        try{
            Type listType = new TypeToken<List<StaticLtiGrades>>(){}.getType();
            List<StaticLtiGrades> ltiGrades = mGson
                    .fromJson(CommonUtils.loadJSONFromAsset(getApplication(), AppConstants.STATICS_LTI_GRADES), listType);

            StaticCategory allCategory = new StaticCategory();
            allCategory.setId(0);
            allCategory.setText(categoryAllStr);
            allCategory.setGrades(null);
            allCategory.setQueryServiceTypeIds(null);

            getRealm().executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(ltiGrades);
                    realm.copyToRealmOrUpdate(allCategory);
                }
            });
        }catch (IOException e){

        }
    }

    public void loadStaticDataFromServer() {
        getCompositeDisposable().add(getDataManager()
                .loadStaticData()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    if(aBoolean){
                        completeStaticData.set(true);
                    } }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void loadAddressDataFromServer(String addressName, String addressFullName) {
        getCompositeDisposable().add(getDataManager()
                .loadStaticAddress(addressName, addressFullName)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    if(aBoolean){
                        completeAddressData.set(true);
                    } }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void loadDiseaseDataFromServer(){
        getCompositeDisposable().add(getDataManager().loadStaticDisease()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    if(aBoolean){
                        completeDiseaseData.set(true);
                    } }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void loadToolDataFromServer(){
        getCompositeDisposable().add(getDataManager().doServerToolApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(this::saveToolData, throwable -> {
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private void saveToolData(JSONArray jsonArray){
        List<StaticTool> toolList = new ArrayList<>();
        for(int i=0; i<jsonArray.length(); i++){
            try{
                JSONObject object = jsonArray.getJSONObject(i);
                ToolObject toolObject = mGson.fromJson(object.toString(), ToolObject.class);
                StaticTool tool = new StaticTool();
                tool.setStaticTool(toolObject);
                toolList.add(tool);
            }catch (JSONException e){

            }
        }

        getRealm().executeTransaction(realm -> {
            for (StaticTool tool : toolList) {
                realm.copyToRealmOrUpdate(tool);
            }

            completeToolData.set(true);
        });
    }


    public void loadSubjectDataFromServer() {
        completeSubjectData.set(true);

        /*getCompositeDisposable().add(getDataManager().loadStaticSubject()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    if(aBoolean){
                        completeSubjectData.set(true);
                    } }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));*/
    }

    private void saveCommonData(CommonResponse_Deprecated response) {

        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                for(StaticTypes staticTypes : response.getStaticTypeList()){
                    realm.copyToRealmOrUpdate(staticTypes);
                }

                completeCommonData.set(true);
            }
        });
    }

    public void checkUserHistory() {
        User user = getRealm().where(User.class).findFirst();
        if(user != null && !user.getToken().isEmpty()){
            login(user);
        }else{
            getNavigator().loadComplete();
        }
    }

    private void login(User userInfo) {
        if (userInfo != null) {
            Flowable<LoginResponse> apiCall = getServerLoginApiCall(userInfo);
            if(userInfo.provider != null){
                if(userInfo.provider.equals("facebook") && AccessToken.isCurrentAccessTokenActive()) {
                    apiCall = getSocialLoginApiCall(userInfo);
                }
                else if(userInfo.provider.equals("naver")){
                    apiCall = getSocialLoginApiCall(userInfo);
                }
            }

            if(apiCall == null){
                return;
            }

            getCompositeDisposable().add(apiCall
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribe(response -> {
                        getDataManager().updateAccessToken(response.getToken(), response.getExpiresIn(), response.getTokenType());
                        getUserInfo(userInfo);

                        AmplitudeManager.getInstance().logEvent(SPLASH_COMPLETE_AUTO_LOGIN);
                        FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.LOGIN_AUTO_COMPLETE);
                        FirebaseManager.getInstance().logEvent(
                                FirebaseEvent.PredefinedEvent.LOGIN_COMPLETE,
                                new FirebaseManager.Builder().addParam(FirebaseParam.PredefinedKey.METHOD, FirebaseParam.Value.AUTO_COMPLETE).build());
                    }, throwable -> {
                        getDataManager().handleThrowable(throwable, getNavigator());
                        logout();
                    }));
        }
    }
    private Flowable<LoginResponse> getSocialLoginApiCall(User user){
        LoginRequest.SocialLoginPathParameter pathParameter = new LoginRequest.SocialLoginPathParameter();
        pathParameter.setProvider(user.provider);

        LoginRequest.SocialInfoRequest socialInfoRequest = new LoginRequest.SocialInfoRequest();
        //socialInfoRequest.setAvatar(user.fullThumbnailUrl);
        socialInfoRequest.setId(user.socialId);
        socialInfoRequest.setToken(user.socialToken);
        socialInfoRequest.setEmail(user.getEmail());
        socialInfoRequest.setName(user.getName());

        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(mGson.toJson(socialInfoRequest));

        }catch (JSONException e){
            return null;
        }

        return getDataManager().doSocialLoginApiCall(pathParameter, jsonObject);
    }


    private void getUserInfo(User user){
        getCompositeDisposable().add(getDataManager()
                .loadUserInfoFromServer(getRealm(), user.getEmail(), user.getPassword(), user.getProvider(), user.getSocialId(), user.getSocialToken())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    if(response){
                        completeUserData.set(true);
                    }

                    if(getDataManager().isLoggedIn()){
                        AmplitudeManager.getInstance().setUserProperty(user);
                    }

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private Flowable<LoginResponse> getServerLoginApiCall(User user) {
        return getDataManager().doServerLoginApiCall(
                new LoginRequest.ServerLoginRequest(user.getEmail(), user.getPassword()));
    }

    public void logout(){
        setIsLoading(true);
        getDataManager().doServerLogoutApiCall(new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    removeAccessToken();
                    deleteAllUser();
                    getDataManager().deleteAllFacility(getRealm());
                    getDataManager().deleteAllTaker(getRealm());
                    FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.TAB_PROFILE_LOGOUT_COMPLETE);
                    getDataManager().setLoggedInMode(AppDataSource.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);
                    getNavigator().onLogout();
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }

    private void removeAccessToken(){
        User user = getRealm().where(User.class).findFirst();
        if(user != null){
            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    user.setToken("");
                    user.setTokenType("");
                    user.setExpiresIn(0);
                    realm.copyToRealmOrUpdate(user);
                }
            });
        }

        getDataManager().updateAccessToken("", 0, "");
    }

    private void deleteAllUser(){
        RealmResults<User> realmResults = getRealm().where(User.class).findAll();
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
            }
        });
    }
}
