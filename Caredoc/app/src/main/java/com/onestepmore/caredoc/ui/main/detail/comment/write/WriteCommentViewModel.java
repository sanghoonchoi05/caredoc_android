package com.onestepmore.caredoc.ui.main.detail.comment.write;

import android.app.Application;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.CommentRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.CommentSaveRequest;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import okhttp3.Response;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.WRITE_COMMENT_COMPLETE;

public class WriteCommentViewModel extends BaseViewModel<WriteCommentNavigator> {
    private ReviewRepository mReviewRepository;

    public ReviewRepository getReviewRepository() {
        return mReviewRepository;
    }

    public WriteCommentViewModel(Application application, AppDataSource appDataSource,
                                 SchedulerProvider schedulerProvider, ReviewRepository reviewRepository) {
        super(application, appDataSource, schedulerProvider);

        mReviewRepository = reviewRepository;
    }

    public void onRegister(){
        getNavigator().onRegisterComment();
    }

    public void registerComment(final long id, final int rating, final String content){
        CommentSaveRequest commentSaveRequest = new CommentSaveRequest();
        commentSaveRequest.setRating(rating * 2);
        commentSaveRequest.setContent(content);

        CommentSaveRequest.PathParameter pathParameter = new CommentSaveRequest.PathParameter();
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility.getType());
        pathParameter.setMorphId(id);

        getReviewRepository().saveCommentApiCall(commentSaveRequest, pathParameter, new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    getNavigator().onCompleteRegisterComment();
                    AmplitudeManager.getInstance().logEvent(WRITE_COMMENT_COMPLETE);
                    FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.WRITE_REVIEW_COMPLETE);
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }

    public void removeComment(long id){
        CommentRemoveRequest.PathParameter pathParameter = new CommentRemoveRequest.PathParameter();
        pathParameter.setId(id);

        getReviewRepository().removeCommentApiCall(pathParameter, new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    getNavigator().onCompleteRemoveComment();
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }
}
