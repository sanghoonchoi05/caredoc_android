package com.onestepmore.caredoc.ui.main.search;

public interface CategoryTabCallback {
    void onSelect(int position);
}
