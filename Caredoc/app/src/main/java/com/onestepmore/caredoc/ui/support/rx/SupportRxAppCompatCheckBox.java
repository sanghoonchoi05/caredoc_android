package com.onestepmore.caredoc.ui.support.rx;

import android.support.v7.widget.AppCompatCheckBox;

import com.jakewharton.rxbinding.internal.MainThreadSubscription;

import rx.Observable;
import rx.Subscriber;

import static com.jakewharton.rxbinding.internal.Preconditions.checkUiThread;

public class SupportRxAppCompatCheckBox implements Observable.OnSubscribe<Boolean> {
    private final AppCompatCheckBox view;

    public SupportRxAppCompatCheckBox(AppCompatCheckBox view) {
        this.view = view;
    }
    @Override
    public void call(Subscriber<? super Boolean> subscriber) {
        checkUiThread();

        view.setOnCheckedChangeListener((box, isChecked) -> {
            if (!subscriber.isUnsubscribed()) {
                subscriber.onNext(isChecked);
            }
        });

        subscriber.add(new MainThreadSubscription() {
            @Override protected void onUnsubscribe() {
                view.setOnCheckedChangeListener(null);
            }
        });

        // Emit initial value.
        subscriber.onNext(view.isChecked());
    }
}
