package com.onestepmore.caredoc.ui.widgets.progress;

public interface ProgressNavigator {
    void onNextButtonClick();
    void onPrevButtonClick();
    void onModifyButtonClick();
    void onExit();
}
