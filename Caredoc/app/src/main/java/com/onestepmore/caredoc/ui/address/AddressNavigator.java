package com.onestepmore.caredoc.ui.address;

import com.onestepmore.caredoc.data.model.realm.RecentlyJuso;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface AddressNavigator extends BaseNavigator {
    void onBack();
    void onSearch();
    void onDelete();
    void onItemDelete(RecentlyJuso juso);
    void onItemClick(RecentlyJuso juso);
}
