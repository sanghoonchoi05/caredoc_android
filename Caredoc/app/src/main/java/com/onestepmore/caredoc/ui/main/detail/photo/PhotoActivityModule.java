package com.onestepmore.caredoc.ui.main.detail.photo;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class PhotoActivityModule {
    @Provides
    PhotoViewModel photoViewModel(Application application, AppDataSource dataSource,
                                               SchedulerProvider schedulerProvider) {
        return new PhotoViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory providePhotoViewModelProvider(PhotoViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
