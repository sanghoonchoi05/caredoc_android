package com.onestepmore.caredoc.ui.main.search.map;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface SearchMapNavigator extends BaseNavigator {
    void onList();
    void onMyLocation();
}
