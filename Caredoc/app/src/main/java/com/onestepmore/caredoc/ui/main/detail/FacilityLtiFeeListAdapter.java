package com.onestepmore.caredoc.ui.main.detail;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.object.LtiFeeSheetsObject;
import com.onestepmore.caredoc.data.model.api.object.UnpaidItemsObject;
import com.onestepmore.caredoc.databinding.IFacilityDetailLtiFeeBinding;

import java.util.List;

public class FacilityLtiFeeListAdapter extends RecyclerView.Adapter<FacilityLtiFeeListAdapter.FacilityLtiFeeListViewHolder> {
    public enum TYPE {
        Fee(0),
        Copayment(1),
        Unpaid(2),
        Total(3);

        private int position;

        TYPE(int position) {
            this.position = position;
        }

        public int getPosition() {
            return position;
        }
    }

    private LtiFeeSheetsObject.Item mLtiFeeSheetItem = null;
    private int mUnpaidPrice;
    private String[] mItemNames;

    public FacilityLtiFeeListAdapter(@NonNull LtiFeeSheetsObject.Item item,
                                     List<UnpaidItemsObject.Item> items,
                                     @NonNull String[] names) {
        this.mLtiFeeSheetItem = item;
        this.mItemNames = names;
        this.mUnpaidPrice = getUnpaidPrice(items);
    }

    public void changeSheets(LtiFeeSheetsObject.Item item){
        this.mLtiFeeSheetItem = item;
        notifyDataSetChanged();
    }

    private int getUnpaidPrice(List<UnpaidItemsObject.Item> items) {
        int unpaid = 0;
        if(items != null){
            for (UnpaidItemsObject.Item item : items) {
                if (item.getCode().equals(UnpaidItemsObject.TYPE.UPI_1.getType()) ||
                        item.getCode().equals(UnpaidItemsObject.TYPE.UPI_5.getType())) {
                    unpaid += item.getAmount();
                }
            }
        }

        return unpaid;
    }

    @NonNull
    @Override
    public FacilityLtiFeeListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFacilityDetailLtiFeeBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_detail_lti_fee, viewGroup, false);
        return new FacilityLtiFeeListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FacilityLtiFeeListViewHolder facilityViewHolder, int position) {
        facilityViewHolder.binding.setIsTotal(false);
        if (position == TYPE.Fee.getPosition()) {
            facilityViewHolder.binding.setName(mItemNames[position]);
            facilityViewHolder.binding.setFee(mLtiFeeSheetItem.getFee());
        } else if (position == TYPE.Copayment.getPosition()) {
            facilityViewHolder.binding.setName(mItemNames[position]);
            facilityViewHolder.binding.setFee(mLtiFeeSheetItem.getCopayment());
        } else if (position == TYPE.Unpaid.getPosition()) {
            facilityViewHolder.binding.setName(mItemNames[position]);
            facilityViewHolder.binding.setFee(mUnpaidPrice);
        } else if (position == TYPE.Total.getPosition()) {
            facilityViewHolder.binding.setIsTotal(true);
            facilityViewHolder.binding.setName(mItemNames[position]);
            facilityViewHolder.binding.setFee(mUnpaidPrice + mLtiFeeSheetItem.getCopayment());
        }
        facilityViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return TYPE.values().length;
    }

    static class FacilityLtiFeeListViewHolder extends RecyclerView.ViewHolder {
        final IFacilityDetailLtiFeeBinding binding;

        private FacilityLtiFeeListViewHolder(IFacilityDetailLtiFeeBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}