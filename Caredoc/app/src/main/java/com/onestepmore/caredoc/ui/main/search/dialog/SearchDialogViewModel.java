package com.onestepmore.caredoc.ui.main.search.dialog;

import android.app.Application;
import android.view.View;

import com.onestepmore.caredoc.data.model.realm.Suggestion;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import io.realm.RealmResults;

public class SearchDialogViewModel extends BaseViewModel<SearchDialogCallback> {

    private RealmResults<Suggestion> mSuggestion;

    public SearchDialogViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public RealmResults<Suggestion> getSuggestion(){
        if(mSuggestion == null){
            mSuggestion = getRealm().where(Suggestion.class).findAll();
        }
        return mSuggestion;
    }

    public void loadSuggestionFromServer(String keyword){
        getCompositeDisposable().add(getDataManager()
                .loadSuggestion(keyword)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {},
                        throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void onClickBackButton(View view){
        getNavigator().onBack();
    }
    public void onClickSearchButton(View view){
        getNavigator().onSearch();
    }

    public void onClickDeleteButton(View view){
        getNavigator().onDelete();
    }

    @Override
    protected void onCleared() {
        unbindRealm();
        super.onCleared();
    }

    private void unbindRealm() {
        if(mSuggestion!=null){
            mSuggestion.removeAllChangeListeners();
        }

        getDataManager().clearSuggestion();
    }
}
