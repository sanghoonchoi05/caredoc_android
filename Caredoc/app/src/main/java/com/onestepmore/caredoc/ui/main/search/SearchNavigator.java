package com.onestepmore.caredoc.ui.main.search;

import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface SearchNavigator extends BaseNavigator {
    void onListLayoutClick();
    void onSearchViewClick();
    void onInitSearchKeyword();
}
