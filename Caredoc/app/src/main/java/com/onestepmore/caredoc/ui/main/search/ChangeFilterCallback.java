package com.onestepmore.caredoc.ui.main.search;

public interface ChangeFilterCallback {
    void onChangedFilter(boolean changedLocationFilter);
}
