package com.onestepmore.caredoc.ui.main.search.filter.address.list;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.databinding.FAddressListBinding;
import com.onestepmore.caredoc.databinding.ISearchAddressFilterBinding;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.main.search.filter.address.AddressFilterNavigator;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

public class AddressListFragment extends BaseFragment<FAddressListBinding, AddressListViewModel> implements
        AddressListNavigator,
        NestedScrollView.OnScrollChangeListener
{

    public static AddressListFragment newInstance(List<StaticAddress> staticAddressList, StaticAddress selectedAddress){
        AddressListFragment fragment = new AddressListFragment();
        fragment.setArguments(staticAddressList, selectedAddress);

        return fragment;
    }

    @Inject
    @Named("AddressListFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private AddressListViewModel mAddressListViewModel;

    private List<StaticAddress> mCachedStaticAddress;
    private StaticAddress mSelectedStaticAddress;

    private AddressFilterNavigator mAddressFilterNavigator;
    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_address_list;
    }

    @Override
    public AddressListViewModel getViewModel() {
        mAddressListViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AddressListViewModel.class);
        return mAddressListViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressListViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle == null){
            return;
        }

        mCachedStaticAddress = bundle.getParcelableArrayList("addressList");
        mSelectedStaticAddress = bundle.getParcelable("selectedAddress");
        if(mSelectedStaticAddress == null){
            mSelectedStaticAddress = mCachedStaticAddress.get(0);
        }

        initListAdapter();
    }

    public void setArguments(List<StaticAddress> staticAddressList, StaticAddress selectedAddress){
        Bundle bundle = new Bundle();
        List<StaticAddress> addressList = new ArrayList<>();
        addressList.addAll(staticAddressList);
        bundle.putParcelableArrayList("addressList", (ArrayList<? extends Parcelable>)addressList);
        bundle.putParcelable("selectedAddress", selectedAddress);

        setArguments(bundle);
    }

    public void setArguments(){
        Bundle bundle = new Bundle();
        List<StaticAddress> addressList = new ArrayList<>();
        addressList.addAll(mCachedStaticAddress);
        bundle.putParcelableArrayList("addressList", (ArrayList<? extends Parcelable>)addressList);
        bundle.putParcelable("selectedAddress", mSelectedStaticAddress);

        setArguments(bundle);
    }

    private void initListAdapter(){
        AddressListAdapter adapter = new AddressListAdapter(mCachedStaticAddress);
        adapter.setCallback(mAddressFilterNavigator);
        getViewDataBinding().fAddressListRecyclerView.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        getViewDataBinding().fAddressListRecyclerView.setLayoutManager(manager);

        int firstSpacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.f_address_list_first_space);
        int lastSpacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.f_address_list_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, 0, true);
        decoration.setSpanCount(2);
        decoration.setSpaceFirstVertical(firstSpacingInPixels);
        decoration.setSpaceLastVertical(lastSpacingInPixels);
        getViewDataBinding().fAddressListRecyclerView.addItemDecoration(decoration);

        getViewDataBinding().fAddressListScrollView.setOnScrollChangeListener(this);
    }
    public void setAddressListNavigator(AddressFilterNavigator navigator){
        mAddressFilterNavigator = navigator;
    }

    public void setAddress(List<StaticAddress> addressList){
        mCachedStaticAddress = addressList;

        // TODO 초기화 버튼 클릭 시
        if(getViewDataBinding() != null){
            AddressListAdapter adapter = (AddressListAdapter)getViewDataBinding().fAddressListRecyclerView.getAdapter();
            if(adapter != null){
                adapter.setStaticAddressList(mCachedStaticAddress);
            }
        }

        setSelectAddress(mCachedStaticAddress.get(0));
    }

    public void setSelectAddress(StaticAddress address) {
        mSelectedStaticAddress = address;

        // TODO 초기화 버튼 클릭 시
        if(getViewDataBinding() == null){
            return;
        }

        int childCount = getViewDataBinding().fAddressListRecyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = getViewDataBinding().fAddressListRecyclerView.getChildAt(i);
            AddressListAdapter.AddressListViewHolder viewHolder = (AddressListAdapter.AddressListViewHolder)getViewDataBinding().fAddressListRecyclerView.getChildViewHolder(view);
            if(viewHolder != null){
                if (viewHolder.binding.getAddress().getbCode().equals(address.getbCode())) {
                    view.setSelected(true);
                    setTextBold(viewHolder.binding.iSearchAddressFilterText, true);
                } else {
                    view.setSelected(false);
                    setTextBold(viewHolder.binding.iSearchAddressFilterText, false);
                }
            }
        }
    }

    public void setTextBold(AppCompatTextView textView, boolean selected){
        if(getBaseActivity() == null){
            return;
        }

        Typeface typeface = ResourcesCompat.getFont(getBaseActivity(), R.font.noto_sans_kr_demi_light_hestia);
        if(selected){
            typeface = ResourcesCompat.getFont(getBaseActivity(), R.font.noto_sans_kr_bold_hestia);
        }

        textView.setTypeface(typeface);
    }

    @Override
    public void onScrollChange(NestedScrollView nestedScrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        mAddressFilterNavigator.onScrollChange(nestedScrollView, scrollX, scrollY, oldScrollX, oldScrollY);
    }

    public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressListViewHolder> {
        private AddressFilterNavigator mCallback;
        private List<StaticAddress> mStaticAddressList;

        public AddressListAdapter(@NonNull List<StaticAddress> addressList) {
            mStaticAddressList = addressList;
            notifyDataSetChanged();
        }

        public void setStaticAddressList(List<StaticAddress> staticAddressList) {
            this.mStaticAddressList = staticAddressList;

            notifyDataSetChanged();
        }

        public void setCallback(AddressFilterNavigator callback) {
            this.mCallback = callback;
        }

        @NonNull
        @Override
        public AddressListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            ISearchAddressFilterBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_search_address_filter, viewGroup, false);
            return new AddressListViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull AddressListViewHolder addressListViewHolder, int position) {
            final StaticAddress address = mStaticAddressList.get(position);
            addressListViewHolder.binding.setAddress(address);
            addressListViewHolder.binding.setCallback(mCallback);
            addressListViewHolder.binding.executePendingBindings();
            boolean selected = checkSelected(address);
            addressListViewHolder.itemView.setSelected(selected);
            setTextBold(addressListViewHolder.binding.iSearchAddressFilterText, selected);
        }

        @Override
        public int getItemCount() {
            return mStaticAddressList.size();
        }

        private boolean checkSelected(StaticAddress address) {
            boolean selected = false;
            if (mSelectedStaticAddress != null) {
                if (address.getbCode().equals(mSelectedStaticAddress.getbCode())) {
                    selected = true;
                }
            }

            return selected;
        }

        class AddressListViewHolder extends RecyclerView.ViewHolder {
            final ISearchAddressFilterBinding binding;

            private AddressListViewHolder(ISearchAddressFilterBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}
