package com.onestepmore.caredoc.ui.main.favorite;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.BookmarkRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class FavoriteViewModel extends BaseViewModel<FavoriteNavigator> {
    private BookmarkRepository mBookmarkRepository;

    public FavoriteViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                             BookmarkRepository bookmarkRepository) {
        super(application, appDataSource, schedulerProvider);

        mBookmarkRepository = bookmarkRepository;
    }

    public void loadMyBookmarkFromServer(){
        getCompositeDisposable().add(mBookmarkRepository.getBookmarkApiCall()
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(response -> getDataManager().saveMyBookmark(getRealm(), response), throwable -> {
                            getDataManager().handleThrowable(throwable, getNavigator());
                        }));
    }

    public void onLoginButtonClick(){
        getNavigator().openLoginFragment();
    }

    public void onRegisterButtonClick(){
        getNavigator().openAccountMainActivity();
    }
}
