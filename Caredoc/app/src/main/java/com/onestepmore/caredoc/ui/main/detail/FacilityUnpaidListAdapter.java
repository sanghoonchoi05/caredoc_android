package com.onestepmore.caredoc.ui.main.detail;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.object.UnpaidItemsObject;
import com.onestepmore.caredoc.databinding.IFacilityDetailUnpaidBinding;

import java.util.List;

public class FacilityUnpaidListAdapter extends RecyclerView.Adapter<FacilityUnpaidListAdapter.FacilityUnpaidListViewHolder> {
    private List<UnpaidItemsObject.Item> mUnpaidItemList = null;

    public FacilityUnpaidListAdapter(@NonNull List<UnpaidItemsObject.Item> items) {
        this.mUnpaidItemList = items;
    }

    @NonNull
    @Override
    public FacilityUnpaidListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFacilityDetailUnpaidBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_detail_unpaid, viewGroup, false);
        return new FacilityUnpaidListViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FacilityUnpaidListViewHolder facilityViewHolder, int position) {
        UnpaidItemsObject.Item item = mUnpaidItemList.get(position);
        facilityViewHolder.binding.setItem(item);
        facilityViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mUnpaidItemList.size();
    }

    static class FacilityUnpaidListViewHolder extends RecyclerView.ViewHolder {
        final IFacilityDetailUnpaidBinding binding;

        private FacilityUnpaidListViewHolder(IFacilityDetailUnpaidBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}