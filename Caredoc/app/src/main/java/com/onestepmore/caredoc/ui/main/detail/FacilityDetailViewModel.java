package com.onestepmore.caredoc.ui.main.detail;

import android.app.Application;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.Gson;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkRemoveRequest;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkSaveRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.realm.BookmarkFacilityList;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.BookmarkRepository;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import okhttp3.Response;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_CALL;

public class FacilityDetailViewModel extends BaseViewModel<FacilityDetailNavigator> {
    private final Gson mGson;
    private FacilityRepository mFacilityRepository;
    private BookmarkRepository mBookmarkRepository;
    private ReviewRepository mReviewRepository;

    public FacilityRepository getFacilityRepository() {
        return mFacilityRepository;
    }

    public BookmarkRepository getBookmarkRepository() {
        return mBookmarkRepository;
    }

    public ReviewRepository getReviewRepository() {
        return mReviewRepository;
    }

    public Gson getGson() {
        return mGson;
    }

    public FacilityDetailViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                                   Gson gson, FacilityRepository facilityRepository, BookmarkRepository bookmarkRepository, ReviewRepository reviewRepository) {
        super(application, appDataSource, schedulerProvider);

        mFacilityRepository = facilityRepository;
        mBookmarkRepository = bookmarkRepository;
        mReviewRepository = reviewRepository;
        mGson = gson;
    }

    public void loadFacilityHospitalDetailFromServer(long id) {
        setIsLoading(true);
        FacilityDetailRequest.PathParameter pathParameter = new FacilityDetailRequest.PathParameter();
        pathParameter.setId(id);
        getCompositeDisposable().add(getFacilityRepository()
                .getFacilityHospitalDetailApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().onLoadHospitalDetail(response);
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void loadFacilityHomecareDetailFromServer(long id) {
        setIsLoading(true);
        FacilityDetailRequest.PathParameter pathParameter = new FacilityDetailRequest.PathParameter();
        pathParameter.setId(id);
        getCompositeDisposable().add(getFacilityRepository()
                .getFacilityHomecareDetailApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().onLoadHomeCareDetail(response);
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void onBack() {
        getNavigator().onClose();
    }

    public void onShowPhoneDial() {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_CALL);
        getNavigator().onShowPhoneDial();
    }

    public void onShowHomepage() {
        getNavigator().onShowHomepage();
    }

    public void onShare() {
        getNavigator().onShare();
    }

    public void onFavorite(FacilityRealmModel facility) {
        if (facility == null) {
            return;
        }

        BookmarkFacilityList bookmark = getRealm().where(BookmarkFacilityList.class).equalTo("fakeId", facility.getFakeId()).findFirst();
        if (bookmark != null) {
            onRemoveFacility(facility.getFakeId());
        } else {
            onSaveFacility(facility.getFakeId());
        }
    }

    void onSaveFacility(long id) {
        BookmarkSaveRequest.PathParameter pathParameter = new BookmarkSaveRequest.PathParameter();
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility.getType());
        pathParameter.setMorphId(id);

        getBookmarkRepository().saveBookmarkApiCall(pathParameter, responseListener);
    }

    void onRemoveFacility(long id) {
        BookmarkRemoveRequest.PathParameter pathParameter = new BookmarkRemoveRequest.PathParameter();
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility.getType());
        pathParameter.setMorphId(id);

        getBookmarkRepository().removeBookmarkApiCall(pathParameter, responseListener);
    }

    private OkHttpResponseListener responseListener = new OkHttpResponseListener() {
        @Override
        public void onResponse(Response response) {
            if (response.isSuccessful()) {
                getNavigator().onChangeBookmark();
                setIsLoading(false);
                loadMyBookmarkFromServer();
            } else {
                setIsLoading(false);
                if (response.body() != null) {
                    getDataManager().handleApiError(response, getNavigator());
                }
            }
        }

        @Override
        public void onError(ANError anError) {
            getDataManager().handleApiError(anError, getNavigator());
        }
    };

    private void loadMyBookmarkFromServer() {
        getCompositeDisposable().add(getBookmarkRepository().getBookmarkApiCall()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> getDataManager().saveMyBookmark(getRealm(), response), throwable -> {
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void onPrevPhoto(View view){
        getNavigator().onPrevPhoto();
    }

    public void onNextPhoto(View view){
        getNavigator().onNextPhoto();
    }
}
