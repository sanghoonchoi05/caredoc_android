package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public class AttachesObject extends ItemResponse<AttachesObject.Item> {

    public class Item{
        @Expose
        @SerializedName("id")
        private long id;

        @Expose
        @SerializedName("attach_type")
        private String attachType;

        @Expose
        @SerializedName("order")
        private int order;

        @Expose
        @SerializedName("url")
        private String url;

        @Expose
        @SerializedName("thumbnail_url")
        private String thumbnailUrl;

        public String getAttachType() {
            return attachType;
        }

        public long getId() {
            return id;
        }

        public int getOrder() {
            return order;
        }

        public String getThumbnailUrl() {
            return thumbnailUrl;
        }

        public String getUrl() {
            return url;
        }
    }
}
