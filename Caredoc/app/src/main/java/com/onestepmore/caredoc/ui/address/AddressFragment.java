package com.onestepmore.caredoc.ui.address;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.RecentlyJuso;
import com.onestepmore.caredoc.databinding.FAddressBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.address.detail.AddressDetailFragment;
import com.onestepmore.caredoc.ui.address.result.AddressResultFragment;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class AddressFragment extends BaseFragment<FAddressBinding, AddressViewModel> implements
        AddressNavigator
{
    @Inject
    @Named("AddressFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private AddressViewModel mAddressViewModel;
    private RealmResults<RecentlyJuso> mJusoResults;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_address;
    }

    @Override
    public AddressViewModel getViewModel() {
        mAddressViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AddressViewModel.class);
        return mAddressViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UIUtils.setTextIncludeAnnotation(getBaseActivity(), R.string.f_address_title, getViewDataBinding().fAddressMainTitle);

        setTextObserver();
        initListener();
        initAdapter();
    }

    private void initListener(){
        getViewDataBinding().fAddressEditLayout.vCommonAddressEditText.setOnEditorActionListener((textView, actionId, event) ->{
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                onSearch();
                return true;
            }
            return false;
        });
    }

    private void initAdapter(){
        mJusoResults = getViewModel().getRealm().where(RecentlyJuso.class).sort("date",Sort.DESCENDING).findAll();
        mJusoResults.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<RecentlyJuso>>() {
            @Override
            public void onChange(RealmResults<RecentlyJuso> results, OrderedCollectionChangeSet changeSet) {
                getViewDataBinding().setExistRecently(results.size() > 0);
            }
        });

        getViewDataBinding().setExistRecently(mJusoResults.size() > 0);

        RecentlyJusoAdapter adapter = new RecentlyJusoAdapter(mJusoResults, this);
        getViewDataBinding().fAddressMainRecentlyRecyclerView.setAdapter(adapter);
        getViewDataBinding().fAddressMainRecentlyRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(),new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_facility));
        dividerItemDecoration.setIncludeLastItem(true);
        getViewDataBinding().fAddressMainRecentlyRecyclerView.addItemDecoration(dividerItemDecoration);
        getViewDataBinding().fAddressMainRecentlyRecyclerView.setItemAnimator(null);
    }

    @Override
    public void onBack() {
        BackStackManager.getInstance().onBackPressed();
    }

    @Override
    public void onSearch() {
        String searchText = getViewDataBinding().getSearchText();

        if(CommonUtils.checkNullAndEmpty(searchText)){
            showCommonDialog(getString(R.string.warning_msg_address_search_title), getString(R.string.warning_msg_address_search_empty_desc));
            return;
        }

        getViewDataBinding().setSearchText("");
        InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(INPUT_METHOD_SERVICE);
        if(imm != null){
            imm.hideSoftInputFromWindow(getViewDataBinding().fAddressEditLayout.vCommonAddressEditText.getWindowToken(), 0);
        }

        BackStackManager.getInstance().addChildFragment(
                AddressResultFragment.newInstance(searchText),
                R.id.a_address_main_fragment_layout);
    }

    @Override
    public void onDelete() {
        getViewDataBinding().setSearchText("");
    }

    @Override
    public void onItemDelete(RecentlyJuso juso) {
        getViewModel().getRealm().executeTransaction((realm) -> RealmObject.deleteFromRealm(juso));
    }

    @Override
    public void onItemClick(RecentlyJuso juso) {
        if(getBaseActivity() instanceof AddressMainActivity){
            if(((AddressMainActivity)getBaseActivity()).isNeedDetail() &&
                    CommonUtils.checkNullAndEmpty(juso.getDetail())){
                BackStackManager.getInstance().addChildFragment(
                        AddressDetailFragment.newInstance(juso),
                        R.id.a_address_main_fragment_layout);
            }
            else{
                ((AddressMainActivity)getBaseActivity()).onSelectJuso(juso);
            }
        }
    }

    private void setTextObserver(){
        rx.Observable<CharSequence> searchObserver = RxTextView.textChanges(getViewDataBinding().fAddressEditLayout.vCommonAddressEditText);
        getViewModel().addSubscription(searchObserver.subscribe((text) -> getViewDataBinding().setSearchText(text.toString())));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mJusoResults!=null){
            mJusoResults.removeAllChangeListeners();
        }
    }
}
