package com.onestepmore.caredoc.ui.main.search;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchFragmentModule {
    @Provides
    SearchViewModel searchViewModel(Application application, AppDataSource dataSource, Gson gson, SchedulerProvider schedulerProvider
            , FacilityRepository facilityRepository) {
        return new SearchViewModel(application, dataSource, schedulerProvider, gson, facilityRepository);
    }

    @Provides
    @Named("SearchFragment")
    ViewModelProvider.Factory provideSearchViewModelProvider(SearchViewModel searchViewModel) {
        return new ViewModelProviderFactory<>(searchViewModel);
    }
}
