package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Deprecated
public final class ServiceVisitCareObject extends ServiceDetailObject{
    @Expose
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public final class Data{
        @Expose
        @SerializedName("key")
        private String key;

        @Expose
        @SerializedName("capacity")
        private int capacity;

        @Expose
        @SerializedName("unpaid_items")
        private UnpaidItemsObject unpaidItemsObject;

        @Expose
        @SerializedName("programs")
        private ProgramsObject programsObject;

        @Expose
        @SerializedName("welfare_tools")
        private WelfareToolsObject welfareToolsObject;

        public String getKey() {
            return key;
        }

        public int getCapacity() {
            return capacity;
        }

        public UnpaidItemsObject getUnpaidItemsObject() {
            return unpaidItemsObject;
        }

        public ProgramsObject getProgramsObject() {
            return programsObject;
        }

        public WelfareToolsObject getWelfareToolsObject() {
            return welfareToolsObject;
        }
    }
}
