package com.onestepmore.caredoc.ui.address;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.data.model.realm.RecentlyJuso;
import com.onestepmore.caredoc.databinding.AAddressMainBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

public class AddressMainActivity extends BaseActivity<AAddressMainBinding, AddressMainViewModel> implements
        HasSupportFragmentInjector,
        AddressMainNavigator {

    public final static int ADDRESS_RESULT_CODE = 99;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private AddressMainViewModel mAddressMainViewModel;
    private FragmentManager mFragmentManager;
    private boolean mIsNeedDetail = true;

    public boolean isNeedDetail() {
        return mIsNeedDetail;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_address_main;
    }

    @Override
    public AddressMainViewModel getViewModel() {
        mAddressMainViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AddressMainViewModel.class);
        return mAddressMainViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressMainViewModel.setNavigator(this);
        mFragmentManager = getSupportFragmentManager();

        if(getIntent() != null){
            mIsNeedDetail = getIntent().getBooleanExtra("needDetail", true);
        }

        BackStackManager.getInstance().addRootFragment(
                this,
                new AddressFragment(),
                R.id.a_address_main_fragment_layout);

    }

    @Override
    public void onFinish() {
        finish();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    public void onSelectJuso(JusoRealmModel juso){
        if(juso == null){
            return;
        }

        saveRecentlyJuso(juso);
    }

    private void saveRecentlyJuso(JusoRealmModel juso){
        if(juso == null){
            return;
        }

        RecentlyJuso recentlyJuso = new RecentlyJuso();
        recentlyJuso.setJuso(juso);
        getViewModel().getRealm().executeTransaction((realm) -> {
            RealmResults<RecentlyJuso> realmResults = getViewModel().getRealm().where(RecentlyJuso.class).findAll();
            RecentlyJuso recently = getViewModel().getRealm().where(RecentlyJuso.class).equalTo("id",juso.getId()).findFirst();
            if(recently != null){
                RealmObject.deleteFromRealm(recently);
            }else if(realmResults != null && realmResults.size() >= RecentlyJuso.MAX){
                RecentlyJuso oldJuso = realmResults.get(realmResults.size()-1);
                if(oldJuso != null){
                    RealmObject.deleteFromRealm(oldJuso);
                }
            }
            realm.copyToRealmOrUpdate(recentlyJuso);

            finishForResult(recentlyJuso);
        });
    }

    private void finishForResult(JusoRealmModel juso){
        Intent intent = new Intent();
        intent.putExtra("juso", juso);

        setResult(ADDRESS_RESULT_CODE, intent);
        BackStackManager.getInstance().removeBackStackInCurrentActivity();
        finish();
    }
}
