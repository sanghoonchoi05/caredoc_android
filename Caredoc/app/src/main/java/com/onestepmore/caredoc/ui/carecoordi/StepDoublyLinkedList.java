package com.onestepmore.caredoc.ui.carecoordi;

public class StepDoublyLinkedList {
    transient int size = 0;

    /**
     * Pointer to first node.
     * Invariant: (first == null && last == null) ||
     *            (first.prev == null && first.item != null)
     */
    transient StepData first;

    /**
     * Pointer to last node.
     * Invariant: (first == null && last == null) ||
     *            (last.next == null && last.item != null)
     */
    transient StepData last;

    public StepDoublyLinkedList() {
    }
}
