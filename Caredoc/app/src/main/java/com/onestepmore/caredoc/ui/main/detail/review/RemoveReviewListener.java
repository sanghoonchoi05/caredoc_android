package com.onestepmore.caredoc.ui.main.detail.review;

public interface RemoveReviewListener {
    void removeReview(long reviewId);
}
