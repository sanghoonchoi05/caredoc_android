package com.onestepmore.caredoc.ui.address.result;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import com.onestepmore.caredoc.data.model.api.external.ExternalCoordinateRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalJusoRequest;
import com.onestepmore.caredoc.data.model.api.object.external.CommonObject;
import com.onestepmore.caredoc.data.model.api.object.external.CoordinateObject;
import com.onestepmore.caredoc.data.model.api.object.external.JusoObject;
import com.onestepmore.caredoc.data.model.realm.Juso;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.CoordinateUtils;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class AddressResultViewModel extends BaseViewModel<AddressResultNavigator> {
    private final ObservableField<String> mCount = new ObservableField<>("");
    private final ObservableBoolean mEmpty = new ObservableBoolean(false);
    private final ObservableBoolean mNoMoreList = new ObservableBoolean(false);

    public ObservableBoolean getEmpty() {
        return mEmpty;
    }

    public ObservableBoolean getNoMoreList() {
        return mNoMoreList;
    }

    public ObservableField<String> getCount() {
        return mCount;
    }

    public AddressResultViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    private int mCurrentPage = 1;
    private final int COUNT_PER_PAGE = 10;
    private final String JUSO_API_KEY = "U01TX0FVVEgyMDE5MDQwMTEyMzQwNzEwODYxNTU=";
    private final String COORDINATE_API_KEY = "U01TX0FVVEgyMDE5MDQwNTE0NTcyMzEwODYyODQ=";
    private final String RESULT_TYPE = "json";

    void getJuso(String searchText, int page){
        mCurrentPage = page;
        getJuso(searchText);
    }
    void getJuso(String searchText){
        ExternalJusoRequest.QueryParameter queryParameter = new ExternalJusoRequest.QueryParameter();
        queryParameter.setCurrentPage(mCurrentPage);
        queryParameter.setCountPerPage(COUNT_PER_PAGE);
        queryParameter.setConfmKey(JUSO_API_KEY);
        queryParameter.setKeyword(searchText);
        queryParameter.setResultType(RESULT_TYPE);
        getCompositeDisposable().add(getDataManager()
                .getExternalJusoApiCall(queryParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response ->{
                    CommonObject commonObject = response.getResults().getCommon();
                    List<JusoObject> jusoObject = response.getResults().getJusoList();
                    mNoMoreList.set(false);
                    mEmpty.set(false);
                    mCount.set("");
                    if(commonObject.getErrorCode().equals("0")){
                        mCount.set(commonObject.getTotalCount());

                        if(commonObject.getTotalCount().equals("0")){
                            mEmpty.set(true);
                        }

                        if(jusoObject == null || jusoObject.size() == 0){
                            mNoMoreList.set(true);

                        }else{
                            if(jusoObject.size() < COUNT_PER_PAGE){
                                mNoMoreList.set(true);
                            }
                        }

                        saveJuso(jusoObject);
                        mCurrentPage += 1;
                    }
                    else{
                        getNavigator().networkError(response.getResults().getCommon().getErrorMessage());
                    }
                }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void saveJuso(List<JusoObject> jusoObjectList){
        if(jusoObjectList == null || jusoObjectList.size() == 0){
            return;
        }

        List<Juso> jusoList = new ArrayList<>();
        for(JusoObject jusoObject : jusoObjectList){
            Juso juso = new Juso();
            juso.setJuso(jusoObject);
            jusoList.add(juso);
        }

        getRealm().executeTransaction((realm -> realm.copyToRealmOrUpdate(jusoList)));
    }

    void clearJuso(){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Juso> jusoRealmResults = realm.where(Juso.class).findAll();
                jusoRealmResults.deleteAllFromRealm();
            }
        });
    }

    void getCoordinate(final Juso juso){
        ExternalCoordinateRequest.QueryParameter queryParameter = new ExternalCoordinateRequest.QueryParameter();
        queryParameter.setAdmCd(juso.getDongCode());
        queryParameter.setRnMgtSn(juso.getRoadCode());
        queryParameter.setUdrtYn(juso.isUnderground());
        queryParameter.setBuldMnnm(juso.getBuildingMainNo());
        queryParameter.setBuldSlno(juso.getBuildingSubNo());
        queryParameter.setConfmKey(COORDINATE_API_KEY);
        queryParameter.setResultType(RESULT_TYPE);
        getCompositeDisposable().add(getDataManager()
                .getExternalCoordinateApiCall(queryParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response ->{
                    CommonObject commonObject = response.getResults().getCommon();
                    List<CoordinateObject> coordinateObjects = response.getResults().getJusoList();
                    if(commonObject.getErrorCode().equals("0")){
                        if(coordinateObjects!= null && coordinateObjects.size() > 0)
                        updateJuso(juso.getId(), coordinateObjects.get(0).getEntX(), coordinateObjects.get(0).getEntY());
                    }
                    else{
                        getNavigator().networkError(response.getResults().getCommon().getErrorMessage());
                    }
                }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    private void updateJuso(String id, String entX, String entY){
        getRealm().executeTransactionAsync((realm -> {
            Juso juso = realm.where(Juso.class).equalTo("id", id).findFirst();
            if (juso == null) {
                return;
            }
            juso.setEntX(entX);
            juso.setEntY(entY);

            double[] coordinate = CoordinateUtils.convertFromUTMKToWGS84(Double.valueOf(entX), Double.valueOf(entY));
            if (coordinate.length > 1) {
                juso.setLat(coordinate[0]);
                juso.setLng(coordinate[1]);
            }

            realm.copyToRealmOrUpdate(juso);
        }), () -> {
            Juso juso = getRealm().where(Juso.class).equalTo("id", id).findFirst();
            if (juso == null) {
                return;
            }
            getNavigator().onUpdateCoordinate(juso);
        });
    }

    public void onClickBackButton(View view){
        getNavigator().onBack();
    }
    public void onClickSearchButton(View view){
        getNavigator().onSearch();
    }

    public void onClickDeleteButton(View view){
        getNavigator().onDelete();
    }
}
