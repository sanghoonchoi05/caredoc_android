package com.onestepmore.caredoc.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.kakao.plusfriend.PlusFriendService;
import com.kakao.util.exception.KakaoException;
import com.onestepmore.caredoc.AppConstants;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeEvent;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.api.common.SuggestionResponse;
import com.onestepmore.caredoc.data.model.api.object.ServiceObject;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.data.model.realm.Suggestion;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.databinding.AMainBinding;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.firebase.FirebaseParam;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.care.CareInfoFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiStartActivity;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.detail.FacilityActivity;
import com.onestepmore.caredoc.ui.main.favorite.FavoriteFragment;
import com.onestepmore.caredoc.ui.main.home.HomeFragment;
import com.onestepmore.caredoc.ui.main.profile.ProfileFragment;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.SnackbarUtils;
import com.zoyi.channel.plugin.android.ChannelIO;
import com.zoyi.channel.plugin.android.ChannelPluginCompletionStatus;
import com.zoyi.channel.plugin.android.ChannelPluginListener;
import com.zoyi.channel.plugin.android.ChannelPluginSettings;
import com.zoyi.channel.plugin.android.Guest;
import com.zoyi.channel.plugin.android.LauncherConfig;
import com.zoyi.channel.plugin.android.OnBootListener;
import com.zoyi.channel.plugin.android.Position;
import com.zoyi.channel.plugin.android.Profile;
import com.zoyi.channel.plugin.android.model.etc.PushEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.HOME_CLICK_CALL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.HOME_CLICK_CHANNELTALK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.HOME_CLICK_KAKAOPLUS;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.OPTIONAL_UPDATE_NOTI_CLICK_UPDATE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_CHANNELTALK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.OPTIONAL_UPDATE_NOTI;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.EXIT;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SEARCH_FACILITY_MAP_GRANTED_LOCATION_PSERMISSION;
import static com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity.RESULT_CODE_CareGuide_ACTIVITY_FINISH;
import static com.onestepmore.caredoc.ui.main.detail.FacilityActivity.EXTRA_FACILITY;
import static com.onestepmore.caredoc.ui.main.detail.FacilityActivity.EXTRA_SERVICE_ID;
import static com.onestepmore.caredoc.ui.main.search.SearchFragment.ADDRESS_KEY;

public class MainActivity extends BaseActivity<AMainBinding, MainViewModel> implements
        MainNavigator,
        HasSupportFragmentInjector,
        BottomNavigationView.OnNavigationItemSelectedListener,
        OnCompleteListener<Location>,
        OnBootListener,
        ChannelPluginListener {
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    public static final int REQUEST_CODE_FACILITY_ACTIVITY_START = 99;
    private static final int REQUEST_CODE_PERMISSION_CALL = 55;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private MainViewModel mMainViewModel;

    private FragmentManager mFragmentManager;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private LatLng mLastKnownLatLng;

    private OnCurrentLocationCallback mCallback;

    private int mCachedTouchServiceId = 0;

    private boolean mIsFabOpen = false;
    private Animation mFabOpenAni, mFabCloseAni;
    private Animation mBottomNaviUpAni, mBottomNaviDownAni;

    private long mLinkFakeId = 0L;

    public void setCurrentLocationCallback(OnCurrentLocationCallback mCallback) {
        this.mCallback = mCallback;
    }

    public boolean isFabOpen() {
        return mIsFabOpen;
    }

    public void setFabOpen(boolean fabOpen) {
        this.mIsFabOpen = fabOpen;
        getViewDataBinding().aMainFab.setFabOpen(fabOpen);
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mMainViewModel = ViewModelProviders.of(this, mViewModelFactory).get(MainViewModel.class);
        return mMainViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMainViewModel.setNavigator(this);

        initAmplitude();

        checkUpdate();

        initChannelIO();
        initFab();
        initBottomNaviAni();

        getViewModel().init();
        initFacilitySearchTempListener();

        Intent intent = getIntent();
        int index = 0;
        if (intent != null) {
            index = intent.getIntExtra("index", 0);
            mLinkFakeId = intent.getLongExtra("fakeId", 0L);
        }

        initFragmentManager();

        initBottomNavigation(index);

        initLocation();

        BackStackManager.getInstance().addRootFragment(this, HomeFragment.newInstance("fakeId", mLinkFakeId), R.id.a_main_content_fragment_layout);
    }

    private void initAmplitude() {
        //Amplitude.getInstance().initialize(this, BuildConfig.AMPLITUDE_API_KEY).enableForegroundTracking(getApplication());

        /*JSONObject eventProperties = new JSONObject();
        JSONObject eventProperties2 = new JSONObject();
        JSONObject eventProperties3 = new JSONObject();
        JSONObject eventProperties4 = new JSONObject();
        JSONObject eventProperties5 = new JSONObject();
        JSONObject eventProperties6 = new JSONObject();
        JSONObject eventProperties7 = new JSONObject();
        JSONObject eventProperties8 = new JSONObject();
        JSONObject groups = new JSONObject();
        JSONArray colors = new JSONArray();
        JSONArray list = new JSONArray();
        try {
            eventProperties.put("key", "value");

            colors.put("rose").put("gold");
            eventProperties2.put("colors", colors);
            eventProperties2.put("key", eventProperties);

            groups.put("group", "groupValue");
            groups.put("groupColors", colors);
            groups.put("groupKey", eventProperties);

            eventProperties3.put("min", 1);
            eventProperties3.put("max", 5);

            eventProperties4.put("min", 1);
            eventProperties4.put("max", 8);

            eventProperties5.put("min", 2);
            eventProperties5.put("max", 10);

            eventProperties6.put("search_term", "방이");
            eventProperties7.put("title", "방이로이병원");
            eventProperties8.put("title", "방이로이요양원");
            list.put(eventProperties7).put(eventProperties8);
            eventProperties6.put("search_list", list);

            //eventProperties3.put("key", )
        } catch (JSONException exception) {
        }*/
        //Amplitude.getInstance().logEvent("royTest", eventProperties);
        //Amplitude.getInstance().logEvent("royTest2", eventProperties2);
        //Amplitude.getInstance().logEvent("royTest3", eventProperties, groups);
        //Amplitude.getInstance().logEvent("royTest4", eventProperties3);
        //Amplitude.getInstance().logEvent("royTest4", eventProperties4);
        //Amplitude.getInstance().logEvent("royTest4", eventProperties5);
        //Amplitude.getInstance().logEvent("royTest5", eventProperties6);
    }

    private void initFragmentManager() {
        mFragmentManager = getSupportFragmentManager();

        BackStackManager.getInstance().setFragmentFadeAnimation();
    }

    public void initBottomNavigation(int index) {
        getViewDataBinding().aMainBottomNavigation.setOnNavigationItemSelectedListener(this);
        getViewDataBinding().aMainBottomNavigation.setItemIconTintList(null);

        showBottomTab(true);

        if (index > 0) {
            getViewDataBinding().aMainBottomNavigation.setSelectedItemId(R.id.id_bottom_navi_search);
        }
    }

    private void initLocation() {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mLastKnownLatLng = CommonUtils.getKoreaCenterPosition();
    }

    private void initChannelIO() {
        ChannelPluginSettings settings = new ChannelPluginSettings(AppConstants.CHANNEL_PLUGIN_KEY);
        settings.setLauncherConfig(new LauncherConfig(Position.RIGHT, 16, 75));
        Profile profile = Profile.create().setProperty("platform", "Android");
        User user = getViewModel().getRealm().where(User.class).findFirst();
        if (getViewModel().getDataManager().isLoggedIn() && user != null) {
            settings.setUserId(String.valueOf(user.getUserId()));
            profile = profile.setName(user.getName())
                    .setEmail(user.getEmail())
                    .setProperty("avatarUrl", user.getPhotoUrl())
                    .setProperty("mobileNumber", user.getContact());
        }

        ChannelIO.setChannelPluginListener(this);
        ChannelIO.boot(settings, profile, this);
    }

    private void initFacilitySearchTempListener() {
        getViewModel().getFacility().addChangeListener((results) -> {
            if (results.size() > 0) {
                showFacilityDetail(results.get(0));
            }
        });
    }

    private void checkUpdate() {
        if (FirebaseManager.getInstance().isOptionalUpdate()) {
            FirebaseManager.getInstance().logEvent(FirebaseEvent.Screen.OPTIONAL_UPDATE_NOTI);
            AmplitudeManager.getInstance().logEvent(OPTIONAL_UPDATE_NOTI);
            SnackbarUtils.showSnackbar(
                    this,
                    FirebaseManager.getInstance().getOptionalUpdateMessage(),
                    Snackbar.LENGTH_LONG,
                    getString(R.string.app_update_ok), (view) -> {
                        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.OPTIONAL_UPDATE_NOTI_CLICK_UPDATE);
                        AmplitudeManager.getInstance().logEvent(OPTIONAL_UPDATE_NOTI_CLICK_UPDATE);
                        finish();
                        FirebaseManager.getInstance().gotoMarket(this);
                    });
        }
    }

    private void initFab() {
        mFabOpenAni = AnimationUtils.loadAnimation(this, R.anim.fab_open);
        mFabCloseAni = AnimationUtils.loadAnimation(this, R.anim.fab_close);

        getViewDataBinding().setOpenFabAnim(mFabOpenAni);
        getViewDataBinding().setCloseFabAnim(mFabCloseAni);

        ViewCompat.setTranslationZ(getViewDataBinding().aMainCsFab.vCsFabBadge, 8);
        ViewCompat.setTranslationZ(getViewDataBinding().aMainCsFab.vCsFabBadgeText, 9);

        /*getViewDataBinding().aMainFab.setVisible(true);
        getViewDataBinding().aMainFab.setFabOpen(mIsFabOpen);
        getViewDataBinding().setBottomNavigationElevation(15f);

        getViewDataBinding().aMainFab.setCloseFabAnim(mFabCloseAni);
        getViewDataBinding().aMainFab.setOpenFabAnim(mFabOpenAni);
        getViewDataBinding().aMainFab.setOpenListener(this::onFabAnim);
        getViewDataBinding().aMainFab.setCloseListener(this::onFabAnim);
        getViewDataBinding().aMainFab.setCallListener(this::onFabCall);
        getViewDataBinding().aMainFab.setEmailListener(this::onFabEmail);
        getViewDataBinding().aMainFab.setKakaoListener(this::onFabKakao);*/
    }

    private void initBottomNaviAni() {
        mBottomNaviUpAni = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_up);
        mBottomNaviDownAni = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_down);
    }

    public void onFabCall(View view) {
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.TAB_HOME_CLICK_CALL);
        AmplitudeManager.getInstance().logEvent(HOME_CLICK_CALL);
        int permissionCheck = ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CALL_PHONE},
                    REQUEST_CODE_PERMISSION_CALL);
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            callIntent.setData(Uri.parse("tel:1833-9861"));
            startActivity(callIntent);
        }
    }

    private void onFabEmail(View view) {
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.TAB_HOME_CLICK_EMAIL);
        getViewDataBinding().aMainFab.setVisible(false);
        BackStackManager.getInstance().addChildFragment(new CareInfoFragment(), R.id.a_main_content_fragment_layout);
    }

    public void onFabKakao(View view) {
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.TAB_HOME_CLICK_KAKAOPLUS);
        AmplitudeManager.getInstance().logEvent(HOME_CLICK_KAKAOPLUS);

        try {
            PlusFriendService.getInstance().chat(this, "_xhYZtj");
        } catch (KakaoException e) {
            // 에러 처리 (앱키 미설정 등등)
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }


    }

    private void onFabAnim(View view) {
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.TAB_HOME_CLICK_CARE_QUESTION);

        if (mIsFabOpen) {
            getViewDataBinding().setBottomNavigationElevation(15f);
            mIsFabOpen = false;
        } else {
            getViewDataBinding().setBottomNavigationElevation(0f);
            mIsFabOpen = true;
        }

        getViewDataBinding().aMainFab.setFabOpen(mIsFabOpen);
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        handleIntent();
    }

    private void handleIntent() {
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();

        if (appLinkData != null) {
            String id = appLinkData.getLastPathSegment();
            Uri mainUri;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int id = menuItem.getItemId();
        onNavigationItemSelected(id, null);

        return true;
    }

    public void onNavigationItemSelected(String tag, Bundle bundle) {
        int id = R.id.id_bottom_navi_home;
        onCheckBottomNavigation(tag);

        if (tag.equals(SearchFragment.class.getName())) {
            id = R.id.id_bottom_navi_search;
        } else if (tag.equals(FavoriteFragment.class.getName())) {
            id = R.id.id_bottom_navi_favorite;
        } else if (tag.equals(ProfileFragment.class.getName())) {
            id = R.id.id_bottom_navi_profile;
        }
        onNavigationItemSelected(id, bundle);
    }

    private void onNavigationItemSelected(@IdRes int id, Bundle bundle) {
        onCheckFab(id);

        Fragment fragment = null;
        boolean alwaysShowRoot = false;

        if (id == R.id.id_bottom_navi_home) {
            fragment = mFragmentManager.findFragmentByTag(HomeFragment.class.getName());
            if (fragment == null) {
                fragment = new HomeFragment();
            }
        } else if (id == R.id.id_bottom_navi_search) {
            fragment = mFragmentManager.findFragmentByTag(SearchFragment.class.getName());
            if (fragment == null) {
                fragment = new SearchFragment();
            }
            alwaysShowRoot = true;
        } else if (id == R.id.id_bottom_navi_favorite) {
            fragment = mFragmentManager.findFragmentByTag(FavoriteFragment.class.getName());
            if (fragment == null) {
                fragment = new FavoriteFragment();
            }
        } else if (id == R.id.id_bottom_navi_profile) {
            fragment = mFragmentManager.findFragmentByTag(ProfileFragment.class.getName());
            if (fragment == null) {
                fragment = new ProfileFragment();
            }
        }

        if (fragment != null) {
            BaseFragment baseFragment = (BaseFragment) fragment;
            if (bundle != null) {
                baseFragment.setArguments(bundle);
            }

            BackStackManager.getInstance().addRootFragment(this, fragment, R.id.a_main_content_fragment_layout, alwaysShowRoot);
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void onFinish() {
        AmplitudeManager.getInstance().logEvent(EXIT);
        FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.EXIT);
        finish();
        BackStackManager.getInstance().removeAllFragment();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {
        onCheckBottomNavigation(tag);
        onCheckFab(tag);
    }

    private void onCheckFab(String tag) {
        getViewDataBinding().aMainFab.setVisible(false);

        // Firebase Crashlytics: MainActivity.java line 382
        if (tag == null) {
            return;
        }

        if (tag.equals(HomeFragment.class.getName())) {
            getViewDataBinding().aMainFab.setVisible(true);
        }
    }

    private void onCheckFab(int bottomNaviId) {
        getViewDataBinding().aMainFab.setVisible(false);
        if (bottomNaviId == R.id.id_bottom_navi_home) {
            getViewDataBinding().aMainFab.setVisible(true);
        }
    }

    private void onCheckBottomNavigation(String tag) {
        if (tag == null) {
            return;
        }

        MenuItem menuItem = null;
        if (tag.equals(HomeFragment.class.getName())) {
            menuItem = getViewDataBinding().aMainBottomNavigation.getMenu().findItem(R.id.id_bottom_navi_home);
        } else if (tag.equals(SearchFragment.class.getName())) {
            menuItem = getViewDataBinding().aMainBottomNavigation.getMenu().findItem(R.id.id_bottom_navi_search);
        } else if (tag.equals(FavoriteFragment.class.getName())) {
            menuItem = getViewDataBinding().aMainBottomNavigation.getMenu().findItem(R.id.id_bottom_navi_favorite);
        } else if (tag.equals(ProfileFragment.class.getName())) {
            menuItem = getViewDataBinding().aMainBottomNavigation.getMenu().findItem(R.id.id_bottom_navi_profile);
        }

        if (menuItem != null) {
            menuItem.setChecked(true);
        }
    }

    private int getBottomNavigationId(String tag) {
        if (tag == null) {
            return -1;
        }

        int bottomNavigationId = R.id.id_bottom_navi_home;
        if (tag.equals(SearchFragment.class.getName())) {
            bottomNavigationId = R.id.id_bottom_navi_search;
        } else if (tag.equals(FavoriteFragment.class.getName())) {
            bottomNavigationId = R.id.id_bottom_navi_favorite;
        } else if (tag.equals(ProfileFragment.class.getName())) {
            bottomNavigationId = R.id.id_bottom_navi_profile;
        }

        return bottomNavigationId;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CODE_CareGuide_ACTIVITY_FINISH) {
            getViewDataBinding().aMainBottomNavigation.setSelectedItemId(R.id.id_bottom_navi_search);
        }
    }

    public void openCareCoordiActivity() {
        Intent intent = newIntent(CareCoordiStartActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                /*if(BackStackManager.getInstance().getBackStackCount() > 1){
                    BackStackManager.getInstance().onBackPressed();
                }*/
                return false;
        }

        return false;
    }

    public void showBottomTab(boolean show) {
        boolean oldShow = getViewDataBinding().aMainBottomNavigation.getVisibility() == View.VISIBLE;
        if (oldShow == show) {
            return;
        }

        Animation newAni = mBottomNaviUpAni;
        if (!show) {
            newAni = mBottomNaviDownAni;
        }

        getViewDataBinding().aMainBottomNavigation.startAnimation(newAni);
        getViewDataBinding().setShowBottomTab(show);
        getViewDataBinding().executePendingBindings();
    }

    @SuppressLint("MissingPermission")
    public void checkPermission() {
        if (checkLocationPermissions()) {
            getViewModel().setLocationPermissionGranted(true);
        } else {
            getViewModel().setLocationPermissionGranted(false);
            requestLocationPermissions();
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkLocationPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestLocationPermissions() {
        // 사용자가 권한 요청을 한번 거절하면 shouldProvideRationale는 true로 온다.
        //
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) {
            SnackbarUtils.showSnackbar(this, getString(R.string.permission_location_rationale),
                    getString(android.R.string.ok), SnackbarRequestPermissionClickListener);
        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public View.OnClickListener SnackbarRequestPermissionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    };

    public View.OnClickListener SnackbarStartSettingActivityClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setAction(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package",
                    BuildConfig.APPLICATION_ID, null);
            intent.setData(uri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    };

    // TODO
    // pause -> resume 시에 퍼미션 항상 체크해서 스넥바 띄움.
    // 일단 사용하지 않음.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //AppLogger.i((TAG, "onRequestPermissionResult");
        //mLocationPermissionGranted = io.reactivex.Observable.just(false);
        getViewModel().setLocationPermissionGranted(false);

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                //AppLogger.i((TAG, "User interaction was cancelled.");
                SnackbarUtils.showSnackbar(this, getString(R.string.permission_user_canceled),
                        getString(android.R.string.ok), SnackbarRequestPermissionClickListener);

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_MAP_GRANTED_LOCATION_PSERMISSION, AmplitudeParam.Key.GRANTED, AmplitudeParam.BOOLEAN.TRUE.toString());
                getViewModel().setLocationPermissionGranted(true);
                requestMyLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.

                AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_MAP_GRANTED_LOCATION_PSERMISSION, AmplitudeParam.Key.GRANTED, AmplitudeParam.BOOLEAN.FALSE.toString());

                SnackbarUtils.showSnackbar(this, getString(R.string.permission_location_rationale),
                        getString(android.R.string.ok), SnackbarRequestPermissionClickListener);
            }
        }
    }

    public void requestMyLocation() {
        checkPermission();

        if (getViewModel().isLocationPermissionGranted()) {
            getDeviceLocation();
        } else {
            onLocationCallback();
        }
    }

    private void onLocationCallback() {
        if (mLastKnownLatLng != null) {
            if (mCallback != null) {
                mCallback.onLocation(mLastKnownLatLng, getViewModel().isLocationPermissionGranted());
            }
        }
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    public void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mFusedLocationProviderClient != null) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this);
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onComplete(@NonNull Task<Location> task) {
        if (task.isSuccessful() && task.getResult() != null) {
            // Set the map's camera position to the current location of the device.
            Location location = task.getResult();
            mLastKnownLatLng = new LatLng(location.getLatitude(), location.getLongitude());

            getViewModel().getDataManager().setLastKnownLocationLatitude(String.valueOf(location.getLatitude()));
            getViewModel().getDataManager().setLastKnownLocationLongitude(String.valueOf(location.getLongitude()));
        }

        onLocationCallback();
    }

    public void onSearch(Suggestion suggestion) {
        if (suggestion == null) {
            return;
        }

        if (suggestion.getType().equals(SuggestionResponse.TYPE.location.toString())) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(ADDRESS_KEY, CommonUtils.getStaticAddress(suggestion));

            onNavigationItemSelected(SearchFragment.class.getName(), bundle);
        } else if (suggestion.getType().equals(SuggestionResponse.TYPE.facility.toString())) {
            if (suggestion.getServices() != null && suggestion.getServices().size() > 0) {
                Service service = suggestion.getServices().get(0);
                if (service == null) {
                    return;
                }
                if (service.getServiceCategory().equals(ServiceObject.CATEGORY.GERIATRIC_HOSPITAL.getName())) {
                    onFacilityHospitalDetailClick(Long.valueOf(suggestion.getValue()));
                } else {
                    onFacilityHomecareDetailClick(Long.valueOf(suggestion.getValue()));
                }
            }
        }

        FirebaseManager.getInstance().logEvent(
                FirebaseEvent.PredefinedEvent.SEARCH,
                new FirebaseManager.Builder()
                        .addParam(FirebaseParam.PredefinedKey.CONTENT_TYPE, suggestion.getType())
                        .addParam(FirebaseParam.PredefinedKey.SEARCH_TERM, suggestion.getText()));
    }

    @Override
    public void onFacilityDetailClick(FacilityRealmModel facility, FROM from, TOUCH touch) {
        Map<String, MapValue<?>> map = new HashMap<>();
        AmplitudeParam.MODE mode = AmplitudeEvent.getAmplitudeMode(from);
        if (AmplitudeEvent.isNeedAmplitudeMode(from)) {
            map.put(AmplitudeParam.Key.MODE, new MapValue<>(mode));
        }

        map.put(AmplitudeParam.Key.VALUE, new MapValue<>(facility.getName()));
        AmplitudeParam.TOUCH ampTouch = AmplitudeParam.TOUCH.ITEM;
        if (touch == TOUCH.PICTURE) {
            ampTouch = AmplitudeParam.TOUCH.PICTURE;
        }
        map.put(AmplitudeParam.Key.TOUCH, new MapValue<>(ampTouch.toString()));

        AmplitudeManager.getInstance().logEvent(AmplitudeEvent.getListItemEventNameForTouchWithFrom(from), map);

        loadFacilityDetail(facility);
    }

    @Override
    public void onFacilityDetailClick(FacilityRealmModel facility, FROM from, Service touchService) {
        Map<String, MapValue<?>> touchMap = new HashMap<>();
        Map<String, MapValue<?>> serviceMap = new HashMap<>();

        AmplitudeParam.MODE mode = AmplitudeEvent.getAmplitudeMode(from);
        if (AmplitudeEvent.isNeedAmplitudeMode(from)) {
            touchMap.put(AmplitudeParam.Key.MODE, new MapValue<>(mode));
            serviceMap.put(AmplitudeParam.Key.MODE, new MapValue<>(mode));
        }

        touchMap.put(AmplitudeParam.Key.VALUE, new MapValue<>(facility.getName()));
        touchMap.put(AmplitudeParam.Key.TOUCH, new MapValue<>(AmplitudeParam.TOUCH.SERVICE));
        AmplitudeManager.getInstance().logEvent(AmplitudeEvent.getListItemEventNameForTouchWithFrom(from), touchMap);

        serviceMap.put(AmplitudeParam.Key.VALUE, new MapValue<>(facility.getName()));
        serviceMap.put(AmplitudeParam.Key.SERVICE, new MapValue<>(touchService.getServiceName()));
        AmplitudeManager.getInstance().logEvent(AmplitudeEvent.getListItemEventNameForServiceWithFrom(from), serviceMap);

        mCachedTouchServiceId = touchService.getId();

        loadFacilityDetail(facility);
    }

    private void loadFacilityDetail(FacilityRealmModel facility) {
        List<Service> serviceList = facility.getServices();
        if (serviceList != null && serviceList.size() > 0) {
            Service service = serviceList.get(0);
            if (service != null && service.getServiceCategory().equals(ServiceObject.CATEGORY.GERIATRIC_HOSPITAL.getName())) {
                onFacilityHospitalDetailClick(facility.getFakeId());
            } else {
                onFacilityHomecareDetailClick(facility.getFakeId());
            }
        }
    }

    @Override
    public void showFacilityDetail(FacilityRealmModel facility) {
        if (facility == null) {
            return;
        }

        FirebaseManager.getInstance().logEvent(
                FirebaseEvent.PredefinedEvent.SELECT_CONTENT,
                new FirebaseManager.Builder()
                        .addParam(FirebaseParam.PredefinedKey.CONTENT_TYPE, FirebaseParam.Value.FACILITY)
                        .addParam(FirebaseParam.PredefinedKey.ITEM_ID, facility.getFakeId()));

        Intent intent = newIntent(FacilityActivity.class);
        intent.putExtra(EXTRA_FACILITY, facility);
        intent.putExtra(EXTRA_SERVICE_ID, mCachedTouchServiceId);
        startActivityForResult(intent, REQUEST_CODE_FACILITY_ACTIVITY_START);

        mCachedTouchServiceId = 0;
    }

    @Override
    public void showChannelTalk() {
        if (ChannelIO.isBooted()) {
            Fragment fragment = BackStackManager.getInstance().getCurrentFragment();
            if(fragment != null && fragment.getActivity() == this){
                if(fragment instanceof HomeFragment){
                    AmplitudeManager.getInstance().logEvent(HOME_CLICK_CHANNELTALK);
                }
                else if(fragment instanceof ProfileFragment){
                    AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_CHANNELTALK);
                }
            }
            ChannelIO.open(MainActivity.this);
        }
    }

    public void loadFacilityDetailBaseFromServer(long id) {
        getViewModel().loadFacilityDetailBaseFromServer(id);
    }

    public void onFacilityHospitalDetailClick(long id) {
        getViewModel().loadFacilityHospitalDetailFromServer(id);
    }

    public void onFacilityHomecareDetailClick(long id) {
        getViewModel().loadFacilityHomecareDetailFromServer(id);
    }

    /*@BindingAdapter({"fabOpen", "closeAnimation", "openAnimation"})
    public static void setFabAnim(FloatingActionButton button, boolean isFabOpen, Animation closeAni, Animation openAni){
        if(closeAni == null || openAni == null){
            return;
        }
        if(isFabOpen){
            button.startAnimation(openAni);
            button.setClickable(true);
        }
        else{
            button.startAnimation(closeAni);
            button.setClickable(false);
        }
    }*/

    /*@BindingAdapter({"fabOpen", "closeAnimation", "openAnimation"})
    public static void setFabAnim(AppCompatTextView textView, boolean isFabOpen, Animation closeAni, Animation openAni){
        if(closeAni == null || openAni == null){
            return;
        }

        if(isFabOpen){
            textView.startAnimation(openAni);
        }
        else{
            textView.startAnimation(closeAni);
        }
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ChannelIO.shutdown();
    }

    @Override
    public void onCompletion(ChannelPluginCompletionStatus status, @Nullable Guest guest) {
        showCSFab(true);
    }

    public void showCSFab(boolean show) {
        if (ChannelIO.isBooted()) {
            getViewDataBinding().setShowCSFab(show);
        }
    }

    @Override
    public void willShowMessenger() {

    }

    @Override
    public void willHideMessenger() {

    }

    @Override
    public void onChangeBadge(int count) {
        getViewModel().setBadgeCount(count);
    }

    @Override
    public void onReceivePush(PushEvent pushEvent) {

    }

    @Override
    public boolean onClickChatLink(String url) {
        return false;
    }

    @Override
    public boolean onClickRedirectUrl(String url) {
        return false;
    }

    @Override
    public void onChangeProfile(String key, @Nullable Object value) {

    }
}
