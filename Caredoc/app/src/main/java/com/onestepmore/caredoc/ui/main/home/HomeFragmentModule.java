package com.onestepmore.caredoc.ui.main.home;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeFragmentModule {
    @Provides
    HomeViewModel homeViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                FacilityDataSource facilityDataSource) {
        return new HomeViewModel(application, dataSource, schedulerProvider, facilityDataSource);
    }

    @Provides
    @Named("HomeFragment")
    ViewModelProvider.Factory provideHomeViewModelProvider(HomeViewModel homeViewModel) {
        return new ViewModelProviderFactory<>(homeViewModel);
    }
}
