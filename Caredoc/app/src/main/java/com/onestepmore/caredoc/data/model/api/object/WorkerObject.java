package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkerObject {
    @Expose
    @SerializedName("medical_subject_code")
    private String medicalSubjectCode;

    @Expose
    @SerializedName("selective_care_doctor")
    private int selectiveCareDoctor;

    @Expose
    @SerializedName("doctor_count")
    private int doctorCount;

    @Expose
    @SerializedName("medical_subject")
    private MedicalSubjectObject medicalSubject;

    public String getMedicalSubjectCode() {
        return medicalSubjectCode;
    }

    public void setMedicalSubjectCode(String medicalSubjectCode) {
        this.medicalSubjectCode = medicalSubjectCode;
    }

    public int getSelectiveCareDoctor() {
        return selectiveCareDoctor;
    }

    public void setSelectiveCareDoctor(int selectiveCareDoctor) {
        this.selectiveCareDoctor = selectiveCareDoctor;
    }

    public int getDoctorCount() {
        return doctorCount;
    }

    public void setDoctorCount(int doctorCount) {
        this.doctorCount = doctorCount;
    }

    public MedicalSubjectObject getMedicalSubject() {
        return medicalSubject;
    }

    public void setMedicalSubject(MedicalSubjectObject medicalSubject) {
        this.medicalSubject = medicalSubject;
    }
}
