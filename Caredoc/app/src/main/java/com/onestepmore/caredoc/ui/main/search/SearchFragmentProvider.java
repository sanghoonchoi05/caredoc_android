package com.onestepmore.caredoc.ui.main.search;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SearchFragmentProvider {
    @ContributesAndroidInjector(modules = SearchFragmentModule.class)
    abstract SearchFragment provideSearchFragmentFactory();
}
