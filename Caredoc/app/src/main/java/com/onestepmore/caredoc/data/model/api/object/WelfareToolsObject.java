package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class WelfareToolsObject extends ItemResponse<WelfareToolsObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("id")
        private int id;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("manufacturer")
        private String manufacturer;

        @Expose
        @SerializedName("model_name")
        private String modelName;

        @Expose
        @SerializedName("usage")
        private String usage;

        @Expose
        @SerializedName("remark")
        private String remark;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(String manufacturer) {
            this.manufacturer = manufacturer;
        }

        public String getModelName() {
            return modelName;
        }

        public void setModelName(String modelName) {
            this.modelName = modelName;
        }

        public String getUsage() {
            return usage;
        }

        public void setUsage(String usage) {
            this.usage = usage;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
