package com.onestepmore.caredoc.ui.carecoordi.userinfo;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxRadioGroup;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.object.CareCoordiSearchFilter;
import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.data.model.realm.ServiceAddress;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.databinding.FUserInfoBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.address.AddressMainActivity;
import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.ui.common.AddressClickListener;
import com.onestepmore.caredoc.ui.common.BirthClickListener;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.taker.card.TakerCardFragment;
import com.onestepmore.caredoc.ui.main.taker.card.TakerCardSelectListener;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialogClickListener;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.DateUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;

import static com.onestepmore.caredoc.ui.address.AddressMainActivity.ADDRESS_RESULT_CODE;
import static com.onestepmore.caredoc.ui.main.taker.add.basic.TakerBasicFragment.ADDRESS_REQUEST_CODE;
import static com.onestepmore.caredoc.utils.DateUtils.SUMMARY_DATE_TIME_FORMAT;

public class UserInfoFragment extends BaseCoordiFragment<FUserInfoBinding, UserInfoViewModel> implements
        UserInfoNavigator,
        BottomSheetDialogClickListener,
        BirthClickListener,
        AddressClickListener,
        TakerCardSelectListener
{

    /*public static UserInfoFragment newInstance(boolean existTaker){
        Bundle bundle = new Bundle();
        bundle.putBoolean("existTaker", existTaker);
        UserInfoFragment userInfoFragment = new UserInfoFragment();
        userInfoFragment.setArguments(bundle);

        return userInfoFragment;

    }*/

    private static final String BOTTOM_SHEET_DIALOG_SERVICE_ADDRESS_TAG = "BOTTOM_SHEET_DIALOG_SERVICE_ADDRESS_TAG";
    private static final String BOTTOM_SHEET_DIALOG_YEAR_TAG = "BOTTOM_SHEET_DIALOG_YEAR_TAG";
    private static final String BOTTOM_SHEET_DIALOG_MONTH_TAG = "BOTTOM_SHEET_DIALOG_MONTH_TAG";
    private static final String BOTTOM_SHEET_DIALOG_DAY_TAG = "BOTTOM_SHEET_DIALOG_DAY_TAG";

    @Inject
    @Named("UserInfoFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private UserInfoViewModel mUserInfoViewModel;

    private List<StaticAddress> mSelectedAddressList = new ArrayList<>();
    private Calendar mBirth = null;

    private CareCoordiSearchFilter.TakerInfo mCachedTakerInfo;

    private boolean mSelectedLti = false;

    private int mDefaultYear = 1945;
    private int mDefaultMonth = 0;
    private int mDefaultDate = 1;


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_user_info;
    }

    @Override
    public UserInfoViewModel getViewModel() {
        mUserInfoViewModel = ViewModelProviders.of(this, mViewModelFactory).get(UserInfoViewModel.class);
        return mUserInfoViewModel;
    }

    private CareCoordiActivity getCareCoordiActivity(){
        if(getBaseActivity() instanceof  CareCoordiActivity){
            return ((CareCoordiActivity)getBaseActivity());
        }

        return null;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserInfoViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getViewModel().getDataManager().isLoggedIn()){
            RealmResults<Taker> takers = getViewModel().getRealm().where(Taker.class).findAll();
            getViewDataBinding().setExistTaker(takers != null && takers.size() > 0);
        }

        if(getCareCoordiActivity() != null){
            mCachedTakerInfo = getCareCoordiActivity().getCachedTakerInfo();
        }

        initBirth();
        refreshData();
        initListener();
        setTextObserver();
    }

    private void initBirth(){
        // 생년월일 초기화
        if(mBirth == null){
            mBirth = Calendar.getInstance();
        }
        mBirth.set(mDefaultYear, mDefaultMonth, mDefaultDate);
        if(mCachedTakerInfo.getBirth() != null && !mCachedTakerInfo.getBirth().isEmpty()){
            mBirth.setTime(DateUtils.getDate(mCachedTakerInfo.getBirth(), SUMMARY_DATE_TIME_FORMAT));
        }
    }

    private void initListener(){
        getViewDataBinding().setBirthListener(this);
        getViewDataBinding().setAddressListener(this);
    }

    private void refreshData(){
        if(getCareCoordiActivity() != null){
            getViewDataBinding().setExistServiceAddress(getCareCoordiActivity().isExistCachedServiceAddress());
        }
        mCachedTakerInfo.setBirth(DateUtils.getSummaryDateTimeFormat(mBirth));
        mCachedTakerInfo.setBirthCalendar(mBirth);
        getViewDataBinding().setTakerInfo(mCachedTakerInfo);
        getViewDataBinding().fUserInfoLtiGroup.check(mCachedTakerInfo.isLti() ? R.id.f_user_info_lti_yes : R.id.f_user_info_lti_no);

    }

    private void setTextObserver(){
        rx.Observable<Integer> ltiObserver = RxRadioGroup.checkedChanges(getViewDataBinding().fUserInfoLtiGroup);
        getViewModel().addSubscription(ltiObserver.subscribe((id) -> {
            mCachedTakerInfo.setLti(id == R.id.f_taker_basic_blhsr_yes);
        }));
    }

    @Override
    public void onClick(String tag, String text) {
        if(tag.equals(BOTTOM_SHEET_DIALOG_YEAR_TAG)){
            mBirth.set(Calendar.YEAR, getBirthNumber(BOTTOM_SHEET_DIALOG_YEAR_TAG, text));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_MONTH_TAG)){
            mBirth.set(Calendar.MONTH, getBirthNumber(BOTTOM_SHEET_DIALOG_MONTH_TAG, text));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_DAY_TAG)){
            mBirth.set(Calendar.DAY_OF_MONTH, getBirthNumber(BOTTOM_SHEET_DIALOG_DAY_TAG, text));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_SERVICE_ADDRESS_TAG)){
            mCachedTakerInfo.setAddressName(text);
            List<ServiceAddress> addressList = getCareCoordiActivity().getCachedServiceAddress();
            for(ServiceAddress address : addressList){
                if(address.getAddressName().equals(text)){
                    String currentPosition = address.getLatLng().getLatitude() + "," + address.getLatLng().getLongitude();
                    getCareCoordiActivity().getViewModel().setCurrentPosition(currentPosition);
                }
            }
        }

        refreshData();
    }

    private int getBirthNumber(String tag, String text){
        int number = 0;
        if(!CommonUtils.checkNullAndEmpty(text)){
            number = Integer.valueOf(text);
        }

        switch (tag){
            case BOTTOM_SHEET_DIALOG_YEAR_TAG:
                if(number == 0){
                    number = mDefaultYear;
                }
                break;
            case BOTTOM_SHEET_DIALOG_MONTH_TAG:
                break;
            case BOTTOM_SHEET_DIALOG_DAY_TAG:
                if(number == 0){
                    number = mDefaultDate;
                }
                break;
            default:
                break;
        }

        return number;
    }

    @Override
    public void onClose(String tag) {

    }

    @Override
    public void onInit(String tag) {
        if(tag.equals(BOTTOM_SHEET_DIALOG_YEAR_TAG)){
            mBirth.set(Calendar.YEAR, getBirthNumber(BOTTOM_SHEET_DIALOG_YEAR_TAG, ""));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_MONTH_TAG)){
            mBirth.set(Calendar.MONTH, getBirthNumber(BOTTOM_SHEET_DIALOG_MONTH_TAG, ""));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_DAY_TAG)){
            mBirth.set(Calendar.DAY_OF_MONTH, getBirthNumber(BOTTOM_SHEET_DIALOG_DAY_TAG, ""));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_SERVICE_ADDRESS_TAG)){
            mCachedTakerInfo.setAddressName("");
            if(getCareCoordiActivity() != null){
                getCareCoordiActivity().getViewModel().setCurrentPosition("");
            }
        }

        refreshData();
    }

    private String [] getBotttomSheetTextList(String tag){
        String [] textList = null;

        if(tag.equals(BOTTOM_SHEET_DIALOG_SERVICE_ADDRESS_TAG)){
            textList = getServiceAddressTextList();
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_YEAR_TAG)){
            textList = getBirthTextList(BOTTOM_SHEET_DIALOG_YEAR_TAG);
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_MONTH_TAG)){
            textList = getBirthTextList(BOTTOM_SHEET_DIALOG_MONTH_TAG);
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_DAY_TAG)){
            textList = getBirthTextList(BOTTOM_SHEET_DIALOG_DAY_TAG);
        }

        return textList;
    }

    private String [] getBirthTextList(String tag){
        if(CommonUtils.checkNullAndEmpty(tag)){
            return  null;
        }

        int min = 0;
        int max = 0;
        if(tag.equals(BOTTOM_SHEET_DIALOG_YEAR_TAG)){
            min = 1900;
            max = 1970;
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_MONTH_TAG)){
            min = 1;
            max = 12;
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_DAY_TAG)){
            min = 1;
            max = 31;
        }

        int count = max - min + 1;
        String [] textList = new String[count];
        for(int i=0; i<count; i++){
            textList[i] = String.valueOf(min + i);
        }

        return textList;
    }

    private String [] getServiceAddressTextList(){
        List<ServiceAddress> serviceAddresses = ((CareCoordiActivity)getBaseActivity()).getCachedServiceAddress();
        if(serviceAddresses == null || serviceAddresses.size() == 0){
            return null;
        }

        String [] textList = new String[serviceAddresses.size()];
        for(int i=0; i<textList.length; i++){
            textList[i] = serviceAddresses.get(i).getAddressName();
        }

        return textList;
    }

    public void onBottomSheetDialog(String tag, String [] textList) {
        if(textList == null || textList.length == 0){
            return;
        }

        BottomSheetDialog dialog = BottomSheetDialog.getInstance(textList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), tag);
    }

    @Override
    public void onShowAddressView(int priority) {
        if(getCareCoordiActivity() != null){
            if(getCareCoordiActivity().isExistCachedServiceAddress()){
                onBottomSheetDialog(BOTTOM_SHEET_DIALOG_SERVICE_ADDRESS_TAG, getServiceAddressTextList());
                return;
            }
        }

        onShowAddressView();
    }

    private void onShowAddressView(){
        Intent intent = getBaseActivity().newIntent(AddressMainActivity.class);
        intent.putExtra("needDetail", false);
        startActivityForResult(intent, ADDRESS_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADDRESS_REQUEST_CODE){
            if(resultCode == ADDRESS_RESULT_CODE){
                if(data == null){
                    return;
                }

                JusoRealmModel juso = data.getParcelableExtra("juso");

                String addressName = juso.getJibunAddress();
                if(CommonUtils.checkNullAndEmpty(addressName)){
                    StringBuilder roadName = new StringBuilder();
                    roadName.append(juso.getRoadAddress());
                    if(!CommonUtils.checkNullAndEmpty(juso.getBuildingName())){
                        roadName.append(" ");
                        roadName.append(juso.getBuildingName());
                    }
                    addressName = roadName.toString();
                }


                if(getCareCoordiActivity() != null){
                    mCachedTakerInfo.setAddressName(addressName);
                    String currentPosition = juso.getLat() + "," + juso.getLng();
                    getCareCoordiActivity().getViewModel().setCurrentPosition(currentPosition);
                }

                refreshData();
            }
        }
    }

    @Override
    public void onDeleteAddress(int priority) {
        mCachedTakerInfo.setAddressName("");
        if(getCareCoordiActivity() != null){
            getCareCoordiActivity().getViewModel().setCurrentPosition("");
        }

        refreshData();
    }

    @Override
    public void setTitle(int titleResId){
        UIUtils.setTextIncludeAnnotation(getBaseActivity(), titleResId, getViewDataBinding().fUserInfoTitle);
    }

    @Override
    public void setStepData(StepData stepData) {
        if(stepData == null){
            return;
        }

        setTitle(CommonUtils.getCareCoordiTitleResId(stepData));
        ((CareCoordiActivity)getBaseActivity()).setSelectedNextUIItem(CareCoordiActivity.UI_ITEM.RESULT);
    }

    @Override
    public void onShowYearList(){
        onBottomSheetDialog(BOTTOM_SHEET_DIALOG_YEAR_TAG, getBotttomSheetTextList(BOTTOM_SHEET_DIALOG_YEAR_TAG));
    }

    @Override
    public void onShowMonthList(){
        onBottomSheetDialog(BOTTOM_SHEET_DIALOG_MONTH_TAG, getBotttomSheetTextList(BOTTOM_SHEET_DIALOG_MONTH_TAG));
    }

    @Override
    public void onShowDateList(){
        onBottomSheetDialog(BOTTOM_SHEET_DIALOG_DAY_TAG, getBotttomSheetTextList(BOTTOM_SHEET_DIALOG_DAY_TAG));
    }

    @Override
    public void onShowTakerCardView() {
        TakerCardFragment fragment = TakerCardFragment.newInstance(true);
        fragment.setTakerCardSelectListener(this);
        BackStackManager.getInstance().addRootFragment(getBaseActivity(), fragment, R.id.v_step_child_fragment_layout);
    }

    @Override
    public void onNewAddress() {
        onShowAddressView();
    }

    @Override
    public void onSelect(Taker taker) {
        if(getCareCoordiActivity() != null){
            getCareCoordiActivity().setTaker(taker);
            mCachedTakerInfo = getCareCoordiActivity().getCachedTakerInfo();
        }

        initBirth();
        refreshData();
    }
}
