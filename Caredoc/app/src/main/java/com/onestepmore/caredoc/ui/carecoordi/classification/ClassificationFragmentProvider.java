package com.onestepmore.caredoc.ui.carecoordi.classification;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ClassificationFragmentProvider {
    @ContributesAndroidInjector(modules = ClassificationFragmentModule.class)
    abstract ClassificationFragment provideClassificationFragmentFactory();
}
