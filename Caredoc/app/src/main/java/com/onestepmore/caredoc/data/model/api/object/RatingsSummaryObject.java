package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class RatingsSummaryObject {
    public final static class Item{
        public Item(){

        }
        @Expose
        @SerializedName("label")
        private String label;

        @Expose
        @SerializedName("value")
        private float value;

        public void setLabel(String label) {
            this.label = label;
        }

        public void setValue(float value) {
            this.value = value;
        }

        public float getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
}
