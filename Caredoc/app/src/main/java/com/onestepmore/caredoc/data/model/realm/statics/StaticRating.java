package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.RatingRelationsObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StaticRating extends RealmObject implements Parcelable {
    @PrimaryKey
    private int id;
    private String label;

    public StaticRating(){}

    protected StaticRating(Parcel in) {
        id = in.readInt();
        label = in.readString();
    }

    public void setRating(RatingRelationsObject.Item item){
        this.id = item.getId();
        this.label = item.getLabel();
    }

    public static final Creator<StaticRating> CREATOR = new Creator<StaticRating>() {
        @Override
        public StaticRating createFromParcel(Parcel in) {
            return new StaticRating(in);
        }

        @Override
        public StaticRating[] newArray(int size) {
            return new StaticRating[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(label);
    }

    public String getLabel() {
        return label;
    }

    public int getId() {
        return id;
    }
}
