package com.onestepmore.caredoc.ui.account.login.forgot;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.widget.EditText;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.account.PasswordEmailRequest;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.RESET_PASSWORD_COMPLETE;

public class LoginForgotViewModel extends BaseViewModel<LoginForgotNavigator> {
    private final ObservableField<String> userEmail = new ObservableField<>();
    private final ObservableBoolean possibleSendPasswordEmail = new ObservableBoolean(false);

    public LoginForgotViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onSendPasswordEmail(){
        if(!possibleSendPasswordEmail.get()){
            getNavigator().invalidEmail();
            return;
        }

        PasswordEmailRequest request = new PasswordEmailRequest();
        request.setEmail(userEmail.get());

        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerPasswordEmail(request)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getNavigator().onComplete();
                    AmplitudeManager.getInstance().logEvent(RESET_PASSWORD_COMPLETE);
                    FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.RESET_PASSWORD_COMPLETE);
                    //getDataManager().updateAccessToken(response.getToken(), response.getExpiresIn(), response.getTokenType());
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void setEditTextChangeObserver(EditText emailEdit){
        rx.Observable<Boolean> emailChange = RxTextView.textChanges(emailEdit)
                .map(this::checkChangedEmail);

        addSubscription(emailChange.subscribe(possibleSendPasswordEmail::set));
    }

    private boolean checkChangedEmail(CharSequence text){
        String email = text.toString().trim();
        userEmail.set(email);
        return CommonUtils.isEmailValid(email);
    }
}
