package com.onestepmore.caredoc.ui.address;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AddressFragmentModule {
    @Provides
    AddressViewModel addressViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new AddressViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("AddressFragment")
    ViewModelProvider.Factory provideAddressViewModelProvider(AddressViewModel addressViewModel) {
        return new ViewModelProviderFactory<>(addressViewModel);
    }
}
