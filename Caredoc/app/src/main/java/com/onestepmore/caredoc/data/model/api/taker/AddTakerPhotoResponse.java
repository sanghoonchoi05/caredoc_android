package com.onestepmore.caredoc.data.model.api.taker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class AddTakerPhotoResponse {
    @Expose
    @SerializedName("attachable_id")
    private long attachable_id;

    @Expose
    @SerializedName("attachable_type")
    private String attachableType;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("full_thumbnail_url")
    private String fullThumbnailUrl;

    public String getUrl() {
        return url;
    }

    public long getAttachable_id() {
        return attachable_id;
    }

    public String getAttachableType() {
        return attachableType;
    }

    public String getFullThumbnailUrl() {
        return fullThumbnailUrl;
    }
}
