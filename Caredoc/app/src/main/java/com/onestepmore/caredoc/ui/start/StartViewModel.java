package com.onestepmore.caredoc.ui.start;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class StartViewModel extends BaseViewModel<StartNavigator> {

    public StartViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onUserStartButtonClick(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.START_CLICK_REGISTER);
        getNavigator().openAccountMainActivity();
    }

    public void onGuestStartButtonClick(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.START_CLICK_CARECOORDI);
        getNavigator().openCareCoordiActivity();
    }

    public void onHomeStartButtonClick(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.START_CLICK_HOME);
        getNavigator().openMainActivity(0);
    }
}
