package com.onestepmore.caredoc.ui.main.favorite;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.BookmarkRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class FavoriteFragmentModule {
    @Provides
    FavoriteViewModel favoriteViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                        BookmarkRepository bookmarkRepository) {
        return new FavoriteViewModel(application, dataSource, schedulerProvider, bookmarkRepository);
    }

    @Provides
    @Named("FavoriteFragment")
    ViewModelProvider.Factory provideFavoriteViewModelProvider(FavoriteViewModel favoriteViewModel) {
        return new ViewModelProviderFactory<>(favoriteViewModel);
    }
}
