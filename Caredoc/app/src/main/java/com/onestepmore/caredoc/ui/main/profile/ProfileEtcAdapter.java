package com.onestepmore.caredoc.ui.main.profile;

import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.IProfileEtcBinding;

import java.util.ArrayList;
import java.util.List;

public class ProfileEtcAdapter extends RecyclerView.Adapter<ProfileEtcAdapter.ProfileEtcViewHolder> implements View.OnClickListener
{
    List<String> mTextList = new ArrayList<>();

    @Nullable
    private final ProfileNavigator mNavigator;

    public ProfileEtcAdapter(ProfileNavigator navigator){
        mNavigator = navigator;
    }

    public void isLogin(boolean isLogin){
        mTextList.clear();
        for(ProfileFragment.ETC_LIST etc : ProfileFragment.ETC_LIST.values()){
            if(!isLogin && etc.isLogin()){
                continue;
            }
            mTextList.add(etc.getTitle());
        }

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProfileEtcViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IProfileEtcBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_profile_etc, viewGroup, false);
        return new ProfileEtcViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileEtcViewHolder bottomSheetDialogViewHolder, int position) {
        bottomSheetDialogViewHolder.binding.setStartText(mTextList.get(position));
        bottomSheetDialogViewHolder.binding.setShowArrow(true);
        bottomSheetDialogViewHolder.binding.setPosition(position);
        bottomSheetDialogViewHolder.binding.setNavigator(mNavigator);

        if(mTextList.get(position).equals(ProfileFragment.ETC_LIST.VERSION.getTitle())){
            bottomSheetDialogViewHolder.binding.setEndText("v" + BuildConfig.CAREDOC_FULL_VERSION);
            bottomSheetDialogViewHolder.binding.setShowArrow(false);
        }

        //bottomSheetDialogViewHolder.binding.setPosition(position);
        bottomSheetDialogViewHolder.itemView.setOnClickListener(this);
        bottomSheetDialogViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mTextList == null ? 0 : mTextList.size();
    }

    @Override
    public void onClick(View v) {

    }

    class ProfileEtcViewHolder extends RecyclerView.ViewHolder {
        final IProfileEtcBinding binding;

        private ProfileEtcViewHolder(IProfileEtcBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
