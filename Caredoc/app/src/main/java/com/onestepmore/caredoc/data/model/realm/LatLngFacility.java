package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;

import com.onestepmore.caredoc.data.model.api.object.ApiCommonObject;

import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class LatLngFacility implements LatLngRealmModel {
    @PrimaryKey
    private int key;     // id는 해쉬코드로 생성
    private String facilityFakeId;
    private String type;
    private double latitude;
    private double longitude;

    public LatLngFacility(){}

    protected LatLngFacility(Parcel in) {
        key = in.readInt();
        facilityFakeId = in.readString();
        type = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<LatLngFacility> CREATOR = new Creator<LatLngFacility>() {
        @Override
        public LatLngFacility createFromParcel(Parcel in) {
            return new LatLngFacility(in);
        }

        @Override
        public LatLngFacility[] newArray(int size) {
            return new LatLngFacility[size];
        }
    };

    public void setLatLng(long fakeId, Service.TYPE type, ApiCommonObject.GeoLocation geoLocation){
        if(geoLocation == null){
            return;
        }

        this.facilityFakeId = String.valueOf(fakeId);
        this.type = type.toString();
        this.latitude = geoLocation.getCoordinates().get(1);
        this.longitude = geoLocation.getCoordinates().get(0);
        this.key = hashCodeForPrimaryKey();
    }

    public void setLatLng(long fakeId, Service.TYPE type, LatLngFacility latLngFacility){
        if(latLngFacility == null){
            return;
        }

        this.facilityFakeId = String.valueOf(fakeId);
        this.type = type.toString();
        this.latitude = latLngFacility.getLatitude();
        this.longitude = latLngFacility.getLongitude();
        this.key = hashCodeForPrimaryKey();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(key);
        dest.writeString(facilityFakeId);
        dest.writeString(type);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String getId() {
        return facilityFakeId;
    }

    @Override
    public void setId(String id) {
        this.facilityFakeId = id;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }
    @Override
    public double getLongitude() {
        return longitude;
    }

    private int hashCodeForPrimaryKey() {
        int result = 17;
        result = 31 * result + (facilityFakeId != null ? facilityFakeId.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
