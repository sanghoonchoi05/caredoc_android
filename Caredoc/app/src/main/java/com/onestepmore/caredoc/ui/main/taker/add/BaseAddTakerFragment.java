package com.onestepmore.caredoc.ui.main.taker.add;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;

import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.UIUtils;

public abstract class BaseAddTakerFragment <T extends ViewDataBinding,V extends BaseViewModel> extends BaseFragment<T,V> {
    /*public abstract void setTitle(int titleResId);
    public abstract void setStepData(StepData stepData);*/
    public boolean mIsModify = false;
    public void onExit(){
        ((AddTakerActivity)getBaseActivity()).onExit();
    }

    public static Bundle getBundle(int titleResId, boolean isModify){
        Bundle bundle = new Bundle();
        bundle.putInt("titleResId", titleResId);
        bundle.putBoolean("isModify", isModify);
        return bundle;
    }

    public void setModify(boolean modify){
        mIsModify = modify;
    }

    public boolean isModify(){
        return mIsModify;
    }

    public void initTitle(AppCompatTextView titleTextView){
        if(getArguments() != null){
            int titleResId = getArguments().getInt("titleResId");
            UIUtils.setTextIncludeAnnotation(getBaseActivity(), titleResId, titleTextView);
        }
    }
}
