package com.onestepmore.caredoc.ui.main.search;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;

import java.util.List;

public class SearchCategoryAdapter extends BaseAdapter {
    List<StaticCategory> data;
    LayoutInflater inflater;


    public SearchCategoryAdapter(Context context, List<StaticCategory> data){
        this.data = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(data!=null) return data.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.i_search_category_normal, parent, false);
        }

        if(data!=null){
            //데이터세팅
            String text = data.get(position).getText();
            ((AppCompatTextView)convertView.findViewById(R.id.i_search_category_normal_text)).setText(text);
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.i_search_category_dropdown, parent, false);
        }

        //데이터세팅
        String text = data.get(position).getText();
        ((AppCompatTextView)convertView.findViewById(R.id.i_search_category_dropdown_text)).setText(text);

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
