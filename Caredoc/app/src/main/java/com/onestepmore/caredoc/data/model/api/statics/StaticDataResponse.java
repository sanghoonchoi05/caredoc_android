package com.onestepmore.caredoc.data.model.api.statics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.AggregatesObject;
import com.onestepmore.caredoc.data.model.api.object.SearchCategoriesObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceObject;
import com.onestepmore.caredoc.data.model.api.object.ServicesObject;
import com.onestepmore.caredoc.data.model.api.object.StaticServiceObject;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;

import java.util.List;

public final class StaticDataResponse {
    @Expose
    @SerializedName("search_categories")
    private SearchCategoriesObject categories;

    @Expose
    @SerializedName("services")
    private ServicesObject<StaticServiceObject> services;

    @Expose
    @SerializedName("aggregates")
    private AggregatesObject aggregates;

    public SearchCategoriesObject getCategories() {
        return categories;
    }

    public ServicesObject<StaticServiceObject> getServices() {
        return services;
    }

    public AggregatesObject getAggregates() {
        return aggregates;
    }
}
