package com.onestepmore.caredoc.data.model.api.carecoordi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.R;

public final class CareCoordiRequest {
    public enum ORDER_BY{
        grade_desc("grade", R.string.care_coordi_result_order_grade_desc, true, true),
        grade_asc("grade", R.string.care_coordi_result_order_grade_asc, true, false),
        distance_asc("distance", R.string.care_coordi_result_order_distance_asc, true, false),
        distance_desc("distance", R.string.care_coordi_result_order_distance_desc, true, true),
        rating_desc("rating", R.string.care_coordi_result_order_rating_desc, true, true),
        rating_asc("rating", R.string.care_coordi_result_order_rating_asc, true, false),
        diseases("diseases", R.string.care_coordi_result_order_disease, false, true),
        scale_desc("scale", R.string.care_coordi_result_order_scale_desc, false, true),
        scale_asc("scale", R.string.care_coordi_result_order_scale_asc, false, false),
        medicalExpenses_asc("medicalExpenses", R.string.care_coordi_result_order_price_asc, false, false),
        medicalExpenses_desc("medicalExpenses", R.string.care_coordi_result_order_price_desc, false, true);

        private int strId;
        private boolean isCommon;
        private String name;
        private boolean desc;
        ORDER_BY(String name, int strId, boolean isCommon, boolean desc){
            this.strId = strId;
            this.isCommon = isCommon;
            this.name = name;
            this.desc = desc;
        }

        public int getStrId() {
            return strId;
        }

        public boolean isCommon() {
            return isCommon;
        }

        public String getName() {
            return name;
        }
        public boolean desc() {
            return desc;
        }
    }
    public enum ORDER_DESC{
        asc,
        desc,
    }
    public static class QueryParameter{
        @Expose
        @SerializedName("filter")
        private String filter;

        @Expose
        @SerializedName("skip")
        private int skip;

        @Expose
        @SerializedName("orderBy")
        private String orderBy;

        @Expose
        @SerializedName("orderDesc")
        private String orderDesc;

        @Expose
        @SerializedName("currentPosition")
        private String currentPosition;

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public int getSkip() {
            return skip;
        }

        public void setSkip(int skip) {
            this.skip = skip;
        }

        public String getOrderBy() {
            return orderBy;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }

        public String getOrderDesc() {
            return orderDesc;
        }

        public void setOrderDesc(String orderDesc) {
            this.orderDesc = orderDesc;
        }

        public String getCurrentPosition() {
            return currentPosition;
        }

        public void setCurrentPosition(String currentPosition) {
            this.currentPosition = currentPosition;
        }
    }
}
