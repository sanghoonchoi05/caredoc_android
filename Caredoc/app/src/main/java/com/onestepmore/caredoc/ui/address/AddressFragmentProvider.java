package com.onestepmore.caredoc.ui.address;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AddressFragmentProvider {
    @ContributesAndroidInjector(modules = AddressFragmentModule.class)
    abstract AddressFragment provideAddressFragmentFactory();
}
