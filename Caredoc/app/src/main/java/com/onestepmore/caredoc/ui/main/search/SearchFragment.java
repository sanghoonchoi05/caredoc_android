package com.onestepmore.caredoc.ui.main.search;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.AdapterView;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.onestepmore.caredoc.data.model.api.object.SearchFilter;
import com.onestepmore.caredoc.data.model.api.object.SearchMapFilter;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.Suggestion;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.databinding.FSearchBinding;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.base.TransactionCallback;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStack;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.search.dialog.SearchDialog;
import com.onestepmore.caredoc.ui.main.search.filter.address.AddressFilterFragment;
import com.onestepmore.caredoc.ui.main.search.filter.detail.FilterDetailFragment;
import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilter;
import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilterFragment;
import com.onestepmore.caredoc.ui.main.search.list.SearchListFragment;
import com.onestepmore.caredoc.ui.main.search.map.SearchMapFragment;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_CATEGORY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_FILTER_DETAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_FILTER_GRADE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_FILTER_LOCATION;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_SEARCH_KEYWORD;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SEARCH_FACILITY_REMOVE_FILTER_GRADE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SEARCH_FACILITY_REMOVE_FILTER_LOCATION;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SEARCH_FACILITY_REMOVE_FILTER_RATING;
import static com.onestepmore.caredoc.data.model.api.object.SearchFilter.DEFAULT_RATING;
import static com.onestepmore.caredoc.ui.main.search.SearchFragment.FILTER_UI.location;
import static com.onestepmore.caredoc.ui.main.search.SearchFragment.FILTER_UI.rating;
import static com.onestepmore.caredoc.ui.main.search.SearchFragment.FILTER_UI.service;

public class SearchFragment extends BaseFragment<FSearchBinding, SearchViewModel> implements
        SearchNavigator,
        FilterUICallback,
        SearchDialog.OnSearchListener,
        ChangeFilterCallback,
        TransactionCallback,
        AdapterView.OnItemSelectedListener
{
    public enum FILTER_UI {
        location(0, true),
        service(1, true),
        rating(2, false);

        private int position;
        private boolean expose;

        FILTER_UI(int position, boolean expose) {
            this.position = position;
            this.expose = expose;
        }

        public boolean isExpose() {
            return expose;
        }

        public int getPosition() {
            return position;
        }
    }

    public static final String SEARCH_CATEGORY_KEY = "SEARCH_CATEGORY_KEY";
    public static final String ADDRESS_KEY = "ADDRESS_KEY";

    @Inject
    @Named("SearchFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private SearchViewModel mSearchViewModel;

    private StaticCategory mSelectedCategory = null;
    private StaticAddress mSelectedAddress = null;
    private List<ServiceFilter> mSelectedServicesFilter = null;
    private Integer mSelectedRating = 1;
    private FacilitySearchRequest.ORDER_BY mOrderBy = FacilitySearchRequest.ORDER_BY.grade;

    private List<FilterUI> mSelectedFilterUIList = new ArrayList<>();
    private String [] mCachedDefaultFilterStrList = null;

    // filter set
    private SearchFilter.SearchFilterBuilder mSearchFilterBuilder = null;
    private SearchMapFilter.SearchMapFilterBuilder mSearchMapFilterBuilder = null;

    public static final float CAMERA_SIDO_ZOOM_VALUE = 10f;
    public static final float CAMERA_DEFAULT_ZOOM_VALUE = 13f;

    private boolean mIgnoreSpinnerSelected = false;

    public List<ServiceFilter> getSelectedServicesFilter() {
        if(mSelectedServicesFilter == null){
            createServicesFilter();
        }
        return mSelectedServicesFilter;
    }

    public Integer getSelectedRating() {
        return mSelectedRating;
    }

    public void setSelectedRating(Integer selectedRating) {
        this.mSelectedRating = selectedRating;
    }

    public StaticAddress getSelectedAddress() {
        return mSelectedAddress;
    }

    public void setSelectedAddress(StaticAddress selectedAddress) {
        this.mSelectedAddress = selectedAddress;
    }

    public FacilitySearchRequest.ORDER_BY getOrderBy() {
        return mOrderBy;
    }

    public void setOrderBy(FacilitySearchRequest.ORDER_BY orderBy) {
        this.mOrderBy = orderBy;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_search;
    }

    @Override
    public SearchViewModel getViewModel() {
        if(getBaseActivity() != null){
            mSearchViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchViewModel.class);
        }
        return mSearchViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSearchViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        UIUtils.setStatusBarColor(getBaseActivity(), getView(), getResources().getColor(R.color.colorPrimary), false);

        // 서치 카테고리 스피너 초기화
        initSearchCategorySpinner();

        // 서치 카테고리 '전체'로 셋팅
        setSelectCategory(0);

        // 주소필터 초기화
        initAddressCode();

        // 서비스필터 초기화
        initServicesFilter();

        // 후기평점필터 초기화
        initRating();

        // arguments가 있는 경우
        Bundle bundle = getArguments();
        if(bundle != null){
            // 서치 카테고리인지, 지역검색인지
            handleBundle(getArguments());
            deleteArguments();
        }

        // 필터리스트 초기화
        initDefaultFilterStrList();
        initFilterUIList();
        changeAllFilterUI();
        initFilterAdapter();

        loadFacilityFromServer();

        BackStackManager.getInstance().addChildFragment(new SearchListFragment(), R.id.f_search_child_fragment_layout);
    }

    private void initSearchCategorySpinner(){
        // 서치 카테고리 UI 구성
        SearchCategoryAdapter searchCategoryAdapter = new SearchCategoryAdapter(getBaseActivity(), RealmDataAccessor.getInstance().getStaticCategory());
        getViewDataBinding().fSearchTop.fSearchCategorySpinner.setAdapter(searchCategoryAdapter);
        getViewDataBinding().fSearchTop.fSearchCategorySpinner.setOnItemSelectedListener(this);
    }

    private void initFilterAdapter(){
        // 필터 UI 구성
        FilterUIAdapter adapter = (FilterUIAdapter) getViewDataBinding().fSearchFilterLayout.fSearchFacilityFilterRecyclerView.getAdapter();
        if (adapter != null) {
            adapter.setFacilityFilterList(mSelectedFilterUIList, mCachedDefaultFilterStrList);
        }
        else{
            int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_search_facility_filter_item_space);
            SpacesItemDecoration decoration = new SpacesItemDecoration(0, spacingInPixels, true);
            spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_search_facility_filter_item_first_last_space);
            decoration.setSpaceFirstAndLastHorizontal(spacingInPixels);
            getViewDataBinding().fSearchFilterLayout.fSearchFacilityFilterRecyclerView.addItemDecoration(decoration);

            FilterUIAdapter newAdapter = new FilterUIAdapter(this);
            newAdapter.setFacilityFilterList(mSelectedFilterUIList, mCachedDefaultFilterStrList);
            getViewDataBinding().fSearchFilterLayout.fSearchFacilityFilterRecyclerView.setAdapter(newAdapter);
            getViewDataBinding().fSearchFilterLayout.fSearchFacilityFilterRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false));
            getViewDataBinding().fSearchFilterLayout.vSearchFilterDetail.setCallback(this);
        }
    }

    private void changeAllFilterUI(){
        changeLocationFilterUI();
        changeServiceFilterUI();
        changeRatingFilterUI();
    }

    private void changeLocationFilterUI(){
        FilterUI locationFilter = mSelectedFilterUIList.get(location.getPosition());
        locationFilter.filterName = getString(R.string.f_search_location_filter);
        locationFilter.isSelect = false;
        if(mSelectedAddress != null){
            locationFilter.filterName = mSelectedAddress.getAddressFullname();
            locationFilter.isSelect = true;
        }

        notifyFilterUIAdapter(location.getPosition());
    }

    private void changeServiceFilterUI(){
        FilterUI serviceFilter = mSelectedFilterUIList.get(service.getPosition());
        serviceFilter.filterName = getString(R.string.f_search_service_filter);
        serviceFilter.isSelect = false;
        String serviceFilterText = getServiceFilterText(mSelectedServicesFilter);
        if(!CommonUtils.checkNullAndEmpty(serviceFilterText)){
            serviceFilter.isSelect = true;
            serviceFilter.filterName = serviceFilterText;
        }

        checkFilterDetailSelect();
        notifyFilterUIAdapter(service.getPosition());
    }

    private void changeRatingFilterUI(){
        FilterUI filter = mSelectedFilterUIList.get(rating.getPosition());
        filter.filterName = getString(R.string.f_search_rating_filter);
        filter.isSelect = true;
        filter.expose = false;
        if(mSelectedRating > DEFAULT_RATING){
            filter.expose = true;
            filter.filterName = getString(R.string.f_search_rating_filter_select, mSelectedRating);
        }

        checkFilterDetailSelect();
        notifyFilterUIAdapter(rating.getPosition());
    }

    public void initAllFilterList() {
        initFilterUIList();
        initFilterAdapter();
    }

    public void initFilterList(int position) {
        initFilterUIList(position);
        notifyFilterUIAdapter(position);
    }

    private void notifyFilterUIAdapter(){
        FilterUIAdapter adapter = (FilterUIAdapter) getViewDataBinding().fSearchFilterLayout.fSearchFacilityFilterRecyclerView.getAdapter();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    private void notifyFilterUIAdapter(int position){
        FilterUIAdapter adapter = (FilterUIAdapter) getViewDataBinding().fSearchFilterLayout.fSearchFacilityFilterRecyclerView.getAdapter();
        if (adapter != null) {
            adapter.notifyItemChanged(position);
        }
    }

    // 시설 하단 리스트 슬라이딩 버튼 클릭
    @Override
    public void onListLayoutClick() {

    }

    // 필터 리스트 아이템 클릭
    @Override
    public void onClick(int filterPosition) {

        // TODO 프래그먼트 업다운 슬라이딩
        ((MainActivity) getBaseActivity()).showBottomTab(false);
        if(filterPosition == -1 || filterPosition == rating.getPosition()){
            AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_CLICK_FILTER_DETAIL, AmplitudeParam.Key.MODE, getMode().toString());
            BackStackManager.getInstance().addRootFragment(getBaseActivity(), new FilterDetailFragment(), R.id.a_main_content_fragment_layout);
        }
        else if (filterPosition == location.getPosition()) {
            // 지역 필터 화면
            AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_CLICK_FILTER_LOCATION, AmplitudeParam.Key.MODE, getMode().toString());
            AddressFilterFragment filterFragment = new AddressFilterFragment();
            BackStackManager.getInstance().addRootFragment(getBaseActivity(), filterFragment, R.id.a_main_content_fragment_layout);
        }
        else if(filterPosition == service.getPosition()){
            // 등급 필터 화면
            // 선택된 카테고리 Enum과 서비스필터를 넘겨준다.
            AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_CLICK_FILTER_GRADE, AmplitudeParam.Key.MODE, getMode().toString());
            ServiceFilterFragment filterFragment = new ServiceFilterFragment();
            filterFragment.setChangeFilterCallback(this);
            BackStackManager.getInstance().addRootFragment(getBaseActivity(), filterFragment, R.id.a_main_content_fragment_layout);
        }
    }

    @Override
    public void onDelete(int filterPosition) {
        boolean changeLocationFilter = false;
        if(filterPosition == location.getPosition()){
            changeLocationFilter = true;
            initAddressCode();
            AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_REMOVE_FILTER_LOCATION, AmplitudeParam.Key.MODE, getMode().toString());
        }
        else if(filterPosition == service.getPosition()){
            initServicesFilter();
            AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_REMOVE_FILTER_GRADE, AmplitudeParam.Key.MODE, getMode().toString());
        }
        else if(filterPosition == rating.getPosition()){
            initRating();
            AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_REMOVE_FILTER_RATING, AmplitudeParam.Key.MODE, getMode().toString());
        }

        initFilterList(filterPosition);

        loadFacilityFromServer(changeLocationFilter);
    }

    @Override
    public void onSearch(Suggestion suggestion) {
        if(suggestion == null || getBaseActivity() == null){
            return;
        }

        ((MainActivity)getBaseActivity()).onSearch(suggestion);
    }

    // 서치뷰 클릭 시
    @Override
    public void onSearchViewClick() {
        AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_CLICK_SEARCH_KEYWORD, AmplitudeParam.Key.MODE, getMode().toString());

        SearchDialog dialog = SearchDialog.getInstance();
        dialog.setOnSearchListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), SearchDialog.class.getName());
    }

    @Override
    public void onInitSearchKeyword() {
        loadFacilityFromServer();
    }

    public void setSelectCategory(int position){
        // 카테고리 변경
        mSelectedCategory = RealmDataAccessor.getInstance().getStaticCategory().get(position);
    }

    private void onChangeCategory(int position){
        setSelectCategory(position);

        // 카테고리가 선택되면 모든 필터가 초기화 된다.
        initFilter();
    }

    private void initFilter(){
        initAddressCode();
        initServicesFilter();
        initRating();

        initAllFilterList();
    }

    private void initAddressCode(){
        mSelectedAddress = null;
        setAddressCode(null);
    }

    private void initServicesFilter(){
        createServicesFilter();
        setService(mSelectedServicesFilter);
    }

    private void initRating(){
        mSelectedRating = DEFAULT_RATING;
        setRating(mSelectedRating);
    }

    public SearchFilter.SearchFilterBuilder getSearchFilterBuilder() {
        if(mSearchFilterBuilder == null){
            mSearchFilterBuilder = createSearchFilterBuilder();
        }

        return mSearchFilterBuilder;
    }

    private SearchFilter.SearchFilterBuilder createSearchFilterBuilder(){

        return new SearchFilter.SearchFilterBuilder()
                .addAddressCode(getAddressCode(mSelectedAddress))
                .addServices(getServiceGradeFilter(mSelectedServicesFilter))
                .addRating(mSelectedRating);
    }

    public SearchMapFilter.SearchMapFilterBuilder getSearchMapFilterBuilder() {
        if(mSearchMapFilterBuilder == null){
            mSearchMapFilterBuilder = createSearchMapFilterBuilder();
        }

        return mSearchMapFilterBuilder;
    }

    public void setCameraZoomValue(float zoomValue){
        getSearchMapFilterBuilder().addZoom((int)zoomValue);
    }

    public void setSWNE(String sw, String ne){
        getSearchMapFilterBuilder().addSwNe(sw, ne);
    }

    public void setAddressCode(StaticAddress staticAddress){
        getViewDataBinding().fSearchTop.setSearchTerm("");
        if (staticAddress != null) {
            setCameraZoomValue(CAMERA_SIDO_ZOOM_VALUE);
            setSWNE(null, null);
            getViewDataBinding().fSearchTop.setSearchTerm(staticAddress.getAddressFullname());
        }

        getSearchFilterBuilder().setAddressCode(getAddressCode(staticAddress));
        getSearchMapFilterBuilder().setAddressCode(getAddressCode(staticAddress));
    }

    private void setService(List<ServiceFilter> service){
        getSearchFilterBuilder().setServices(getServiceGradeFilter(service));
        getSearchMapFilterBuilder().setServices(getServiceGradeFilter(service));
    }

    private void setRating(Integer rating){
        getSearchFilterBuilder().setRating(rating);
        getSearchMapFilterBuilder().setRating(rating);
    }

    private SearchMapFilter.SearchMapFilterBuilder createSearchMapFilterBuilder() {

        return new SearchMapFilter.SearchMapFilterBuilder()
                .addSearchMode("map")
                .addRating(mSelectedRating)
                .addAddressCode(getAddressCode(mSelectedAddress))
                .addZoom((int) CAMERA_DEFAULT_ZOOM_VALUE)
                .addSwNe(null , null)
                .addServices(getServiceGradeFilter(mSelectedServicesFilter));
    }

    private void createServicesFilter() {
        if (mSelectedServicesFilter != null) {
            mSelectedServicesFilter.clear();
            mSelectedServicesFilter = null;
        }

        mSelectedServicesFilter = new ArrayList<>();

        List<StaticService> staticServices = RealmDataAccessor.getInstance().getStaticService(mSelectedCategory);
        boolean isCheck = true;

        // 서치카테고리가 서비스타입이 없는 경우(전체 카테고리)
        if(mSelectedCategory.getQueryServiceTypeIds() == null ||
                mSelectedCategory.getQueryServiceTypeIds().size() == 0){
            isCheck = false;
        }

        if (staticServices != null) {
            for (StaticService staticService : staticServices) {
                ServiceFilter service = new ServiceFilter();
                service.setId(staticService.getId());
                service.setServiceName(staticService.getTypeName());
                service.setChecked(isCheck);
                // TODO 평가여부 서버에서 받아 와야겠다.
                boolean evaluation = true;
                StaticCategory category = RealmDataAccessor.getInstance().getStaticCategory(staticService.getId());
                if(category == null){
                    evaluation = false;
                }
                /*if(staticService.getServiceType() == StaticService.SERVICE_TYPE.NURSING_HOME_WHO_DEMENTIA ||
                        staticService.getServiceType() == StaticService.SERVICE_TYPE.PROTECT_WHO_DEMENTIA_FOR_DAY_AND_NIGHT){
                    evaluation = false;
                }*/
                service.setEvaluation(evaluation);
                if(evaluation){
                    service.setGradeText(getString(R.string.common_all_grade));
                }
                mSelectedServicesFilter.add(service);
            }
        }
    }

    private List<SearchFilter.Service> getServiceGradeFilter(List<ServiceFilter> serviceFilters) {
        List<SearchFilter.Service> services = new ArrayList<>();

        if(serviceFilters != null){
            for (ServiceFilter serviceFilter : serviceFilters) {
                if (serviceFilter.isChecked()) {
                    SearchFilter.Service service = new SearchFilter.Service();
                    service.setId(serviceFilter.getId());
                    if(serviceFilter.isEvaluation()){
                        service.setGrades(serviceFilter.getGrades());
                    }
                    services.add(service);
                }
            }
        }


        return services;
    }

    public String getAddressCode(StaticAddress address) {
        String addressCode = null;
        if(address != null){
            addressCode = "";
            if (address.getSidoCode() != null && !address.getSidoCode().equals("0")) {
                addressCode += address.getSidoCode();
            }
            if (address.getGugunCode() != null && !address.getGugunCode().equals("0")) {
                addressCode += address.getGugunCode();
            }
            if (address.getDongCode() != null && !address.getDongCode().equals("0")) {
                addressCode += address.getDongCode();
            }
        }
        return addressCode;
    }

    @Override
    public void onChangedFilter(boolean changedLocationFilter) {
        if(!changedLocationFilter){
            setService(mSelectedServicesFilter);
            setRating(mSelectedRating);
        }

        setAddressCode(mSelectedAddress);

        changeAllFilterUI();

        loadFacilityFromServer(changedLocationFilter);
    }

    private String getServiceFilterText(final List<ServiceFilter> serviceList){
        String filterText = null;
        int checkedCount = 0;
        for(ServiceFilter filter : serviceList){
            if(filter.isChecked()){
                if(checkedCount == 0){
                    if(filter.getGradeText() != null){
                        filterText = getString(R.string.f_search_service_filter_simple, filter.getServiceName(), filter.getGradeText());
                    }
                    else{
                        filterText = filter.getServiceName();
                    }
                }
                checkedCount++;
            }

        }

        if(checkedCount > 1){
            filterText = getString(R.string.f_search_service_filter_simple_more, filterText, checkedCount - 1);
        }

        return filterText;
    }

    // 이미 SearchFragment가 Hide상태에서 Show상태가 될 때
    @Override
    public void onShow() {
        super.onShow();

        if(getBaseActivity() instanceof MainActivity){
            ((MainActivity) getBaseActivity()).showBottomTab(true);
            UIUtils.setStatusBarColor(getBaseActivity(), getView(), getResources().getColor(R.color.colorPrimary), false);
        }

        if(getViewModel() != null){
            Bundle bundle = getArguments();
            if(bundle != null){
                mIgnoreSpinnerSelected = false;
                getViewDataBinding().fSearchTop.fSearchCategorySpinner.setSelection(0);
                onChangeCategory(0);

                handleBundle(getArguments());

                changeAllFilterUI();

                loadFacilityFromServer();

                deleteArguments();
            }

            BackStack backStack = BackStackManager.getInstance().getCurrentBackStack();
            if(backStack != null && backStack.getStackItems().size() == 0){
                BackStackManager.getInstance().addChildFragment(new SearchListFragment(), R.id.f_search_child_fragment_layout);
            }
        }
    }

    private void handleBundle(Bundle bundle) {
        if (bundle != null) {
            StaticCategory category = bundle.getParcelable(SEARCH_CATEGORY_KEY);
            if(category != null){
                int position = 0;
                for(StaticCategory staticCategory : RealmDataAccessor.getInstance().getStaticCategory()){
                    if(staticCategory.getId() == category.getId()){
                        break;
                    }
                    position++;
                }
                onChangeCategory(position);
                getViewDataBinding().fSearchTop.fSearchCategorySpinner.setSelection(position);
            }

            StaticAddress address = bundle.getParcelable(ADDRESS_KEY);
            if(address != null){
                mSelectedAddress = address;
                setAddressCode(address);
            }
        }
    }

    private void setServiceFilter(StaticService service) {
        if (service != null) {
            for (ServiceFilter filter : mSelectedServicesFilter) {
                filter.setChecked(false);
            }

            for (ServiceFilter filter : mSelectedServicesFilter) {
                if (filter.getId() == service.getId()) {
                    filter.setChecked(true);
                    break;
                }
            }
        }
    }

    private void deleteArguments() {
        if(!isStateSaved()){
            setArguments(null);
        }
    }

    public void initFilterUIList() {
        mSelectedFilterUIList.clear();

        for (int i = 0; i<FILTER_UI.values().length; i++) {
            FilterUI filterUI = new FilterUI();
            mSelectedFilterUIList.add(filterUI);
        }

        for (FILTER_UI filter : FILTER_UI.values()) {
            initFilterUIList(filter.getPosition());
        }
    }

    private void initDefaultFilterStrList(){
        if(mCachedDefaultFilterStrList == null){
            mCachedDefaultFilterStrList = new String[mSelectedFilterUIList.size()];
            for(int i=0; i<mSelectedFilterUIList.size(); i++){
                mCachedDefaultFilterStrList[i] = mSelectedFilterUIList.get(i).filterName;
            }
        }
    }

    private void initFilterUIList(int position){
        FilterUI filterUI = mSelectedFilterUIList.get(position);
        FILTER_UI filter = location;
        for (FILTER_UI _filterUI : FILTER_UI.values()) {
            if(_filterUI.getPosition() == position){
                filter = _filterUI;
                break;
            }
        }

        filterUI.filterName = CommonUtils.getFilterName(getBaseActivity(), filter);
        filterUI.isSelect = false;
        filterUI.expose = filter.isExpose();

        checkFilterDetailSelect();
    }

    private void checkFilterDetailSelect(){
        FilterUI serviceFilter = mSelectedFilterUIList.get(service.getPosition());
        FilterUI ratingFilter = mSelectedFilterUIList.get(rating.getPosition());
        boolean select = serviceFilter.isSelect | ratingFilter.expose;

        getViewDataBinding().fSearchFilterLayout.vSearchFilterDetail.setIsSelect(select);
    }

    private void loadFacilityFromServer(){
        loadFacilityFromServer(false);
    }

    private void loadFacilityFromServer(boolean changedLocationFilter){
        BackStack backStack = BackStackManager.getInstance().getCurrentBackStack();
        if(backStack.getRootFragment() != this){
            return;
        }

        for(String child : backStack.getStackItems()){
            Fragment fragment = backStack.mFragmentManager.findFragmentByTag(child);
            if(fragment instanceof SearchListFragment){
                ((SearchListFragment)fragment).loadFacilityFromServer();
            }
            else if(fragment instanceof SearchMapFragment){
                ((SearchMapFragment)fragment).setChangedLocationFilter(changedLocationFilter);
                ((SearchMapFragment)fragment).loadFacilitySearchClusterFromServer();
            }
        }
    }

    public AmplitudeParam.MODE getMode(){
        AmplitudeParam.MODE mode = AmplitudeParam.MODE.LIST;
        Fragment fragment = BackStackManager.getInstance().getCurrentFragment();
        if(fragment instanceof SearchMapFragment){
            mode = AmplitudeParam.MODE.MAP;
        }
        return  mode;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(!mIgnoreSpinnerSelected){
            mIgnoreSpinnerSelected = true;
            return;
        }

        if(RealmDataAccessor.getInstance().getStaticCategory() != null){
            StaticCategory category = RealmDataAccessor.getInstance().getStaticCategory().get(position);
            Map<String, MapValue<?>> map = new HashMap<>();
            map.put(AmplitudeParam.Key.MODE, new MapValue<>(getMode().toString()));
            map.put(AmplitudeParam.Key.VALUE, new MapValue<>(category.getText()));
            AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_CLICK_CATEGORY, map);
        }

        onChangeCategory(position);
        loadFacilityFromServer();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
