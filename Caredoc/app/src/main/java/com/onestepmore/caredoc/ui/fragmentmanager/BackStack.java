package com.onestepmore.caredoc.ui.fragmentmanager;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.onestepmore.caredoc.ui.base.BaseActivity;

import java.util.LinkedList;

public class BackStack {

    //region Members
    public final BaseActivity mRootActivity;
    public final BackStackFragmentManager mFragmentManager;
    public final Fragment mRootFragment;
    final LinkedList<String> mStackItems;
    private boolean mAlwaysShowRoot = false;
    //endregion


    //region Constructors
    public BackStack(@NonNull final BaseActivity rootActivity, @NonNull BackStackFragmentManager fragmentManager, @NonNull final Fragment rootFragment) {
        mRootActivity = rootActivity;
        mFragmentManager = fragmentManager;
        mRootFragment = rootFragment;
        mStackItems = new LinkedList<>();
        mAlwaysShowRoot = false;
    }

    public BackStack(@NonNull final BaseActivity rootActivity,
                     @NonNull BackStackFragmentManager fragmentManager,
                     @NonNull final Fragment rootFragment,
                     boolean alwaysShowRoot) {
        mRootActivity = rootActivity;
        mFragmentManager = fragmentManager;
        mRootFragment = rootFragment;
        mStackItems = new LinkedList<>();
        mAlwaysShowRoot = alwaysShowRoot;
    }

    //endregion


    //region Methods
    public String pop() {
        if (isEmpty()) return null;
        return mStackItems.pop();
    }

    public void push(@NonNull final String id) {
        mStackItems.push(id);
    }

    public void removeChildAll(){
        mStackItems.clear();
    }

    public boolean isEmpty() {
        return mStackItems.isEmpty();
    }

    public String getCurrentStackId(){
        String uuid = null;
        if(!mStackItems.isEmpty()){
            uuid = mStackItems.getLast();
        }
        return uuid;
    }

    public LinkedList<String> getStackItems() {
        return mStackItems;
    }

    public Fragment getRootFragment() {
        return mRootFragment;
    }

    public boolean isAlwaysShowRoot() {
        return mAlwaysShowRoot;
    }

    public void setAlwaysShowRoot(boolean alwaysShowRoot) {
        this.mAlwaysShowRoot = alwaysShowRoot;
    }

    //endregion
}
