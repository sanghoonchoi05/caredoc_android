package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.FacilityCommentResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceSummaryResponse;
import com.onestepmore.caredoc.data.model.api.review.CommentRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.CommentSaveRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewSaveRequest;

import org.json.JSONObject;

import io.reactivex.Flowable;

public interface ReviewApiHelper {
    Flowable<ReviewFacilityServiceSummaryResponse> getReviewServiceSummaryApiCall(ReviewFacilityRequest.PathParameter pathParameter);
    Flowable<ReviewFacilityServiceResponse> getReviewServiceApiCall(ReviewFacilityRequest.PathParameter pathParameter);
    Flowable<FacilityCommentResponse> getCommentApiCall(ReviewFacilityRequest.PathParameter pathParameter);

    void saveCommentApiCall(CommentSaveRequest request, CommentSaveRequest.PathParameter parameter, OkHttpResponseListener listener);
    void removeCommentApiCall(CommentRemoveRequest.PathParameter parameter, OkHttpResponseListener listener);

    void saveReviewApiCall(JSONObject jsonRequest, ReviewSaveRequest.PathParameter parameter, OkHttpResponseListener listener);
    void removeReviewApiCall(ReviewRemoveRequest.PathParameter parameter, OkHttpResponseListener listener);
}
