package com.onestepmore.caredoc.ui.main.detail;

import android.view.View;

import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailEntranceResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailMedicalResponse;
import com.onestepmore.caredoc.data.model.api.object.FacilityDetailObject;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceResponse;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface FacilityDetailNavigator extends BaseNavigator {
    void onClose();
    void onShowPhoneDial();
    void onShowHomepage();
    void onShare();
    void onChangeBookmark();
    void onSaveFailedForGuest();
    void onWriteComment();
    void onRemoveComment(long id);
    void onShowTooltip(View anchorView, String title, String text);

    void onShowReview(long serviceFakeId);
    void onWriteReview(long serviceFakeId, int serviceId);
    void onRemoveReview(long serviceFakeId);

    void onShowGoogleMap();

    void onLoadHospitalDetail(FacilityDetailMedicalResponse response);
    void onLoadHomeCareDetail(FacilityDetailEntranceResponse response);

    void onPrevPhoto();
    void onNextPhoto();
}
