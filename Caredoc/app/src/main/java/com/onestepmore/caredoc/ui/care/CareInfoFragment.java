package com.onestepmore.caredoc.ui.care;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.databinding.FCareInfoBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;

import javax.inject.Inject;
import javax.inject.Named;

public class CareInfoFragment extends BaseFragment<FCareInfoBinding, CareInfoViewModel> implements CareInfoNavigator {
    @Inject
    @Named("CareInfoFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private CareInfoViewModel mCareInfoViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_care_info;
    }

    @Override
    public CareInfoViewModel getViewModel() {
        mCareInfoViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CareInfoViewModel.class);
        return mCareInfoViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCareInfoViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fCareInfoToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }

        User user = getViewModel().getRealm().where(User.class).findFirst();
        if(user != null){
            getViewDataBinding().fCareInfoEmailEdit.setText(user.getEmail());
            if(user.getContact() != null){
                String text = getViewDataBinding().fCareInfoTextInput.getText().toString();
                text += user.getContact();
                getViewDataBinding().fCareInfoTextInput.setText(text);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BackStackManager.getInstance().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSendMail() {
        String email = getViewDataBinding().fCareInfoEmailEdit.getText().toString().trim();
        String content = getViewDataBinding().fCareInfoTextInput.getText().toString().trim();
        if(content.length() == 0 || email.length() == 0) {
            showCommonDialog(getString(R.string.warning_msg_care_info_failed), getString(R.string.warning_msg_care_info_invalid_content));
            return;
        }

        String subject = getString(R.string.f_care_info_title);
        String name = "";

        User user = getViewModel().getRealm().where(User.class).findFirst();
        if(user != null){
            name = user.getName();
        }

        getViewModel().sendEmail(
                name + " <" + email + ">",
                "care@caredoc.kr",
                subject,
                content
        );
    }

    @Override
    public void onSuccessSendMail() {
        showCommonDialog(
                getString(R.string.f_care_info_succeed_title),
                getString(R.string.f_care_info_succeed_desc),
                "common_dialog",
                (view) -> BackStackManager.getInstance().onBackPressed(),
                getString(R.string.common_confirm),
                "");
    }
}
