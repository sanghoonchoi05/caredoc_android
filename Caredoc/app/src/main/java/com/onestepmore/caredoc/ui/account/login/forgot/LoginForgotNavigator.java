package com.onestepmore.caredoc.ui.account.login.forgot;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface LoginForgotNavigator extends BaseNavigator {
    void onComplete();
    void invalidEmail();
}
