package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;

import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class LatLngAddress implements LatLngRealmModel {
    @PrimaryKey
    private String id;
    private double latitude;
    private double longitude;

    public LatLngAddress() {
    }

    protected LatLngAddress(Parcel in) {
        id = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<LatLngAddress> CREATOR = new Creator<LatLngAddress>() {
        @Override
        public LatLngAddress createFromParcel(Parcel in) {
            return new LatLngAddress(in);
        }

        @Override
        public LatLngAddress[] newArray(int size) {
            return new LatLngAddress[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @Override
    public double getLatitude() {
        return latitude;
    }
    @Override
    public double getLongitude() {
        return longitude;
    }
    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    @Override
    public String getId() {
        return id;
    }
    @Override
    public void setId(String id) {
        this.id = id;
        ;
    }
}
