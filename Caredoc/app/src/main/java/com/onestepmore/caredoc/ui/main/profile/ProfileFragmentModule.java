package com.onestepmore.caredoc.ui.main.profile;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ProfileFragmentModule {
    @Provides
    ProfileViewModel profileViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new ProfileViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("ProfileFragment")
    ViewModelProvider.Factory provideProfileViewModelProvider(ProfileViewModel profileViewModel) {
        return new ViewModelProviderFactory<>(profileViewModel);
    }
}
