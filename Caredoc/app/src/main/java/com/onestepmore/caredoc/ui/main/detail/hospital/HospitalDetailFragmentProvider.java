package com.onestepmore.caredoc.ui.main.detail.hospital;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HospitalDetailFragmentProvider {
    @ContributesAndroidInjector(modules = HospitalDetailFragmentModule.class)
    abstract HospitalDetailFragment provideHospitalDetailFragmentFactory();
}
