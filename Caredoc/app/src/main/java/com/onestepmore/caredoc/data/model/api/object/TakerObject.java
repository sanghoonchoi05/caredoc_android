package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.realm.Protector;
import com.onestepmore.caredoc.data.model.realm.ServiceAddress;
import com.onestepmore.caredoc.data.model.realm.statics.StaticDisease;

import java.util.Date;

import io.realm.RealmList;

public class TakerObject {
    @Expose
    @SerializedName("fake_id")
    private long fakeId;

    @Expose
    @SerializedName("photo_url")
    private String photoUrl;

    @Expose
    @SerializedName("relation")
    private String relation;

    @Expose
    @SerializedName("relation_str")
    private String relationStr;

    @Expose
    @SerializedName("religion")
    private String religion;

    @Expose
    @SerializedName("religion_str")
    private String religionStr;

    @Expose
    @SerializedName("blood_type")
    private String bloodType;

    @Expose
    @SerializedName("is_blhsr")
    private boolean isBlhsr;  // 기초생활수급여부

    @Expose
    @SerializedName("live_with")
    private String liveWith;

    @Expose
    @SerializedName("live_with_str")
    private String liveWithStr;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("birth")
    private String birth;

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("address_code")
    private String addressCode;

    @Expose
    @SerializedName("address_detail")
    private String addressDetail;

    @Expose
    @SerializedName("height")
    private String height;

    @Expose
    @SerializedName("weight")
    private String weight;

    @Expose
    @SerializedName("lti_number")
    private String ltiNumber;

    @Expose
    @SerializedName("lti_grade")
    private String ltiGrade;

    @Expose
    @SerializedName("performance_meal")
    private String performanceMeal;

    @Expose
    @SerializedName("performance_meal_str")
    private String performanceMealStr;

    @Expose
    @SerializedName("performance_walk")
    private String performanceWalk;

    @Expose
    @SerializedName("performance_walk_str")
    private String performanceWalkStr;

    @Expose
    @SerializedName("performance_wash")
    private String performanceWash;

    @Expose
    @SerializedName("performance_wash_str")
    private String performanceWashStr;

    @Expose
    @SerializedName("performance_cloth")
    private String performanceCloth;

    @Expose
    @SerializedName("performance_cloth_str")
    private String performanceClothStr;

    @Expose
    @SerializedName("performance_bath")
    private String performanceBath;

    @Expose
    @SerializedName("performance_bath_str")
    private String performanceBathStr;

    @Expose
    @SerializedName("performance_toilet")
    private String performanceToilet;

    @Expose
    @SerializedName("performance_toilet_str")
    private String performanceToiletStr;

    @Expose
    @SerializedName("nutrition_favorite")
    private String nutritionFavorite;

    @Expose
    @SerializedName("nutrition_hate")
    private String nutritionHate;

    @Expose
    @SerializedName("nutrition_allergy")
    private String nutritionAllergy;

    @Expose
    @SerializedName("nutrition_is_diabetic")
    private boolean nutritionIsDiabetic;

    @Expose
    @SerializedName("remark_habit")
    private String remarkHabit;

    @Expose
    @SerializedName("remark_tendency")
    private String remarkTendency;

    @Expose
    @SerializedName("remark_tendency_str")
    private String remarkTendencyStr;

    @Expose
    @SerializedName("remark_speak")
    private String remarkSpeak;

    @Expose
    @SerializedName("remark_speak_str")
    private String remarkSpeakStr;

    @Expose
    @SerializedName("service_addresses")
    private ServiceAddressObject serviceAddressObject;

    @Expose
    @SerializedName("is_own")
    private boolean isOwn;

    @Expose
    @SerializedName("created_at")
    private String createdAt;

    public long getFakeId() {
        return fakeId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getRelation() {
        return relation;
    }

    public String getReligion() {
        return religion;
    }

    public String getBloodType() {
        return bloodType;
    }

    public boolean isBlhsr() {
        return isBlhsr;
    }

    public String getName() {
        return name;
    }

    public String getBirth() {
        return birth;
    }

    public String getAddress() {
        return address;
    }

    public String getHeight() {
        return height;
    }

    public String getWeight() {
        return weight;
    }

    public String getLtiNumber() {
        return ltiNumber;
    }

    public String getLtiGrade() {
        return ltiGrade;
    }

    public String getPerformanceMeal() {
        return performanceMeal;
    }

    public String getPerformanceWalk() {
        return performanceWalk;
    }

    public String getPerformanceWash() {
        return performanceWash;
    }

    public String getPerformanceCloth() {
        return performanceCloth;
    }

    public String getPerformanceBath() {
        return performanceBath;
    }

    public String getPerformanceToilet() {
        return performanceToilet;
    }

    public String getNutritionFavorite() {
        return nutritionFavorite;
    }

    public String getNutritionHate() {
        return nutritionHate;
    }

    public String getNutritionAllergy() {
        return nutritionAllergy;
    }

    public boolean isNutritionIsDiabetic() {
        return nutritionIsDiabetic;
    }

    public String getRemarkHabit() {
        return remarkHabit;
    }

    public String getRemarkTendency() {
        return remarkTendency;
    }

    public String getRemarkSpeak() {
        return remarkSpeak;
    }

    public ServiceAddressObject getServiceAddressObject() {
        return serviceAddressObject;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getLiveWith() {
        return liveWith;
    }

    public String getPerformanceMealStr() {
        return performanceMealStr;
    }

    public String getPerformanceWalkStr() {
        return performanceWalkStr;
    }

    public String getPerformanceWashStr() {
        return performanceWashStr;
    }

    public String getPerformanceClothStr() {
        return performanceClothStr;
    }

    public String getPerformanceBathStr() {
        return performanceBathStr;
    }

    public String getPerformanceToiletStr() {
        return performanceToiletStr;
    }

    public String getRemarkTendencyStr() {
        return remarkTendencyStr;
    }

    public String getRemarkSpeakStr() {
        return remarkSpeakStr;
    }

    public String getRelationStr() {
        return relationStr;
    }

    public String getReligionStr() {
        return religionStr;
    }

    public String getLiveWithStr() {
        return liveWithStr;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public String getAddressDetail() {
        return addressDetail;
    }
}
