/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.ui.base;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.main.detail.review.write.WriteReviewActivity;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.UIUtils;

import dagger.android.support.AndroidSupportInjection;

public abstract class BaseFragment<T extends ViewDataBinding, V extends BaseViewModel> extends Fragment implements
        TransactionCallback, BaseNavigator
{

    public interface OnBackPressedListener{
        void doBack();
    }
    private BaseActivity mActivity;
    private View mRootView;
    private T mViewDataBinding;
    private V mViewModel;
    private Toolbar mToolbar;

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    public void setSupportToolBar(Toolbar toolbar) {
        this.mToolbar = toolbar;
        this.mActivity.setSupportActionBar(toolbar);
    }

    public Toolbar getSupportToolBar(){
        return mToolbar;
    }

    public boolean isSupportToolBar(){
        return mToolbar != null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.mActivity = activity;
            activity.onFragmentAttached();
        }
    }

    @Override
    public void startActivity(Intent intent) {
        if(checkLoginService(intent) && checkNeedLogin()){
            showNeedLoginPopup();
            return;
        }

        getBaseActivity().setTouchBlock(true);
        super.startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if(checkLoginService(intent) && checkNeedLogin()){
            showNeedLoginPopup();
            return;
        }

        getBaseActivity().setTouchBlock(true);
        super.startActivityForResult(intent, requestCode);
    }

    private boolean checkLoginService(Intent intent){
        if(intent == null){
            return false;
        }

        /*if(intent.getComponent() != null && intent.getComponent().getClassName().equals(WriteReviewActivity.class.getName())){
            return true;
        }*/

        return false;
    }

    @Override
    public void showNeedLoginPopup(){
        showNeedLoginPopup(this::onYes);
    }

    @Override
    public void showNeedLoginPopup(View.OnClickListener listener){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.Screen.NEED_LOGIN_POPUP);
        CommonYNDialog dialogV120 = CommonYNDialog.newInstance(
                getString(R.string.warning_msg_need_login_title),
                getString(R.string.warning_msg_need_login_desc),
                getString(R.string.common_return),
                getString(R.string.common_start)
        );
        dialogV120.setOnOkClickListener(listener);
        dialogV120.show(getFragmentManager(), null);
    }

    public void onYes(View view){
        gotoAccountMainActivity();
    }

    private boolean checkNeedLogin(){
        return !getViewModel().getDataManager().isLoggedIn();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        mViewModel = getViewModel();
        setHasOptionsMenu(false);

        getBaseActivity().setLoadingObserver(getViewModel().getIsLoading());
    }

    @Override
    public void onResume() {
        super.onResume();

        FirebaseManager.getInstance().logEvent(this);
        AmplitudeManager.getInstance().logEvent(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        mRootView = mViewDataBinding.getRoot();
        return mRootView;
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
        mViewDataBinding.setLifecycleOwner(getViewLifecycleOwner());

        UIUtils.setStatusBarColor(getBaseActivity(), getView(), Color.WHITE, true);
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    public void hideKeyboard() {
        if (mActivity != null) {
            mActivity.hideKeyboard();
        }
    }

    public boolean isNetworkConnected() {
        return mActivity != null && mActivity.isNetworkConnected();
    }

    public void openActivityOnTokenExpire() {
        if (mActivity != null) {
            mActivity.openActivityOnTokenExpire();
        }
    }

    private void performDependencyInjection() {
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onHide() {

    }

    @Override
    public void onShow() {
        UIUtils.setStatusBarColor(getBaseActivity(), getView(), Color.WHITE, true);
    }

    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }

    public void showCommonDialog(String title, String desc){
        showCommonDialog(title, desc, "common_dialog", null, getString(R.string.common_confirm), getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag){
        showCommonDialog(title, desc, tag, null, getString(R.string.common_confirm), getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener){
        showCommonDialog(title, desc, tag, listener, getString(R.string.common_confirm), getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener, String okText){
        showCommonDialog(title, desc, tag, listener, okText, getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener, String okText, String cancelText){
        showCommonDialog(title, desc, tag, listener, okText, cancelText, false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener, String okText, String cancelText, boolean blockBackButton){
        CommonYNDialog dialog = CommonYNDialog.newInstance(
                title,
                desc,
                cancelText,
                okText
        );
        dialog.setCancelable(!blockBackButton);
        dialog.setOnOkClickListener(listener);
        dialog.show(getFragmentManager(), tag);

    }

    @Override
    public void gotoAccountMainActivity(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.NEED_LOGIN_POPUP_CLICK_START);
        Intent intent = getBaseActivity().newIntent(AccountMainActivity.class);
        startActivity(intent);
    }

    @Override
    public void handleError(Throwable throwable) {
        String msg = throwable.toString();
        if(!isNetworkConnected()){
            msg = getString(R.string.warning_msg_network_failed);
        }
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), msg, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        if(!isNetworkConnected()){
            msg = getString(R.string.warning_msg_network_failed);
        }
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }
}
