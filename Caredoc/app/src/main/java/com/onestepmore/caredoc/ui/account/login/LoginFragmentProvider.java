package com.onestepmore.caredoc.ui.account.login;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LoginFragmentProvider {
    @ContributesAndroidInjector(modules = LoginFragmentModule.class)
    abstract LoginFragment provideLoginFragmentFactory();
}
