package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class RatingRelationsObject extends ItemResponse<RatingRelationsObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("id")
        private int id;

        @Expose
        @SerializedName("label")
        private String label;

        public String getLabel() {
            return label;
        }

        public int getId() {
            return id;
        }
    }
}
