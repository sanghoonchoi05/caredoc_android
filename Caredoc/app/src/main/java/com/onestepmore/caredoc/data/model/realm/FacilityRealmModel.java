package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcelable;

import io.realm.RealmList;
import io.realm.RealmModel;

public interface FacilityRealmModel extends RealmModel, ModelDiff, Parcelable {
    String getName();
    void setName(String name);

    String getAddress();
    void setAddress(String address);

    String getRoadAddress();
    void setRoadAddress(String roadAddress);

    String getHomepage();
    void setHomepage(String homepage);

    String getContact();
    void setContact(String contact);

    LatLngFacility getLatLng();
    void setLatLng(LatLngFacility latLng);

    String getThumbnail();
    void setThumbnail(String thumbnail);

    float getRatingAvg();
    void setRatingAvg(float ratingAvg);

    RealmList<Service> getServices();
    void setServices(RealmList<Service> services);

    RealmList<Attach> getAttaches();
    void setAttaches(RealmList<Attach> attaches);

    Long getFakeId();
    void setFakeId(Long fakeId);

    int getReviewCount();
    void setReviewCount(int reviewCount);

    boolean isBookMarked();
    void setBookmarked(boolean bookmarked);

    void deleteRealm(FacilityRealmModel realmModel);
}
