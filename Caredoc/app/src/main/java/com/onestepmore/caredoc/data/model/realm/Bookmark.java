package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.BookmarkObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceSimpleObject;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Bookmark extends RealmObject implements Parcelable {
    @PrimaryKey
    private long fakeId;
    private RealmList<Integer> serviceTypeIds;
    private RealmList<String> searchCategoryList;
    private boolean show;

    public Bookmark(){}

    protected Bookmark(Parcel in) {
        fakeId = in.readLong();
        this.serviceTypeIds = new RealmList<>();
        in.readList(this.serviceTypeIds, Integer.class.getClassLoader());
        this.searchCategoryList = new RealmList<>();
        in.readList(this.searchCategoryList, String.class.getClassLoader());
        show = in.readByte() != 0;
    }

    public static final Creator<Bookmark> CREATOR = new Creator<Bookmark>() {
        @Override
        public Bookmark createFromParcel(Parcel in) {
            return new Bookmark(in);
        }

        @Override
        public Bookmark[] newArray(int size) {
            return new Bookmark[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(fakeId);
        dest.writeList(serviceTypeIds);
        dest.writeList(searchCategoryList);
        dest.writeByte((byte)(show ? 1 : 0));
    }

    public void setBookmark(BookmarkObject object){
        this.fakeId = object.getBookmarkable().getFakeId();
        this.serviceTypeIds = new RealmList<>();
        this.searchCategoryList = new RealmList<>();
        for(ServiceSimpleObject item : object.getBookmarkable().getServices().getItems()){
            serviceTypeIds.add(item.getServiceId());
            StaticCategory category = RealmDataAccessor.getInstance().getStaticCategory(item.getServiceId());
            if(category != null && !searchCategoryList.contains(category.getText())){
                searchCategoryList.add(category.getText());
            }
        }

        this.show = searchCategoryList.size() > 0;
    }

    public long getFakeId() {
        return fakeId;
    }

    public RealmList<String> getSearchCategoryList() {
        return searchCategoryList;
    }

    public void setFakeId(long fakeId) {
        this.fakeId = fakeId;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }
}
