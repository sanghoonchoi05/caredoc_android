/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.ui.base;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ViewDataBinding;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackHelperListener;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.detail.review.write.WriteReviewActivity;
import com.onestepmore.caredoc.ui.support.rx.SupportRxObserver;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.NetworkUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import dagger.android.AndroidInjection;
import rx.Observable;

public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity
        implements BaseFragment.Callback, BackStackHelperListener, BaseNavigator {

    private static final int CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE = 100;

    public interface OnBaseActivityListener{
        void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

    // TODO
    // this can probably depend on isLoading variable of BaseViewModel,
    // since its going to be common for all the activities
    private ProgressDialog mProgressDialog;
    private T mViewDataBinding;
    private V mViewModel;

    private boolean touchBlock = false;

    void setTouchBlock(boolean block){
        touchBlock = block;
    }

    public boolean isTouchBlock() {
        return touchBlock;
    }

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    public abstract int getBindingVariable();

    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        performDependencyInjection();
        super.onCreate(savedInstanceState);
        performDataBinding();

        UIUtils.setStatusBarColor(this, getWindow().getDecorView(), Color.WHITE, true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTouchBlock(false);

        FirebaseManager.getInstance().setCurrentScreen(this);
        FirebaseManager.getInstance().logEvent(this);

        AmplitudeManager.getInstance().logEvent(this);
    }

    public T getViewDataBinding() {
        return mViewDataBinding;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    public void setLoadingObserver(ObservableBoolean loadingObservable){
        Observable<Boolean> observer = SupportRxObserver.observableBooleanChanges(loadingObservable);
        getViewModel().setLoadingObserver(observer);
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    public void openActivityOnTokenExpire() {
        //startActivity(Login1Activity.newIntent(this));
        finish();
    }

    public void performDependencyInjection() {
        AndroidInjection.inject(this);
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    public void showLoading() {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        this.mViewModel = mViewModel == null ? getViewModel() : mViewModel;
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
        mViewDataBinding.setLifecycleOwner(this);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Fragment fragment = BackStackManager.getInstance().getCurrentFragment();
        if(fragment!= null && fragment instanceof OnBaseActivityListener){
            OnBaseActivityListener listener = (OnBaseActivityListener)fragment;
            listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = BackStackManager.getInstance().getCurrentFragment();
        if(fragment instanceof OnBaseActivityListener){
            OnBaseActivityListener listener = (OnBaseActivityListener)fragment;
            listener.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(isTouchBlock() || getViewModel().getIsLoading().get())
            return false;

        return super.dispatchTouchEvent(ev);
    }

    public Intent newIntent(Class<? extends BaseActivity> clazz) {
        return new Intent(this, clazz);
    }

    @Override
    public void startActivity(Intent intent) {
        if(checkLoginService(intent) && checkNeedLogin()){
            showNeedLoginPopup();
            return;
        }
        super.startActivity(intent);
        setTouchBlock(true);
    }

    private boolean checkLoginService(Intent intent){
        if(intent == null){
            return true;
        }

        if(intent.getComponent() != null && intent.getComponent().getClassName().equals(WriteReviewActivity.class.getName())){
            return true;
        }

        return false;
    }

    private boolean checkNeedLogin(){
        return !getViewModel().getDataManager().isLoggedIn();
    }

    @Override
    public void showNeedLoginPopup(){
        showNeedLoginPopup(this::onYes);
    }

    @Override
    public void showNeedLoginPopup(View.OnClickListener listener){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.Screen.NEED_LOGIN_POPUP);
        CommonYNDialog dialog = CommonYNDialog.newInstance(
                getString(R.string.warning_msg_need_login_title),
                getString(R.string.warning_msg_need_login_desc),
                getString(R.string.common_return),
                getString(R.string.common_start)
        );
        dialog.setOnOkClickListener(listener);
        dialog.show(getSupportFragmentManager(), null);
    }

    public void onYes(View view){
        gotoAccountMainActivity();
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if(checkLoginService(intent) && checkNeedLogin()){
            showNeedLoginPopup();
            return;
        }

        super.startActivityForResult(intent, requestCode);
        setTouchBlock(true);
    }

    public void startActivityForResult(Intent intent, int requestCode, Fragment fragment) {
        fragment.startActivityForResult(intent, requestCode);

        setTouchBlock(true);
    }

    @Override
    public void onBackPressed() {
        if(getViewModel().getIsLoading().get()){
            getViewModel().setIsLoading(false);
        }else{
            Fragment curFragment = BackStackManager.getInstance().getCurrentFragment();
            if(curFragment != null && curFragment.getActivity() == this){
                AmplitudeManager.getInstance().logEventForBackKey((BaseFragment)curFragment);
                if(curFragment instanceof BaseFragment.OnBackPressedListener){
                    ((BaseFragment.OnBackPressedListener)curFragment).doBack();
                }
                else if(curFragment instanceof BaseDialog.OnBackPressedListener){
                    ((BaseDialog.OnBackPressedListener)curFragment).doBack();
                }
                else{
                    BackStackManager.getInstance().onBackPressed();
                }
            }else{
                AmplitudeManager.getInstance().logEventForBackKey(this);
                finish();
            }
        }
    }

    public void refreshActivity() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    public void showCommonDialog(String title, String desc){
        showCommonDialog(title, desc, "common_dialog", null, getString(R.string.common_confirm), getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag){
        showCommonDialog(title, desc, tag, null, getString(R.string.common_confirm), getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener){
        showCommonDialog(title, desc, tag, listener, getString(R.string.common_confirm), getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener, String okText){
        showCommonDialog(title, desc, tag, listener, okText, getString(R.string.common_cancel), false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener, String okText, String cancelText){
        showCommonDialog(title, desc, tag, listener, okText, cancelText, false);
    }

    public void showCommonDialog(String title, String desc, String tag, View.OnClickListener listener, String okText, String cancelText, boolean blockBackButton){
        CommonYNDialog dialog = CommonYNDialog.newInstance(
                title,
                desc,
                cancelText,
                okText
        );
        dialog.setCancelable(!blockBackButton);
        dialog.setOnOkClickListener(listener);
        dialog.show(getSupportFragmentManager(), tag);

    }

    public boolean checkPhotoPermissions() {
        boolean check = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;

        if(check){
            String[] neededPermissions = getNeededPhotoPermissions();
            ActivityCompat.requestPermissions(this, neededPermissions, CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        }
        return check;
    }

    private String[] getNeededPhotoPermissions() {
        return new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE};
    }

    @Override
    public void gotoAccountMainActivity(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.NEED_LOGIN_POPUP_CLICK_START);
        Intent intent = newIntent(AccountMainActivity.class);
        startActivity(intent);
    }

    @Override
    public void handleError(Throwable throwable) {
        String msg = throwable.toString();
        if(!isNetworkConnected()){
            msg = getString(R.string.warning_msg_network_failed);
        }
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), msg, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        if(!isNetworkConnected()){
            msg = getString(R.string.warning_msg_network_failed);
        }
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }
}

