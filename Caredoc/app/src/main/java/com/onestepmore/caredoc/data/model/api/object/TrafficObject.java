package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TrafficObject {
    @Expose
    @SerializedName("order")
    private int order;

    @Expose
    @SerializedName("transportation_name")
    private String transportationName;

    @Expose
    @SerializedName("line_name")
    private String lineName;

    @Expose
    @SerializedName("disembark")
    private String disembark;

    @Expose
    @SerializedName("direction")
    private String direction;

    @Expose
    @SerializedName("distance")
    private String distance;

    @Expose
    @SerializedName("note")
    private String note;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTransportationName() {
        return transportationName;
    }

    public void setTransportationName(String transportationName) {
        this.transportationName = transportationName;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getDisembark() {
        return disembark;
    }

    public void setDisembark(String disembark) {
        this.disembark = disembark;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
