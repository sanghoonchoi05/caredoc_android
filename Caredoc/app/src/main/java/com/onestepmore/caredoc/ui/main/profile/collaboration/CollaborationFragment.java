package com.onestepmore.caredoc.ui.main.profile.collaboration;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.databinding.FCollaborationBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;

import javax.inject.Inject;
import javax.inject.Named;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PARTNER_CLICK_BACK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.PARTNER_COMPLETE_SEND_EMAIL;

public class CollaborationFragment extends BaseFragment<FCollaborationBinding, CollaborationViewModel> implements CollaborationNavigator {
    @Inject
    @Named("CollaborationFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private CollaborationViewModel mCollaborationViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_collaboration;
    }

    @Override
    public CollaborationViewModel getViewModel() {
        mCollaborationViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CollaborationViewModel.class);
        return mCollaborationViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCollaborationViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fCollaborationToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AmplitudeManager.getInstance().logEvent(PARTNER_CLICK_BACK);
                BackStackManager.getInstance().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSendMail() {
        String content = getViewDataBinding().fCollaborationTextInput.getText().toString().trim();
        int textLength = content.length();
        if(textLength == 0) {
            showCommonDialog(getString(R.string.warning_msg_collaboration_failed), getString(R.string.warning_msg_collaboration_invalid_content));
            return;
        }

        String subject = getString(R.string.f_collaboration_title);
        String email = "";
        String name = "";
        User user = getViewModel().getRealm().where(User.class).findFirst();
        if(user != null){
            email = user.getEmail();
            name = user.getName();
        }

        getViewModel().sendEmail(
                name + " <" + email + ">",
                "partner@caredoc.kr",
                subject,
                content
        );
    }

    @Override
    public void onSuccessSendMail() {
        AmplitudeManager.getInstance().logEvent(PARTNER_COMPLETE_SEND_EMAIL);
        showCommonDialog(
                getString(R.string.f_collaboration_succeed_title),
                getString(R.string.f_collaboration_succeed_desc),
                "common_dialog",
                (view) -> BackStackManager.getInstance().onBackPressed(),
                getString(R.string.common_confirm),
                "");
    }
}
