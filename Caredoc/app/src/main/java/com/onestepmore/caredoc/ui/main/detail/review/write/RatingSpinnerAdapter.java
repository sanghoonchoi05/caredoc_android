package com.onestepmore.caredoc.ui.main.detail.review.write;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.onestepmore.caredoc.R;

import java.util.ArrayList;
import java.util.List;

public class RatingSpinnerAdapter extends BaseAdapter {
    private List<String> data;
    private LayoutInflater inflater;


    public RatingSpinnerAdapter(Context context, String defaultText){
        this.data = new ArrayList<>();
        for(int i=0; i<11; i++){
            if(i==0){
                this.data.add(defaultText);
            }
            else{
                this.data.add(String.valueOf(i));
            }
        }

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(data!=null) return data.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.i_write_review_rating_spinner_normal, parent, false);
        }

        if(data!=null){
            //데이터세팅
            String point = data.get(position);
            ((AppCompatTextView)convertView.findViewById(R.id.i_write_review_rating_spinner_normal_text)).setText(point);
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(R.layout.i_write_review_rating_spinner_dropdown, parent, false);
        }

        if(data!=null){
            //데이터세팅
            String point = data.get(position);
            ((AppCompatTextView)convertView.findViewById(R.id.i_write_review_rating_spinner_dropdown_text)).setText(point);
        }

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
