package com.onestepmore.caredoc.ui.main.detail.homecare;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HomecareDetailFragmentProvider {
    @ContributesAndroidInjector(modules = HomecareDetailFragmentModule.class)
    abstract HomecareDetailFragment provideHomecareDetailFragmentFactory();
}
