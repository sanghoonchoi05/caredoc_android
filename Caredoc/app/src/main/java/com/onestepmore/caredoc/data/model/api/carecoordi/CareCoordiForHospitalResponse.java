package com.onestepmore.caredoc.data.model.api.carecoordi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.AggregatesObject;
import com.onestepmore.caredoc.data.model.api.object.FacilityCareCoordiForHospitalObject;
import com.onestepmore.caredoc.data.model.api.object.FacilityCareCoordiObject;

import java.util.List;

public class CareCoordiForHospitalResponse {
    @Expose
    @SerializedName("aggregates")
    private AggregatesObject aggregates;

    @Expose
    @SerializedName("total")
    private int total;

    @Expose
    @SerializedName("list")
    private List<FacilityCareCoordiForHospitalObject> list;

    public int getTotal() {
        return total;
    }

    public List<FacilityCareCoordiForHospitalObject> getList() {
        return list;
    }

    public void setList(List<FacilityCareCoordiForHospitalObject> list) {
        this.list = list;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public AggregatesObject getAggregates() {
        return aggregates;
    }

    public void setAggregates(AggregatesObject aggregates) {
        this.aggregates = aggregates;
    }
}
