package com.onestepmore.caredoc.ui.carecoordi.result;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ResultFragmentProvider {
    @ContributesAndroidInjector(modules = ResultFragmentModule.class)
    abstract ResultFragment provideResultFragmentFactory();
}
