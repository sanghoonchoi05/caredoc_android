package com.onestepmore.caredoc.ui.main.search.list;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.onestepmore.caredoc.data.model.api.object.SearchFilter;
import com.onestepmore.caredoc.databinding.FSearchListBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.search.SearchFacilityAdapter;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.map.SearchMapFragment;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_FILTER_DETAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_MODE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SEARCH_FACILITY_LOAD_LIST;

public class SearchListFragment extends BaseFragment<FSearchListBinding, SearchListViewModel> implements
        SearchListNavigator,
        NestedScrollView.OnScrollChangeListener,
        BaseFragment.OnBackPressedListener
{
    @Inject
    @Named("SearchListFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private SearchListViewModel mSearchListViewModel;

    private int mNestedScrollViewY = 0;
    private SearchFilter.SearchFilterBuilder mCachedSearchFilterBuilder;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_search_list;
    }

    @Override
    public SearchListViewModel getViewModel() {
        mSearchListViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchListViewModel.class);
        return mSearchListViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSearchListViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UIUtils.setStatusBarColor(getBaseActivity(), getView(), getResources().getColor(R.color.colorPrimary), false);

        SearchFragment searchFragment = (SearchFragment) BackStackManager.getInstance().getAddedRootFragment(SearchFragment.class.getName());
        if(searchFragment != null){
            mCachedSearchFilterBuilder = searchFragment.getSearchFilterBuilder();
            initListAdapter();
            initObserver();

            loadFacilityFromServer();
        }
    }

    private void initListAdapter(){
        SearchFacilityAdapter searchAdapter = new SearchFacilityAdapter(getViewModel().getFacilitySearchList());
        searchAdapter.setMainNavigator((MainActivity)getBaseActivity());
        getViewDataBinding().fSearchListRecyclerView.setAdapter(searchAdapter);
        getViewDataBinding().fSearchListRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        getViewDataBinding().fSearchListRecyclerView.setItemAnimator(null);
        getViewDataBinding().fSearchListScrollView.setOnScrollChangeListener(this);

        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(),new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_facility));
        getViewDataBinding().fSearchListRecyclerView.addItemDecoration(dividerItemDecoration);
    }

    private void initObserver(){
        // 시설 검색 리스트 Realm Change Observer 추가
        getViewModel().getFacilitySearchList().addChangeListener((results, changeSets) -> {
            getViewModel().setSkip(results.size());
            getViewModel().setEmpty(results.size() == 0);
        });

        getViewModel().getFacilityRepository().getObservableFacilityTotalCount().observe(getViewLifecycleOwner(),
                count -> getViewDataBinding().setTotalCount(count != null ? count : 0));
    }

    @Override
    public void onScrollChange(NestedScrollView nestedScrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        mNestedScrollViewY = scrollY;

        if (!nestedScrollView.canScrollVertically(1)) {
            AppLogger.i(getClass(), "리스트 끝에 도착");
            getViewDataBinding().fSearchListRecyclerView.stopScroll();
            getViewDataBinding().fSearchListRecyclerView.stopNestedScroll();
            int loadedMoreCount = getLoadedMoreCount();
            int totalLoadMoreCount = getTotalLoadMoreCount();

            if(totalLoadMoreCount - loadedMoreCount > 0){
                Map<String, MapValue<?>> map = new HashMap<>();
                map.put(AmplitudeParam.Key.MAX_COUNT, new MapValue<>(totalLoadMoreCount));
                map.put(AmplitudeParam.Key.COUNT, new MapValue<>(loadedMoreCount + 1));
                AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_LOAD_LIST, map);
                getViewModel().loadFacilitySearchListFromServer(mCachedSearchFilterBuilder.build(), getOrderBy());
            }
            else{
                getViewModel().setNoMoreList(true);
            }
        }
    }

    private int getFacilityTotalCount(){
        int count = 0;
        if(getViewModel().getFacilityRepository().getObservableFacilityTotalCount() != null){
            count = getViewModel().getFacilityRepository().getObservableFacilityTotalCount().getValue();
        }
        return count;
    }

    private int getTotalLoadMoreCount(){
        int totalCount = getFacilityTotalCount();
        int take = getViewModel().getTake();
        if(totalCount == 0 || totalCount <= take){
            return 0;
        }

        int moreCount = totalCount - take;
        int loadMoreCount = moreCount / take;
        int _count = moreCount % take;
        if(_count > 0){
            loadMoreCount += 1;
        }
        return loadMoreCount;
    }

    private int getLoadedMoreCount(){
        int skip = getViewModel().getSkip();
        int take = getViewModel().getTake();
        if(skip == 0 || skip <= take){
            return 0;
        }

        int moreCount = skip - take;
        int loadedMoreCount = moreCount / take;
        int _count = moreCount % take;
        if(_count > 0){
            loadedMoreCount += 1;
        }
        return loadedMoreCount;
    }

    public boolean isScrollTop() {
        return mNestedScrollViewY == 0;
    }

    public void scrollUpToTop() {
        getViewDataBinding().fSearchListScrollView.setScrollY(0);
    }

    // 현재 설정된 카테고리와 필터로 검색
    public void loadFacilityFromServer() {
        getViewModel().setSkip(0);
        getViewModel().getFacilityRepository().setCachedFacilityTotalCount(0);
        getViewModel().getFacilityRepository().fetchFacilityTotalCount();
        getViewModel().clearFacilityRealm();
        getViewModel().loadFacilitySearchListFromServer(mCachedSearchFilterBuilder.build(), getOrderBy());
    }

    @Override
    public void onDestroyView() {
        removeObserver();
        super.onDestroyView();
    }

    private void removeObserver(){
        getViewModel().getFacilitySearchList().removeAllChangeListeners();
        getViewModel().getFacilityRepository().getObservableFacilityTotalCount().removeObservers(getViewLifecycleOwner());
    }

    @Override
    public void doBack() {
        BackStackManager.getInstance().removeBackStackInCurrentActivity();
    }

    @Override
    public void onMap() {
        AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_CLICK_MODE, AmplitudeParam.Key.MODE, AmplitudeParam.MODE.MAP.toString());
        BackStackManager.getInstance().addChildFragment(new SearchMapFragment(), R.id.f_search_child_fragment_layout);
    }

    @Override
    public void onShow(){
        UIUtils.setStatusBarColor(getBaseActivity(), getView(), getResources().getColor(R.color.colorPrimary), false);
    }

    private FacilitySearchRequest.ORDER_BY getOrderBy(){
        return ((SearchFragment) BackStackManager.getInstance().getAddedRootFragment(SearchFragment.class.getName())).getOrderBy();
    }
}
