/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.data.local.pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.onestepmore.caredoc.AppConstants;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.di.PreferenceInfo;
import com.onestepmore.caredoc.utils.DateUtils;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import static com.onestepmore.caredoc.utils.DateUtils.STANDARD_DATE_TIME_FORMAT;

public class PreferencesHelperImp implements PreferencesHelper {

    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_EXPIRES_IN = "PREF_KEY_EXPIRES_IN";
    private static final String PREF_KEY_TOKEN_TYPE = "PREF_KEY_TOKEN_TYPE";
    private static final String PREF_KEY_LAST_KNOWN_LOCATION_LATITUDE = "PREF_KEY_LAST_KNOWN_LOCATION_LATITUDE";
    private static final String PREF_KEY_LAST_KNOWN_LOCATION_LONGITUDE = "PREF_KEY_LAST_KNOWN_LOCATION_LONGITUDE";
    private static final String PREF_KEY_LOCATION_PERMISSION_GRANTED = "PREF_KEY_LOCATION_PERMISSION_GRANTED";
    private static final String PREF_TOTAL_FACILITY_COUNT = "PREF_TOTAL_FACILITY_COUNT";
    private static final String PREF_KEY_SHOW_EVENT_POPUP = "PREF_KEY_SHOW_EVENT_POPUP";

    private final SharedPreferences mPrefs;

    @Inject
    public PreferencesHelperImp(Context context, @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Override
    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    @Override
    public void setExpiresIn(long expiresIn) {
        mPrefs.edit().putLong(PREF_KEY_EXPIRES_IN, expiresIn).apply();
    }

    @Override
    public long getExpiresIn() {
        return mPrefs.getLong(PREF_KEY_EXPIRES_IN, 0);
    }

    @Override
    public String getTokenType() {
        return mPrefs.getString(PREF_KEY_TOKEN_TYPE, null);
    }

    @Override
    public void setTokenType(String tokenType) {
        mPrefs.edit().putString(PREF_KEY_TOKEN_TYPE, tokenType).apply();
    }

    @Override
    public boolean isLocationPermissionGranted() {
        return mPrefs.getBoolean(PREF_KEY_LOCATION_PERMISSION_GRANTED, false);
    }

    @Override
    public void setLocationPermissionGranted(boolean granted) {
        mPrefs.edit().putBoolean(PREF_KEY_LOCATION_PERMISSION_GRANTED, granted).apply();
    }

    @Override
    public String getLastKnownLocationLatitude() {
        return mPrefs.getString(PREF_KEY_LAST_KNOWN_LOCATION_LATITUDE, "");
    }

    @Override
    public void setLastKnownLocationLatitude(String latitude) {
        mPrefs.edit().putString(PREF_KEY_LAST_KNOWN_LOCATION_LATITUDE, latitude).apply();
    }

    @Override
    public String getLastKnownLocationLongitude() {
        return mPrefs.getString(PREF_KEY_LAST_KNOWN_LOCATION_LONGITUDE, "");
    }

    @Override
    public void setLastKnownLocationLongitude(String longitude) {
        mPrefs.edit().putString(PREF_KEY_LAST_KNOWN_LOCATION_LONGITUDE, longitude).apply();
    }

    @Override
    public int getTotalFacilityCount() {
        return mPrefs.getInt(PREF_TOTAL_FACILITY_COUNT, 0);
    }

    @Override
    public void setTotalFacilityCount(int totalCount) {
        mPrefs.edit().putInt(PREF_TOTAL_FACILITY_COUNT, totalCount).apply();
    }

    @Override
    public boolean isShowEventPopup() {
        return mPrefs.getBoolean(PREF_KEY_SHOW_EVENT_POPUP, false);
    }

    @Override
    public void setShowEventPopup(boolean show) {
        mPrefs.edit().putBoolean(PREF_KEY_SHOW_EVENT_POPUP, show).apply();
    }
}
