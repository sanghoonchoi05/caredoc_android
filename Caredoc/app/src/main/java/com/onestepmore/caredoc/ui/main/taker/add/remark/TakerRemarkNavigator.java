package com.onestepmore.caredoc.ui.main.taker.add.remark;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface TakerRemarkNavigator extends BaseNavigator {
    void onClick(String tag);
}
