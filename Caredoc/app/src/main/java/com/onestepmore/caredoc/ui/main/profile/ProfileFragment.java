package com.onestepmore.caredoc.ui.main.profile;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.View;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.nhn.android.naverlogin.OAuthLogin;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.realm.FacilityRecentlyList;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.databinding.FProfileBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.profile.collaboration.CollaborationFragment;
import com.onestepmore.caredoc.ui.main.profile.contact.ContactFragment;
import com.onestepmore.caredoc.ui.main.profile.recently.RecentlyFragment;
import com.onestepmore.caredoc.ui.main.profile.webview.WebViewActivity;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.card.TakerCardFragment;
import com.onestepmore.caredoc.ui.main.taker.detail.TakerDetailActivity;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.ui.widgets.StartPagerSnapHelper;
import com.onestepmore.caredoc.utils.UIUtils;
import com.zoyi.channel.plugin.android.ChannelIO;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

import static android.support.v4.view.ViewPager.SCROLL_STATE_DRAGGING;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_AUTH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_CS;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_FAQ;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_PARTNER;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_RECENTLY_ALL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_TERM_AGREEMENT;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_TERM_PRIVACY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.PROFILE_COMPLETE_LOGOUT;

public class ProfileFragment extends BaseFragment<FProfileBinding, ProfileViewModel> implements
        ProfileNavigator
{
    public enum ETC_LIST{
        /*PROFILE_RECENTLY("최근 본 시설", false),*/
        CONTACT("서비스 문의", true),
        COLLABORATION("파트너 문의", true),
        FAQ("자주묻는 질문", false),
        TERMS("이용약관" , false),
        PRIVACY("개인정보 처리방침", false),
        VERSION("버전정보", false),
        LOGOUT("로그아웃", true);

        private String title;
        private boolean isLogin;

        ETC_LIST(String title, boolean isLogin){
            this.title = title;
            this.isLogin = isLogin;
        }

        public String getTitle() {
            return title;
        }

        public boolean isLogin() {
            return isLogin;
        }
    }

    @Inject
    @Named("ProfileFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private ProfileViewModel mProfileViewModel;

    RealmResults<Taker> mTakerList;
    RealmResults<FacilityRecentlyList> mFacilityRecentlyList;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_profile;
    }

    @Override
    public ProfileViewModel getViewModel() {
        mProfileViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ProfileViewModel.class);
        return mProfileViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProfileViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity)getBaseActivity()).showCSFab(true);

        //getViewDataBinding().setName(getString(R.string.f_profile_hello));
        //getViewDataBinding().setEmail(getString(R.string.f_profile_need_login));

        boolean isLogin = getViewModel().getDataManager().isLoggedIn();
        if(isLogin){
            User user = getViewModel().getRealm().where(User.class).findFirst();
            getViewDataBinding().setUser(user);
            //getViewDataBinding().setName(user.getName());
            //getViewDataBinding().setEmail(user.getEmail());
        }
        getViewDataBinding().setIsLogin(isLogin);

        // 어르신 관련 홀드
        /*mTakerList = getViewModel().getRealm().where(Taker.class).sort("createdAt",Sort.DESCENDING).limit(4).findAll();
        mTakerList.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Taker>>() {
            @Override
            public void onChange(RealmResults<Taker> results, OrderedCollectionChangeSet changeSet) {
                getViewModel().setExistTaker(results.size() > 0);
                getViewDataBinding().fProfileTakerRecyclerView.scrollToPosition(0);
            }
        });
        getViewModel().setExistTaker(mTakerList.size() > 0);

        TakerSimpleCardAdapter adapter = new TakerSimpleCardAdapter(mTakerList, this);
        getViewDataBinding().fProfileTakerRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManagerCenter = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fProfileTakerRecyclerView.setLayoutManager(layoutManagerCenter);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_taker_simple_card_horizontal_item_space);
        int spacingFirstLast = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_taker_simple_card_horizontal_first_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, spacingInPixels, true);
        decoration.setSpaceFirstAndLastHorizontal(spacingFirstLast);
        getViewDataBinding().fProfileTakerRecyclerView.addItemDecoration(decoration);*/

        initRecentlyList();
        initEtcList(isLogin);

        UIUtils.setStatusBarColor(getBaseActivity(), getView(), getResources().getColor(R.color.colorPrimary), false);
    }

    private void initRecentlyList(){
// 시설 검색 리스트 Realm Change Observer 추가
        mFacilityRecentlyList = getViewModel().getRealm().where(FacilityRecentlyList.class).sort("date",Sort.DESCENDING).limit(4).findAll();
        mFacilityRecentlyList.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<FacilityRecentlyList>>() {
            @Override
            public void onChange(RealmResults<FacilityRecentlyList> results, OrderedCollectionChangeSet changeSet) {
                getViewModel().setExistRecentlyFacility(results.size() > 0);
                getViewDataBinding().fProfileRecentlyRecyclerView.scrollToPosition(0);
                AppLogger.i(getClass(), "ProfileFragment realm Facility size: " + results.size());
            }
        });
        getViewModel().setExistRecentlyFacility(mFacilityRecentlyList.size() > 0);

        FacilityRecentlyAdapter adapter = new FacilityRecentlyAdapter(mFacilityRecentlyList);
        adapter.setMainNavigator((MainActivity)getBaseActivity());
        getViewDataBinding().fProfileRecentlyRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fProfileRecentlyRecyclerView.setLayoutManager(layoutManager);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_recently_horizontal_item_space);
        int spacingFirstLast = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_recently_horizontal_first_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, spacingInPixels, true);
        decoration.setSpaceFirstAndLastHorizontal(spacingFirstLast);
        getViewDataBinding().fProfileRecentlyRecyclerView.addItemDecoration(decoration);
        getViewDataBinding().fProfileRecentlyRecyclerView.setItemAnimator(null);

        SnapHelper snapHelperCenter = new StartPagerSnapHelper(0);
        snapHelperCenter.attachToRecyclerView(getViewDataBinding().fProfileRecentlyRecyclerView);

        getViewDataBinding().fProfileRecentlyRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == SCROLL_STATE_DRAGGING){
                    int snapPosition = RecyclerView.NO_POSITION;
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    if(layoutManager!=null){
                        View snapView = snapHelperCenter.findSnapView(layoutManager);
                        if(snapView!=null){
                            RecyclerView.Adapter adapter = recyclerView.getAdapter();
                            if(adapter != null){
                                snapPosition = layoutManager.getPosition(snapView);
                                int start = snapPosition - 1;
                                int end = snapPosition + 2;
                                if(start < 0){
                                    start = 0;
                                }
                                if(end > adapter.getItemCount() - 1){
                                    end = adapter.getItemCount() - 1;
                                }

                                if(end - start == 3){
                                    adapter.notifyItemChanged(start);
                                    adapter.notifyItemChanged(end);
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    private void initEtcList(boolean isLogin){
        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(),new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_profile_etc));

        ProfileEtcAdapter etcAdapter = new ProfileEtcAdapter(this);
        etcAdapter.isLogin(isLogin);
        getViewDataBinding().fProfileEtcRecyclerView.addItemDecoration(dividerItemDecoration);
        getViewDataBinding().fProfileEtcRecyclerView.setAdapter(etcAdapter);
        getViewDataBinding().fProfileEtcRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
    }

    @Override
    public void onItemClick(int position, String text) {
        /*if(text.equals(ETC_LIST.PROFILE_RECENTLY.getTitle())){
            BackStackManager.getInstance().addChildFragment(new RecentlyFragment(), R.id.a_main_content_fragment_layout);
        }
        else */if(text.equals(ETC_LIST.CONTACT.getTitle())){
            AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_CS);
            BackStackManager.getInstance().addChildFragment(new ContactFragment(), R.id.a_main_content_fragment_layout);
        }else if(text.equals(ETC_LIST.COLLABORATION.getTitle())){
            AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_PARTNER);
            BackStackManager.getInstance().addChildFragment(new CollaborationFragment(), R.id.a_main_content_fragment_layout);
        }else if(text.equals(ETC_LIST.FAQ.getTitle())
                || text.equals(ETC_LIST.TERMS.getTitle())
                || text.equals(ETC_LIST.PRIVACY.getTitle())){
            Intent intent = getBaseActivity().newIntent(WebViewActivity.class);
            intent.putExtra("title", text);
            String url = BuildConfig.BASE_URL + "/m/faq/service?webview=1";
            if(text.equals(ETC_LIST.FAQ.getTitle())){
                AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_FAQ);
            }
            if(text.equals(ETC_LIST.TERMS.getTitle())){
                AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_TERM_AGREEMENT);
                url = BuildConfig.BASE_URL + "/m/terms/member?webview=1";
            }else if(text.equals(ETC_LIST.PRIVACY.getTitle())){
                AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_TERM_PRIVACY);
                url = BuildConfig.BASE_URL + "/m/terms/privacy?webview=1";
            }
            intent.putExtra("url", url);
            getBaseActivity().startActivity(intent);
        }else if(text.equals(ETC_LIST.LOGOUT.getTitle())){
            getViewModel().logout();
        }
    }

    @Override
    public void openLoginFragment() {
        /*Intent intent = getBaseActivity().newIntent(AccountMainActivity.class);
        intent.putExtra("login", true);
        startActivity(intent);*/
    }

    @Override
    public void openAccountMainActivity(boolean logout) {
        AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_AUTH);

        Intent intent = getBaseActivity().newIntent(AccountMainActivity.class);
        intent.putExtra("logout", logout);
        startActivity(intent);

        if(logout){
            BackStackManager.getInstance().removeAllFragment();
            getBaseActivity().finish();
        }
    }

    @Override
    public void onShowTakerAll() {
        ((MainActivity)getBaseActivity()).showBottomTab(false);
        BackStackManager.getInstance().addChildFragment(TakerCardFragment.newInstance(false), R.id.a_main_content_fragment_layout);
    }

    @Override
    public void onShowRecentlyAll() {
        AmplitudeManager.getInstance().logEvent(PROFILE_CLICK_RECENTLY_ALL);
        ((MainActivity)getBaseActivity()).showBottomTab(false);
        BackStackManager.getInstance().addChildFragment(new RecentlyFragment(), R.id.a_main_content_fragment_layout);
    }

    @Override
    public void onTakerItemClick(int position, Taker taker) {
        if(position == 0){
            if(getViewModel().getDataManager().isLoggedIn()){
                Intent intent = getBaseActivity().newIntent(AddTakerActivity.class);
                startActivity(intent);
            }
            else{
                showNeedLoginPopup();
            }
        }else{
            Intent intent = getBaseActivity().newIntent(TakerDetailActivity.class);
            intent.putExtra("takerId", taker.getFakeId());
            startActivity(intent);
        }
    }

    @Override
    public void onLogout() {
        AmplitudeManager.getInstance().logEvent(PROFILE_COMPLETE_LOGOUT);
        ((MainActivity)getBaseActivity()).showCSFab(false);
        LoginManager.getInstance().logOut();
        OAuthLogin.getInstance().logout(getBaseActivity());
        openAccountMainActivity(true);
    }

    /**
     * OnDestroy we unbind from the model and controller.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mTakerList != null){
            mTakerList.removeAllChangeListeners();
        }

        if(mFacilityRecentlyList != null){
            mFacilityRecentlyList.removeAllChangeListeners();
        }
    }

    @Override
    public void onAdded() {
        super.onAdded();
    }

    @Override
    public void onShow(){
        ((MainActivity)getBaseActivity()).showCSFab(true);

        if(getBaseActivity() instanceof MainActivity){
            ((MainActivity) getBaseActivity()).showBottomTab(true);
            UIUtils.setStatusBarColor(getBaseActivity(), getView(), getResources().getColor(R.color.colorPrimary), false);
        }
    }

    @Override
    public void onHide() {
        super.onHide();

        ((MainActivity)getBaseActivity()).showCSFab(false);
    }
}
