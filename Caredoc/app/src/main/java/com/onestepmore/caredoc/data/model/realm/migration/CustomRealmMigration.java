package com.onestepmore.caredoc.data.model.realm.migration;

import com.onestepmore.caredoc.data.model.realm.Juso;
import com.onestepmore.caredoc.data.model.realm.LatLngTaker;
import com.onestepmore.caredoc.data.model.realm.Protector;
import com.onestepmore.caredoc.data.model.realm.RecentlyJuso;
import com.onestepmore.caredoc.data.model.realm.ServiceAddress;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.model.realm.statics.StaticLtiGrades;
import com.onestepmore.caredoc.data.model.realm.statics.StaticRating;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.Locale;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmList;
import io.realm.RealmMigration;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class CustomRealmMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 1) {
            // version 2 에서 바뀐 내용
            RealmObjectSchema staticService = schema.get("StaticService");
            if(staticService != null){
                staticService.addField("evaluationKey", String.class);

            }

            schema.create("FacilityCareCoordiList")
                    .addField("fakeId", Long.class, FieldAttribute.PRIMARY_KEY)
                    .addField("name", String.class)
                    .addField("address", String.class)
                    .addField("roadAddress", String.class)
                    .addField("homepage", String.class)
                    .addField("latestGrade", String.class)
                    .addField("evalType", String.class)
                    .addField("isBookmarked", boolean.class)
                    .addRealmObjectField("latLng", schema.get("LatLngFacility"))
                    .addField("attachesCount", int.class)
                    .addField("thumbnail", String.class)
                    .addRealmObjectField("latestEvaluation", schema.get("Evaluation"))
                    .addField("ratingAvg", float.class)
                    .addField("reviewCount", int.class)
                    .addRealmObjectField("type", schema.get("Type"))
                    .addRealmListField("services", schema.get("Service"))
                    .addRealmListField("evaluations", schema.get("Evaluation"))
                    .addField("distance", double.class)
                    .addField("medicalExpenses", int.class)
                    .addField("scaleCount", int.class);

            schema.create("StaticDisease")
                    .addField("typeCode", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("typeName", String.class);

            schema.create("StaticTypes")
                    .addField("id", int.class, FieldAttribute.PRIMARY_KEY)
                    .addField("category", String.class)
                    .addField("typeCode", String.class)
                    .addField("typeName", String.class);

            schema.create("StaticTool")
                    .addField("typeCode", String.class, FieldAttribute.PRIMARY_KEY)
                    .addField("categoryCode", String.class)
                    .addField("typeName", String.class)
                    .addField("modelName", String.class)
                    .addField("manufacturer", String.class)
                    .addField("usage", String.class)
                    .addField("remark", String.class);

            oldVersion++;
        }

        if(oldVersion == 2){
            RealmObjectSchema facilitySchema = schema.get("FacilitySearchMain");
            if(facilitySchema != null){
                facilitySchema.addField("evaluationSource", String.class);
                facilitySchema.renameField("evalType", "evaluationType");
            }

            facilitySchema = schema.get("FacilitySearchList");
            if(facilitySchema != null){
                facilitySchema.addField("evaluationSource", String.class);
                facilitySchema.renameField("evalType", "evaluationType");
            }

            facilitySchema = schema.get("FacilityRecentlyList");
            if(facilitySchema != null){
                facilitySchema.addField("evaluationSource", String.class);
                facilitySchema.renameField("evalType", "evaluationType");
            }

            facilitySchema = schema.get("FacilityCareCoordiList");
            if(facilitySchema != null){
                facilitySchema.addField("evaluationSource", String.class);
                facilitySchema.renameField("evalType", "evaluationType");
            }

            facilitySchema = schema.get("BookmarkFacilityList");
            if(facilitySchema != null){
                facilitySchema.addField("evaluationSource", String.class);
                facilitySchema.renameField("evalType", "evaluationType");
            }

            oldVersion++;
        }

        if(oldVersion == 3){
            create(schema, StaticLtiGrades.class);

            RealmObjectSchema protectorObject = create(schema, Protector.class);
            RealmObjectSchema latLngTakerObject = create(schema, LatLngTaker.class);
            RealmObjectSchema serviceAddressObject = create(schema, ServiceAddress.class, latLngTakerObject);

            create(schema, Taker.class, protectorObject, serviceAddressObject);
            create(schema, Juso.class);
            create(schema, RecentlyJuso.class);

            oldVersion++;
        }

        if(oldVersion == 4){
            // version 4 에서는 바뀐 내용이 많아 적지 않겠어...
            oldVersion++;
        }

        if(oldVersion == 5){
            create(schema, StaticRating.class);

            RealmObjectSchema tempSchema = schema.get("StaticService");
            if(tempSchema != null){
                tempSchema.addRealmListField("ratings", schema.get("StaticRating"));
            }

            tempSchema = schema.get("Service");
            if(tempSchema != null){
                tempSchema.addField("isClosed", boolean.class);
            }

            oldVersion++;
        }

        if(oldVersion == 6){
            RealmObjectSchema tempSchema = schema.get("StaticCategory");
            if(tempSchema != null){
                tempSchema.addField("id", int.class);
            }

            oldVersion++;
        }

        if (oldVersion < newVersion) {
            throw new IllegalStateException(String.format(Locale.KOREA, "Migration missing from v%d to v%d", oldVersion, newVersion));
        }
    }

    private RealmObjectSchema create(RealmSchema schema, Class<?> realmClass, RealmObjectSchema ... realmObjectParams){
        RealmObjectSchema realmObjectSchema = schema.create(realmClass.getSimpleName());
        for(Field field : realmClass.getDeclaredFields()){
            boolean ignore = field.getAnnotation(Ignore.class) != null;
            if(ignore){
                continue;
            }

            if(java.lang.reflect.Modifier.isStatic(field.getModifiers())){
                continue;
            }

            FieldAttribute attribute = field.getAnnotation(PrimaryKey.class) != null ? FieldAttribute.PRIMARY_KEY : null;

            if(RealmObject.class.isAssignableFrom(field.getType()) ||
                    RealmModel.class.isAssignableFrom(field.getType())){
                String className = field.getType().getSimpleName();
                RealmObjectSchema objectSchema = schema.get(className);
                if(objectSchema == null){
                    objectSchema = getRealmObjectSchema(field.getType().getName(), realmObjectParams);
                }
                realmObjectSchema.addRealmObjectField(field.getName(), objectSchema);
            }
            else if(field.getType() == RealmList.class){
                ParameterizedType parameterizedType = (ParameterizedType)field.getGenericType();
                Class<?> listClass = (Class<?>) parameterizedType.getActualTypeArguments()[0];
                String className = listClass.getSimpleName();
                RealmObjectSchema objectSchema = schema.get(className);
                if(objectSchema == null){
                    objectSchema = getRealmObjectSchema(listClass.getName(), realmObjectParams);
                }
                realmObjectSchema.addRealmListField(field.getName(), objectSchema);
            }
            else if(field.getType().isPrimitive() ||
                    field.getType() == Date.class ||
                    field.getType() == String.class){
                realmObjectSchema.addField(field.getName(), field.getType(), attribute);
            }
        }

        return realmObjectSchema;
    }

    private RealmObjectSchema getRealmObjectSchema(String className, RealmObjectSchema... schemas){
        for(RealmObjectSchema schema : schemas){
            if(schema.getClassName().equals(className)){
                return schema;
            }
        }

        return null;
    }
}
