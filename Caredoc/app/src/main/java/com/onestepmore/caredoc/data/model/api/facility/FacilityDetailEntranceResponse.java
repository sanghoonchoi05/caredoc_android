package com.onestepmore.caredoc.data.model.api.facility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.FacilityDetailObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceEntranceObject;
import com.onestepmore.caredoc.data.model.api.object.ServicesObject;

public class FacilityDetailEntranceResponse extends FacilityDetailObject {
    @Expose
    @SerializedName("services")
    private ServicesObject<ServiceEntranceObject> services;

    public ServicesObject<ServiceEntranceObject> getServices() {
        return services;
    }
}
