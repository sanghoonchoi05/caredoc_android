package com.onestepmore.caredoc.ui.main;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;

import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailRequest;
import com.onestepmore.caredoc.data.model.api.object.ServiceObject;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchTemp;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import io.realm.RealmResults;

public class MainViewModel extends BaseViewModel<MainNavigator> {

    private RealmResults<FacilitySearchTemp> mFacilitySearchTemp;
    private FacilityRepository mFacilityRepository;
    private MutableLiveData<Integer> mBadgeCount = new MutableLiveData<>();

    public MutableLiveData<Integer> getBadgeCount() {
        return mBadgeCount;
    }

    public void setBadgeCount(int badgeCount) {
        this.mBadgeCount.setValue(badgeCount);
    }

    public FacilityRepository getFacilityRepository() {
        return mFacilityRepository;
    }

    public MainViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                         FacilityRepository facilityRepository) {
        super(application, appDataSource, schedulerProvider);

        mFacilityRepository = facilityRepository;
    }

    public void init(){
        clearFacilitySearchTemp();

        if(mFacilitySearchTemp == null){
            mFacilitySearchTemp = getFacilityRepository().getFacilitySearchTemp();
        }
    }

    public RealmResults<FacilitySearchTemp> getFacility(){
        return mFacilitySearchTemp;
    }

    public void setLocationPermissionGranted(boolean granted){
        getDataManager().setLocationPermissionGranted(granted);
    }

    public boolean isLocationPermissionGranted(){
        return getDataManager().isLocationPermissionGranted();
    }

    public void loadFacilityDetailBaseFromServer(long id) {
        clearFacilitySearchTemp();

        setIsLoading(true);
        FacilityDetailRequest.PathParameter pathParameter = new FacilityDetailRequest.PathParameter();
        pathParameter.setId(id);
        getCompositeDisposable().add(getFacilityRepository()
                .getFacilityDetailBaseApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    if(response.getServices() != null && response.getServices().getItems().size() > 0){
                        ServiceObject service = response.getServices().getItems().get(0);
                        if(service == null){
                            return;
                        }

                        if(service.getServiceCategory().equals(ServiceObject.CATEGORY.GERIATRIC_HOSPITAL.getName())){
                            loadFacilityHospitalDetailFromServer(id);
                        }
                        else{
                            loadFacilityHomecareDetailFromServer(id);
                        }
                    }
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void loadFacilityHospitalDetailFromServer(long id) {
        clearFacilitySearchTemp();

        setIsLoading(true);
        FacilityDetailRequest.PathParameter pathParameter = new FacilityDetailRequest.PathParameter();
        pathParameter.setId(id);
        getCompositeDisposable().add(getFacilityRepository()
                .loadFacilityHospitalDetail(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void loadFacilityHomecareDetailFromServer(long id) {
        clearFacilitySearchTemp();

        setIsLoading(true);
        FacilityDetailRequest.PathParameter pathParameter = new FacilityDetailRequest.PathParameter();
        pathParameter.setId(id);
        getCompositeDisposable().add(getFacilityRepository()
                .loadFacilityHomecareDetail(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private void clearFacilitySearchTemp(){
        getFacilityRepository().clearFacilityRealm(FacilitySearchTemp.class);
    }

    public void showChannelTalk(){
        getNavigator().showChannelTalk();
    }

    @Override
    protected void onCleared() {
        unbindRealm();
        super.onCleared();
    }

    private void unbindRealm() {
        if(mFacilitySearchTemp !=null){
            mFacilitySearchTemp.removeAllChangeListeners();
        }

        clearFacilitySearchTemp();
    }
}
