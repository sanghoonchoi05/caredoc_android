package com.onestepmore.caredoc.ui.main.taker.detail;

import com.onestepmore.caredoc.ui.base.BaseNavigator;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;

public interface TakerDetailNavigator extends BaseNavigator {
    void showTakerModifyView(AddTakerActivity.STEP step);
    void showNameDialog();
    void showTakerPhoto();
    void onDelete();
}
