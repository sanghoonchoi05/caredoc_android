package com.onestepmore.caredoc.data.repository;

import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchTemp;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchMapList;
import com.onestepmore.caredoc.data.remote.FacilitySearchApiHelper;

import java.util.List;

import io.reactivex.Flowable;
import io.realm.RealmResults;

public interface FacilityDataSource extends FacilitySearchApiHelper {
    Flowable<Integer> loadFacilitySearchList(FacilitySearchRequest.QueryParameter queryParameter);
    Flowable<Boolean> loadFacilitySearchCode(FacilitySearchMapRequest.QueryParameter queryParameter, String code);
    Flowable<Boolean> loadFacilityHospitalDetail(FacilityDetailRequest.PathParameter pathParameter);
    Flowable<Boolean> loadFacilityHomecareDetail(FacilityDetailRequest.PathParameter pathParameter);

    RealmResults<FacilitySearchTemp> getFacilitySearchTemp();
    RealmResults<FacilitySearchList> getFacilitySearchList();
    RealmResults<FacilitySearchMapList> getFacilitySearchMapList(boolean show);
    RealmResults<FacilitySearchMapList> getFacilitySearchMapList(String code);

    void showFacilitySearchMapList(List<FacilitySearchMapList> list, boolean show);
    void clearFacilityRealm(Class<? extends FacilityRealmModel> facilityClass);
}
