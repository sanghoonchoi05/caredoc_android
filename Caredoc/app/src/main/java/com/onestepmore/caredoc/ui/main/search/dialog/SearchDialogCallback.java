package com.onestepmore.caredoc.ui.main.search.dialog;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface SearchDialogCallback extends BaseNavigator {
    void onSearch();
    void onDelete();
    void onBack();
}
