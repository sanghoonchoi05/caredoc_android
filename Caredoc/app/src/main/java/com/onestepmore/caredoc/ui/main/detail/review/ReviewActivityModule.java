package com.onestepmore.caredoc.ui.main.detail.review;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ReviewActivityModule {
    @Provides
    ReviewViewModel reviewViewModel(Application application, AppDataSource dataSource,
                                               SchedulerProvider schedulerProvider, ReviewRepository reviewRepository) {
        return new ReviewViewModel(application, dataSource, schedulerProvider, reviewRepository);
    }

    @Provides
    ViewModelProvider.Factory provideReviewViewModelProvider(ReviewViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
