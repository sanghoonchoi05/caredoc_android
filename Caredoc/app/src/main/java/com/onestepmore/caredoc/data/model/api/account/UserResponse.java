package com.onestepmore.caredoc.data.model.api.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.AvatarObject;
import com.onestepmore.caredoc.data.model.api.object.SocialAccountProviderObject;

import java.util.List;

public final class UserResponse {

    @Expose
    @SerializedName("id")
    private long id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("email_verified_at")
    private boolean emailVerifiedAt;

    @Expose
    @SerializedName("contact")
    private String contact;

    @Expose
    @SerializedName("photo_url")
    private String photoUrl;

    @Expose
    @SerializedName("avatars")
    private List<AvatarObject> avatars;

    @Expose
    @SerializedName("social_account_providers")
    private List<SocialAccountProviderObject> socialAccountProvider;

    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public boolean isEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public String getContact() {
        return contact;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getName() {
        return name;
    }

    public List<AvatarObject> getAvatars() {
        return avatars;
    }

    public List<SocialAccountProviderObject> getSocialAccountProvider() {
        return socialAccountProvider;
    }
}