package com.onestepmore.caredoc.data.model.api.object.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoordinateObject {
    @Expose
    @SerializedName("buldMnnm")
    public String buldMnnm;

    @Expose
    @SerializedName("rnMgtSn")
    public String rnMgtSn;

    @Expose
    @SerializedName("bdNm")
    public String bdNm;

    @Expose
    @SerializedName("entX")
    public String entX;

    @Expose
    @SerializedName("entY")
    public String entY;

    @Expose
    @SerializedName("admCd")
    public String admCd;

    @Expose
    @SerializedName("bdMgtSn")
    public String bdMgtSn;

    @Expose
    @SerializedName("buldSlno")
    public String buldSlno;

    @Expose
    @SerializedName("udrtYn")
    public String udrtYn;

    public String getBuldMnnm() {
        return buldMnnm;
    }

    public String getRnMgtSn() {
        return rnMgtSn;
    }

    public String getBdNm() {
        return bdNm;
    }

    public String getEntX() {
        return entX;
    }

    public String getEntY() {
        return entY;
    }

    public String getAdmCd() {
        return admCd;
    }

    public String getBdMgtSn() {
        return bdMgtSn;
    }

    public String getBuldSlno() {
        return buldSlno;
    }

    public String getUdrtYn() {
        return udrtYn;
    }
}
