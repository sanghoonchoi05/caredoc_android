package com.onestepmore.caredoc.ui.estimate.request;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.VStepMainBinding;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.widgets.progress.ProgressNavigator;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;

public class EstimateRequestActivity extends BaseActivity<VStepMainBinding, EstimateRequestViewModel> implements
        EstimateRequestNavigator,
        ProgressNavigator/*,,
        HasSupportFragmentInjector,
        ViewPager.OnPageChangeListener*/ {
    public enum STEP {
        TAKER(0, false),
        ADDRESS(1, false),
        SERVICE(2, false),
        DISEASE(3, false),
        REMARK(4, false),
        PROTECTOR(5, false),
        RESULT(6, false);

        private int step;
        private boolean canSkip;

        STEP(int step, boolean canSkip) {
            this.step = step;
            this.canSkip = canSkip;
        }

        public int getStep() {
            return step;
        }

        public boolean isCanSkip() {
            return canSkip;
        }
    }

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private EstimateRequestViewModel mEstimateRequestViewModel;

    private FragmentManager mFragmentManager;
    private EstimateRequestPagerAdapter mPagerAdapter;

    private EstimateRequestActivity.STEP mCurrentStep = STEP.TAKER;
    private EstimateRequestActivity.STEP mPrevStep = STEP.TAKER;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.v_step_main;
    }

    @Override
    public EstimateRequestViewModel getViewModel() {
        mEstimateRequestViewModel = ViewModelProviders.of(this, mViewModelFactory).get(EstimateRequestViewModel.class);
        return mEstimateRequestViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEstimateRequestViewModel.setNavigator(this);

        //initPager();

        getViewDataBinding().vStepMainView.setShowExit(true);
        //checkBottomButton();
    }

    @Override
    public void handleError(Throwable throwable) {

    }

    @Override
    public void networkError(String msg) {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onNextButtonClick() {

    }

    @Override
    public void onPrevButtonClick() {

    }

    @Override
    public void onModifyButtonClick() {
        // not used.
    }

    @Override
    public void onExit() {
        String title = getString(R.string.estimate_request_exit_dialog_title);
        String desc = getString(R.string.estimate_request_exit_dialog_desc);
        showCommonDialog(title, desc, "", (view) -> finish(), getString(R.string.common_exit), getString(R.string.common_return));
    }
}
