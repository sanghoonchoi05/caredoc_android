package com.onestepmore.caredoc.ui.widgets;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.DialogCommonYnV120Binding;
import com.onestepmore.caredoc.ui.base.BaseDialog;

public class CommonYNDialogV120 extends BaseDialog{
    private View.OnClickListener yesListener;
    private View.OnClickListener noListener;

    public static CommonYNDialogV120 newInstance(String title, String desc, String cancelText, String okText){
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("desc", desc);
        bundle.putString("cancelText", cancelText);
        bundle.putString("okText", okText);

        CommonYNDialogV120 dialog = new CommonYNDialogV120();
        dialog.setArguments(bundle);

        return dialog;

    }

    public void setOnYesClickListener(View.OnClickListener listener){
        this.yesListener = listener;
    }

    public void setOnNoClickListener(View.OnClickListener listener){
        this.noListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DialogCommonYnV120Binding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()), R.layout.dialog_common_yn_v120, null, false);

        binding.dialogCommonYnCancel.setOnClickListener((view) -> {
            dismissDialog(getTag());
            if(noListener != null){
                noListener.onClick(view);
            }
        });

        binding.dialogCommonYnButton.setOnClickListener((view) -> {
            dismissDialog(getTag());
            if(yesListener != null){
                yesListener.onClick(view);
            }
        });

        if(getArguments() != null){
            binding.setTitle(getArguments().getString("title"));
            binding.setDesc(getArguments().getString("desc"));
            binding.setCancelText(getArguments().getString("cancelText"));
            binding.setOkText(getArguments().getString("okText"));
        }

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);


        return dialog;
    }
}
