package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StaticTypes extends RealmObject implements Parcelable {
    public enum TYPE_ENUM{
        BLOOD_TYPE,
        RELIGION,
        RELATION,
        LIVING,
        PERFORMANCE_LEVEL,
        COMMON_LEVEL,
        TENDENCY
    }
    @PrimaryKey
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("category")
    private String category;
    @Expose
    @SerializedName("type_code")
    private String typeCode;
    @Expose
    @SerializedName("type_name")
    private String typeName;

    public StaticTypes(){}

    protected StaticTypes(Parcel in) {
        id = in.readInt();
        category = in.readString();
        typeCode = in.readString();
        typeName = in.readString();
    }

    public static final Creator<StaticTypes> CREATOR = new Creator<StaticTypes>() {
        @Override
        public StaticTypes createFromParcel(Parcel in) {
            return new StaticTypes(in);
        }

        @Override
        public StaticTypes[] newArray(int size) {
            return new StaticTypes[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(category);
        dest.writeString(typeCode);
        dest.writeString(typeName);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
