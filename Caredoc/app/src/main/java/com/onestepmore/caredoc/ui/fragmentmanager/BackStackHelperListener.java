package com.onestepmore.caredoc.ui.fragmentmanager;

public interface BackStackHelperListener {
    /** Let the listener know that the app should close. The backstack is depleted */
    void onFinish();

    /** Let the listener know that the user clicked on an already main root. So app can do
     * a secondary action if needed
     */
    void refresh();

    void onBack(String tag);

}
