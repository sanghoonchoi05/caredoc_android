package com.onestepmore.caredoc.data.repository;

import android.content.Context;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.Gson;
import com.onestepmore.caredoc.data.local.pref.PreferencesHelper;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.FacilityCommentResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceSummaryResponse;
import com.onestepmore.caredoc.data.model.api.review.CommentRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.CommentSaveRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewSaveRequest;
import com.onestepmore.caredoc.data.remote.ReviewApiHelper;

import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class ReviewRepository implements ReviewDataSource {
    private ReviewApiHelper mReviewApiHelper;

    @Inject
    public ReviewRepository(Context context, PreferencesHelper preferencesHelper, Gson gson,
                            ReviewApiHelper reviewApiHelper) {
        mReviewApiHelper = reviewApiHelper;
    }

    @Override
    public Flowable<ReviewFacilityServiceSummaryResponse> getReviewServiceSummaryApiCall(ReviewFacilityRequest.PathParameter pathParameter) {
        return mReviewApiHelper.getReviewServiceSummaryApiCall(pathParameter);
    }

    @Override
    public Flowable<ReviewFacilityServiceResponse> getReviewServiceApiCall(ReviewFacilityRequest.PathParameter pathParameter) {
        return mReviewApiHelper.getReviewServiceApiCall(pathParameter);
    }

    @Override
    public Flowable<FacilityCommentResponse> getCommentApiCall(ReviewFacilityRequest.PathParameter pathParameter) {
        return mReviewApiHelper.getCommentApiCall(pathParameter);
    }

    @Override
    public void saveCommentApiCall(CommentSaveRequest request, CommentSaveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        mReviewApiHelper.saveCommentApiCall(request, parameter, listener);
    }

    @Override
    public void removeCommentApiCall(CommentRemoveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        mReviewApiHelper.removeCommentApiCall(parameter, listener);
    }

    @Override
    public void saveReviewApiCall(JSONObject jsonRequest, ReviewSaveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        mReviewApiHelper.saveReviewApiCall(jsonRequest, parameter, listener);
    }

    @Override
    public void removeReviewApiCall(ReviewRemoveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        mReviewApiHelper.removeReviewApiCall(parameter, listener);
    }
}
