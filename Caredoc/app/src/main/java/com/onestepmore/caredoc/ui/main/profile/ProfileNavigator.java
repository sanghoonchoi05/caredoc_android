package com.onestepmore.caredoc.ui.main.profile;

import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface ProfileNavigator extends BaseNavigator {
    void onItemClick(int position, String text);
    void openLoginFragment();
    void openAccountMainActivity(boolean logout);
    void onShowTakerAll();
    void onShowRecentlyAll();
    void onTakerItemClick(int position, Taker taker);
    void onLogout();

}
