package com.onestepmore.caredoc.ui.carecoordi;

import com.onestepmore.caredoc.data.model.api.object.AggregatesObject;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface CareCoordiNavigator extends BaseNavigator {
    void onResult(AggregatesObject object, int totalCount);
}
