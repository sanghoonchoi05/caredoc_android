/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.di.module;

import android.app.Application;
import android.content.Context;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onestepmore.caredoc.AppConstants;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.data.local.pref.PreferencesHelper;
import com.onestepmore.caredoc.data.local.pref.PreferencesHelperImp;
import com.onestepmore.caredoc.data.remote.ApiHeader;
import com.onestepmore.caredoc.data.remote.ApiHelper;
import com.onestepmore.caredoc.data.remote.ApiHelperImp;
import com.onestepmore.caredoc.data.remote.BookmarkApiHelper;
import com.onestepmore.caredoc.data.remote.BookmarkApiHelperImp;
import com.onestepmore.caredoc.data.remote.FacilitySearchApiHelper;
import com.onestepmore.caredoc.data.remote.FacilitySearchApiHelperImp;
import com.onestepmore.caredoc.data.remote.ReviewApiHelper;
import com.onestepmore.caredoc.data.remote.ReviewApiHelperImp;
import com.onestepmore.caredoc.data.remote.TakerApiHelper;
import com.onestepmore.caredoc.data.remote.TakerApiHelperImp;
import com.onestepmore.caredoc.data.repository.AppDataRepository;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.BookmarkDataSource;
import com.onestepmore.caredoc.data.repository.BookmarkRepository;
import com.onestepmore.caredoc.data.repository.FacilityDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.data.repository.ReviewDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.data.repository.TakerDataSource;
import com.onestepmore.caredoc.data.repository.TakerRepository;
import com.onestepmore.caredoc.di.ApiInfo;
import com.onestepmore.caredoc.di.DatabaseInfo;
import com.onestepmore.caredoc.di.PreferenceInfo;
import com.onestepmore.caredoc.di.RealmInfo;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.utils.DateUtils;
import com.onestepmore.caredoc.utils.rx.AppSchedulerProvider;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

import static com.onestepmore.caredoc.BasicApp.UNRELIABLE_INTEGER_FACTORY;

@Module
public class AppModule {

    @Provides
    @Singleton
    ApiHelper provideApiHelper(ApiHelperImp apiHelperImp) {
        return apiHelperImp;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return BuildConfig.API_KEY;
    }


    @Provides
    @Singleton
    Context provideContext(Application application) {
        return application;
    }

    @Provides
    @Singleton
    AppDataSource provideAppDataSource(AppDataRepository appDataManager) {
        return appDataManager;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return AppConstants.DB_NAME;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(DateUtils.STANDARD_DATE_TIME_FORMAT)
                .registerTypeAdapterFactory(UNRELIABLE_INTEGER_FACTORY)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(PreferencesHelperImp preferencesHelperImp) {
        return preferencesHelperImp;
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(
                apiKey,
                preferencesHelper.getTokenType() + " " + preferencesHelper.getAccessToken()
                );
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    /*@Provides
    @Singleton
    CallbackManager provideFBCallbackManager(){
        return new CallbackManager.Factory().create();
    }*/

    @Provides
    @Singleton
    FirebaseAnalytics provideFirebaseAnalytics(Context context){
        return FirebaseAnalytics.getInstance(context);
    }

    @Provides
    @RealmInfo
    Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

    @Provides
    @Singleton
    FacilitySearchApiHelper provideFacilitySearchApiHelper(FacilitySearchApiHelperImp facilitySearchApiHelperImp) {
        return facilitySearchApiHelperImp;
    }

    @Provides
    @Singleton
    BookmarkApiHelper provideBookmarkApiHelper(BookmarkApiHelperImp bookmarkApiHelperImp) {
        return bookmarkApiHelperImp;
    }

    @Provides
    @Singleton
    FacilityDataSource provideFacilityDataSource(FacilityRepository facilityRepository) {
        return facilityRepository;
    }

    @Provides
    @Singleton
    BookmarkDataSource provideBookmarkDataSource(BookmarkRepository bookmarkRepository) {
        return bookmarkRepository;
    }

    @Provides
    @Singleton
    ReviewDataSource provideReviewDataSource(ReviewRepository reviewRepository) {
        return reviewRepository;
    }

    @Provides
    @Singleton
    ReviewApiHelper provideReviewApiHelper(ReviewApiHelperImp reviewApiHelperImp) {
        return reviewApiHelperImp;
    }

    @Provides
    @Singleton
    TakerDataSource provideTakerDataSource(TakerRepository takerRepository) {
        return takerRepository;
    }

    @Provides
    @Singleton
    TakerApiHelper provideTakerApiHelper(TakerApiHelperImp takerApiHelperImp) {
        return takerApiHelperImp;
    }
}
