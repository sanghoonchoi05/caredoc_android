package com.onestepmore.caredoc.ui.main.search.filter.address.list;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AddressListFragmentModule {
    @Provides
    AddressListViewModel addressListViewModel(Application application, AppDataSource dataSource, Gson gson, SchedulerProvider schedulerProvider) {
        return new AddressListViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("AddressListFragment")
    ViewModelProvider.Factory provideAddressListViewModelProvider(AddressListViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
