package com.onestepmore.caredoc.data.model.api.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class RegisterRequest {

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("password")
    private String password;

    @Expose
    @SerializedName("password_confirmation")
    private String passwordConfirmation;

    public RegisterRequest(String email, String name, String password, String passwordConfirmation){
        this.email = email;
        this.name = name;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }
}
