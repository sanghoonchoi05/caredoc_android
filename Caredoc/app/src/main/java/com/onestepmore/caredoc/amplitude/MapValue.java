package com.onestepmore.caredoc.amplitude;

public class MapValue<T>
{
    private T _t;

    public MapValue(T t){
        _t = t;
    }
    public T get()
    {
        return _t;
    }
}