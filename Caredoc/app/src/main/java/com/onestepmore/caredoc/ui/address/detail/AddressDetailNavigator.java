package com.onestepmore.caredoc.ui.address.detail;

import com.onestepmore.caredoc.data.model.realm.Juso;
import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface AddressDetailNavigator extends BaseNavigator {
    void onSelectJuso(JusoRealmModel juso);
}
