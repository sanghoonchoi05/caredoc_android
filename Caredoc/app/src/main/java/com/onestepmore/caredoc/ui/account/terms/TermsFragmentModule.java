package com.onestepmore.caredoc.ui.account.terms;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TermsFragmentModule {
    @Provides
    TermsViewModel termsViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TermsViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TermsFragment")
    ViewModelProvider.Factory provideTermsViewModelProvider(TermsViewModel termsViewModel) {
        return new ViewModelProviderFactory<>(termsViewModel);
    }
}
