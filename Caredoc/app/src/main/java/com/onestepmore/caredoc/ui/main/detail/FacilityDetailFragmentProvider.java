package com.onestepmore.caredoc.ui.main.detail;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FacilityDetailFragmentProvider {
    @ContributesAndroidInjector(modules = FacilityDetailFragmentModule.class)
    abstract FacilityDetailFragment provideFacilityDetailFragmentFactory();
}
