package com.onestepmore.caredoc.ui.main.taker.add;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;

import java.util.ArrayList;
import java.util.List;

public class AddTakerPagerAdapter extends FragmentPagerAdapter {

    private List<BaseAddTakerFragment> addTakerFragmentList = new ArrayList<>();
    private Context context;

    public AddTakerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void pushAddTakerFragment(BaseAddTakerFragment fragment){
        addTakerFragmentList.add(fragment);
    }
    @Override
    public int getCount() {
        return addTakerFragmentList.size();
    }

    @Override
    public BaseAddTakerFragment getItem(int position) {
        return addTakerFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
}
