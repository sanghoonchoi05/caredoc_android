package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.CareCoordiSearchFilter;
import com.onestepmore.caredoc.data.model.api.object.ServiceAddressObject;
import com.onestepmore.caredoc.data.model.api.object.TakerDetailObject;
import com.onestepmore.caredoc.data.model.api.object.TakerDiseaseObject;
import com.onestepmore.caredoc.data.model.api.object.TakerObject;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.realm.statics.StaticDisease;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.DateUtils;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Address;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Road;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Location;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

import static com.onestepmore.caredoc.utils.DateUtils.SUMMARY_DATE_TIME_FORMAT;

public class Taker extends RealmObject implements Parcelable {
    @PrimaryKey
    private long fakeId;
    private String photoUrl;
    private String relation;
    private String relationStr;
    private String religion;
    private String religionStr;
    private String bloodType;
    private String liveWith;
    private String liveWithStr;
    private boolean isBlhsr;  // 기초생활수급여부
    private String name;
    private Date birth;
    private String address;
    private String addressCode;
    private String addressDetail;
    private String height;
    private String weight;
    private String ltiNumber;
    private String ltiGrade;
    private String performanceMeal;     // StaticTypes 관계테이블로 엮는게 나을수도...
    private String performanceMealStr;
    private String performanceWalk;
    private String performanceWalkStr;
    private String performanceWash;
    private String performanceWashStr;
    private String performanceCloth;
    private String performanceClothStr;
    private String performanceBath;
    private String performanceBathStr;
    private String performanceToilet;
    private String performanceToiletStr;
    private String nutritionFavorite;
    private String nutritionHate;
    private String nutritionAllergy;
    private boolean nutritionIsDiabetic;
    private String remarkHabit;
    private String remarkTendency;
    private String remarkTendencyStr;
    private String remarkSpeak;
    private String remarkSpeakStr;
    private RealmList<ServiceAddress> serviceAddresses;
    private RealmList<StaticDisease> staticDiseases;
    private Protector protector;
    private boolean isOwn;
    private Date createdAt;

    @Ignore
    private int birthYear;
    @Ignore
    private int birthMonth;
    @Ignore
    private int birthDay;

    @Ignore
    private int createdYear;
    @Ignore
    private int createdMonth;
    @Ignore
    private int createdDay;

    @Ignore
    private String diseases;

    public Taker(){}

    protected Taker(Parcel in) {
        fakeId = in.readLong();
        photoUrl = in.readString();
        relation = in.readString();
        relationStr = in.readString();
        religion = in.readString();
        religionStr = in.readString();
        bloodType = in.readString();
        liveWith = in.readString();
        liveWithStr = in.readString();
        isBlhsr = in.readByte() != 0;
        name = in.readString();
        birth = (java.util.Date) in.readSerializable();
        address = in.readString();
        addressCode = in.readString();
        addressDetail = in.readString();
        height = in.readString();
        weight = in.readString();
        ltiNumber = in.readString();
        ltiGrade = in.readString();
        performanceMeal = in.readString();
        performanceMealStr = in.readString();
        performanceWalk = in.readString();
        performanceWalkStr = in.readString();
        performanceWash = in.readString();
        performanceWashStr = in.readString();
        performanceCloth = in.readString();
        performanceClothStr = in.readString();
        performanceBath = in.readString();
        performanceBathStr = in.readString();
        performanceToilet = in.readString();
        performanceToiletStr = in.readString();
        nutritionFavorite = in.readString();
        nutritionHate = in.readString();
        nutritionAllergy = in.readString();
        nutritionIsDiabetic = in.readByte() != 0;
        remarkHabit = in.readString();
        remarkTendency = in.readString();
        remarkTendencyStr = in.readString();
        remarkSpeak = in.readString();
        remarkSpeakStr = in.readString();
        serviceAddresses = new RealmList<>();
        in.readList(serviceAddresses , ServiceAddress.class.getClassLoader());
        staticDiseases = new RealmList<>();
        in.readList(staticDiseases , StaticDisease.class.getClassLoader());
        protector = in.readParcelable(Protector.class.getClassLoader());
        isOwn = in.readByte() != 0;
        createdAt = (java.util.Date) in.readSerializable();
    }

    public static final Creator<Taker> CREATOR = new Creator<Taker>() {
        @Override
        public Taker createFromParcel(Parcel in) {
            return new Taker(in);
        }

        @Override
        public Taker[] newArray(int size) {
            return new Taker[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(fakeId);
        dest.writeString(photoUrl);
        dest.writeString(relation);
        dest.writeString(relationStr);
        dest.writeString(religion);
        dest.writeString(religionStr);
        dest.writeString(bloodType);
        dest.writeString(liveWith);
        dest.writeString(liveWithStr);
        dest.writeByte((byte) (isBlhsr ? 1 : 0));
        dest.writeString(name);
        dest.writeSerializable(birth);
        dest.writeString(address);
        dest.writeString(addressCode);
        dest.writeString(addressDetail);
        dest.writeString(height);
        dest.writeString(weight);
        dest.writeString(ltiNumber);
        dest.writeString(ltiGrade);
        dest.writeString(performanceMeal);
        dest.writeString(performanceMealStr);
        dest.writeString(performanceWalk);
        dest.writeString(performanceWalkStr);
        dest.writeString(performanceWash);
        dest.writeString(performanceWashStr);
        dest.writeString(performanceCloth);
        dest.writeString(performanceClothStr);
        dest.writeString(performanceBath);
        dest.writeString(performanceBathStr);
        dest.writeString(performanceToilet);
        dest.writeString(performanceToiletStr);
        dest.writeString(nutritionFavorite);
        dest.writeString(nutritionHate);
        dest.writeString(nutritionAllergy);
        dest.writeByte((byte) (nutritionIsDiabetic ? 1 : 0));
        dest.writeString(remarkHabit);
        dest.writeString(remarkTendency);
        dest.writeString(remarkTendencyStr);
        dest.writeString(remarkSpeak);
        dest.writeString(remarkSpeakStr);
        dest.writeList(serviceAddresses);
        dest.writeList(staticDiseases);
        dest.writeParcelable(protector, flags);
        dest.writeByte((byte) (isOwn ? 1 : 0));
        dest.writeSerializable(createdAt);
    }

    public void setTaker(TakerObject taker){
        this.fakeId = taker.getFakeId();
        this.photoUrl = taker.getPhotoUrl();
        this.relation = taker.getRelation();
        this.relationStr = taker.getRelationStr();
        this.religion = taker.getReligion();
        this.religionStr = taker.getReligionStr();
        this.bloodType = taker.getBloodType();
        this.liveWith = taker.getLiveWith();
        this.liveWithStr = taker.getLiveWithStr();
        this.isBlhsr = taker.isBlhsr();
        this.name = taker.getName();
        this.birth = DateUtils.getDate(taker.getBirth(), SUMMARY_DATE_TIME_FORMAT);
        this.address = taker.getAddress();
        this.addressCode = taker.getAddressCode();
        this.addressDetail = taker.getAddressDetail();
        this.height = taker.getHeight();
        this.weight = taker.getWeight();
        this.ltiNumber = taker.getLtiNumber();
        this.ltiGrade = taker.getLtiGrade();
        this.performanceMeal = taker.getPerformanceMeal();
        this.performanceMealStr = taker.getPerformanceMealStr();
        this.performanceWalk = taker.getPerformanceWalk();
        this.performanceWalkStr = taker.getPerformanceWalkStr();
        this.performanceWash = taker.getPerformanceWash();
        this.performanceWashStr = taker.getPerformanceWashStr();
        this.performanceCloth = taker.getPerformanceCloth();
        this.performanceClothStr = taker.getPerformanceClothStr();
        this.performanceBath = taker.getPerformanceBath();
        this.performanceBathStr = taker.getPerformanceBathStr();
        this.performanceToilet = taker.getPerformanceToilet();
        this.performanceToiletStr = taker.getPerformanceToiletStr();
        this.nutritionFavorite = taker.getNutritionFavorite();
        this.nutritionHate = taker.getNutritionHate();
        this.nutritionAllergy = taker.getNutritionAllergy();
        this.nutritionIsDiabetic = taker.isNutritionIsDiabetic();
        this.remarkHabit = taker.getRemarkHabit();
        this.remarkTendency = taker.getRemarkTendency();
        this.remarkTendencyStr = taker.getRemarkTendencyStr();
        this.remarkSpeak = taker.getRemarkSpeak();
        this.remarkSpeakStr = taker.getRemarkSpeakStr();
        this.serviceAddresses = new RealmList<>();
        if(taker.getServiceAddressObject() != null && taker.getServiceAddressObject().getItems().size() > 0){
            for(ServiceAddressObject.Data data : taker.getServiceAddressObject().getItems()){
                ServiceAddress serviceAddress = new ServiceAddress();
                serviceAddress.setServiceAddress(data);
                this.serviceAddresses.add(serviceAddress);
            }
        }
        this.isOwn = taker.isOwn();
        this.createdAt = DateUtils.getDate(taker.getCreatedAt());

        if(taker instanceof TakerDetailObject){
            TakerDetailObject takerDetail = (TakerDetailObject)taker;
            this.staticDiseases = new RealmList<>();
            if(takerDetail.getTakerDiseaseObjects() != null && takerDetail.getTakerDiseaseObjects().size() > 0){
                for(TakerDiseaseObject diseaseObject : takerDetail.getTakerDiseaseObjects()){
                    StaticDisease staticDisease = new StaticDisease();
                    staticDisease.setStaticDisease(diseaseObject.getDisease());
                    this.staticDiseases.add(staticDisease);
                }
            }
            this.protector = new Protector();
            this.protector.setProtector(takerDetail.getProtectorObject());
        }
    }

    public long getFakeId() {
        return fakeId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getRelation() {
        return relation;
    }

    public String getReligion() {
        return religion;
    }

    public String getBloodType() {
        return bloodType;
    }

    public boolean isBlhsr() {
        return isBlhsr;
    }

    public String getName() {
        return name;
    }

    public Date getBirth() {
        return birth;
    }

    public String getAddress() {
        return address;
    }

    public String getHeight() {
        return height;
    }

    public String getWeight() {
        return weight;
    }

    public String getLtiNumber() {
        return ltiNumber;
    }

    public String getLtiGrade() {
        return ltiGrade;
    }

    public String getPerformanceMeal() {
        return performanceMeal;
    }

    public String getPerformanceWalk() {
        return performanceWalk;
    }

    public String getPerformanceWash() {
        return performanceWash;
    }

    public String getPerformanceCloth() {
        return performanceCloth;
    }

    public String getPerformanceBath() {
        return performanceBath;
    }

    public String getPerformanceToilet() {
        return performanceToilet;
    }

    public String getNutritionFavorite() {
        return nutritionFavorite;
    }

    public String getNutritionHate() {
        return nutritionHate;
    }

    public String getNutritionAllergy() {
        return nutritionAllergy;
    }

    public boolean isNutritionIsDiabetic() {
        return nutritionIsDiabetic;
    }

    public String getRemarkHabit() {
        return remarkHabit;
    }

    public String getRemarkTendency() {
        return remarkTendency;
    }

    public String getRemarkSpeak() {
        return remarkSpeak;
    }

    public RealmList<ServiceAddress> getServiceAddresses() {
        return serviceAddresses;
    }

    public RealmList<StaticDisease> getStaticDiseases() {
        return staticDiseases;
    }

    public Protector getProtector() {
        return protector;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getLiveWith() {
        return liveWith;
    }

    public int getBirthYear() {
        birthYear = DateUtils.getCalendarNumber(birth, Calendar.YEAR);
        return birthYear;
    }

    public int getBirthMonth() {
        birthMonth = DateUtils.getCalendarNumber(birth, Calendar.MONTH) + 1;
        return birthMonth;
    }

    public int getBirthDay() {
        birthDay = DateUtils.getCalendarNumber(birth, Calendar.DAY_OF_MONTH);
        return birthDay;
    }

    public int getCreatedYear() {
        createdYear = DateUtils.getCalendarNumber(createdAt, Calendar.YEAR);
        return createdYear;
    }

    public int getCreatedMonth() {
        createdMonth = DateUtils.getCalendarNumber(createdAt, Calendar.MONTH) + 1;
        return createdMonth;
    }

    public int getCreatedDay() {
        createdDay = DateUtils.getCalendarNumber(createdAt, Calendar.DAY_OF_MONTH);
        return createdDay;
    }

    public String getDiseases(){
        diseases = null;
        if(staticDiseases != null && staticDiseases.size() > 0){
            StringBuilder builder = new StringBuilder();
            for(int i=0; i<staticDiseases.size(); i++){
                StaticDisease disease = staticDiseases.get(i);
                if(disease != null){
                    builder.append(disease.getTypeName());
                    if(i != staticDiseases.size() - 1){
                        builder.append(", ");
                    }
                }
            }
            diseases = builder.toString();
        }

        return diseases;
    }

    public CareCoordiSearchFilter.TakerInfo getTakerInfo(){
        CareCoordiSearchFilter.TakerInfo takerInfo = new CareCoordiSearchFilter.TakerInfo(DateUtils.getDateFormatTime(getBirth(), DateUtils.SUMMARY_DATE_TIME_FORMAT));
        takerInfo.setLti(!CommonUtils.checkNullAndEmpty(getLtiGrade()));
        takerInfo.setBirth(DateUtils.getDateFormatTime(getBirth(), DateUtils.SUMMARY_DATE_TIME_FORMAT));

        return takerInfo;
    }

    public AddTakerRequest.Basic getBasic(){
        AddTakerRequest.Basic basic = new AddTakerRequest.Basic();

        basic.setName(getName());
        Address address = null;
        if(getAddress() != null){
            address = new Address();
            address.setName(getAddress());
            address.setCode(getAddressCode());
        }
        basic.setAddress(address);
        basic.setBirth(DateUtils.getDateFormatTime(getBirth(), DateUtils.SUMMARY_DATE_TIME_FORMAT));
        basic.setBlhsr(isBlhsr());
        basic.setBloodType(getBloodType());
        basic.setLiveWith(getLiveWith());
        basic.setLiveWithStr(getLiveWithStr());
        basic.setRelation(getRelation());
        basic.setRelationStr(getRelationStr());
        basic.setReligion(getReligion());
        basic.setReligionStr(getReligionStr());

        return basic;
    }

    public AddTakerRequest.Care getCare(){
        AddTakerRequest.Care care = new AddTakerRequest.Care();

        care.setHeight(getHeight());
        care.setWeight(getWeight());
        care.setLtiGrade(getLtiGrade());
        care.setLtiNumber(getLtiNumber());

        return care;
    }

    public AddTakerRequest.Performances getPerformance(){
        AddTakerRequest.Performances performances = new AddTakerRequest.Performances();

        performances.setBath(getPerformanceBath());
        performances.setBathStr(getPerformanceBathStr());
        performances.setCloth(getPerformanceCloth());
        performances.setClothStr(getPerformanceClothStr());
        performances.setMeal(getPerformanceMeal());
        performances.setMealStr(getPerformanceMealStr());
        performances.setToilet(getPerformanceToilet());
        performances.setToiletStr(getPerformanceToiletStr());
        performances.setWalk(getPerformanceWalk());
        performances.setWalkStr(getPerformanceWalkStr());
        performances.setWash(getPerformanceWash());
        performances.setWashStr(getPerformanceWashStr());

        return performances;
    }

    public AddTakerRequest.Nutritions getNutritions(){
        AddTakerRequest.Nutritions nutritions = new AddTakerRequest.Nutritions();

        nutritions.setAllergy(getNutritionAllergy());
        nutritions.setDiabetic(isNutritionIsDiabetic());
        nutritions.setFavorite(getNutritionFavorite());
        nutritions.setHate(getNutritionHate());

        return nutritions;
    }

    public AddTakerRequest.Remarks getRemarks(){
        AddTakerRequest.Remarks remarks = new AddTakerRequest.Remarks();

        remarks.setHabit(getRemarkHabit());
        remarks.setSpeak(getRemarkSpeak());
        remarks.setSpeakStr(getRemarkSpeakStr());
        remarks.setTendency(getRemarkTendency());
        remarks.setTendencyStr(getRemarkTendencyStr());

        return remarks;
    }

    public List<String> getDiseaseList(){
        List<String> diseases = new ArrayList<>();

        RealmList<StaticDisease> staticDiseases = getStaticDiseases();
        if(staticDiseases != null){
            for(StaticDisease disease : staticDiseases){
                diseases.add(disease.getTypeCode());
            }
        }

        return diseases;
    }

    public List<Address> getAddressList(){
        List<Address> addressList = new ArrayList<>();

        RealmList<ServiceAddress> serviceAddresses = getServiceAddresses();
        if(serviceAddresses != null){
            for(ServiceAddress serviceAddress : serviceAddresses){
                Address address = new Address();
                address.setCode(serviceAddress.getAddressCode());
                address.setName(serviceAddress.getAddressName());
                address.setDetail(serviceAddress.getAddressDetail());
                Road road = new Road();
                road.setName(serviceAddress.getRoadAddressName());
                road.setCode(serviceAddress.getAddressCode());
                address.setRoad(road);
                Location location = new Location();
                location.setLat(String.valueOf(serviceAddress.getLatLng().getLatitude()));
                location.setLng(String.valueOf(serviceAddress.getLatLng().getLongitude()));
                address.setLocation(location);
                addressList.add(address);
            }
        }

        return addressList;
    }

    public String getPerformanceMealStr() {
        return performanceMealStr;
    }

    public String getPerformanceWalkStr() {
        return performanceWalkStr;
    }

    public String getPerformanceWashStr() {
        return performanceWashStr;
    }

    public String getPerformanceClothStr() {
        return performanceClothStr;
    }

    public String getPerformanceBathStr() {
        return performanceBathStr;
    }

    public String getPerformanceToiletStr() {
        return performanceToiletStr;
    }

    public String getRemarkTendencyStr() {
        return remarkTendencyStr;
    }

    public String getRemarkSpeakStr() {
        return remarkSpeakStr;
    }

    public String getRelationStr() {
        return relationStr;
    }

    public String getReligionStr() {
        return religionStr;
    }

    public String getLiveWithStr() {
        return liveWithStr;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
