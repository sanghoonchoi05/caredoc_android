package com.onestepmore.caredoc.ui.main.detail.review.write;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.onestepmore.caredoc.R;

import java.util.List;

public class ExposeSpinnerAdapter extends BaseAdapter {
    List<String> data;
    LayoutInflater inflater;


    public ExposeSpinnerAdapter(Context context, List<String> data){
        this.data = data;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if(data!=null) return data.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.i_write_review_expose_spinner_normal, parent, false);
        }

        if(data!=null){
            //데이터세팅
            String expose = data.get(position);
            ((AppCompatTextView)convertView.findViewById(R.id.i_write_review_expose_spinner_normal_text)).setText(expose);
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.i_write_review_expose_spinner_dropdown, parent, false);
        }

        if(data!=null){
            //데이터세팅
            String expose = data.get(position);
            ((AppCompatTextView)convertView.findViewById(R.id.i_write_review_expose_spinner_dropdown_text)).setText(expose);
        }

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
