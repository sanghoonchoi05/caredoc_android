package com.onestepmore.caredoc.ui.base;

import android.content.Context;
import android.support.design.widget.BottomSheetDialogFragment;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.utils.FirebaseAnalyticsUtils;

public class BaseBottomSheetDialog extends BottomSheetDialogFragment {
    private BaseActivity mActivity;
    private FirebaseAnalytics mFirebaseAnalytics;

    public FirebaseAnalytics getFirebaseAnalytics() {
        if(mFirebaseAnalytics == null){
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getBaseActivity());
        }
        return mFirebaseAnalytics;
    }

    public BaseActivity getBaseActivity() {
        return mActivity;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity mActivity = (BaseActivity) context;
            this.mActivity = mActivity;
            mActivity.onFragmentAttached();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Dialog는 화면 단위 보다는 버튼 단위로 처리하자
        //FirebaseAnalyticsUtils.setCurrentScreen(getFirebaseAnalytics(), this);
    }

    @Override
    public void onDetach() {
        mActivity = null;
        super.onDetach();
    }
}
