package com.onestepmore.caredoc.data.model.api.facility;

import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.object.FacilitySearchMapObject;

public final class FacilitySearchMapResponse extends ItemResponse<FacilitySearchMapObject> {
}
