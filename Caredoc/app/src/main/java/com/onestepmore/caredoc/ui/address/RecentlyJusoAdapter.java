package com.onestepmore.caredoc.ui.address;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.RecentlyJuso;
import com.onestepmore.caredoc.databinding.IRecentlyJusoBinding;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class RecentlyJusoAdapter extends RealmRecyclerViewAdapter<RecentlyJuso, RecentlyJusoAdapter.RecentlyJusoViewHolder> {

    private final AddressNavigator mAddressNavigator;

    RecentlyJusoAdapter(OrderedRealmCollection<RecentlyJuso> data, AddressNavigator navigator) {
        super(data, true);

        mAddressNavigator = navigator;
    }

    @NonNull
    @Override
    public RecentlyJusoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        IRecentlyJusoBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_recently_juso, viewGroup, false);
        return new RecentlyJusoViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentlyJusoViewHolder addressViewHolder, int position) {
        OrderedRealmCollection data = getData();
        if (data != null) {
            final RecentlyJuso juso = getData().get(position);
            addressViewHolder.binding.setJuso(juso);
            addressViewHolder.binding.setNavigator(mAddressNavigator);
            addressViewHolder.binding.executePendingBindings();
        }
    }

    static class RecentlyJusoViewHolder extends RecyclerView.ViewHolder {
        final IRecentlyJusoBinding binding;

        private RecentlyJusoViewHolder(IRecentlyJusoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
