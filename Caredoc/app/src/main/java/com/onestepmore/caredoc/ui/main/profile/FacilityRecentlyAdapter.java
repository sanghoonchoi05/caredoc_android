package com.onestepmore.caredoc.ui.main.profile;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilityRecentlyList;
import com.onestepmore.caredoc.databinding.IFacilityHorizontalBinding;
import com.onestepmore.caredoc.ui.main.FacilityHorizontalAdapter;
import com.onestepmore.caredoc.ui.main.MainNavigator;

import io.realm.OrderedRealmCollection;

public class FacilityRecentlyAdapter extends FacilityHorizontalAdapter<FacilityRecentlyList, FacilityHorizontalAdapter.FacilityHorizontalViewHolder> {

    public FacilityRecentlyAdapter(@Nullable OrderedRealmCollection<FacilityRecentlyList> data) {
        super(data, MainNavigator.FROM.PROFILE_RECENTLY);
    }

    @NonNull
    @Override
    public FacilityHorizontalAdapter.FacilityHorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFacilityHorizontalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_horizontal, viewGroup, false);
        return new FacilityHorizontalAdapter.FacilityHorizontalViewHolder(binding);
    }
}