package com.onestepmore.caredoc.ui.carecoordi.result;

import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface ResultNavigator extends BaseNavigator {
    void onFacilityDetailClick(FacilityRealmModel facility);
    void onCareCoordiRetryButtonClick();
    void onOrderButtonClick();
    void onExit();
}
