package com.onestepmore.caredoc.data.model.api.facility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class FacilitySearchMapRequest {
    public static class QueryParameter{
        @Expose
        @SerializedName("filter")
        private String filter;

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }
    }
}
