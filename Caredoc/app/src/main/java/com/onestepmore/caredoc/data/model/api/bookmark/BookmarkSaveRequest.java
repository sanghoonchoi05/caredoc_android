package com.onestepmore.caredoc.data.model.api.bookmark;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class BookmarkSaveRequest {
    public static class PathParameter {
        @Expose
        @SerializedName("morphType")
        private String morphType;

        @Expose
        @SerializedName("morphId")
        private long morphId;

        public long getMorphId() {
            return morphId;
        }

        public void setMorphId(long morphId) {
            this.morphId = morphId;
        }

        public String getMorphType() {
            return morphType;
        }

        public void setMorphType(String morphType) {
            this.morphType = morphType;
        }
    }
}
