package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ApiCommonObject {
    public final class GeoLocation{
        @Expose
        @SerializedName("type")
        private String type;

        @Expose
        @SerializedName("coordinates")
        private List<Double> coordinates;

        public String getType() {
            return type;
        }

        public List<Double> getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(List<Double> coordinates) {
            this.coordinates = coordinates;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public final class LatestEvaluation{
        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("evaluable_type")
        private String evaluableType;

        @Expose
        @SerializedName("evaluable_id")
        private String evaluableId;

        @Expose
        @SerializedName("evaluation_type")
        private String evaluationType;

        @Expose
        @SerializedName("grade")
        private String grade;

        @Expose
        @SerializedName("year")
        private String year;

        @Expose
        @SerializedName("created_at")
        private String createdAt;

        @Expose
        @SerializedName("updated_at")
        private String updatedAt;

        public String getEvaluationType() {
            return evaluationType;
        }

        public String getEvaluableType() {
            return evaluableType;
        }

        public Long getId() {
            return id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getEvaluableId() {
            return evaluableId;
        }

        public String getGrade() {
            return grade;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public String getYear() {
            return year;
        }
    }

    public final class CategoryType{
        @Expose
        @SerializedName("type_code")
        private String typeCode;

        @Expose
        @SerializedName("category_code")
        private String categoryCode;

        @Expose
        @SerializedName("type_name")
        private String typeName;

        public String getTypeCode() {
            return typeCode;
        }

        public String getCategoryCode() {
            return categoryCode;
        }

        public String getTypeName() {
            return typeName;
        }
    }
}
