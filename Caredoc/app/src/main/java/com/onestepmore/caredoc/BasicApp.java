package com.onestepmore.caredoc;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.gsonparserfactory.GsonParserFactory;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.migration.CustomRealmMigration;
import com.onestepmore.caredoc.di.component.DaggerAppComponent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.utils.DateUtils;
import com.zoyi.channel.plugin.android.ChannelIO;

import java.io.IOException;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.exceptions.RealmMigrationNeededException;
import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

/**
 * Android Application class. Used for accessing singletons.
 */
public class BasicApp extends MultiDexApplication implements HasActivityInjector, HasServiceInjector {

    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Inject
    DispatchingAndroidInjector<Service> serviceDispatchingAndroidInjector;

    private AppExecutors mAppExecutors;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppExecutors = new AppExecutors();

        DaggerAppComponent.builder()
                .application(this)
                .build()
                .inject(this);

        // logger init.
        AppLogger.init();

        // android networking init.
        AndroidNetworking.initialize(getApplicationContext());
        if (BuildConfig.DEBUG) {
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY);
        }

        /**
         * realm so파일의 architecture 지원 문제로 보임
         * gradle에 ndk{}블록에 addfilters 지정으로 해결할 수 있을 것 같다...
         * https://ajh322.tistory.com/132 참고하자
         * https://developer.android.com/studio/projects/add-native-code?hl=ko */
        Realm.init(this);

        RealmConfiguration.Builder builder = new RealmConfiguration.Builder().schemaVersion(BuildConfig.REALM_SCHEMA_VERSION);

        if(BuildConfig.DEBUG){
            builder = builder.deleteRealmIfMigrationNeeded();
        }else{
            builder = builder.migration(new CustomRealmMigration());
        }
        RealmConfiguration configuration = builder.build();

        Realm.setDefaultConfiguration(configuration);
        try{
            Realm.getInstance(configuration);
        }catch (RealmMigrationNeededException e){
            AppLogger.e(getClass(), e.getMessage());
            Realm.deleteRealm(configuration);
            Realm.getInstance(configuration);
        }

        if(!BuildConfig.DEBUG){
            Context ctx = this.getApplicationContext();
            // Use the Sentry DSN (client key) from the Project Settings page on Sentry
            String sentryDsn = AppConstants.SENTRY_DSN;
            Sentry.init(
                    sentryDsn,
                    new AndroidSentryClientFactory(ctx)
            );
        }

        FirebaseManager.newInstance(this);
        //CallbackManager.Factory.create();

        // fast android networking parser factory
        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(UNRELIABLE_INTEGER_FACTORY)
                .setDateFormat(DateUtils.STANDARD_DATE_TIME_FORMAT)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
        AndroidNetworking.setParserFactory(new GsonParserFactory(gson));


        // 채널톡
        ChannelIO.initialize(this);

        // 앰플리튜드
        AmplitudeManager.newInstance(this, this);
    }

    @Override
    public void onTerminate() {
        RealmDataAccessor.getInstance().onClose();
        Realm.getDefaultInstance().close();
        Sentry.close();
        super.onTerminate();
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return serviceDispatchingAndroidInjector;
    }

    // 클라이언트의 pojo 객체의 필드 중 데이터 타입이 int나 Integer이고
    // 서버로 부터 받은 데이터가 float이나 double의 경우
    // NumberFormatException을 피하기 위해 double로 파싱 후 int로 타입캐스트 한다.
    public static final TypeAdapter<Number> UNRELIABLE_INTEGER = new TypeAdapter<Number>() {
        @Override
        public Number read(JsonReader in) throws IOException {
            JsonToken jsonToken = in.peek();
            switch (jsonToken) {
                case NUMBER:
                case STRING:
                    String s = in.nextString();
                    try {
                        return Integer.parseInt(s);
                    } catch (NumberFormatException ignored) {
                    }
                    try {
                        return (int)Double.parseDouble(s);
                    } catch (NumberFormatException ignored) {
                    }
                    return null;
                case NULL:
                    in.nextNull();
                    return null;
                case BOOLEAN:
                    in.nextBoolean();
                    return null;
                default:
                    throw new JsonSyntaxException("Expecting number, got: " + jsonToken);
            }
        }
        @Override
        public void write(JsonWriter out, Number value) throws IOException {
            out.value(value);
        }
    };
    public static final TypeAdapterFactory UNRELIABLE_INTEGER_FACTORY = TypeAdapters.newFactory(int.class, Integer.class, UNRELIABLE_INTEGER);
}
