package com.onestepmore.caredoc.ui.account.terms;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.ITermsBinding;
import com.onestepmore.caredoc.ui.AppLogger;

public class TermsAdapter extends RecyclerView.Adapter<TermsAdapter.TermsViewHolder> implements CompoundButton.OnCheckedChangeListener {
    private TermsNavigator mCallback;
    private String [] mTextList;
    public void setCallback(TermsNavigator callback) {
        this.mCallback = callback;
    }

    public void addAll(String [] textList){
        mTextList = textList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TermsAdapter.TermsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ITermsBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_terms, viewGroup, false);
        return new TermsAdapter.TermsViewHolder(binding, mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull TermsAdapter.TermsViewHolder viewHolder, int position) {
        viewHolder.binding.setCallback(mCallback);
        viewHolder.binding.executePendingBindings();
        viewHolder.binding.setText(mTextList[position]);
        viewHolder.binding.setPosition(position);
        viewHolder.binding.iTermsCheckBox.setChecked(false);
        viewHolder.binding.iTermsCheckBox.setId(position);
    }

    @Override
    public int getItemCount() {
        return mTextList == null ? 0 : mTextList.length;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mCallback.onTermsCheck(buttonView.getId(), isChecked);
    }

    class TermsViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        final ITermsBinding binding;
        private TermsNavigator callback;

        private TermsViewHolder(ITermsBinding binding, TermsNavigator callback) {
            super(binding.getRoot());
            this.binding = binding;
            this.callback = callback;
            this.binding.iTermsCheckBox.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            binding.iTermsCheckBox.setSelected(isChecked);
            callback.onTermsCheck(binding.getPosition(), isChecked);
        }
    }
}
