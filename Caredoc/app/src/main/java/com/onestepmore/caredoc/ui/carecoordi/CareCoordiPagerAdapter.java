package com.onestepmore.caredoc.ui.carecoordi;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class CareCoordiPagerAdapter extends FragmentPagerAdapter {

    private List<BaseCoordiFragment> careCoordiFragmentList = new ArrayList<>();
    private Context context;

    public CareCoordiPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addCareCoordiFragment(BaseCoordiFragment fragment){
        careCoordiFragmentList.add(fragment);
    }
    @Override
    public int getCount() {
        return careCoordiFragmentList.size();
    }

    @Override
    public BaseCoordiFragment getItem(int position) {
        return careCoordiFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
}
