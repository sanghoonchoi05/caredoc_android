package com.onestepmore.caredoc.ui.carecoordi.userinfo;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class UserInfoFragmentProvider {
    @ContributesAndroidInjector(modules = UserInfoFragmentModule.class)
    abstract UserInfoFragment provideUserInfoFragmentFactory();
}
