package com.onestepmore.caredoc.ui.common;

public interface AddressClickListener {
    void onShowAddressView(int priority);
    void onDeleteAddress(int priority);
}
