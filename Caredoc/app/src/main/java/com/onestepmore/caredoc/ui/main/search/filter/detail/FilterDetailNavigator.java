package com.onestepmore.caredoc.ui.main.search.filter.detail;

import android.view.View;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface FilterDetailNavigator extends BaseNavigator {
    void onApplyFilter();
    void onOrderByGradeClick();
    void onOrderByReviewClick();
    void onOrderByRatingClick();
}
