package com.onestepmore.caredoc.data.repository;

import android.content.Context;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.Gson;
import com.onestepmore.caredoc.data.local.pref.PreferencesHelper;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkFacilityResponse;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkRemoveRequest;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkSaveRequest;
import com.onestepmore.caredoc.data.remote.BookmarkApiHelper;
import com.onestepmore.caredoc.data.remote.FacilitySearchApiHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class BookmarkRepository implements BookmarkDataSource {
    private BookmarkApiHelper mBookmarkApiHelper;

    @Inject
    public BookmarkRepository(Context context, PreferencesHelper preferencesHelper, Gson gson,
                              BookmarkApiHelper bookmarkApiHelper) {
        mBookmarkApiHelper = bookmarkApiHelper;
    }

    @Override
    public Flowable<BookmarkFacilityResponse> getBookmarkApiCall() {
        return mBookmarkApiHelper.getBookmarkApiCall();
    }

    @Override
    public void saveBookmarkApiCall(BookmarkSaveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        mBookmarkApiHelper.saveBookmarkApiCall(parameter, listener);
    }

    @Override
    public void removeBookmarkApiCall(BookmarkRemoveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        mBookmarkApiHelper.removeBookmarkApiCall(parameter, listener);
    }
}
