package com.onestepmore.caredoc.utils;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.onestepmore.caredoc.R;

/**
 * Provides a method to show a Snackbar.
 */
public class SnackbarUtils {

    private static Snackbar mSnackbar= null;
    public static void showSnackbar(View v, String snackbarText) {
        if (v == null || snackbarText == null) {
            return;
        }

        dismiss();

        mSnackbar = Snackbar.make(v, snackbarText, Snackbar.LENGTH_LONG);
        mSnackbar.show();
    }

    public static void showSnackbar(Activity activity, String snackbarText) {
        if (activity == null || snackbarText == null) {
            return;
        }

        dismiss();

        mSnackbar = Snackbar.make(activity.findViewById(android.R.id.content), snackbarText, Snackbar.LENGTH_LONG);
        mSnackbar.show();
    }

    public static void showSnackbar(Activity activity, String snackbarText, int duration, String textOk, View.OnClickListener listener) {
        if (activity == null || snackbarText == null) {
            return;
        }

        dismiss();

        mSnackbar = Snackbar.make(activity.findViewById(android.R.id.content), snackbarText, duration)
                .setAction(textOk, listener)
                .setActionTextColor(ContextCompat.getColor(activity, R.color.white));
        mSnackbar.show();
    }

    public static void showSnackbar(View v, String snackbarText, String textOk, View.OnClickListener listener) {
        if (v == null || snackbarText == null) {
            return;
        }

        dismiss();

        mSnackbar = Snackbar.make(v, snackbarText, Snackbar.LENGTH_INDEFINITE).setAction(textOk, listener);
        mSnackbar.show();
    }

    public static void showSnackbar(Activity activity, String snackbarText, String textOk, View.OnClickListener listener) {
        if (activity == null || snackbarText == null) {
            return;
        }

        dismiss();

        mSnackbar = Snackbar.make(activity.findViewById(android.R.id.content), snackbarText, Snackbar.LENGTH_INDEFINITE)
                .setAction(textOk, listener)
                .setActionTextColor(ContextCompat.getColor(activity, R.color.white));;
        mSnackbar.show();
    }

    public static void show(){
        if(mSnackbar != null){
            mSnackbar.show();
        }
    }

    public static void dismiss(){
        if(mSnackbar != null){
            mSnackbar.dismiss();
            mSnackbar.removeCallback(null);
            mSnackbar = null;
        }
    }
}
