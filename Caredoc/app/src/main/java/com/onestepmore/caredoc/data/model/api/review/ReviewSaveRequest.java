package com.onestepmore.caredoc.data.model.api.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public final class ReviewSaveRequest {
    @Expose
    @SerializedName("ratings")
    private List<Rating> ratings;

    @Expose
    @SerializedName("content")
    private String content;

    @Expose
    @SerializedName("exposeAfter")
    private int exposeAfter;

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRating(int id, int value){
        if(ratings == null){
            ratings = new ArrayList<>();
        }

        boolean exist = false;
        for(Rating rating : ratings){
            if(rating.id == id){
                if(value > 0){
                    rating.value = value;
                }
                else{
                    // 점수가 0이면 미선택처리. 리스트에서 삭제
                    ratings.remove(rating);
                }
                exist = true;
                break;
            }
        }

        if(!exist && value > 0){
            Rating newRating = new Rating();
            newRating.id = id;
            newRating.value = value;
            ratings.add(newRating);
        }
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setExposeAfter(int expose) {
        this.exposeAfter = expose;
    }

    public int getExposeAfter() {
        return exposeAfter;
    }

    public static class Rating {
        @Expose
        @SerializedName("id")
        private int id;

        @Expose
        @SerializedName("value")
        private int value;

        public void setValue(int value) {
            this.value = value;
        }
        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public int getValue() {
            return value;
        }
    }

    public static class PathParameter {
        @Expose
        @SerializedName("morphType")
        private String morphType;

        @Expose
        @SerializedName("morphId")
        private long morphId;

        public long getMorphId() {
            return morphId;
        }

        public void setMorphId(long morphId) {
            this.morphId = morphId;
        }

        public String getMorphType() {
            return morphType;
        }

        public void setMorphType(String morphType) {
            this.morphType = morphType;
        }
    }
}
