package com.onestepmore.caredoc.data.model.api.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExternalCoordinateRequest {
    public static class QueryParameter {
        @Expose
        @SerializedName("confmKey")
        private String confmKey;

        @Expose
        @SerializedName("admCd")
        private String admCd;

        @Expose
        @SerializedName("rnMgtSn")
        private String rnMgtSn;

        @Expose
        @SerializedName("udrtYn")
        private String udrtYn;

        @Expose
        @SerializedName("buldMnnm")
        private String buldMnnm;

        @Expose
        @SerializedName("buldSlno")
        private String buldSlno;

        @Expose
        @SerializedName("resultType")
        private String resultType;

        public String getConfmKey() {
            return confmKey;
        }

        public void setConfmKey(String confmKey) {
            this.confmKey = confmKey;
        }

        public String getAdmCd() {
            return admCd;
        }

        public void setAdmCd(String admCd) {
            this.admCd = admCd;
        }

        public String getRnMgtSn() {
            return rnMgtSn;
        }

        public void setRnMgtSn(String rnMgtSn) {
            this.rnMgtSn = rnMgtSn;
        }

        public String getUdrtYn() {
            return udrtYn;
        }

        public void setUdrtYn(String udrtYn) {
            this.udrtYn = udrtYn;
        }

        public String getBuldMnnm() {
            return buldMnnm;
        }

        public void setBuldMnnm(String buldMnnm) {
            this.buldMnnm = buldMnnm;
        }

        public String getBuldSlno() {
            return buldSlno;
        }

        public void setBuldSlno(String buldSlno) {
            this.buldSlno = buldSlno;
        }

        public String getResultType() {
            return resultType;
        }

        public void setResultType(String resultType) {
            this.resultType = resultType;
        }
    }
}
