package com.onestepmore.caredoc.ui.account.login;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.widget.EditText;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.account.LoginRequest;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.firebase.FirebaseParam;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.EMAIL_LOGIN_CLICK_RESETPASSWORD;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.EMAIL_LOGIN_COMPLETE;

public class LoginViewModel extends BaseViewModel<LoginNavigator> {
    private final ObservableField<String> userEmail = new ObservableField<>();
    private final ObservableField<String> password = new ObservableField<>();
    private final ObservableBoolean possibleLogin = new ObservableBoolean(false);

    public LoginViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void login() {
        if(!possibleLogin.get()){
            getNavigator().onLoginFailed();
            return;
        }

        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerLoginApiCall(new LoginRequest.ServerLoginRequest(userEmail.get(), password.get()))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getDataManager().updateAccessToken(response.getToken(), response.getExpiresIn(), response.getTokenType());
                    getUserInfo();

                    AmplitudeManager.getInstance().logEvent(EMAIL_LOGIN_COMPLETE);
                    FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.LOGIN_COMPLETE);
                    FirebaseManager.getInstance().logEvent(
                            FirebaseEvent.PredefinedEvent.LOGIN_COMPLETE,
                            new FirebaseManager.Builder().addParam(FirebaseParam.PredefinedKey.METHOD, FirebaseParam.Value.COMPLETE).build());
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private void getUserInfo(){
        getCompositeDisposable().add(getDataManager()
                .loadUserInfoFromServer(getRealm(), userEmail.get(), password.get())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    if(response){
                        getNavigator().openMainActivity();
                    }

                    User user = getRealm().where(User.class).findFirst();
                    if(getDataManager().isLoggedIn() && user != null){
                        AmplitudeManager.getInstance().setUserProperty(user);
                    }

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void setEditTextChangeObserver(EditText emailEdit, EditText passwordEdit){
        rx.Observable<Boolean> emailChange = RxTextView.textChanges(emailEdit)
                .map(this::checkChangedEmail);

        rx.Observable<Boolean> passwordChange = RxTextView.textChanges(passwordEdit)
                .map(this::checkChangedPassword);

        rx.Observable<Boolean> isChange = rx.Observable.combineLatest(passwordChange, emailChange,
                (_passwordChange, _emailChange) -> _passwordChange && _emailChange);

        addSubscription(isChange.subscribe(possibleLogin::set));
    }

    private boolean checkChangedEmail(CharSequence text){
        String email = text.toString().trim();
        userEmail.set(email);
        return CommonUtils.isEmailValid(email);
    }

    private boolean checkChangedPassword(CharSequence text){
        password.set(text.toString().trim());
        return text.toString().length() > 0;

    }

    public void onShowResetPasswordView(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.LOGIN_CLICK_RESET_PASSWORD);
        AmplitudeManager.getInstance().logEvent(EMAIL_LOGIN_CLICK_RESETPASSWORD);
        getNavigator().showResetPasswordView();
    }
}
