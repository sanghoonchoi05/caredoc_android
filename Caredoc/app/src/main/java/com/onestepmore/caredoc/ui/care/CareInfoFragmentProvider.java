package com.onestepmore.caredoc.ui.care;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CareInfoFragmentProvider {
    @ContributesAndroidInjector(modules =CareInfoFragmentModule.class)
    abstract CareInfoFragment provideCareInfoFragmentFactory();
}
