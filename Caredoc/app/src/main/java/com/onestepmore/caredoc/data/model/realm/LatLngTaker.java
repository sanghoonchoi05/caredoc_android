package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;

import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class LatLngTaker implements LatLngRealmModel {
    @PrimaryKey
    private String serviceAddressId;
    private double latitude;
    private double longitude;

    public LatLngTaker(){}

    protected LatLngTaker(Parcel in) {
        serviceAddressId = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<LatLngTaker> CREATOR = new Creator<LatLngTaker>() {
        @Override
        public LatLngTaker createFromParcel(Parcel in) {
            return new LatLngTaker(in);
        }

        @Override
        public LatLngTaker[] newArray(int size) {
            return new LatLngTaker[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceAddressId);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @Override
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    @Override
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    @Override
    public String getId() {
        return serviceAddressId;
    }
    @Override
    public void setId(String serviceAddressId) {
        this.serviceAddressId = serviceAddressId;
    }
    @Override
    public double getLatitude() {
        return latitude;
    }
    @Override
    public double getLongitude() {
        return longitude;
    }
}
