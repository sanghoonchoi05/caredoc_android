package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcelable;

import io.realm.RealmModel;

public interface JusoRealmModel extends RealmModel, ModelDiff, Parcelable {
    String getId();
    void setId(String id);

    String getRoadAddress();
    void setRoadAddress(String roadAddress);

    String getJibunAddress();
    void setJibunAddress(String jibunAddress);

    String getRoadCode();
    void setRoadCode(String roadCode);

    String getZipCode();
    void setZipCode(String zipCode);

    String getBuildingName();
    void setBuildingName(String buildingName);

    String getDetail();
    void setDetail(String detail);

    String getDongCode();
    void setDongCode(String dongCode);

    String getBuildingMainNo();
    void setBuildingMainNo(String buildingMainNo);

    String getBuildingSubNo();
    void setBuildingSubNo(String buildingSubNo);

    String isUnderground();
    void setUnderground(String  underground);

    String getEntX();
    void setEntX(String entX);

    String getEntY();
    void setEntY(String entY);

    double getLat();
    double getLng();

    void setLat(double lat);
    void setLng(double lng);
}
