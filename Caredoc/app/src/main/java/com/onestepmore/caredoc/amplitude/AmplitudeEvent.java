package com.onestepmore.caredoc.amplitude;

import com.onestepmore.caredoc.ui.account.account.AccountFragment;
import com.onestepmore.caredoc.ui.account.login.LoginFragment;
import com.onestepmore.caredoc.ui.account.login.forgot.LoginForgotFragment;
import com.onestepmore.caredoc.ui.account.register.email.EmailRegisterFragment;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.main.MainNavigator;
import com.onestepmore.caredoc.ui.main.detail.FacilityDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.comment.write.WriteCommentActivity;
import com.onestepmore.caredoc.ui.main.detail.photo.PhotoActivity;
import com.onestepmore.caredoc.ui.main.detail.review.ReviewActivity;
import com.onestepmore.caredoc.ui.main.detail.review.write.WriteReviewActivity;
import com.onestepmore.caredoc.ui.main.favorite.FavoriteFragment;
import com.onestepmore.caredoc.ui.main.favorite.detail.FavoriteDetailFragment;
import com.onestepmore.caredoc.ui.main.home.HomeFragment;
import com.onestepmore.caredoc.ui.main.profile.ProfileFragment;
import com.onestepmore.caredoc.ui.main.profile.collaboration.CollaborationFragment;
import com.onestepmore.caredoc.ui.main.profile.contact.ContactFragment;
import com.onestepmore.caredoc.ui.main.profile.recently.RecentlyFragment;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.filter.address.AddressFilterFragment;
import com.onestepmore.caredoc.ui.main.search.filter.detail.FilterDetailFragment;
import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilterFragment;
import com.onestepmore.caredoc.ui.splash.SplashActivity;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.BOOKMARK_DETAIL_CLICK_LIST_ITEM_WITH_SERVICE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.BOOKMARK_DETAIL_CLICK_LIST_ITEM_WITH_TOUCH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.CS_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FILTER_DETAIL_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PARTNER_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PHOTO_GALLERY_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_RECENTLY_LIST_ITEM_WITH_SERVICE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PROFILE_CLICK_RECENTLY_LIST_ITEM_WITH_TOUCH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.RECENTLY_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.RECENTLY_CLICK_LIST_ITEM_WITH_SERVICE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.RECENTLY_CLICK_LIST_ITEM_WITH_TOUCH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_LIST_ITEM_WITH_SERVICE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_LIST_ITEM_WITH_TOUCH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.VIEW_REVIEW_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.WRITE_REVIEW_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.AUTH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.BOOKMARK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.BOOKMARK_DETAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.EMAIL_LOGIN;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.EMAIL_REGISTER;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.FACILITY_DETAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.FILTER_DETAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.FILTER_GRADE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.FILTER_LOCATION;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.HOME;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.PHOTO_GALLERY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.PROFILE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.RECENTLY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.RESET_PASSWORD;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.SEARCH_FACILITY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.SPLASH;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.VIEW_REVIEW;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.WRITE_COMMENT;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.WRITE_REVIEW;

public class AmplitudeEvent {
    public static class Screen{
        public static final String SPLASH = "Splash";
        public static final String AUTH = "Auth";
        public static final String EMAIL_REGISTER = "EmailRegister";
        public static final String EMAIL_LOGIN = "EmailLogin";
        public static final String RESET_PASSWORD = "ResetPassword";
        public static final String HOME = "Home";
        public static final String SEARCH_KEYWORD = "SearchKeyword";
        public static final String SEARCH_FACILITY = "SearchFacility";
        public static final String FILTER_LOCATION = "FilterLocation";
        public static final String FILTER_GRADE = "FilterGrade";
        public static final String FILTER_DETAIL = "FilterDetail";
        public static final String BOOKMARK = "Bookmark";
        public static final String BOOKMARK_DETAIL = "BookmarkDetail";
        public static final String PROFILE = "Profile";
        public static final String RECENTLY = "Recently";
        public static final String FACILITY_DETAIL = "FacilityDetail";
        public static final String PHOTO_GALLERY = "PhotoGallery";
        public static final String WRITE_COMMENT = "WriteComment";
        public static final String WRITE_REVIEW = "WriteReview";
        public static final String VIEW_REVIEW = "ViewReview";
        public static final String FORCE_UPDATE_POPUP = "ForceUpdatePopup";
        public static final String OPTIONAL_UPDATE_NOTI = "OptionalUpdateNoti";
    }

    public static class ClickAction{
        public static final String AUTH_CLICK_EMAIL = "Auth_Click_Email";
        public static final String AUTH_CLICK_LOGIN = "Auth_Click_Login";

        public static final String EMAIL_LOGIN_CLICK_RESETPASSWORD = "EmailLogin_Click_ResetPassword";

        public static final String HOME_CLICK_FACILITY_CATEGORY = "Home_Click_FacilityCategory";
        public static final String HOME_CLICK_CALL = "Home_Click_Call";
        public static final String HOME_CLICK_KAKAOPLUS = "Home_Click_KakaoPlus";
        public static final String HOME_CLICK_CHANNELTALK = "Home_Click_Channeltalk";
        public static final String HOME_CLICK_AUTH = "Home_Click_Auth";
        public static final String HOME_CLICK_PROFILE = "Home_Click_Profile";
        public static final String HOME_CLICK_SEARCH_KEYWORD = "Home_Click_SearchKeyword";

        public static final String SEARCH_KEYWORD_CLICK_LIST_ITEM = "SearchKeyword_Click_ListItem";

        public static final String SEARCH_FACILITY_CLICK_SEARCH_KEYWORD = "SearchFacility_Click_SearchKeyword";
        public static final String SEARCH_FACILITY_CLICK_CATEGORY = "SearchFacility_Click_Category";
        public static final String SEARCH_FACILITY_CLICK_FILTER_LOCATION = "SearchFacility_Click_FilterLocation";
        public static final String SEARCH_FACILITY_CLICK_FILTER_GRADE = "SearchFacility_Click_FilterGrade";
        public static final String SEARCH_FACILITY_CLICK_FILTER_DETAIL = "SearchFacility_Click_FilterDetail";
        public static final String SEARCH_FACILITY_CLICK_LIST_ITEM_WITH_SERVICE = "SearchFacility_Click_ListItemWithService";
        public static final String SEARCH_FACILITY_CLICK_LIST_ITEM_WITH_TOUCH = "SearchFacility_Click_ListItemWithTouch";
        public static final String SEARCH_FACILITY_CLICK_MODE = "SearchFacility_Click_Mode";

        public static final String SEARCH_FACILITY_MAP_LOCATION_MARKER = "SearchFacilityMap_Click_LocationMarker";
        public static final String SEARCH_FACILITY_MAP_FACILITY_MARKER = "SearchFacilityMap_Click_FacilityMarker";

        public static final String FILTER_DETAIL_CLICK_BACK = "FilterDetail_Click_Back";
        public static final String FILTER_DETAIL_CLICK_BACK_KEY = "FilterDetail_Click_BackKey";
        public static final String FILTER_DETAIL_CLICK_INIT = "FilterDetail_Click_Init";

        public static final String BOOKMARK_CLICK_AUTH = "Bookmark_Click_Auth";

        public static final String BOOKMARK_DETAIL_CLICK_LIST_ITEM_WITH_SERVICE = "BookmarkDetail_Click_ListItemWithService";
        public static final String BOOKMARK_DETAIL_CLICK_LIST_ITEM_WITH_TOUCH = "BookmarkDetail_Click_ListItemWithTouch";

        public static final String PROFILE_CLICK_AUTH = "Profile_Click_Auth";
        public static final String PROFILE_CLICK_RECENTLY_LIST_ITEM_WITH_SERVICE = "Profile_Click_RecentlyListItemWithService";
        public static final String PROFILE_CLICK_RECENTLY_LIST_ITEM_WITH_TOUCH = "Profile_Click_RecentlyListItemWithTouch";
        public static final String PROFILE_CLICK_RECENTLY_ALL = "Profile_Click_RecentlyAll";
        public static final String PROFILE_CLICK_FAQ = "Profile_Click_FAQ";
        public static final String PROFILE_CLICK_TERM_AGREEMENT = "Profile_Click_TermAgreement";
        public static final String PROFILE_CLICK_TERM_PRIVACY = "Profile_Click_TermPrivacy";
        public static final String PROFILE_CLICK_CS = "Profile_Click_CS";
        public static final String PROFILE_CLICK_PARTNER = "Profile_Click_Partner";
        public static final String PROFILE_CLICK_CHANNELTALK = "Profile_Click_Channeltalk";

        public static final String RECENTLY_CLICK_LIST_ITEM_WITH_SERVICE = "Recently_Click_ListItemWithService";
        public static final String RECENTLY_CLICK_LIST_ITEM_WITH_TOUCH = "Recently_Click_ListItemWithTouch";
        public static final String RECENTLY_CLICK_BACK = "Recently_Click_Back";
        public static final String RECENTLY_CLICK_BACK_KEY = "Recently_Click_BackKey";

        public static final String CS_CLICK_BACK = "CS_Click_Back";
        public static final String CS_CLICK_BACK_KEY = "CS_Click_BackKey";

        public static final String PARTNER_CLICK_BACK = "Partner_Click_Back";
        public static final String PARTNER_CLICK_BACK_KEY = "Partner_Click_BackKey";

        public static final String FACILITY_DETAIL_CLICK_CALL = "FacilityDetail_Click_Call";
        public static final String FACILITY_DETAIL_CLICK_DETAIL_CALL = "FacilityDetail_Click_DetailCall";
        public static final String FACILITY_DETAIL_CLICK_SHARE = "FacilityDetail_Click_Share";
        public static final String FACILITY_DETAIL_CLICK_BOOKMARK_SAVE = "FacilityDetail_Click_BookmarkSave";
        public static final String FACILITY_DETAIL_CLICK_BOOKMARK_REMOVE = "FacilityDetail_Click_BookmarkRemove";
        public static final String FACILITY_DETAIL_CLICK_CAROUSEL_INDICATOR = "FacilityDetail_Click_CarouselIndicator";
        public static final String FACILITY_DETAIL_CLICK_HOMEPAGE = "FacilityDetail_Click_Homepage";
        public static final String FACILITY_DETAIL_CLICK_WRITE_COMMENT = "FacilityDetail_Click_WriteComment";
        public static final String FACILITY_DETAIL_CLICK_VIEW_REVIEW = "FacilityDetail_Click_ViewReview";
        public static final String FACILITY_DETAIL_CLICK_WRITE_REVIEW = "FacilityDetail_Click_WriteReview";
        public static final String FACILITY_DETAIL_CLICK_BACK = "FacilityDetail_Click_Back";
        public static final String FACILITY_DETAIL_CLICK_BACK_KEY = "FacilityDetail_Click_BackKey";
        public static final String FACILITY_DETAIL_CLICK_TOOLTIP = "FacilityDetail_Click_ToolTip";
        public static final String FACILITY_DETAIL_CLICK_MAP = "FacilityDetail_Click_Map";
        public static final String FACILITY_DETAIL_CLICK_SWITCH_SERVICE = "FacilityDetail_Click_SwitchService";
        public static final String FACILITY_DETAIL_CLICK_SELECT_FEE_SHEET = "FacilityDetail_Click_SelectFeeSheet";
        public static final String FACILITY_DETAIL_CLICK_AUTH_FOR_REVIEW = "FacilityDetail_Click_AuthForReview";

        public static final String PHOTO_GALLERY_CLICK_CAROUSEL_INDICATOR = "PhotoGallery_Click_CarouselIndicator";
        public static final String PHOTO_GALLERY_CLICK_BACK = "PhotoGallery_Click_Back";
        public static final String PHOTO_GALLERY_CLICK_BACK_KEY = "PhotoGallery_Click_BackKey";

        public static final String WRITE_REVIEW_CLICK_CHOOSE_RATING = "WriteReview_Click_ChooseRating";
        public static final String WRITE_REVIEW_CLICK_CHOOSE_EXPOSE_PERIOD = "WriteReview_Click_ChooseExposePeriod";
        public static final String WRITE_REVIEW_CLICK_BACK = "WriteReview_Click_Back";
        public static final String WRITE_REVIEW_CLICK_BACK_KEY = "WriteReview_Click_BackKey";

        public static final String VIEW_REVIEW_CLICK_BACK = "ViewReview_Click_Back";
        public static final String VIEW_REVIEW_CLICK_BACK_KEY = "ViewReview_Click_BackKey";

        public static final String FORCE_UPDATE_POPUP_CLICK_UPDATE = "ForceUpdatePopup_Click_Update";
        public static final String OPTIONAL_UPDATE_NOTI_CLICK_UPDATE = "OptionalUpdateNoti_Click_Update";
    }

    public static class Situation{
        public static final String SPLASH_COMPLETE_AUTO_LOGIN = "Splash_Complete_AutoLogin";
        public static final String AUTH_COMPLETE_FACEBOOK = "Auth_Complete_Facebook";
        public static final String EMAIL_REGISTER_COMPLETE_REGISTER = "EmailRegister_Complete_Register";
        public static final String EMAIL_LOGIN_COMPLETE = "EmailLogin_Complete";
        public static final String RESET_PASSWORD_COMPLETE = "ResetPassword_Complete";

        public static final String SEARCH_KEYWORD_SEARCH = "SearchKeyword_Search";

        public static final String SEARCH_FACILITY_LOAD_LIST = "SearchFacility_Load_List";
        public static final String SEARCH_FACILITY_REMOVE_FILTER_LOCATION = "SearchFacility_Remove_FilterLocation";
        public static final String SEARCH_FACILITY_REMOVE_FILTER_GRADE = "SearchFacility_Remove_FilterGrade";
        public static final String SEARCH_FACILITY_REMOVE_FILTER_RATING = "SearchFacility_Remove_FilterRating";

        public static final String SEARCH_FACILITY_MAP_GRANTED_LOCATION_PSERMISSION = "SearchFacilityMap_Granted_LocationPermission";
        public static final String SEARCH_FACILITY_MAP_ZOOM = "SearchFacilityMap_Zoom";

        public static final String FILTER_LOCATION_APPLY = "FilterLocation_Apply";
        public static final String FILTER_GRADE_APPLY = "FilterGrade_Apply";
        public static final String FILTER_DETAIL_APPLY_ORDER_BY = "FilterDetail_Apply_OrderBy";
        public static final String FILTER_DETAIL_APPLY_RATING = "FilterDetail_Apply_Rating";
        public static final String FILTER_DETAIL_APPLY_GRADE = "FilterDetail_Apply_Grade";

        public static final String PROFILE_COMPLETE_LOGOUT = "Profile_Complete_Logout";

        public static final String CS_COMPLETE_SEND_EMAIL = "CS_Complete_SendEmail";
        public static final String PARTNER_COMPLETE_SEND_EMAIL = "Partner_Complete_SendEmail";

        public static final String WRITE_COMMENT_COMPLETE = "WriteComment_Complete";
        public static final String WRITE_REVIEW_COMPLETE = "WriteReview_Complete";
        public static final String EXIT = "Exit";
    }

    public static String getScreenName(BaseActivity baseActivity){
        return getEventName(baseActivity);
    }

    public static String getEventName(BaseActivity activity){
        String screenName = "";
        if(activity instanceof SplashActivity){
            screenName = SPLASH;
        }
        else if(activity instanceof PhotoActivity){
            screenName = PHOTO_GALLERY;
        }
        else if(activity instanceof WriteCommentActivity){
            screenName = WRITE_COMMENT;
        }
        else if(activity instanceof WriteReviewActivity){
            screenName = WRITE_REVIEW;
        }
        else if(activity instanceof ReviewActivity){
            screenName = VIEW_REVIEW;
        }

        return screenName;
    }

    public static String getScreenName(BaseFragment baseFragment){
        return getEventName(baseFragment);
    }

    public static String getEventName(BaseFragment fragment){
        String screenName = "";
        if(fragment instanceof AccountFragment){
            screenName = AUTH;
        }
        else if(fragment instanceof LoginFragment){
            screenName = EMAIL_LOGIN;
        }
        else if(fragment instanceof EmailRegisterFragment){
            screenName = EMAIL_REGISTER;
        }
        else if(fragment instanceof LoginForgotFragment){
            screenName = RESET_PASSWORD;
        }
        else if(fragment instanceof HomeFragment){
            screenName = HOME;
        }
        else if(fragment instanceof SearchFragment){
            screenName = SEARCH_FACILITY;
        }
        else if(fragment instanceof AddressFilterFragment){
            screenName = FILTER_LOCATION;
        }
        else if(fragment instanceof ServiceFilterFragment){
            screenName = FILTER_GRADE;
        }
        else if(fragment instanceof FilterDetailFragment){
            screenName = FILTER_DETAIL;
        }
        else if(fragment instanceof FavoriteFragment){
            screenName = BOOKMARK;
        }
        else if(fragment instanceof FavoriteDetailFragment){
            screenName = BOOKMARK_DETAIL;
        }
        else if(fragment instanceof ProfileFragment){
            screenName = PROFILE;
        }
        else if(fragment instanceof FacilityDetailFragment){
            screenName = FACILITY_DETAIL;
        }
        else if(fragment instanceof RecentlyFragment){
            screenName = RECENTLY;
        }

        return screenName;
    }

    public static String getBackKeyEventName(BaseFragment fragment){
        String backKeyEvent = "";
        if(fragment instanceof FilterDetailFragment){
            backKeyEvent = FILTER_DETAIL_CLICK_BACK_KEY;
        }
        else if(fragment instanceof ContactFragment){
            backKeyEvent = CS_CLICK_BACK_KEY;
        }
        else if(fragment instanceof CollaborationFragment){
            backKeyEvent = PARTNER_CLICK_BACK_KEY;
        }
        else if(fragment instanceof FacilityDetailFragment){
            backKeyEvent = FACILITY_DETAIL_CLICK_BACK_KEY;
        }
        else if(fragment instanceof RecentlyFragment){
            backKeyEvent = RECENTLY_CLICK_BACK_KEY;
        }

        return backKeyEvent;
    }

    public static String getBackKeyEventName(BaseActivity activity){
        String backKeyEvent = "";
        if(activity instanceof PhotoActivity){
            backKeyEvent = PHOTO_GALLERY_CLICK_BACK_KEY;
        }
        else if(activity instanceof ReviewActivity){
            // 액티비티에서 해결
            //backKeyEvent = VIEW_REVIEW_CLICK_BACK_KEY;
        }
        else if(activity instanceof WriteReviewActivity){
            backKeyEvent = WRITE_REVIEW_CLICK_BACK_KEY;
        }

        return backKeyEvent;
    }

    public static boolean isNeedAmplitudeMode(MainNavigator.FROM from){
        AmplitudeParam.MODE mode = AmplitudeParam.MODE.NONE;
        if(from == MainNavigator.FROM.SEARCH_FACILITY_LIST){
            mode = AmplitudeParam.MODE.LIST;
        }
        else if(from == MainNavigator.FROM.SEARCH_FACILITY_MAP){
            mode = AmplitudeParam.MODE.MAP;
        }

        return mode != AmplitudeParam.MODE.NONE;
    }

    public static AmplitudeParam.MODE getAmplitudeMode(MainNavigator.FROM from){
        AmplitudeParam.MODE mode = AmplitudeParam.MODE.NONE;
        if(from == MainNavigator.FROM.SEARCH_FACILITY_LIST){
            mode = AmplitudeParam.MODE.LIST;
        }
        else if(from == MainNavigator.FROM.SEARCH_FACILITY_MAP){
            mode = AmplitudeParam.MODE.MAP;
        }

        return mode;
    }

    public static String getListItemEventNameForServiceWithFrom(MainNavigator.FROM from){
        String eventName = SEARCH_FACILITY_CLICK_LIST_ITEM_WITH_SERVICE;
        if(from == MainNavigator.FROM.BOOKMARK_DETAIL){
            eventName = BOOKMARK_DETAIL_CLICK_LIST_ITEM_WITH_SERVICE;
        }
        else if(from == MainNavigator.FROM.PROFILE_RECENTLY){
            eventName = PROFILE_CLICK_RECENTLY_LIST_ITEM_WITH_SERVICE;
        }
        else if(from == MainNavigator.FROM.RECENTLY){
            eventName = RECENTLY_CLICK_LIST_ITEM_WITH_SERVICE;
        }

        return eventName;
    }

    public static String getListItemEventNameForTouchWithFrom(MainNavigator.FROM from){
        String eventName = SEARCH_FACILITY_CLICK_LIST_ITEM_WITH_TOUCH;
        if(from == MainNavigator.FROM.BOOKMARK_DETAIL){
            eventName = BOOKMARK_DETAIL_CLICK_LIST_ITEM_WITH_TOUCH;
        }
        else if(from == MainNavigator.FROM.PROFILE_RECENTLY){
            eventName = PROFILE_CLICK_RECENTLY_LIST_ITEM_WITH_TOUCH;
        }
        else if(from == MainNavigator.FROM.RECENTLY){
            eventName = RECENTLY_CLICK_LIST_ITEM_WITH_TOUCH;
        }

        return eventName;
    }
}
