package com.onestepmore.caredoc.ui.main.taker.card;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.databinding.ITakerCardBinding;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class TakerCardAdapter extends RealmRecyclerViewAdapter<Taker, TakerCardAdapter.TakerCardViewHolder> {
    @Nullable
    private final TakerCardNavigator mTakerCardNavigator;
    private final boolean mSelectedMode;

    TakerCardAdapter(OrderedRealmCollection<Taker> data, TakerCardNavigator listener, boolean selectedMode) {
        super(data, true);
        mTakerCardNavigator = listener;
        mSelectedMode = selectedMode;
    }

    @NonNull
    @Override
    public TakerCardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ITakerCardBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_taker_card, viewGroup, false);
        return new TakerCardViewHolder(binding, mTakerCardNavigator, mSelectedMode);
    }

    @Override
    public void onBindViewHolder(@NonNull TakerCardViewHolder takerCardViewHolder, int position) {
        OrderedRealmCollection<Taker> data = getData();
        if(data != null){
            takerCardViewHolder.binding.setTaker(getData().get(position));
            takerCardViewHolder.binding.setSelectedMode(mSelectedMode);
            takerCardViewHolder.binding.executePendingBindings();
        }
    }

    static class TakerCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final ITakerCardBinding binding;
        final TakerCardNavigator listener;
        final boolean selectedMode;

        private TakerCardViewHolder(ITakerCardBinding binding, TakerCardNavigator listener, boolean selectedMode) {
            super(binding.getRoot());
            this.listener = listener;
            this.binding = binding;
            this.selectedMode = selectedMode;

            binding.getRoot().setOnClickListener(this);
            binding.iTakerCardSelectButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if(listener!=null){
                Taker taker = binding.getTaker();
                if(selectedMode){
                    listener.onSelectTaker(taker);
                }else{
                    listener.onTakerItemClick(taker);
                }
            }
        }
    }
}
