package com.onestepmore.caredoc.ui.address.detail;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AddressDetailFragmentModule {
    @Provides
    AddressDetailViewModel addressDetailViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new AddressDetailViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("AddressDetailFragment")
    ViewModelProvider.Factory provideAddressDetailViewModelProvider(AddressDetailViewModel addressDetailViewModel) {
        return new ViewModelProviderFactory<>(addressDetailViewModel);
    }
}
