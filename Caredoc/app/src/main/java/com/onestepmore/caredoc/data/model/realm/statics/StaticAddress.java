package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.AddressObject;
import com.onestepmore.caredoc.data.model.api.object.AddressObject_Deprecated;
import com.onestepmore.caredoc.data.model.realm.LatLngAddress;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class StaticAddress extends RealmObject implements Parcelable {
    @PrimaryKey
    private String bCode;
    private String sidoCode;
    private String gugunCode;
    @Index
    private String dongCode;
    private String addressFullname;
    private String addressName;
    private LatLngAddress latLng;

    public StaticAddress(){}

    protected StaticAddress(Parcel in) {
        bCode = in.readString();
        sidoCode = in.readString();
        gugunCode = in.readString();
        dongCode = in.readString();
        addressFullname = in.readString();
        addressName = in.readString();
        latLng = in.readParcelable(LatLngAddress.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bCode);
        dest.writeString(sidoCode);
        dest.writeString(gugunCode);
        dest.writeString(dongCode);
        dest.writeString(addressFullname);
        dest.writeString(addressName);
        dest.writeParcelable(latLng, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StaticAddress> CREATOR = new Creator<StaticAddress>() {
        @Override
        public StaticAddress createFromParcel(Parcel in) {
            return new StaticAddress(in);
        }

        @Override
        public StaticAddress[] newArray(int size) {
            return new StaticAddress[size];
        }
    };

    public void setStaticAddress(AddressObject address){
        this.bCode = address.getbCode();
        this.sidoCode = address.getSidoCode();
        this.gugunCode = address.getGugunCode();
        this.dongCode = address.getDongCode();
        this.addressFullname = address.getAddressFullname();
        this.addressName = address.getAddressName();
        LatLngAddress latLng = new LatLngAddress();
        latLng.setId(address.getbCode());
        latLng.setLatitude(address.getLat());
        latLng.setLongitude(address.getLng());
        this.latLng = latLng;
    }

    public String getbCode() {
        return bCode;
    }

    public void setbCode(String bCode) {
        this.bCode = bCode;
    }

    public String getSidoCode() {
        return sidoCode;
    }

    public void setSidoCode(String sidoCode) {
        this.sidoCode = sidoCode;
    }

    public String getGugunCode() {
        return gugunCode;
    }

    public void setGugunCode(String gugunCode) {
        this.gugunCode = gugunCode;
    }

    public String getDongCode() {
        return dongCode;
    }

    public void setDongCode(String dongCode) {
        this.dongCode = dongCode;
    }

    public String getAddressFullname() {
        return addressFullname;
    }

    public void setAddressFullname(String addressFullname) {
        this.addressFullname = addressFullname;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public LatLngAddress getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLngAddress latLng) {
        this.latLng = latLng;
    }
}
