package com.onestepmore.caredoc.ui.carecoordi.service;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.databinding.IServiceBinding;

import java.util.ArrayList;
import java.util.List;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceViewHolder> implements ServiceClickCallback {
    List<? extends StaticService> mServiceList;
    List<Boolean> mSelectedList;

    private int mSelectedPosition = -1;

    @Nullable
    private final ServiceClickCallback mServiceClickListener;

    public ServiceAdapter(@Nullable ServiceClickCallback serviceSelectListener){
        mServiceClickListener = serviceSelectListener;
    }

    public void setServiceList(final List<? extends StaticService> serviceList){
        mServiceList = serviceList;
        mSelectedList = new ArrayList<>();
        for(int i=0; i<serviceList.size(); i++){
            mSelectedList.add(false);
        }
        notifyItemRangeInserted(0, serviceList.size());
    }

    @NonNull
    @Override
    public ServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IServiceBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_service, viewGroup, false);
        return new ServiceViewHolder(binding, this);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceViewHolder serviceViewHolder, int position) {
        serviceViewHolder.binding.setService(mServiceList.get(position));
        serviceViewHolder.binding.getRoot().setSelected(mSelectedList.get(position));
        serviceViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mServiceList == null ? 0 : mServiceList.size();
    }

    @Override
    public void onClick(StaticService service) {
        int selectedPosition = -1;

        if(service != null){
            mSelectedList.clear();
            for(int i=0; i<mServiceList.size(); i++){
                if(mServiceList.get(i).getId() == service.getId()){
                    mSelectedList.add(true);
                    selectedPosition = i;
                    mSelectedPosition = selectedPosition;
                }else{
                    mSelectedList.add(false);
                }
            }
        }

        notifyDataSetChanged();

        if(mServiceClickListener!=null){
            mServiceClickListener.onClick(service);
        }
    }

    static class ServiceViewHolder extends RecyclerView.ViewHolder implements ServiceClickCallback {
        final IServiceBinding binding;
        final View view;
        final ServiceClickCallback listener;

        private ServiceViewHolder(IServiceBinding binding, ServiceClickCallback listener) {
            super(binding.getRoot());
            this.binding = binding;
            this.listener = listener;
            view = binding.getRoot();
            binding.setCallback(this);
        }

        @Override
        public void onClick(StaticService service) {
            if(view.isSelected()){
                return;
            }

            view.setSelected(true);

            if(listener!=null){
                listener.onClick(service);
            }
        }
    }
}
