package com.onestepmore.caredoc.ui.main.profile.collaboration;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class CollaborationFragmentModule {
    @Provides
    CollaborationViewModel collaborationViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new CollaborationViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("CollaborationFragment")
    ViewModelProvider.Factory provideCollaborationViewModelProvider(CollaborationViewModel collaborationViewModel) {
        return new ViewModelProviderFactory<>(collaborationViewModel);
    }
}
