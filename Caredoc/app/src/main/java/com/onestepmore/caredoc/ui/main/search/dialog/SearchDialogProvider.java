package com.onestepmore.caredoc.ui.main.search.dialog;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SearchDialogProvider {
    @ContributesAndroidInjector(modules = SearchDialogModule.class)
    abstract SearchDialog provideSearchDialogFactory();
}
