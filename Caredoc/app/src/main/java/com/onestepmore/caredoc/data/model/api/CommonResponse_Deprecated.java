package com.onestepmore.caredoc.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTypes;

import java.util.List;

@Deprecated
public final class CommonResponse_Deprecated {
    @Expose
    @SerializedName("types")
    private List<StaticTypes> staticTypeList;

    public List<StaticTypes> getStaticTypeList() {
        return staticTypeList;
    }
}
