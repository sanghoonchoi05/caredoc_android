package com.onestepmore.caredoc.data.model.api.taker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.utils.CommonUtils;

import java.util.Calendar;
import java.util.List;

public class TakerDetailRequest {

    public static class PathParameter {
        @Expose
        @SerializedName("id")
        private long id;

        public PathParameter(long id){
            this.id = id;
        }

        public long getId() {
            return id;
        }
    }
}
