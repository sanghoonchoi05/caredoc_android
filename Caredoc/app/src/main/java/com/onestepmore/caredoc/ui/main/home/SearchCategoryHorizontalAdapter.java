package com.onestepmore.caredoc.ui.main.home;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.databinding.ISearchCategoryHorizontalBinding;
import com.onestepmore.caredoc.utils.CommonUtils;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class SearchCategoryHorizontalAdapter extends RealmRecyclerViewAdapter<StaticCategory, SearchCategoryHorizontalAdapter.SearchCategoryHorizontalViewHolder>
{
    @Nullable
    private final SearchCategoryClickCallback mListener;

    public SearchCategoryHorizontalAdapter(@Nullable OrderedRealmCollection<StaticCategory> data, @Nullable final SearchCategoryClickCallback listener){
        super(data, true);
        mListener = listener;
    }

    @NonNull
    @Override
    public SearchCategoryHorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ISearchCategoryHorizontalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_search_category_horizontal, viewGroup, false);
        return new SearchCategoryHorizontalViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchCategoryHorizontalViewHolder bottomSheetDialogViewHolder, int position) {
        OrderedRealmCollection data = getData();
        if (data != null) {
            final StaticCategory category = getData().get(position);
            bottomSheetDialogViewHolder.binding.setIconResourceId(CommonUtils.getSearchCategoryIdIconResourceId(category));
            bottomSheetDialogViewHolder.binding.setCategory(category);
            bottomSheetDialogViewHolder.binding.executePendingBindings();
        }
    }

    class SearchCategoryHorizontalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final ISearchCategoryHorizontalBinding binding;

        private SearchCategoryHorizontalViewHolder(ISearchCategoryHorizontalBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.iServiceHorizontalCard.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mListener!=null){
                mListener.onClick(binding.getCategory());
            }
        }
    }
}
