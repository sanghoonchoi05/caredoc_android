package com.onestepmore.caredoc.ui.widgets;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.view.View;

/**
 *
 */
public class StickyBottomBehavior extends CoordinatorLayout.Behavior {
    private int mAnchorId;

    public StickyBottomBehavior(int anchorId) {
        mAnchorId = anchorId;
    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull
            View child, @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
        return (axes == ViewCompat.SCROLL_AXIS_VERTICAL);
    }

    @Override
    public void onNestedPreScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull View
            child, @NonNull View target, int dx, int dy, @NonNull int[] consumed, int type) {

        View anchor = coordinatorLayout.findViewById(mAnchorId);
        int[] anchorLocation = new int[2];
        anchor.getLocationInWindow(anchorLocation);

        int coordBottom = coordinatorLayout.getBottom();

        //vertical position, cannot scroll over the bottom of the coordinator layout
        int minHeight = Math.min(anchorLocation[1], coordBottom - child.getHeight());
        if(minHeight < anchorLocation[1]){
            child.setVisibility(View.VISIBLE);
        }
        else{
            child.setVisibility(View.INVISIBLE);
        }

    }
}
