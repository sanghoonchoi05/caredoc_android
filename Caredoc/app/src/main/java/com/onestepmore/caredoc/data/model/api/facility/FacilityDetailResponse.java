package com.onestepmore.caredoc.data.model.api.facility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.FacilityDetailObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceObject;
import com.onestepmore.caredoc.data.model.api.object.ServicesObject;

public class FacilityDetailResponse extends FacilityDetailObject {
    @Expose
    @SerializedName("services")
    private ServicesObject<ServiceObject> services;

    public ServicesObject<ServiceObject> getServices() {
        return services;
    }
}
