package com.onestepmore.caredoc.firebase;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.ui.AppLogger;

public class FirebaseAppUpdaterChecker {

    public enum STORE{
        Google,
        Onestore,
    }

    private static final String ONESTORE_PRODUCT_ID = "0000740156";
    public interface AppInfo{
        Version getVersion();
    }

    @Expose
    @SerializedName("google")
    private Google google;

    @Expose
    @SerializedName("onestore")
    private Onestore onestore;

    private AppInfo mAppInfo;

    public void fatch(){
        if(BuildConfig.STORE.equals(STORE.Google.toString())){
            mAppInfo = getGoogle();
        }else if(BuildConfig.STORE.equals(STORE.Onestore.toString())){
            mAppInfo = getOnestore();
        }

        if(mAppInfo != null){
            mAppInfo.getVersion().fetch();
        }
    }

    public Google getGoogle() {
        return google;
    }

    public Onestore getOnestore() {
        return onestore;
    }

    public boolean isForceUpdate() {
        if(mAppInfo == null){
            return false;
        }
        return mAppInfo.getVersion().isForceUpdate();
    }

    public boolean isOptionalUpdate() {
        if(mAppInfo == null){
            return false;
        }
        return mAppInfo.getVersion().isOptionalUpdate();
    }

    public String getForceUpdateMessage(){
        if(mAppInfo == null){
            return "";
        }
        return mAppInfo.getVersion().getForceUpdateMessage();
    }

    public String getOptionalUpdateMessage(){
        if(mAppInfo == null){
            return "";
        }
        return mAppInfo.getVersion().getOptionalUpdateMessage();
    }

    public class Google implements AppInfo{
        @Expose
        @SerializedName("version")
        private Version version;

        @Override
        public Version getVersion() {
            return version;
        }
    }

    public class Onestore implements AppInfo{
        @Expose
        @SerializedName("version")
        private Version version;

        @Override
        public Version getVersion() {
            return version;
        }
    }

    public class Version{
        @Expose
        @SerializedName("latest_version_name")
        private String latestVersionName;

        @Expose
        @SerializedName("latest_version_code")
        private String latestVersionCode;

        @Expose
        @SerializedName("minimum_version_name")
        private String minimumVersionName;

        @Expose
        @SerializedName("minimum_version_code")
        private String minimumVersionCode;

        @Expose
        @SerializedName("force_update_message")
        private String forceUpdateMessage;

        @Expose
        @SerializedName("optional_update_message")
        private String optionalUpdateMessage;

        private boolean isForceUpdate = false;
        private boolean isOptionalUpdate = false;

        public String getLatestVersionName() {
            return latestVersionName;
        }

        public String getLatestVersionCode() {
            return latestVersionCode;
        }

        public String getMinimumVersionName() {
            return minimumVersionName;
        }

        public String getMinimumVersionCode() {
            return minimumVersionCode;
        }

        public String getForceUpdateMessage() {
            return forceUpdateMessage;
        }

        public String getOptionalUpdateMessage() {
            return optionalUpdateMessage;
        }

        public void fetch(){
            isForceUpdate = false;
            isOptionalUpdate = false;
            if(compareVersionName(getMinimumVersionName()) && compareVersionCode(getMinimumVersionCode())){
                isForceUpdate = true;
            }

            if(compareVersionName(getLatestVersionName()) && compareVersionCode(getLatestVersionCode())){
                isOptionalUpdate = true;
            }
        }

        private boolean compareVersionCode(String nextVersionCode){
            AppLogger.i(getClass(), "NextVersionCode: " + nextVersionCode);
            try{
                int nextInt = Integer.valueOf(nextVersionCode);
                if(BuildConfig.VERSION_CODE < nextInt){
                    return true;
                }
            }catch (NumberFormatException e){
                return false;
            }

            return false;
        }

        private boolean compareVersionName(String nextVersionName){
            AppLogger.i(getClass(), "NextVersionName: " + nextVersionName);
            String [] curSplit = BuildConfig.VERSION_NAME.split("\\.");
            String [] nextSplit = nextVersionName.split("\\.");

            if(curSplit.length == 0 || nextSplit.length == 0){
                return false;
            }

            int count = curSplit.length;
            if(nextSplit.length > count){
                count = nextSplit.length;
            }

            for(int i=0; i<count; i++){
                String curVer = curSplit.length > i ? curSplit[i] : "0";
                String nextVer = nextSplit.length > i ? nextSplit[i] : "0";
                String [] curDashSplit = curVer.split("-");
                if(curDashSplit.length > 1){
                    curVer = curDashSplit[0];
                }
                String [] nextDashSplit = nextVer.split("-");
                if(nextDashSplit.length > 1){
                    nextVer = nextDashSplit[0];
                }

                try{
                    int curInt = Integer.valueOf(curVer);
                    int nextInt = Integer.valueOf(nextVer);
                    if(curInt < nextInt){
                        return true;
                    }
                    else if(curInt > nextInt){
                        return false;
                    }
                }catch (NumberFormatException e){
                    return false;
                }
            }


            return false;
        }

        public boolean isForceUpdate() {
            return isForceUpdate;
        }

        public boolean isOptionalUpdate() {
            return isOptionalUpdate;
        }
    }

    public void gotoMarket(Context context){
        if(mAppInfo == null){
            return;
        }

        final String appPackageName = context.getPackageName();
        try {
            if(mAppInfo instanceof Google){
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            }else if(mAppInfo instanceof Onestore){
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("onestore://common/product/" + ONESTORE_PRODUCT_ID)));
            }
        } catch (android.content.ActivityNotFoundException anfe) {
            if(mAppInfo instanceof Google){
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }else if(mAppInfo instanceof Onestore){
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://onesto.re/" + ONESTORE_PRODUCT_ID)));
            }
        }
    }
}
