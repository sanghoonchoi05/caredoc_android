package com.onestepmore.caredoc.ui.main.favorite;

import com.onestepmore.caredoc.ui.main.home.HomeFragment;
import com.onestepmore.caredoc.ui.main.home.HomeFragmentModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FavoriteFragmentProvider {
    @ContributesAndroidInjector(modules = FavoriteFragmentModule.class)
    abstract FavoriteFragment provideFavoriteFragmentFactory();
}
