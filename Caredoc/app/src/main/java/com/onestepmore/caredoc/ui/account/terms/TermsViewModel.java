package com.onestepmore.caredoc.ui.account.terms;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TermsViewModel extends BaseViewModel<TermsNavigator> {

    public TermsViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onAgree(){
        getNavigator().onAgree();
    }
}
