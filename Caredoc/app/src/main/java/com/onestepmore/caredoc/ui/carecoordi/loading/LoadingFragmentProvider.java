package com.onestepmore.caredoc.ui.carecoordi.loading;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LoadingFragmentProvider {
    @ContributesAndroidInjector(modules = LoadingFragmentModule.class)
    abstract LoadingFragment provideLoadingFragmentFactory();
}
