package com.onestepmore.caredoc.ui.main.taker.add.performance;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface TakerPerformanceNavigator extends BaseNavigator {
    void onClick(String tag);
}
