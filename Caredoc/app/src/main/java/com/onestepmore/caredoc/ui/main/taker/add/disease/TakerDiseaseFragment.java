package com.onestepmore.caredoc.ui.main.taker.add.disease;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticDisease;
import com.onestepmore.caredoc.databinding.FTakerDiseaseBinding;
import com.onestepmore.caredoc.databinding.VChipGroupItemChoiceBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.BaseAddTakerFragment;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

public class TakerDiseaseFragment extends BaseAddTakerFragment<FTakerDiseaseBinding, TakerDiseaseViewModel> implements
        TakerDiseaseNavigator
{
    public static TakerDiseaseFragment newInstance(int titleResId, boolean isModify){
        TakerDiseaseFragment fragment = new TakerDiseaseFragment();
        fragment.setArguments(getBundle(titleResId, isModify));

        return  fragment;
    }

    @Inject
    @Named("TakerDiseaseFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerDiseaseViewModel mTakerDiseaseViewModel;
    private List<String> mCachedDiseases;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_disease;
    }

    @Override
    public TakerDiseaseViewModel getViewModel() {
        mTakerDiseaseViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerDiseaseViewModel.class);
        return mTakerDiseaseViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerDiseaseViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            setModify(getArguments().getBoolean("isModify"));
        }

        mCachedDiseases = ((AddTakerActivity)getBaseActivity()).getCachedDiseases();

        initTitle(getViewDataBinding().fTakerDiseaseTitle);

        initChipGroup();
    }

    private void initChipGroup() {
        getViewDataBinding().fTakerDiseaseChipGroup.setSingleSelection(false);
        final List<StaticDisease> staticDiseases = getViewModel().getRealm().where(StaticDisease.class).findAll();

        if(staticDiseases == null){
            return;
        }

        getViewDataBinding().fTakerDiseaseChipGroup.removeAllViews();

        List<String> textArray = new ArrayList<>();
        List<Boolean> checkedArray = new ArrayList<>();
        for(StaticDisease disease : staticDiseases){
            textArray.add(disease.getTypeName());
            boolean checked = false;
            if(mCachedDiseases != null){
                for(String checkedDisease : mCachedDiseases){
                    if(disease.getTypeCode().equals(checkedDisease)){
                        checked = true;
                        break;
                    }
                }
            }
            checkedArray.add(checked);
        }

        for (int i=0; i<textArray.size(); i++) {
            VChipGroupItemChoiceBinding binding = DataBindingUtil.inflate(
                    getLayoutInflater(),
                    R.layout.v_chip_group_item_choice,
                    getViewDataBinding().fTakerDiseaseChipGroup,
                    false);
            binding.vChipGroupItemChoiceChip.setText(textArray.get(i));
            binding.vChipGroupItemChoiceChip.setId(i);
            binding.vChipGroupItemChoiceChip.setChecked(checkedArray.get(i));
            binding.vChipGroupItemChoiceChip.setOnCheckedChangeListener((buttonView, isChecked) ->  {
                if(staticDiseases != null){
                    StaticDisease disease = staticDiseases.get(buttonView.getId());
                    if(disease != null){
                        String typeCode =disease.getTypeCode();
                        checkDisease(typeCode);
                    }
                }
            });
            getViewDataBinding().fTakerDiseaseChipGroup.addView(binding.getRoot());
        }
    }

    private void checkDisease(String type){
        if(mCachedDiseases.contains(type)){
            mCachedDiseases.remove(type);
            return;
        }

        mCachedDiseases.add(type);
    }
}
