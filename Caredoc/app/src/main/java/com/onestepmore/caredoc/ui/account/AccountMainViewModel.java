package com.onestepmore.caredoc.ui.account;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class AccountMainViewModel extends BaseViewModel<AccountMainNavigator> {
    public AccountMainViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onEmailRegisterButtonClick(){

    }

    public void onFacebookLoginButtonClick(){

    }

    public void onKakaoLoginButtonClick(){

    }

    public void onNaverLoginButtonClick(){

    }
}
