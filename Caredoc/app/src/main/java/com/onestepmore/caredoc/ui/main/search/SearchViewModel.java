package com.onestepmore.caredoc.ui.main.search;

import android.app.Application;
import android.view.View;

import com.google.gson.Gson;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class SearchViewModel extends BaseViewModel<SearchNavigator>
{

    private FacilityRepository mFacilityRepository;

    public SearchViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider, Gson gson, FacilityRepository facilityRepository) {
        super(application, appDataSource, schedulerProvider);

        mFacilityRepository = facilityRepository;
    }

    public FacilityRepository getFacilityRepository() {
        return mFacilityRepository;
    }

    public void onListLayoutClick(){
        getNavigator().onListLayoutClick();
    }

    public void onSearch(View view){
        getNavigator().onSearchViewClick();
    }

    public void onSearchDeleteClick(){
        getNavigator().onInitSearchKeyword();
    }
}
