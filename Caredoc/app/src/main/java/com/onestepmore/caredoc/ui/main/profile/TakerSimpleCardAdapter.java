package com.onestepmore.caredoc.ui.main.profile;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilityRecentlyList;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.databinding.ITakerSimpleCardBinding;
import com.onestepmore.caredoc.databinding.ITakerSimpleCardPlusBinding;

import java.util.List;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class TakerSimpleCardAdapter extends RealmRecyclerViewAdapter<Taker, TakerSimpleCardAdapter.TakerSimpleCardViewHolder> {

    static final int PLUS_VIEW = 1;
    private final ProfileNavigator mNavigator;
    OrderedRealmCollection<Taker> mData;

    TakerSimpleCardAdapter(OrderedRealmCollection<Taker> data, ProfileNavigator navigator) {
        super(data, true);
        mData = data;
        mNavigator = navigator;
    }

    @NonNull
    @Override
    public TakerSimpleCardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == PLUS_VIEW) {
            ITakerSimpleCardPlusBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_taker_simple_card_plus, viewGroup, false);

            return new TakerSimpleCardViewHolder(binding);
        }

        ITakerSimpleCardBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_taker_simple_card, viewGroup, false);
        return new TakerSimpleCardViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TakerSimpleCardViewHolder takerSimpleCardViewHolder, int i) {
        if(takerSimpleCardViewHolder.binding instanceof ITakerSimpleCardBinding){
            OrderedRealmCollection data = getData();
            if (data != null) {
                Taker taker = getData().get(i - 1);
                ((ITakerSimpleCardBinding) takerSimpleCardViewHolder.binding).setTaker(taker);
            }
        }
        takerSimpleCardViewHolder.setIndex(i);
        takerSimpleCardViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0){
            return PLUS_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        int count = 1;
        if(mData != null && mData.isValid()){
            count += mData.size();
        }
        return count;
    }

    class TakerSimpleCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final ViewDataBinding binding;
        private int index;

        public void setIndex(int index) {
            this.index = index;
        }

        private TakerSimpleCardViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mNavigator != null){
                Taker taker = null;
                if(binding instanceof ITakerSimpleCardBinding){
                    taker = ((ITakerSimpleCardBinding)binding).getTaker();
                }
                mNavigator.onTakerItemClick(index, taker);
            }
        }
    }
}
