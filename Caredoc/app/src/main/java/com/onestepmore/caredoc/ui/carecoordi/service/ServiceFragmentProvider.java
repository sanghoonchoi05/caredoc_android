package com.onestepmore.caredoc.ui.carecoordi.service;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ServiceFragmentProvider {
    @ContributesAndroidInjector(modules = ServiceFragmentModule.class)
    abstract ServiceFragment provideServiceFragmentFactory();
}
