package com.onestepmore.caredoc.ui.main.taker.add.performance;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.realm.statics.StaticLtiGrades;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTypes;
import com.onestepmore.caredoc.databinding.FTakerPerformanceBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.BaseAddTakerFragment;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialogClickListener;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;

public class TakerPerformanceFragment extends BaseAddTakerFragment<FTakerPerformanceBinding, TakerPerformanceViewModel> implements
        TakerPerformanceNavigator,
        BottomSheetDialogClickListener
{
    public static TakerPerformanceFragment newInstance(int titleResId, boolean isModify){
        TakerPerformanceFragment fragment = new TakerPerformanceFragment();
        fragment.setArguments(getBundle(titleResId, isModify));

        return  fragment;
    }

    @Inject
    @Named("TakerPerformanceFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerPerformanceViewModel mTakerPerformanceViewModel;
    private AddTakerRequest.Performances mCachedPerformances;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_performance;
    }

    @Override
    public TakerPerformanceViewModel getViewModel() {
        mTakerPerformanceViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerPerformanceViewModel.class);
        return mTakerPerformanceViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerPerformanceViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            setModify(getArguments().getBoolean("isModify"));
        }

        mCachedPerformances = ((AddTakerActivity)getBaseActivity()).getCachedPerformances();
        if(isModify() && mCachedPerformances != null){
            getViewDataBinding().setPerformance(mCachedPerformances);
        }

        initTitle(getViewDataBinding().fTakerPerformanceTitle);
    }

    @Override
    public void onClick(String tag) {
        RealmResults<StaticTypes> staticTypes = getViewModel().getRealm()
                .where(StaticTypes.class)
                .equalTo("category", StaticTypes.TYPE_ENUM.PERFORMANCE_LEVEL.toString())
                .findAll();

        if(staticTypes != null){
            String [] textList = new String[staticTypes.size()];
            for(int i=0; i<staticTypes.size(); i++){
                textList[i] = staticTypes.get(i).getTypeName();
            }
            showBottomSheetDialog(textList, tag);
        }
    }

    public void showBottomSheetDialog(String [] textList, String tag){
        BottomSheetDialog dialog = BottomSheetDialog.getInstance(textList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), tag);
    }

    @Override
    public void onClick(String tag, String text) {
        AddTakerActivity addTakerActivity = (AddTakerActivity)getBaseActivity();
        if(addTakerActivity == null){
            return;
        }

        String typeCode = addTakerActivity.getTypeCode(text, StaticTypes.TYPE_ENUM.PERFORMANCE_LEVEL);
        if(tag.equals(getString(R.string.f_taker_performance_meal))){
            mCachedPerformances.setMeal(typeCode);
            mCachedPerformances.setMealStr(text);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_walk))){
            mCachedPerformances.setWalk(typeCode);
            mCachedPerformances.setWalkStr(text);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_wash))){
            mCachedPerformances.setWash(typeCode);
            mCachedPerformances.setWashStr(text);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_cloth))){
            mCachedPerformances.setCloth(typeCode);
            mCachedPerformances.setClothStr(text);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_bath))){
            mCachedPerformances.setBath(typeCode);
            mCachedPerformances.setBathStr(text);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_toilet))){
            mCachedPerformances.setToilet(typeCode);
            mCachedPerformances.setToiletStr(text);
        }

        getViewDataBinding().setPerformance(mCachedPerformances);
    }

    @Override
    public void onClose(String tag) {

    }

    @Override
    public void onInit(String tag) {
        if(tag.equals(getString(R.string.f_taker_performance_meal))){
            mCachedPerformances.setMeal(null);
            mCachedPerformances.setMealStr(null);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_walk))){
            mCachedPerformances.setWalk(null);
            mCachedPerformances.setWalkStr(null);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_wash))){
            mCachedPerformances.setWash(null);
            mCachedPerformances.setWashStr(null);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_cloth))){
            mCachedPerformances.setCloth(null);
            mCachedPerformances.setClothStr(null);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_bath))){
            mCachedPerformances.setBath(null);
            mCachedPerformances.setBathStr(null);
        }
        else if(tag.equals(getString(R.string.f_taker_performance_toilet))){
            mCachedPerformances.setToilet(null);
            mCachedPerformances.setToiletStr(null);
        }

        getViewDataBinding().setPerformance(mCachedPerformances);
    }
}
