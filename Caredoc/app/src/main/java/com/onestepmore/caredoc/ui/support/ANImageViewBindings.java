package com.onestepmore.caredoc.ui.support;

import android.databinding.BindingAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.androidnetworking.widget.ANImageView;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Evaluation;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.utils.CommonUtils;

import java.util.List;

public class ANImageViewBindings {
    @BindingAdapter("ImageUrl")
    public static void setImageUrl(ANImageView imageView, String thumbnailUrl) {
        imageView.setDefaultImageResId(R.drawable.ico_profile_basic_big);
        imageView.setImageUrl(BuildConfig.BASE_IMAGE_URL + thumbnailUrl);
    }

    @BindingAdapter("ImageUrlForComment")
    public static void setImageUrlForComment(ANImageView imageView, String thumbnailUrl) {
        imageView.setDefaultImageResId(R.drawable.ico_profile_basic);
        imageView.setImageUrl(BuildConfig.BASE_IMAGE_URL + thumbnailUrl);
    }

    @BindingAdapter("ImageUrlForReview")
    public static void setImageUrlForReview(ANImageView imageView, String thumbnailUrl) {
        imageView.setDefaultImageResId(R.drawable.ico_profile_mini);
        imageView.setImageUrl(BuildConfig.BASE_IMAGE_URL + thumbnailUrl);
    }

    @BindingAdapter("ImageUrlForHome")
    public static void setImageUrlForHome(ANImageView imageView, User user) {
        if(user == null){
            return;
        }
        imageView.setDefaultImageResId(R.drawable.img_noimg_title);
        imageView.setImageUrl(BuildConfig.BASE_IMAGE_URL + user.getPhotoUrl());
    }

    @BindingAdapter("ImageFacilityUrl")
    public static void setImageFacilityUrl(ANImageView imageView, String thumbnailUrl) {
        imageView.setImageUrl(thumbnailUrl);
    }

    @BindingAdapter("ImageFacilityDetailUrl")
    public static void setImageFacilityDetailUrl(ANImageView imageView, String url) {
        imageView.setImageUrl(url);
    }

    @BindingAdapter("ImageTakerUrl")
    public static void setImageTakerUrl(ANImageView imageView, String thumbnailUrl) {
        if(CommonUtils.checkNullAndEmpty(thumbnailUrl)){
            imageView.setDefaultImageResId(R.drawable.btn_detail_senior);
            return;
        }
        imageView.setImageUrl(BuildConfig.BASE_IMAGE_URL + thumbnailUrl);
    }

    @BindingAdapter("ImageTakerDetailUrl")
    public static void setImageTakerDetailUrl(ANImageView imageView, String thumbnailUrl) {
        if(CommonUtils.checkNullAndEmpty(thumbnailUrl)){
            imageView.setDefaultImageResId(R.drawable.btn_senioruseredit_pic);
            return;
        }
        imageView.setImageUrl(BuildConfig.BASE_IMAGE_URL + thumbnailUrl);
    }
}
