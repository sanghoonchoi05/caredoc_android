package com.onestepmore.caredoc.ui.main.taker.card;

import com.onestepmore.caredoc.data.model.realm.Taker;

public interface TakerCardSelectListener {
    void onSelect(Taker taker);
}
