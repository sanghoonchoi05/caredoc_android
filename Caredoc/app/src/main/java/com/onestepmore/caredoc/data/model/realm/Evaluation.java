package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.ApiCommonObject;
import com.onestepmore.caredoc.data.model.api.object.EvaluationObject;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Evaluation extends RealmObject implements Parcelable {
    @PrimaryKey
    private int key;     // id는 해쉬코드로 생성
    private long fakeId;
    private int serviceTypeId;
    private String type;
    private String year;
    private String grade;

    public Evaluation(){}

    public Evaluation(Parcel parcel){
        this.key = parcel.readInt();
        this.fakeId = parcel.readLong();
        this.serviceTypeId = parcel.readInt();
        this.type = parcel.readString();
        this.year = parcel.readString();
        this.grade = parcel.readString();
    }

    public void setEvaluation(long fakeId, Service.TYPE type, EvaluationObject evaluationObject){
        this.fakeId = fakeId;
        this.serviceTypeId = evaluationObject.getServiceTypeId();
        this.type = type.toString();
        this.year = evaluationObject.getYear();
        this.grade = evaluationObject.getGrade();
        this.key = hashCodeForPrimaryKey();
    }

    public void setEvaluation(long fakeId, Service.TYPE type, Evaluation evaluation){
        this.fakeId = fakeId;
        this.serviceTypeId = evaluation.getServiceTypeId();
        this.type = type.toString();
        this.year = evaluation.getYear();
        this.grade = evaluation.getGrade();
        this.key = hashCodeForPrimaryKey();
    }

    public int getKey() {
        return key;
    }

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public long getFakeId() {
        return fakeId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(key);
        dest.writeLong(fakeId);
        dest.writeInt(serviceTypeId);
        dest.writeString(type);
        dest.writeString(year);
        dest.writeString(grade);
    }

    public static Parcelable.Creator<Evaluation> CREATOR = new Parcelable.Creator<Evaluation>() {

        @Override
        public Evaluation createFromParcel(Parcel source) {
            return new Evaluation(source);
        }

        @Override
        public Evaluation[] newArray(int size) {
            return new Evaluation[size];
        }

    };

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        Evaluation evaluation = (Evaluation) object;
        if (!String.valueOf(fakeId).equals(String.valueOf(evaluation.fakeId))) {
            return false;
        }

        if (serviceTypeId != 0 ? serviceTypeId != evaluation.serviceTypeId : evaluation.serviceTypeId != 0) {
            return false;
        }

        if(year != null ? year.equals(evaluation.year) : evaluation.year == null){
            return false;
        }

        if(type != null ? type.equals(evaluation.type) : evaluation.type == null){
            return false;
        }

        return grade != null ? grade.equals(evaluation.grade) : evaluation.grade == null;

    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + String.valueOf(fakeId).hashCode();
        result = 31 * result + serviceTypeId;
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (grade != null ? grade.hashCode() : 0);
        return result;
    }

    private int hashCodeForPrimaryKey() {
        int result = 17;
        result = 31 * result + String.valueOf(fakeId).hashCode();
        result = 31 * result + serviceTypeId;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        return result;
    }
}
