package com.onestepmore.caredoc.ui.main.profile.recently;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class RecentlyFragmentModule {
    @Provides
    RecentlyViewModel recentlyViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new RecentlyViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("RecentlyFragment")
    ViewModelProvider.Factory provideRecentlyViewModelProvider(RecentlyViewModel recentlyViewModel) {
        return new ViewModelProviderFactory<>(recentlyViewModel);
    }
}
