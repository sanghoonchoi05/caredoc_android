package com.onestepmore.caredoc.ui.account.register.email;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class EmailRegisterFragmentModule {
    @Provides
    EmailRegisterViewModel emailRegisterViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new EmailRegisterViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("EmailRegisterFragment")
    ViewModelProvider.Factory provideEmailRegisterViewModelProvider(EmailRegisterViewModel emailRegisterViewModel) {
        return new ViewModelProviderFactory<>(emailRegisterViewModel);
    }
}
