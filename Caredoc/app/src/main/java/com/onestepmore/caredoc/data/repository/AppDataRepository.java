package com.onestepmore.caredoc.data.repository;

import android.content.Context;
import android.support.annotation.NonNull;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.Gson;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.local.pref.PreferencesHelper;
import com.onestepmore.caredoc.data.model.api.CommonResponse_Deprecated;
import com.onestepmore.caredoc.data.model.api.account.LoginRequest;
import com.onestepmore.caredoc.data.model.api.account.LoginResponse;
import com.onestepmore.caredoc.data.model.api.account.PasswordEmailRequest;
import com.onestepmore.caredoc.data.model.api.account.PasswordEmailResponse;
import com.onestepmore.caredoc.data.model.api.account.RegisterRequest;
import com.onestepmore.caredoc.data.model.api.account.RegisterResponse;
import com.onestepmore.caredoc.data.model.api.account.UserResponse;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkFacilityResponse;
import com.onestepmore.caredoc.data.model.api.common.AddressResponse;
import com.onestepmore.caredoc.data.model.api.common.SuggestionResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalCoordinateRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalCoordinateResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalJusoRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalJusoResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalMailgunRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalMailgunResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalNaverUserInfoResponse;
import com.onestepmore.caredoc.data.model.api.object.BookmarkObject;
import com.onestepmore.caredoc.data.model.api.object.StaticServiceObject;
import com.onestepmore.caredoc.data.model.api.object.TakerObject;
import com.onestepmore.caredoc.data.model.api.statics.DiseaseResponse;
import com.onestepmore.caredoc.data.model.api.statics.StaticDataResponse;
import com.onestepmore.caredoc.data.model.api.statics.SubjectResponse;
import com.onestepmore.caredoc.data.model.api.taker.MyTakerResponse;
import com.onestepmore.caredoc.data.model.realm.Bookmark;
import com.onestepmore.caredoc.data.model.realm.BookmarkFacilityList;
import com.onestepmore.caredoc.data.model.realm.Evaluation;
import com.onestepmore.caredoc.data.model.realm.FacilityRecentlyList;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchTemp;
import com.onestepmore.caredoc.data.model.realm.LatLngAddress;
import com.onestepmore.caredoc.data.model.realm.LatLngFacility;
import com.onestepmore.caredoc.data.model.realm.LatLngTaker;
import com.onestepmore.caredoc.data.model.realm.Protector;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.data.model.realm.ServiceAddress;
import com.onestepmore.caredoc.data.model.realm.Suggestion;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticDisease;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.data.remote.ApiHeader;
import com.onestepmore.caredoc.data.remote.ApiHelper;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseNavigator;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

@Singleton
public class AppDataRepository implements AppDataSource {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    private final ApiHelper mApiHelper;
    private final Context mContext;
    private final Gson mGson;
    private final PreferencesHelper mPreferencesHelper;
    private final SchedulerProvider mSchedulerProvider;

    private LoggedInMode mLoggedInMode = LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT;

    @Inject
    public AppDataRepository(Context context,
                             SchedulerProvider schedulerProvider,
                             PreferencesHelper preferencesHelper,
                             ApiHelper apiHelper,
                             Gson gson) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
        mGson = gson;
        this.mSchedulerProvider = schedulerProvider;
    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onLogin() {

    }

    @Override
    public void onLogout() {

    }

    @Override
    public LoggedInMode getLoggedInMode() {
        return mLoggedInMode;
    }

    @Override
    public void setLoggedInMode(LoggedInMode mode) {
        mLoggedInMode = mode;
    }

    @Override
    public boolean isLoggedIn() {
        return getLoggedInMode() != LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT;
    }

    @Override
    public void handleApiError(ANError anError, @NonNull BaseNavigator navigator) {
        if(anError.getErrorCode() == HTTP_UNAUTHORIZED){
            navigator.showNeedLoginPopup();
            return;
        }
        String message = getApiError(anError);
        navigator.networkError(message);
    }

    @Override
    public void handleThrowable(Throwable throwable, @NonNull BaseNavigator navigator) {
        if (throwable instanceof ANError) {
            if(((ANError)throwable).getErrorCode() == HTTP_UNAUTHORIZED){
                navigator.showNeedLoginPopup();
                return;
            }
            String message = getApiError((ANError) throwable);
            navigator.networkError(message);
        }
        else{
            navigator.handleError(throwable);
        }
    }

    @Override
    public void handleApiError(String jsonErrorBody, @NonNull BaseNavigator navigator) {
        String errMsg = handleJsonErrorBody(jsonErrorBody);
        navigator.networkError(errMsg);
    }

    @Override
    public void handleApiError(Response response, @NonNull BaseNavigator navigator){
        if(response.code() == HTTP_UNAUTHORIZED){
            navigator.showNeedLoginPopup();
            return;
        }

        ResponseBody responseBody = response.body();
        long contentLength = responseBody.contentLength();
        if(contentLength != 0){
            BufferedSource source = responseBody.source();
            try{
                source.request(Long.MAX_VALUE); // Buffer the entire body.
            }catch (IOException e){

            }
            Buffer buffer = source.buffer();
            Charset charset = UTF8;
            MediaType contentType = responseBody.contentType();
            if (contentType != null) {
                charset = contentType.charset(UTF8);
            }

            String message = handleJsonErrorBody(buffer.clone().readString(charset));
            navigator.networkError(message);
        }
    }

    @Override
    public Flowable<Boolean> loadUserInfoFromServer(Realm realm, String email, String password, String provider, String socialId, String socialToken) {
        Flowable<Boolean> userResponseFlowable = doServerUserApiCall()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .concatMap(response -> {
                    User user = realm.where(User.class).equalTo("userId", response.getId()).findFirst();
                    if(user == null){
                        user = new User();
                    }
                    updateUser(realm, response, user, password, provider, socialId, socialToken);
                    return Flowable.just(true);
                });

        Flowable<Boolean> bookmarkFacilityResponseFlowable = getBookmarkApiCall()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .concatMap(response -> {
                    saveMyBookmark(realm, response);
                    return Flowable.just(true);
                });

        return Flowable.combineLatest(userResponseFlowable, bookmarkFacilityResponseFlowable,
                (userResponse , bookmarkResponse) -> userResponse && bookmarkResponse);
    }

    @Override
    public Flowable<Boolean> loadMyTakerFromServer(Realm realm){
        return getMyTakerApiCall()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .concatMap(response -> {
                    saveMyTaker(realm, response);
                    return Flowable.just(true);
                });
    }

    @Override
    public Flowable<Boolean> loadStaticData() {
        return doServerStaticDataApiCall()
                .concatMap(staticDataResponse -> {
                    setTotalFacilityCount(staticDataResponse.getAggregates().getFacilityTotal());
                    return Flowable.merge(getCategoryList(staticDataResponse), getServiceList(staticDataResponse));
                })
                .map(this::saveRealmObjectList);
    }

    @Override
    public Flowable<Boolean> loadStaticDisease() {
        return doServerDiseaseApiCall()
                .map(this::getDiseaseList)
                .map(this::saveRealmObjectList);
    }

    private List<StaticDisease> getDiseaseList(DiseaseResponse diseaseResponses){
        List<StaticDisease> diseaseList = new ArrayList<>();
        for(int i=0; i<diseaseResponses.getItems().size(); i++){
            StaticDisease disease = new StaticDisease();
            disease.setStaticDisease(diseaseResponses.getItems().get(i));
            diseaseList.add(disease);
        }

        return diseaseList;
    }

    private Flowable<List<? extends RealmObject>> getCategoryList(StaticDataResponse response){
        List<StaticCategory> categories = new ArrayList<>();
        for(int i=0; i<response.getCategories().getItems().size(); i++){
            StaticCategory category = new StaticCategory();
            category.setCategory(response.getCategories().getItems().get(i));
            categories.add(category);
        }

        return Flowable.just(categories);
    }

    private Flowable<List<? extends RealmObject>> getServiceList(StaticDataResponse response){
        List<StaticService> services = new ArrayList<>();
        for(StaticServiceObject staticServiceObject : response.getServices().getItems()){
            StaticService service = new StaticService();
            service.setService(staticServiceObject);
            services.add(service);
        }
        return Flowable.just(services);
    }

    private boolean saveRealmObjectList(List<? extends RealmObject> realmObjects){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(realmObjects);
        realm.commitTransaction();
        realm.close();
        return true;
    }

    @Override
    public Flowable<Boolean> loadStaticSubject() {
        return doServerSubjectApiCall()
                .map(subjectResponses -> {
                    return true;
                });
    }

    @Override
    public Flowable<Boolean> loadStaticAddress(String wholeAddressName, String wholeAddressFullName) {
        return doServerAddressApiCall(0, 0)
                .map(response -> getStaticAddressList(wholeAddressName, wholeAddressFullName, null, null ,0, 0, response))
                .map(this::saveRealmObjectList);
    }

    @Override
    public Flowable<Boolean> loadStaticAddress(String wholeAddressName, String wholeAddressFullName, String sidoCode, String gugunCode, double lat, double lng) {
        int sido = 0;
        int gugun = 0;
        if(sidoCode != null){
            sido = Integer.valueOf(sidoCode);
        }

        if(gugunCode != null){
            gugun = Integer.valueOf(gugunCode);
        }

        return doServerAddressApiCall(sido, gugun)
                .map(response -> getStaticAddressList(wholeAddressName, wholeAddressFullName, sidoCode, gugunCode, lat, lng, response))
                .map(this::saveRealmObjectList);
    }

    private List<StaticAddress> getStaticAddressList(String wholeAddressName, String wholeAddressFullName, String sidoCode, String gugunCode, double lat, double lng, AddressResponse response){
        List<StaticAddress> staticAddresses = new ArrayList<>();

        // 전국, 전체 추가..
        staticAddresses.add(getWholeStaticAddress(wholeAddressName, wholeAddressFullName, sidoCode, gugunCode, lat, lng));

        for(int i=0; i<response.getItems().size(); i++){
            StaticAddress address = new StaticAddress();
            address.setStaticAddress(response.getItems().get(i));
            staticAddresses.add(address);
        }

        return staticAddresses;
    }

    private StaticAddress getWholeStaticAddress(String wholeAddressName, String wholeAddressFullName, String sidoCode, String gugunCode, double lat, double lng){
        StaticAddress wholeAddress = new StaticAddress();
        StringBuilder bCode = new StringBuilder();

        String dongCode = null;

        if(sidoCode == null){
            sidoCode = "0";
            bCode.append(sidoCode);
        }
        else{
            bCode.append(sidoCode);
            if(gugunCode == null){
                gugunCode = "0";
                bCode.append(gugunCode);
            }
            else{
                dongCode = "0";
                bCode.append(gugunCode);
                bCode.append(dongCode);
            }
        }

        wholeAddress.setbCode(bCode.toString());
        wholeAddress.setSidoCode(sidoCode);
        wholeAddress.setGugunCode(gugunCode);
        wholeAddress.setDongCode(dongCode);
        wholeAddress.setAddressName(wholeAddressName);
        LatLngAddress latLngAddress = new LatLngAddress();
        latLngAddress.setId(bCode.toString());
        latLngAddress.setLatitude(lat);
        latLngAddress.setLongitude(lng);
        wholeAddress.setLatLng(latLngAddress);
        wholeAddress.setAddressFullname(wholeAddressFullName);

        return wholeAddress;
    }

    @Override
    public Flowable<Boolean> loadSuggestion(String keyword) {
        return doServerSuggestionApiCall(keyword)
                .map(this::getSuggestionList)
                .map(this::saveSuggestionList);
    }

    @Override
    public void clearSuggestion() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<Suggestion> suggestions = realm.where(Suggestion.class).findAll();
        if(suggestions != null){
            suggestions.deleteAllFromRealm();
        }
        realm.commitTransaction();
        realm.close();
    }

    private List<Suggestion> getSuggestionList(List<SuggestionResponse> suggestionResponseList){
        List<Suggestion> suggestions = new ArrayList<>();
        for(SuggestionResponse response : suggestionResponseList){
            Suggestion suggestion = new Suggestion();
            suggestion.setSuggestion(response);
            suggestions.add(suggestion);
        }

        return suggestions;
    }

    private boolean saveSuggestionList(List<Suggestion> suggestionList) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<Suggestion> suggestions = realm.where(Suggestion.class).findAll();
        if(suggestions != null){
            suggestions.deleteAllFromRealm();
        }
        realm.copyToRealmOrUpdate(suggestionList);
        realm.commitTransaction();
        realm.close();
        return true;
    }

    @Override
    public Flowable<Boolean> loadUserInfoFromServer(Realm realm, String email, String password) {
        return loadUserInfoFromServer(realm, email, password, "", "", "");
    }

    private void updateUser(Realm realm, UserResponse response, final User user, final String password,
                            final String provider, final String socialId, final String socialToken){
        if(user != null){
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm _realm) {
                    if(user.getUserId() == 0){
                        user.setUserId(response.getId());
                    }
                    user.setName(response.getName());
                    user.setEmail(response.getEmail());
                    user.setPassword(password);
                    user.setContact(response.getContact());
                    user.setPhotoUrl(response.getPhotoUrl());
                    user.setToken(getAccessToken());
                    user.setTokenType(getTokenType());
                    user.setExpiresIn(getExpiresIn());
                    user.setProvider(provider);
                    user.setSocialId(socialId);
                    user.setSocialToken(socialToken);
                    _realm.copyToRealmOrUpdate(user);
                }
            });
        }
    }

    private void updateUser(Realm realm, UserResponse response, final User user, final String password){
        updateUser(realm, response, user, password, "","","");
    }

    private String getApiError(ANError anError) {
        StringBuilder errMesage = new StringBuilder();
        if (anError.getErrorCode() != 0) {
            errMesage.append(handleJsonErrorBody(anError.getErrorBody()));
        } else {
            errMesage.append(mContext.getResources().getString(R.string.network_error_unknown));
        }

        AppLogger.i(getClass(), "Network Error =>" + errMesage);
        return errMesage.toString();
    }

    private String handleJsonErrorBody(String jsonErrorBody){
        StringBuilder errMessage = new StringBuilder();

        try {
            JSONObject obj = new JSONObject(jsonErrorBody);
            if (obj.has("errors")) {
                JSONObject errorObj = obj.getJSONObject("errors");

                if (errorObj != null) {
                    List<String> keyList = new ArrayList<>();
                    Iterator it = errorObj.keys();
                    while (it.hasNext()) {
                        String b = it.next().toString();
                        keyList.add(b);
                    }

                    for (int i = 0; i < keyList.size(); i++) {
                        String key = keyList.get(i);
                        JSONArray array = errorObj.getJSONArray(key);
                        errMessage.append(key);
                        errMessage.append(": ");
                        for (int j = 0; j < array.length(); j++) {
                            errMessage.append(array.get(j).toString());
                            if (j < array.length() - 1) {
                                errMessage.append("\n");
                            }
                        }

                        if (i < keyList.size() - 1) {
                            errMessage.append("\n");
                        }
                    }
                }

            } else {
                errMessage.append(obj.getString("message"));
            }
        } catch (JSONException e1) {
            if (BuildConfig.DEBUG) {
                // 디버그모드 일때
                errMessage.append(jsonErrorBody);
            }
        }

        return errMessage.toString();
    }

    @Override
    public void updateAccessToken(String accessToken, long expiresIn, String tokenType) {
        setAccessToken(accessToken);
        setExpiresIn(expiresIn);
        setTokenType(tokenType);

        mApiHelper.getApiHeader().getProtectedApiHeader().setAccessToken(tokenType + " " + accessToken);
    }

    @Override
    public void deleteAllFacility(Realm realm) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm _realm) {
                RealmResults<Bookmark> bookmarks = _realm.where(Bookmark.class).findAll();
                if(bookmarks != null){
                    bookmarks.deleteAllFromRealm();
                }

                RealmResults<BookmarkFacilityList> bookmarkFacilityLists = _realm.where(BookmarkFacilityList.class).findAll();
                if(bookmarkFacilityLists != null){
                    bookmarkFacilityLists.deleteAllFromRealm();
                }

                RealmResults<FacilityRecentlyList> facilityRecentlyLists = _realm.where(FacilityRecentlyList.class).findAll();
                if(facilityRecentlyLists != null){
                    facilityRecentlyLists.deleteAllFromRealm();
                }

                RealmResults<FacilitySearchList> facilitySearchLists = _realm.where(FacilitySearchList.class).findAll();
                if(facilitySearchLists != null){
                    facilitySearchLists.deleteAllFromRealm();
                }

                RealmResults<FacilitySearchTemp> facilitySearchTemps = _realm.where(FacilitySearchTemp.class).findAll();
                if(facilitySearchTemps != null){
                    facilitySearchTemps.deleteAllFromRealm();
                }

                RealmResults<Service> services = _realm.where(Service.class).findAll();
                if(services != null){
                    services.deleteAllFromRealm();
                }

                RealmResults<Evaluation> evaluations = _realm.where(Evaluation.class).findAll();
                if(evaluations != null){
                    evaluations.deleteAllFromRealm();
                }

                RealmResults<LatLngFacility> latLngFacilities = _realm.where(LatLngFacility.class).findAll();
                if(latLngFacilities != null){
                    latLngFacilities.deleteAllFromRealm();
                }
            }
        });
    }

    @Override
    public void deleteAllTaker(Realm realm) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm _realm) {
                RealmResults<Taker> takers = _realm.where(Taker.class).findAll();
                if(takers != null){
                    takers.deleteAllFromRealm();
                }

                RealmResults<ServiceAddress> serviceAddresses = _realm.where(ServiceAddress.class).findAll();
                if(serviceAddresses != null){
                    serviceAddresses.deleteAllFromRealm();
                }

                RealmResults<LatLngTaker> latLngTakers = _realm.where(LatLngTaker.class).findAll();
                if(latLngTakers != null){
                    latLngTakers.deleteAllFromRealm();
                }

                RealmResults<Protector> protectors = _realm.where(Protector.class).findAll();
                if(protectors != null){
                    protectors.deleteAllFromRealm();
                }
            }
        });
    }

    @Override
    public void saveMyBookmark(Realm realm, BookmarkFacilityResponse response) {
        List<Bookmark> bookmarkList = new ArrayList<>();
        List<BookmarkFacilityList> bookmarkFacilityList = new ArrayList<>();
        if(response != null && response.getTotal() > 0){
            for(BookmarkObject object : response.getItems()){
                Bookmark bookmark = new Bookmark();
                bookmark.setBookmark(object);
                bookmarkList.add(bookmark);
                BookmarkFacilityList facility = new BookmarkFacilityList();
                facility.setFacility(object.getBookmarkable());
                bookmarkFacilityList.add(facility);
            }
        }

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm _realm) {
                RealmResults<Bookmark> bookmarks = _realm.where(Bookmark.class).findAll();
                if(bookmarks != null){
                    bookmarks.deleteAllFromRealm();
                }

                RealmResults<BookmarkFacilityList> facilityLists = _realm.where(BookmarkFacilityList.class).findAll();
                if(facilityLists != null){
                    while (facilityLists.size() > 0){
                        BookmarkFacilityList facility = facilityLists.get(facilityLists.size()-1);
                        if(facility!= null){
                            //facility.deleteRealm(facility);
                            RealmObject.deleteFromRealm(facility);
                        }
                    }
                }

                if(response != null && response.getTotal() > 0){
                    _realm.copyToRealmOrUpdate(bookmarkList);
                    _realm.copyToRealmOrUpdate(bookmarkFacilityList);
                }
            }
        });
    }

    @Override
    public void saveMyTaker(Realm realm, MyTakerResponse response) {
        List<Taker> takerList = new ArrayList<>();
        if(response != null && response.getTotal() > 0){
            for(TakerObject takerObject : response.getItems()){
                Taker taker = new Taker();
                taker.setTaker(takerObject);
                takerList.add(taker);
            }
        }

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm _realm) {
                RealmResults<Taker> takers = _realm.where(Taker.class).findAll();
                if(takers != null){
                    takers.deleteAllFromRealm();
                }

                RealmResults<ServiceAddress> serviceAddresses = _realm.where(ServiceAddress.class).findAll();
                if(serviceAddresses != null){
                    serviceAddresses.deleteAllFromRealm();
                }

                if(takerList.size() > 0){
                    _realm.copyToRealmOrUpdate(takerList);
                }
            }
        });
    }

    @Override
    public void deleteMyBookmark(Realm realm) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm _realm) {
                RealmResults<Bookmark> bookmarks = _realm.where(Bookmark.class).findAll();
                if(bookmarks != null){
                    bookmarks.deleteAllFromRealm();
                }

                RealmResults<BookmarkFacilityList> facilityLists = _realm.where(BookmarkFacilityList.class).findAll();
                if(facilityLists != null){
                    while (facilityLists.size() > 0){
                        BookmarkFacilityList facility = facilityLists.get(facilityLists.size()-1);
                        if(facility!= null){
                            //facility.deleteRealm(facility);
                            RealmObject.deleteFromRealm(facility);
                        }
                    }
                }
            }
        });
    }

    @Override
    public String getAccessToken() {
        return mPreferencesHelper.getAccessToken();
    }

    @Override
    public void setAccessToken(String accessToken) {
        mPreferencesHelper.setAccessToken(accessToken);
    }

    @Override
    public void setExpiresIn(long expiresIn) {
        mPreferencesHelper.setExpiresIn(expiresIn);
    }

    @Override
    public long getExpiresIn() {
        return mPreferencesHelper.getExpiresIn();
    }

    @Override
    public String getTokenType() {
        return mPreferencesHelper.getTokenType();
    }

    @Override
    public void setTokenType(String tokenType) {
        mPreferencesHelper.setTokenType(tokenType);
    }

    @Override
    public boolean isLocationPermissionGranted() {
        return mPreferencesHelper.isLocationPermissionGranted();
    }

    @Override
    public void setLocationPermissionGranted(boolean granted) {
        mPreferencesHelper.setLocationPermissionGranted(granted);
    }

    @Override
    public String getLastKnownLocationLatitude() {
        return mPreferencesHelper.getLastKnownLocationLatitude();
    }

    @Override
    public void setLastKnownLocationLatitude(String latitude) {
        mPreferencesHelper.setLastKnownLocationLatitude(latitude);
    }

    @Override
    public String getLastKnownLocationLongitude() {
        return mPreferencesHelper.getLastKnownLocationLongitude();
    }

    @Override
    public void setLastKnownLocationLongitude(String longitude) {
        mPreferencesHelper.setLastKnownLocationLongitude(longitude);
    }

    @Override
    public int getTotalFacilityCount() {
        return mPreferencesHelper.getTotalFacilityCount();
    }

    @Override
    public void setTotalFacilityCount(int totalCount) {
        mPreferencesHelper.setTotalFacilityCount(totalCount);
    }

    @Override
    public boolean isShowEventPopup() {
        return mPreferencesHelper.isShowEventPopup();
    }

    @Override
    public void setShowEventPopup(boolean show) {
        mPreferencesHelper.setShowEventPopup(show);
    }

    @Override
    public Flowable<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request) {
        return mApiHelper.doServerLoginApiCall(request)
                .flatMap(response -> {
                    setLoggedInMode(LoggedInMode.LOGGED_IN_MODE_SERVER);
                    return Flowable.just(response);
                });
    }

    @Override
    public Flowable<LoginResponse> doSocialLoginApiCall(LoginRequest.SocialLoginPathParameter pathParameter, JSONObject request) {
        return mApiHelper.doSocialLoginApiCall(pathParameter, request)
                .flatMap(response -> {
                    setLoggedInMode(LoggedInMode.LOGGED_IN_MODE_FB);
                    return Flowable.just(response);
                });
    }

    @Override
    public Flowable<UserResponse> doServerUserApiCall() {
        return mApiHelper.doServerUserApiCall();
    }

    @Override
    public Single<JSONObject> doServerLoginApiCallGetJson(LoginRequest.ServerLoginRequest request) {
        return mApiHelper.doServerLoginApiCallGetJson(request)
                .flatMap(response -> {
                    setLoggedInMode(LoggedInMode.LOGGED_IN_MODE_FB);
                    return Single.just(response);
                });
    }

    @Override
    public Single<RegisterResponse> doServerRegisterApiCall(RegisterRequest request) {
        return mApiHelper.doServerRegisterApiCall(request);
    }

    @Override
    public void doServerLogoutApiCall(OkHttpResponseListener listener) {
        mApiHelper.doServerLogoutApiCall(listener);
    }

    @Override
    public Flowable<PasswordEmailResponse> doServerPasswordEmail(PasswordEmailRequest request) {
        return mApiHelper.doServerPasswordEmail(request);
    }

    @Override
    public Flowable<CommonResponse_Deprecated> doServerCommonApiCall() {
        return mApiHelper.doServerCommonApiCall();
    }

    @Override
    public Flowable<StaticDataResponse> doServerStaticDataApiCall() {
        return mApiHelper.doServerStaticDataApiCall();
    }

    @Override
    public Flowable<DiseaseResponse> doServerDiseaseApiCall() {
        return mApiHelper.doServerDiseaseApiCall();
    }

    @Override
    public Flowable<SubjectResponse> doServerSubjectApiCall() {
        return mApiHelper.doServerSubjectApiCall();
    }

    @Override
    public Flowable<JSONArray> doServerToolApiCall() {
        return mApiHelper.doServerToolApiCall();
    }

    @Override
    public Flowable<AddressResponse> doServerAddressApiCall(int sidoCode, int gugunCode) {
        return mApiHelper.doServerAddressApiCall(sidoCode, gugunCode);
    }

    @Override
    public Flowable<List<SuggestionResponse>> doServerSuggestionApiCall(String keyword) {
        return mApiHelper.doServerSuggestionApiCall(keyword);
    }

    @Override
    public Flowable<BookmarkFacilityResponse> getBookmarkApiCall() {
        return mApiHelper.getBookmarkApiCall();
    }

    @Override
    public Flowable<MyTakerResponse> getMyTakerApiCall() {
        return mApiHelper.getMyTakerApiCall();
    }

    @Override
    public Flowable<ExternalJusoResponse> getExternalJusoApiCall(ExternalJusoRequest.QueryParameter queryParameter) {
        return mApiHelper.getExternalJusoApiCall(queryParameter);
    }

    @Override
    public Flowable<ExternalCoordinateResponse> getExternalCoordinateApiCall(ExternalCoordinateRequest.QueryParameter queryParameter) {
        return mApiHelper.getExternalCoordinateApiCall(queryParameter);
    }

    @Override
    public Flowable<ExternalNaverUserInfoResponse> getExternalNaverUserInfo(String accessToken) {
        return mApiHelper.getExternalNaverUserInfo(accessToken);
    }

    @Override
    public Flowable<ExternalMailgunResponse> sendExternalMailgun(ExternalMailgunRequest.QueryParameter queryParameter) {
        return mApiHelper.sendExternalMailgun(queryParameter);
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHelper.getApiHeader();
    }
}
