package com.onestepmore.caredoc.data.model.api.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExternalMailgunResponse {
    @Expose
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }
}
