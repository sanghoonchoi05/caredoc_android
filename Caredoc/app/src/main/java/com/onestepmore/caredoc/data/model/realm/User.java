package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject implements Parcelable {
    @PrimaryKey
    private long userId;
    private String email;
    private String password;
    private String name;
    private String contact;
    private boolean emailVerifiedAt;
    private String photoUrl;
    private String socialPhotoUrl;
    private String token;
    private String tokenType;
    private long expiresIn;
    public String provider;
    public String socialId;
    public String socialToken;

    public User(){ }

    protected User(Parcel in) {
        userId = in.readLong();
        email = in.readString();
        password = in.readString();
        name = in.readString();
        contact = in.readString();
        emailVerifiedAt = in.readByte() != 0;
        photoUrl = in.readString();
        socialPhotoUrl = in.readString();
        token = in.readString();
        tokenType = in.readString();
        expiresIn = in.readLong();
        provider = in.readString();
        socialId = in.readString();
        socialToken = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(userId);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(name);
        dest.writeString(contact);
        dest.writeByte((byte)(emailVerifiedAt ? 1: 0));
        dest.writeString(photoUrl);
        dest.writeString(socialPhotoUrl);
        dest.writeString(token);
        dest.writeString(tokenType);
        dest.writeLong(expiresIn);
        dest.writeString(provider);
        dest.writeString(socialId);
        dest.writeString(socialToken);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public boolean isEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(boolean emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getSocialToken() {
        return socialToken;
    }

    public void setSocialToken(String socialToken) {
        this.socialToken = socialToken;
    }

    public String getSocialPhotoUrl() {
        return socialPhotoUrl;
    }

    public void setSocialPhotoUrl(String socialPhotoUrl) {
        this.socialPhotoUrl = socialPhotoUrl;
    }
}
