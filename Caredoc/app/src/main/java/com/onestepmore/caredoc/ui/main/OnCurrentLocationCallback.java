package com.onestepmore.caredoc.ui.main;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public interface OnCurrentLocationCallback {
    void onLocation(LatLng latLng, boolean granted);
}
