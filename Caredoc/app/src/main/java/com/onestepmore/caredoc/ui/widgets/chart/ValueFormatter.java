package com.onestepmore.caredoc.ui.widgets.chart;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

public class ValueFormatter implements IValueFormatter {

    private String [] mValues;

    public ValueFormatter(String [] values){
        mValues = values;
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        int position = (int)value;
        return mValues.length > position ? mValues[position] : "";
    }
}
