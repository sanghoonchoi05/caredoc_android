package com.onestepmore.caredoc.ui.main.profile.recently;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.realm.FacilityRecentlyList;
import com.onestepmore.caredoc.databinding.FRecentlyBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.RECENTLY_CLICK_BACK;

public class RecentlyFragment extends BaseFragment<FRecentlyBinding, RecentlyViewModel> implements RecentlyNavigator {

    @Inject
    @Named("RecentlyFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private RecentlyViewModel mRecentlyViewModel;

    private RealmResults<FacilityRecentlyList> mFacilityRecentlyLists;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_recently;
    }

    @Override
    public RecentlyViewModel getViewModel() {
        mRecentlyViewModel = ViewModelProviders.of(this, mViewModelFactory).get(RecentlyViewModel.class);
        return mRecentlyViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRecentlyViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fRecentlyToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }

        // 시설 리스트 UI 구성
        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(),new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_facility));
        getViewDataBinding().fRecentlyRecyclerView.addItemDecoration(dividerItemDecoration);

        // 시설 검색 리스트 Realm Change Observer 추가
        mFacilityRecentlyLists = getViewModel().getRealm().where(FacilityRecentlyList.class).sort("date",Sort.DESCENDING).findAll();
        mFacilityRecentlyLists.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<FacilityRecentlyList>>() {
            @Override
            public void onChange(RealmResults<FacilityRecentlyList> results, OrderedCollectionChangeSet changeSet) {
                UIUtils.setTextIncludeAnnotation(
                        getBaseActivity(),
                        R.string.f_recently_desc,
                        getViewDataBinding().fRecentlyDesc,
                        String.valueOf(results.size()));
                AppLogger.i(getClass(), "RecentlyFragment realm Facility size: " + results.size());
            }
        });

        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.f_recently_desc,
                getViewDataBinding().fRecentlyDesc,
                String.valueOf(mFacilityRecentlyLists.size()));

        RecentlyFacilityAdapter searchAdapter = new RecentlyFacilityAdapter(mFacilityRecentlyLists);
        searchAdapter.setMainNavigator(((MainActivity)getBaseActivity()));
        getViewDataBinding().fRecentlyRecyclerView.setAdapter(searchAdapter);
        getViewDataBinding().fRecentlyRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AmplitudeManager.getInstance().logEvent(RECENTLY_CLICK_BACK);
                BackStackManager.getInstance().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * OnDestroy we unbind from the model and controller.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mFacilityRecentlyLists!=null){
            mFacilityRecentlyLists.removeAllChangeListeners();
        }
    }
}
