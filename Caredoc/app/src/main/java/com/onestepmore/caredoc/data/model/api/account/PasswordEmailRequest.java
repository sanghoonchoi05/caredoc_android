package com.onestepmore.caredoc.data.model.api.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class PasswordEmailRequest {
    @Expose
    @SerializedName("email")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
