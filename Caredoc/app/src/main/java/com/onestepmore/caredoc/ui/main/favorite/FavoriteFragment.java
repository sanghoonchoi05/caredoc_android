package com.onestepmore.caredoc.ui.main.favorite;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.realm.Bookmark;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.databinding.FFavoriteBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.favorite.detail.FavoriteDetailFragment;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.BOOKMARK_CLICK_AUTH;

public class FavoriteFragment extends BaseFragment<FFavoriteBinding, FavoriteViewModel> implements FavoriteNavigator {

    @Inject
    @Named("FavoriteFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private FavoriteViewModel mFavoriteViewModel;
    private RealmResults<Bookmark> mBookmarkRealmResults;
    private int[] mCountList;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_favorite;
    }

    @Override
    public FavoriteViewModel getViewModel() {
        if (getBaseActivity() != null) {
            mFavoriteViewModel = ViewModelProviders.of(this, mViewModelFactory).get(FavoriteViewModel.class);
        }
        return mFavoriteViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFavoriteViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getViewDataBinding().setIsLogin(false);
        if (getViewModel().getDataManager().isLoggedIn()) {
            getViewDataBinding().setIsLogin(true);
            RealmResults<StaticCategory> staticCategories = RealmDataAccessor.getInstance().getStaticCategory();
            mCountList = new int[staticCategories.size()];

            FavoriteFacilityAdapter adapter = new FavoriteFacilityAdapter(this);
            getViewDataBinding().fFavoriteRecyclerView.setAdapter(adapter);
            getViewDataBinding().fFavoriteRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter.setCategoryList(staticCategories);

            int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_favorite_facility_item_space);
            getViewDataBinding().fFavoriteRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels, 0));

            loadBookmarkCount();
            getViewModel().loadMyBookmarkFromServer();
        }
    }

    @Override
    public void onItemClick(StaticCategory category) {
        BackStackManager.getInstance().addChildFragment(FavoriteDetailFragment.newInstance(category), R.id.a_main_content_fragment_layout);
    }

    @Override
    public void openLoginFragment() {
        /*Intent intent = getBaseActivity().newIntent(AccountMainActivity.class);
        intent.putExtra("logout", true);
        startActivity(intent);*/
    }

    @Override
    public void openAccountMainActivity() {
        AmplitudeManager.getInstance().logEvent(BOOKMARK_CLICK_AUTH);
        Intent intent = getBaseActivity().newIntent(AccountMainActivity.class);
        intent.putExtra("logout", false);
        startActivity(intent);
    }

    public void loadBookmarkCount() {
        mBookmarkRealmResults = getViewModel().getRealm().where(Bookmark.class).equalTo("show", true).findAll();

        setCount(mBookmarkRealmResults);
        changeCount();

        mBookmarkRealmResults.addChangeListener(new RealmChangeListener<RealmResults<Bookmark>>() {
            @Override
            public void onChange(RealmResults<Bookmark> bookmarks) {
                setCount(bookmarks);
                changeCount();
            }
        });
    }

    private void setCount(RealmResults<Bookmark> bookmarks){
        RealmResults<StaticCategory> staticCategories = getViewModel().getRealm().where(StaticCategory.class).findAll();

        for(int i=0; i<mCountList.length; i++){
            mCountList[i] = 0;
        }

        // 전체 서치카테고리는 여기서 셋!
        mCountList[0] = bookmarks.size();

        for (int i = 0; i < staticCategories.size(); i++) {
            StaticCategory searchCategory = staticCategories.get(i);
            // 전체 서치카테고리는 넘어가자
            if (searchCategory == null || searchCategory.getQueryServiceTypeIds().size() == 0) {
                continue;
            }

            // 북마크에 저장된 저장된 서치카테고리 비교
            for (Bookmark bookmark : bookmarks) {
                for (String searchText : bookmark.getSearchCategoryList()) {
                    if (searchCategory.getText().equals(searchText)) {
                        mCountList[i] += 1;
                        break;
                    }
                }
            }
        }
    }

    private void changeCount() {
        FavoriteFacilityAdapter adapter = (FavoriteFacilityAdapter) getViewDataBinding().fFavoriteRecyclerView.getAdapter();
        if (adapter != null) {
            adapter.setCountList(mCountList);
        }

        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.f_favorite_desc,
                getViewDataBinding().fFavoriteDesc.fFavoriteDesc,
                String.valueOf(mBookmarkRealmResults.size()));
        getViewDataBinding().setEmpty(mBookmarkRealmResults.size() == 0);
    }

    /**
     * OnDestroy we unbind from the model and controller.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {

        if(mBookmarkRealmResults != null){
            mBookmarkRealmResults.removeAllChangeListeners();
        }
    }

    @Override
    public void onShow() {
        super.onShow();

        if (getViewModel() == null) {
            return;
        }

        if (getViewModel().getDataManager().isLoggedIn()) {
            getViewModel().loadMyBookmarkFromServer();
        }
    }
}
