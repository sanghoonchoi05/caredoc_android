package com.onestepmore.caredoc.ui.carecoordi.guide;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class GuideFragmentModule {
    @Provides
    GuideViewModel guideViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new GuideViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("GuideFragment")
    ViewModelProvider.Factory provideGuideViewModelProvider(GuideViewModel guideViewModel) {
        return new ViewModelProviderFactory<>(guideViewModel);
    }
}
