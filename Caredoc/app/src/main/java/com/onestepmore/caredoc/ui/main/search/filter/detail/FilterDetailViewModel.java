package com.onestepmore.caredoc.ui.main.search.filter.detail;

import android.app.Application;
import android.view.View;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class FilterDetailViewModel extends BaseViewModel<FilterDetailNavigator> {

    public FilterDetailViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onApply(){
        getNavigator().onApplyFilter();
    }

    public void onOrderByGradeClick(View view){
        getNavigator().onOrderByGradeClick();
    }

    public void onOrderByReviewClick(View view){
        getNavigator().onOrderByReviewClick();
    }

    public void onOrderByRatingClick(View view){
        getNavigator().onOrderByRatingClick();
    }
}
