package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class StaticServiceObject {
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("category")
    private String category;
    @Expose
    @SerializedName("type_name")
    private String typeName;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("warning")
    private String warning;
    @Expose
    @SerializedName("evaluation_source_id")
    private int evaluationSourceId;
    @Expose
    @SerializedName("lti_fee_sheet_types")
    private LtiFeeSheetTypesObject ltiFeeSheetTypesObject;
    @Expose
    @SerializedName("rating_relations")
    private RatingRelationsObject ratingRelations;

    public int getId() {
        return id;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getDescription() {
        return description;
    }

    public String getWarning() {
        return warning;
    }

    public int getEvaluationSourceId() {
        return evaluationSourceId;
    }

    public LtiFeeSheetTypesObject getLtiFeeSheetTypesObject() {
        return ltiFeeSheetTypesObject;
    }

    public String getCategory() {
        return category;
    }

    public RatingRelationsObject getRatingRelations() {
        return ratingRelations;
    }
}
