package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TakerDiseaseObject {
    @Expose
    @SerializedName("id")
    private long id;

    @Expose
    @SerializedName("medical_disease_type_code")
    private String medicalDiseaseTypeCode;

    @Expose
    @SerializedName("taker_id")
    private long takerId;

    @Expose
    @SerializedName("disease")
    private DiseaseObject disease;

    public long getId() {
        return id;
    }

    public String getMedicalDiseaseTypeCode() {
        return medicalDiseaseTypeCode;
    }

    public long getTakerId() {
        return takerId;
    }

    public DiseaseObject getDisease() {
        return disease;
    }
}
