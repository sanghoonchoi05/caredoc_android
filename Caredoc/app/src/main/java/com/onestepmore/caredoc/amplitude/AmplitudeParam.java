package com.onestepmore.caredoc.amplitude;

public class AmplitudeParam {

    public enum MODE{
        NONE,
        LIST,
        MAP
    }

    public enum DIRECTION{
        LEFT,
        RIGHT
    }

    public enum BOOLEAN{
        TRUE,
        FALSE
    }

    public enum ZOOM{
        IN,
        OUT
    }

    public enum ORDER_BY_MODE{
        GRADE,
        REVIEW,
        RATING
    }

    public enum TOUCH{
        SERVICE,
        PICTURE,
        ITEM
    }

    public static class Key{
        public static final String VALUE = "value";
        public static final String MODE = "mode";
        public static final String MAX_COUNT = "max_count";
        public static final String COUNT = "count";
        public static final String TYPE = "type";
        public static final String GRADE = "grade";
        public static final String RATING = "rating";
        public static final String GRANTED = "granted";
        public static final String ZOOM = "zoom";
        public static final String SERVICE = "service";
        public static final String TOUCH = "touch";
        public static final String SECOND = "second";
    }

    public static class Value{
        public static final String EMAIL = "email";
        public static final String FACEBOOK = "facebook";
        public static final String NAVER = "naver";
        public static final String KAKAO = "kakao";
        public static final String AUTO_COMPLETE = "auto_complete";
        public static final String GO = "go";
        public static final String COMPLETE = "complete";
        public static final String FACILITY = "facility";
        public static final String ADDRESS = "address";
        public static final String TAKER = "taker";
    }
}
