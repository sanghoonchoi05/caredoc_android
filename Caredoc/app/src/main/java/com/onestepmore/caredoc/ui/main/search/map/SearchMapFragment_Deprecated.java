package com.onestepmore.caredoc.ui.main.search.map;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.databinding.FSearchMapBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

public class SearchMapFragment_Deprecated extends BaseFragment<FSearchMapBinding, SearchMapViewModel> implements
        SearchMapNavigator/*,
        OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraMoveStartedListener*/
{

    @Inject
    @Named("SearchMapFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private SearchMapViewModel mSearchMapViewModel;

    private int mCurrentSnapPosition = RecyclerView.NO_POSITION;
    private static int NO_KEY = -1;

    private GoogleMap mMap = null;
    private List<FacilitySearchList> mFacilityList = null;
    private SparseArray<Marker> mMarkerMap = new SparseArray<>();
    private List<Marker> mClusterMarkerMap = new ArrayList<>();
    private int mSelectedFacilityKey = NO_KEY;
    private int mClickedFacilityKey = NO_KEY;

    private float mCameraZoomVal = 7f;
    private View mMarkerView = null;
    private LinearLayout mMarkerBackground = null;
    private AppCompatImageView mMarkerImg = null;

    private View mClusterView = null;
    private AppCompatTextView mClusterCountView = null;
    private AppCompatTextView mClusterTitleView = null;

    private DisplayMetrics mDisplayMetrics;
    private RecyclerView.SmoothScroller mSmoothScroller;

    //private ClusterManager<MyClusterItem> mClusterManager;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_search_map;
    }

    @Override
    public SearchMapViewModel getViewModel() {
        mSearchMapViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchMapViewModel.class);
        return mSearchMapViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSearchMapViewModel.setNavigator(this);

        mDisplayMetrics = new DisplayMetrics();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*getBaseActivity().setSupportActionBar(getViewDataBinding().fSearchMapToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.vec_ic_close_gray900_24dp);
        }*/

        /*mFacilityList = TestDataUtils.getCareFacilityTempDataMore(getBaseActivity());
        mSelectedFacilityKey = mFacilityList.size() > 0 ? mFacilityList.get(0).key : NO_KEY;

        SearchMapCareFacilityAdapter adapter = new SearchMapCareFacilityAdapter(this);
        adapter.setFacilityList(mFacilityList);
        adapter.setRatePointFormat(getResources().getString(R.string.i_care_facility_rate_point));
        adapter.setReviewCountFormat(getResources().getString(R.string.i_care_facility_review_count));
        getViewDataBinding().fSearchMapRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManagerCenter = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fSearchMapRecyclerView.setLayoutManager(layoutManagerCenter);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_care_facility_map_item_space);
        getViewDataBinding().fSearchMapRecyclerView.addItemDecoration(new SpacesItemDecoration(0, spacingInPixels, true));

        spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_care_facility_map_item_snap_space);
        SnapHelper snapHelperCenter = new StartPagerSnapHelper(0*//*spacingInPixels*//*);
        snapHelperCenter.attachToRecyclerView(getViewDataBinding().fSearchMapRecyclerView);

        getViewDataBinding().fSearchMapRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int snapPosition = RecyclerView.NO_POSITION;
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if(layoutManager!=null){
                    View snapView = snapHelperCenter.findSnapView(layoutManager);
                    if(snapView!=null){
                        snapPosition = layoutManager.getPosition(snapView);
                        onChangeSnapPosition(snapPosition);
                    }
                }

                AppLogger.i(getClass(), "Scrolled x: " + dx);
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        mSmoothScroller = new LinearSmoothScroller(getBaseActivity()) {
            @Override protected int getHorizontalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FragmentManager fragmentManager = getChildFragmentManager();
        SupportMapFragment supportMapFragment = (SupportMapFragment)fragmentManager.findFragmentById(R.id.f_search_map_fragment);
        if(supportMapFragment != null){
            //supportMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onList() {

    }

    @Override
    public void onMyLocation() {

    }
/*

    private void onChangeSnapPosition(int snapPosition){
        boolean needChangeSnap = false;
        if(mClickedFacilityKey != NO_KEY){
            if(snapPosition == getPositionWithKey(mClickedFacilityKey)){
                needChangeSnap = true;
            }
        }
        else{
            if(snapPosition != mCurrentSnapPosition){
                needChangeSnap = true;
            }
        }

        if(needChangeSnap){
            mClickedFacilityKey = NO_KEY;
            mCurrentSnapPosition = snapPosition;
            AppLogger.i(getClass(), "snap position: " + mCurrentSnapPosition);

            if(mMap != null){
                // 기존 마커를 노말상태로
                if(mSelectedFacilityKey != NO_KEY){
                    changeMarker(mSelectedFacilityKey, false);
                }

                // 바뀐 마커를 선택상태로
                Facility facility = mFacilityList.get(mCurrentSnapPosition);
                changeMarker(facility.key, true);

                moveCameraWithLoaction(facility.latLng, true);
            }
        }
    }

    private void moveCameraWithLoaction(CameraUpdate cameraUpdate, boolean animate){
        getViewModel().setTouchedMap(false);

        if(animate){
            mMap.animateCamera(cameraUpdate, 500, null);
        }
        else{
            mMap.moveCamera(cameraUpdate);
        }
    }

    private void moveCameraWithLoaction(LatLng latLng, boolean animate){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(mCameraZoomVal)
                .build();

        moveCameraWithLoaction(CameraUpdateFactory.newCameraPosition(cameraPosition), animate);
    }

    private void changeMarker(int key, boolean selected){
        // 기존 마커를 지움
        Marker marker = mMarkerMap.get(key);
        if(marker != null){
            marker.remove();
        }

        Facility facility = null;
        for(Facility fac : mFacilityList)
        {
            if(fac.key == key){
                facility = fac;
            }
        }

        if(facility != null){
            // 새로운 마커를 다시 그림
            BitmapDescriptor descriptor = getMarkerBitmapBitmapDescriptor(selected);
            markerLocationOnMap(facility, descriptor);
            if(selected){
                mSelectedFacilityKey = facility.key;
            }
            else{
                mSelectedFacilityKey = NO_KEY;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnCameraMoveStartedListener(this);

        mMarkerView = (LayoutInflater.from(getBaseActivity()).inflate(R.layout.v_facility_grade_marker, null));
        //mMarkerBackground = mMarkerView.findViewById(R.id.v_facility_grade_marker_background);
        mMarkerImg = mMarkerView.findViewById(R.id.v_facility_grade_marker_img);

        mClusterView = (LayoutInflater.from(getBaseActivity()).inflate(R.layout.v_cluster_marker_normal, null));
        mClusterCountView = mClusterView.findViewById(R.id.v_cluster_marker_normal_count);
        mClusterTitleView = mClusterView.findViewById(R.id.v_cluster_marker_normal_title);

        addFacilityMarker();
        setCameraPosition();

        moveCameraWithLoaction(TestDataUtils.getKoreaCenterPosition(), false);

        //setUpClusterer();

        */
/*try {
            GeoJsonLayer layer = new GeoJsonLayer(mMap, R.raw.gwanyang1_dong, getContext());
            layer.addLayerToMap();

        }catch (JSONException e){

        }catch (IOException ioe){

        }*//*

    }

    public void addClusterMarker(){
        List<MyClusterItem> clusterItemList = TestDataUtils.getClusterItems(mMap);
        if(clusterItemList != null){
            int index = 0;
            for(MyClusterItem item : clusterItemList){
                BitmapDescriptor descriptor = getClusterMarkerBitmapBitmapDescriptor(item.getTitle(), item.getCount());
                clusterLocationOnMap(index, item.getPosition(), descriptor);
                index++;
            }
        }
    }

    public void addFacilityMarker(){
        if(mFacilityList != null){
            for(Facility facility : mFacilityList){
                BitmapDescriptor descriptor = getMarkerBitmapBitmapDescriptor(facility.key==mSelectedFacilityKey);
                markerLocationOnMap(facility, descriptor);
            }
        }
    }

    public void setCameraPosition(){
        if(mFacilityList != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for(Facility facility : mFacilityList){
                LatLngBounds bounds = builder.include(facility.latLng).build();
                moveCameraWithLoaction(CameraUpdateFactory.newLatLngBounds(bounds, 0), false);
            }
        }
    }

    @Override
    public void onCameraIdle() {
        CameraPosition position = mMap.getCameraPosition();
        float zoom = position.zoom;
        long delay = 0L;
        if(mCameraZoomVal != zoom){
            if(mCameraZoomVal == 0f){
                delay = 800L;
            }
            mCameraZoomVal = zoom;
        }
        //addKMMarker(delay);

        for(Marker marker : mClusterMarkerMap){
            marker.remove();
        }

        addClusterMarker();

        AppLogger.i(getClass(), "CameraZoom: " + zoom);
    }

    private BitmapDescriptor getMarkerBitmapBitmapDescriptor(boolean selected){
        if(selected){
            mMarkerBackground.setBackgroundResource(R.drawable.facility_grade_marker);
            mMarkerImg.setImageResource(R.drawable.ic_hospi_red);
        }else{
            mMarkerBackground.setBackgroundResource(R.drawable.facility_grade_normal_marker);
            mMarkerImg.setImageResource(R.drawable.ic_hospi_red);
        }

        BitmapDescriptor descriptor = null;
        getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        Bitmap bitmap = GraphicsUtils.createBitmapFromView(mMarkerView, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
        if(bitmap != null){
            descriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return descriptor;
    }

    private BitmapDescriptor getClusterMarkerBitmapBitmapDescriptor(String title, int count){
        mClusterTitleView.setText(title);
        mClusterCountView.setText(String.valueOf(count));

        BitmapDescriptor descriptor = null;
        getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        Bitmap bitmap = GraphicsUtils.createBitmapFromView(mClusterView, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
        if(bitmap != null){
            descriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return descriptor;
    }

    private void markerLocationOnMap(Facility facility, BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor == null) {
            return ;
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(facility.latLng)
                .icon(bitmapDescriptor);
        Marker marker =mMap.addMarker(markerOptions);
        mMarkerMap.put(facility.key, marker);
    }

    private void clusterLocationOnMap(int index, LatLng latLng, BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor == null) {
            return ;
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(latLng)
                .icon(bitmapDescriptor);
        Marker marker =mMap.addMarker(markerOptions);
        mClusterMarkerMap.add(marker);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        // 기존 마커를 노말상태로
        if(mSelectedFacilityKey != NO_KEY){
            changeMarker(mSelectedFacilityKey, false);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int key = NO_KEY;

        for(int i=0; i<mMarkerMap.size(); i++){
            if(mMarkerMap.get(mMarkerMap.keyAt(i)).getServiceId().equals(marker.getServiceId())){
                key = mMarkerMap.keyAt(i);
                break;
            }
        }

        //int key = mMarkerMap.indexOfValue(marker);
        if(key != NO_KEY && mSelectedFacilityKey == key){
            return true;
        }

        // 바뀐 마커를 선택상태로
        */
/*changeMarker(mSelectedFacilityKey, false);
        changeMarker(key, true);
        moveCameraWithLoaction(marker.getPosition(), true);*//*


        moveToScrollPosition(key);

        return true;
    }

    private void moveToScrollPosition(int key){
        int position = getPositionWithKey(key);
        if(position != NO_KEY){
            //getViewDataBinding().fSearchMapRecyclerView.smoothScrollToPosition(i);
            */
/*RecyclerView.LayoutManager manager = getViewDataBinding().fSearchMapRecyclerView.getLayoutManager();
            if(mSmoothScroller!= null && manager!=null){
                mSmoothScroller.setTargetPosition(position);
                manager.startSmoothScroll(mSmoothScroller);
                mClickedFacilityKey = key;
            }*//*

        }
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            AppLogger.i(getClass(), "The user gestured on the map.");
            getViewModel().setTouchedMap(true);
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {
            AppLogger.i(getClass(), "The user tapped something on the map.");
            getViewModel().setTouchedMap(true);
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {
            AppLogger.i(getClass(), "The app moved the camera.");
            getViewModel().setTouchedMap(false);
        }

        // 기존 마커를 노말상태로
        if(getViewModel().getTouchedMap().get() && mSelectedFacilityKey != NO_KEY){
            changeMarker(mSelectedFacilityKey, false);
            mClickedFacilityKey = NO_KEY;
        }
    }

    private int getPositionWithKey(int key){
        int position = NO_KEY;
        if(mFacilityList != null){
            for(int i=0; i<mFacilityList.size(); i++){
                Facility facility = mFacilityList.get(i);
                if(facility.key == key){
                    position = i;
                    break;
                }
            }
        }

        return position;
    }

    @Override
    public void onDetach() {
        ((MainActivity)getBaseActivity()).showBottomTab(true);
        super.onDetach();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSearchRetryClick() {
        Marker marker = mMarkerMap.get(0);
        onMarkerClick(marker);
    }

    @Override
    public void onSearchCareFacilityOnList() {
        BackStackManager.getInstance().onBackPressed();
    }

    private void setUpClusterer() {
        // Position the map.
        //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 10));

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<MyClusterItem>(getBaseActivity(), mMap);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);

        // Add cluster items (markers) to the cluster manager.
        addItems();

        mClusterManager.setAnimation(true);
    }

    private void addItems() {
        int count = 100000;
        List<LatLng> latLngs = TestDataUtils.getRandomKoreaLocations(count);

        // Add ten cluster items in close proximity, for purposes of this example.
        for (int i = 0; i < count; i++) {
            MyClusterItem offsetItem = new MyClusterItem(latLngs.get(i), "title " + i, "snippet " + i);
            mClusterManager.addItem(offsetItem);
        }
    }
    */
}

