package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.AttachesObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

public class Attach extends RealmObject implements Parcelable {
    @PrimaryKey
    private long id;
    private String attachType;
    private int order;
    private String url;
    private String thumbnailUrl;

    public Attach() {
    }

    protected Attach(Parcel in) {
        id = in.readLong();
        attachType = in.readString();
        order = in.readInt();
        url = in.readString();
        thumbnailUrl = in.readString();
    }

    public void setAttach(AttachesObject.Item item){
        this.id = item.getId();
        this.attachType = item.getAttachType();
        this.order = item.getOrder();
        this.url = item.getUrl();
        this.thumbnailUrl = item.getThumbnailUrl();
    }

    public static final Creator<Attach> CREATOR = new Creator<Attach>() {
        @Override
        public Attach createFromParcel(Parcel in) {
            return new Attach(in);
        }

        @Override
        public Attach[] newArray(int size) {
            return new Attach[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(attachType);
        dest.writeInt(order);
        dest.writeString(url);
        dest.writeString(thumbnailUrl);
    }

    public long getId() {
        return id;
    }

    public String getAttachType() {
        return attachType;
    }

    public int getOrder() {
        return order;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }
}
