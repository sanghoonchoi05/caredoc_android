package com.onestepmore.caredoc.ui.main.taker.add;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface AddTakerNavigator extends BaseNavigator {
    void onCompleteAddTaker();
    void onCompleteModifyTaker();
}
