package com.onestepmore.caredoc.ui.common;

public interface BirthClickListener {
    void onShowYearList();
    void onShowMonthList();
    void onShowDateList();
}
