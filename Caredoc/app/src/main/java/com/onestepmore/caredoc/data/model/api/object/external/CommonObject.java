package com.onestepmore.caredoc.data.model.api.object.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonObject {
    @Expose
    @SerializedName("errorMessage")
    public String errorMessage;

    @Expose
    @SerializedName("totalCount")
    public String totalCount;

    @Expose
    @SerializedName("errorCode")
    public String errorCode;

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
