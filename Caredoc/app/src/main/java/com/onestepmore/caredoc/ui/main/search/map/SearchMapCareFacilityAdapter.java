package com.onestepmore.caredoc.ui.main.search.map;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchMapList;
import com.onestepmore.caredoc.databinding.IFacilityHorizontalBinding;
import com.onestepmore.caredoc.ui.main.FacilityHorizontalAdapter;
import com.onestepmore.caredoc.ui.main.MainNavigator;

import io.realm.OrderedRealmCollection;

public class SearchMapCareFacilityAdapter extends FacilityHorizontalAdapter<FacilitySearchMapList, FacilityHorizontalAdapter.FacilityHorizontalViewHolder>
{
    public SearchMapCareFacilityAdapter(@Nullable OrderedRealmCollection<FacilitySearchMapList> data) {
        super(data, MainNavigator.FROM.SEARCH_FACILITY_MAP);
    }

    @NonNull
    @Override
    public FacilityHorizontalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFacilityHorizontalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_horizontal, viewGroup, false);
        return new FacilityHorizontalViewHolder(binding);
    }
}