package com.onestepmore.caredoc.ui.account.login;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.databinding.FLoginBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.account.AccountViewModel;
import com.onestepmore.caredoc.ui.account.login.forgot.LoginForgotFragment;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.widgets.CommonDialog;

import javax.inject.Inject;
import javax.inject.Named;

public class LoginFragment extends BaseFragment<FLoginBinding,LoginViewModel> implements LoginNavigator {

    @Inject
    @Named("LoginFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private LoginViewModel mLoginViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_login;
    }

    @Override
    public LoginViewModel getViewModel() {
        mLoginViewModel = ViewModelProviders.of(this, mViewModelFactory).get(LoginViewModel.class);
        return mLoginViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = getViewModel().getRealm().where(User.class).findFirst();
        if(user!=null){
            getViewDataBinding().fLoginEmailEdit.setText(user.getEmail());
        }
        mLoginViewModel.setEditTextChangeObserver(getViewDataBinding().fLoginEmailEdit, getViewDataBinding().fLoginPasswordEdit);
    }

    @Override
    public void openMainActivity() {
        BackStackManager.getInstance().removeAllFragment();
        Intent intent = getBaseActivity().newIntent(MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("index", 0);
        startActivity(intent);
        getBaseActivity().finish();
    }

    @Override
    public void showResetPasswordView() {
        BackStackManager.getInstance().addChildFragment(new LoginForgotFragment(), R.id.v_app_bar_start_fragment_layout);
    }

    @Override
    public void onLoginFailed() {
        /*CommonDialog dialog = new CommonDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", getString(R.string.warning_msg_login_failed));
        bundle.putString("desc", getString(R.string.warning_msg_invalid_email_and_password));
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), "common_dialog");*/

        showCommonDialog(getString(R.string.warning_msg_login_failed), getString(R.string.warning_msg_invalid_email_and_password));
    }
}
