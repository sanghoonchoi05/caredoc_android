package com.onestepmore.caredoc.data.model.api.object.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JusoResultObject {
    @Expose
    @SerializedName("common")
    public CommonJusoObject common;

    @Expose
    @SerializedName("juso")
    public List<JusoObject> jusoList;

    public CommonJusoObject getCommon() {
        return common;
    }

    public List<JusoObject> getJusoList() {
        return jusoList;
    }
}
