package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.DiseaseObject;
import com.onestepmore.caredoc.data.model.api.statics.DiseaseResponse;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StaticDisease extends RealmObject implements Parcelable {
    @PrimaryKey
    private String typeCode;
    private String typeName;

    public StaticDisease(){}

    protected StaticDisease(Parcel in) {
        typeCode = in.readString();
        typeName = in.readString();
    }

    public void setStaticDisease(DiseaseResponse.Item response){
        this.typeCode = response.getTypeCode();
        this.typeName = response.getTypeName();
    }

    public void setStaticDisease(DiseaseObject diseaseObject){
        this.typeCode = diseaseObject.getTypeCode();
        this.typeName = diseaseObject.getTypeName();
    }

    public static final Creator<StaticDisease> CREATOR = new Creator<StaticDisease>() {
        @Override
        public StaticDisease createFromParcel(Parcel in) {
            return new StaticDisease(in);
        }

        @Override
        public StaticDisease[] newArray(int size) {
            return new StaticDisease[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeCode);
        dest.writeString(typeName);
    }

    public String getTypeCode() {
        return typeCode;
    }

    public String getTypeName() {
        return typeName;
    }
}
