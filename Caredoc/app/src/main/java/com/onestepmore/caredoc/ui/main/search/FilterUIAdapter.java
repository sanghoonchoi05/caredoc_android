package com.onestepmore.caredoc.ui.main.search;

import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.ISearchFacilityFilterBinding;

import java.util.List;

public class FilterUIAdapter extends RecyclerView.Adapter<FilterUIAdapter.FacilityFilterViewHolder> {
    private List<? extends FilterUI> mFacilityFilterList = null;
    private String [] mFacilityFilterStrList;

    @Nullable
    private final FilterUICallback mCallback;

    public FilterUIAdapter(@Nullable FilterUICallback filterUICallback){
        mCallback = filterUICallback;
    }

    public List<? extends FilterUI> getFacilityFilterList(){
        return mFacilityFilterList;
    }

    public void setFacilityFilterList(final List<? extends FilterUI> facilityCategoryList, final String [] defaultFilterStrList){
        mFacilityFilterList = facilityCategoryList;
        mFacilityFilterStrList = defaultFilterStrList;
        notifyDataSetChanged();
    }

    public void changeFacilityFilter(int position, String text, boolean isSelect){
        FilterUI filter = mFacilityFilterList.get(position);
        filter.filterName = text;
        filter.isSelect = isSelect;
        notifyItemChanged(position);
    }

    public void changeFacilityFilter(int position, boolean expose){
        FilterUI filter = mFacilityFilterList.get(position);
        filter.expose = expose;
        filter.isSelect = expose;
        notifyItemChanged(position);
    }

    public void initFacilityFilter(int position){
        FilterUI filterUI = mFacilityFilterList.get(position);
        filterUI.filterName = mFacilityFilterStrList[position];
        filterUI.isSelect = false;
        notifyItemChanged(position);
    }


    @NonNull
    @Override
    public FacilityFilterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ISearchFacilityFilterBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_search_facility_filter, viewGroup, false);
        return new FacilityFilterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FacilityFilterViewHolder facilityViewHolder, int position) {
        facilityViewHolder.binding.setFilter(mFacilityFilterList.get(position));
        facilityViewHolder.binding.setPosition(position);
        facilityViewHolder.binding.setCallback(mCallback);
        facilityViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mFacilityFilterList == null ? 0 : mFacilityFilterList.size();
    }

    @BindingAdapter("filterButtonSelected")
    public static void setFilterButtonSelected(AppCompatButton button, boolean selected) {
        button.setSelected(selected);
    }

    @BindingAdapter("filterSelected")
    public static void setFilterSelected(ConstraintLayout constraintLayout, boolean selected) {
        constraintLayout.setSelected(selected);
    }

    @BindingAdapter("filterSelected")
    public static void setFilterSelected(AppCompatTextView textView, boolean selected) {
        textView.setSelected(selected);
    }

    @BindingAdapter("filterSelected")
    public static void setFilterSelected(AppCompatImageView imageView, boolean selected) {
        imageView.setSelected(selected);
    }

    static class FacilityFilterViewHolder extends RecyclerView.ViewHolder {
        final ISearchFacilityFilterBinding binding;

        private FacilityFilterViewHolder(ISearchFacilityFilterBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}