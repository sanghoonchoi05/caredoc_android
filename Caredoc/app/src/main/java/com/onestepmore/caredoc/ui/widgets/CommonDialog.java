package com.onestepmore.caredoc.ui.widgets;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.DialogCommonBinding;
import com.onestepmore.caredoc.ui.base.BaseDialog;

@Deprecated
public class CommonDialog extends BaseDialog implements View.OnClickListener{

    private View.OnClickListener listener;
    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // the content
        DialogCommonBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()), R.layout.dialog_common, null, false);
        Bundle bundle = getArguments();
        if(bundle != null){
            binding.setTitle(bundle.getString("title"));
            binding.setDesc(bundle.getString("desc"));
        }

        if(listener != null){
            binding.dialogCommonButton.setOnClickListener(listener);
        }else{
            binding.dialogCommonButton.setOnClickListener(this);
        }

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);


        return dialog;
    }

    @Override
    public void onClick(View v) {
        dismissDialog(getTag());
    }
}
