package com.onestepmore.caredoc.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceAddressRequest {
    public static class Address{
        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("code")
        private String code;

        @Expose
        @SerializedName("detail")
        private String detail;

        @Expose
        @SerializedName("road")
        private Road road;

        @Expose
        @SerializedName("location")
        private Location location;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public Road getRoad() {
            return road;
        }

        public void setRoad(Road road) {
            this.road = road;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }
    }

    public static class Road{
        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("code")
        private String code;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

    public static class Location{
        @Expose
        @SerializedName("lat")
        private String lat;

        @Expose
        @SerializedName("lng")
        private String lng;

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }
    }
}
