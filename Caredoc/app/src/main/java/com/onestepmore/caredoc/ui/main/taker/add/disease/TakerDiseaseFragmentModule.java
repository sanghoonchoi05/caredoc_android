package com.onestepmore.caredoc.ui.main.taker.add.disease;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerDiseaseFragmentModule {
    @Provides
    TakerDiseaseViewModel takerDiseaseViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerDiseaseViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerDiseaseFragment")
    ViewModelProvider.Factory provideTakerDiseaseViewModelProvider(TakerDiseaseViewModel takerDiseaseViewModel) {
        return new ViewModelProviderFactory<>(takerDiseaseViewModel);
    }
}
