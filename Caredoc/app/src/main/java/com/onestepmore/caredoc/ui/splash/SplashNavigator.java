package com.onestepmore.caredoc.ui.splash;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface SplashNavigator extends BaseNavigator {
    void loadComplete();
    void onLogout();
}
