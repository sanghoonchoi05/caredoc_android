package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class BookmarkObject {
    @Expose
    @SerializedName("bookmarkable")
    private FacilitySimpleObject bookmarkable;

    public FacilitySimpleObject getBookmarkable() {
        return bookmarkable;
    }
}
