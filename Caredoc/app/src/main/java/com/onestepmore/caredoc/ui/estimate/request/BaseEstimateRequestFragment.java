package com.onestepmore.caredoc.ui.estimate.request;

import android.databinding.ViewDataBinding;

import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.base.BaseViewModel;

public abstract class BaseEstimateRequestFragment<T extends ViewDataBinding,V extends BaseViewModel> extends BaseFragment<T,V> {
    public void onExit(){
        ((EstimateRequestActivity)getBaseActivity()).onExit();
    }
}
