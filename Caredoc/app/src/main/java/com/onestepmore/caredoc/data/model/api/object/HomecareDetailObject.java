package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomecareDetailObject {
    @Expose
    @SerializedName("id")
    private long id;
    @Expose
    @SerializedName("external_id")
    private long externalId;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("post")
    private String post;
    @Expose
    @SerializedName("sido_code")
    private int sidoCode;
    @Expose
    @SerializedName("sigugun_code")
    private int sigugunCode;
    @Expose
    @SerializedName("dong_code")
    private int dongCode;
    @Expose
    @SerializedName("law_dong_code")
    private int lawDongCode;
    @Expose
    @SerializedName("ri_code")
    private int riCode;
    @Expose
    @SerializedName("detail_address")
    private String detailAddress;
    @Expose
    @SerializedName("road_code")
    private long roadCode;
    @Expose
    @SerializedName("building_number_primary")
    private int buildingNumberPrimary;
    @Expose
    @SerializedName("building_number_sub")
    private int buildingNumberSub;
    @Expose
    @SerializedName("floor")
    private int floor;
    @Expose
    @SerializedName("tel_1")
    private String tel1;
    @Expose
    @SerializedName("tel_2")
    private String tel2;
    @Expose
    @SerializedName("tel_3")
    private String tel3;
    @Expose
    @SerializedName("long_term_designated_at")
    private String longTermDesignatedAt;
    @Expose
    @SerializedName("installed_at")
    private String installedAt;

    @Expose
    @SerializedName("single_room_count")
    private int singleRoomCount;
    @Expose
    @SerializedName("double_room_count")
    private int doubleRoomCount;
    @Expose
    @SerializedName("triple_room_count")
    private int tripleRoomCount;
    @Expose
    @SerializedName("quadruple_room_count")
    private int quadrupleRoomCount;
    @Expose
    @SerializedName("special_room_count")
    private int specialRoomCount;
    @Expose
    @SerializedName("office_count")
    private int officeCount;
    @Expose
    @SerializedName("medical_room_count")
    private int medicalRoomCount;
    @Expose
    @SerializedName("function_room_count")
    private int functionRoomCount;
    @Expose
    @SerializedName("program_room_count")
    private int programRoomCount;
    @Expose
    @SerializedName("cuisine_room_count")
    private int cuisineRoomCount;
    @Expose
    @SerializedName("toilet_count")
    private int toiletCount;
    @Expose
    @SerializedName("bath_room_count")
    private int bathRoomCount;
    @Expose
    @SerializedName("laundry_room_count")
    private int laundryRoomCount;
    @Expose
    @SerializedName("chief_count")
    private int chiefCount;
    @Expose
    @SerializedName("office_director_count")
    private int officeDirectorCount;
    @Expose
    @SerializedName("social_worker_count")
    private int socialWorkerCount;
    @Expose
    @SerializedName("full_time_doctor_count")
    private int fullTimeDoctorCount;
    @Expose
    @SerializedName("part_time_doctor_count")
    private int partTimeDoctorCount;
    @Expose
    @SerializedName("nurse_count")
    private int nurseCount;
    @Expose
    @SerializedName("nursing_assistant_count")
    private int nursingAssistantCount;
    @Expose
    @SerializedName("dentist_hygienist_count")
    private int dentistHygienistCount;
    @Expose
    @SerializedName("physical_therapist_count")
    private int physicalTherapistCount;
    @Expose
    @SerializedName("occupational_therapist_count")
    private int occupationalTherapistCount;
    @Expose
    @SerializedName("first_care_giver_count")
    private int firstCareGiverCount;
    @Expose
    @SerializedName("second_care_giver_count")
    private int secondCareGiverCount;
    @Expose
    @SerializedName("grace_care_giver_count")
    private int graceCareGiverCount;
    @Expose
    @SerializedName("officer_count")
    private int officerCount;
    @Expose
    @SerializedName("nutritionist_count")
    private int nutritionistCount;
    @Expose
    @SerializedName("cook_count")
    private int cookCount;
    @Expose
    @SerializedName("hygiener_count")
    private int hygienerCount;
    @Expose
    @SerializedName("manager_count")
    private int managerCount;
    @Expose
    @SerializedName("assistant_count")
    private int assistantCount;
    @Expose
    @SerializedName("etc_count")
    private int etcCount;
    @Expose
    @SerializedName("total_men_count")
    private int totalMenCount;
    @Expose
    @SerializedName("current_male_count")
    private int currentMaleCount;
    @Expose
    @SerializedName("current_female_count")
    private int currentFemaleCount;
    @Expose
    @SerializedName("waiting_male_count")
    private int waitingMaleCount;
    @Expose
    @SerializedName("waiting_female_count")
    private int waitingFemaleCount;
    @Expose
    @SerializedName("homepage")
    private String homepage;
    @Expose
    @SerializedName("traffic")
    private String traffic;
    @Expose
    @SerializedName("parking_lot")
    private String parkingLot;
    @Expose
    @SerializedName("geo_location")
    private ApiCommonObject.GeoLocation geoLocation;

    @Expose
    @SerializedName("programs")
    private List<ProgramObject_Deprecated> programs;

    @Expose
    @SerializedName("unpaid_items")
    private List<UnpaidItemsObject> unpaidItems;

    @Expose
    @SerializedName("welfare_tools")
    private List<WelfareToolsObject> welfareTools;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getExternalId() {
        return externalId;
    }

    public void setExternalId(long externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public int getSidoCode() {
        return sidoCode;
    }

    public void setSidoCode(int sidoCode) {
        this.sidoCode = sidoCode;
    }

    public int getSigugunCode() {
        return sigugunCode;
    }

    public void setSigugunCode(int sigugunCode) {
        this.sigugunCode = sigugunCode;
    }

    public int getDongCode() {
        return dongCode;
    }

    public void setDongCode(int dongCode) {
        this.dongCode = dongCode;
    }

    public int getLawDongCode() {
        return lawDongCode;
    }

    public void setLawDongCode(int lawDongCode) {
        this.lawDongCode = lawDongCode;
    }

    public int getRiCode() {
        return riCode;
    }

    public void setRiCode(int riCode) {
        this.riCode = riCode;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public long getRoadCode() {
        return roadCode;
    }

    public void setRoadCode(long roadCode) {
        this.roadCode = roadCode;
    }

    public int getBuildingNumberPrimary() {
        return buildingNumberPrimary;
    }

    public void setBuildingNumberPrimary(int buildingNumberPrimary) {
        this.buildingNumberPrimary = buildingNumberPrimary;
    }

    public int getBuildingNumberSub() {
        return buildingNumberSub;
    }

    public void setBuildingNumberSub(int buildingNumberSub) {
        this.buildingNumberSub = buildingNumberSub;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getTel3() {
        return tel3;
    }

    public void setTel3(String tel3) {
        this.tel3 = tel3;
    }

    public String getLongTermDesignatedAt() {
        return longTermDesignatedAt;
    }

    public void setLongTermDesignatedAt(String longTermDesignatedAt) {
        this.longTermDesignatedAt = longTermDesignatedAt;
    }

    public String getInstalledAt() {
        return installedAt;
    }

    public void setInstalledAt(String installedAt) {
        this.installedAt = installedAt;
    }

    public int getSingleRoomCount() {
        return singleRoomCount;
    }

    public void setSingleRoomCount(int singleRoomCount) {
        this.singleRoomCount = singleRoomCount;
    }

    public int getDoubleRoomCount() {
        return doubleRoomCount;
    }

    public void setDoubleRoomCount(int doubleRoomCount) {
        this.doubleRoomCount = doubleRoomCount;
    }

    public int getTripleRoomCount() {
        return tripleRoomCount;
    }

    public void setTripleRoomCount(int tripleRoomCount) {
        this.tripleRoomCount = tripleRoomCount;
    }

    public int getQuadrupleRoomCount() {
        return quadrupleRoomCount;
    }

    public void setQuadrupleRoomCount(int quadrupleRoomCount) {
        this.quadrupleRoomCount = quadrupleRoomCount;
    }

    public int getSpecialRoomCount() {
        return specialRoomCount;
    }

    public void setSpecialRoomCount(int specialRoomCount) {
        this.specialRoomCount = specialRoomCount;
    }

    public int getOfficeCount() {
        return officeCount;
    }

    public void setOfficeCount(int officeCount) {
        this.officeCount = officeCount;
    }

    public int getMedicalRoomCount() {
        return medicalRoomCount;
    }

    public void setMedicalRoomCount(int medicalRoomCount) {
        this.medicalRoomCount = medicalRoomCount;
    }

    public int getFunctionRoomCount() {
        return functionRoomCount;
    }

    public void setFunctionRoomCount(int functionRoomCount) {
        this.functionRoomCount = functionRoomCount;
    }

    public int getProgramRoomCount() {
        return programRoomCount;
    }

    public void setProgramRoomCount(int programRoomCount) {
        this.programRoomCount = programRoomCount;
    }

    public int getCuisineRoomCount() {
        return cuisineRoomCount;
    }

    public void setCuisineRoomCount(int cuisineRoomCount) {
        this.cuisineRoomCount = cuisineRoomCount;
    }

    public int getToiletCount() {
        return toiletCount;
    }

    public void setToiletCount(int toiletCount) {
        this.toiletCount = toiletCount;
    }

    public int getBathRoomCount() {
        return bathRoomCount;
    }

    public void setBathRoomCount(int bathRoomCount) {
        this.bathRoomCount = bathRoomCount;
    }

    public int getLaundryRoomCount() {
        return laundryRoomCount;
    }

    public void setLaundryRoomCount(int laundryRoomCount) {
        this.laundryRoomCount = laundryRoomCount;
    }

    public int getChiefCount() {
        return chiefCount;
    }

    public void setChiefCount(int chiefCount) {
        this.chiefCount = chiefCount;
    }

    public int getOfficeDirectorCount() {
        return officeDirectorCount;
    }

    public void setOfficeDirectorCount(int officeDirectorCount) {
        this.officeDirectorCount = officeDirectorCount;
    }

    public int getSocialWorkerCount() {
        return socialWorkerCount;
    }

    public void setSocialWorkerCount(int socialWorkerCount) {
        this.socialWorkerCount = socialWorkerCount;
    }

    public int getFullTimeDoctorCount() {
        return fullTimeDoctorCount;
    }

    public void setFullTimeDoctorCount(int fullTimeDoctorCount) {
        this.fullTimeDoctorCount = fullTimeDoctorCount;
    }

    public int getPartTimeDoctorCount() {
        return partTimeDoctorCount;
    }

    public void setPartTimeDoctorCount(int partTimeDoctorCount) {
        this.partTimeDoctorCount = partTimeDoctorCount;
    }

    public int getNurseCount() {
        return nurseCount;
    }

    public void setNurseCount(int nurseCount) {
        this.nurseCount = nurseCount;
    }

    public int getNursingAssistantCount() {
        return nursingAssistantCount;
    }

    public void setNursingAssistantCount(int nursingAssistantCount) {
        this.nursingAssistantCount = nursingAssistantCount;
    }

    public int getDentistHygienistCount() {
        return dentistHygienistCount;
    }

    public void setDentistHygienistCount(int dentistHygienistCount) {
        this.dentistHygienistCount = dentistHygienistCount;
    }

    public int getOccupationalTherapistCount() {
        return occupationalTherapistCount;
    }

    public void setOccupationalTherapistCount(int occupationalTherapistCount) {
        this.occupationalTherapistCount = occupationalTherapistCount;
    }

    public int getFirstCareGiverCount() {
        return firstCareGiverCount;
    }

    public void setFirstCareGiverCount(int firstCareGiverCount) {
        this.firstCareGiverCount = firstCareGiverCount;
    }

    public int getSecondCareGiverCount() {
        return secondCareGiverCount;
    }

    public void setSecondCareGiverCount(int secondCareGiverCount) {
        this.secondCareGiverCount = secondCareGiverCount;
    }

    public int getGraceCareGiverCount() {
        return graceCareGiverCount;
    }

    public void setGraceCareGiverCount(int graceCareGiverCount) {
        this.graceCareGiverCount = graceCareGiverCount;
    }

    public int getOfficerCount() {
        return officerCount;
    }

    public void setOfficerCount(int officerCount) {
        this.officerCount = officerCount;
    }

    public int getNutritionistCount() {
        return nutritionistCount;
    }

    public void setNutritionistCount(int nutritionistCount) {
        this.nutritionistCount = nutritionistCount;
    }

    public int getCookCount() {
        return cookCount;
    }

    public void setCookCount(int cookCount) {
        this.cookCount = cookCount;
    }

    public int getHygienerCount() {
        return hygienerCount;
    }

    public void setHygienerCount(int hygienerCount) {
        this.hygienerCount = hygienerCount;
    }

    public int getManagerCount() {
        return managerCount;
    }

    public void setManagerCount(int managerCount) {
        this.managerCount = managerCount;
    }

    public int getAssistantCount() {
        return assistantCount;
    }

    public void setAssistantCount(int assistantCount) {
        this.assistantCount = assistantCount;
    }

    public int getEtcCount() {
        return etcCount;
    }

    public void setEtcCount(int etcCount) {
        this.etcCount = etcCount;
    }

    public int getTotalMenCount() {
        return totalMenCount;
    }

    public void setTotalMenCount(int totalMenCount) {
        this.totalMenCount = totalMenCount;
    }

    public int getCurrentMaleCount() {
        return currentMaleCount;
    }

    public void setCurrentMaleCount(int currentMaleCount) {
        this.currentMaleCount = currentMaleCount;
    }

    public int getCurrentFemaleCount() {
        return currentFemaleCount;
    }

    public void setCurrentFemaleCount(int currentFemaleCount) {
        this.currentFemaleCount = currentFemaleCount;
    }

    public int getWaitingMaleCount() {
        return waitingMaleCount;
    }

    public void setWaitingMaleCount(int waitingMaleCount) {
        this.waitingMaleCount = waitingMaleCount;
    }

    public int getWaitingFemaleCount() {
        return waitingFemaleCount;
    }

    public void setWaitingFemaleCount(int waitingFemaleCount) {
        this.waitingFemaleCount = waitingFemaleCount;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getTraffic() {
        return traffic;
    }

    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }

    public String getParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(String parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ApiCommonObject.GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(ApiCommonObject.GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public List<ProgramObject_Deprecated> getPrograms() {
        return programs;
    }

    public void setPrograms(List<ProgramObject_Deprecated> programs) {
        this.programs = programs;
    }

    public List<UnpaidItemsObject> getUnpaidItems() {
        return unpaidItems;
    }

    public void setUnpaidItems(List<UnpaidItemsObject> unpaidItems) {
        this.unpaidItems = unpaidItems;
    }

    public int getPhysicalTherapistCount() {
        return physicalTherapistCount;
    }

    public void setPhysicalTherapistCount(int physicalTherapistCount) {
        this.physicalTherapistCount = physicalTherapistCount;
    }

    public List<WelfareToolsObject> getWelfareTools() {
        return welfareTools;
    }

    public void setWelfareTools(List<WelfareToolsObject> welfareTools) {
        this.welfareTools = welfareTools;
    }
}
