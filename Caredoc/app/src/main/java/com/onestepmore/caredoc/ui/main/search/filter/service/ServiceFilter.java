package com.onestepmore.caredoc.ui.main.search.filter.service;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;

import java.util.ArrayList;
import java.util.List;

public class ServiceFilter implements Parcelable {
    private int id;
    private String serviceName;
    private List<String> grades;
    private String gradeText;
    private boolean checked;
    private boolean evaluation;

    public ServiceFilter(){}

    protected ServiceFilter(Parcel in) {
        id = in.readInt();
        serviceName = in.readString();
        grades = new ArrayList<>();
        in.readStringList(grades);
        gradeText = in.readString();
        checked = in.readByte() != 0;
        evaluation = in.readByte() != 0;
    }

    public void initGrades(StaticCategory category){
        for(String grade : category.getGrades()){
            addGrade(grade);
        }
    }

    public static final Creator<ServiceFilter> CREATOR = new Creator<ServiceFilter>() {
        @Override
        public ServiceFilter createFromParcel(Parcel in) {
            return new ServiceFilter(in);
        }

        @Override
        public ServiceFilter[] newArray(int size) {
            return new ServiceFilter[size];
        }
    };

    public void setEvaluation(boolean evaluation) {
        this.evaluation = evaluation;
    }

    public boolean isEvaluation() {
        return evaluation;
    }

    public void setGradeText(String gradeText) {
        this.gradeText = gradeText;
    }

    public String getGradeText() {
        return gradeText;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void addGrade(String grade){
        if(grades == null){
            grades = new ArrayList<>();
        }

        if(!grades.contains(grade)){
            grades.add(grade);
        }
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isChecked() {
        return checked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(serviceName);
        dest.writeStringList(grades);
        dest.writeString(gradeText);
        dest.writeByte((byte) (checked ? 1 : 0));
        dest.writeByte((byte) (evaluation ? 1 : 0));
    }
}
