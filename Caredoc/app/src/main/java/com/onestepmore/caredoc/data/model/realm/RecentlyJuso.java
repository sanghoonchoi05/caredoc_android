package com.onestepmore.caredoc.data.model.realm;

import android.arch.persistence.room.Ignore;
import android.os.Parcel;

import java.util.Calendar;

import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class RecentlyJuso implements JusoRealmModel {
    @Ignore
    public static final int MAX = 10;

    @PrimaryKey
    private String id;
    private String roadAddress;
    private String jibunAddress;
    private String roadCode;
    private String zipCode;
    private String buildingName;
    private String detail;
    private String dongCode;
    private String buildingMainNo;
    private String buildingSubNo;
    private String isUnderground;
    private String entX;
    private String entY;
    private double lat;
    private double lng;
    private long date;

    public RecentlyJuso() {}

    protected RecentlyJuso(Parcel in) {
        id = in.readString();
        roadAddress = in.readString();
        jibunAddress = in.readString();
        roadCode = in.readString();
        zipCode = in.readString();
        buildingName = in.readString();
        detail = in.readString();
        dongCode = in.readString();
        buildingMainNo = in.readString();
        buildingSubNo = in.readString();
        isUnderground = in.readString();
        entX = in.readString();
        entY = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        date = in.readLong();
    }

    public static final Creator<RecentlyJuso> CREATOR = new Creator<RecentlyJuso>() {
        @Override
        public RecentlyJuso createFromParcel(Parcel in) {
            return new RecentlyJuso(in);
        }

        @Override
        public RecentlyJuso[] newArray(int size) {
            return new RecentlyJuso[size];
        }
    };

    public void setJuso(JusoRealmModel juso){
        this.id = juso.getId();
        this.roadCode = juso.getRoadCode();
        this.roadAddress = juso.getRoadAddress();
        this.jibunAddress = juso.getJibunAddress();
        this.zipCode = juso.getZipCode();
        this.buildingName = juso.getBuildingName();
        this.detail =juso.getDetail();
        this.dongCode = juso.getDongCode();
        this.buildingMainNo = juso.getBuildingMainNo();
        this.buildingSubNo = juso.getBuildingSubNo();
        this.isUnderground = juso.isUnderground();
        this.date = Calendar.getInstance().getTime().getTime();
        this.entX = juso.getEntX();
        this.entY = juso.getEntY();
        this.lat = juso.getLat();
        this.lng = juso.getLng();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(roadAddress);
        dest.writeString(jibunAddress);
        dest.writeString(roadCode);
        dest.writeString(zipCode);
        dest.writeString(buildingName);
        dest.writeString(detail);
        dest.writeString(dongCode);
        dest.writeString(buildingMainNo);
        dest.writeString(buildingSubNo);
        dest.writeString(isUnderground);
        dest.writeString(entX);
        dest.writeString(entY);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeLong(date);
    }

    @Override
    public String getId() {
        return id;
    }
    @Override
    public void setId(String id) {
        this.id = id;
    }
    @Override
    public String getRoadAddress() {
        return roadAddress;
    }
    @Override
    public void setRoadAddress(String roadAddress) {
        this.roadAddress = roadAddress;
    }
    @Override
    public String getJibunAddress() {
        return jibunAddress;
    }
    @Override
    public void setJibunAddress(String jibunAddress) {
        this.jibunAddress = jibunAddress;
    }
    @Override
    public String getRoadCode() {
        return roadCode;
    }
    @Override
    public void setRoadCode(String roadCode) {
        this.roadCode = roadCode;
    }
    @Override
    public String getZipCode() {
        return zipCode;
    }
    @Override
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    @Override
    public String getBuildingName() {
        return buildingName;
    }
    @Override
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }
    @Override
    public String getDetail() {
        return detail;
    }
    @Override
    public void setDetail(String detail) {
        this.detail = detail;
    }
    @Override
    public String getDongCode() {
        return dongCode;
    }
    @Override
    public void setDongCode(String dongCode) {
        this.dongCode = dongCode;
    }
    @Override
    public String getBuildingMainNo() {
        return buildingMainNo;
    }
    @Override
    public void setBuildingMainNo(String buildingMainNo) {
        this.buildingMainNo = buildingMainNo;
    }
    @Override
    public String getBuildingSubNo() {
        return buildingSubNo;
    }
    @Override
    public void setBuildingSubNo(String buildingSubNo) {
        this.buildingSubNo = buildingSubNo;
    }
    @Override
    public String isUnderground() {
        return isUnderground;
    }
    @Override
    public void setUnderground(String  underground) {
        isUnderground = underground;
    }
    @Override
    public String getEntX() {
        return entX;
    }
    @Override
    public void setEntX(String entX) {
        this.entX = entX;
    }
    @Override
    public String getEntY() {
        return entY;
    }
    @Override
    public void setEntY(String entY) {
        this.entY = entY;
    }
    @Override
    public double getLat() {
        return lat;
    }
    @Override
    public double getLng() {
        return lng;
    }
    @Override
    public void setLat(double lat) {
        this.lat = lat;
    }
    @Override
    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getDate() {
        return date;
    }

    @Override
    public boolean isModelSame(Object obj) {
        return false;
    }

    @Override
    public boolean isModelContentsSame(Object obj) {
        return false;
    }
}
