package com.onestepmore.caredoc.ui.account.register.email;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class EmailRegisterFragmentProvider {
    @ContributesAndroidInjector(modules = EmailRegisterFragmentModule.class)
    abstract EmailRegisterFragment provideEmailRegisterFragmentFactory();
}
