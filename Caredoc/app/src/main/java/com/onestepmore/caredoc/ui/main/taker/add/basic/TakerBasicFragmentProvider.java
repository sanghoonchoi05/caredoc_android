package com.onestepmore.caredoc.ui.main.taker.add.basic;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerBasicFragmentProvider {
    @ContributesAndroidInjector(modules = TakerBasicFragmentModule.class)
    abstract TakerBasicFragment provideTakerBasicFragmentFactory();
}
