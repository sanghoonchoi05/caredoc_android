package com.onestepmore.caredoc.ui.main.search.filter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.IServiceGradeFilterBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilter;

import java.util.List;

public class ServiceFilterAdapter extends RecyclerView.Adapter<ServiceFilterAdapter.ServiceFilterViewHolder> implements CompoundButton.OnCheckedChangeListener {
    private ServiceFilterCallback mCallback;
    private List<ServiceFilter> mServiceList;
    public void setCallback(ServiceFilterCallback callback) {
        this.mCallback = callback;
    }

    public void addAll(List<ServiceFilter> serviceList){
        if(mServiceList!=null){
            mServiceList.clear();
        }

        mServiceList = serviceList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ServiceFilterAdapter.ServiceFilterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IServiceGradeFilterBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_service_grade_filter, viewGroup, false);
        return new ServiceFilterAdapter.ServiceFilterViewHolder(binding, mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceFilterAdapter.ServiceFilterViewHolder serviceFilterViewHolder, int position) {
        serviceFilterViewHolder.binding.setCallback(mCallback);
        serviceFilterViewHolder.binding.executePendingBindings();
        serviceFilterViewHolder.binding.setServiceFilter(mServiceList.get(position));
        serviceFilterViewHolder.binding.setPosition(position);
        serviceFilterViewHolder.binding.iDialogServiceGradeFilter.setChecked(mServiceList.get(position).isChecked());
        serviceFilterViewHolder.binding.iDialogServiceGradeFilter.setId(position);
        serviceFilterViewHolder.binding.iServiceGradeFilterGradeLayout.setEnabled(mServiceList.get(position).isChecked());
    }

    @Override
    public int getItemCount() {
        return mServiceList == null ? 0 : mServiceList.size();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mCallback.onServiceCheck(mServiceList.get(buttonView.getId()), isChecked);
        AppLogger.i(getClass(), "checked Id: " + buttonView.getId());
    }

        /*private boolean checkSelected(StaticAddress address) {
            boolean selected = false;
            if (mSelectedSidoAddress != null) {
                if (address.getbCode().equals(mSelectedSidoAddress.getbCode())) {
                    selected = true;
                }
            }

            if (mSelectedGugunAddress != null) {
                if (address.getbCode().equals(mSelectedGugunAddress.getbCode())) {
                    selected = true;
                }
            }

            if (mSelectedDongAddress != null) {
                if (address.getbCode().equals(mSelectedDongAddress.getbCode())) {
                    selected = true;
                }
            }

            return selected;
        }*/

    class ServiceFilterViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        final IServiceGradeFilterBinding binding;
        private ServiceFilterCallback callback;

        private ServiceFilterViewHolder(IServiceGradeFilterBinding binding, ServiceFilterCallback callback) {
            super(binding.getRoot());
            this.binding = binding;
            this.callback = callback;
            this.binding.iDialogServiceGradeFilter.setOnCheckedChangeListener(this);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            binding.iServiceGradeFilterGradeLayout.setSelected(isChecked);
            binding.iServiceGradeFilterGradeLayout.setEnabled(isChecked);
            callback.onServiceCheck(binding.getServiceFilter(), isChecked);
        }
    }
}
