package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.EvaluationObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceDetailObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceSimpleObject;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Service extends RealmObject implements Parcelable {
    public enum TYPE {
        SEARCH_LIST,
        SEARCH_MAP,
        RECENTLY,
        BOOKMARK,
        CARECOORDI,
        SUGGESTION,
        SEARCH_TEMP
    }

    @PrimaryKey
    private int key;     // id는 해쉬코드로 생성
    private long fakeId;
    private int id;
    private String type;
    private String latestGrade;
    private Evaluation latestEvaluation;
    private String serviceCategory;
    private String serviceName;
    private String serviceDescription;

    @Deprecated
    private String lti;
    @Deprecated
    private String serviceCostTypeCode;
    @Deprecated
    private String caringPlaceTypeCode;
    @Deprecated
    private RealmList<Evaluation> evaluations;

    // schema version 6
    private boolean isClosed;

    public Service(){}

    public Service(Parcel parcel){
        this.key = parcel.readInt();
        this.fakeId = parcel.readLong();
        this.id = parcel.readInt();
        this.type = parcel.readString();
        this.latestGrade = parcel.readString();
        this.latestEvaluation = parcel.readParcelable(Evaluation.class.getClassLoader());
        this.serviceCategory = parcel.readString();
        this.serviceName = parcel.readString();
        this.serviceDescription = parcel.readString();
        this.lti = parcel.readString();
        this.serviceCostTypeCode = parcel.readString();
        this.caringPlaceTypeCode = parcel.readString();
        this.evaluations = new RealmList<>();
        parcel.readList(this.evaluations , Evaluation.class.getClassLoader());

        this.isClosed = parcel.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(key);
        dest.writeLong(fakeId);
        dest.writeInt(id);
        dest.writeString(type);
        dest.writeString(latestGrade);
        dest.writeParcelable(latestEvaluation, flags);
        dest.writeString(serviceCategory);
        dest.writeString(serviceName);
        dest.writeString(serviceDescription);
        dest.writeString(lti);
        dest.writeString(serviceCostTypeCode);
        dest.writeString(caringPlaceTypeCode);
        dest.writeList(evaluations);
        dest.writeByte((byte) (isClosed ? 1 : 0));
    }

    public void setService(TYPE type, Service service){
        this.fakeId = service.getFakeId();
        this.id = service.getId();
        this.type = type.toString();
        this.latestGrade = service.getLatestGrade();
        if(service.getLatestEvaluation() != null){
            Evaluation evaluation = new Evaluation();
            evaluation.setEvaluation(service.getFakeId(), type, service.getLatestEvaluation());
            this.latestEvaluation = evaluation;
        }
        this.serviceCategory = service.getServiceCategory();
        this.serviceName = service.getServiceName();
        this.serviceDescription = service.getServiceDescription();
        this.isClosed = service.isClosed();

        this.key = hashCodeForPrimaryKey();
    }

    public void setService(TYPE type, ServiceSimpleObject item){
        this.fakeId = item.getFakeId();
        this.id = item.getServiceId();
        this.type = type.toString();
        this.latestGrade = item.getLatestGrade();
        if(item.getLatestEvaluation() != null){
            Evaluation evaluation = new Evaluation();
            evaluation.setEvaluation(item.getFakeId(), type, item.getLatestEvaluation());
            this.latestEvaluation = evaluation;
        }
        this.serviceCategory = item.getServiceCategory();
        this.serviceName = item.getServiceName();
        this.serviceDescription = item.getServiceDescription();
        this.isClosed = item.isClosed();

        this.key = hashCodeForPrimaryKey();
    }

    public void setService(TYPE type, ServiceDetailObject item){
        this.fakeId = item.getFakeId();
        this.id = item.getServiceId();
        this.type = type.toString();
        this.latestGrade = item.getLatestGrade();
        if(item.getLatestEvaluation() != null){
            Evaluation evaluation = new Evaluation();
            evaluation.setEvaluation(item.getFakeId(), type, item.getLatestEvaluation());
            this.latestEvaluation = evaluation;
        }
        this.serviceCategory = item.getServiceCategory();
        this.serviceName = item.getServiceName();
        this.serviceDescription = item.getServiceDescription();
        this.evaluations = new RealmList<>();
        if(item.getEvaluations() != null){
            for(EvaluationObject evaluationObject : item.getEvaluations().getItems()){
                Evaluation evaluation = new Evaluation();
                evaluation.setEvaluation(item.getFakeId(), type, evaluationObject);
                this.evaluations.add(evaluation);
            }
        }
        this.isClosed = item.isClosed();

        this.key = hashCodeForPrimaryKey();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatestGrade() {
        return latestGrade;
    }

    public void setLatestGrade(String latestGrade) {
        this.latestGrade = latestGrade;
    }

    public Evaluation getLatestEvaluation() {
        return latestEvaluation;
    }

    public void setLatestEvaluation(Evaluation latestEvaluation) {
        this.latestEvaluation = latestEvaluation;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    public long getFakeId() {
        return fakeId;
    }

    public String getLti() {
        return lti;
    }

    public void setLti(String lti) {
        this.lti = lti;
    }

    public String getServiceCostTypeCode() {
        return serviceCostTypeCode;
    }

    public void setServiceCostTypeCode(String serviceCostTypeCode) {
        this.serviceCostTypeCode = serviceCostTypeCode;
    }

    public String getCaringPlaceTypeCode() {
        return caringPlaceTypeCode;
    }

    public void setCaringPlaceTypeCode(String caringPlaceTypeCode) {
        this.caringPlaceTypeCode = caringPlaceTypeCode;
    }

    public RealmList<Evaluation> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(RealmList<Evaluation> evaluations) {
        this.evaluations = evaluations;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public static Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {

        @Override
        public Service createFromParcel(Parcel source) {
            return new Service(source);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }

    };

    public void deleteRealm(){
        RealmList<Evaluation> evaluations = getEvaluations();
        if (evaluations != null) {
            for (int j = 0; j < evaluations.size(); j++) {
                Evaluation evaluation = evaluations.get(j);
                if (evaluation != null) {
                    evaluation.deleteFromRealm();
                }
            }
        }

        if (getLatestEvaluation() != null) {
            getLatestEvaluation().deleteFromRealm();
        }

        this.deleteFromRealm();
    }

    private int hashCodeForPrimaryKey() {
        int result = 17;
        result = 31 * result + String.valueOf(fakeId).hashCode();
        result = 31 * result + id;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
