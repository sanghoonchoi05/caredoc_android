package com.onestepmore.caredoc.ui.main.search.list;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchListFragmentModule {
    @Provides
    SearchListViewModel searchListViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                          Gson gson, FacilityRepository facilityRepository) {
        return new SearchListViewModel(application, dataSource, schedulerProvider, gson, facilityRepository);
    }

    @Provides
    @Named("SearchListFragment")
    ViewModelProvider.Factory provideSearchListViewModelProvider(SearchListViewModel searchListViewModel) {
        return new ViewModelProviderFactory<>(searchListViewModel);
    }
}
