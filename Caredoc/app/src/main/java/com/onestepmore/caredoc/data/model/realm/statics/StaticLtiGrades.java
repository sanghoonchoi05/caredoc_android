package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StaticLtiGrades extends RealmObject implements Parcelable {
    @PrimaryKey
    @Expose
    @SerializedName("grade")
    private int grade;
    @Expose
    @SerializedName("grade_name")
    private String gradeName;

    public StaticLtiGrades(){}

    protected StaticLtiGrades(Parcel in) {
        grade = in.readInt();
        gradeName = in.readString();
    }

    public static final Creator<StaticLtiGrades> CREATOR = new Creator<StaticLtiGrades>() {
        @Override
        public StaticLtiGrades createFromParcel(Parcel in) {
            return new StaticLtiGrades(in);
        }

        @Override
        public StaticLtiGrades[] newArray(int size) {
            return new StaticLtiGrades[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(grade);
        dest.writeString(gradeName);
    }

    public int getGrade() {
        return grade;
    }

    public String getGradeName() {
        return gradeName;
    }
}
