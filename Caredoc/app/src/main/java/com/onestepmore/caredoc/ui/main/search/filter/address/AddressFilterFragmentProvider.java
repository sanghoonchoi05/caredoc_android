package com.onestepmore.caredoc.ui.main.search.filter.address;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AddressFilterFragmentProvider {
    @ContributesAndroidInjector(modules = AddressFilterFragmentModule.class)
    abstract AddressFilterFragment provideAddressFilterFragmentFactory();
}
