package com.onestepmore.caredoc.data.model.realm;

import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;

import java.util.List;

import javax.inject.Singleton;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

@Singleton
public final class RealmDataAccessor {
    private static RealmDataAccessor mRealmDataAccessor;
    private Realm mRealm;
    private final RealmResults<StaticCategory> mStaticCategory;

    public Realm getRealm() {
        if(mRealm == null){
            mRealm = Realm.getDefaultInstance();
        }
        return mRealm;
    }

    public static RealmDataAccessor getInstance(){
        if(mRealmDataAccessor == null){
            mRealmDataAccessor = new RealmDataAccessor();
        }

        return mRealmDataAccessor;
    }

    private RealmDataAccessor(){
        mRealm = getRealm();
        mStaticCategory = mRealm.where(StaticCategory.class).findAll();
    }

    public RealmResults<StaticService> getStaticService(StaticCategory category){
        if(category == null){
            return null;
        }

        RealmQuery<StaticService> realmQuery = getRealm().where(StaticService.class);
        if(category.getQueryServiceTypeIds() != null && category.getQueryServiceTypeIds().size() > 0){
            Integer [] serviceTypeIds = new Integer[category.getQueryServiceTypeIds().size()];
            category.getQueryServiceTypeIds().toArray(serviceTypeIds);
            realmQuery = realmQuery.in("id", serviceTypeIds);
        }

        return realmQuery.findAll();
    }

    public StaticService getStaticService(int id){
        return getRealm().where(StaticService.class).equalTo("id", id).findFirst();
    }

    public RealmList<Integer> getStaticServiceIdAll(){
        RealmList<Integer> serviceIdList = new RealmList<>();
        List<StaticService> staticServiceList = getRealm().where(StaticService.class).findAll();
        for(StaticService staticService : staticServiceList){
            serviceIdList.add(staticService.getId());
        }
        return serviceIdList;
    }

    public StaticCategory getStaticCategory(int id){
        StaticCategory staticCategory = null;
        List<StaticCategory> staticCategories = getRealm().where(StaticCategory.class).findAll();
        for(StaticCategory category : staticCategories){
            for(Integer serviceTypeId : category.getQueryServiceTypeIds()){
                if(serviceTypeId == id){
                    staticCategory = category;
                    break;
                }
            }
        }
        return staticCategory;
    }

    public RealmResults<StaticCategory> getStaticCategory() {
        return mStaticCategory;
    }

    public RealmResults<StaticCategory> getStaticCategoryExcludeAll() {
        return getRealm().where(StaticCategory.class).notEqualTo("text", "전체").findAll();
    }

    public void onClose(){
        if(mRealm != null){
            mRealm.close();
        }
    }
}
