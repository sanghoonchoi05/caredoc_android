package com.onestepmore.caredoc.data.model.api.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExternalMailgunRequest {
    public static class QueryParameter {
        @Expose
        @SerializedName("from")
        private String from;

        @Expose
        @SerializedName("to")
        private String to;

        @Expose
        @SerializedName("subject")
        private String subject;

        @Expose
        @SerializedName("text")
        private String text;

        public String getFrom() {
            return from;
        }

        public String getTo() {
            return to;
        }

        public String getSubject() {
            return subject;
        }

        public String getText() {
            return text;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public void setText(String text) {
            this.text = text;
        }
    }
}
