package com.onestepmore.caredoc.ui.carecoordi;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class CareCoordiActivityModule {
    @Provides
    CareCoordiViewModel careCoordiViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                           Gson gson, FacilityRepository repository) {
        return new CareCoordiViewModel(application, dataSource, schedulerProvider, gson, repository);
    }

    @Provides
    ViewModelProvider.Factory provideCareCoordiViewModelProvider(CareCoordiViewModel careCoordiViewModel) {
        return new ViewModelProviderFactory<>(careCoordiViewModel);
    }
}
