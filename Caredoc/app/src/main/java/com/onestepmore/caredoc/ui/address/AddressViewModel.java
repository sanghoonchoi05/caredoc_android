package com.onestepmore.caredoc.ui.address;

import android.app.Application;
import android.view.View;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class AddressViewModel extends BaseViewModel<AddressNavigator> {

    public AddressViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClickBackButton(View view){
        getNavigator().onBack();
    }

    public void onClickSearchButton(View view){
        getNavigator().onSearch();
    }

    public void onClickDeleteButton(View view){
        getNavigator().onDelete();
    }
}
