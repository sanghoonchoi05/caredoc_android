package com.onestepmore.caredoc.data.model.api.taker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteTakerRequest {

    public static class PathParameter {
        @Expose
        @SerializedName("id")
        private long id;

        public PathParameter(long id){
            this.id = id;
        }

        public long getId() {
            return id;
        }
    }
}
