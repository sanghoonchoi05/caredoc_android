package com.onestepmore.caredoc.ui.support;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.onestepmore.caredoc.ui.AppLogger;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int spaceVertical;
    private int spanCount;
    private int spaceHorizontal;
    private boolean includeFirstAndLastItem;
    private int spaceFirstAndLastHorizontal;
    private int spaceFirstVertical;
    private int spaceLastVertical;

    public SpacesItemDecoration(int spaceVertical, int spaceHorizontal) {
        this.spaceVertical = spaceVertical;
        this.spaceHorizontal = spaceHorizontal;
    }

    public SpacesItemDecoration(int spaceVertical, int spaceHorizontal, boolean includeFirstAndLastItem) {
        this.spaceVertical = spaceVertical;
        this.spaceHorizontal = spaceHorizontal;
        this.includeFirstAndLastItem = includeFirstAndLastItem;
    }

    public void setSpaceFirstAndLastHorizontal(int spaceFirstAndLastHorizontal) {
        this.spaceFirstAndLastHorizontal = spaceFirstAndLastHorizontal;
    }

    public void setSpaceLastVertical(int spaceLastVertical) {
        this.spaceLastVertical = spaceLastVertical;
    }

    public void setSpaceFirstVertical(int spaceFirstVertical) {
        this.spaceFirstVertical = spaceFirstVertical;
    }

    public void setSpanCount(int spanCount) {
        this.spanCount = spanCount;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {

        final int itemCount = state.getItemCount();

        outRect.left = 0;
        outRect.right = 0;
        outRect.top = 0;
        outRect.bottom = 0;
        if(spaceHorizontal > 0){
            int childPosition = parent.getChildAdapterPosition(view);
            if(spanCount > 0){
                int columnIndex = childPosition % spanCount;
                if(columnIndex != 0){
                    outRect.left = spaceHorizontal;
                }
            }
            else{
                if (childPosition == 0) {
                    if(includeFirstAndLastItem){
                        outRect.left = spaceHorizontal;
                        if(spaceFirstAndLastHorizontal > 0){
                            outRect.left = spaceFirstAndLastHorizontal;
                        }
                    }
                }
                /** last position */
                else if (itemCount > 0 && childPosition == itemCount - 1) {
                    outRect.left = spaceHorizontal;
                    if(includeFirstAndLastItem){
                        outRect.right = spaceHorizontal;
                        if(spaceFirstAndLastHorizontal > 0){
                            outRect.right = spaceFirstAndLastHorizontal;
                        }
                    }
                }
                else{
                    outRect.left = spaceHorizontal;
                }
            }
        }

        if(spaceVertical > 0 || includeFirstAndLastItem){
            outRect.bottom = spaceVertical;
            int childPosition = parent.getChildAdapterPosition(view);
            int lastPosition = itemCount - 1;
            if(spanCount > 0){
                // row position을 찾자
                childPosition = childPosition / spanCount;
                lastPosition = lastPosition / spanCount;
            }

            if(childPosition == 0 && includeFirstAndLastItem){
                outRect.top = spaceVertical;
                if(spaceFirstVertical > 0){
                    outRect.top = spaceFirstVertical;
                }
            }else if (lastPosition > -1 && childPosition == lastPosition) {
                if(includeFirstAndLastItem){
                    if(spaceLastVertical > 0){
                        outRect.bottom = spaceLastVertical;
                    }
                }
            }
        }

        // Add top margin only for the first item to avoid double spaceVertical between items
            /*if (parent.getChildLayoutPosition(view) == 0) {
                outRect.top = spaceVertical;
            } else {
                outRect.top = 0;
            }*/
    }
}