package com.onestepmore.caredoc.ui.main.taker.card;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.databinding.FTakerCardBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.detail.TakerDetailActivity;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

public class TakerCardFragment extends BaseCoordiFragment<FTakerCardBinding, TakerCardViewModel> implements
        TakerCardNavigator
{
    private TakerCardSelectListener mTakerCardSelectListener;
    public void setTakerCardSelectListener(TakerCardSelectListener listener){
        mTakerCardSelectListener = listener;
    }

    public static TakerCardFragment newInstance(boolean selectedMode){
        Bundle bundle = new Bundle();

        TakerCardFragment fragment = new TakerCardFragment();
        bundle.putBoolean("selectedMode", selectedMode);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Inject
    @Named("TakerCardFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerCardViewModel mTakerCardViewModel;

    private TakerCardAdapter mTakerAdapter;
    RealmResults<Taker> mTakerList;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_card;
    }

    @Override
    public TakerCardViewModel getViewModel() {
        mTakerCardViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerCardViewModel.class);
        return mTakerCardViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerCardViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        boolean selectedMode = false;
        if(getArguments() != null){
            selectedMode = getArguments().getBoolean("selectedMode");
        }

        setSupportToolBar(getViewDataBinding().fTakerCardToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }

        getViewDataBinding().setSelectedMode(selectedMode);

        mTakerList = getViewModel().getRealm().where(Taker.class).sort("createdAt",Sort.DESCENDING).findAll();
        mTakerList.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Taker>>() {
            @Override
            public void onChange(RealmResults<Taker> results, OrderedCollectionChangeSet changeSet) {
                getViewDataBinding().setCount(getString(R.string.common_num_formatted, results.size()));
                AppLogger.i(getClass(), "ProfileFragment realm Taker size: " + results.size());
            }
        });
        getViewDataBinding().setCount(getString(R.string.common_num_formatted,mTakerList.size()));

        mTakerAdapter = new TakerCardAdapter(mTakerList, this, selectedMode);
        getViewDataBinding().fTakerCardRecyclerView.setAdapter(mTakerAdapter);

        GridLayoutManager manager = new GridLayoutManager(getBaseActivity(), 2);
        getViewDataBinding().fTakerCardRecyclerView.setLayoutManager(manager);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_taker_card_item_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(spacingInPixels, spacingInPixels);
        decoration.setSpanCount(2);
        getViewDataBinding().fTakerCardRecyclerView.addItemDecoration(decoration);
        getViewDataBinding().fTakerCardRecyclerView.setNestedScrollingEnabled(false);

        getViewDataBinding().fTakerCardNextButtonAnchor.setNextText(getString(R.string.f_taker_card_add));
    }

    @Override
    public void setTitle(int titleResId) {

    }

    @Override
    public void setStepData(StepData stepData) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mTakerList != null){
            mTakerList.removeAllChangeListeners();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BackStackManager.getInstance().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showAddTakerView() {
        Intent intent = getBaseActivity().newIntent(AddTakerActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTakerItemClick(Taker taker) {
        Intent intent = getBaseActivity().newIntent(TakerDetailActivity.class);
        intent.putExtra("takerId", taker.getFakeId());
        startActivity(intent);
    }

    @Override
    public void onSelectTaker(Taker taker) {
        if(mTakerCardSelectListener != null){
            mTakerCardSelectListener.onSelect(taker);
        }

        BackStackManager.getInstance().onBackPressed();
    }
}
