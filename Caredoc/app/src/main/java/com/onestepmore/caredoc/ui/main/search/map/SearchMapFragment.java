package com.onestepmore.caredoc.ui.main.search.map;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.data.model.api.object.FacilitySearchMapObject;
import com.onestepmore.caredoc.data.model.api.object.SearchCodeFilter;
import com.onestepmore.caredoc.data.model.api.object.SearchMapFilter;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchMapList;
import com.onestepmore.caredoc.databinding.FSearchMapBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.base.TransactionCallback;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.OnCurrentLocationCallback;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.ui.widgets.StartPagerSnapHelper;
import com.onestepmore.caredoc.utils.GraphicsUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

import static android.support.design.widget.BottomSheetBehavior.STATE_COLLAPSED;
import static android.support.design.widget.BottomSheetBehavior.STATE_EXPANDED;
import static android.support.design.widget.BottomSheetBehavior.STATE_SETTLING;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_CLICK_MODE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_MAP_FACILITY_MARKER;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_FACILITY_MAP_LOCATION_MARKER;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SEARCH_FACILITY_MAP_ZOOM;

public class SearchMapFragment extends BaseFragment<FSearchMapBinding, SearchMapViewModel> implements
        SearchMapNavigator,
        OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraMoveStartedListener,
        NestedScrollView.OnScrollChangeListener,
        OnCurrentLocationCallback,
        TransactionCallback,
        BaseFragment.OnBackPressedListener
{
    @Inject
    @Named("SearchMapFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private SearchMapViewModel mSearchMapViewModel;

    private SearchMapFilter.SearchMapFilterBuilder mCachedSearchMapFilterBuilder = null;

    private static String NO_KEY = "";

    private GoogleMap mMap = null;
    private Map<String, Marker> mMarkerMap = new HashMap<>();    // code, Marker
    private Map<String, FacilitySearchMapObject> mMarkerDataMap = new HashMap<>();   // code, FacilitySearchMapObject

    private Marker mMyPositionMarker = null;

    private View mMarkerView = null;
    private AppCompatTextView mMarkerText = null;
    private ConstraintLayout mMarkerLayout = null;

    private View mClusterView = null;
    private AppCompatTextView mClusterCountView = null;
    private AppCompatTextView mClusterTitleView = null;

    private DisplayMetrics mDisplayMetrics;

    private int mBottomSheetState = STATE_COLLAPSED;
    private int mNestedScrollViewY = 0;

    private String mMarkerGroup = "";
    private RecyclerView.SmoothScroller mSmoothScroller;

    private String mSelectedFacilityKey = NO_KEY;
    private boolean mForceCameraMove = false;
    private boolean mChangedLocationFilter = false;
    private boolean mInitMap = false;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_search_map;
    }

    @Override
    public SearchMapViewModel getViewModel() {
        mSearchMapViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchMapViewModel.class);
        return mSearchMapViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSearchMapViewModel.setNavigator(this);

        mDisplayMetrics = new DisplayMetrics();
        getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UIUtils.setStatusBarColor(getBaseActivity(), getView(), getResources().getColor(R.color.colorPrimary), false);

        mCachedSearchMapFilterBuilder = ((SearchFragment) BackStackManager.getInstance().getCurrentBackStack().getRootFragment()).getSearchMapFilterBuilder();
        setChangedLocationFilter(mCachedSearchMapFilterBuilder.getAddressCode() != null);

        subscribeToMarker();

        initListAdapter();
        addChangeListener();
        
        // 맵 초기화
        initMap();
    }

    public void setForceCameraMove(boolean forceCameraMove) {
        this.mForceCameraMove = forceCameraMove;
    }

    public void setChangedLocationFilter(boolean mChangedLocationFilter) {
        this.mChangedLocationFilter = mChangedLocationFilter;
    }

    public boolean isChangedLocationFilter() {
        return mChangedLocationFilter;
    }

    public boolean isForceCameraMove() {
        return mForceCameraMove;
    }

    public void setCameraZoomValue(int zoomValue){
        mCachedSearchMapFilterBuilder.addZoom(zoomValue);
    }

    public int getCameraZoomValue(){
        AppLogger.i(getClass(), "Get CameraZoom: " + mCachedSearchMapFilterBuilder.getZoom());
        return mCachedSearchMapFilterBuilder.getZoom();
    }

    public void setSWNE(String sw, String ne){
        mCachedSearchMapFilterBuilder.addSwNe(sw, ne);
    }

    private void initMap(){
        setCameraZoomValue((int)SearchFragment.CAMERA_DEFAULT_ZOOM_VALUE);
        FragmentManager fragmentManager = getChildFragmentManager();
        SupportMapFragment supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.f_search_map_fragment);
        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }
    }

    private void initListAdapter(){
        SearchMapCareFacilityAdapter adapter = new SearchMapCareFacilityAdapter(getViewModel().getFacilitySearchMapList());
        adapter.setMainNavigator((MainActivity)getBaseActivity());
        getViewDataBinding().fSearchMapRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fSearchMapRecyclerView.setLayoutManager(layoutManager);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_recently_horizontal_item_space);
        int spacingFirstLast = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_recently_horizontal_first_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, spacingInPixels, true);
        decoration.setSpaceFirstAndLastHorizontal(spacingFirstLast);
        getViewDataBinding().fSearchMapRecyclerView.addItemDecoration(decoration);

        spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_care_facility_map_item_snap_space);
        SnapHelper snapHelperCenter = new StartPagerSnapHelper(0/*spacingInPixels*/);
        snapHelperCenter.attachToRecyclerView(getViewDataBinding().fSearchMapRecyclerView);

        getViewDataBinding().fSearchMapRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int snapPosition = RecyclerView.NO_POSITION;
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if(layoutManager!=null){
                    View snapView = snapHelperCenter.findSnapView(layoutManager);
                    if(snapView!=null){
                        snapPosition = layoutManager.getPosition(snapView);
                        //onChangeSnapPosition(snapPosition);
                    }
                }

                AppLogger.i(getClass(), "Scrolled x: " + dx);
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        mSmoothScroller = new LinearSmoothScroller(getBaseActivity()) {
            @Override protected int getHorizontalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setMaxZoomPreference(20f);

        /*mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setPadding(0, 0, 0, getResources().getDimensionPixelSize(R.dimen.f_search_peek_height));*/

        mMarkerView = (LayoutInflater.from(getBaseActivity()).inflate(R.layout.v_facility_grade_marker, null));
        mMarkerText = mMarkerView.findViewById(R.id.v_facility_grade_marker_title);
        mMarkerLayout = mMarkerView.findViewById(R.id.v_facility_grade_marker_layout);

        mClusterView = (LayoutInflater.from(getBaseActivity()).inflate(R.layout.v_cluster_marker_normal, null));
        mClusterCountView = mClusterView.findViewById(R.id.v_cluster_marker_normal_count);
        mClusterTitleView = mClusterView.findViewById(R.id.v_cluster_marker_normal_title);

        onMyLocation();
    }

    @Override
    public void onList() {
        AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_CLICK_MODE, AmplitudeParam.Key.MODE, AmplitudeParam.MODE.LIST.toString());
        BackStackManager.getInstance().onBackPressed();
    }

    private void subscribeToMarker() {
        getViewModel().getObservableClusterObj().observe(this, new Observer<List<FacilitySearchMapObject>>() {
            @Override
            public void onChanged(@Nullable List<FacilitySearchMapObject> facilitySearchMapObjects) {
                if (mMap != null) {
                    hideMarker();

                    if(facilitySearchMapObjects != null && facilitySearchMapObjects.size() > 0){
                        // group_by가 바뀌었다면 마커 클리어
                        String groupBy = facilitySearchMapObjects.get(0).getGroupBy();
                        if(isChangeMarkerGroup(groupBy)){
                            mMarkerGroup = groupBy;
                            clearMarker();
                        }

                        addFacilityMarker(facilitySearchMapObjects);
                    }
                    else{
                        // 리스트가 없을 경우
                        /*if(mSelectedSearchMode == FacilitySearchRequest.SEARCH_MODE.address || mSelectedSearchMode == FacilitySearchRequest.SEARCH_MODE.keyword){
                            Toast.makeText(getBaseActivity(), getString(R.string.common_not_exist_facility), Toast.LENGTH_SHORT).show();
                        }

                        setSelectedSearchMode(FacilitySearchRequest.SEARCH_MODE.map);*/
                    }
                }
            }
        });
    }

    private void addChangeListener(){
        getViewModel().getFacilitySearchMapList().addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<FacilitySearchMapList>>() {
            @Override
            public void onChange(@NonNull RealmResults<FacilitySearchMapList> facilitySearchMapLists,@NonNull OrderedCollectionChangeSet changeSet) {
                if(facilitySearchMapLists.size() > 0){
                    for(FacilitySearchMapList facility: facilitySearchMapLists){
                        AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_MAP_FACILITY_MARKER, AmplitudeParam.Key.VALUE, facility.getName());
                    }
                    setBottomSheetBehaviorState(STATE_EXPANDED);
                }
            }
        });
    }

    private void hideMarker(){
        for(String key : mMarkerMap.keySet()){
            Marker marker = mMarkerMap.get(key);
            if(marker != null){
                marker.setVisible(false);
            }
        }
    }

    private void clearMarker() {
        for(String key : mMarkerMap.keySet()){
            Marker marker = mMarkerMap.get(key);
            if(marker != null){
                marker.remove();
            }
        }

        mMarkerDataMap.clear();
        mMarkerMap.clear();
    }

    private boolean isChangeMarkerGroup(String markerGroupBy){
        boolean isChange = false;
        if(markerGroupBy!=null){
            if(!mMarkerGroup.equals(markerGroupBy)){
                isChange = true;
            }
        }
        return isChange;
    }

    private void addFacilityMarker(List<FacilitySearchMapObject> responseList){
        if (responseList != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (FacilitySearchMapObject responseObject : responseList) {
                FacilitySearchMapObject addedClusterObject = mMarkerDataMap.get(responseObject.getCode());
                Marker addedMarker = mMarkerMap.get(responseObject.getCode());


                if(addedMarker != null && addedClusterObject != null && addedClusterObject.getCount() == responseObject.getCount()){
                    addedMarker.setVisible(true);
                }else{
                    if(addedMarker != null){
                        mMarkerMap.remove(responseObject.getCode());
                        addedMarker.remove();
                    }

                    if(addedClusterObject != null){
                        mMarkerDataMap.remove(responseObject.getCode());
                    }

                    addMarker(responseObject, false);
                }
                builder.include(new LatLng(responseObject.getLat(), responseObject.getLng()));
            }

            if(isChangedLocationFilter()){
                setChangedLocationFilter(false);
                setForceCameraMove(true);
                moveCameraWithLoaction(CameraUpdateFactory.newLatLngBounds(builder.build(), 0), false);
            }
        }
    }

    private void addMarker(FacilitySearchMapObject object, boolean selected){
        BitmapDescriptor descriptor = getClusterItemBitmapDescriptor(object.getName(), object.getCount());
        if (object.getGroupBy().equals(FacilitySearchMapObject.GROUP_BY.MARKER.getText())) {
            descriptor = getMarkerItemBitmapDescriptor(object.getAllName(), selected);
        }

        LatLng position = new LatLng(object.getLat(), object.getLng());

        Marker marker = drawFacilityMarkerOnMap(object.getCode(), position, descriptor, selected);
        if(marker != null){
            mMarkerMap.put(object.getCode(), marker);
            mMarkerDataMap.put(object.getCode(), object);
        }
    }

    private Marker drawFacilityMarkerOnMap(String code, LatLng latLng, BitmapDescriptor bitmapDescriptor, boolean selected) {
        if (bitmapDescriptor == null) {
            return null;
        }
        int zIndex = 0;
        if(selected){
            zIndex = 1;
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(latLng)
                .zIndex(zIndex)
                .icon(bitmapDescriptor);
        Marker marker = mMap.addMarker(markerOptions);
        marker.setTag(code);
        return marker;
    }

    private BitmapDescriptor getClusterItemBitmapDescriptor(String title, int count) {
        mClusterTitleView.setText(title);
        mClusterCountView.setText(String.valueOf(count));
        mClusterTitleView.requestLayout();

        BitmapDescriptor descriptor = null;
        Bitmap bitmap = GraphicsUtils.createBitmapFromView(mClusterView, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
        if (bitmap != null) {
            descriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return descriptor;
    }

    private BitmapDescriptor getMarkerItemBitmapDescriptor(String text, boolean selected) {
        if(selected){
            mMarkerText.setTextColor(ContextCompat.getColor(getBaseActivity(), R.color.white));
            mMarkerLayout.setBackgroundResource(R.drawable.facility_marker_selected);
        }
        else{
            mMarkerText.setTextColor(ContextCompat.getColor(getBaseActivity(), R.color.colorPrimary));
            mMarkerLayout.setBackgroundResource(R.drawable.facility_marker);
        }

        mMarkerText.setText(text);
        mMarkerText.requestLayout();

        BitmapDescriptor descriptor = null;
        Bitmap bitmap = GraphicsUtils.createBitmapFromView(mMarkerView, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
        if (bitmap != null) {
            descriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return descriptor;
    }

    private void moveCameraWithLoaction(LatLng latLng, boolean animate) {
        moveCameraWithLoaction(latLng, getCameraZoomValue(), animate);
    }

    private void moveCameraWithLoaction(LatLng latLng, int zoom, boolean animate) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(zoom)
                .build();

        moveCameraWithLoaction(CameraUpdateFactory.newCameraPosition(cameraPosition), animate);
    }

    private void moveCameraWithLoaction(CameraUpdate cameraUpdate, boolean animate) {
        if (animate) {
            mMap.animateCamera(cameraUpdate, 500, null);
        } else {
            mMap.moveCamera(cameraUpdate);
        }
    }

    public void onBottomSheetStateChanged(@NonNull View bottomSheet, int newState) {
        if (newState == STATE_SETTLING) {
            if (mBottomSheetState == STATE_EXPANDED) {
                // 리스트를 내리는 상태에서 목록 초기화

            } else if (mBottomSheetState == STATE_COLLAPSED) {

            }
            mBottomSheetState = newState;
        }

        if (newState == STATE_EXPANDED) {
            if (mBottomSheetState != STATE_SETTLING) {
            }
            mBottomSheetState = newState;
        } else if (newState == STATE_COLLAPSED) {
            if (mBottomSheetState != STATE_SETTLING) {
            }
            mBottomSheetState = newState;
        }
    }

    private void setBottomSheetBehaviorState(int state){
        BottomSheetBehavior bottomSheet = BottomSheetBehavior.from(getViewDataBinding().fSearchMapListLayout2);
        bottomSheet.setState(state);
    }

    // 현재 내 위치 가져오기
    @Override
    public void onMyLocation() {
        if (getBaseActivity() instanceof MainActivity) {
            ((MainActivity) getBaseActivity()).setCurrentLocationCallback(this);
            ((MainActivity) getBaseActivity()).requestMyLocation();
        }
    }

    @Override
    public void onCameraIdle() {
        if(!mInitMap){
            // 맵초기화 후 현 위치 불러오기 전에는 return
            return;
        }
        CameraPosition position = mMap.getCameraPosition();
        int zoom = (int)position.zoom;
        if (getCameraZoomValue() != zoom) {
            if(getCameraZoomValue() > zoom){
                AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_MAP_ZOOM, AmplitudeParam.Key.ZOOM, AmplitudeParam.ZOOM.OUT.toString());
            }
            else{
                AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_MAP_ZOOM, AmplitudeParam.Key.ZOOM, AmplitudeParam.ZOOM.IN.toString());
            }
            setCameraZoomValue(zoom);
        }

        AppLogger.i(getClass(), "CameraZoom: " + zoom);

        LatLngBounds curScreen = mMap.getProjection().getVisibleRegion().latLngBounds;
        String ne = curScreen.northeast.latitude + "," + curScreen.northeast.longitude;
        String sw = curScreen.southwest.latitude + "," + curScreen.southwest.longitude;
        setSWNE(sw, ne);

        if(!isForceCameraMove()){
            loadFacilitySearchClusterFromServer();
        }else{
            setForceCameraMove(false);
        }
    }

    // 현재 설정된 카테고리와 필터로 검색
    public void loadFacilitySearchClusterFromServer() {
        SearchMapFilter filter = mCachedSearchMapFilterBuilder.build();
        if(isChangedLocationFilter()){
            filter.setZoom((int)SearchFragment.CAMERA_SIDO_ZOOM_VALUE);
            filter.setNe(null);
            filter.setSw(null);
        }
        getViewModel().loadFacilitySearchClusterFromServer(filter);
    }

    public void loadFacilitySearchCodeFromServer(String code) {
        getViewModel().loadFacilitySearchCodeFromServer(getSearchCodeFilter(code));
    }

    public SearchCodeFilter getSearchCodeFilter(String code) {
        if(mMap == null){
            return null;
        }

        SearchCodeFilter filter = new SearchCodeFilter();
        filter.setCode(code);

        return filter;
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        // 유저에 의한 맵 이동 시 맵모드로 전환
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            AppLogger.i(getClass(), "The user gestured on the map.");
            // 기존 마커는 normal 상태로
            changeMarker(mSelectedFacilityKey, false);
            setBottomSheetBehaviorState(STATE_COLLAPSED);
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {
            AppLogger.i(getClass(), "The user tapped something on the map.");
        } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {
            AppLogger.i(getClass(), "The app moved the camera.");
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        // 기존 마커는 normal 상태로
        changeMarker(mSelectedFacilityKey, false);
        setBottomSheetBehaviorState(STATE_COLLAPSED);
    }

    /**
     * @return 길찾기 버튼 노출 여부(true면 노출하지 않음)
     * */
    @Override
    public boolean onMarkerClick(Marker marker) {
        String newKey = (String)marker.getTag();
        String selectedKey = mSelectedFacilityKey;

        if(newKey == null){
            return false;
        }

        FacilitySearchMapObject facilitySearchMapObject = mMarkerDataMap.get(newKey);
        if(facilitySearchMapObject != null){
            if(!facilitySearchMapObject.getGroupBy().equals(FacilitySearchMapObject.GROUP_BY.MARKER.getText())){
                AmplitudeManager.getInstance().logEvent(SEARCH_FACILITY_MAP_LOCATION_MARKER, AmplitudeParam.Key.VALUE, facilitySearchMapObject.getName());
                // 지역 클러스터면 줌만 땡긴다
                moveCameraWithLoaction(
                        new LatLng(facilitySearchMapObject.getLat(), facilitySearchMapObject.getLng()),
                        facilitySearchMapObject.getNextZoom(),
                        true);
                setBottomSheetBehaviorState(STATE_COLLAPSED);
                return true;
            }
        }

        // 기존 마커는 normal 상태로
        changeMarker(selectedKey, false);

        if(!selectedKey.equals(NO_KEY) &&
                newKey.equals(selectedKey)){
            // 같은 마커를 선택했다면
            setBottomSheetBehaviorState(STATE_COLLAPSED);
            return true;
        }

        // 새로운 마커는 select 상태로
        changeMarker(newKey, true);
        List<FacilitySearchMapList> facilityList = getViewModel().getFacilityList(newKey);
        if(facilityList.size() == 0){
            loadFacilitySearchCodeFromServer(newKey);
        }

        moveCameraWithLoaction(marker.getPosition(), true);

        //moveToScrollPosition(key);

        return true;
    }

    private void changeMarker(String key, boolean selected){
        // 리스트를 보여줄 지 말지 처리
        // selected가 false라면 show가 true인 데이터는 다 false로
        List<FacilitySearchMapList> facilityList = getViewModel().getFacilityList(true);
        if(selected){
            facilityList = getViewModel().getFacilityList(key);
        }

        if(facilityList.size() > 0){
            getViewModel().getFacilityRepository().showFacilitySearchMapList(facilityList, selected);
        }

        // 마커 처리
        if(key.equals(NO_KEY)) {
            return;
        }

        // 기존 마커를 지움
        Marker marker = mMarkerMap.get(key);
        if(marker != null){
            marker.remove();
        }

        FacilitySearchMapObject facilitySearchMapObject = mMarkerDataMap.get(key);
        if(facilitySearchMapObject != null){
            // 새로운 마커를 다시 그림
            addMarker(facilitySearchMapObject, selected);

            if(selected){
                mSelectedFacilityKey = facilitySearchMapObject.getCode();
            }
            else{
                mSelectedFacilityKey = NO_KEY;
            }
        }
    }

    @Override
    public void onScrollChange(NestedScrollView nestedScrollView, int i, int i1, int i2, int i3) {

    }

    @Override
    public void doBack() {
        BackStackManager.getInstance().onBackPressed();
    }

    @Override
    public void onLocation(LatLng latLng, boolean granted) {
        AppLogger.i(getClass(), "CameraZoom onLocation called");

        // Firebase Crashlytics - SearchMapFragment.java line 130
        if(getBaseActivity() == null || getViewModel() == null){
            return;
        }

        drawMyLocation(latLng);
        moveCameraWithLoaction(latLng, false);

        mInitMap = true;
    }

    private void drawMyLocation(LatLng location){
        if(mMyPositionMarker != null){
            mMyPositionMarker.setPosition(location);
            mMyPositionMarker.setVisible(true);
            return;
        }

        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
                GraphicsUtils.getBitmapFromVectorDrawable(
                        getBaseActivity(),
                        R.drawable.oval_my_position_dot));

        MarkerOptions markerOptions = new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(location)
                //.zIndex(1)
                .icon(bitmapDescriptor);
        mMyPositionMarker = mMap.addMarker(markerOptions);
    }
}

