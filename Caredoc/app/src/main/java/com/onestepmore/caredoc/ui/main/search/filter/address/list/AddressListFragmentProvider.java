package com.onestepmore.caredoc.ui.main.search.filter.address.list;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AddressListFragmentProvider {
    @ContributesAndroidInjector(modules = AddressListFragmentModule.class)
    abstract AddressListFragment provideAddressListFragmentFactory();
}
