package com.onestepmore.caredoc.ui.support;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.object.ProgramsObject;
import com.onestepmore.caredoc.ui.address.result.AddressResultAdapter;
import com.onestepmore.caredoc.ui.carecoordi.result.ResultAdapter;
import com.onestepmore.caredoc.ui.main.FacilityVerticalAdapter;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.DateUtils;

import java.io.File;
import java.util.Date;
import java.util.List;

public class CommonViewBindings {
    @BindingAdapter("setGradeColor")
    public static void setGradeColor(AppCompatTextView textView, int colorId) {
        textView.setTextColor(colorId);
    }

    @BindingAdapter("setBirth")
    public static void setBirth(AppCompatTextView textView, Date birth) {
        textView.setText(DateUtils.getDateFormatTime(birth, DateUtils.BIRTH_DATE_FORMAT));
    }

    @BindingAdapter("showFooter")
    public static void setShowFooter(RecyclerView view, Boolean noMoreList) {
        if (view.getAdapter() instanceof FacilityVerticalAdapter) {
            FacilityVerticalAdapter adapter = (FacilityVerticalAdapter) view.getAdapter();
            if (adapter != null) {
                adapter.setShowFooter(!noMoreList);
            }
        } else if (view.getAdapter() instanceof ResultAdapter) {
            ResultAdapter adapter = (ResultAdapter) view.getAdapter();
            if (adapter != null) {
                adapter.setShowFooter(!noMoreList);
            }
        } else if (view.getAdapter() instanceof AddressResultAdapter) {
            AddressResultAdapter adapter = (AddressResultAdapter) view.getAdapter();
            if (adapter != null) {
                adapter.setShowFooter(!noMoreList);
            }
        }
    }

    @BindingAdapter("imageTakerBasicUrl")
    public static void setImageTakerBasicUrl(AppCompatImageView imageView, File file) {
        imageView.setImageResource(R.drawable.btn_senioruseredit_pic);

        if (file != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    @BindingAdapter("android:src")
    public static void setImageDrawable(AppCompatImageView view, Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    @BindingAdapter("android:src")
    public static void setImageResource(AppCompatImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    @BindingAdapter("android:src")
    public static void setImageResource(CardView cardView, Drawable drawable) {
        cardView.setBackground(drawable);
    }

    @BindingAdapter("backgroundResource")
    public static void setBackgroundResource(View view, int resId) {
        view.setBackgroundResource(resId);
    }

    @BindingAdapter({"facilityRating", "facilityRatingStr"})
    public static void setFacilityRating(AppCompatTextView textView, float rating, String pointStr) {
        String ratingText = "-";
        if (rating > 0) {
            if (rating % 1 > 0) {
                ratingText = String.valueOf(rating);
            } else {
                ratingText = String.valueOf((int) rating);
            }
        }

        if (!CommonUtils.checkNullAndEmpty(pointStr)) {
            ratingText += pointStr;
        }

        textView.setText(ratingText);
    }

    @BindingAdapter("forceTextSize")
    public static void setTextSize(AppCompatTextView textView, int size) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, size);
    }

    @BindingAdapter({"fabOpen", "closeAnimation", "openAnimation"})
    public static void setFabAnim(View view, boolean isFabOpen, Animation closeAni, Animation openAni) {
        if (closeAni == null || openAni == null) {
            return;
        }
        if (isFabOpen) {
            view.startAnimation(openAni);
            view.setClickable(true);
        } else {
            view.startAnimation(closeAni);
            view.setClickable(false);
        }
    }
}
