package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class SocialAccountProviderObject {
    @Expose
    @SerializedName("provider")
    private String provider;
    @Expose
    @SerializedName("auth_id")
    private String authId;

    public String getProvider() {
        return provider;
    }

    public String getAuthId() {
        return authId;
    }
}
