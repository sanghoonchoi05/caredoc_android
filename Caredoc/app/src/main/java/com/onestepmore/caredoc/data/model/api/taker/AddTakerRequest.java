package com.onestepmore.caredoc.data.model.api.taker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Address;
import com.onestepmore.caredoc.utils.CommonUtils;

import java.util.Calendar;
import java.util.List;

public class AddTakerRequest {

    public static class PathParameter {
        @Expose
        @SerializedName("id")
        private long id;

        public PathParameter(long id){
            this.id = id;
        }

        public long getId() {
            return id;
        }
    }

    @Expose
    @SerializedName("basic")
    private Basic basic;

    @Expose
    @SerializedName("care")
    private Care care;

    @Expose
    @SerializedName("performances")
    private Performances performances;

    @Expose
    @SerializedName("nutritions")
    private Nutritions nutritions;

    @Expose
    @SerializedName("remarks")
    private Remarks remarks;

    @Expose
    @SerializedName("diseases")
    private List<String> diseases;

    @Expose
    @SerializedName("addresses")
    private List<Address> addresses;

    public Basic getBasic() {
        return basic;
    }

    public void setBasic(Basic basic) {
        this.basic = basic;
    }

    public Care getCare() {
        return care;
    }

    public void setCare(Care care) {
        this.care = care;
    }

    public Performances getPerformances() {
        return performances;
    }

    public void setPerformances(Performances performances) {
        this.performances = performances;
    }

    public Nutritions getNutritions() {
        return nutritions;
    }

    public void setNutritions(Nutritions nutritions) {
        this.nutritions = nutritions;
    }

    public Remarks getRemarks() {
        return remarks;
    }

    public void setRemarks(Remarks remarks) {
        this.remarks = remarks;
    }

    public List<String> getDiseases() {
        return diseases;
    }

    public void setDiseases(List<String> diseases) {
        this.diseases = diseases;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public static class Builder extends AddTakerRequest {
        public Builder addBasic(Basic basic){
            setBasic(basic);
            return this;
        }

        public Builder addCare(Care care){
            setCare(care);
            return this;
        }

        public Builder addPerformances(Performances performances){
            setPerformances(performances);
            return this;
        }

        public Builder addNutritions(Nutritions nutritions){
            setNutritions(nutritions);
            return this;
        }

        public Builder addRemarks(Remarks remarks){
            setRemarks(remarks);
            return this;
        }

        public Builder addDiseases(List<String> diseases){
            setDiseases(diseases);
            return this;
        }

        public Builder addAddresses(List<Address> addresses){
            setAddresses(addresses);
            return this;
        }

        public AddTakerRequest build(){
            AddTakerRequest request = new AddTakerRequest();
            request.setBasic(getBasic());
            request.setCare(getCare());
            request.setPerformances(getPerformances());
            request.setNutritions(getNutritions());
            request.setRemarks(getRemarks());
            request.setDiseases(getDiseases());
            request.setAddresses(getAddresses());
            return request;
        }
    }

    public static class Basic{

        @Expose
        @SerializedName("address")
        private Address address;

        @Expose
        @SerializedName("birth")
        private String birth;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("relation")
        private String relation;
        private String relationStr;

        @Expose
        @SerializedName("religion")
        private String religion;
        private String religionStr;

        @Expose
        @SerializedName("bloodType")
        private String bloodType;

        @Expose
        @SerializedName("liveWith")
        private String liveWith;
        private String liveWithStr;

        @Expose
        @SerializedName("isBlhsr")
        private boolean isBlhsr;

        private Calendar birthCalendar;
        private String year;
        private String month;
        private String day;

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        public String getReligion() {
            return religion;
        }

        public void setReligion(String religion) {
            this.religion = religion;
        }

        public String getBloodType() {
            return bloodType;
        }

        public void setBloodType(String bloodType) {
            this.bloodType = bloodType;
        }

        public String getLiveWith() {
            return liveWith;
        }

        public void setLiveWith(String liveWith) {
            this.liveWith = liveWith;
        }

        public boolean isBlhsr() {
            return isBlhsr;
        }

        public void setBlhsr(boolean blhsr) {
            isBlhsr = blhsr;
        }

        public boolean isValid(){
            boolean valid = true;

            if(CommonUtils.checkNullAndEmpty(getRelation())){
                valid = false;
            }

            if(CommonUtils.checkNullAndEmpty(getName())){
                valid = false;
            }

            if(CommonUtils.checkNullAndEmpty(getBirth())){
                valid = false;
            }

            if(getAddress() == null){
                valid = false;
            }

            if(CommonUtils.checkNullAndEmpty(getReligion())){
                valid = false;
            }

            if(CommonUtils.checkNullAndEmpty(getBloodType())){
                valid = false;
            }

            if(CommonUtils.checkNullAndEmpty(getLiveWith())){
                valid = false;
            }

            return valid;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public Calendar getBirthCalendar() {
            return birthCalendar;
        }

        public void setBirthCalendar(Calendar birthCalendar) {
            this.birthCalendar = birthCalendar;
            year = String.valueOf(birthCalendar.get(Calendar.YEAR));
            month = String.valueOf(birthCalendar.get(Calendar.MONTH) + 1);
            day = String.valueOf(birthCalendar.get(Calendar.DAY_OF_MONTH));
        }

        public String getRelationStr() {
            return relationStr;
        }

        public void setRelationStr(String relationStr) {
            this.relationStr = relationStr;
        }

        public String getReligionStr() {
            return religionStr;
        }

        public void setReligionStr(String religionStr) {
            this.religionStr = religionStr;
        }

        public String getLiveWithStr() {
            return liveWithStr;
        }

        public void setLiveWithStr(String liveWithStr) {
            this.liveWithStr = liveWithStr;
        }
    }

    public static class Care{
        @Expose
        @SerializedName("height")
        private String height;

        @Expose
        @SerializedName("weight")
        private String weight;

        @Expose
        @SerializedName("ltiNumber")
        private String ltiNumber;

        @Expose
        @SerializedName("ltiGrade")
        private String ltiGrade;

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getLtiNumber() {
            return ltiNumber;
        }

        public void setLtiNumber(String ltiNumber) {
            this.ltiNumber = ltiNumber;
        }

        public String getLtiGrade() {
            return ltiGrade;
        }

        public void setLtiGrade(String ltiGrade) {
            this.ltiGrade = ltiGrade;
        }
    }

    public static class Performances{
        @Expose
        @SerializedName("meal")
        private String meal;
        private String mealStr;

        @Expose
        @SerializedName("walk")
        private String walk;
        private String walkStr;

        @Expose
        @SerializedName("wash")
        private String wash;
        private String washStr;

        @Expose
        @SerializedName("cloth")
        private String cloth;
        private String clothStr;

        @Expose
        @SerializedName("bath")
        private String bath;
        private String bathStr;

        @Expose
        @SerializedName("toilet")
        private String toilet;
        private String toiletStr;

        public String getMeal() {
            return meal;
        }

        public void setMeal(String meal) {
            this.meal = meal;
        }

        public String getWalk() {
            return walk;
        }

        public void setWalk(String walk) {
            this.walk = walk;
        }

        public String getWash() {
            return wash;
        }

        public void setWash(String wash) {
            this.wash = wash;
        }

        public String getCloth() {
            return cloth;
        }

        public void setCloth(String cloth) {
            this.cloth = cloth;
        }

        public String getBath() {
            return bath;
        }

        public void setBath(String bath) {
            this.bath = bath;
        }

        public String getToilet() {
            return toilet;
        }

        public void setToilet(String toilet) {
            this.toilet = toilet;
        }

        public String getMealStr() {
            return mealStr;
        }

        public void setMealStr(String mealStr) {
            this.mealStr = mealStr;
        }

        public String getWalkStr() {
            return walkStr;
        }

        public void setWalkStr(String walkStr) {
            this.walkStr = walkStr;
        }

        public String getWashStr() {
            return washStr;
        }

        public void setWashStr(String washStr) {
            this.washStr = washStr;
        }

        public String getClothStr() {
            return clothStr;
        }

        public void setClothStr(String clothStr) {
            this.clothStr = clothStr;
        }

        public String getBathStr() {
            return bathStr;
        }

        public void setBathStr(String bathStr) {
            this.bathStr = bathStr;
        }

        public String getToiletStr() {
            return toiletStr;
        }

        public void setToiletStr(String toiletStr) {
            this.toiletStr = toiletStr;
        }
    }

    public static class Nutritions{
        @Expose
        @SerializedName("favorite")
        private String favorite;

        @Expose
        @SerializedName("hate")
        private String hate;

        @Expose
        @SerializedName("allergy")
        private String allergy;

        @Expose
        @SerializedName("isDiabetic")
        private boolean isDiabetic;

        public String getFavorite() {
            return favorite;
        }

        public void setFavorite(String favorite) {
            this.favorite = favorite;
        }

        public String getHate() {
            return hate;
        }

        public void setHate(String hate) {
            this.hate = hate;
        }

        public String getAllergy() {
            return allergy;
        }

        public void setAllergy(String allergy) {
            this.allergy = allergy;
        }

        public boolean isDiabetic() {
            return isDiabetic;
        }

        public void setDiabetic(boolean diabetic) {
            isDiabetic = diabetic;
        }
    }

    public static class Remarks{
        @Expose
        @SerializedName("habit")
        private String habit;

        @Expose
        @SerializedName("tendency")
        private String tendency;
        private String tendencyStr;

        @Expose
        @SerializedName("speak")
        private String speak;
        private String speakStr;

        public String getHabit() {
            return habit;
        }

        public void setHabit(String habit) {
            this.habit = habit;
        }

        public String getTendency() {
            return tendency;
        }

        public void setTendency(String tendency) {
            this.tendency = tendency;
        }

        public String getSpeak() {
            return speak;
        }

        public void setSpeak(String speak) {
            this.speak = speak;
        }

        public String getTendencyStr() {
            return tendencyStr;
        }

        public void setTendencyStr(String tendencyStr) {
            this.tendencyStr = tendencyStr;
        }

        public String getSpeakStr() {
            return speakStr;
        }

        public void setSpeakStr(String speakStr) {
            this.speakStr = speakStr;
        }
    }
}
