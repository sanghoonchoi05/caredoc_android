package com.onestepmore.caredoc.data.repository;

import android.arch.lifecycle.MediatorLiveData;

import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailEntranceResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailMedicalResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchCodeResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchResponse;
import com.onestepmore.caredoc.data.model.api.object.FacilitySimpleObject;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchTemp;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchMapList;
import com.onestepmore.caredoc.data.remote.FacilitySearchApiHelper;
import com.onestepmore.caredoc.di.RealmInfo;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmResults;

@Singleton
public class FacilityRepository implements FacilityDataSource {
    private final FacilitySearchApiHelper mFacilitySearchApiHelper;
    private final Realm mRealm;
    private MediatorLiveData<Integer> mObservableFacilityTotalCount;
    private int mCachedFacilityTotalCount;

    @Inject
    public FacilityRepository(FacilitySearchApiHelper facilitySearchApiHelper,
                              @RealmInfo Realm realm) {
        mFacilitySearchApiHelper = facilitySearchApiHelper;
        mRealm = realm;

        mObservableFacilityTotalCount = new MediatorLiveData<>();
        mObservableFacilityTotalCount.setValue(0);
        mCachedFacilityTotalCount = 0;
    }

    public Realm getRealm() {
        return mRealm;
    }

    public MediatorLiveData<Integer> getObservableFacilityTotalCount() {
        return mObservableFacilityTotalCount;
    }

    private void setObservableFacilityTotalCount(int facilityTotalCount) {
        this.mObservableFacilityTotalCount.setValue(facilityTotalCount);
    }

    public void setCachedFacilityTotalCount(int mCachedFacilityTotalCount) {
        this.mCachedFacilityTotalCount = mCachedFacilityTotalCount;
    }

    public void fetchFacilityTotalCount(){
        setObservableFacilityTotalCount(mCachedFacilityTotalCount);
    }

    @Override
    public Flowable<FacilitySearchMapResponse> getFacilitySearchClusterApiCall(FacilitySearchMapRequest.QueryParameter queryParameter) {
        return mFacilitySearchApiHelper.getFacilitySearchClusterApiCall(queryParameter);
    }

    @Override
    public Flowable<FacilitySearchResponse> getFacilitySearchListApiCall(FacilitySearchRequest.QueryParameter queryParameter) {
        return mFacilitySearchApiHelper.getFacilitySearchListApiCall(queryParameter);
    }

    @Override
    public Flowable<FacilitySearchCodeResponse> getFacilitySearchCodeApiCall(FacilitySearchMapRequest.QueryParameter queryParameter) {
        return mFacilitySearchApiHelper.getFacilitySearchCodeApiCall(queryParameter);
    }

    @Override
    public Flowable<FacilityDetailResponse> getFacilityDetailBaseApiCall(FacilityDetailRequest.PathParameter pathParameter) {
        return mFacilitySearchApiHelper.getFacilityDetailBaseApiCall(pathParameter);
    }

    @Override
    public Flowable<FacilityDetailMedicalResponse> getFacilityHospitalDetailApiCall(FacilityDetailRequest.PathParameter pathParameter) {
        return mFacilitySearchApiHelper.getFacilityHospitalDetailApiCall(pathParameter);
    }

    @Override
    public Flowable<FacilityDetailEntranceResponse> getFacilityHomecareDetailApiCall(FacilityDetailRequest.PathParameter pathParameter) {
        return mFacilitySearchApiHelper.getFacilityHomecareDetailApiCall(pathParameter);
    }

    @Override
    public Flowable<JSONObject> getFacilityCareCoordiListApiCall(CareCoordiRequest.QueryParameter queryParameter) {
        return mFacilitySearchApiHelper.getFacilityCareCoordiListApiCall(queryParameter);
    }

    @Override
    public Flowable<Integer> loadFacilitySearchList(FacilitySearchRequest.QueryParameter queryParameter) {
        return getFacilitySearchListApiCall(queryParameter)
                .map(response -> {
                    setCachedFacilityTotalCount(response.getTotal());
                    return response;
                })
                .map(this::getFacilitySearchList)
                .map(this::saveFacilitySearchList);
    }

    private List<FacilityRealmModel> getFacilitySearchList(FacilitySearchResponse response){
        List<FacilityRealmModel> facilityList = new ArrayList<>();
        for(FacilitySimpleObject facilitySimpleObject : response.getItems()){
            FacilitySearchList facility = new FacilitySearchList();
            facility.setFacility(facilitySimpleObject);
            facilityList.add(facility);
        }

        return facilityList;
    }

    @Override
    public Flowable<Boolean> loadFacilitySearchCode(FacilitySearchMapRequest.QueryParameter queryParameter, String code) {
        return getFacilitySearchCodeApiCall(queryParameter)
                .map(response -> getFacilitySearchMap(response, code))
                .map(this::saveFacility);
    }

    @Override
    public Flowable<Boolean> loadFacilityHospitalDetail(FacilityDetailRequest.PathParameter pathParameter) {
        return getFacilityHospitalDetailApiCall(pathParameter)
                .map(this::getFacilityHospitalDetail)
                .map(this::saveFacility);
    }

    @Override
    public Flowable<Boolean> loadFacilityHomecareDetail(FacilityDetailRequest.PathParameter pathParameter) {
        return getFacilityHomecareDetailApiCall(pathParameter)
                .map(this::getFacilityHomecareDetail)
                .map(this::saveFacility);
    }


    private List<FacilityRealmModel> getFacilitySearchMap(FacilitySearchCodeResponse response, String code){
        List<FacilityRealmModel> facilityList = new ArrayList<>();
        for(FacilitySimpleObject facilityObject : response.getItems()){
            FacilitySearchMapList facility = new FacilitySearchMapList();
            facility.setFacility(facilityObject, code);
            facilityList.add(facility);
        }

        return facilityList;
    }

    private FacilityRealmModel getFacilityHospitalDetail(FacilityDetailMedicalResponse response){
        FacilitySearchTemp facilitySearchTemp = new FacilitySearchTemp();
        facilitySearchTemp.setFacility(response);

        return facilitySearchTemp;
    }

    private FacilityRealmModel getFacilityHomecareDetail(FacilityDetailEntranceResponse response){
        FacilitySearchTemp facilitySearchTemp = new FacilitySearchTemp();
        facilitySearchTemp.setFacility(response);

        return facilitySearchTemp;
    }

    private Boolean saveFacility(List<? extends FacilityRealmModel> facilityList){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        for(FacilityRealmModel facility : facilityList){
            realm.copyToRealmOrUpdate(facility);
        }
        realm.commitTransaction();
        realm.close();
        return true;
    }

    private Boolean saveFacility(FacilityRealmModel facilityRealmModel){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(facilityRealmModel);
        realm.commitTransaction();
        realm.close();
        return true;
    }

    private Integer saveFacilitySearchList(List<? extends FacilityRealmModel> facilityList){
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        for(FacilityRealmModel facility : facilityList){
            realm.copyToRealmOrUpdate(facility);
        }
        realm.commitTransaction();
        realm.close();

        return facilityList.size();
    }
    @Override
    public void clearFacilityRealm(Class<? extends FacilityRealmModel> facilityClass){
        getRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<? extends FacilityRealmModel> realmResults = realm.where(facilityClass).findAll();
                if(realmResults != null){
                    for(FacilityRealmModel realmModel : realmResults){
                        realmModel.deleteRealm(realmModel);
                    }
                }
            }
        });
    }

    @Override
    public RealmResults<FacilitySearchTemp> getFacilitySearchTemp() {
        return getRealm().where(FacilitySearchTemp.class).findAll();
    }

    @Override
    public RealmResults<FacilitySearchList> getFacilitySearchList() {
        return getRealm().where(FacilitySearchList.class).findAll();
    }

    @Override
    public RealmResults<FacilitySearchMapList> getFacilitySearchMapList(boolean show) {
        return getRealm().where(FacilitySearchMapList.class).equalTo("show", show).findAll();
    }

    @Override
    public RealmResults<FacilitySearchMapList> getFacilitySearchMapList(String code) {
        return getRealm().where(FacilitySearchMapList.class).equalTo("code", code).findAll();
    }

    @Override
    public void showFacilitySearchMapList(List<FacilitySearchMapList> list, boolean show) {
        getRealm().beginTransaction();
        for(FacilitySearchMapList searchMapList : list){
            searchMapList.setShow(show);
        }
        getRealm().copyToRealmOrUpdate(list);
        getRealm().commitTransaction();
    }
}
