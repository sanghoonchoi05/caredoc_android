package com.onestepmore.caredoc.data.local.pref;

public interface PreferencesHelper {

    String getAccessToken();
    void setAccessToken(String accessToken);

    void setExpiresIn(long expiresIn);
    long getExpiresIn();

    String getTokenType();
    void setTokenType(String tokenType);

    boolean isLocationPermissionGranted();
    void setLocationPermissionGranted(boolean granted);

    String getLastKnownLocationLatitude();
    void setLastKnownLocationLatitude(String latitude);

    String getLastKnownLocationLongitude();
    void setLastKnownLocationLongitude(String longitude);

    int getTotalFacilityCount();
    void setTotalFacilityCount(int totalCount);

    boolean isShowEventPopup();
    void setShowEventPopup(boolean show);
}
