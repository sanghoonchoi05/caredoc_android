package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class TimeTablesObject extends ItemResponse<TimeTablesObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("id")
        private int id;

        @Expose
        @SerializedName("timeable_type")
        private String timeableType;

        @Expose
        @SerializedName("timeable_id")
        private int timeableId;

        @Expose
        @SerializedName("type_code")
        private String typeCode;

        @Expose
        @SerializedName("label")
        private String label;

        @Expose
        @SerializedName("from")
        private String from;

        @Expose
        @SerializedName("to")
        private String to;

        @Expose
        @SerializedName("text")
        private String text;

        @Expose
        @SerializedName("type_str")
        private String typeStr;

        public int getId() {
            return id;
        }

        public String getTimeableType() {
            return timeableType;
        }

        public int getTimeableId() {
            return timeableId;
        }

        public String getTypeCode() {
            return typeCode;
        }

        public String getLabel() {
            return label;
        }

        public String getFrom() {
            return from;
        }

        public String getTo() {
            return to;
        }

        public String getText() {
            return text;
        }

        public String getTypeStr() {
            return typeStr;
        }
    }
}
