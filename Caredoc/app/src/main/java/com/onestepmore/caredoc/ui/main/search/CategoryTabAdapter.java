package com.onestepmore.caredoc.ui.main.search;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.databinding.ISearchCategoryTabBinding;

import io.realm.RealmResults;

public class CategoryTabAdapter extends RecyclerView.Adapter<CategoryTabAdapter.CategoryTabViewHolder>{
    private RealmResults<StaticCategory> mCategoryList = null;
    private Context mContext;
    private float [] mAlphaList = null;
    private final CategoryTabCallback mCallback;

    public CategoryTabAdapter(Context context, CategoryTabCallback callback){
        this.mCallback = callback;
        this.mContext = context;
        this.mCategoryList = RealmDataAccessor.getInstance().getStaticCategory();
        mAlphaList = new float[mCategoryList.size()];
        for(int i=0; i<mAlphaList.length; i++){
            mAlphaList[i] = 0.5f;
        }
    }

    public void setSelect(boolean select, int position){
        float alpha = select ? 1.0f : 0.5f;
        mAlphaList[position] = alpha;
        notifyItemChanged(position);
    }

    @NonNull
    @Override
    public CategoryTabViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ISearchCategoryTabBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_search_category_tab, viewGroup, false);
        return new CategoryTabViewHolder(binding, mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryTabViewHolder facilityViewHolder, int position) {
        StaticCategory category = mCategoryList.get(position);
        if(category != null){
            facilityViewHolder.binding.setText(category.getText());
            //facilityViewHolder.binding.setIcon(CommonUtils.getCategoryIconForTab(mContext, category.getStr()));
        }

        facilityViewHolder.binding.setAlpha(mAlphaList[position]);
        facilityViewHolder.setPosition(position);
        facilityViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCategoryList == null ? 0 : mCategoryList.size();
    }

    static class CategoryTabViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final ISearchCategoryTabBinding binding;
        final CategoryTabCallback callback;
        int position;

        private CategoryTabViewHolder(ISearchCategoryTabBinding binding, CategoryTabCallback callback) {
            super(binding.getRoot());
            this.binding = binding;
            this.callback = callback;
            this.binding.iSearchCategoryTabLayout.setOnClickListener(this);
        }

        public void setPosition(int position){
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            callback.onSelect(position);
        }
    }
}
