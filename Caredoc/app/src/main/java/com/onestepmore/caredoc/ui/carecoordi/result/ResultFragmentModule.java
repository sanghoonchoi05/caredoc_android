package com.onestepmore.caredoc.ui.carecoordi.result;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.carecoordi.classification.ClassificationViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ResultFragmentModule {
    @Provides
    ResultViewModel resultViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new ResultViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("ResultFragment")
    ViewModelProvider.Factory provideResultViewModelProvider(ResultViewModel resultViewModel) {
        return new ViewModelProviderFactory<>(resultViewModel);
    }
}
