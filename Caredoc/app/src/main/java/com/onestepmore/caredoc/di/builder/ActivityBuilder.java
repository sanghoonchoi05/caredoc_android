/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.di.builder;

import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.account.AccountMainActivityModule;
import com.onestepmore.caredoc.ui.account.account.AccountFragmentProvider;
import com.onestepmore.caredoc.ui.account.login.LoginFragmentProvider;
import com.onestepmore.caredoc.ui.account.login.forgot.LoginForgotFragmentProvider;
import com.onestepmore.caredoc.ui.account.register.email.EmailRegisterFragmentProvider;
import com.onestepmore.caredoc.ui.account.terms.TermsFragmentProvider;
import com.onestepmore.caredoc.ui.address.AddressFragmentProvider;
import com.onestepmore.caredoc.ui.address.AddressMainActivity;
import com.onestepmore.caredoc.ui.address.AddressMainActivityModule;
import com.onestepmore.caredoc.ui.address.detail.AddressDetailFragmentProvider;
import com.onestepmore.caredoc.ui.address.result.AddressResultFragmentProvider;
import com.onestepmore.caredoc.ui.care.CareInfoFragmentProvider;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivityModule;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiStartActivity;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiStartActivityModule;
import com.onestepmore.caredoc.ui.carecoordi.classification.ClassificationFragmentProvider;
import com.onestepmore.caredoc.ui.carecoordi.guide.GuideFragmentProvider;
import com.onestepmore.caredoc.ui.carecoordi.loading.LoadingFragmentProvider;
import com.onestepmore.caredoc.ui.carecoordi.result.ResultFragmentProvider;
import com.onestepmore.caredoc.ui.carecoordi.service.ServiceFragmentProvider;
import com.onestepmore.caredoc.ui.carecoordi.userinfo.UserInfoFragmentProvider;
import com.onestepmore.caredoc.ui.estimate.request.EstimateRequestStartActivity;
import com.onestepmore.caredoc.ui.estimate.request.EstimateRequestStartActivityModule;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.MainActivityModule;
import com.onestepmore.caredoc.ui.main.detail.FacilityActivity;
import com.onestepmore.caredoc.ui.main.detail.FacilityActivityModule;
import com.onestepmore.caredoc.ui.main.detail.FacilityDetailFragmentProvider;
import com.onestepmore.caredoc.ui.main.detail.comment.write.WriteCommentActivity;
import com.onestepmore.caredoc.ui.main.detail.comment.write.WriteCommentActivityModule;
import com.onestepmore.caredoc.ui.main.detail.homecare.HomecareDetailFragmentProvider;
import com.onestepmore.caredoc.ui.main.detail.hospital.HospitalDetailFragmentProvider;
import com.onestepmore.caredoc.ui.main.detail.photo.PhotoActivity;
import com.onestepmore.caredoc.ui.main.detail.photo.PhotoActivityModule;
import com.onestepmore.caredoc.ui.main.detail.review.ReviewActivity;
import com.onestepmore.caredoc.ui.main.detail.review.ReviewActivityModule;
import com.onestepmore.caredoc.ui.main.detail.review.write.WriteReviewActivity;
import com.onestepmore.caredoc.ui.main.detail.review.write.WriteReviewActivityModule;
import com.onestepmore.caredoc.ui.main.favorite.FavoriteFragmentProvider;
import com.onestepmore.caredoc.ui.main.favorite.detail.FavoriteDetailFragmentProvider;
import com.onestepmore.caredoc.ui.main.home.HomeFragmentProvider;
import com.onestepmore.caredoc.ui.main.profile.ProfileFragmentProvider;
import com.onestepmore.caredoc.ui.main.profile.collaboration.CollaborationFragmentProvider;
import com.onestepmore.caredoc.ui.main.profile.contact.ContactFragmentProvider;
import com.onestepmore.caredoc.ui.main.profile.recently.RecentlyFragmentProvider;
import com.onestepmore.caredoc.ui.main.profile.webview.WebViewActivity;
import com.onestepmore.caredoc.ui.main.profile.webview.WebViewActivityModule;
import com.onestepmore.caredoc.ui.main.search.SearchFragmentProvider;
import com.onestepmore.caredoc.ui.main.search.dialog.SearchDialogProvider;
import com.onestepmore.caredoc.ui.main.search.filter.address.AddressFilterFragmentProvider;
import com.onestepmore.caredoc.ui.main.search.filter.address.list.AddressListFragmentProvider;
import com.onestepmore.caredoc.ui.main.search.filter.detail.FilterDetailFragmentProvider;
import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilterFragmentProvider;
import com.onestepmore.caredoc.ui.main.search.list.SearchListFragmentProvider;
import com.onestepmore.caredoc.ui.main.search.map.SearchMapFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivityModule;
import com.onestepmore.caredoc.ui.main.taker.add.address.TakerServiceAddressFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.add.basic.TakerBasicFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.add.care.TakerCareFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.add.disease.TakerDiseaseFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.add.food.TakerFoodFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.add.performance.TakerPerformanceFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.add.remark.TakerRemarkFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.card.TakerCardFragmentProvider;
import com.onestepmore.caredoc.ui.main.taker.detail.TakerDetailActivity;
import com.onestepmore.caredoc.ui.main.taker.detail.TakerDetailActivityModule;
import com.onestepmore.caredoc.ui.splash.SplashActivity;
import com.onestepmore.caredoc.ui.splash.SplashActivityModule;
import com.onestepmore.caredoc.ui.start.StartActivity;
import com.onestepmore.caredoc.ui.start.StartActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = StartActivityModule.class)
    abstract StartActivity bindStartActivity();

    @ContributesAndroidInjector(modules = {
            TermsFragmentProvider.class,
            AccountMainActivityModule.class,
            AccountFragmentProvider.class,
            LoginFragmentProvider.class,
            EmailRegisterFragmentProvider.class,
            LoginForgotFragmentProvider.class
    })
    abstract AccountMainActivity bindAccountMainActivity();

    @ContributesAndroidInjector(modules = CareCoordiStartActivityModule.class)
    abstract CareCoordiStartActivity bindCareCoordiActivity();

    @ContributesAndroidInjector(modules = {
            CareCoordiActivityModule.class,
            ServiceFragmentProvider.class,
            TakerCardFragmentProvider.class,
            UserInfoFragmentProvider.class,
            ClassificationFragmentProvider.class,
            AddressFilterFragmentProvider.class,
            GuideFragmentProvider.class,
            ResultFragmentProvider.class,
            LoadingFragmentProvider.class
    })
    abstract CareCoordiActivity bindCareGuideActivity();

    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashActivity bindSplashActivity();

    @ContributesAndroidInjector(modules = {
            MainActivityModule.class,
            HomeFragmentProvider.class,
            SearchFragmentProvider.class,
            AddressFilterFragmentProvider.class,
            AddressListFragmentProvider.class,
            ServiceFilterFragmentProvider.class,
            FavoriteFragmentProvider.class,
            ProfileFragmentProvider.class,
            SearchListFragmentProvider.class,
            SearchMapFragmentProvider.class,
            FavoriteDetailFragmentProvider.class,
            ContactFragmentProvider.class,
            CollaborationFragmentProvider.class,
            RecentlyFragmentProvider.class,
            TakerCardFragmentProvider.class,
            CareInfoFragmentProvider.class,
            SearchDialogProvider.class,
            FilterDetailFragmentProvider.class
    })
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {
            FacilityActivityModule.class,
            FacilityDetailFragmentProvider.class,
            HomecareDetailFragmentProvider.class,
            HospitalDetailFragmentProvider.class,
    })
    abstract FacilityActivity bindFacilityActivity();

    @ContributesAndroidInjector(modules = WebViewActivityModule.class)
    abstract WebViewActivity bindWebViewActivity();

    @ContributesAndroidInjector(modules = WriteCommentActivityModule.class)
    abstract WriteCommentActivity bindWriteCommentActivity();

    @ContributesAndroidInjector(modules = PhotoActivityModule.class)
    abstract PhotoActivity bindPhotoActivity();

    @ContributesAndroidInjector(modules = ReviewActivityModule.class)
    abstract ReviewActivity bindReviewActivity();

    @ContributesAndroidInjector(modules = WriteReviewActivityModule.class)
    abstract WriteReviewActivity bindWriteReviewActivity();

    @ContributesAndroidInjector(modules = TakerDetailActivityModule.class)
    abstract TakerDetailActivity bindTakerDetailActivity();

    @ContributesAndroidInjector(modules = {
            AddTakerActivityModule.class,
            AddressFilterFragmentProvider.class,
            TakerBasicFragmentProvider.class,
            TakerServiceAddressFragmentProvider.class,
            TakerCareFragmentProvider.class,
            TakerDiseaseFragmentProvider.class,
            TakerFoodFragmentProvider.class,
            TakerPerformanceFragmentProvider.class,
            TakerRemarkFragmentProvider.class
    })
    abstract AddTakerActivity bindAddTakerActivity();

    @ContributesAndroidInjector(modules = {
            AddressMainActivityModule.class,
            AddressFragmentProvider.class,
            AddressResultFragmentProvider.class,
            AddressDetailFragmentProvider.class
    })
    abstract AddressMainActivity bindAddressMainActivity();

    @ContributesAndroidInjector(modules = EstimateRequestStartActivityModule.class)
    abstract EstimateRequestStartActivity bindEstimateRequestStartActivity();
}
