package com.onestepmore.caredoc.ui.main.detail;

import android.content.Context;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.object.ServiceDetailObject;
import com.onestepmore.caredoc.databinding.VFacilityDetailServiceTabBinding;
import com.onestepmore.caredoc.ui.main.detail.base.BaseFacilityDetailFragment;
import com.onestepmore.caredoc.utils.CommonUtils;

import java.util.ArrayList;
import java.util.List;

public class FacilityDetailPagerAdapter extends FragmentStatePagerAdapter {

    private List<BaseFacilityDetailFragment> facilityDetailFragmentList = new ArrayList<>();

    public FacilityDetailPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFacilityDetailFragment(BaseFacilityDetailFragment fragment, FacilityDetailNavigator navigator) {
        fragment.setFacilityDetailNavigator(navigator);
        facilityDetailFragmentList.add(fragment);
    }

    @Override
    public int getCount() {
        return facilityDetailFragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return facilityDetailFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    public View getTabView(int position, Context context) {
        VFacilityDetailServiceTabBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.v_facility_detail_service_tab, null, false);
        ServiceDetailObject serviceDetailObject = facilityDetailFragmentList.get(position).getServiceDetailObject();
        binding.vFacilityDetailServiceTabGradeText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
        binding.setTitle(serviceDetailObject.getServiceName());
        binding.setGrade(serviceDetailObject.getLatestGrade());
        binding.setGradeIcon(CommonUtils.getOvalDrawableWithGrade(context, serviceDetailObject.getLatestGrade(), serviceDetailObject.isClosed()));

        ColorStateList textColor = context.getResources().getColorStateList(R.color.white);
        binding.vFacilityDetailServiceTabTitle.setPaintFlags(0);
        if(serviceDetailObject.isClosed()){
            textColor = context.getResources().getColorStateList(R.color.colorClosedText);
            binding.vFacilityDetailServiceTabTitle.setPaintFlags(binding.vFacilityDetailServiceTabTitle.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
        }
        binding.vFacilityDetailServiceTabGradeText.setTextColor(textColor);

        return binding.getRoot();
    }
}
