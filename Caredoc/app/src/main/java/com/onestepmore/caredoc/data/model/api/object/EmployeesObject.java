package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class EmployeesObject extends ItemResponse<EmployeesObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("code")
        private String code;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("total_count")
        private int totalCount;

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public int getTotalCount() {
            return totalCount;
        }
    }
}
