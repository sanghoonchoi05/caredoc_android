package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class AvatarObject {
    @Expose
    @SerializedName("attach_type")
    private String attachType;
    @Expose
    @SerializedName("order")
    private int order;
    @Expose
    @SerializedName("url")
    private String url;
    @Expose
    @SerializedName("thumbnail_url")
    private String thumbnailUrl;
    @Expose
    @SerializedName("description")
    private String description;

    public String getAttachType() {
        return attachType;
    }

    public int getOrder() {
        return order;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getDescription() {
        return description;
    }
}
