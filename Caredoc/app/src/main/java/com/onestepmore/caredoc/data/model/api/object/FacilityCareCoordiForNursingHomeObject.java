package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityCareCoordiForNursingHomeObject extends FacilityCareCoordiObject {
    @Expose
    @SerializedName("scale_count")
    private int scaleCount;

    public int getScaleCount() {
        return scaleCount;
    }
}
