package com.onestepmore.caredoc.data.model.api.review;

import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.object.ReviewServiceObject;

public final class ReviewFacilityServiceResponse extends ItemResponse<ReviewServiceObject> {

}
