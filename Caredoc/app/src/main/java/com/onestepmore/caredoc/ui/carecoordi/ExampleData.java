package com.onestepmore.caredoc.ui.carecoordi;

public class ExampleData {
    private CareCoordiActivity.EXAMPLE example;
    private boolean isSelected;

    public CareCoordiActivity.EXAMPLE getExample() {
        return example;
    }

    public void setExample(CareCoordiActivity.EXAMPLE example) {
        this.example = example;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
