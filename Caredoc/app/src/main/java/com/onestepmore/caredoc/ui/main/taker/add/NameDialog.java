package com.onestepmore.caredoc.ui.main.taker.add;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.DialogCommonYnBinding;
import com.onestepmore.caredoc.databinding.DialogNameBinding;
import com.onestepmore.caredoc.ui.base.BaseDialog;
import com.onestepmore.caredoc.utils.CommonUtils;

public class NameDialog extends BaseDialog{

    public interface OnNameInputListener{
        void OnNameOk(String name);
    }

    private OnNameInputListener listener;

    public static NameDialog newInstance(String name) {
        Bundle args = new Bundle();
        NameDialog dialog = new NameDialog();
        args.putString("name", name);
        dialog.setArguments(args);

        return dialog;

    }
    public void setOnNameInputListener(OnNameInputListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DialogNameBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()), R.layout.dialog_name, null, false);

        binding.dialogNameCancel.setOnClickListener((view) -> {
            dismissDialog(getTag());
        });

        binding.dialogNameOk.setOnClickListener((view) -> {
            if(CommonUtils.checkNullAndEmpty(binding.dialogNameEdit.getText().toString())) {
                Toast.makeText(getContext(), R.string.name_dialog_title, Toast.LENGTH_SHORT).show();
            }else{
                dismissDialog(getTag());
                if (listener != null) {
                    listener.OnNameOk(binding.dialogNameEdit.getText().toString());
                }
            }
        });

        if(getArguments() != null){
            binding.setName(getArguments().getString("name"));
        }

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);


        return dialog;
    }
}
