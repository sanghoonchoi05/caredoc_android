package com.onestepmore.caredoc.ui.support.rx;

import android.databinding.ObservableField;

import com.jakewharton.rxbinding.internal.MainThreadSubscription;

import rx.Observable;
import rx.Subscriber;

import static com.jakewharton.rxbinding.internal.Preconditions.checkUiThread;

public class SupportObservableFieldOnSubscribe<T> implements Observable.OnSubscribe<T>{
    private ObservableField<T> observableField;

    public SupportObservableFieldOnSubscribe(ObservableField<T> observableField){
        this.observableField = observableField;
    }
    @Override
    public void call(Subscriber<? super T> subscriber) {
        checkUiThread();

        final android.databinding.Observable.OnPropertyChangedCallback changedCallback = new android.databinding.Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(android.databinding.Observable sender, int propertyId) {
                subscriber.onNext(((ObservableField<T>)sender).get());
            }
        };

        observableField.addOnPropertyChangedCallback(changedCallback);

        subscriber.add(new MainThreadSubscription() {
            @Override
            protected void onUnsubscribe() {
                observableField.removeOnPropertyChangedCallback(changedCallback);
            }
        });

        // 초기화
        if(observableField.get() != null){
            subscriber.onNext(observableField.get());
        }
    }
}
