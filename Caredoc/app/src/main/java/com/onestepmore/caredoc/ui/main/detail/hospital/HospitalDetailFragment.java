package com.onestepmore.caredoc.ui.main.detail.hospital;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.object.ApiCommonObject;
import com.onestepmore.caredoc.data.model.api.object.EmployeesObject;
import com.onestepmore.caredoc.data.model.api.object.EquipmentObject;
import com.onestepmore.caredoc.data.model.api.object.EvaluationsMedicalObject;
import com.onestepmore.caredoc.data.model.api.object.MealAddOnsObject;
import com.onestepmore.caredoc.data.model.api.object.MedicalSubjectObject;
import com.onestepmore.caredoc.data.model.api.object.NursingAddOnsObject;
import com.onestepmore.caredoc.data.model.api.object.ProgramsObject;
import com.onestepmore.caredoc.data.model.api.object.RatingRelationsObject;
import com.onestepmore.caredoc.data.model.api.object.RatingsSummaryObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceMedicalObject;
import com.onestepmore.caredoc.data.model.api.object.SubsObject;
import com.onestepmore.caredoc.data.model.api.object.TimeTablesObject;
import com.onestepmore.caredoc.data.model.api.review.FacilityCommentResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceSummaryResponse;
import com.onestepmore.caredoc.data.model.realm.Review;
import com.onestepmore.caredoc.databinding.FHospitalDetailBinding;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.detail.FacilityEvaluationListAdapter;
import com.onestepmore.caredoc.ui.main.detail.RatingBarAdapter;
import com.onestepmore.caredoc.ui.main.detail.base.BaseFacilityDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.comment.CommentAdapter;
import com.onestepmore.caredoc.ui.main.detail.comment.RemoveCommentListener;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.DateUtils;
import com.onestepmore.caredoc.utils.GraphicsUtils;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;
import io.realm.Sort;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_DETAIL_CALL;
import static com.onestepmore.caredoc.utils.DateUtils.FACILITY_DETAIL_TIME_INPUT_DATE_FORMAT;
import static com.onestepmore.caredoc.utils.DateUtils.FACILITY_DETAIL_TIME_OUTPUT_DATE_FORMAT;

public class HospitalDetailFragment extends BaseFacilityDetailFragment<FHospitalDetailBinding, HospitalDetailViewModel, ServiceMedicalObject> implements
        HospitalDetailNavigator,
        OnMapReadyCallback,
        RemoveCommentListener
{
    @Inject
    @Named("HospitalDetailFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private HospitalDetailViewModel mHospitalDetailViewModel;

    private DisplayMetrics mDisplayMetrics;
    private GoogleMap mMap;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_hospital_detail;
    }

    @Override
    public HospitalDetailViewModel getViewModel() {
        mHospitalDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(HospitalDetailViewModel.class);
        return mHospitalDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHospitalDetailViewModel.setNavigator(this);

        mDisplayMetrics = new DisplayMetrics();
    }


    @Override
    public void refreshComment() {
        if(getBaseActivity() == null){
            return;
        }

        getViewModel().loadCommentFacilityFromServer(getFacilityDetailObject().getFakeId());
    }

    @Override
    public void refreshReview() {
        if(getBaseActivity() == null){
            return;
        }

        getViewModel().loadReviewServiceSummaryFromServer(getServiceDetailObject().getFakeId());
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Crashlytics
        ServiceMedicalObject serviceMedicalObject = getServiceDetailObject();
        if(serviceMedicalObject == null){
            showCommonDialog(
                    getString(R.string.warning_msg_facility_detail_load_failed),
                    getString(R.string.warning_msg_facility_detail_load_failed_desc),
                    CommonYNDialog.class.getName(),
                    (clickView) -> {
                        if(getFacilityDetailNavigator() != null){
                            getFacilityDetailNavigator().onClose();
                        }
                        else{
                            BackStackManager.getInstance().onBackPressed();
                        }},
                    getString(R.string.common_confirm),
                    "",
                    true);
            return;
        }

        if(getServiceDetailObject().isClosed()){
            getViewDataBinding().fHospitalDetailClosedText.setText(getString(R.string.f_facility_detail_service_closed, getServiceDetailObject().getServiceName()));
            getViewDataBinding().setIsClosed(getServiceDetailObject().isClosed());
            return;
        }

        getViewModel().loadReviewServiceSummaryFromServer(getServiceDetailObject().getFakeId());
        getViewModel().loadCommentFacilityFromServer(getFacilityDetailObject().getFakeId());


        initToolTip();

        setData();
    }

    private void initToolTip(){
        getViewDataBinding().fHospitalDetailMedicalExpensesLayout.fHospitalDetailMedicalExpensesToolTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFacilityDetailNavigator().onShowTooltip(v, getString(R.string.tool_tip_price_title), getString(R.string.tool_tip_price));
            }
        });

        /*getViewDataBinding().fHospitalDetailFacilityInfoLayout.fHospitalDetailFacilityInfoToolTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFacilityDetailNavigator().onShowTooltip(v, getString(R.string.tool_tip_bed));
            }
        });*/

        getViewDataBinding().fHospitalDetailMealsLayout.fHospitalDetailMealsToolTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFacilityDetailNavigator().onShowTooltip(v, getString(R.string.tool_tip_eat_title), getString(R.string.tool_tip_eat));
            }
        });

        getViewDataBinding().fHospitalDetailPersonLayout.fHospitalDetailPersonToolTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFacilityDetailNavigator().onShowTooltip(v, getString(R.string.tool_tip_staff_title), getString(R.string.tool_tip_staff));
            }
        });

        getViewDataBinding().fHospitalDetailMedicalPatientLayout.fHospitalDetailPatientToolTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFacilityDetailNavigator().onShowTooltip(v, getString(R.string.tool_tip_patient_title), getString(R.string.tool_tip_patient));
            }
        });

        getViewDataBinding().fHospitalDetailGradeLayout.vFacilityDetailGradeToolTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFacilityDetailNavigator().onShowTooltip(v, getString(R.string.tool_tip_grade_title), getString(R.string.tool_tip_grade));
            }
        });
    }

    private void initRatingBar(@NonNull List<RatingsSummaryObject.Item> ratings) {
        RatingBarAdapter adapter = new RatingBarAdapter(ratings);
        getViewDataBinding().fHospitalDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        getViewDataBinding().fHospitalDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.setLayoutManager(manager);

        int verticalSpacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_rating_bar_item_space);
        getViewDataBinding().fHospitalDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.addItemDecoration(new SpacesItemDecoration(verticalSpacingInPixels, 0));

        getViewDataBinding().fHospitalDetailRatingLayout.vFacilityDetailRatingBarLeftButton.setOnClickListener(
                (view) -> {
                    if(ratings.size() > 0){
                        getFacilityDetailNavigator().onShowReview(getServiceDetailObject().getFakeId());
                    }
                });
        getViewDataBinding().fHospitalDetailRatingLayout.vFacilityDetailRatingBarRightButton.setOnClickListener(
                (view ) -> {
                    if(ratings.size() > 0){
                        getFacilityDetailNavigator().onWriteReview(getServiceDetailObject().getFakeId(), getServiceDetailObject().getServiceId());
                    }
                });
    }

    private void setRatingLayout(float ratingAvg, int total){
        getViewDataBinding().fHospitalDetailRatingLayout.setRatingAvg(ratingAvg);
        getViewDataBinding().fHospitalDetailRatingLayout.setCount(total);
    }

    private void setData(){
        if(getViewDataBinding() != null){

            getViewDataBinding().setService(getServiceDetailObject());
            getViewDataBinding().setFacilityDetail(getFacilityDetailObject());

            initEvaluationList();
            initPrice();
            initSubjectInfo();
            initPersons();
            initMeals();
            initGrade();
            initProgramInfo();
            initSubInfo();
            initEmployeesInfo();
            initEquipmentInfo();
            initTime();

            initMap();
            initCommentList();
        }
    }

    private void initEvaluationList() {
        FacilityEvaluationListAdapter gradeChartAdapter = new FacilityEvaluationListAdapter(
                getBaseActivity(),
                getServiceDetailObject().getEvaluations().getItems());
        getViewDataBinding().fHospitalDetailEvaluationLayout.fFacilityDetailEvaluationRecyclerView.setAdapter(gradeChartAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fHospitalDetailEvaluationLayout.fFacilityDetailEvaluationRecyclerView.setLayoutManager(layoutManager);

        int horizontalSpace = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_detail_evaluation_item_space);
        int firstLastSpace = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_detail_evaluation_item_first_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, horizontalSpace, true);
        decoration.setSpaceFirstAndLastHorizontal(firstLastSpace);
        getViewDataBinding().fHospitalDetailEvaluationLayout.fFacilityDetailEvaluationRecyclerView.addItemDecoration(decoration);
    }

    private void initTime() {
        StringBuilder closedAll = new StringBuilder();

        List<TimeTablesObject.Item> timeTablesObjects = getServiceDetailObject().getData().getTimeTables().getItems();
        for (TimeTablesObject.Item timeObject : timeTablesObjects) {
            String time = "";
            // 시간대가 있으면
            if (timeObject.getFrom() != null && timeObject.getTo() != null) {
                String startTime = DateUtils.getDate(timeObject.getFrom(), FACILITY_DETAIL_TIME_INPUT_DATE_FORMAT, FACILITY_DETAIL_TIME_OUTPUT_DATE_FORMAT);
                String endTime = DateUtils.getDate(timeObject.getTo(), FACILITY_DETAIL_TIME_INPUT_DATE_FORMAT, FACILITY_DETAIL_TIME_OUTPUT_DATE_FORMAT);
                time = String.format(Locale.getDefault(), "%s~%s", startTime, endTime);
            }
            if (timeObject.getTypeCode().equals("LUNCH")) {
                if (timeObject.getLabel().equals("평일")) {
                    getViewModel().setLunchWeekdays(time);
                } else if (timeObject.getLabel().equals("토요일")) {
                    getViewModel().setLunchSat(time);
                } else if (timeObject.getLabel().equals("일요일")) {
                    getViewModel().setLunchSun(time);
                }

            } else if (timeObject.getTypeCode().equals("CLOSED")) {
                if (timeObject.getLabel().equals("기타")) {
                    getViewModel().setClosedEtc(timeObject.getText());
                } else {
                    if (closedAll.length() > 0) {
                        closedAll.append(",");
                    }
                    closedAll.append(timeObject.getLabel());
                }

            } else if (timeObject.getTypeCode().equals("RECEPTION")) {
                if (timeObject.getLabel().equals("평일")) {
                    getViewModel().setReceptionWeekdays(time);
                } else if (timeObject.getLabel().equals("토요일")) {
                    getViewModel().setReceptionSat(time);
                } else if (timeObject.getLabel().equals("일요일")) {
                    getViewModel().setReceptionSun(time);
                }
            }
        }

        getViewModel().setClosedAll(closedAll.toString());
    }

    private void initMap(){
        FragmentManager fragmentManager = getChildFragmentManager();
        SupportMapFragment supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.v_facility_detail_map);
        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }
    }

    private void initCommentList(){
        RealmResults<Review> reviewRealmResults = getViewModel().getRealm().where(Review.class)
                .sort("isOwn", Sort.DESCENDING).findAll();

        CommentAdapter adapter = new CommentAdapter(reviewRealmResults, this);
        getViewDataBinding().fHospitalDetailCommentLayout.vFacilityDetailCommentRecyclerView.setAdapter(adapter);
        getViewDataBinding().fHospitalDetailCommentLayout.vFacilityDetailCommentRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_comment_item_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(spacingInPixels, 0, true);
        getViewDataBinding().fHospitalDetailCommentLayout.vFacilityDetailCommentRecyclerView.addItemDecoration(decoration);
    }

    private void initPrice() {
        int color = getDiffTextColor(getServiceDetailObject().getData().getMedicalExpensesDiff());
        getViewDataBinding().fHospitalDetailMedicalExpensesLayout.vFacilityDetailMedicalExpensesText3.setTextColor(color);
    }

    private void initSubjectInfo() {
        StringBuilder text = new StringBuilder();
        if(getServiceDetailObject().getData().getMedicalSubjectObject() != null){
            List<MedicalSubjectObject.Item> items = getServiceDetailObject().getData().getMedicalSubjectObject().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(items.get(i).getDoctorCount() <= 0){
                        continue;
                    }

                    if(text.length() > 0){
                        text.append(", ");
                    }
                    text.append(getCountStr(items.get(i).getName(), items.get(i).getDoctorCount()));
                }
            }
        }

        getViewDataBinding().fHospitalDetailSubjectLayout.setText(text.toString());
    }

    private void initPersons() {
        int color = getDiffTextColor(getServiceDetailObject().getData().getPersonPerDoctorDiff());
        getViewDataBinding().fHospitalDetailPersonLayout.fHospitalDetailInfoRow1.vFacilityDetailInfoRowText2.setTextColor(color);

        color = getDiffTextColor(getServiceDetailObject().getData().getPersonPerNurseDiff());
        getViewDataBinding().fHospitalDetailPersonLayout.fHospitalDetailInfoRow3.vFacilityDetailInfoRowText2.setTextColor(color);

        color = getDiffTextColor(getServiceDetailObject().getData().getPersonPerNursingStaffDiff());
        getViewDataBinding().fHospitalDetailPersonLayout.fHospitalDetailInfoRow4.vFacilityDetailInfoRowText2.setTextColor(color);

        for (NursingAddOnsObject.Item item : getServiceDetailObject().getData().getNursingAddOnsObject().getItems()) {
            if (item.getCode().equals("03")) {
                getViewDataBinding().fHospitalDetailPersonLayout.setDoctorGrade(item.getGrade());
            } else if (item.getCode().equals("04")) {
                getViewDataBinding().fHospitalDetailPersonLayout.setNursingStaffGrade(item.getGrade());
            }
        }
    }

    private void initMeals() {
        for (MealAddOnsObject.Item item : getServiceDetailObject().getData().getMealAddOnsObject().getItems()) {
            String addedText = "";
            if (item.getGeneralMealAdded() > 0) {
                addedText = getString(R.string.general_meal_added);
            }
            if (item.getTreatmentMealGrade() > 0) {
                addedText = String.format(addedText + ", " + getString(R.string.treatment_meal_grade), item.getTreatmentMealGrade());
            }

            if (item.getCode().equals("01")) {
                getViewDataBinding().fHospitalDetailMealsLayout.setType01Count(String.format(getString(R.string.meals_type01), item.getCalculationCount()));
                getViewDataBinding().fHospitalDetailMealsLayout.setType01Added(addedText);
            } else {
                getViewDataBinding().fHospitalDetailMealsLayout.setType02Count(String.format(getString(R.string.meals_type02), item.getCalculationCount()));
                getViewDataBinding().fHospitalDetailMealsLayout.setType02Added(addedText);
            }
        }
    }

    // TODO XML에서 직접 바인딩하자..
    private void initGrade() {
        String per = "-";
        EvaluationsMedicalObject.Item item = null;
        if(getServiceDetailObject().getData().getEvaluationsMedicalObject().getItems() != null &&
                getServiceDetailObject().getData().getEvaluationsMedicalObject().getItems().size() > 0){
            item = getServiceDetailObject().getData().getEvaluationsMedicalObject().getItems().get(0);
        }

        getViewModel().setWorseActivityPer(String.format(getString(R.string.common_percent_formatted), per));
        getViewModel().setWorseOutRoomPer(String.format(getString(R.string.common_percent_formatted), per));
        getViewModel().setNewlyUlcersPer(String.format(getString(R.string.common_percent_formatted), per));
        getViewModel().setReduceUlcers(String.format(getString(R.string.common_percent_formatted), per));
        getViewModel().setWorseUlcers(String.format(getString(R.string.common_percent_formatted), per));

        if(item != null){
            Float fPer = null;

            // 일상생활 감퇴
            fPer = CommonUtils.plusFloat(item.getDementiaPatient(), item.getDementiaPatientExcluded());
            if(fPer != null) {
                per = String.valueOf(fPer);
            }
            getViewModel().setWorseActivityPer(String.format(getString(R.string.common_percent_formatted), per));

            // 방밖으로 나가기
            per = "-";
            fPer = CommonUtils.plusFloat(item.getLeavingRoomDementiaPatient(), item.getLeavingRoomDementiaPatientExcluded());
            if(fPer != null) {
                per = String.valueOf(fPer);
            }
            getViewModel().setWorseOutRoomPer(String.format(getString(R.string.common_percent_formatted), per));

            // 욕창 발생
            per = "-";
            fPer = CommonUtils.plusFloat(item.getNewlyDevelopedPressureUlcersHighRiskGroup(), item.getPressureUlcersLowRiskGroup());
            if(fPer != null) {
                per = String.valueOf(fPer);
            }
            getViewModel().setNewlyUlcersPer(String.format(getString(R.string.common_percent_formatted), per));

            // 욕창 감소
            per = "-";
            fPer = item.getImprovedPressureUlcersHighRiskGroup();
            if(fPer != null) {
                per = String.valueOf(fPer);
            }
            getViewModel().setReduceUlcers(String.format(getString(R.string.common_percent_formatted), per));

            // 욕창 악화
            per = "-";
            fPer = item.getUlcersWorsenedHighRiskGroup();
            if(fPer != null) {
                per = String.valueOf(fPer);
            }
            getViewModel().setWorseUlcers(String.format(getString(R.string.common_percent_formatted), per));
        }
    }


    private void initProgramInfo() {
        StringBuilder programText = new StringBuilder();
        if(getServiceDetailObject().getData().getPrograms() != null){
            List<ProgramsObject.Item> items = getServiceDetailObject().getData().getPrograms().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(programText.length() > 0){
                        programText.append(", ");
                    }
                    programText.append(items.get(i).getName());
                }
            }
        }

        getViewDataBinding().fHospitalDetailProgramLayout.setText(programText.toString());
    }

    private void initSubInfo() {
        StringBuilder subText = new StringBuilder();
        if(getServiceDetailObject().getData().getSubs() != null){
            List<SubsObject.Item> items = getServiceDetailObject().getData().getSubs().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(subText.length() > 0){
                        subText.append(", ");
                    }
                    subText.append(getCountStr(items.get(i).getName(), items.get(i).getTotalCount()));
                }
            }
        }

        getViewDataBinding().fHospitalDetailSubLayout.setText(subText.toString());
    }

    private void initEmployeesInfo() {
        StringBuilder employeesText = new StringBuilder();
        if(getServiceDetailObject().getData().getEmployees() != null){
            List<EmployeesObject.Item> items = getServiceDetailObject().getData().getEmployees().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(employeesText.length() > 0){
                        employeesText.append(", ");
                    }
                    employeesText.append(getCountStr(items.get(i).getName(), items.get(i).getTotalCount()));
                }
            }
        }

        getViewDataBinding().fHospitalDetailEmployeeLayout.setText(employeesText.toString());
    }

    private void initEquipmentInfo() {
        StringBuilder equipmentText = new StringBuilder();
        if(getServiceDetailObject().getData().getEquipment() != null){
            List<EquipmentObject.Item> items = getServiceDetailObject().getData().getEquipment().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(equipmentText.length() > 0){
                        equipmentText.append(", ");
                    }
                    equipmentText.append(getCountStr(items.get(i).getName(), items.get(i).getTotalCount()));
                }
            }
        }

        getViewDataBinding().fHospitalDetailEquipmentLayout.setText(equipmentText.toString());
    }

    private String getCountStr(String name, int count) {
        String appendText = "";
        if (count > 0) {
            appendText = String.format(Locale.getDefault(), "%s(%d) ", name, count);
        }

        return appendText;
    }

    private int getDiffTextColor(float diffValue) {
        int color = getResources().getColor(R.color.common_normal_number);
        if (diffValue > 0) {
            color = getResources().getColor(R.color.common_negative_number);
        } else if (diffValue < 0) {
            color = getResources().getColor(R.color.common_positive_number);
        }

        return color;
    }

    @Override
    public void onLoadReviewServiceSummary(ReviewFacilityServiceSummaryResponse response) {
        if (response.getRatings() == null || getServiceDetailObject() == null) {
            return;
        }

        List<RatingsSummaryObject.Item> ratings = response.getRatings();
        if (ratings.size() == 0) {
            for (RatingRelationsObject.Item item : getServiceDetailObject().getRatingRelations().getItems()) {
                RatingsSummaryObject.Item ratingItem = new RatingsSummaryObject.Item();
                ratingItem.setLabel(item.getLabel());
                ratingItem.setValue(0);
                ratings.add(ratingItem);
            }
        }

        RatingBarAdapter adapter = (RatingBarAdapter)getViewDataBinding().fHospitalDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.getAdapter();
        if(adapter != null){
            adapter.setRatingList(ratings);
        }
        else{
            initRatingBar(ratings);
        }

        setRatingLayout(response.getRating(), response.getTotal());
    }

    @Override
    public void onLoadCommentFacility(FacilityCommentResponse response) {
        getFacilityDetailObject().setTotalRating(response.getRatingAverage());
        getFacilityDetailObject().setTotalReviewCount(response.getTotal());
        getViewDataBinding().setFacilityDetail(getFacilityDetailObject());
    }

    @Override
    public void onShowPhoneDial() {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_DETAIL_CALL);
        getFacilityDetailNavigator().onShowPhoneDial();
    }

    @Override
    public void onWrite() {
        getFacilityDetailNavigator().onWriteComment();
    }

    @Override
    public void onGoHomepage() {
        getFacilityDetailNavigator().onShowHomepage();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                getFacilityDetailNavigator().onShowGoogleMap();
            }
        });

        if(getFacilityDetailObject().getGeoLocation() != null){
            setMarker(getFacilityDetailObject().getGeoLocation());
        }
    }

    private void setMarker(ApiCommonObject.GeoLocation location){
        if(mMap != null && location!=null){
            View view = (LayoutInflater.from(getBaseActivity()).inflate(R.layout.v_facility_detail_location_marker, null));
            BitmapDescriptor descriptor = getMarkerBitmapDescriptor(view);
            LatLng position = new LatLng(location.getCoordinates().get(1),location.getCoordinates().get(0));

            MarkerOptions markerOptions = new MarkerOptions()
                    .anchor(0.5f, 0.5f)
                    .position(position)
                    .icon(descriptor);
            mMap.addMarker(markerOptions);

            moveCameraWithLoaction(position);
        }
    }

    private BitmapDescriptor getMarkerBitmapDescriptor(View view) {
        BitmapDescriptor descriptor = null;
        getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        Bitmap bitmap = GraphicsUtils.createBitmapFromView(view, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
        if (bitmap != null) {
            descriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return descriptor;
    }

    private void moveCameraWithLoaction(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(14f)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void removeComment(long commentId) {
        showCommonDialog(
                getString(R.string.warning_msg_remove_comment_title),
                getString(R.string.warning_msg_remove_comment_desc),
                CommonYNDialog.class.getName(),
                (view) -> getViewModel().removeComment(commentId, getFacilityDetailObject().getFakeId()),
                getString(R.string.common_delete),
                getString(R.string.common_cancel)
        );
    }
}
