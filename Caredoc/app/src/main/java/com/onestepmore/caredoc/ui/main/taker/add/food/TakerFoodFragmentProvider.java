package com.onestepmore.caredoc.ui.main.taker.add.food;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerFoodFragmentProvider {
    @ContributesAndroidInjector(modules = TakerFoodFragmentModule.class)
    abstract TakerFoodFragment provideTakerFoodFragmentFactory();
}
