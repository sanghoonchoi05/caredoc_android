package com.onestepmore.caredoc.ui.main.home;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.IFacilityBinding;

public class FacilityAdapter extends RecyclerView.Adapter<FacilityAdapter.FacilityViewHolder> {
    private String [] mFacilityList = null;

    @Nullable
    private final HomeNavigator mHomeNavigator;

    public FacilityAdapter(@Nullable HomeNavigator homeNavigator){
        mHomeNavigator = homeNavigator;
    }

    public void setFacilityList(final String [] facilityList){
        mFacilityList = facilityList;
        notifyItemRangeInserted(0, mFacilityList.length);
    }

    @NonNull
    @Override
    public FacilityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFacilityBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility, viewGroup, false);
        return new FacilityViewHolder(binding, mHomeNavigator);
    }

    @Override
    public void onBindViewHolder(@NonNull FacilityViewHolder facilityViewHolder, int position) {
        facilityViewHolder.binding.setName(mFacilityList[position]);
        facilityViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mFacilityList == null ? 0 : mFacilityList.length;
    }

    static class FacilityViewHolder extends RecyclerView.ViewHolder {
        final IFacilityBinding binding;

        private FacilityViewHolder(IFacilityBinding binding, HomeNavigator listener) {
            super(binding.getRoot());
            this.binding = binding;
            binding.setCallback(listener);
        }
    }
}
