package com.onestepmore.caredoc.ui.main.detail.review;

import android.app.Application;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.object.ReviewServiceObject;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewRemoveRequest;
import com.onestepmore.caredoc.data.model.realm.ReviewService;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Response;

public class ReviewViewModel extends BaseViewModel<ReviewNavigator> {
    private ReviewRepository mReviewRepository;

    public ReviewRepository getReviewRepository() {
        return mReviewRepository;
    }

    public ReviewViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                           ReviewRepository reviewRepository) {
        super(application, appDataSource, schedulerProvider);

        mReviewRepository = reviewRepository;
    }

    void loadReviewServiceFromServer(long id) {
        deleteReviewService();
        setIsLoading(true);

        ReviewFacilityRequest.PathParameter pathParameter = new ReviewFacilityRequest.PathParameter();
        pathParameter.setMorphId(id);
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility_service.getType());
        getCompositeDisposable().add(getReviewRepository()
                .getReviewServiceApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {

                    setIsLoading(false);
                    saveReviewService(response.getItems());
                    getNavigator().onLoadReviewService(response);

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void deleteReviewService(){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<ReviewService> reviews = realm.where(ReviewService.class).findAll();
                reviews.deleteAllFromRealm();
            }
        });
    }

    private void saveReviewService(List<ReviewServiceObject> serviceObjects){
        if(serviceObjects != null && serviceObjects.size() > 0){
            List<ReviewService> reviewList = new ArrayList<>();
            for(ReviewServiceObject object : serviceObjects){
                ReviewService review = new ReviewService();
                review.setReview(object);
                reviewList.add(review);
            }

            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for(ReviewService review : reviewList){
                        realm.copyToRealmOrUpdate(review);
                    }
                }
            });
        }
    }

    void removeReview(long id){
        ReviewRemoveRequest.PathParameter pathParameter = new ReviewRemoveRequest.PathParameter();
        pathParameter.setId(id);

        getReviewRepository().removeReviewApiCall(pathParameter, new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    getNavigator().onCompleteRemoveReview();
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }
}
