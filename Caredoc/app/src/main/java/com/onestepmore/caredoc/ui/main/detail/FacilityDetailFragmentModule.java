package com.onestepmore.caredoc.ui.main.detail;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.BookmarkRepository;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class FacilityDetailFragmentModule {
    @Provides
    FacilityDetailViewModel facilityDetailViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                                    Gson gson, FacilityRepository facilityRepository,
                                                    BookmarkRepository bookmarkRepository, ReviewRepository reviewRepository) {
        return new FacilityDetailViewModel(application, dataSource, schedulerProvider, gson, facilityRepository, bookmarkRepository, reviewRepository);
    }

    @Provides
    @Named("FacilityDetailFragment")
    ViewModelProvider.Factory provideFacilityDetailViewModelProvider(FacilityDetailViewModel facilityDetailViewModel) {
        return new ViewModelProviderFactory<>(facilityDetailViewModel);
    }
}
