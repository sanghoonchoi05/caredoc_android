package com.onestepmore.caredoc.ui.main.taker.card;

import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiCommonNavigator;

public interface TakerCardNavigator extends CareCoordiCommonNavigator {
    void showAddTakerView();
    void onTakerItemClick(Taker taker);
    void onSelectTaker(Taker taker);
}
