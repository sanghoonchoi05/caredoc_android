package com.onestepmore.caredoc.ui.main.taker.detail;

import android.app.Application;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.onestepmore.caredoc.data.model.api.object.TakerDetailObject;
import com.onestepmore.caredoc.data.model.api.object.TakerObject;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoResponse;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerResponse;
import com.onestepmore.caredoc.data.model.api.taker.DeleteTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailResponse;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.TakerRepository;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.realm.Realm;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class TakerDetailViewModel extends BaseViewModel<TakerDetailNavigator> {

    private final TakerRepository mTakerRepository;
    private final Gson mGson;

    public TakerDetailViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                                Gson gson, TakerRepository takerRepository) {
        super(application, appDataSource, schedulerProvider);

        mGson = gson;
        mTakerRepository = takerRepository;
    }

    public TakerRepository getTakerRepository() {
        return mTakerRepository;
    }

    public void modifyTakerName(AddTakerRequest request, long takerId) {
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(mGson.toJson(request, AddTakerRequest.class));
        }catch (JSONException e){

        }

        if(jsonObject == null){
            return;
        }

        getCompositeDisposable().add(getTakerRepository()
                .modifyTakerApiCall(jsonObject, new AddTakerRequest.PathParameter(takerId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(this::saveTaker, throwable ->
                        getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void getTakerDetail(long takerId){
        getCompositeDisposable().add(getTakerRepository()
                .getTakerDetailApiCall(new TakerDetailRequest.PathParameter(takerId))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(this::saveTaker, throwable ->
                        getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void deleteTaker(long takerId){
        getTakerRepository().deleteTakerApiCall(new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    getMyTakers();
                } else {
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        }, new DeleteTakerRequest.PathParameter(takerId));
    }

    public void getMyTakers(){
        getCompositeDisposable().add(getDataManager()
                .loadMyTakerFromServer(getRealm())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> getNavigator().onDelete(), throwable ->
                        getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void saveTaker(AddTakerResponse response) {
        saveTaker((TakerObject)response);
    }

    public void saveTaker(TakerDetailResponse response) {
        saveTaker((TakerDetailObject)response);
    }

    public void saveTaker(TakerObject object) {
        if(object == null){
            return;
        }

        getRealm().executeTransaction((realm -> {
            Taker taker = new Taker();
            taker.setTaker(object);
            realm.copyToRealmOrUpdate(taker);
        }));
    }

    public void uploadPhoto(long takerId, File imageFile) {
        getTakerRepository().addTakerPhotoApiCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    if(response.body() != null){
                        savePhotoUrl(mGson.fromJson(response.body().string(), AddTakerPhotoResponse.class), takerId);
                    }
                }
                else{
                    if (response.body() != null) {
                        onErrorPhotoUpload(response.body().string());
                    }
                }
            }
        }, takerId, imageFile);
    }

    private void onErrorPhotoUpload(String response){
        getCompositeDisposable().add(Flowable.fromCallable(() -> true)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> getDataManager().handleApiError(response, getNavigator())));
    }

    private void savePhotoUrl(AddTakerPhotoResponse response, long takerId) {
        if (response == null) {
            return;
        }

        getCompositeDisposable().add(Flowable.fromCallable(() -> true)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    getRealm().executeTransaction( (realm) -> {
                        Taker taker = getRealm().where(Taker.class).equalTo("fakeId", takerId).findFirst();
                        if(taker != null){
                            taker.setPhotoUrl(response.getFullThumbnailUrl());
                        }
                        realm.copyToRealmOrUpdate(taker);
                    });
                }));
    }

    public void onNameModify(){
        getNavigator().showNameDialog();
    }
    public void onPhotoClick() {
        getNavigator().showTakerPhoto();
    }

    public void onBasicModify() {
        getNavigator().showTakerModifyView(AddTakerActivity.STEP.BASIC);
    }

    public void onAddressModify() {
        getNavigator().showTakerModifyView(AddTakerActivity.STEP.ADDRESS);
    }

    public void onCareModify() {
        getNavigator().showTakerModifyView(AddTakerActivity.STEP.CARE);
    }

    public void onDiseaseModify() {
        getNavigator().showTakerModifyView(AddTakerActivity.STEP.DISEASE);
    }

    public void onPerformanceModify() {
        getNavigator().showTakerModifyView(AddTakerActivity.STEP.PERFORMANCE);
    }

    public void onFoodModify() {
        getNavigator().showTakerModifyView(AddTakerActivity.STEP.FOOD);
    }

    public void onRemarkModify() {
        getNavigator().showTakerModifyView(AddTakerActivity.STEP.REMARK);
    }
}
