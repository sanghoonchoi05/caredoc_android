package com.onestepmore.caredoc.ui.address.detail;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Juso;
import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.data.model.realm.RecentlyJuso;
import com.onestepmore.caredoc.databinding.FAddressDetailBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.address.AddressMainActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.GraphicsUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.Realm;

public class AddressDetailFragment extends BaseFragment<FAddressDetailBinding, AddressDetailViewModel> implements
        AddressDetailNavigator,
        OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener
{

    public static AddressDetailFragment newInstance(JusoRealmModel juso){
        Bundle bundle = new Bundle();
        bundle.putParcelable("juso", juso);

        AddressDetailFragment fragment = new AddressDetailFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Inject
    @Named("AddressDetailFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private AddressDetailViewModel mAddressDetailViewModel;
    private JusoRealmModel mJuso;

    private GoogleMap mMap = null;
    private Marker mMarker = null;
    private float mCameraZoomVal = 18f;
    private DisplayMetrics mDisplayMetrics;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_address_detail;
    }

    @Override
    public AddressDetailViewModel getViewModel() {
        mAddressDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AddressDetailViewModel.class);
        return mAddressDetailViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressDetailViewModel.setNavigator(this);

        mDisplayMetrics = new DisplayMetrics();
        getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fAddressDetailToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }

        handleArguments();
        initMap();
        initListener();
    }

    private void initListener(){
        getViewDataBinding().fAddressDetailEdit.setOnEditorActionListener((textView, actionId, event) ->{
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onSelectJuso(mJuso);
                return true;
            }
            return false;
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BackStackManager.getInstance().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleArguments(){
        if(getArguments() != null){
            mJuso = getArguments().getParcelable("juso");
            if(mJuso == null){
                return;
            }
            getViewDataBinding().setJuso(mJuso);
        }
    }

    private void initMap(){
        FragmentManager fragmentManager = getChildFragmentManager();
        SupportMapFragment supportMapFragment = (SupportMapFragment)fragmentManager.findFragmentById(R.id.f_address_detail_map_fragment);
        if(supportMapFragment != null){
            supportMapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onSelectJuso(JusoRealmModel juso){
        String detail = getViewDataBinding().fAddressDetailEdit.getEditableText().toString();
        if(CommonUtils.checkNullAndEmpty(detail)){
            showCommonDialog(getString(R.string.warning_msg_address_detail_title), getString(R.string.warning_msg_address_detail_empty_desc));
            return;
        }

        String id = juso.getId();
        getViewModel().getRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                JusoRealmModel detailJuso = realm.where(Juso.class).equalTo("id", id).findFirst();
                if(detailJuso == null){
                    detailJuso = realm.where(RecentlyJuso.class).equalTo("id", id).findFirst();
                }

                if(detailJuso != null){
                    detailJuso.setDetail(detail);
                    realm.copyToRealmOrUpdate(detailJuso);
                }
            }
        }, () -> {
            JusoRealmModel updatedJuso = getViewModel().getRealm().where(Juso.class).equalTo("id", id).findFirst();
            if(updatedJuso == null){
                updatedJuso = getViewModel().getRealm().where(RecentlyJuso.class).equalTo("id", id).findFirst();
            }

            if(updatedJuso != null){
                if (getBaseActivity() instanceof AddressMainActivity) {
                    ((AddressMainActivity) getBaseActivity()).onSelectJuso(updatedJuso);
                }
            }
        });
    }

    @Override
    public void onCameraIdle() {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);

        LatLng latLng = new LatLng(mJuso.getLat(), mJuso.getLng());

        View markerView = (LayoutInflater.from(getBaseActivity()).inflate(R.layout.v_address_detail_marker, null));
        mMarker = drawMarkerOnMap(latLng, getMarkerItemBitmapDescriptor(markerView));

        moveCameraWithLoaction(latLng);
    }

    private BitmapDescriptor getMarkerItemBitmapDescriptor(View view) {
        BitmapDescriptor descriptor = null;
        Bitmap bitmap = GraphicsUtils.createBitmapFromView(view, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
        if (bitmap != null) {
            descriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return descriptor;
    }

    private Marker drawMarkerOnMap(LatLng latLng, BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor == null) {
            return null;
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .anchor(0.5f, 0.5f)
                .position(latLng)
                .icon(bitmapDescriptor);
        return mMap.addMarker(markerOptions);
    }

    private void moveCameraWithLoaction(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(mCameraZoomVal)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(mMarker != null){
            mMarker.remove();
        }
    }
}
