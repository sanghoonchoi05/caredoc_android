package com.onestepmore.caredoc.ui.account;

import android.accounts.Account;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.AAccountMainBinding;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.account.AccountFragment;
import com.onestepmore.caredoc.ui.account.login.LoginFragment;
import com.onestepmore.caredoc.ui.account.login.forgot.LoginForgotFragment;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStack;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackHelperListener;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class AccountMainActivity extends BaseActivity<AAccountMainBinding, AccountMainViewModel> implements
        HasSupportFragmentInjector,
        AccountMainNavigator{

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    private AccountMainViewModel mAccountMainViewModel;

    private FragmentManager mFragmentManager;

    boolean fromLogout = false;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_account_main;
    }

    @Override
    public AccountMainViewModel getViewModel() {
        mAccountMainViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AccountMainViewModel.class);
        return mAccountMainViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAccountMainViewModel.setNavigator(this);

        mFragmentManager = getSupportFragmentManager();

        setSupportActionBar(getViewDataBinding().aAccountMainAppBar.vAppBarStartToolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back_title);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        BaseFragment baseFragment = new AccountFragment();
        Intent intent = getIntent();
        if(intent != null){
            fromLogout = intent.getBooleanExtra("logout" , false);
        }

        BackStackManager.getInstance().addRootFragment(
                this,
                baseFragment,
                R.id.v_app_bar_start_fragment_layout);
    }
    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void onFinish() {
        if(fromLogout){
            openMainActivity();
        }
        else{
            finish();
        }
    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BackStackManager.getInstance().onBackPressed();
                if(fromLogout){
                    openMainActivity();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openMainActivity() {
        BackStackManager.getInstance().removeAllFragment();
        Intent intent = newIntent(MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("index", 0);
        startActivity(intent);
        finish();
    }
}
