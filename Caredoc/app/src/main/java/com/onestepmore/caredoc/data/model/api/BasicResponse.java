package com.onestepmore.caredoc.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class BasicResponse {
    public final class Location{
        @Expose
        @SerializedName("lat")
        private double lat;

        @Expose
        @SerializedName("lng")
        private double lng;

        public double getLng() {
            return lng;
        }

        public double getLat() {
            return lat;
        }
    }
}
