package com.onestepmore.caredoc.ui.fragmentmanager;

import android.support.annotation.AnimRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.onestepmore.caredoc.ui.base.TransactionCallback;

import java.util.List;

import io.sentry.Sentry;
import io.sentry.event.Event;
import io.sentry.event.EventBuilder;

public class BackStackFragmentManager {

    //region Members
    /** Reference to fragment manager */
    private final FragmentManager mFragmentManager;
    /** Last added fragment */
    private Fragment mLastAdded;
    //endregion


    //region Constructors
    public BackStackFragmentManager(@NonNull final FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }
    //endregion


    //region Methods
    public Fragment getLastFragment(){
        return mLastAdded;
    }

    /** Switch root fragment */
    /*public void switchFragment(@NonNull final Fragment fragment, @AnimRes final int enterAni, @AnimRes final int exitAni) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.hide(mLastAdded);
        transaction.show(fragment);

        transaction.commit();

        //mLastAdded = fragment;
    }*/

    public void switchFragment(@NonNull final Fragment showFragment, @NonNull final Fragment hideFragment, @AnimRes final int enterAni, @AnimRes final int exitAni) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        if(enterAni != 0 && exitAni != 0){
            transaction.setCustomAnimations(enterAni, 0, enterAni, 0);
        }
        else{
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        }
        transaction.hide(hideFragment);
        transaction.show(showFragment);
        onHide(hideFragment);
        onShow(showFragment);

        transaction.commitAllowingStateLoss();

        //mLastAdded = showFragment;
    }

    private void onHide(Fragment fragment){
        if(fragment instanceof TransactionCallback){
            ((TransactionCallback)fragment).onHide();
        }
    }

    private void onShow(Fragment fragment){
        if(fragment instanceof TransactionCallback){
            ((TransactionCallback)fragment).onShow();
        }
    }

    private void onAdded(Fragment fragment){
        if(fragment instanceof TransactionCallback){
            ((TransactionCallback)fragment).onAdded();
        }
    }

    public void showLastFragment(@AnimRes final int enterAni, @AnimRes final int exitAni) {
        showFragment(mLastAdded, enterAni, exitAni);
    }

    public void showFragment(Fragment fragment, @AnimRes final int enterAni, @AnimRes final int exitAni) {
        if(fragment != null){
            final FragmentTransaction transaction = mFragmentManager.beginTransaction();
            if(enterAni != 0 && exitAni != 0){
                transaction.setCustomAnimations(enterAni, 0, enterAni, 0);
            }
            else{
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            }
            transaction.show(fragment);
            onShow(fragment);
            transaction.commitAllowingStateLoss();
            // TODO commitAllowingStateLoss
        }
    }

    /** Adding child fragment to a root */
    public void addChildFragment(@NonNull final Fragment fragment,
                                 final int layoutId,
                                 @NonNull final String tag, final int enterAni,final int exitAni) {

        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        /*transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out,
                R.anim.fade_in, R.anim.fade_out);*/
        if(enterAni != 0 && exitAni !=0){
            transaction.setCustomAnimations(enterAni, 0, enterAni, 0);
            transaction.add(layoutId, fragment, tag);
            transaction.setCustomAnimations(0, exitAni, 0, exitAni);
            transaction.hide(mLastAdded);
            onHide(mLastAdded);
        }
        else{
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(layoutId, fragment, tag);
            transaction.hide(mLastAdded);
            onHide(mLastAdded);
        }
        transaction.commit();

        mLastAdded = fragment;
    }

    /** Replace a menu fragment */
    public void replaceMenuFragment(@NonNull Fragment fragment, int layoutId) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        List<Fragment> fragmentList = mFragmentManager.getFragments();
        for(Fragment fr : fragmentList){
            transaction.remove(fr);
        }

        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        transaction.replace(layoutId, fragment, fragment.getClass().getName());
        transaction.commit();
    }

    public void removeAllFragment(){
        removeAllFragment(false);
    }

    public void removeAllFragment(boolean commitAllowingStateLoss){
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        List<Fragment> fragmentList = mFragmentManager.getFragments();
        if(fragmentList.size() > 0){
            for(Fragment fr : fragmentList){
                transaction.remove(fr);
            }
            if(commitAllowingStateLoss){
                transaction.commitAllowingStateLoss();
            }else{
                transaction.commit();
            }
        }
    }

    /** Add a root fragment */
    public void addFragment(@NonNull Fragment fragment, int layoutId, @AnimRes final int enterAni, @AnimRes final int exitAni) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();

        if(enterAni != 0 && exitAni != 0){
            transaction.setCustomAnimations(enterAni, 0, enterAni, 0);
        }
        else{
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        }

        //since we hide/show. This should only happen initially
        boolean isAdded = fragment.isAdded();
        if (!isAdded) {
            transaction.add(layoutId, fragment, fragment.getClass().getName());
        }
        else {
            transaction.show(fragment);
        }

        /*if (mLastAdded != null) {
            transaction.setCustomAnimations(0, exitAni, 0, exitAni);
            transaction.hide(mLastAdded);
            onHide(mLastAdded);
        }*/

        transaction.commit();

        mLastAdded = fragment;

        if (!isAdded) {
            onAdded(fragment);
        }
        else {
            onShow(fragment);
        }
    }

    public void removeRoot(@NonNull BackStack backStack, @AnimRes final int exitAni) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();
        Fragment fragment = backStack.mRootFragment;
        if(fragment != null){
            transaction.setCustomAnimations(0, exitAni, 0, exitAni);
            transaction.remove(fragment);
            transaction.commit();
        }
    }

    /** Pop back stack
     * Function is removing childs that is not used!
     */
    public void removeChild(@NonNull final BackStack backStack, @NonNull final String tag, @AnimRes final int rootEnterAni, @AnimRes final int enterAni, @AnimRes final int exitAni) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();

        // 현재 자식프래그먼트를 삭제
        final Fragment fragment = mFragmentManager.findFragmentByTag(tag);
        if(fragment != null){
            transaction.setCustomAnimations(0, exitAni, 0, exitAni);
            transaction.remove(fragment);
        }

        // 그 다음 자식프래그먼트를 찾음
        String curTag = backStack.getCurrentStackId();
        Fragment curFragment = null;
        if(curTag != null){
            curFragment = mFragmentManager.findFragmentByTag(curTag);
            transaction.setCustomAnimations(enterAni, 0, enterAni, 0);
        }
        else{
            // 자식프래그먼트가 없다면 부모프래그먼트를 보여줌
            curFragment = backStack.mRootFragment;
            transaction.setCustomAnimations(rootEnterAni, 0, rootEnterAni, 0);
        }
        if(curFragment != null){
            transaction.show(curFragment);
            onShow(curFragment);
        }
        transaction.commit();

        mLastAdded = curFragment;
    }

    public void removeAllChild(@NonNull final BackStack backStack) {
        final FragmentTransaction transaction = mFragmentManager.beginTransaction();

        for(String uuid : backStack.mStackItems){
            final Fragment fragment = mFragmentManager.findFragmentByTag(uuid);
            if(fragment != null){
                transaction.remove(fragment);
            }
        }
        /*Fragment curFragment = backStack.mRootFragment;
        transaction.show(curFragment);
        onShow(curFragment);*/

        transaction.commit();
        mLastAdded = backStack.mRootFragment;
    }

    public Fragment findFragmentByTag(String tag){
        return mFragmentManager.findFragmentByTag(tag);
    }

    public void hideLastFragment(){
        hideFragment(mLastAdded);
    }

    public void hideFragment(Fragment fragment){
        if (fragment != null){
            final FragmentTransaction transaction = mFragmentManager.beginTransaction();
            transaction.hide(fragment);
            onHide(fragment);
            try{
                transaction.commitAllowingStateLoss();
            }catch (IllegalStateException e){
                captureException(e, fragment);
            }
        }

    }

    private void captureException(IllegalStateException e, Fragment fragment){
        EventBuilder builder = new EventBuilder()
                .withExtra("Type", "Developer Capture")
                .withExtra("Exception", e.fillInStackTrace())
                .withExtra("Id", fragment.getId())
                .withExtra("Fragment Tag", fragment.getTag());

        if(fragment.getActivity()!=null){
            builder = builder
                    .withExtra("FragmentActivity Name", fragment.getActivity().getPackageName());
        }
        Sentry.capture(builder.build());
        Sentry.capture(e);
    }


    //endregion
}
