package com.onestepmore.caredoc.data.model.api.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.external.NaverUserInfoObject;

public class ExternalNaverUserInfoResponse {
    @Expose
    @SerializedName("resultcode")
    private String resultsCode;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("response")
    private NaverUserInfoObject response;

    public String getResultsCode() {
        return resultsCode;
    }

    public String getMessage() {
        return message;
    }

    public NaverUserInfoObject getResponse() {
        return response;
    }
}
