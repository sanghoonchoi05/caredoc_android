package com.onestepmore.caredoc.ui.main.detail;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.MenuItem;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.databinding.AFacilityBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class FacilityActivity extends BaseActivity<AFacilityBinding, FacilityViewModel> implements
        FacilityNavigator,
        HasSupportFragmentInjector
{
    public static final String EXTRA_FACILITY= "com.onestepmore.caredoc.ui.main.detail.FACILITY";
    public static final String EXTRA_SERVICE_ID= "com.onestepmore.caredoc.ui.main.detail.SERVICE_ID";
    private static final int RESULT_CODE_FACILITY_ACTIVITY_JUST_FINISH = 99;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private FacilityViewModel mFacilityViewModel;

    private FragmentManager mFragmentManager;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_facility;
    }

    @Override
    public FacilityViewModel getViewModel() {
        mFacilityViewModel = ViewModelProviders.of(this, mViewModelFactory).get(FacilityViewModel.class);
        return mFacilityViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFacilityViewModel.setNavigator(this);
        mFragmentManager = getSupportFragmentManager();
        BackStackManager.getInstance().setFragmentFadeAnimation();

        Intent intent = getIntent();
        if(intent != null){
            FacilityRealmModel facility = intent.getParcelableExtra(EXTRA_FACILITY);
            int serviceId = intent.getIntExtra(EXTRA_SERVICE_ID, 0);
            if(facility!=null){
                FacilityDetailFragment fragment = FacilityDetailFragment.newInstance(facility, serviceId);
                BackStackManager.getInstance().addRootFragment(this, fragment, R.id.a_facility_content_fragment_layout);
            }
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onFinish() {
        finish();
        setResult(RESULT_CODE_FACILITY_ACTIVITY_JUST_FINISH);
        //BackStackManager.getInstance().removeAllFragment();
    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //BackStackManager.getInstance().onBackPressed();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
