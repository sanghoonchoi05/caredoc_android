package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchFilter {
    public static int DEFAULT_RATING = 0;
    @Expose
    @SerializedName("addressCode")
    private String addressCode;

    @Expose
    @SerializedName("services")
    private List<Service> services;

    @Expose
    @SerializedName("rating")
    private int rating;

    public String getAddressCode() {
        return addressCode;
    }

    public void setAddressCode(String addressCode) {
        this.addressCode = addressCode;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public static class Service{
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("grade")
        private List<String> grades;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<String> getGrades() {
            return grades;
        }

        public void setGrades(List<String> grades) {
            this.grades = grades;
        }
    }

    public static class SearchFilterBuilder extends SearchFilter {

        public SearchFilterBuilder addServices(List<Service> services){
            setServices(services);
            return this;
        }

        public SearchFilterBuilder addAddressCode(String addressCode){
            setAddressCode(addressCode);
            return this;
        }

        public SearchFilterBuilder addRating(int rating){
            setRating(rating);
            return this;
        }


        public SearchFilter build(){
            SearchFilter filter = new SearchFilter();
            filter.setAddressCode(getAddressCode());
            filter.setServices(getServices());
            filter.setRating(getRating());
            return filter;
        }
    }
}
