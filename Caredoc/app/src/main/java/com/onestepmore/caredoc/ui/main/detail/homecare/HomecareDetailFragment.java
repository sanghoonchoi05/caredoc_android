package com.onestepmore.caredoc.ui.main.detail.homecare;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.api.object.ApiCommonObject;
import com.onestepmore.caredoc.data.model.api.object.EmployeesObject;
import com.onestepmore.caredoc.data.model.api.object.EquipmentObject;
import com.onestepmore.caredoc.data.model.api.object.LtiFeeSheetsObject;
import com.onestepmore.caredoc.data.model.api.object.ProgramsObject;
import com.onestepmore.caredoc.data.model.api.object.RatingRelationsObject;
import com.onestepmore.caredoc.data.model.api.object.RatingsSummaryObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceEntranceObject;
import com.onestepmore.caredoc.data.model.api.object.SubsObject;
import com.onestepmore.caredoc.data.model.api.review.FacilityCommentResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceSummaryResponse;
import com.onestepmore.caredoc.data.model.realm.Review;
import com.onestepmore.caredoc.data.model.realm.statics.StaticLtiFeeSheetType;
import com.onestepmore.caredoc.databinding.FHomecareDetailBinding;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.detail.FacilityEvaluationListAdapter;
import com.onestepmore.caredoc.ui.main.detail.FacilityLtiFeeListAdapter;
import com.onestepmore.caredoc.ui.main.detail.FacilityUnpaidListAdapter;
import com.onestepmore.caredoc.ui.main.detail.LtiFeeSpinnerAdapter;
import com.onestepmore.caredoc.ui.main.detail.RatingBarAdapter;
import com.onestepmore.caredoc.ui.main.detail.base.BaseFacilityDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.comment.CommentAdapter;
import com.onestepmore.caredoc.ui.main.detail.comment.RemoveCommentListener;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.GraphicsUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;
import io.realm.Sort;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_DETAIL_CALL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_SELECT_FEE_SHEET;

public class HomecareDetailFragment extends BaseFacilityDetailFragment<FHomecareDetailBinding, HomecareDetailViewModel, ServiceEntranceObject> implements
        HomecareDetailNavigator,
        OnMapReadyCallback,
        RemoveCommentListener
{
    @Inject
    @Named("HomecareDetailFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private HomecareDetailViewModel mHomecareDetailViewModel;

    private int mFeeType = 0;
    private String mGradeText = "";

    private DisplayMetrics mDisplayMetrics;
    private GoogleMap mMap;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_homecare_detail;
    }

    @Override
    public HomecareDetailViewModel getViewModel() {
        mHomecareDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(HomecareDetailViewModel.class);
        return mHomecareDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHomecareDetailViewModel.setNavigator(this);

        mDisplayMetrics = new DisplayMetrics();
    }

    @Override
    public void refreshComment() {
        // Firebase Crashlytics - HomecareDetailFragment.java line 93
        if(getBaseActivity() == null){
            return;
        }
        getViewModel().loadCommentFacilityFromServer(getFacilityDetailObject().getFakeId());
    }

    @Override
    public void refreshReview() {
        if(getBaseActivity() == null){
            return;
        }
        getViewModel().loadReviewServiceSummaryFromServer(getServiceDetailObject().getFakeId());
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Crashlytics
        ServiceEntranceObject serviceEntranceObject = getServiceDetailObject();
        if(serviceEntranceObject == null){
            showCommonDialog(
                    getString(R.string.warning_msg_facility_detail_load_failed),
                    getString(R.string.warning_msg_facility_detail_load_failed_desc),
                    CommonYNDialog.class.getName(),
                    (clickView) ->
                    {
                        if(getFacilityDetailNavigator() != null){
                            getFacilityDetailNavigator().onClose();
                        }
                        else{
                            BackStackManager.getInstance().onBackPressed();
                        }},
                    getString(R.string.common_confirm),
                    "",
                    true);
            return;
        }

        if(getServiceDetailObject().isClosed()){
            getViewDataBinding().fHomecareDetailClosedText.setText(getString(R.string.f_facility_detail_service_closed, getServiceDetailObject().getServiceName()));
            getViewDataBinding().setIsClosed(getServiceDetailObject().isClosed());
            return;
        }

        getViewModel().loadReviewServiceSummaryFromServer(getServiceDetailObject().getFakeId());
        getViewModel().loadCommentFacilityFromServer(getFacilityDetailObject().getFakeId());

        setData();
    }

    private void setData() {
        if (getViewDataBinding() != null) {

            getViewDataBinding().setService(getServiceDetailObject());
            getViewDataBinding().setFacilityDetail(getFacilityDetailObject());

            initEvaluationList();
            initUnpaidList();
            initLtiFeeList();
            initLtiFeeSpinner();
            initProgramInfo();
            initSubInfo();
            initEmployeesInfo();
            initEquipmentInfo();
            initMap();
            initCommentList();
        }
    }

    private void initEvaluationList() {
        FacilityEvaluationListAdapter gradeChartAdapter = new FacilityEvaluationListAdapter(
                getBaseActivity(),
                getServiceDetailObject().getEvaluations().getItems());
        getViewDataBinding().fHomecareDetailEvaluationLayout.fFacilityDetailEvaluationRecyclerView.setAdapter(gradeChartAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fHomecareDetailEvaluationLayout.fFacilityDetailEvaluationRecyclerView.setLayoutManager(layoutManager);

        int horizontalSpace = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_detail_evaluation_item_space);
        int firstLastSpace = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_facility_detail_evaluation_item_first_last_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(0, horizontalSpace, true);
        decoration.setSpaceFirstAndLastHorizontal(firstLastSpace);
        getViewDataBinding().fHomecareDetailEvaluationLayout.fFacilityDetailEvaluationRecyclerView.addItemDecoration(decoration);
    }

    private void initUnpaidList(){
        if(getServiceDetailObject().getData().getUnpaidItems() == null ||
                getServiceDetailObject().getData().getUnpaidItems().getItems().size() == 0){
            getViewDataBinding().fHomecareDetailUnpaidLayout.vFacilityDetailUnpaidRoot.setVisibility(View.GONE);
        }
        else{
            FacilityUnpaidListAdapter adapter = new FacilityUnpaidListAdapter(getServiceDetailObject().getData().getUnpaidItems().getItems());
            getViewDataBinding().fHomecareDetailUnpaidLayout.fFacilityDetailUnpaidRecyclerView.setAdapter(adapter);
            getViewDataBinding().fHomecareDetailUnpaidLayout.fFacilityDetailUnpaidRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        }
    }

    private void initLtiFeeList(){
        if(getServiceDetailObject().getLtiFeeSheetsObject() == null ||
                getServiceDetailObject().getLtiFeeSheetsObject().getItems().size() == 0){
            getViewDataBinding().fHomecareDetailLtiFeeLayout.vFacilityDetailLtiFeeRoot.setVisibility(View.GONE);
        }
        else{
            LtiFeeSheetsObject.Item item = getServiceDetailObject().getLtiFeeSheetsObject().getItems().get(0);
            FacilityLtiFeeListAdapter adapter = new FacilityLtiFeeListAdapter(
                    item,
                    getServiceDetailObject().getData().getUnpaidItems().getItems(),
                    getResources().getStringArray(R.array.v_facility_detail_lti_fee_table_name));
            getViewDataBinding().fHomecareDetailLtiFeeLayout.fFacilityDetailLtiFeeRecyclerView.setAdapter(adapter);
            getViewDataBinding().fHomecareDetailLtiFeeLayout.fFacilityDetailLtiFeeRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        }
    }

    private void initLtiFeeSpinner(){
        if(getServiceDetailObject().getLtiFeeSheetsObject() == null ||
                getServiceDetailObject().getLtiFeeSheetsObject().getItems().size() == 0){
            return;
        }

        List<StaticLtiFeeSheetType> types = getViewModel().getRealm().where(StaticLtiFeeSheetType.class)
                .equalTo("serviceId", getServiceDetailObject().getServiceId())
                .findAll();

        List<String> descriptionList = new ArrayList<>();
        List<String> gradeList = new ArrayList<>();
        List<Integer> feeTypeList = new ArrayList<>();

        for(StaticLtiFeeSheetType type : types){
            descriptionList.add(type.getTypeName());
            feeTypeList.add(type.getTypeId());
        }

        for(LtiFeeSheetsObject.Item item : getServiceDetailObject().getLtiFeeSheetsObject().getItems()){

            if(!gradeList.contains(item.getGradeText())){
                gradeList.add(item.getGradeText());
            }
        }

        mFeeType = feeTypeList.get(0);
        mGradeText = gradeList.get(0);

        LtiFeeSpinnerAdapter feeTypeAdapter = new LtiFeeSpinnerAdapter(getBaseActivity(), descriptionList);
        getViewDataBinding().fHomecareDetailLtiFeeLayout.vFacilityDetailLtiFeeTypeSpinner.setAdapter(feeTypeAdapter);
        getViewDataBinding().fHomecareDetailLtiFeeLayout.vFacilityDetailLtiFeeTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFeeType = feeTypeList.get(position);
                changeLtiFeeSheets();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        LtiFeeSpinnerAdapter gradeAdapter = new LtiFeeSpinnerAdapter(getBaseActivity(), gradeList);
        getViewDataBinding().fHomecareDetailLtiFeeLayout.vFacilityDetailLtiFeeGradeSpinner.setAdapter(gradeAdapter);
        getViewDataBinding().fHomecareDetailLtiFeeLayout.vFacilityDetailLtiFeeGradeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mGradeText = gradeList.get(position);
                changeLtiFeeSheets();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void changeLtiFeeSheets(){
        for(LtiFeeSheetsObject.Item item : getServiceDetailObject().getLtiFeeSheetsObject().getItems()){
            if(item.getFeeType() == mFeeType && item.getGradeText().equals(mGradeText)){
                FacilityLtiFeeListAdapter adapter = (FacilityLtiFeeListAdapter)getViewDataBinding().fHomecareDetailLtiFeeLayout.fFacilityDetailLtiFeeRecyclerView.getAdapter();
                if(adapter != null){
                    Map<String, MapValue<?>> map = new HashMap<>();
                    if(item.getDescription() != null){
                        map.put(AmplitudeParam.Key.TYPE, new MapValue<>(item.getDescription()));
                    }
                    map.put(AmplitudeParam.Key.GRADE, new MapValue<>(item.getGradeText()));
                    AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_SELECT_FEE_SHEET, map);

                    adapter.changeSheets(item);
                }
                break;
            }
        }
    }

    private void initProgramInfo() {
        StringBuilder programText = new StringBuilder();
        if(getServiceDetailObject().getData().getPrograms() != null){
            List<ProgramsObject.Item> items = getServiceDetailObject().getData().getPrograms().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(programText.length() > 0){
                        programText.append(", ");
                    }
                    programText.append(items.get(i).getName());
                }
                /*View view = getView();
                ViewGroup viewGroup = null;
                if (view != null) {
                    viewGroup = ((ViewGroup) getView().getParent());
                }
                if (viewGroup != null) {
                    for (ProgramsObject.Item item : items) {
                        getViewDataBinding().fHomecareDetailProgramLayout.setNoData(false);
                        VFacilityDetailProgramBinding binding = DataBindingUtil.inflate
                                (LayoutInflater.from(getBaseActivity()), R.layout.v_facility_detail_program, viewGroup, false);
                        binding.setTitle(item.getName());
                        binding.setName(item.getName());
                        getViewDataBinding().fHomecareDetailProgramLayout.vFacilityDetailTableLayout.addView(binding.getRoot());
                    }
                }*/
            }
        }

        getViewDataBinding().fHomecareDetailProgramLayout.setText(programText.toString());
    }

    private void initSubInfo() {
        StringBuilder subText = new StringBuilder();
        if(getServiceDetailObject().getData().getSubs() != null){
            List<SubsObject.Item> items = getServiceDetailObject().getData().getSubs().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(subText.length() > 0){
                        subText.append(", ");
                    }
                    subText.append(getCountStr(items.get(i).getName(), items.get(i).getTotalCount()));
                }
            }
        }

        getViewDataBinding().fHomecareDetailSubLayout.setText(subText.toString());
    }

    private void initEmployeesInfo() {
        StringBuilder employeesText = new StringBuilder();
        if(getServiceDetailObject().getData().getEmployees() != null){
            List<EmployeesObject.Item> items = getServiceDetailObject().getData().getEmployees().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(employeesText.length() > 0){
                        employeesText.append(", ");
                    }
                    employeesText.append(getCountStr(items.get(i).getName(), items.get(i).getTotalCount()));
                }
            }
        }

        getViewDataBinding().fHomecareDetailEmployeeLayout.setText(employeesText.toString());
    }

    private void initEquipmentInfo() {
        StringBuilder equipmentText = new StringBuilder();
        if(getServiceDetailObject().getData().getEquipment() != null){
            List<EquipmentObject.Item> items = getServiceDetailObject().getData().getEquipment().getItems();
            if (items != null) {
                for(int i=0; i<items.size(); i++){
                    if(equipmentText.length() > 0){
                        equipmentText.append(", ");
                    }
                    equipmentText.append(getCountStr(items.get(i).getName(), items.get(i).getTotalCount()));
                }
            }
        }

        getViewDataBinding().fHomecareDetailEquipmentLayout.setText(equipmentText.toString());
    }

    private void initMap(){
        FragmentManager fragmentManager = getChildFragmentManager();
        SupportMapFragment supportMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.v_facility_detail_map);
        if (supportMapFragment != null) {
            supportMapFragment.getMapAsync(this);
        }
    }

    private void initCommentList(){
        RealmResults<Review> reviewRealmResults = getViewModel().getRealm().where(Review.class)
                .sort("isOwn", Sort.DESCENDING).findAll();

        CommentAdapter adapter = new CommentAdapter(reviewRealmResults, this);
        getViewDataBinding().fHomecareDetailCommentLayout.vFacilityDetailCommentRecyclerView.setAdapter(adapter);
        getViewDataBinding().fHomecareDetailCommentLayout.vFacilityDetailCommentRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_comment_item_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(spacingInPixels, 0, true);
        getViewDataBinding().fHomecareDetailCommentLayout.vFacilityDetailCommentRecyclerView.addItemDecoration(decoration);
    }

    private void initRatingBar(@NonNull List<RatingsSummaryObject.Item> ratings) {
        RatingBarAdapter adapter = new RatingBarAdapter(ratings);
        getViewDataBinding().fHomecareDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.setAdapter(adapter);
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        getViewDataBinding().fHomecareDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.setLayoutManager(manager);

        int verticalSpacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.i_rating_bar_item_space);
        getViewDataBinding().fHomecareDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.addItemDecoration(new SpacesItemDecoration(verticalSpacingInPixels, 0));

        getViewDataBinding().fHomecareDetailRatingLayout.vFacilityDetailRatingBarLeftButton.setOnClickListener(
                (view) -> {
                    if(ratings.size() > 0){
                        getFacilityDetailNavigator().onShowReview(getServiceDetailObject().getFakeId());
                    }
                });
        getViewDataBinding().fHomecareDetailRatingLayout.vFacilityDetailRatingBarRightButton.setOnClickListener(
                (view ) -> {
                    if(ratings.size() > 0){
                        getFacilityDetailNavigator().onWriteReview(getServiceDetailObject().getFakeId(), getServiceDetailObject().getServiceId());
                    }
                });
    }

    private void setRatingLayout(float ratingAvg, int total){
        getViewDataBinding().fHomecareDetailRatingLayout.setRatingAvg(ratingAvg);
        getViewDataBinding().fHomecareDetailRatingLayout.setCount(total);
    }

    private String getCountStr(String name, int count) {
        String appendText = "";
        if (count > 0) {
            appendText = String.format(Locale.getDefault(), "%s(%d) ", name, count);
        }

        return appendText;
    }

    @Override
    public void onLoadReviewServiceSummary(ReviewFacilityServiceSummaryResponse response) {
        if (response.getRatings() == null || getServiceDetailObject() == null) {
            return;
        }

        List<RatingsSummaryObject.Item> ratings = response.getRatings();
        if (ratings.size() == 0) {
            for (RatingRelationsObject.Item item : getServiceDetailObject().getRatingRelations().getItems()) {
                RatingsSummaryObject.Item ratingItem = new RatingsSummaryObject.Item();
                ratingItem.setLabel(item.getLabel());
                ratingItem.setValue(0);
                ratings.add(ratingItem);
            }
        }

        RatingBarAdapter adapter = (RatingBarAdapter)getViewDataBinding().fHomecareDetailRatingLayout.vFacilityDetailRatingBarRecyclerView.getAdapter();
        if(adapter != null){
            adapter.setRatingList(ratings);
        }
        else{
            initRatingBar(ratings);
        }

        setRatingLayout(response.getRating(), response.getTotal());
    }

    @Override
    public void onLoadCommentFacility(FacilityCommentResponse response) {
        getFacilityDetailObject().setTotalRating(response.getRatingAverage());
        getFacilityDetailObject().setTotalReviewCount(response.getTotal());
        getViewDataBinding().setFacilityDetail(getFacilityDetailObject());
    }

    @Override
    public void onShowPhoneDial() {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_DETAIL_CALL);
        getFacilityDetailNavigator().onShowPhoneDial();
    }

    @Override
    public void onWrite() {
        getFacilityDetailNavigator().onWriteComment();
    }

    @Override
    public void onGoHomepage() {
        getFacilityDetailNavigator().onShowHomepage();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                getFacilityDetailNavigator().onShowGoogleMap();
            }
        });

        if(getFacilityDetailObject().getGeoLocation() != null){
            setMarker(getFacilityDetailObject().getGeoLocation());
        }
    }

    private void setMarker(ApiCommonObject.GeoLocation location){
        if(mMap != null && location!=null){
            View view = (LayoutInflater.from(getBaseActivity()).inflate(R.layout.v_facility_detail_location_marker, null));
            BitmapDescriptor descriptor = getMarkerBitmapDescriptor(view);
            LatLng position = new LatLng(location.getCoordinates().get(1),location.getCoordinates().get(0));

            MarkerOptions markerOptions = new MarkerOptions()
                    .anchor(0.5f, 0.5f)
                    .position(position)
                    .icon(descriptor);
            mMap.addMarker(markerOptions);

            moveCameraWithLoaction(position);
        }
    }

    private BitmapDescriptor getMarkerBitmapDescriptor(View view) {
        BitmapDescriptor descriptor = null;
        getBaseActivity().getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        Bitmap bitmap = GraphicsUtils.createBitmapFromView(view, mDisplayMetrics.widthPixels, mDisplayMetrics.heightPixels);
        if (bitmap != null) {
            descriptor = BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        return descriptor;
    }

    private void moveCameraWithLoaction(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(14f)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void removeComment(long commentId) {
        showCommonDialog(
                getString(R.string.warning_msg_remove_comment_title),
                getString(R.string.warning_msg_remove_comment_desc),
                CommonYNDialog.class.getName(),
                (view) -> getViewModel().removeComment(commentId, getFacilityDetailObject().getFakeId()),
                getString(R.string.common_delete),
                getString(R.string.common_cancel)
        );
    }
}
