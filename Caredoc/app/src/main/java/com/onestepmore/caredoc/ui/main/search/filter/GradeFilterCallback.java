package com.onestepmore.caredoc.ui.main.search.filter;

public interface GradeFilterCallback {
    void onGradeFilterClick(int position, String text);
    void onGradeFilterClose();
    void onGradeFilterInit();
}
