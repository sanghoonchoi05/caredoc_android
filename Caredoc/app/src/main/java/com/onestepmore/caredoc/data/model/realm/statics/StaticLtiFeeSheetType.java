package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.LtiFeeSheetTypesObject;
import com.onestepmore.caredoc.data.model.api.object.LtiFeeSheetsObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StaticLtiFeeSheetType extends RealmObject implements Parcelable {
    @PrimaryKey
    private int key;     // id는 해쉬코드로 생성
    private int serviceId;
    private int typeId;
    private String typeName;

    public StaticLtiFeeSheetType(){}

    protected StaticLtiFeeSheetType(Parcel in) {
        key = in.readInt();
        serviceId = in.readInt();
        typeId = in.readInt();
        typeName = in.readString();
    }

    public void setLtiFeeSheetType(int serviceId, LtiFeeSheetTypesObject.Item item){
        this.serviceId = serviceId;
        this.typeId = item.getTypeId();
        this.typeName = item.getTypeName();

        this.key = hashCodeForPrimaryKey();
    }

    public static final Creator<StaticLtiFeeSheetType> CREATOR = new Creator<StaticLtiFeeSheetType>() {
        @Override
        public StaticLtiFeeSheetType createFromParcel(Parcel in) {
            return new StaticLtiFeeSheetType(in);
        }

        @Override
        public StaticLtiFeeSheetType[] newArray(int size) {
            return new StaticLtiFeeSheetType[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(key);
        dest.writeInt(serviceId);
        dest.writeInt(typeId);
        dest.writeString(typeName);
    }

    private int hashCodeForPrimaryKey() {
        int result = 17;
        result = 31 * result + serviceId;
        result = 31 * result + typeId;
        return result;
    }

    public String getTypeName() {
        return typeName;
    }

    public int getTypeId() {
        return typeId;
    }
}
