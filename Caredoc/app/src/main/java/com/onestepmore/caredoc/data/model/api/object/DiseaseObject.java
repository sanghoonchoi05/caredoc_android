package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Deprecated
public class DiseaseObject {
    @Expose
    @SerializedName("type_code")
    private String typeCode;
    @Expose
    @SerializedName("type_name")
    private String typeName;

    public String getTypeCode() {
        return typeCode;
    }

    public String getTypeName() {
        return typeName;
    }
}
