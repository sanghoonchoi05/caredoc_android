package com.onestepmore.caredoc.ui.main.taker.add.remark;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerRemarkFragmentProvider {
    @ContributesAndroidInjector(modules = TakerRemarkFragmentModule.class)
    abstract TakerRemarkFragment provideTakerRemarkFragmentFactory();
}
