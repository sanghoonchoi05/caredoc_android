package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class ServiceEntranceObject extends ServiceDetailObject{
    @Expose
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    public final class Data{
        @Expose
        @SerializedName("key")
        private String key;

        @Expose
        @SerializedName("total_men_count")
        private int totalMenCount;

        @Expose
        @SerializedName("current_male_count")
        private int currentMaleCount;

        @Expose
        @SerializedName("current_female_count")
        private int currentFemaleCount;

        @Expose
        @SerializedName("waiting_male_count")
        private int waitingMaleCount;

        @Expose
        @SerializedName("waiting_female_count")
        private int waitingFemaleCount;

        @Expose
        @SerializedName("unpaid_items")
        private UnpaidItemsObject unpaidItems;

        @Expose
        @SerializedName("programs")
        private ProgramsObject programs;

        @Expose
        @SerializedName("subs")
        private SubsObject subsObjects;

        @Expose
        @SerializedName("employees")
        private EmployeesObject employeesObject;

        @Expose
        @SerializedName("time_tables")
        private TimeTablesObject timeTables;

        @Expose
        @SerializedName("equipment")
        private EquipmentObject equipment;

        public TimeTablesObject getTimeTables() {
            return timeTables;
        }

        public SubsObject getSubs() {
            return subsObjects;
        }

        public EmployeesObject getEmployees() {
            return employeesObject;
        }

        public String getKey() {
            return key;
        }

        public int getTotalMenCount() {
            return totalMenCount;
        }

        public int getCurrentMaleCount() {
            return currentMaleCount;
        }

        public int getCurrentFemaleCount() {
            return currentFemaleCount;
        }

        public int getWaitingMaleCount() {
            return waitingMaleCount;
        }

        public int getWaitingFemaleCount() {
            return waitingFemaleCount;
        }

        public UnpaidItemsObject getUnpaidItems() {
            return unpaidItems;
        }

        public ProgramsObject getPrograms() {
            return programs;
        }

        public SubsObject getSubsObjects() {
            return subsObjects;
        }

        public EquipmentObject getEquipment() {
            return equipment;
        }
    }
}
