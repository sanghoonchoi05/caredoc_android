package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class LtiFeeSheetsObject extends ItemResponse<LtiFeeSheetsObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("service_type_id")
        private int serviceTypeId;

        @Expose
        @SerializedName("fee_type")
        private int feeType;

        @Expose
        @SerializedName("fee")
        private int fee;

        @Expose
        @SerializedName("copayment")
        private int copayment;

        @Expose
        @SerializedName("description")
        private String description;

        @Expose
        @SerializedName("grade_text")
        private String gradeText;

        public int getServiceTypeId() {
            return serviceTypeId;
        }

        public String getDescription() {
            return description;
        }

        public String getGradeText() {
            return gradeText;
        }

        public int getCopayment() {
            return copayment;
        }

        public int getFee() {
            return fee;
        }

        public int getFeeType() {
            return feeType;
        }
    }
}
