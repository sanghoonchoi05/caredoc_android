package com.onestepmore.caredoc.ui.carecoordi;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class CareCoordiStartViewModel extends BaseViewModel<CareCoordiStartNavigator> {

    public CareCoordiStartViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClose(){
        getNavigator().onClose();
    }

    public void onStart(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.CARECOORDI_CLICK_START);
        getNavigator().onStartCoordi();
    }
}
