package com.onestepmore.caredoc.ui.carecoordi;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class CareCoordiStartActivityModule {
    @Provides
    CareCoordiStartViewModel careCoordiViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new CareCoordiStartViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideCareCoordiViewModelProvider(CareCoordiStartViewModel careCoordiStartViewModel) {
        return new ViewModelProviderFactory<>(careCoordiStartViewModel);
    }
}
