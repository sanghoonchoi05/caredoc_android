package com.onestepmore.caredoc.ui.base;

import android.view.View;

public interface BaseNavigator {
    void handleError(Throwable throwable);
    void networkError(String msg);
    void showNeedLoginPopup();
    void showNeedLoginPopup(View.OnClickListener listener);
    void gotoAccountMainActivity();
}
