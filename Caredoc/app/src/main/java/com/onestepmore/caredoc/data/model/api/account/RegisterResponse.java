package com.onestepmore.caredoc.data.model.api.account;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class RegisterResponse {
    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("id")
    private Long userId;

    @Expose
    @SerializedName("photo_url")
    private String photoUrl;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Long getUserId() {
        return userId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}
