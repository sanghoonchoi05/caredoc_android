package com.onestepmore.caredoc.ui.main.profile.collaboration;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface CollaborationNavigator extends BaseNavigator {
    void onSendMail();
    void onSuccessSendMail();
}
