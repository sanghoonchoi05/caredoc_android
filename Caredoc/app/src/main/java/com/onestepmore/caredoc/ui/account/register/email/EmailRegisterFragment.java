package com.onestepmore.caredoc.ui.account.register.email;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.FEmailRegisterBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.login.LoginFragment;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.widgets.CommonDialog;
import com.onestepmore.caredoc.utils.CommonUtils;

import javax.inject.Inject;
import javax.inject.Named;

public class EmailRegisterFragment extends BaseFragment<FEmailRegisterBinding, EmailRegisterViewModel> implements EmailRegisterNavigator {

    @Inject
    @Named("EmailRegisterFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private EmailRegisterViewModel mEmailRegisterViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_email_register;
    }

    @Override
    public EmailRegisterViewModel getViewModel() {
        mEmailRegisterViewModel = ViewModelProviders.of(this, mViewModelFactory).get(EmailRegisterViewModel.class);
        return mEmailRegisterViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEmailRegisterViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setEditTextChangeObserver(
                getViewDataBinding().fEmailRegisterEmailEdit,
                getViewDataBinding().fEmailRegisterNameEdit,
                getViewDataBinding().fEmailRegisterPasswordEdit,
                getViewDataBinding().fEmailRegisterPasswordConfirmEdit);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.email_register, menu);
    }

    @Override
    public void openMainActivity() {
        BackStackManager.getInstance().removeAllFragment();
        Intent intent = getBaseActivity().newIntent(MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("index", 0);
        startActivity(intent);
        getBaseActivity().finish();
    }

    @Override
    public void onRegister() {
        getViewModel().register(
                getViewDataBinding().fEmailRegisterEmailEdit.getText().toString(),
                getViewDataBinding().fEmailRegisterNameEdit.getText().toString(),
                getViewDataBinding().fEmailRegisterPasswordEdit.getText().toString(),
                getViewDataBinding().fEmailRegisterPasswordConfirmEdit.getText().toString());
    }

    @Override
    public void onRegisterFailed(boolean email, boolean name, boolean password, boolean passwordConfirm) {
        /*CommonDialog dialog = new CommonDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", getString(R.string.warning_msg_register_failed));
        if(!email){
            bundle.putString("desc", getString(R.string.warning_msg_invalid_email));
        }else if(!name){
            bundle.putString("desc", getString(R.string.warning_msg_invalid_name));
        }else if(!password || !passwordConfirm){
            bundle.putString("desc", getString(R.string.warning_msg_invalid_password));
        }
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), "common_dialog");*/

        String title = getString(R.string.warning_msg_register_failed);
        String desc = "";
        if(!email){
            desc = getString(R.string.warning_msg_invalid_email);
        }else if(!name){
            desc = getString(R.string.warning_msg_invalid_name);
        }else if(!password || !passwordConfirm){
            desc = getString(R.string.warning_msg_invalid_password);
        }

        showCommonDialog(title, desc);
    }

    public void setEditTextChangeObserver(TextInputEditText emailEdit, TextInputEditText nameEdit,
                                          TextInputEditText passwordEdit, TextInputEditText passwordConfirmEdit) {
        rx.Observable<Boolean> emailChange = RxTextView.textChanges(emailEdit)
                .map(email -> CommonUtils.isEmailValid(email.toString().trim()));
        emailChange.subscribe(this::onChangedEmailText);

        rx.Observable<Boolean> nameChange = RxTextView.textChanges(nameEdit)
                .map(name -> CommonUtils.isNameValid(name.toString().trim()));
        nameChange.subscribe(this::onChangedNameText);

        rx.Observable<Boolean> isValidEmailAndName = rx.Observable.combineLatest(nameChange, emailChange,
                (_nameChange, _emailChange) -> _nameChange && _emailChange);

        rx.Observable<CharSequence> passwordChange = RxTextView.textChanges(passwordEdit);
        passwordChange.map(password -> CommonUtils.isPasswordValid(password.toString().trim())).subscribe(this::onChangedPasswordText);

        rx.Observable<CharSequence> passwordConfirmChange = RxTextView.textChanges(passwordConfirmEdit);

        rx.Observable<Boolean> isValidPassword = rx.Observable.combineLatest(passwordChange, passwordConfirmChange,
                (passwordText, passwordConfirmText) ->
                        CommonUtils.isPasswordValid(passwordText.toString().trim()) &&
                                passwordText.toString().trim().equals(passwordConfirmText.toString().trim()));
        isValidPassword.subscribe(this::checkPasswordEditText);

        rx.Observable<Boolean> isAllValid = rx.Observable.combineLatest(isValidEmailAndName, isValidPassword,
                (_isChange, _isChangePassword) -> _isChange && _isChangePassword);
        getViewModel().addSubscription(isAllValid.subscribe(this::onValid));
    }

    private void onValid(Boolean valid){
        getViewModel().setPossibleRegister(valid);
    }

    private void checkPasswordEditText(Boolean equals){
        getViewModel().setPasswordConfirmCorrect(equals);
        if(equals){
            getViewDataBinding().fEmailRegisterPasswordConfirmDesc.setText(R.string.f_email_register_password_confirm_correct);
            getViewDataBinding().fEmailRegisterPasswordConfirmDesc.setTextColor(getResources().getColor(R.color.colorBasicTitleText));
        }else{
            getViewDataBinding().fEmailRegisterPasswordConfirmDesc.setText(R.string.f_email_register_password_confirm_desc);
            if(getViewDataBinding().fEmailRegisterPasswordConfirmEdit.getText().length() > 0){
                getViewDataBinding().fEmailRegisterPasswordConfirmDesc.setTextColor(getResources().getColor(R.color.colorPrimary));
            }else{
                getViewDataBinding().fEmailRegisterPasswordConfirmDesc.setTextColor(getResources().getColor(R.color.colorDesc));
            }
        }
    }

    private void onChangedEmailText(Boolean emailValid){
        getViewModel().setEmailCorrect(emailValid);
        if(emailValid){
            getViewDataBinding().fEmailRegisterEmailDesc.setText(R.string.f_email_register_email_desc_correct);
            getViewDataBinding().fEmailRegisterEmailDesc.setTextColor(getResources().getColor(R.color.colorBasicTitleText));
        }else{
            getViewDataBinding().fEmailRegisterEmailDesc.setText(R.string.f_email_register_email_desc);
            getViewDataBinding().fEmailRegisterEmailDesc.setTextColor(getResources().getColor(R.color.colorDesc));
        }
    }

    private void onChangedNameText(Boolean valid){
        getViewModel().setNameCorrect(valid);
        if(valid){
            getViewDataBinding().fEmailRegisterNameDesc.setText(R.string.f_email_register_name_desc_correct);
            getViewDataBinding().fEmailRegisterNameDesc.setTextColor(getResources().getColor(R.color.colorBasicTitleText));
        }else{
            getViewDataBinding().fEmailRegisterNameDesc.setText(R.string.f_email_register_name_desc);
            getViewDataBinding().fEmailRegisterNameDesc.setTextColor(getResources().getColor(R.color.colorDesc));
        }
    }

    private void onChangedPasswordText(Boolean valid){
        getViewModel().setPasswordCorrect(valid);
        if(valid){
            getViewDataBinding().fEmailRegisterPasswordDesc.setText(R.string.f_email_register_password_desc_correct);
            getViewDataBinding().fEmailRegisterPasswordDesc.setTextColor(getResources().getColor(R.color.colorBasicTitleText));
        }else{
            getViewDataBinding().fEmailRegisterPasswordDesc.setText(R.string.f_email_register_password_desc);
            if(getViewDataBinding().fEmailRegisterPasswordEdit.getText().length() > 0){
                getViewDataBinding().fEmailRegisterPasswordDesc.setTextColor(getResources().getColor(R.color.colorPrimary));
            }else{
                getViewDataBinding().fEmailRegisterPasswordDesc.setTextColor(getResources().getColor(R.color.colorDesc));
            }
        }
    }
}
