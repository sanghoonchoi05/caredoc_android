package com.onestepmore.caredoc.data.model.api.object.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NaverUserInfoObject {
    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("profile_image")
    private String profile_image;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("name")
    private String name;

    public String getId() {
        return id;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }
}
