package com.onestepmore.caredoc.ui.address.detail;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AddressDetailFragmentProvider {
    @ContributesAndroidInjector(modules = AddressDetailFragmentModule.class)
    abstract AddressDetailFragment provideAddressDetailFragmentFactory();
}
