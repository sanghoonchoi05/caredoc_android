package com.onestepmore.caredoc.ui.main.search;

public interface FilterUICallback {
    void onClick(int filterPosition);
    void onDelete(int filterPosition);
}
