package com.onestepmore.caredoc.ui.main.taker.card;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerCardFragmentModule {
    @Provides
    TakerCardViewModel takerCardViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerCardViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerCardFragment")
    ViewModelProvider.Factory provideTakerCardViewModelProvider(TakerCardViewModel takerCardViewModel) {
        return new ViewModelProviderFactory<>(takerCardViewModel);
    }
}
