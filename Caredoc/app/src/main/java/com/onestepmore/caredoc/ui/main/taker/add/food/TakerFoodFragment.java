package com.onestepmore.caredoc.ui.main.taker.add.food;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxRadioGroup;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.databinding.FTakerFoodBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.BaseAddTakerFragment;

import javax.inject.Inject;
import javax.inject.Named;

public class TakerFoodFragment extends BaseAddTakerFragment<FTakerFoodBinding, TakerFoodViewModel> implements
        TakerFoodNavigator
{
    public static TakerFoodFragment newInstance(int titleResId, boolean isModify){
        TakerFoodFragment fragment = new TakerFoodFragment();
        fragment.setArguments(getBundle(titleResId, isModify));

        return  fragment;
    }

    @Inject
    @Named("TakerFoodFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerFoodViewModel mTakerFoodViewModel;
    private AddTakerRequest.Nutritions mCachedNutritions;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_food;
    }

    @Override
    public TakerFoodViewModel getViewModel() {
        mTakerFoodViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerFoodViewModel.class);
        return mTakerFoodViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerFoodViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            setModify(getArguments().getBoolean("isModify"));
        }

        mCachedNutritions = ((AddTakerActivity)getBaseActivity()).getCachedNutritions();
        if(isModify() && mCachedNutritions != null){
            getViewDataBinding().setNutritions(mCachedNutritions);
        }

        initTitle(getViewDataBinding().fTakerFoodTitle);
        getViewDataBinding().fTakerFoodDiabeticGroup.check(mCachedNutritions.isDiabetic() ? R.id.f_taker_food_diabetic_need : R.id.f_taker_food_diabetic_need_not);
        setTextObserver();
    }

    private void setTextObserver(){
        rx.Observable<CharSequence> favoriteObserver = RxTextView.textChanges(getViewDataBinding().fTakerFoodLikeLayout.vCommonTextLayout3FirstText);
        getViewModel().addSubscription(favoriteObserver.skip(1).subscribe((text) -> mCachedNutritions.setFavorite(text.toString())));

        rx.Observable<CharSequence> hateObserver = RxTextView.textChanges(getViewDataBinding().fTakerFoodDislikeLayout.vCommonTextLayout3FirstText);
        getViewModel().addSubscription(hateObserver.skip(1).subscribe((text) -> mCachedNutritions.setHate(text.toString())));

        rx.Observable<CharSequence> allergyObserver = RxTextView.textChanges(getViewDataBinding().fTakerFoodAllergyLayout.vCommonTextLayout3FirstText);
        getViewModel().addSubscription(allergyObserver.skip(1).subscribe((text) -> mCachedNutritions.setAllergy(text.toString())));

        rx.Observable<Integer> diabeticObserver = RxRadioGroup.checkedChanges(getViewDataBinding().fTakerFoodDiabeticGroup);
        getViewModel().addSubscription(diabeticObserver.subscribe((id) -> {
            mCachedNutritions.setDiabetic(id == R.id.f_taker_food_diabetic_need);
        }));
    }
}
