package com.onestepmore.caredoc.di.component;

import android.app.Application;

import com.onestepmore.caredoc.BasicApp;
import com.onestepmore.caredoc.di.builder.ActivityBuilder;
import com.onestepmore.caredoc.di.builder.ServiceBuilder;
import com.onestepmore.caredoc.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, AppModule.class, ActivityBuilder.class, ServiceBuilder.class})
public interface AppComponent {

    void inject(BasicApp app);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
