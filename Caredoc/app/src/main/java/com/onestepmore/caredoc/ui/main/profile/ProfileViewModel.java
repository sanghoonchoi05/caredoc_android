package com.onestepmore.caredoc.ui.main.profile;

import android.app.Application;
import android.databinding.ObservableBoolean;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Response;

public class ProfileViewModel extends BaseViewModel<ProfileNavigator> {
    private final ObservableBoolean mExistTaker = new ObservableBoolean(false);
    private final ObservableBoolean mExistRecentlyFacility = new ObservableBoolean(false);

    public ObservableBoolean getExistRecentlyFacility() {
        return mExistRecentlyFacility;
    }

    public void setExistRecentlyFacility(boolean existRecentlyFacility) {
        mExistRecentlyFacility.set(existRecentlyFacility);
    }

    public void setExistTaker(boolean existTaker) {
        mExistTaker.set(existTaker);
    }

    public ObservableBoolean getExistTaker() {
        return mExistTaker;
    }

    public ProfileViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClickTakerAllButton(){
        getNavigator().onShowTakerAll();
    }

    public void logout(){
        setIsLoading(true);
        getDataManager().doServerLogoutApiCall(new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    removeAccessToken();
                    deleteAllUser();
                    getDataManager().deleteAllFacility(getRealm());
                    getDataManager().deleteAllTaker(getRealm());
                    FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.TAB_PROFILE_LOGOUT_COMPLETE);
                    getDataManager().setLoggedInMode(AppDataSource.LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT);
                    getNavigator().onLogout();
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }

    private void deleteAllUser(){
        RealmResults<User> realmResults = getRealm().where(User.class).findAll();
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realmResults.deleteAllFromRealm();
            }
        });
    }

    public void onLoginButtonClick(){
        getNavigator().openLoginFragment();
    }

    public void onRegisterButtonClick(){
        getNavigator().openAccountMainActivity(false);
    }

    private void removeAccessToken(){
        User user = getRealm().where(User.class).findFirst();
        if(user != null){
            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    user.setToken("");
                    user.setTokenType("");
                    user.setExpiresIn(0);
                    realm.copyToRealmOrUpdate(user);
                }
            });
        }

        getDataManager().updateAccessToken("", 0, "");
    }

    public void onClickRecentlyAllButton(){
        getNavigator().onShowRecentlyAll();
    }
}
