package com.onestepmore.caredoc.ui.account.login;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface LoginNavigator extends BaseNavigator {
    void openMainActivity();
    void showResetPasswordView();
    void onLoginFailed();
}
