package com.onestepmore.caredoc.ui.main.search;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.databinding.IFacilityVerticalBinding;
import com.onestepmore.caredoc.databinding.IFacilityVerticalFooterBinding;
import com.onestepmore.caredoc.ui.main.FacilityVerticalAdapter;
import com.onestepmore.caredoc.ui.main.MainNavigator;

import io.realm.OrderedRealmCollection;

public class SearchFacilityAdapter extends FacilityVerticalAdapter<FacilitySearchList, FacilityVerticalAdapter.FacilityVerticalViewHolder> {

    public SearchFacilityAdapter(@Nullable OrderedRealmCollection<FacilitySearchList> data) {
        super(data, MainNavigator.FROM.SEARCH_FACILITY_LIST);
    }

    @Override
    public int getItemViewType(int position) {
        if(isShowFooter() && getData() != null && position == getData().size()){
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if(getData() == null || !getData().isValid()){
            return 0;
        }

        int addCount = 0;
        if(isShowFooter()){
            addCount = 1;
        }

        return getData().size() + addCount;
    }

    @NonNull
    @Override
    public FacilityVerticalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == FOOTER_VIEW) {
            IFacilityVerticalFooterBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_vertical_footer, viewGroup, false);

            return new FacilityVerticalViewHolder(binding);
        }

        IFacilityVerticalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_vertical, viewGroup, false);


        return new FacilityVerticalViewHolder(binding);
    }
}