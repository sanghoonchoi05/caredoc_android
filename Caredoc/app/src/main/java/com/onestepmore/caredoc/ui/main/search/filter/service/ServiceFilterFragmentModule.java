package com.onestepmore.caredoc.ui.main.search.filter.service;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceFilterFragmentModule {
    @Provides
    ServiceFilterViewModel serviceFilterViewModel(Application application, AppDataSource dataSource, Gson gson, SchedulerProvider schedulerProvider) {
        return new ServiceFilterViewModel(application, dataSource, schedulerProvider, gson);
    }

    @Provides
    @Named("ServiceFilterFragment")
    ViewModelProvider.Factory provideServiceFilterViewModelProvider(ServiceFilterViewModel serviceFilterViewModel) {
        return new ViewModelProviderFactory<>(serviceFilterViewModel);
    }
}
