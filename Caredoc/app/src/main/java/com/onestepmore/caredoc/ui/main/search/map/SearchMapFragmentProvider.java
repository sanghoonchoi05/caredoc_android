package com.onestepmore.caredoc.ui.main.search.map;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SearchMapFragmentProvider {
    @ContributesAndroidInjector(modules = SearchMapFragmentModule.class)
    abstract SearchMapFragment provideSearchMapFragmentFactory();
}
