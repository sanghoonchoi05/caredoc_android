package com.onestepmore.caredoc.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.ui.main.search.FilterUI;
import com.onestepmore.caredoc.ui.widgets.chart.ValueFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public final class TestDataUtils {

    private static LatLng getRandomKoreaLocation(){
        Random random = new Random();
        float lat = random.nextFloat()*3 + 34.3f;
        float lng = random.nextFloat()*3 + 126.7f;
        return new LatLng(lat, lng);
    }

    private static LatLng getRandomKoreaLocation(LatLngBounds bounds){
        Random random = new Random();
        double lat = random.nextFloat()*(bounds.northeast.latitude - bounds.southwest.latitude) + bounds.southwest.latitude;
        double lng = random.nextFloat()*(bounds.northeast.longitude - bounds.southwest.longitude) + bounds.southwest.longitude;
        return new LatLng(lat, lng);
    }

    public static List<LatLng> getRandomKoreaLocations(int count){
        List<LatLng> latLngList = new ArrayList<>();
        Random random = new Random();
        for(int i=0; i<count; i++){
            latLngList.add(getRandomKoreaLocation());
        }
        return latLngList;
    }

    public static List<LatLng> getRandomKoreaLocations(int count, LatLngBounds bounds){
        List<LatLng> latLngList = new ArrayList<>();
        Random random = new Random();
        for(int i=0; i<count; i++){
            latLngList.add(getRandomKoreaLocation(bounds));
        }
        return latLngList;
    }

    public static int getRandomInteger(int range){
        int ran = 0;
        Random random = new Random();
        ran = random.nextInt(range);
        return ran;
    }

    public static float getRandomFloat(float range){
        float ran = 0f;
        Random random = new Random();
        ran = random.nextFloat()*range;
        return ran;
    }

    public static List<FilterUI> getFacilityFilterListTempData(Context context){
        String [] filters = context.getResources().getStringArray(R.array.f_search_facility_facility_filter);
        List<FilterUI> filterUIList = new ArrayList<>();
        for(String filter : filters){
            FilterUI filterUI = new FilterUI();
            filterUI.filterName = filter;
            filterUI.isSelect = false;
            filterUIList.add(filterUI);
        }

        return filterUIList;
    }

    public static LineDataSet getGradeLineDataSet(Context context, int [] grades, Drawable icon){

        ArrayList<Entry> values = new ArrayList<>();
        for (int i = 0; i < grades.length; i++) {
            values.add(new Entry(i, grades[i], icon));
        }

        /*String [] gradeList = context.getResources().getStringArray(R.array.grade_reverse_list);
        if(!isHospital){
            gradeList = context.getResources().getStringArray(R.array.grade_alphabet_detail_reverse_list);
        }*/

        String [] gradeList = new String[]{};

        LineDataSet lineDataSet = new LineDataSet(values, "");
        lineDataSet.setDrawIcons(false);
        lineDataSet.setValueFormatter(new ValueFormatter(gradeList));
        lineDataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);

        lineDataSet.setColor(context.getResources().getColor(R.color.facility_detail_chart_line));
        Integer [] colorset = getColorSetWithGrade(context, grades);
        lineDataSet.setCircleColors(Arrays.asList(colorset));
        lineDataSet.setValueTextColors(Arrays.asList(colorset));

        // line thickness and point size
        lineDataSet.setLineWidth(1f);
        lineDataSet.setCircleRadius(3f);

        // draw points as solid circles
        lineDataSet.setDrawCircleHole(false);

        // customize legend entry
        lineDataSet.setFormLineWidth(2f);
        //lineDataSet.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
        lineDataSet.setFormSize(16.f);

        // text size of values
        lineDataSet.setValueTextSize(12.7f);

        return lineDataSet;
    }

    public static Integer [] getColorSetWithGrade(Context context, int [] gradeList){
        Integer [] colorSet = new Integer[gradeList.length];
        for(int i=0; i<gradeList.length; i++){
            colorSet[i] = getColorWithGrade(context, gradeList[i]);
        }

        return colorSet;
    }

    public static Integer getColorWithGrade(Context context, int grade){
        Integer color = ContextCompat.getColor(context, R.color.no_grade);
        if(grade == 1){
            color = ContextCompat.getColor(context, R.color.grade_5);
        }else if(grade == 2){
            color = ContextCompat.getColor(context, R.color.grade_4);
        }else if(grade == 3){
            color = ContextCompat.getColor(context, R.color.grade_3);
        }else if(grade == 4){
            color = ContextCompat.getColor(context, R.color.grade_2);
        }else if(grade == 5){
            color = ContextCompat.getColor(context, R.color.grade_1);
        }

        return color;
    }


    public static final float SIDO_ZOOM_MIN = 7f;
    public static final float SIGUNGU_ZOOM_MIN = 11f;
    public static final float DONGMYUN_ZOOM_MIN = 14f;
    public static final float DONGMYUN_ZOOM_MAX = 15f;

    public static LatLng getKoreaCenterPosition(){
        return new LatLng(36.613071, 127.930777);
    }
}
