package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public class NursingAddOnsObject extends ItemResponse<NursingAddOnsObject.Item> {

    public final class Item{
        @Expose
        @SerializedName("code")
        private String code;

        @Expose
        @SerializedName("name")
        private String typeName;

        @Expose
        @SerializedName("grade")
        private int grade;

        public String getCode() {
            return code;
        }

        public String getName() {
            return typeName;
        }

        public int getGrade() {
            return grade;
        }

        public void setGrade(int grade) {
            this.grade = grade;
        }
    }
}
