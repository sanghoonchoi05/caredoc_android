package com.onestepmore.caredoc.ui.main.search.dialog;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchDialogModule {
    @Provides
    SearchDialogViewModel searchDialogViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new SearchDialogViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("SearchDialog")
    ViewModelProvider.Factory provideSearchDialogViewModelProvider(SearchDialogViewModel searchDialogViewModel) {
        return new ViewModelProviderFactory<>(searchDialogViewModel);
    }
}
