package com.onestepmore.caredoc.ui.main.detail.photo;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.data.model.realm.Attach;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.databinding.APhotoBinding;
import com.onestepmore.caredoc.databinding.IPhotoBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.widgets.StartPagerSnapHelper;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.List;

import javax.inject.Inject;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PHOTO_GALLERY_CLICK_BACK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.PHOTO_GALLERY_CLICK_CAROUSEL_INDICATOR;

public class PhotoActivity extends BaseActivity<APhotoBinding, PhotoViewModel> implements PhotoNavigator{

    public static final String EXTRA_FACILITY= "com.onestepmore.caredoc.ui.main.detail.photo.FACILITY";

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private PhotoViewModel mPhotoViewModel;

    private int mPhotoPosition = 0;
    private int mPhotoListCount = 0;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_photo;
    }

    @Override
    public PhotoViewModel getViewModel() {
        mPhotoViewModel = ViewModelProviders.of(this, mViewModelFactory).get(PhotoViewModel.class);
        return mPhotoViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPhotoViewModel.setNavigator(this);

        UIUtils.setStatusBarColor(this, getWindow().getDecorView(), Color.BLACK, true);

        if(getIntent() != null){
            FacilityRealmModel facility = getIntent().getParcelableExtra(EXTRA_FACILITY);
            if(facility != null){
                initAdapter(facility.getAttaches());
            }
        }
    }

    private void initAdapter(List<Attach> attaches){
        if(getViewDataBinding() == null || attaches == null){
            return;
        }

        PhotoAdapter adapter = new PhotoAdapter();
        getViewDataBinding().aPhotoRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManagerCenter = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().aPhotoRecyclerView.setLayoutManager(layoutManagerCenter);

        if(attaches.size()==0){
            getViewDataBinding().aPhotoIndicatorLayout.setVisibility(View.GONE);
        }else{
            String [] urls = new String[attaches.size()];
            for(int i=0; i<urls.length; i++){
                urls[i] = attaches.get(i).getUrl();
            }

            mPhotoListCount = urls.length;
            setPhotoIndicatorText();
            adapter.setImgList(urls);
        }

        SnapHelper snapHelperCenter = new StartPagerSnapHelper(0);
        snapHelperCenter.attachToRecyclerView(getViewDataBinding().aPhotoRecyclerView);

        getViewDataBinding().aPhotoRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int snapPosition = RecyclerView.NO_POSITION;
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if(layoutManager!=null){
                    View snapView = snapHelperCenter.findSnapView(layoutManager);
                    if(snapView!=null){
                        snapPosition = layoutManager.getPosition(snapView);
                        onChangeSnapPosition(snapPosition);
                    }
                }

                AppLogger.i(getClass(), "Scrolled x: " + dx);
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void setPhotoIndicatorText(){
        getViewDataBinding().aPhotoIndicatorText.setText(String.format(getString(R.string.common_slash_num2), mPhotoPosition+1, mPhotoListCount));
    }

    private void onChangeSnapPosition(int snapPosition){
        if(mPhotoPosition == snapPosition){
            return;
        }
        if(mPhotoPosition > snapPosition){
            AmplitudeManager.getInstance().logEvent(PHOTO_GALLERY_CLICK_CAROUSEL_INDICATOR, AmplitudeParam.Key.VALUE, AmplitudeParam.DIRECTION.LEFT.toString());
        }
        else{
            AmplitudeManager.getInstance().logEvent(PHOTO_GALLERY_CLICK_CAROUSEL_INDICATOR, AmplitudeParam.Key.VALUE, AmplitudeParam.DIRECTION.RIGHT.toString());
        }

        mPhotoPosition = snapPosition;
        setPhotoIndicatorText();
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onClose() {
        AmplitudeManager.getInstance().logEvent(PHOTO_GALLERY_CLICK_BACK);
        finish();
    }

    public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

        @Nullable
        private String [] mImgUrls;

        public void setImgList(final String [] imgUrls){
            mImgUrls = imgUrls;
            notifyItemRangeInserted(0, mImgUrls.length);
        }

        @NonNull
        @Override
        public PhotoAdapter.PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            IPhotoBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_photo, viewGroup, false);
            return new PhotoViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull PhotoViewHolder viewHolder, int position) {
            if(mImgUrls != null){
                Glide.with(viewHolder.itemView.getContext()).load(mImgUrls[position]).into(viewHolder.binding.iPhoto);
                viewHolder.binding.executePendingBindings();
            }
        }

        @Override
        public int getItemCount() {
            return mImgUrls == null ? 0 : mImgUrls.length;
        }

        class PhotoViewHolder extends RecyclerView.ViewHolder {
            final IPhotoBinding binding;

            private PhotoViewHolder(IPhotoBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}
