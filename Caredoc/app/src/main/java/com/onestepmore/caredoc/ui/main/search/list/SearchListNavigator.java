package com.onestepmore.caredoc.ui.main.search.list;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface SearchListNavigator extends BaseNavigator {
    void onMap();
}
