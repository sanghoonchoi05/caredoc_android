package com.onestepmore.caredoc.data.remote;

import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailEntranceResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailMedicalResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchCodeResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchResponse;
import com.onestepmore.caredoc.data.model.api.object.ServiceEntranceObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceMedicalObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceObject;

import org.json.JSONObject;

import io.reactivex.Flowable;

public interface FacilitySearchApiHelper {
    Flowable<FacilitySearchMapResponse> getFacilitySearchClusterApiCall(FacilitySearchMapRequest.QueryParameter queryParameter);
    Flowable<FacilitySearchResponse> getFacilitySearchListApiCall(FacilitySearchRequest.QueryParameter queryParameter);
    Flowable<FacilitySearchCodeResponse> getFacilitySearchCodeApiCall(FacilitySearchMapRequest.QueryParameter queryParameter);

    Flowable<FacilityDetailResponse> getFacilityDetailBaseApiCall(FacilityDetailRequest.PathParameter pathParameter);
    Flowable<FacilityDetailMedicalResponse> getFacilityHospitalDetailApiCall(FacilityDetailRequest.PathParameter pathParameter);
    Flowable<FacilityDetailEntranceResponse> getFacilityHomecareDetailApiCall(FacilityDetailRequest.PathParameter pathParameter);

    Flowable<JSONObject> getFacilityCareCoordiListApiCall(CareCoordiRequest.QueryParameter queryParameter);
}
