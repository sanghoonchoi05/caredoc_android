package com.onestepmore.caredoc.ui.main.search.filter.address;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.onestepmore.caredoc.ui.main.search.filter.address.list.AddressListFragment;

import java.util.ArrayList;
import java.util.List;

public class AddressFilterPagerAdapter extends FragmentStatePagerAdapter {

    private List<AddressListFragment> addressListFragments = new ArrayList<>();

    public AddressFilterPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addAddressListFragment(AddressListFragment fragment, AddressFilterNavigator navigator){
        fragment.setAddressListNavigator(navigator);
        addressListFragments.add(fragment);
    }

    public AddressListFragment getAddressListFragment(int position) {
        return addressListFragments.get(position);
    }

    @Override
    public int getCount() {
        return addressListFragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return addressListFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
}
