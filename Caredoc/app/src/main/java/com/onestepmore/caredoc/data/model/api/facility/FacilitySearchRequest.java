package com.onestepmore.caredoc.data.model.api.facility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class FacilitySearchRequest {
    public enum ORDER_BY{
        grade("grade"),
        review("review"),
        rating("rating");

        private String name;
        ORDER_BY(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    public static class QueryParameter{
        @Expose
        @SerializedName("skip")
        private int skip;

        @Expose
        @SerializedName("take")
        private int take;

        @Expose
        @SerializedName("filter")
        private String filter;

        @Expose
        @SerializedName("orderBy")
        private String orderBy;

        public String getFilter() {
            return filter;
        }

        public void setFilter(String filter) {
            this.filter = filter;
        }

        public int getSkip() {
            return skip;
        }

        public void setSkip(int skip) {
            this.skip = skip;
        }

        public void setOrderBy(String orderBy) {
            this.orderBy = orderBy;
        }

        public void setTake(int take) {
            this.take = take;
        }

        public int getTake() {
            return take;
        }

        public String getOrderBy() {
            return orderBy;
        }
    }
}
