package com.onestepmore.caredoc.ui.account.login.forgot;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class LoginForgotFragmentProvider {
    @ContributesAndroidInjector(modules = LoginForgotFragmentModule.class)
    abstract LoginForgotFragment provideLoginForgotFragmentFactory();
}
