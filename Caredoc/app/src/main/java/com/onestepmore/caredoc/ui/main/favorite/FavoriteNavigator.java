package com.onestepmore.caredoc.ui.main.favorite;

import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface FavoriteNavigator extends BaseNavigator {
    void onItemClick(StaticCategory category);
    void openLoginFragment();
    void openAccountMainActivity();
}
