package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceDetailObject extends ServiceObject{
    @Expose
    @SerializedName("evaluations")
    private EvaluationsObject evaluations;

    @Expose
    @SerializedName("rating_relations")
    private RatingRelationsObject ratingRelations;


    public EvaluationsObject getEvaluations() {
        return evaluations;
    }

    public RatingRelationsObject getRatingRelations() {
        return ratingRelations;
    }
}
