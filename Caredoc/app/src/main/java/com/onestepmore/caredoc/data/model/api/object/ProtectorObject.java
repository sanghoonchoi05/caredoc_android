package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProtectorObject {
    @Expose
    @SerializedName("id")
    private long id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("photo_url")
    private String photoUrl;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("email_verified_at")
    private String emailVerifiedAt;

    @Expose
    @SerializedName("contact")
    private String contact;

    @Expose
    @SerializedName("is_admin")
    private boolean isAdmin;

    @Expose
    @SerializedName("user_type")
    private String userType;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public String getContact() {
        return contact;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public String getUserType() {
        return userType;
    }
}
