package com.onestepmore.caredoc.data.model.api.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.external.CoordinateResultObject;
import com.onestepmore.caredoc.data.model.api.object.external.JusoResultObject;

public class ExternalCoordinateResponse {
    @Expose
    @SerializedName("results")
    private CoordinateResultObject results;

    public CoordinateResultObject getResults() {
        return results;
    }
}
