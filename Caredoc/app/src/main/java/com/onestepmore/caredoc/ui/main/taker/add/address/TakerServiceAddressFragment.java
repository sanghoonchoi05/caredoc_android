package com.onestepmore.caredoc.ui.main.taker.add.address;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.databinding.FTakerAddressBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.address.AddressMainActivity;
import com.onestepmore.caredoc.ui.common.AddressClickListener;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.BaseAddTakerFragment;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Address;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Road;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Location;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.onestepmore.caredoc.ui.address.AddressMainActivity.ADDRESS_RESULT_CODE;

public class TakerServiceAddressFragment extends BaseAddTakerFragment<FTakerAddressBinding, TakerServiceAddressViewModel> implements
        TakerServiceAddressNavigator,
        AddressClickListener
{
    public static TakerServiceAddressFragment newInstance(int titleResId, boolean isModify){
        TakerServiceAddressFragment fragment = new TakerServiceAddressFragment();
        fragment.setArguments(getBundle(titleResId, isModify));

        return  fragment;
    }

    private final static int FIRST_ADDRESS_REQUEST_CODE = 101;
    private final static int SECONDE_ADDRESS_REQUEST_CODE = 102;
    private final static int THIRD_ADDRESS_REQUEST_CODE = 103;

    @Inject
    @Named("TakerServiceAddressFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerServiceAddressViewModel mTakerServiceAddressViewModel;
    private List<Address> mCachedAddress;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_address;
    }

    @Override
    public TakerServiceAddressViewModel getViewModel() {
        mTakerServiceAddressViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerServiceAddressViewModel.class);
        return mTakerServiceAddressViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerServiceAddressViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            setModify(getArguments().getBoolean("isModify"));
        }

        mCachedAddress = ((AddTakerActivity)getBaseActivity()).getCachedAddresses();
        if(isModify() && mCachedAddress != null){
            getViewDataBinding().setAddressList(mCachedAddress);
        }

        initTitle(getViewDataBinding().fTakerAddressTitle);

        getViewDataBinding().setAddressListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == ADDRESS_RESULT_CODE){

            if(data == null){
                return;
            }

            int index = -1;
            if(requestCode == FIRST_ADDRESS_REQUEST_CODE){
                index = 0;
            }
            else if(requestCode == SECONDE_ADDRESS_REQUEST_CODE){
                index = 1;
            }
            else if(requestCode == THIRD_ADDRESS_REQUEST_CODE){
                index = 2;
            }

            if(index == -1){
                return;
            }

            if(mCachedAddress.size() > index){
                mCachedAddress.remove(index);
            }

            JusoRealmModel juso = data.getParcelableExtra("juso");
            addCachedAddress(index, juso);

            getViewDataBinding().setAddressList(mCachedAddress);
        }
    }

    private void addCachedAddress(int index, JusoRealmModel juso){
        String addressName = juso.getJibunAddress();
        StringBuilder roadName = new StringBuilder();
        roadName.append(juso.getRoadAddress());
        if(!CommonUtils.checkNullAndEmpty(juso.getBuildingName())){
            roadName.append(" ");
            roadName.append(juso.getBuildingName());
        }

        if(CommonUtils.checkNullAndEmpty(addressName)){
            addressName = roadName.toString();
        }

        Address address = new Address();
        address.setName(addressName);
        address.setCode(juso.getId());
        address.setDetail(juso.getDetail());
        Road road = new Road();
        road.setName(roadName.toString());
        road.setCode(juso.getRoadCode());
        address.setRoad(road);
        Location location = new Location();
        location.setLat(String.valueOf(juso.getLat()));
        location.setLng(String.valueOf(juso.getLng()));
        address.setLocation(location);

        mCachedAddress.add(index, address);
    }

    @Override
    public void onShowAddressView(int priority) {
        int requestCode = FIRST_ADDRESS_REQUEST_CODE;
        if(priority == 2){
            requestCode = SECONDE_ADDRESS_REQUEST_CODE;
        }
        else if(priority == 3){
            requestCode = THIRD_ADDRESS_REQUEST_CODE;
        }
        Intent intent = getBaseActivity().newIntent(AddressMainActivity.class);
        getBaseActivity().startActivityForResult(intent, requestCode,this);
    }

    @Override
    public void onDeleteAddress(int priority) {
        if(mCachedAddress.size() >= priority){
            mCachedAddress.remove(priority - 1);
        }

        getViewDataBinding().setAddressList(mCachedAddress);
    }
}
