package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class RatingsObject extends ItemResponse<RatingsObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("label")
        private String label;

        @Expose
        @SerializedName("rating")
        private int rating;

        public String getLabel() {
            return label;
        }

        public int getRating() {
            return rating;
        }
    }
}
