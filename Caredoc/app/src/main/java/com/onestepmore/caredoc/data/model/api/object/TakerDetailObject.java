package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TakerDetailObject extends TakerObject {
    @Expose
    @SerializedName("diseases")
    private List<TakerDiseaseObject> takerDiseaseObjects;

    @Expose
    @SerializedName("protector")
    private ProtectorObject protectorObject;

    public List<TakerDiseaseObject> getTakerDiseaseObjects() {
        return takerDiseaseObjects;
    }

    public ProtectorObject getProtectorObject() {
        return protectorObject;
    }
}
