package com.onestepmore.caredoc.ui.start;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class StartActivityModule {
    @Provides
    StartViewModel startViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new StartViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideStartViewModelProvider(StartViewModel startViewModel) {
        return new ViewModelProviderFactory<>(startViewModel);
    }
}
