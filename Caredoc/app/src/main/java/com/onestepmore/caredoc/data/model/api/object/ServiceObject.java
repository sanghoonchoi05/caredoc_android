package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceObject {
    public enum CATEGORY{
        NURSING_HOME("nursing-home"),
        VISIT_HOME("visit-home"),
        GERIATRIC_HOSPITAL("geriatric-hospital");

        private String name;
        CATEGORY(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    @Expose
    @SerializedName("fake_id")
    private long fakeId;

    @Expose
    @SerializedName("service_id")
    private int serviceId;

    @Expose
    @SerializedName("service_name")
    private String serviceName;

    @Expose
    @SerializedName("service_category")
    private String serviceCategory;

    @Expose
    @SerializedName("service_description")
    private String serviceDescription;

    @Expose
    @SerializedName("latest_grade")
    private String latestGrade;

    @Expose
    @SerializedName("rating_avg")
    private float ratingAvg;

    @Expose
    @SerializedName("reviews_count")
    private int reviewsCount;

    @Expose
    @SerializedName("is_closed")
    private boolean isClosed;

    @Expose
    @SerializedName("latest_evaluation")
    private EvaluationObject latestEvaluation;

    @Expose
    @SerializedName("lti_fee_sheets")
    private LtiFeeSheetsObject ltiFeeSheetsObject;

    @Expose
    @SerializedName("evaluation_source")
    private EvaluationSourceObject evaluationSource;

    public long getFakeId() {
        return fakeId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public String getLatestGrade() {
        return latestGrade;
    }

    public EvaluationObject getLatestEvaluation() {
        return latestEvaluation;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public EvaluationSourceObject getEvaluationSource() {
        return evaluationSource;
    }

    public String getServiceCategory() {
        return serviceCategory;
    }

    public LtiFeeSheetsObject getLtiFeeSheetsObject() {
        return ltiFeeSheetsObject;
    }

    public boolean isClosed() {
        return isClosed;
    }
}
