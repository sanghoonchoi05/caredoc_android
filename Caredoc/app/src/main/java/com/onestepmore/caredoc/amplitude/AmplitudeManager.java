package com.onestepmore.caredoc.amplitude;

import android.app.Application;
import android.content.Context;

import com.amplitude.api.Amplitude;
import com.amplitude.api.AmplitudeClient;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.amplitude.api.Identify;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import javax.inject.Singleton;

@Singleton
public class AmplitudeManager {

    private static final String TRUE = "TRUE";

    private static AmplitudeManager mAmplitudeManager;
    private AmplitudeClient mAmplitudeClient;

    public static AmplitudeManager newInstance(Context context, Application application){
        if(mAmplitudeManager == null){
            mAmplitudeManager = new AmplitudeManager();
            mAmplitudeManager.initialize(context, application);
        }

        return mAmplitudeManager;
    }

    private void initialize(Context context, Application application) {
        mAmplitudeClient = Amplitude.getInstance().initialize(context, BuildConfig.AMPLITUDE_API_KEY).enableForegroundTracking(application);
    }

    public static AmplitudeManager getInstance(){
        return mAmplitudeManager;
    }

    public void setUserProperty(User user){
        Identify identify = new Identify().set("id", user.getUserId()).set("name", user.getName());
        mAmplitudeClient.identify(identify);
    }

    public void logEvent(BaseActivity baseActivity){
        String eventName = AmplitudeEvent.getEventName(baseActivity);
        if(CommonUtils.checkNullAndEmpty(eventName)){
            return;
        }

        logEvent(eventName);
    }

    public void logEvent(BaseFragment baseFragment){
        String eventName = AmplitudeEvent.getEventName(baseFragment);
        if(CommonUtils.checkNullAndEmpty(eventName)){
            return;
        }

        logEvent(eventName);
    }

    public void logEventForBackKey(BaseFragment baseFragment){
        String eventName = AmplitudeEvent.getBackKeyEventName(baseFragment);
        if(CommonUtils.checkNullAndEmpty(eventName)){
            return;
        }

        logEvent(eventName);
    }

    public void logEventForBackKey(BaseActivity baseActivity){
        String eventName = AmplitudeEvent.getBackKeyEventName(baseActivity);
        if(CommonUtils.checkNullAndEmpty(eventName)){
            return;
        }

        logEvent(eventName);
    }

    public void logEvent(String eventName){
        logEvent(eventName, new JSONObject());
    }

    public void logEvent(String eventName, String key, int value){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put(key, value);
            logEvent(eventName, jsonObject);
        }catch (JSONException e){

        }
    }

    public void logEvent(String eventName, String key, float value){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put(key, value);
            logEvent(eventName, jsonObject);
        }catch (JSONException e){

        }
    }

    public void logEvent(String eventName, String key, String value){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put(key, value);
            logEvent(eventName, jsonObject);
        }catch (JSONException e){

        }
    }

    public void logEvent(String eventName, Map<String, MapValue<?>> map){
        JSONObject jsonObject = new JSONObject();
        try{
            for(String key : map.keySet()){
                if(map.get(key) != null){
                    jsonObject.put(key, map.get(key).get());
                }
            }
            logEvent(eventName, jsonObject);
        }catch (JSONException e){

        }
    }

    public void logEvent(String eventName, JSONObject jsonObject){
        if(jsonObject != null && jsonObject.length() > 0){
            mAmplitudeClient.logEvent(eventName, jsonObject);
        }
        else{
            mAmplitudeClient.logEvent(eventName);
        }
    }
}
