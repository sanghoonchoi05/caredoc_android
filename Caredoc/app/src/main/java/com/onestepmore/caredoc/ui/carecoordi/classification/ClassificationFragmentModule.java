package com.onestepmore.caredoc.ui.carecoordi.classification;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ClassificationFragmentModule {
    @Provides
    ClassificationViewModel classificationViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new ClassificationViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("ClassificationFragment")
    ViewModelProvider.Factory provideClassificationProvider(ClassificationViewModel classificationViewModel) {
        return new ViewModelProviderFactory<>(classificationViewModel);
    }
}
