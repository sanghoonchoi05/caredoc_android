package com.onestepmore.caredoc.ui.main.profile.contact;

import android.app.Application;

import com.onestepmore.caredoc.data.model.api.external.ExternalMailgunRequest;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class ContactViewModel extends BaseViewModel<ContactNavigator> {
    public ContactViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onTypeLayoutClick(){
        getNavigator().onTypeLayoutClick();
    }

    public void onSendEmail(){
        getNavigator().onSendMail();
    }

    public void sendEmail(String from, String to, String subject, String text){
        ExternalMailgunRequest.QueryParameter queryParameter = new ExternalMailgunRequest.QueryParameter();
        queryParameter.setFrom(from);
        queryParameter.setTo(to);
        queryParameter.setSubject(subject);
        queryParameter.setText(text);
        getCompositeDisposable().add(getDataManager()
                .sendExternalMailgun(queryParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if(response != null && response.getMessage().startsWith("Queued")){
                        getNavigator().onSuccessSendMail();
                    }
                }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }
}
