package com.onestepmore.caredoc.ui.main.home;

import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;

public interface SearchCategoryClickCallback {
    void onClick(StaticCategory category);
}
