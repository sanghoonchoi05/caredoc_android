package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

import java.util.List;

public class ServiceAddressObject extends ItemResponse<ServiceAddressObject.Data> {
    public class Data{
        @Expose
        @SerializedName("id")
        private long id;

        @Expose
        @SerializedName("address_name")
        private String addressName;

        @Expose
        @SerializedName("address_code")
        private String addressCode;

        @Expose
        @SerializedName("address_detail")
        private String addressDetail;

        @Expose
        @SerializedName("road_address_name")
        private String roadAddressName;

        @Expose
        @SerializedName("road_address_code")
        private String roadAddressCode;

        @Expose
        @SerializedName("geo_location")
        private ApiCommonObject.GeoLocation geoLocation;

        public long getId() {
            return id;
        }

        public String getAddressName() {
            return addressName;
        }

        public String getAddressCode() {
            return addressCode;
        }

        public String getRoadAddressName() {
            return roadAddressName;
        }

        public String getRoadAddressCode() {
            return roadAddressCode;
        }

        public ApiCommonObject.GeoLocation getGeoLocation() {
            return geoLocation;
        }

        public String getAddressDetail() {
            return addressDetail;
        }
    }
}
