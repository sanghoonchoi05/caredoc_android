/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

public class ApiError {

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("errors")
    private JSONObject errorsJson;

    //@Expose
    @SerializedName("errors")
    private ErrorResponse errors;

    public ApiError(String message, String type, JSONObject errorsJson) {
        this.message = message;
        this.type = type;
        this.errorsJson = errorsJson;
    }

    /*@Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        ApiError apiError = (ApiError) object;

        return message != null ? message.equals(apiError.message) : apiError.message == null;

    }

    @Override
    public int hashCode() {
        int result = (message != null ? message.hashCode() : 0);
        return result;
    }*/

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }

    public ErrorResponse getErrorsResponse() {
        return errors;
    }

    public class ErrorResponse{
        @Expose
        @SerializedName("password")
        private String[] passwords;

        public ErrorResponse(String[] passwords){
            this.passwords = passwords;
        }

        public String[] getPasswords() {
            return passwords;
        }
    }
}
