package com.onestepmore.caredoc.ui.main.favorite.detail;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FavoriteDetailFragmentProvider {
    @ContributesAndroidInjector(modules = FavoriteDetailFragmentModule.class)
    abstract FavoriteDetailFragment provideFavoriteDetailFragmentFactory();
}
