package com.onestepmore.caredoc.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemResponse<T> {
    @Expose
    @SerializedName("items")
    private List<T> items;

    @Expose
    @SerializedName("total")
    private int total;

    public int getTotal() {
        return total;
    }

    public List<T> getItems() {
        return items;
    }
}
