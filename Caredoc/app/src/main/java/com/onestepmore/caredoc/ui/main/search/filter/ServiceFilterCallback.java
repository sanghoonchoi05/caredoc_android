package com.onestepmore.caredoc.ui.main.search.filter;

import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilter;

public interface ServiceFilterCallback {
    void onServiceCheck(ServiceFilter service, boolean checked);
    void onServiceGradeClick(int position);
}
