package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.common.Priority;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.androidnetworking.internal.InternalNetworking;
import com.androidnetworking.utils.Utils;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoResponse;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerResponse;
import com.onestepmore.caredoc.data.model.api.taker.DeleteTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

@Singleton
public class TakerApiHelperImp implements TakerApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    public TakerApiHelperImp(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public Flowable<AddTakerPhotoResponse> addTakerPhotoApiCall(AddTakerPhotoRequest.PathParameter pathParameter, File file) {
        return Rx2AndroidNetworking.upload(ApiEndPoint.ENDPOINT_ADD_TAKER_PHOTO)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addMultipartFile("file",file)
                //.setPriority(Priority.HIGH)
                .addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(AddTakerPhotoResponse.class);
    }

    @Override
    public void addTakerPhotoApiCall(Callback responseCallback, long takerId, File file) {
        MediaType mediaType = MediaType.parse(Utils.getMimeType(file.getName()));
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file",file.getName(), RequestBody.create(mediaType,file))
                .build();

        Request request = new Request.Builder()
                .url(ApiEndPoint.ENDPOINT_ADD_TAKER_PHOTO.replace("{id}", String.valueOf(takerId)))
                .post(requestBody)
                .addHeader("Authorization", mApiHeader.getProtectedApiHeader().getAccessToken())
                .addHeader("CAREDOC-API-KEY", mApiHeader.getProtectedApiHeader().getApiKey())
                .build();

        OkHttpClient client = InternalNetworking.getClient();
        client.newCall(request).enqueue(responseCallback);
    }

    @Override
    public Flowable<AddTakerResponse> addTakerApiCall(JSONObject jsonObject) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_TAKER)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(jsonObject)
                .build()
                .getObjectFlowable(AddTakerResponse.class);
    }

    @Override
    public Flowable<AddTakerResponse> modifyTakerApiCall(JSONObject jsonObject, AddTakerRequest.PathParameter pathParameter) {
        return Rx2AndroidNetworking.patch(ApiEndPoint.ENDPOINT_MODIFY_TAKER)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(jsonObject)
                .addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(AddTakerResponse.class);
    }

    @Override
    public Flowable<TakerDetailResponse> getTakerDetailApiCall(TakerDetailRequest.PathParameter pathParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_TAKER_DETAIL)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(TakerDetailResponse.class);
    }

    @Override
    public void deleteTakerApiCall(OkHttpResponseListener listener, DeleteTakerRequest.PathParameter pathParameter) {
        Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_DELETE_TAKER)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(pathParameter)
                .build()
                .getAsOkHttpResponse(listener);
    }
}
