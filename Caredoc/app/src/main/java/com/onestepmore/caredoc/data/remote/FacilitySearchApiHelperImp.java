package com.onestepmore.caredoc.data.remote;

import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailEntranceResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailMedicalResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailResponse;
import com.onestepmore.caredoc.data.model.api.object.FacilityDetailObject;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchCodeResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class FacilitySearchApiHelperImp implements FacilitySearchApiHelper {

    private ApiHeader mApiHeader;

    @Inject
    public FacilitySearchApiHelperImp(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public Flowable<FacilitySearchMapResponse> getFacilitySearchClusterApiCall(FacilitySearchMapRequest.QueryParameter queryParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_FACILITY_SEARCH_MAP)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(queryParameter)
                .build()
                .getObjectFlowable(FacilitySearchMapResponse.class);
    }

    @Override
    public Flowable<FacilitySearchResponse> getFacilitySearchListApiCall(FacilitySearchRequest.QueryParameter queryParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_FACILITY_SEARCH)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(queryParameter)
                .build()
                .getObjectFlowable(FacilitySearchResponse.class);
    }

    @Override
    public Flowable<FacilitySearchCodeResponse> getFacilitySearchCodeApiCall(FacilitySearchMapRequest.QueryParameter queryParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_FACILITY_SEARCH)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(queryParameter)
                .build()
                .getObjectFlowable(FacilitySearchCodeResponse.class);
    }

    @Override
    public Flowable<FacilityDetailResponse> getFacilityDetailBaseApiCall(FacilityDetailRequest.PathParameter pathParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_FACILITY_DETAIL)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(FacilityDetailResponse.class);
    }

    @Override
    public Flowable<FacilityDetailMedicalResponse> getFacilityHospitalDetailApiCall(FacilityDetailRequest.PathParameter pathParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_FACILITY_DETAIL)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(FacilityDetailMedicalResponse.class);
    }

    @Override
    public Flowable<FacilityDetailEntranceResponse> getFacilityHomecareDetailApiCall(FacilityDetailRequest.PathParameter pathParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_FACILITY_DETAIL)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(FacilityDetailEntranceResponse.class);
    }

    @Override
    public Flowable<JSONObject> getFacilityCareCoordiListApiCall(CareCoordiRequest.QueryParameter queryParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_FACILITY_CARE_COORDI_LIST)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addQueryParameter(queryParameter)
                .build()
                .getJSONObjectFlowable();
    }
}
