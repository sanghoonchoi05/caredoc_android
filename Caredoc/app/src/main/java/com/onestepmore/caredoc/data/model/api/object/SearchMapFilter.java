package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchMapFilter extends SearchFilter{
    @Expose
    @SerializedName("searchMode")
    private String searchMode;

    @Expose
    @SerializedName("sw")
    private String sw;

    @Expose
    @SerializedName("ne")
    private String ne;

    @Expose
    @SerializedName("zoom")
    private int zoom;

    public String getSw() {
        return sw;
    }

    public void setSw(String sw) {
        this.sw = sw;
    }

    public String getNe() {
        return ne;
    }

    public void setNe(String ne) {
        this.ne = ne;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public String getSearchMode() {
        return searchMode;
    }

    public void setSearchMode(String searchMode) {
        this.searchMode = searchMode;
    }

    public static class SearchMapFilterBuilder extends SearchMapFilter {
        public SearchMapFilterBuilder addSearchMode(String searchMode){
            setSearchMode(searchMode);
            return this;
        }

        public SearchMapFilterBuilder addZoom(int zoom){
            setZoom(zoom);
            return this;
        }

        public SearchMapFilterBuilder addSwNe(String sw, String ne){
            setNe(ne);
            setSw(sw);
            return this;
        }

        public SearchMapFilterBuilder addServices(List<Service> services){
            setServices(services);
            return this;
        }

        public SearchMapFilterBuilder addAddressCode(String addressCode){
            setAddressCode(addressCode);
            return this;
        }

        public SearchMapFilterBuilder addRating(int rating){
            setRating(rating);
            return this;
        }


        public SearchMapFilter build(){
            SearchMapFilter filter = new SearchMapFilter();
            filter.setSearchMode(getSearchMode());
            filter.setAddressCode(getAddressCode());
            filter.setNe(getNe());
            filter.setSw(getSw());
            filter.setZoom(getZoom());
            filter.setServices(getServices());
            return filter;
        }
    }
}
