package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class AggregatesObject {
    @Expose
    @SerializedName("expenses_avg")
    private int expensesAvg;

    @Expose
    @SerializedName("facility_total")
    private int facilityTotal;

    public int getFacilityTotal() {
        return facilityTotal;
    }

    public int getExpensesAvg() {
        return expensesAvg;
    }

    public void setExpensesAvg(int expensesAvg) {
        this.expensesAvg = expensesAvg;
    }
}
