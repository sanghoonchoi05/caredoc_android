package com.onestepmore.caredoc.ui.main.taker.add.disease;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerDiseaseFragmentProvider {
    @ContributesAndroidInjector(modules = TakerDiseaseFragmentModule.class)
    abstract TakerDiseaseFragment provideTakerDiseaseFragmentFactory();
}
