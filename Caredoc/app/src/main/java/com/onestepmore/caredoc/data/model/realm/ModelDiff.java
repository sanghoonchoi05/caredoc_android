package com.onestepmore.caredoc.data.model.realm;

public interface ModelDiff {
    boolean isModelSame(Object obj);
    boolean isModelContentsSame(Object obj);

}
