package com.onestepmore.caredoc.ui.main.detail;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.onestepmore.caredoc.R;

import java.util.List;

public class LtiFeeSpinnerAdapter extends BaseAdapter {
    List<String> data;
    LayoutInflater inflater;


    public LtiFeeSpinnerAdapter(Context context, List<String> data) {
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if (data != null) return data.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.i_lti_fee_spinner_normal, parent, false);
        }

        if (data != null) {
            ((AppCompatTextView) convertView.findViewById(R.id.i_lti_fee_spinner_text)).setText(data.get(position));
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.i_lti_fee_spinner_dropdown, parent, false);
        }

        ((AppCompatTextView) convertView.findViewById(R.id.i_lti_fee_spinner_dropdown_text)).setText(data.get(position));

        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
