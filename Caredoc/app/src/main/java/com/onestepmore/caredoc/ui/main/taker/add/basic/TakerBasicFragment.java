package com.onestepmore.caredoc.ui.main.taker.add.basic;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxRadioGroup;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTypes;
import com.onestepmore.caredoc.databinding.FTakerBasicBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.address.AddressMainActivity;
import com.onestepmore.caredoc.ui.common.AddressClickListener;
import com.onestepmore.caredoc.ui.common.BirthClickListener;
import com.onestepmore.caredoc.ui.common.TextLayoutClickListener;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.BaseAddTakerFragment;
import com.onestepmore.caredoc.ui.main.taker.add.NameDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialogClickListener;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.DateUtils;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Address;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.onestepmore.caredoc.ui.address.AddressMainActivity.ADDRESS_RESULT_CODE;
import static com.onestepmore.caredoc.utils.DateUtils.SUMMARY_DATE_TIME_FORMAT;

public class TakerBasicFragment extends BaseAddTakerFragment<FTakerBasicBinding, TakerBasicViewModel> implements
        TakerBasicNavigator,
        BirthClickListener,
        AddressClickListener,
        BottomSheetDialogClickListener,
        NameDialog.OnNameInputListener
{
    public static TakerBasicFragment newInstance(int titleResId, boolean isModify){
        TakerBasicFragment fragment = new TakerBasicFragment();
        fragment.setArguments(getBundle(titleResId, isModify));

        return  fragment;
    }

    private static final String BOTTOM_SHEET_DIALOG_RELATION_TAG = "BOTTOM_SHEET_DIALOG_RELATION_TAG";
    private static final String BOTTOM_SHEET_DIALOG_RELIGION_TAG = "BOTTOM_SHEET_DIALOG_RELIGION_TAG";
    private static final String BOTTOM_SHEET_DIALOG_YEAR_TAG = "BOTTOM_SHEET_DIALOG_YEAR_TAG";
    private static final String BOTTOM_SHEET_DIALOG_MONTH_TAG = "BOTTOM_SHEET_DIALOG_MONTH_TAG";
    private static final String BOTTOM_SHEET_DIALOG_DAY_TAG = "BOTTOM_SHEET_DIALOG_DAY_TAG";
    private static final String BOTTOM_SHEET_DIALOG_BLOOD_TAG = "BOTTOM_SHEET_DIALOG_BLOOD_TAG";
    private static final String BOTTOM_SHEET_DIALOG_LIVING_TAG = "BOTTOM_SHEET_DIALOG_LIVING_TAG";

    public final static int ADDRESS_REQUEST_CODE = 101;

    @Inject
    @Named("TakerBasicFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerBasicViewModel mTakerBasicViewModel;

    private AddTakerRequest.Basic mCachedBasic;
    private File mCachedFile;
    private Calendar mBirth = null;
    private StaticAddress mCachedStaticAddress;

    private int mDefaultYear = 1945;
    private int mDefaultMonth = 0;
    private int mDefaultDate = 1;

    ImagePicker mImagePicker;
    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_basic;
    }

    @Override
    public TakerBasicViewModel getViewModel() {
        mTakerBasicViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerBasicViewModel.class);
        return mTakerBasicViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerBasicViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            setModify(getArguments().getBoolean("isModify"));
        }

        mCachedBasic = ((AddTakerActivity)getBaseActivity()).getCachedBasic();

        initTitle(getViewDataBinding().fTakerBasicTitle);
        initBirth();
        refreshData();

        initListener();
        setTextObserver();
    }

    private void initBirth(){
        // 생년월일 초기화
        if(mBirth == null){
            mBirth = Calendar.getInstance();
        }
        mBirth.set(mDefaultYear, mDefaultMonth, mDefaultDate);
        if(mCachedBasic.getBirth() != null){
            mBirth.setTime(DateUtils.getDate(mCachedBasic.getBirth(), SUMMARY_DATE_TIME_FORMAT));
        }
    }

    private void refreshData(){
        mCachedBasic.setBirth(DateUtils.getSummaryDateTimeFormat(mBirth));
        mCachedBasic.setBirthCalendar(mBirth);
        getViewDataBinding().setFile(mCachedFile);
        getViewDataBinding().setBasic(mCachedBasic);
        getViewDataBinding().setIsModify(isModify());
        getViewDataBinding().fTakerBasicBlhsrGroup.check(mCachedBasic.isBlhsr() ? R.id.f_taker_basic_blhsr_yes : R.id.f_taker_basic_blhsr_no);
    }

    private void initListener(){
        getViewDataBinding().setBirthListener(this);
        getViewDataBinding().setAddressListener(this);

        getViewDataBinding().fTakerBasicRelationNameLayout.setListener(new TextLayoutClickListener() {
            @Override
            public void onClickFirstLayout() {
                String [] textList = getStringArray(StaticTypes.TYPE_ENUM.RELATION.toString());
                if(textList != null){
                    showBottomSheetDialog(textList, BOTTOM_SHEET_DIALOG_RELATION_TAG);
                }
            }

            @Override
            public void onClickSecondLayout() {
                NameDialog dialog = NameDialog.newInstance(mCachedBasic.getName());
                dialog.setOnNameInputListener(TakerBasicFragment.this::OnNameOk);
                dialog.show(getChildFragmentManager(), NameDialog.class.getName());
            }
        });

        getViewDataBinding().fTakerBasicReligionBloodLayout.setListener(new TextLayoutClickListener() {
            @Override
            public void onClickFirstLayout() {
                String [] textList = getStringArray(StaticTypes.TYPE_ENUM.RELIGION.toString());
                if(textList != null){
                    showBottomSheetDialog(textList, BOTTOM_SHEET_DIALOG_RELIGION_TAG);
                }
            }

            @Override
            public void onClickSecondLayout() {
                String [] textList = getStringArray(StaticTypes.TYPE_ENUM.BLOOD_TYPE.toString());
                if(textList != null){
                    showBottomSheetDialog(textList, BOTTOM_SHEET_DIALOG_BLOOD_TAG);
                }
            }
        });

        getViewDataBinding().fTakerBasicLiveWithLayout.setListener(new TextLayoutClickListener() {
            @Override
            public void onClickFirstLayout() {
                String [] textList = getStringArray(StaticTypes.TYPE_ENUM.LIVING.toString());
                if(textList != null){
                    showBottomSheetDialog(textList, BOTTOM_SHEET_DIALOG_LIVING_TAG);
                }
            }

            @Override
            public void onClickSecondLayout() {

            }
        });
    }

    private String [] getStringArray(String category){
        List<StaticTypes> typesList =  getViewModel().getRealm().where(StaticTypes.class)
                .equalTo("category", category)
                .findAll();

        if(typesList.size() > 0){
            String [] textList = new String[typesList.size()];
            for(int i=0; i<typesList.size(); i++){
                textList[i] = typesList.get(i).getTypeName();
            }

            return textList;
        }

        return null;
    }

    @Override
    public void onShowYearList() {
        onBirthBottomSheetDialog(0);
    }

    @Override
    public void onShowMonthList() {
        onBirthBottomSheetDialog(1);
    }

    @Override
    public void onShowDateList() {
        onBirthBottomSheetDialog(2);
    }

    public void onBirthBottomSheetDialog(int index) {
        int min = 0;
        int max = 0;
        String tag = "";
        if(index == 0){
            min = 1900;
            max = 1970;
            tag = BOTTOM_SHEET_DIALOG_YEAR_TAG;
        }
        else if(index == 1){
            min = 1;
            max = 12;
            tag = BOTTOM_SHEET_DIALOG_MONTH_TAG;
        }
        else if(index == 2){
            min = 1;
            max = 31;
            tag = BOTTOM_SHEET_DIALOG_DAY_TAG;
        }

        int count = max - min + 1;
        String [] textList = new String[count];
        for(int i=0; i<count; i++){
            textList[i] = String.valueOf(min + i);
        }

        BottomSheetDialog dialog = BottomSheetDialog.getInstance(textList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), tag);
    }

    @Override
    public void onShowAddressView(int priority) {
        Intent intent = getBaseActivity().newIntent(AddressMainActivity.class);
        intent.putExtra("needDetail", false);
        getBaseActivity().startActivityForResult(intent, ADDRESS_REQUEST_CODE, this);
    }

    @Override
    public void onDeleteAddress(int priority) {
        mCachedStaticAddress = null;
        mCachedBasic.setAddress(null);

        refreshData();
    }

    @Override
    public void registerPicture() {
        if(getBaseActivity() instanceof AddTakerActivity){
            mImagePicker = new ImagePicker(
                    getBaseActivity(),
                    null,
                    imageUri -> {
                        mCachedFile = mImagePicker.getImageFile();
                        ((AddTakerActivity)getBaseActivity()).setCachedProfileImageFile(mCachedFile);
                        refreshData();
                    });
            mImagePicker.setWithImageCrop(1, 1);
            ((AddTakerActivity)getBaseActivity()).showTakerPhoto(mImagePicker);
        }
    }

    public void showBottomSheetDialog(String [] textList, String tag){
        BottomSheetDialog dialog = BottomSheetDialog.getInstance(textList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), tag);
    }

    @Override
    public void onClick(String tag, String text) {
        setTextWithTag(tag, text);
        refreshData();
    }

    private void setTextWithTag(String tag, String text){
        AddTakerActivity addTakerActivity = (AddTakerActivity)getBaseActivity();
        if(addTakerActivity == null){
            return;
        }

        if(tag.equals(BOTTOM_SHEET_DIALOG_RELATION_TAG)){
            mCachedBasic.setRelation(addTakerActivity.getTypeCode(text, StaticTypes.TYPE_ENUM.RELATION));
            mCachedBasic.setRelationStr(text);
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_RELIGION_TAG)){
            mCachedBasic.setReligion(addTakerActivity.getTypeCode(text, StaticTypes.TYPE_ENUM.RELIGION));
            mCachedBasic.setReligionStr(text);
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_YEAR_TAG)){
            mBirth.set(Calendar.YEAR, getBirthNumber(BOTTOM_SHEET_DIALOG_YEAR_TAG, text));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_MONTH_TAG)){
            mBirth.set(Calendar.MONTH, getBirthNumber(BOTTOM_SHEET_DIALOG_MONTH_TAG, text));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_DAY_TAG)){
            mBirth.set(Calendar.DAY_OF_MONTH, getBirthNumber(BOTTOM_SHEET_DIALOG_DAY_TAG, text));
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_BLOOD_TAG)){
            mCachedBasic.setBloodType(text);
        }
        else if(tag.equals(BOTTOM_SHEET_DIALOG_LIVING_TAG)){
            mCachedBasic.setLiveWith(addTakerActivity.getTypeCode(text, StaticTypes.TYPE_ENUM.LIVING));
            mCachedBasic.setLiveWithStr(text);
        }
    }

    @Override
    public void onClose(String tag) {

    }

    @Override
    public void onInit(String tag) {
        setTextWithTag(tag, null);
        refreshData();
    }

    @Override
    public void OnNameOk(String name) {
        mCachedBasic.setName(name);
        refreshData();
    }

    private void setTextObserver(){
        rx.Observable<Integer> blhsrObserver = RxRadioGroup.checkedChanges(getViewDataBinding().fTakerBasicBlhsrGroup);
        getViewModel().addSubscription(blhsrObserver.subscribe((id) -> {
            mCachedBasic.setBlhsr(id == R.id.f_taker_basic_blhsr_yes);
        }));
    }

    private int getBirthNumber(String tag, String text){
        int number = 0;
        if(!CommonUtils.checkNullAndEmpty(text)){
            number = Integer.valueOf(text);
        }

        switch (tag){
            case BOTTOM_SHEET_DIALOG_YEAR_TAG:
                if(number == 0){
                    number = mDefaultYear;
                }
                break;
            case BOTTOM_SHEET_DIALOG_MONTH_TAG:
                break;
            case BOTTOM_SHEET_DIALOG_DAY_TAG:
                if(number == 0){
                    number = mDefaultDate;
                }
                break;
            default:
                break;
        }

        return number;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADDRESS_REQUEST_CODE){
            if(resultCode == ADDRESS_RESULT_CODE){
                if(data == null){
                    return;
                }

                JusoRealmModel juso = data.getParcelableExtra("juso");

                String addressName = juso.getJibunAddress();
                if(CommonUtils.checkNullAndEmpty(addressName)){
                    StringBuilder roadName = new StringBuilder();
                    roadName.append(juso.getRoadAddress());
                    if(!CommonUtils.checkNullAndEmpty(juso.getBuildingName())){
                        roadName.append(" ");
                        roadName.append(juso.getBuildingName());
                    }
                    addressName = roadName.toString();
                }

                Address address = new Address();
                address.setName(addressName);
                address.setCode(juso.getId());
                mCachedBasic.setAddress(address);

                refreshData();
            }
        }
    }
}
