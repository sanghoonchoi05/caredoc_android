package com.onestepmore.caredoc.ui.account.login;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginFragmentModule {
    @Provides
    LoginViewModel loginViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new LoginViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("LoginFragment")
    ViewModelProvider.Factory provideAccountViewModelProvider(LoginViewModel loginViewModel) {
        return new ViewModelProviderFactory<>(loginViewModel);
    }
}
