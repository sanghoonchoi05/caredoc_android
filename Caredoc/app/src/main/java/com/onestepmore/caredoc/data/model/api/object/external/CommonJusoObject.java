package com.onestepmore.caredoc.data.model.api.object.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonJusoObject extends CommonObject{
    @Expose
    @SerializedName("countPerPage")
    public String countPerPage;

    @Expose
    @SerializedName("currentPage")
    public String currentPage;

    public String getCountPerPage() {
        return countPerPage;
    }

    public String getCurrentPage() {
        return currentPage;
    }
}
