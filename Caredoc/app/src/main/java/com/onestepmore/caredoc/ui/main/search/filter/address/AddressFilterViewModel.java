package com.onestepmore.caredoc.ui.main.search.filter.address;

import android.app.Application;

import com.google.gson.Gson;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import java.util.List;

public class AddressFilterViewModel extends BaseViewModel<AddressFilterNavigator> {
    private final Gson mGson;

    public AddressFilterViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider, Gson gson) {
        super(application, appDataSource, schedulerProvider);

        mGson = gson;
    }

    public void loadAddressDataFromServer(String addressName, String addressFullName, StaticAddress staticAddress) {
        getCompositeDisposable().add(getDataManager()
                .loadStaticAddress(
                        addressName,
                        addressFullName,
                        staticAddress.getSidoCode(),
                        staticAddress.getGugunCode(),
                        staticAddress.getLatLng().getLatitude(),
                        staticAddress.getLatLng().getLongitude())
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    if(aBoolean){
                        getNavigator().onLoadComplete();
                    }
                        },
                        throwable -> getDataManager().handleThrowable(throwable, getNavigator())
                ));
    }

    public void onClose(){
        getNavigator().onClose();
    }

    public void onInit(){
        getNavigator().onInit();
    }

    public void onApply(){
        getNavigator().onApplyFilter();
    }

    public void onAddressTextClick(int position){
        getNavigator().onAddressTextClick(position);
    }
}
