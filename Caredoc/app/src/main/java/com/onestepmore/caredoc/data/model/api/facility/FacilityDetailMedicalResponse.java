package com.onestepmore.caredoc.data.model.api.facility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.FacilityDetailObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceMedicalObject;
import com.onestepmore.caredoc.data.model.api.object.ServicesObject;

public class FacilityDetailMedicalResponse extends FacilityDetailObject {
    @Expose
    @SerializedName("services")
    private ServicesObject<ServiceMedicalObject> services;

    public ServicesObject<ServiceMedicalObject> getServices() {
        return services;
    }
}
