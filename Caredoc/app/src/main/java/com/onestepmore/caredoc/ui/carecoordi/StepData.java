package com.onestepmore.caredoc.ui.carecoordi;

public class StepData{
    private CareCoordiActivity.STEP step;
    private CareCoordiActivity.UI_ITEM uiItem;

    public CareCoordiActivity.STEP getStep() {
        return step;
    }

    public void setStep(CareCoordiActivity.STEP step) {
        this.step = step;
    }

    public CareCoordiActivity.UI_ITEM getUIItem() {
        return uiItem;
    }

    public void setUIItem(CareCoordiActivity.UI_ITEM uiItem) {
        this.uiItem = uiItem;
    }
}
