package com.onestepmore.caredoc.ui.carecoordi.service;

import android.app.Application;
import android.databinding.ObservableField;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class ServiceViewModel extends BaseViewModel<ServiceNavigator> {

    public ObservableField<String> nextText = new ObservableField<>();

    public ServiceViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onNextButtonClick(){
    }
}
