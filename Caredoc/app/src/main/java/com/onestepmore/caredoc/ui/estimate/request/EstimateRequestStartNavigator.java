package com.onestepmore.caredoc.ui.estimate.request;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface EstimateRequestStartNavigator extends BaseNavigator {
    void onClose();
    void onStartEstimateRequest();
}
