package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CareCoordiSearchFilter {
    public enum CATEGORY{
        NURSING_HOME("nursing-home"),
        VISIT_HOME("visit-home"),
        GERIATRIC_HOSPITAL("geriatric-hospital");

        private String name;
        CATEGORY(String name){
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
    @Expose
    @SerializedName("takerInfo")
    private TakerInfo takerInfo;

    @Expose
    @SerializedName("category")
    private String category;

    @Expose
    @SerializedName("services")
    private List<Service> services;

    @Expose
    @SerializedName("classifications")
    private Classification classification;

    public static class TakerInfo{
        @Expose
        @SerializedName("birth")
        private String birth;

        @Expose
        @SerializedName("lti")
        private boolean lti;

        private Calendar birthCalendar;
        private String year;
        private String month;
        private String day;
        private String addressName;

        public TakerInfo(String birth){
            this.birth = birth;
            this.lti = false;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public boolean isLti() {
            return lti;
        }

        public void setLti(boolean lti) {
            this.lti = lti;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public Calendar getBirthCalendar() {
            return birthCalendar;
        }

        public void setBirthCalendar(Calendar birthCalendar) {
            this.birthCalendar = birthCalendar;
            year = String.valueOf(birthCalendar.get(Calendar.YEAR));
            month = String.valueOf(birthCalendar.get(Calendar.MONTH) + 1);
            day = String.valueOf(birthCalendar.get(Calendar.DAY_OF_MONTH));
        }

        public String getAddressName() {
            return addressName;
        }

        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }
    }

    public static class Classification{
        @Expose
        @SerializedName("diseases")
        private List<String> diseases;
        private List<String> diseasesName;
        @Expose
        @SerializedName("welfareTools")
        private List<String> welfareTools;
        private List<String> welfareToolsName;

        public Classification(){
            diseases = new ArrayList<>();
            welfareTools = new ArrayList<>();
            diseasesName = new ArrayList<>();
            welfareToolsName = new ArrayList<>();
        }

        public void setDiseases(List<String> diseases) {
            this.diseases = diseases;
        }

        public List<String> getDiseases() {
            return diseases;
        }

        public List<String> getWelfareTools() {
            return welfareTools;
        }

        public List<String> getDiseasesName() {
            return diseasesName;
        }

        public List<String> getWelfareToolsName() {
            return welfareToolsName;
        }

        public void setWelfareTools(List<String> welfareTools) {
            this.welfareTools = welfareTools;
        }
    }

    public static class Service{
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("grades")
        private List<String> grades;

        public Service(){ }

        public Service(int id){
            this.id = id;
        }
        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public List<String> getGrades() {
            return grades;
        }

        public void setGrades(List<String> grades) {
            this.grades = grades;
        }
    }

    public TakerInfo getTakerInfo() {
        return takerInfo;
    }

    public void setTakerInfo(TakerInfo takerInfo) {
        this.takerInfo = takerInfo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public static class FilterBuilder extends CareCoordiSearchFilter{
        public FilterBuilder addTakerInfo(TakerInfo takerInfo){
            setTakerInfo(takerInfo);
            return this;
        }

        public FilterBuilder addCategory(String category){
            setCategory(category);
            return this;
        }

        public FilterBuilder addServices(List<Service> services){
            setServices(services);
            return this;
        }

        public FilterBuilder addClassifications(Classification classification){
            setClassification(classification);
            return this;
        }

        public CareCoordiSearchFilter build(){
            CareCoordiSearchFilter filter = new CareCoordiSearchFilter();
            filter.setTakerInfo(getTakerInfo());
            filter.setCategory(getCategory());
            filter.setServices(getServices());
            filter.setClassification(getClassification());
            return filter;
        }
    }
}
