package com.onestepmore.caredoc.ui.carecoordi.result;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiRequest;
import com.onestepmore.caredoc.data.model.realm.FacilityCareCoordiList;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.databinding.FCareCoordiResultBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.detail.FacilityActivity;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialogClickListener;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UnitUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmResults;

import static com.onestepmore.caredoc.ui.main.detail.FacilityActivity.EXTRA_FACILITY;

public class ResultFragment extends BaseCoordiFragment<FCareCoordiResultBinding, ResultViewModel> implements
        ResultNavigator,
        NestedScrollView.OnScrollChangeListener,
        BottomSheetDialogClickListener,
        BaseFragment.OnBackPressedListener
{
    public static final String SERVICE_NAME_KEY = "service_name_key";
    public static final String CATEGORY_NAME_KEY = "category_name_key";
    public static final String CLASSIFICATION_LIST_KEY = "classification_list_key";
    public static final String TOTAL_COUNT_KEY = "total_count_key";
    public static final String PRICE_KEY = "price_key";
    public static final String ORDER_LIST_KEY = "order_list_key";

    public static ResultFragment newInstance(
            String serviceName,
            String categoryName,
            List<String> classificationList,
            int totalCount,
            int price,
            List<String> orderList) {

        Bundle args = new Bundle();
        ResultFragment fragment = new ResultFragment();
        args.putString(SERVICE_NAME_KEY, serviceName);
        args.putString(CATEGORY_NAME_KEY, categoryName);
        args.putStringArrayList(CLASSIFICATION_LIST_KEY, new ArrayList<>(classificationList));
        args.putInt(TOTAL_COUNT_KEY, totalCount);
        args.putInt(PRICE_KEY, price);
        args.putStringArrayList(ORDER_LIST_KEY, new ArrayList<>(orderList));
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    @Named("ResultFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private ResultViewModel mResultViewModel;

    private RealmResults<FacilityCareCoordiList> mFacilityCareCoordiList;
    private int mTotalCount = 0;
    private String [] mOrderList;
    private int mNestedScrollViewY = 0;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_care_coordi_result;
    }

    @Override
    public ResultViewModel getViewModel() {
        mResultViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ResultViewModel.class);
        return mResultViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getViewModel().setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null){
            String serviceName = bundle.getString(SERVICE_NAME_KEY);
            String categoryName = bundle.getString(CATEGORY_NAME_KEY);
            int strId = R.string.care_coordi_result_title_top2;
            if(CommonUtils.isIncludeLastConsonant(serviceName.charAt(serviceName.length() - 1))){
                strId = R.string.care_coordi_result_title_top1;
            }

            getViewDataBinding().fCareCoordiResultTitleTop.setText(serviceName);
            getViewDataBinding().fCareCoordiResultTitleBottom.setText(categoryName);
            getViewDataBinding().fCareCoordiResultTitleTopDesc.setText(strId);
            /*CharSequence title = CommonUtils.getText(getBaseActivity(), strId, serviceName, categoryName);
            UIUtils.setTextIncludeAnnotation(getBaseActivity(), title, getViewDataBinding().fCareCoordiResultTitle);*/

            StringBuilder classificationStr = new StringBuilder();
            List<String> classificationList = bundle.getStringArrayList(CLASSIFICATION_LIST_KEY);
            if(classificationList != null && classificationList.size() > 0){
                for(int i=0; i<classificationList.size(); i++){
                    String classification = classificationList.get(i);
                    classificationStr.append(classification);

                    if(classificationList.size()-1 != i){
                        classificationStr.append(", ");
                    }
                }
            }

            getViewDataBinding().setDesc(classificationStr.toString());

            int price = bundle.getInt(PRICE_KEY);
            String priceStr = "";
            if(price > 0){
                price = price * 30;
                priceStr = String.format(getString(R.string.common_won), UnitUtils.thousandComma(price));
            }
            getViewDataBinding().setPrice(priceStr);

            mTotalCount = bundle.getInt(TOTAL_COUNT_KEY);
            getViewDataBinding().setTotalCount(getString(R.string.common_num_formatted, mTotalCount));

            List<String> orderList = bundle.getStringArrayList(ORDER_LIST_KEY);
            if(orderList.size() > 0){
                mOrderList = new String[orderList.size()];
                for(int i=0; i<mOrderList.length; i++){
                    mOrderList[i] = orderList.get(i);
                }
                getViewDataBinding().fCareCoordiResultOrderText.setText(mOrderList[0]);
            }
        }

        // 시설 검색 리스트 Realm Change Observer 추가
        mFacilityCareCoordiList = getViewModel().getRealm().where(FacilityCareCoordiList.class).findAll();
        mFacilityCareCoordiList.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<FacilityCareCoordiList>>() {
            @Override
            public void onChange(RealmResults<FacilityCareCoordiList> results, OrderedCollectionChangeSet changeSet) {
                AppLogger.i(getClass(), "ResultFragment realm Facility size: " + results.size());
                getViewModel().setEmpty(results.size() == 0);
                if(mTotalCount == results.size()){
                    getViewModel().setNoMoreList(true);
                }
            }
        });

        getViewModel().setEmpty(mFacilityCareCoordiList.size() == 0);
        if(mTotalCount == mFacilityCareCoordiList.size()){
            getViewModel().setNoMoreList(true);
        }

        ResultAdapter resultAdapter = new ResultAdapter(mFacilityCareCoordiList, getBaseActivity());
        resultAdapter.setResultNavigator(this);
        getViewDataBinding().fCareCoordiResultRecyclerView.setAdapter(resultAdapter);
        getViewDataBinding().fCareCoordiResultRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(),new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_facility));
        getViewDataBinding().fCareCoordiResultRecyclerView.addItemDecoration(dividerItemDecoration);
        getViewDataBinding().fCareCoordiResultScrollView.setOnScrollChangeListener(this);
    }

    @Override
    public void setTitle(int titleResId) {

    }

    @Override
    public void setStepData(StepData stepData) {
        if(stepData == null){
            return;
        }
    }

    @Override
    public void onFacilityDetailClick(FacilityRealmModel facility) {
        Intent intent = getBaseActivity().newIntent(FacilityActivity.class);
        intent.putExtra(EXTRA_FACILITY, facility);
        startActivity(intent);
    }

    @Override
    public void onCareCoordiRetryButtonClick() {
        BackStackManager.getInstance().onBackPressed();
        getBaseActivity().refreshActivity();
    }

    @Override
    public void onOrderButtonClick() {
        BottomSheetDialog dialog = BottomSheetDialog.getInstance(mOrderList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), "order");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mFacilityCareCoordiList!=null){
            mFacilityCareCoordiList.removeAllChangeListeners();
        }
    }

    @Override
    public void onScrollChange(NestedScrollView nestedScrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        mNestedScrollViewY = scrollY;

        if (!nestedScrollView.canScrollVertically(1)) {
            AppLogger.i(getClass(), "리스트 끝에 도착");
            getViewDataBinding().fCareCoordiResultRecyclerView.stopScroll();
            getViewDataBinding().fCareCoordiResultRecyclerView.stopNestedScroll();

            ((CareCoordiActivity)getBaseActivity()).loadCareCoordiResult();
        }
    }

    @Override
    public void onClick(String tag, String text) {
        if(text.equals(getViewDataBinding().fCareCoordiResultOrderText.getText().toString())){
            return;
        }

        getViewDataBinding().fCareCoordiResultOrderText.setText(text);
        loadOrderBy(text);
    }

    @Override
    public void onClose(String tag) {

    }

    @Override
    public void onInit(String tag) {
        if(mOrderList.length > 0){
            if(mOrderList[0].equals(getViewDataBinding().fCareCoordiResultOrderText.getText().toString())){
                return;
            }

            getViewDataBinding().fCareCoordiResultOrderText.setText(mOrderList[0]);
            loadOrderBy(mOrderList[0]);
        }
    }

    private void loadOrderBy(String orderByText){
        if(!isScrollTop()){
            scrollUpToTop();
        }

        for(CareCoordiRequest.ORDER_BY orderBy : CareCoordiRequest.ORDER_BY.values()){
            if(getString(orderBy.getStrId()).equals(orderByText)){
                ((CareCoordiActivity)getBaseActivity()).getViewModel().setOrderBy(orderBy);
                ((CareCoordiActivity)getBaseActivity()).loadCareCoordiResult(0);
                break;
            }
        }
    }

    public void scrollUpToTop() {
        getViewDataBinding().fCareCoordiResultScrollView.setScrollY(0);
    }

    public boolean isScrollTop() {
        return mNestedScrollViewY == 0;
    }

    @Override
    public void onExit(){
        showCommonDialog(
                getString(R.string.care_coordi_exit_dialog_title),
                getString(R.string.care_coordi_exit_dialog_desc),
                CommonYNDialog.class.getName(),
                (view) -> {
                    BackStackManager.getInstance().onBackPressed();
                    getBaseActivity().finish();
                },
                getString(R.string.common_return),
                getString(R.string.common_exit)
        );
    }

    @Override
    public void doBack() {
        if(!isScrollTop()){
            scrollUpToTop();
        }else{
            onExit();
        }
    }
}
