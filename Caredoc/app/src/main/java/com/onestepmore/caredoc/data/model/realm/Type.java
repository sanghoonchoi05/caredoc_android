package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.ApiCommonObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Type extends RealmObject implements Parcelable {
    @PrimaryKey
    private String typeCode;
    private String categoryCode;
    private String typeName;

    public Type(){}

    protected Type(Parcel in) {
        typeCode = in.readString();
        categoryCode = in.readString();
        typeName = in.readString();
    }

    public static final Creator<Type> CREATOR = new Creator<Type>() {
        @Override
        public Type createFromParcel(Parcel in) {
            return new Type(in);
        }

        @Override
        public Type[] newArray(int size) {
            return new Type[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeCode);
        dest.writeString(categoryCode);
        dest.writeString(typeName);
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setType(ApiCommonObject.CategoryType categoryType){
        this.typeCode = categoryType.getTypeCode();
        this.categoryCode = categoryType.getCategoryCode();
        this.typeName = categoryType.getTypeName();
    }
}
