package com.onestepmore.caredoc.ui.main.profile.collaboration;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CollaborationFragmentProvider {
    @ContributesAndroidInjector(modules =CollaborationFragmentModule.class)
    abstract CollaborationFragment provideCollaborationFragmentFactory();
}
