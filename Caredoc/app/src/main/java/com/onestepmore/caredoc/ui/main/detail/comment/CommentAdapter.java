package com.onestepmore.caredoc.ui.main.detail.comment;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Review;
import com.onestepmore.caredoc.databinding.ICommentBinding;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class CommentAdapter extends RealmRecyclerViewAdapter<Review, CommentAdapter.CommentViewHolder> {

    private RemoveCommentListener mRemoveListener;

    public CommentAdapter(OrderedRealmCollection<Review> data, RemoveCommentListener listener) {
        super(data, true);
        mRemoveListener = listener;
    }

    @NonNull
    @Override
    public CommentAdapter.CommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ICommentBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_comment, viewGroup, false);
        return new CommentAdapter.CommentViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.CommentViewHolder reviewViewHolder, int position) {
        OrderedRealmCollection data = getData();
        if (data != null) {
            final Review review = getData().get(position);
            reviewViewHolder.binding.iCommentContentLayout.setSelected(review.isOwn());
            reviewViewHolder.binding.setListener(mRemoveListener);
            reviewViewHolder.binding.setReview(review);
            reviewViewHolder.binding.executePendingBindings();
        }
    }

    static class CommentViewHolder extends RecyclerView.ViewHolder {
        final ICommentBinding binding;

        private CommentViewHolder(ICommentBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
