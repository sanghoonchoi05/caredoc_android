package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.reflect.TypeToken;
import com.onestepmore.caredoc.data.model.api.common.AddressRequest;
import com.onestepmore.caredoc.data.model.api.CommonResponse_Deprecated;
import com.onestepmore.caredoc.data.model.api.common.AddressResponse;
import com.onestepmore.caredoc.data.model.api.common.SuggestionRequest;
import com.onestepmore.caredoc.data.model.api.common.SuggestionResponse;
import com.onestepmore.caredoc.data.model.api.statics.DiseaseResponse;
import com.onestepmore.caredoc.data.model.api.statics.StaticDataResponse;
import com.onestepmore.caredoc.data.model.api.account.LoginRequest;
import com.onestepmore.caredoc.data.model.api.account.LoginResponse;
import com.onestepmore.caredoc.data.model.api.account.PasswordEmailRequest;
import com.onestepmore.caredoc.data.model.api.account.PasswordEmailResponse;
import com.onestepmore.caredoc.data.model.api.account.RegisterRequest;
import com.onestepmore.caredoc.data.model.api.account.RegisterResponse;
import com.onestepmore.caredoc.data.model.api.account.UserResponse;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkFacilityResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalCoordinateRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalCoordinateResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalJusoRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalJusoResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalMailgunRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalMailgunResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalNaverUserInfoResponse;
import com.onestepmore.caredoc.data.model.api.statics.SubjectResponse;
import com.onestepmore.caredoc.data.model.api.taker.MyTakerResponse;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Singleton
public class ApiHelperImp implements ApiHelper {
    private ApiHeader mApiHeader;

    @Inject
    public ApiHelperImp(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public Flowable<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectFlowable(LoginResponse.class);
    }

    @Override
    public Flowable<LoginResponse> doSocialLoginApiCall(LoginRequest.SocialLoginPathParameter pathParameter, JSONObject request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_SOCIAL_LOGIN_PROVIDER)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addJSONObjectBody(request)
                .addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(LoginResponse.class);
    }

    @Override
    public Flowable<UserResponse> doServerUserApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_AUTH_USER)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectFlowable(UserResponse.class);
    }

    @Override
    public Single<JSONObject> doServerLoginApiCallGetJson(LoginRequest.ServerLoginRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGIN)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getJSONObjectSingle();
    }

    @Override
    public Single<RegisterResponse> doServerRegisterApiCall(RegisterRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_REGISTER)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectSingle(RegisterResponse.class);
    }

    @Override
    public void doServerLogoutApiCall(OkHttpResponseListener listener) {
        Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_LOGOUT)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getAsOkHttpResponse(listener);
    }

    @Override
    public Flowable<PasswordEmailResponse> doServerPasswordEmail(PasswordEmailRequest request) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_SERVER_PASSWORD_EMAIL)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addBodyParameter(request)
                .build()
                .getObjectFlowable(PasswordEmailResponse.class);
    }

    @Override
    public Flowable<CommonResponse_Deprecated> doServerCommonApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_COMMON)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectFlowable(CommonResponse_Deprecated.class);
    }

    @Override
    public Flowable<StaticDataResponse> doServerStaticDataApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_STATICS)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectFlowable(StaticDataResponse.class);
    }

    @Override
    public Flowable<DiseaseResponse> doServerDiseaseApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_DISEASE)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectFlowable(DiseaseResponse.class);
    }

    @Override
    public Flowable<SubjectResponse> doServerSubjectApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_SUBJECT)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getObjectFlowable(SubjectResponse.class);
    }

    @Override
    public Flowable<JSONArray> doServerToolApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_TOOL)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .build()
                .getJSONArrayFlowable();
    }

    @Override
    public Flowable<AddressResponse> doServerAddressApiCall(int sidoCode, int gugunCode) {
        AddressRequest.QueryParameter parameter = new AddressRequest.QueryParameter();
        if(sidoCode != 0 || gugunCode != 0){
            parameter.setSidoCode(sidoCode);
            parameter.setGugunCode(gugunCode);
        }
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_ADDRESS_V2)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addQueryParameter(parameter)
                .build()
                .getObjectFlowable(AddressResponse.class);
    }

    @Override
    public Flowable<List<SuggestionResponse>> doServerSuggestionApiCall(String keyword) {
        SuggestionRequest.QueryParameter parameter = new SuggestionRequest.QueryParameter();
        parameter.setKeyword(keyword);
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_SERVER_SUGGESTION)
                .addHeaders(mApiHeader.getPublicApiHeader())
                .addQueryParameter(parameter)
                .build()
                .getParseFlowable(new TypeToken<List<SuggestionResponse>>(){});
    }

    @Override
    public Flowable<BookmarkFacilityResponse> getBookmarkApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_BOOKMARK)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectFlowable(BookmarkFacilityResponse.class);
    }

    @Override
    public Flowable<MyTakerResponse> getMyTakerApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_MY_TAKER)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectFlowable(MyTakerResponse.class);
    }

    @Override
    public Flowable<ExternalJusoResponse> getExternalJusoApiCall(ExternalJusoRequest.QueryParameter queryParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_EXTERNAL_JUSO)
                .addQueryParameter(queryParameter)
                .build()
                .getObjectFlowable(ExternalJusoResponse.class);
    }

    @Override
    public Flowable<ExternalCoordinateResponse> getExternalCoordinateApiCall(ExternalCoordinateRequest.QueryParameter queryParameter) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_EXTERNAL_COORDINATE)
                .addQueryParameter(queryParameter)
                .build()
                .getObjectFlowable(ExternalCoordinateResponse.class);
    }

    @Override
    public Flowable<ExternalNaverUserInfoResponse> getExternalNaverUserInfo(String accessToken) {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_EXTERNAL_GET_NAVER_USER_INFO)
                .addHeaders(new ApiHeader.ExternalProtectedApiHeader(accessToken))
                .build()
                .getObjectFlowable(ExternalNaverUserInfoResponse.class);
    }

    @Override
    public Flowable<ExternalMailgunResponse> sendExternalMailgun(ExternalMailgunRequest.QueryParameter queryParameter) {
        return Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_EXTERNAL_MAILGUN)
                .addHeaders(new ApiHeader.ExternalMailgunProtectedApiHeader())
                .addQueryParameter(queryParameter)
                .build()
                .getObjectFlowable(ExternalMailgunResponse.class);
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }
}
