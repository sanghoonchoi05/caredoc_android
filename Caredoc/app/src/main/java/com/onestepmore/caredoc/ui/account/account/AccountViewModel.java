package com.onestepmore.caredoc.ui.account.account;

import android.app.Application;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.google.gson.Gson;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.account.LoginRequest;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.firebase.FirebaseParam;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.AUTH_CLICK_EMAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.AUTH_CLICK_LOGIN;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.AUTH_COMPLETE_FACEBOOK;

public class AccountViewModel extends BaseViewModel<AccountNavigator> implements FacebookCallback<LoginResult> {

    private Gson mGson;

    public AccountViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                            Gson gson) {
        super(application, appDataSource, schedulerProvider);

        mGson = gson;
    }

    public void onEmailRegisterButtonClick(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.REGISTER_CLICK_EMAIL);
        AmplitudeManager.getInstance().logEvent(AUTH_CLICK_EMAIL);
        getNavigator().showEmailRegisterView();
    }

    public void onFacebookLoginButtonClick(){
        getNavigator().showFacebookView();
    }

    public void onKakaoLoginButtonClick(){

    }

    public void onNaverLoginButtonClick(){
        getNavigator().showNaverView();
    }

    public void getNaverUserInfo(String accessToken){
        getCompositeDisposable().add(getDataManager().getExternalNaverUserInfo(accessToken)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    if(response != null){
                        if(response.getResultsCode().equals("00")){
                            onSocialLogin(
                                    accessToken,
                                    response.getResponse().getId(),
                                    response.getResponse().getName(),
                                    response.getResponse().getEmail(),
                                    response.getResponse().getProfile_image(),
                                    "naver");
                        }
                        else{
                            getNavigator().networkError(response.getMessage());
                        }
                    }

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void onShowLoginView(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.REGISTER_CLICK_LOGIN);
        AmplitudeManager.getInstance().logEvent(AUTH_CLICK_LOGIN);
        getNavigator().showLoginView();
    }

    public void onSocialLogin(AccessToken accessToken, String name, String email, String picUrl, String provider) {
        onSocialLogin(accessToken.getToken(), accessToken.getUserId(), name, email, picUrl, provider);
    }
    public void onSocialLogin(String accessToken, String id, String name, String email, String picUrl, String provider) {
        setIsLoading(true);

        LoginRequest.SocialLoginPathParameter pathParameter = new LoginRequest.SocialLoginPathParameter();
        pathParameter.setProvider(provider);

        LoginRequest.SocialInfoRequest socialInfoRequest = new LoginRequest.SocialInfoRequest();
        socialInfoRequest.setAvatar(picUrl);
        socialInfoRequest.setId(id);
        socialInfoRequest.setToken(accessToken);
        socialInfoRequest.setEmail(email);
        socialInfoRequest.setName(name);

        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(mGson.toJson(socialInfoRequest));

        }catch (JSONException e){
            setIsLoading(false);
            return;
        }

        getCompositeDisposable().add(getDataManager()
                .doSocialLoginApiCall(pathParameter, jsonObject)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    getDataManager().updateAccessToken(response.getToken(), response.getExpiresIn(), response.getTokenType());
                    getUserInfo(email, "", provider, id, accessToken);

                    firebaseRegisterLogEvent(provider);
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private void firebaseRegisterLogEvent(String provider){
        String situationEventName = "";
        String paramValue = "";
        if(provider.equals("facebook")){
            AmplitudeManager.getInstance().logEvent(AUTH_COMPLETE_FACEBOOK);
            situationEventName = FirebaseEvent.Situation.REGISTER_FACEBOOK_COMPLETE;
            paramValue = FirebaseParam.Value.FACEBOOK;
        }else if(provider.equals("naver")){
            situationEventName = FirebaseEvent.Situation.REGISTER_NAVER_COMPLETE;
            paramValue = FirebaseParam.Value.NAVER;
        }

        if(!situationEventName.isEmpty()){
            FirebaseManager.getInstance().logEvent(situationEventName);
            FirebaseManager.getInstance().logEvent(
                    FirebaseEvent.PredefinedEvent.SIGN_UP_COMPLETE,
                    new FirebaseManager.Builder().addParam(FirebaseParam.PredefinedKey.METHOD, paramValue).build());
        }
    }

    private void getUserInfo(String email, String password, String provider, String socialId, String socialToken){
        getCompositeDisposable().add(getDataManager()
                .loadUserInfoFromServer(getRealm(), email, password, provider, socialId, socialToken)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    if(response){
                        getNavigator().openMainActivity();
                    }
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        AccessToken accessToken = loginResult.getAccessToken();
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject me, GraphResponse response) {

                        if (response.getError() != null) {
                            getNavigator().networkError(response.getError().getErrorMessage());
                        } else {
                            try {
                                String user_last_name = me.optString("last_name");
                                String user_first_name = me.optString("first_name");
                                String user_email = response.getJSONObject().optString("email");
                                String picUrl = response.getJSONObject().getJSONObject("picture").getJSONObject("data").optString("url");

                                /*Profile profile = Profile.getCurrentProfile();
                                String picUrl = profile.getProfilePictureUri(200, 200).toString();*/

                                onSocialLogin(
                                        accessToken,
                                        user_first_name + " " + user_last_name,
                                        user_email,
                                        picUrl,
                                        "facebook");
                            } catch (JSONException e) {
                                getDataManager().handleThrowable(e, getNavigator());
                            }

                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "picture.type(large),last_name,first_name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {
        getNavigator().handleError(error);
    }
}
