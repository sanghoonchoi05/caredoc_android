package com.onestepmore.caredoc.ui.main.detail.homecare;

import android.app.Application;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.object.CommentObject;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.CommentRemoveRequest;
import com.onestepmore.caredoc.data.model.realm.Review;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Response;

public class HomecareDetailViewModel extends BaseViewModel<HomecareDetailNavigator> {
    private ReviewRepository mReviewRepository;

    public ReviewRepository getReviewRepository() {
        return mReviewRepository;
    }

    public HomecareDetailViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                                   ReviewRepository reviewRepository) {
        super(application, appDataSource, schedulerProvider);

        mReviewRepository = reviewRepository;
    }


    void loadReviewServiceSummaryFromServer(long serviceFakeId) {
        //setIsLoading(true);

        ReviewFacilityRequest.PathParameter pathParameter = new ReviewFacilityRequest.PathParameter();
        pathParameter.setMorphId(serviceFakeId);
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility_service.getType());
        getCompositeDisposable().add(getReviewRepository()
                .getReviewServiceSummaryApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);

                    getNavigator().onLoadReviewServiceSummary(response);

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void loadCommentFacilityFromServer(long id){
        deleteComment();
        //setIsLoading(true);

        ReviewFacilityRequest.PathParameter pathParameter = new ReviewFacilityRequest.PathParameter();
        pathParameter.setMorphId(id);
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility.getType());
        getCompositeDisposable().add(getReviewRepository()
                .getCommentApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);

                    saveComment(response.getItems());
                    getNavigator().onLoadCommentFacility(response);

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void removeComment(final long id, final long facilityId){
        CommentRemoveRequest.PathParameter pathParameter = new CommentRemoveRequest.PathParameter();
        pathParameter.setId(id);

        getReviewRepository().removeCommentApiCall(pathParameter, new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    loadCommentFacilityFromServer(facilityId);
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }

    private void deleteComment(){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Review> reviews = realm.where(Review.class).findAll();
                if(reviews != null){
                    while(reviews.size() > 0){
                        Review review = reviews.get(reviews.size()-1);
                        if(review != null){
                            if(review.getWriter() != null){
                                review.getWriter().deleteFromRealm();
                            }
                            review.deleteFromRealm();
                        }
                    }
                }
            }
        });
    }

    private void saveComment(List<CommentObject> commentObjects){
        if(commentObjects != null && commentObjects.size() > 0){
            List<Review> reviewList = new ArrayList<>();
            for(CommentObject commentObject : commentObjects){
                Review review = new Review();
                review.setReview(commentObject);
                reviewList.add(review);
            }

            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for(Review review : reviewList){
                        realm.copyToRealmOrUpdate(review);
                    }
                }
            });
        }
    }

    public void onCall(View view){
        getNavigator().onShowPhoneDial();
    }

    public void onWrite(View view){
        getNavigator().onWrite();
    }

    public void onGoHomepage(View view){
        getNavigator().onGoHomepage();
    }
}
