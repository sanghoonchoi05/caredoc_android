package com.onestepmore.caredoc.ui.main.detail.homecare;

import com.onestepmore.caredoc.data.model.api.review.FacilityCommentResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceSummaryResponse;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface HomecareDetailNavigator extends BaseNavigator {
    void onLoadReviewServiceSummary(ReviewFacilityServiceSummaryResponse response);
    void onLoadCommentFacility(FacilityCommentResponse response);
    void onShowPhoneDial();
    void onWrite();
    void onGoHomepage();
}
