package com.onestepmore.caredoc.ui.main.search.list;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SearchListFragmentProvider {
    @ContributesAndroidInjector(modules = SearchListFragmentModule.class)
    abstract SearchListFragment provideSearchListFragmentFactory();
}
