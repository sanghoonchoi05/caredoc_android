package com.onestepmore.caredoc.ui.carecoordi.loading;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class LoadingFragmentModule {
    @Provides
    LoadingViewModel loadingViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new LoadingViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("LoadingFragment")
    ViewModelProvider.Factory provideLoadingViewModelProvider(LoadingViewModel loadingViewModel) {
        return new ViewModelProviderFactory<>(loadingViewModel);
    }
}
