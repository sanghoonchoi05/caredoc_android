package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.common.SuggestionResponse;
import com.onestepmore.caredoc.data.model.api.object.ServiceSimpleObject;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Suggestion extends RealmObject implements Parcelable {
    @PrimaryKey
    private int key;
    private String type;
    private String text;
    private String value;
    private double lat;
    private double lng;
    private String address;
    private RealmList<Service> services;

    public Suggestion(){}

    protected Suggestion(Parcel in) {
        key = in.readInt();
        type = in.readString();
        text = in.readString();
        value = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        address = in.readString();
        this.services = new RealmList<>();
        in.readList(this.services, Service.class.getClassLoader());
    }

    public void setSuggestion(SuggestionResponse response){
        this.type = response.getType();
        this.text = response.getText();
        this.value = response.getValue();
        if(response.getLocation() != null){
            this.lat = response.getLocation().getLat();
            this.lng = response.getLocation().getLng();
        }

        if(response.getExtraData() != null){
            address = response.getExtraData().getAddress();
            this.services = new RealmList<>();
            for (ServiceSimpleObject item: response.getExtraData().getServices().getItems()) {
                Service service = new Service();
                service.setService(Service.TYPE.SUGGESTION, item);
                this.services.add(service);
            }
        }

        this.key = hashCodeForPrimaryKey();
    }

    public static final Creator<Suggestion> CREATOR = new Creator<Suggestion>() {
        @Override
        public Suggestion createFromParcel(Parcel in) {
            return new Suggestion(in);
        }

        @Override
        public Suggestion[] newArray(int size) {
            return new Suggestion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(key);
        dest.writeString(type);
        dest.writeString(text);
        dest.writeString(value);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeString(address);
        dest.writeList(services);
    }

    private int hashCodeForPrimaryKey() {
        int result = 17;
        result = 31 * result + type.hashCode();
        result = 31 * result + text.hashCode();
        result = 31 * result + value.hashCode();

        return result;
    }

    public int getKey() {
        return key;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getAddress() {
        return address;
    }

    public RealmList<Service> getServices() {
        return services;
    }
}
