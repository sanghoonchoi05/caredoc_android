package com.onestepmore.caredoc.ui.support.rx;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.jakewharton.rxbinding.internal.MainThreadSubscription;

import rx.Observable;
import rx.Subscriber;

import static com.jakewharton.rxbinding.internal.Preconditions.checkUiThread;

public class SupportObservableBooleanOnSubscribe implements Observable.OnSubscribe<Boolean>{
    private ObservableBoolean observableBoolean;

    public SupportObservableBooleanOnSubscribe(ObservableBoolean observableBoolean){
        this.observableBoolean = observableBoolean;
    }
    @Override
    public void call(Subscriber<? super Boolean> subscriber) {
        checkUiThread();

        final android.databinding.Observable.OnPropertyChangedCallback changedCallback = new android.databinding.Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(android.databinding.Observable sender, int propertyId) {
                subscriber.onNext(((ObservableBoolean)sender).get());
            }
        };

        observableBoolean.addOnPropertyChangedCallback(changedCallback);

        subscriber.add(new MainThreadSubscription() {
            @Override
            protected void onUnsubscribe() {
                observableBoolean.removeOnPropertyChangedCallback(changedCallback);
            }
        });

        // 초기화
        if(observableBoolean != null){
            subscriber.onNext(observableBoolean.get());
        }
    }
}
