package com.onestepmore.caredoc.ui.main.detail.review.write;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.api.review.ReviewSaveRequest;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.statics.StaticRating;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.databinding.AWriteReviewBinding;
import com.onestepmore.caredoc.databinding.IWriteReviewRatingBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.ui.support.rx.SupportRxObserver;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.WRITE_REVIEW_CLICK_BACK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.WRITE_REVIEW_CLICK_CHOOSE_EXPOSE_PERIOD;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.WRITE_REVIEW_CLICK_CHOOSE_RATING;
import static com.onestepmore.caredoc.ui.main.detail.FacilityDetailFragment.RESULT_CODE_WRITE_REVIEW;

public class WriteReviewActivity extends BaseActivity<AWriteReviewBinding, WriteReviewViewModel> implements
        WriteReviewNavigator
{

    private static final int RATING = 1;
    private static final int CONTENT = 1 << 1;
    private static final int CONTRACT = 1 << 2;
    private static final int VALID = RATING | CONTENT | CONTRACT;

    private long mServiceFakeId;
    private StaticService mStaticService;
    private ReviewSaveRequest mSaveRequest;

    public ReviewSaveRequest getSaveRequest() {
        if(mSaveRequest == null){
            mSaveRequest = new ReviewSaveRequest();
        }
        return mSaveRequest;
    }

    private int mValidFlag = 0;
    private ObservableBoolean mObservableValidRating = new ObservableBoolean(false);

    public void setValidFlag(int flag) {
        this.mValidFlag = flag;
    }

    public boolean isValid(int flag) {
        return (mValidFlag & flag) == flag;
    }

    public void setObservableValidRating(boolean validRating) {
        this.mObservableValidRating.set(validRating);
    }

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private WriteReviewViewModel mWriteReviewViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_write_review;
    }

    @Override
    public WriteReviewViewModel getViewModel() {
        mWriteReviewViewModel = ViewModelProviders.of(this, mViewModelFactory).get(WriteReviewViewModel.class);
        return mWriteReviewViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWriteReviewViewModel.setNavigator(this);

        setSupportActionBar(getViewDataBinding().aWriteReviewToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back_title);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Intent intent = getIntent();
        mServiceFakeId = intent.getLongExtra("serviceFakeId", 0L);
        int serviceId = intent.getIntExtra("serviceId", 0);
        if (mServiceFakeId == 0 || serviceId == 0) {
            finish();
            return;
        }

        mStaticService = RealmDataAccessor.getInstance().getStaticService(serviceId);
        if(mStaticService == null){
            finish();
            return;
        }

        getViewDataBinding().aWriteReviewTitle.setText(getString(R.string.a_write_review_title, mStaticService.getTypeName()));

        initRatingList(mStaticService);
        initExpose();
        initObserver();
    }

    private void checkRatingValid(){
        if(getSaveRequest().getRatings().size() == 0 ||
                getSaveRequest().getRatings().size() != mStaticService.getRatings().size()){
            setObservableValidRating(false);
            return;
        }

        boolean valid = true;
        for(ReviewSaveRequest.Rating rating : getSaveRequest().getRatings()){
            if(rating.getValue() == 0){
                valid = false;
                break;
            }
        }

        setObservableValidRating(valid);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AmplitudeManager.getInstance().logEvent(WRITE_REVIEW_CLICK_BACK);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initRatingList(StaticService staticService){
        WriteReviewRatingAdapter adapter = new WriteReviewRatingAdapter(staticService.getRatings());
        getViewDataBinding().aWriteReviewRatingBarRecyclerView.setAdapter(adapter);
        getViewDataBinding().aWriteReviewRatingBarRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        int verticalSpacingInPixels = getResources().getDimensionPixelSize(R.dimen.i_write_review_rating_vertical_item_space);
        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration(verticalSpacingInPixels, 0);
        getViewDataBinding().aWriteReviewRatingBarRecyclerView.addItemDecoration(spacesItemDecoration);
    }

    private void initExpose(){
        String [] exposeList = getResources().getStringArray(R.array.a_write_review_expose_list);
        ExposeSpinnerAdapter adapter = new ExposeSpinnerAdapter(getApplicationContext(), Arrays.asList(exposeList));
        getViewDataBinding().aWriteReviewExposeSpinner.setAdapter(adapter);
        getViewDataBinding().aWriteReviewExposeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int plusDate = 0;
                if(position == 1){
                    plusDate = 7;
                }
                else if(position == 2){
                    plusDate = 15;
                }
                else if(position == 3){
                    plusDate = 30;
                }

                String [] exposeList = getResources().getStringArray(R.array.a_write_review_expose_list);

                AmplitudeManager.getInstance().logEvent(WRITE_REVIEW_CLICK_CHOOSE_EXPOSE_PERIOD, AmplitudeParam.Key.VALUE, exposeList[position]);

                getSaveRequest().setExposeAfter(plusDate);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initObserver(){
        rx.Observable<Boolean> checkBoxChanges = SupportRxObserver.observableCheckBoxChanges(getViewDataBinding().aWriteReviewContractCheck);

        rx.Observable<CharSequence> contentChanges = RxTextView.textChanges(getViewDataBinding().aWriteReviewTextInput);
        getViewModel().addSubscription(contentChanges.subscribe(this::onContentChanged));

        rx.Observable<Integer> isValid = rx.Observable.combineLatest(checkBoxChanges, contentChanges, (checkBox, content) -> {
            int contractFlag = checkBox ? CONTRACT : 0;
            int contentFlag = content.toString().length() >= getResources().getInteger(R.integer.review_text_length_min) ? CONTENT : 0;
            return contractFlag | contentFlag;
        });

        rx.Observable<Boolean> ratingChanges = SupportRxObserver.observableBooleanChanges(mObservableValidRating);

        rx.Observable<Integer> isAllValid = rx.Observable.combineLatest(isValid, ratingChanges, (a, b) -> {
            int ratingFlag = b ? RATING : 0;
            return a | ratingFlag;
        });

        getViewModel().addSubscription(isAllValid.subscribe(this::enableRegisterButton));
    }

    private void enableRegisterButton(int flag){
        setValidFlag(flag);
        getViewDataBinding().aWriteReviewRegisterButton.setSelected(isValid(VALID));
    }

    private void onContentChanged(CharSequence charSequence){
        if(charSequence.toString().length() > 0){
            UIUtils.setTextIncludeAnnotation(
                    getApplicationContext(),
                    R.string.common_slash_num,
                    getViewDataBinding().aWriteReviewTextLength,
                    String.valueOf(charSequence.toString().length()),
                    String.valueOf(getResources().getInteger(R.integer.review_text_length_min)));
        }
        else{
            getViewDataBinding().aWriteReviewTextLength.setText(
                    getString(R.string.common_slash_num2, 0, getResources().getInteger(R.integer.review_text_length_min))
            );
        }
        getSaveRequest().setContent(charSequence.toString());
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onRegisterReview() {
        if(!isValid(VALID)){
            String msg = "";
            if(!isValid(RATING)){
                msg = getString(R.string.warning_msg_review_invalid_rating);
            }
            else if(!isValid(CONTENT)){
                msg = getString(R.string.warning_msg_review_invalid_text, getResources().getInteger(R.integer.review_text_length_min));
            }
            else if(!isValid(CONTRACT)){
                msg = getString(R.string.warning_msg_review_invalid_contract);
            }
            showCommonDialog(
                    getString(R.string.warning_msg_review_failed),
                    msg
            );
        }else{
            getViewModel().registerReview(mServiceFakeId, getSaveRequest());
            AppLogger.i(getClass(), "onRegisterReview");
        }
    }

    @Override
    public void onCompleteRegisterReview() {
        setResult(RESULT_CODE_WRITE_REVIEW);
        finish();
    }

    @Override
    public void onCompleteRemoveReview() {
        setResult(RESULT_CODE_WRITE_REVIEW);
        finish();
    }

    public class WriteReviewRatingAdapter extends RecyclerView.Adapter<WriteReviewRatingAdapter.WriteReviewRatingViewHolder> {
        private List<StaticRating> mRatingList;
        private int initSpinnerCount = 0;

        public WriteReviewRatingAdapter(@NonNull List<StaticRating> ratingList) {
            mRatingList = ratingList;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public WriteReviewRatingAdapter.WriteReviewRatingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            IWriteReviewRatingBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_write_review_rating, viewGroup, false);
            return new WriteReviewRatingAdapter.WriteReviewRatingViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull WriteReviewRatingAdapter.WriteReviewRatingViewHolder viewHolder, int position) {
            int first = position * 2;
            int second = position * 2 + 1;
            final StaticRating rating = mRatingList.get(first);
            viewHolder.binding.setLabel(rating.getLabel());
            initRatingSpinner(viewHolder.binding.iWriteReviewRatingSpinner, first);

            viewHolder.binding.iWriteReviewRatingLabel2.setVisibility(View.INVISIBLE);
            viewHolder.binding.iWriteReviewRatingSpinner2.setVisibility(View.INVISIBLE);
            if(mRatingList.size() > second){
                viewHolder.binding.iWriteReviewRatingLabel2.setVisibility(View.VISIBLE);
                viewHolder.binding.iWriteReviewRatingSpinner2.setVisibility(View.VISIBLE);
                final StaticRating rating2 = mRatingList.get(second);
                viewHolder.binding.setLabel2(rating2.getLabel());
                initRatingSpinner(viewHolder.binding.iWriteReviewRatingSpinner2, second);
            }

            viewHolder.binding.executePendingBindings();
        }

        private void initRatingSpinner(AppCompatSpinner spinner, int id){
            RatingSpinnerAdapter adapter = new RatingSpinnerAdapter(getApplicationContext(), getString(R.string.common_choice));
            spinner.setAdapter(adapter);
            spinner.setId(id);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(initSpinnerCount < mRatingList.size()){
                        initSpinnerCount++;
                        return;
                    }

                    StaticRating staticRating = mRatingList.get(parent.getId());
                    if(staticRating != null){
                        if(position > 0){
                            Map<String, MapValue<?>> map = new HashMap<>();
                            map.put(AmplitudeParam.Key.VALUE, new MapValue<>(staticRating.getLabel()));
                            map.put(AmplitudeParam.Key.RATING, new MapValue<>(position));
                            AmplitudeManager.getInstance().logEvent(WRITE_REVIEW_CLICK_CHOOSE_RATING, map);
                        }

                        getSaveRequest().setRating(staticRating.getId(), position);
                    }
                    checkRatingValid();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }

        @Override
        public int getItemCount() {
            return (mRatingList.size() + 1) / 2;
        }

        class WriteReviewRatingViewHolder extends RecyclerView.ViewHolder {
            final IWriteReviewRatingBinding binding;

            private WriteReviewRatingViewHolder(IWriteReviewRatingBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}
