package com.onestepmore.caredoc.firebase;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseParam {

    public static class PredefinedKey{
        public static final String METHOD = FirebaseAnalytics.Param.METHOD;
        public static final String CONTENT_TYPE = FirebaseAnalytics.Param.CONTENT_TYPE;
        public static final String SEARCH_TERM = FirebaseAnalytics.Param.SEARCH_TERM;
        public static final String ITEM_ID = FirebaseAnalytics.Param.ITEM_ID;
    }

    public static class Key{
        public static final String NAME = "name";
    }

    public static class Value{
        public static final String EMAIL = "email";
        public static final String FACEBOOK = "facebook";
        public static final String NAVER = "naver";
        public static final String KAKAO = "kakao";
        public static final String AUTO_COMPLETE = "auto_complete";
        public static final String GO = "go";
        public static final String COMPLETE = "complete";
        public static final String FACILITY = "facility";
        public static final String ADDRESS = "address";
        public static final String TAKER = "taker";
    }
}
