package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityCareCoordiForHospitalObject extends FacilityCareCoordiObject {
    @Expose
    @SerializedName("medical_expenses")
    private int medicalExpenses;

    public int getMedicalExpenses() {
        return medicalExpenses;
    }
}
