package com.onestepmore.caredoc.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Annotation;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.SpannedString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.ui.support.CustomTypefaceSpan;

import java.util.ArrayList;
import java.util.List;

public final class UIUtils {

    public static void setStatusBarColor(Activity activity, View rootView, int color, boolean lightBar){
        if(activity == null || rootView == null){
            return;
        }

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

        }*/

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
            if(lightBar){
                rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }else{
                rootView.setSystemUiVisibility(0);
            }
        }
    }

    public static void setStatusBarTranslucent(Activity activity, View rootView, boolean makeTranslucent){
        if(activity == null || rootView == null){
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = activity.getWindow();
            if (makeTranslucent) {
                window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            } else {
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            //rootView.setSystemUiVisibility(0);
            rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    public static void setTextIncludeAnnotation(Context context, int stringIdIncludedAnnotation, TextView inputTextView, String ... args){
        CharSequence titleText = context.getText(stringIdIncludedAnnotation);
        setTextIncludeAnnotation(context, titleText, inputTextView, args);
    }

    public static void setTextIncludeAnnotation(Context context, CharSequence textIncludedAnnotation, TextView inputTextView, String ... args){
        SpannedString spannedTitleText = new SpannedString(textIncludedAnnotation);

        // Strings.xml에 하나의 Annotation에 여러개의 속성 Key로 정의했어도
        // Annotation에는 하나의 Key로 존재하기 때문에 Key만큼 Annotation이 생성됨.
        // 같은 위치의 Annotation끼리 Set로 묶는다.
        Annotation[] annotations = spannedTitleText.getSpans(0, spannedTitleText.length(), Annotation.class);
        List<List<Annotation>> annotationList = new ArrayList<>();
        for(Annotation annotation : annotations) {
            boolean check = false;
            for (List<Annotation> anoList : annotationList) {
                if (anoList.size() == 0) {
                    continue;
                }

                Annotation ano = anoList.get(0);
                int spanStart = spannedTitleText.getSpanStart(annotation);
                if (spanStart == spannedTitleText.getSpanStart(ano)) {
                    check = true;
                    anoList.add(annotation);
                    break;
                }
            }

            if (!check) {
                List<Annotation> newAnoList = new ArrayList<>();
                newAnoList.add(annotation);
                annotationList.add(newAnoList);
            }
        }

        // 하나의 Annotation에 여러개의 속성Key가 있을 수 있으므로 SpannableStringBuilder로 선언.
        SpannableStringBuilder spannableTitleText = new SpannableStringBuilder(spannedTitleText);

        // format할 argument가 있다면 format함.
        // 텍스트 길이가 바뀜
        if(args.length > 0){
            String textStr = String.format(textIncludedAnnotation.toString(), args);
            spannableTitleText = new SpannableStringBuilder(textStr);
        }

        // Argument Index
        int argsIndex = 0;
        // format으로 인한 텍스트 변한 길이
        int increment = 0;
        for(List<Annotation> anoList : annotationList)
        {
            int spanStart = spannedTitleText.getSpanStart(anoList.get(0)) + increment;
            int spanEnd = spannedTitleText.getSpanEnd(anoList.get(0)) + increment;
            int originSpanStart = spannedTitleText.getSpanStart(anoList.get(0));
            int originSpanEnd = spannedTitleText.getSpanEnd(anoList.get(0));
            String fontName = "";
            String colorName = "";
            boolean isFormatted = false;

            Typeface typeface = null;
            int colorInt = 0;

            for(Annotation annotation : anoList){
                if(annotation.getKey().equals("font")){
                    fontName = annotation.getValue();
                }

                if(annotation.getKey().equals("color")){
                    colorName = annotation.getValue();
                }

                if(annotation.getKey().equals("format")){
                    isFormatted = true;
                }
            }

            if(isFormatted){
                spanEnd = spanStart + args[argsIndex].length();
            }

            if(!fontName.isEmpty()){
                if(fontName.equals("bold")){
                    typeface = Typeface.createFromAsset(context.getAssets(), "noto_sans_kr_bold_hestia.otf");
                }else if(fontName.equals("medium")){
                    typeface = Typeface.createFromAsset(context.getAssets(), "noto_sans_kr_medium_hestia.otf");
                }
                else if(fontName.equals("regular")){
                    typeface = Typeface.createFromAsset(context.getAssets(), "noto_sans_kr_regular_hestia.otf");
                }
            }

            if(!colorName.isEmpty()){
                if(colorName.equals("colorPrimary")){
                    colorInt = ContextCompat.getColor(context, R.color.colorPrimary);
                }
            }

            Object spanObejct = null;
            if(typeface != null){
                spanObejct = new CustomTypefaceSpan("", typeface, colorInt);
            }else if(colorInt > 0){
                spanObejct = new ForegroundColorSpan(colorInt);
            }

            if(spanObejct != null){
                spannableTitleText.setSpan(spanObejct,
                        spanStart,
                        spanEnd,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            if(isFormatted){
                increment = increment + args[argsIndex].length() - (originSpanEnd - originSpanStart);
                argsIndex++;
            }
        }

        inputTextView.setText(spannableTitleText,TextView.BufferType.SPANNABLE);
    }

    public static void setTextIncludeAnnotaion1(Context context, String textIncludedAnnotaion, TextView inputTextView){
        SpannedString spannedTitleText = new SpannedString(textIncludedAnnotaion);
        SpannableStringBuilder spannableTitleText = new SpannableStringBuilder(spannedTitleText);
        Annotation[] annotations = spannedTitleText.getSpans(0, spannedTitleText.length(), Annotation.class);
        for(Annotation annotation : annotations){
            String fontName = "";
            String colorName = "";

            Typeface typeface = null;
            int colorInt = 0;

            if(annotation.getKey().equals("font")){
                fontName = annotation.getValue();
            }

            if(annotation.getKey().equals("color")){
                colorName = annotation.getValue();
            }

            if(!fontName.isEmpty()){
                if(fontName.equals("bold")){
                    //Typeface typeface = ResourcesCompat.getFont(getBaseActivity(), R.font.noto_sans_kr_bold_hestia);
                    typeface = Typeface.createFromAsset(context.getAssets(), "noto_sans_kr_bold_hestia.otf");
                }else if(fontName.equals("medium")){
                    typeface = Typeface.createFromAsset(context.getAssets(), "noto_sans_kr_medium_hestia.otf");
                }
            }

            if(!colorName.isEmpty()){
                if(colorName.equals("colorPrimary")){
                    colorInt = R.color.colorPrimary;
                }
            }

            Object spanObejct = null;
            if(typeface != null){
                spanObejct = new CustomTypefaceSpan("", typeface, colorInt);
            }else if(colorInt > 0){
                spanObejct = new ForegroundColorSpan(colorInt);
            }

            if(spanObejct != null){
                spannableTitleText.setSpan(spanObejct,
                        spannableTitleText.getSpanStart(annotation),
                        spannableTitleText.getSpanEnd(annotation),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        inputTextView.setText(spannableTitleText,TextView.BufferType.SPANNABLE);
    }
}
