package com.onestepmore.caredoc.data.remote;

import com.onestepmore.caredoc.BuildConfig;

public class ApiEndPoint {
    private static final String ENDPOINT_VERSION = "/v1";
    private static final String ENDPOINT_VERSION_V2 = "/v2";
    public static final String ENDPOINT_SERVER_COMMON = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/common";
    public static final String ENDPOINT_SERVER_COMMON_V2 = BuildConfig.BASE_API_URL + ENDPOINT_VERSION_V2 + "/common";
    public static final String ENDPOINT_SERVER_STATICS = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/statics";
    public static final String ENDPOINT_SERVER_MEDICAL = ENDPOINT_SERVER_STATICS + "/medical";
    @Deprecated
    public static final String ENDPOINT_SERVER_ADDRESS = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/address";
    @Deprecated
    public static final String ENDPOINT_SERVER_TOOL = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/welfare-tool";
    public static final String ENDPOINT_SERVER_AUTH = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/auth";
    public static final String ENDPOINT_SERVER_USER = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/user";
    public static final String ENDPOINT_SERVER_USER_V2 = BuildConfig.BASE_API_URL + ENDPOINT_VERSION_V2 + "/user";
    public static final String ENDPOINT_FACILITY = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/facility";
    public static final String ENDPOINT_FACILITY_V2 = BuildConfig.BASE_API_URL + ENDPOINT_VERSION_V2 + "/facility";
    public static final String ENDPOINT_BOOKMARK = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/bookmark";
    public static final String ENDPOINT_COMMENT = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/comment";
    public static final String ENDPOINT_REVIEW_V2 = BuildConfig.BASE_API_URL + ENDPOINT_VERSION_V2 + "/review";
    public static final String ENDPOINT_TAKER = BuildConfig.BASE_API_URL + ENDPOINT_VERSION + "/taker";

    // 주소
    public static final String ENDPOINT_SERVER_ADDRESS_V2 = ENDPOINT_SERVER_COMMON_V2 + "/address";
    //검색어 제안
    public static final String ENDPOINT_SERVER_SUGGESTION = ENDPOINT_SERVER_COMMON + "/suggestion";

    // 질환
    public static final String ENDPOINT_SERVER_SUBJECT = ENDPOINT_SERVER_MEDICAL + "/subject";
    public static final String ENDPOINT_SERVER_DISEASE = ENDPOINT_SERVER_MEDICAL + "/disease";


    // 인증
    public static final String ENDPOINT_SERVER_AUTH_USER = ENDPOINT_SERVER_AUTH + "/user";
    public static final String ENDPOINT_SERVER_LOGIN = ENDPOINT_SERVER_AUTH + "/login";
    public static final String ENDPOINT_SERVER_SOCIAL_LOGIN_PROVIDER = ENDPOINT_SERVER_AUTH + "/social-login/{provider}";
    public static final String ENDPOINT_SERVER_REGISTER = ENDPOINT_SERVER_AUTH + "/register";
    public static final String ENDPOINT_SERVER_LOGOUT = ENDPOINT_SERVER_AUTH + "/logout";
    public static final String ENDPOINT_SERVER_PASSWORD_EMAIL = ENDPOINT_SERVER_AUTH + "/password/email";

    // 시설 상세
    public static final String ENDPOINT_FACILITY_DETAIL = ENDPOINT_FACILITY_V2 + "/{id}";

    public static final String ENDPOINT_FACILITY_SEARCH = ENDPOINT_FACILITY + "/search";
    public static final String ENDPOINT_FACILITY_SEARCH_MAP = ENDPOINT_FACILITY_SEARCH + "/map";
    public static final String ENDPOINT_FACILITY_SEARCH_CODE = ENDPOINT_FACILITY_SEARCH + "/result";

    // 케어 코디
    public static final String ENDPOINT_FACILITY_CARE_COORDI_LIST = ENDPOINT_FACILITY_SEARCH + "/step-by-step";

    // 북마크
    public static final String ENDPOINT_GET_BOOKMARK = ENDPOINT_SERVER_USER_V2 + "/bookmark/facility";
    public static final String ENDPOINT_BOOKMARK_SAVE = ENDPOINT_BOOKMARK + "/{morphType}/{morphId}";
    public static final String ENDPOINT_BOOKMARK_REMOVE= ENDPOINT_BOOKMARK + "/{morphType}/{morphId}";

    // 리뷰 및 댓글
    public static final String ENDPOINT_GET_COMMENT = ENDPOINT_COMMENT + "/{morphType}/{morphId}";
    public static final String ENDPOINT_GET_REVIEW_V2 = ENDPOINT_REVIEW_V2 + "/{morphType}/{morphId}";
    public static final String ENDPOINT_GET_REVIEW_SUMMARY_V2 = ENDPOINT_GET_REVIEW_V2 + "/summery";
    public static final String ENDPOINT_REVIEW_V2_SAVE = ENDPOINT_REVIEW_V2 + "/{morphType}/{morphId}";
    public static final String ENDPOINT_REVIEW_V2_REMOVE = ENDPOINT_REVIEW_V2 + "/{id}";
    public static final String ENDPOINT_COMMENT_SAVE = ENDPOINT_COMMENT + "/{morphType}/{morphId}";
    public static final String ENDPOINT_COMMENT_REMOVE = ENDPOINT_COMMENT + "/{id}";

    // 어르신
    public static final String ENDPOINT_GET_MY_TAKER = ENDPOINT_SERVER_USER + "/taker";
    public static final String ENDPOINT_ADD_TAKER_PHOTO = ENDPOINT_TAKER + "/{id}/upload";
    public static final String ENDPOINT_MODIFY_TAKER = ENDPOINT_TAKER + "/{id}";
    public static final String ENDPOINT_TAKER_DETAIL = ENDPOINT_TAKER + "/{id}";
    public static final String ENDPOINT_DELETE_TAKER = ENDPOINT_TAKER + "/{id}";

    // 주소검색 API(정부)
    public static final String ENDPOINT_EXTERNAL_JUSO = "https://www.juso.go.kr/addrlink/addrLinkApi.do";
    // 좌표검색 API(정부)
    public static final String ENDPOINT_EXTERNAL_COORDINATE = "https://www.juso.go.kr/addrlink/addrCoordApi.do";
    // 네이버 사용자 조회(네이버)
    public static final String ENDPOINT_EXTERNAL_GET_NAVER_USER_INFO = "https://openapi.naver.com/v1/nid/me";
    // 메일 보내기 API(메일건)
    public static final String ENDPOINT_EXTERNAL_MAILGUN = "https://api.mailgun.net/v3/caredoc.kr/messages";
}
