package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailEntranceResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailMedicalResponse;
import com.onestepmore.caredoc.data.model.api.object.AttachesObject;
import com.onestepmore.caredoc.data.model.api.object.FacilityDetailObject;
import com.onestepmore.caredoc.data.model.api.object.FacilitySimpleObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceDetailObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceSimpleObject;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

import static com.onestepmore.caredoc.data.model.realm.Service.TYPE.SEARCH_LIST;
import static com.onestepmore.caredoc.data.model.realm.Service.TYPE.SEARCH_TEMP;

@RealmClass
public class FacilitySearchTemp implements FacilityRealmModel {
    @PrimaryKey
    private Long fakeId;
    private String name;
    private String address;
    private String roadAddress;
    private String homepage;
    private String contact;
    private boolean isBookmarked;
    private LatLngFacility latLng;
    private String thumbnail;
    private float ratingAvg;
    private int reviewCount;
    private RealmList<Service> services;
    private RealmList<Attach> attaches;

    public FacilitySearchTemp(){}

    public FacilitySearchTemp(Parcel parcel){
        this.fakeId = parcel.readLong();
        this.name = parcel.readString();
        this.address = parcel.readString();
        this.roadAddress = parcel.readString();
        this.homepage = parcel.readString();
        this.contact = parcel.readString();
        this.isBookmarked = parcel.readByte() != 0;
        this.latLng = parcel.readParcelable(LatLng.class.getClassLoader());
        this.thumbnail = parcel.readString();
        this.ratingAvg = parcel.readFloat();
        this.reviewCount = parcel.readInt();
        this.services = new RealmList<>();
        parcel.readList(this.services , Service.class.getClassLoader());
        this.attaches = new RealmList<>();
        parcel.readList(this.attaches , Attach.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(fakeId);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(roadAddress);
        dest.writeString(homepage);
        dest.writeString(contact);
        dest.writeByte((byte) (isBookmarked ? 1 : 0));
        dest.writeParcelable(latLng, flags);
        dest.writeString(thumbnail);
        dest.writeFloat(ratingAvg);
        dest.writeInt(reviewCount);
        dest.writeList(services);
        dest.writeList(attaches);
    }

    public static Parcelable.Creator<FacilitySearchTemp> CREATOR = new Parcelable.Creator<FacilitySearchTemp>() {

        @Override
        public FacilitySearchTemp createFromParcel(Parcel source) {
            return new FacilitySearchTemp(source);
        }

        @Override
        public FacilitySearchTemp[] newArray(int size) {
            return new FacilitySearchTemp[size];
        }

    };

    public void setFacility(FacilitySimpleObject facility){
        this.name = facility.getName();
        this.address = facility.getAddress();
        this.roadAddress = facility.getRoadAddress();
        this.homepage = facility.getHomepage();
        this.contact = facility.getContact();
        this.isBookmarked = facility.isBookmarked();
        this.latLng = new LatLngFacility();
        this.latLng.setLatLng(facility.getFakeId(), Service.TYPE.SEARCH_TEMP, facility.getGeoLocation());
        this.fakeId = facility.getFakeId();
        this.thumbnail = facility.getThumbnail();
        this.ratingAvg = facility.getTotalRating();
        this.reviewCount = facility.getTotalReviewCount();
        this.services = new RealmList<>();
        for (ServiceSimpleObject item: facility.getServices().getItems()) {
            Service service = new Service();
            service.setService(SEARCH_TEMP, item);
            this.services.add(service);
        }
        this.attaches = new RealmList<>();
        for (AttachesObject.Item item: facility.getAttaches().getItems()) {
            Attach attach = new Attach();
            attach.setAttach(item);
            this.attaches.add(attach);
        }
    }

    public void setFacilityDetail(FacilityDetailObject facility){
        this.name = facility.getName();
        this.address = facility.getAddress();
        this.roadAddress = facility.getRoadAddress();
        this.homepage = facility.getHomepage();
        this.contact = facility.getContact();
        this.isBookmarked = facility.isBookmarked();
        this.latLng = new LatLngFacility();
        this.latLng.setLatLng(facility.getFakeId(), Service.TYPE.SEARCH_TEMP, facility.getGeoLocation());
        this.fakeId = facility.getFakeId();
        this.thumbnail = facility.getThumbnail();
        this.ratingAvg = facility.getTotalRating();
        this.reviewCount = facility.getTotalReviewCount();
        this.attaches = new RealmList<>();
        for (AttachesObject.Item item: facility.getAttaches().getItems()) {
            Attach attach = new Attach();
            attach.setAttach(item);
            this.attaches.add(attach);
        }
    }

    public void setFacility(FacilityDetailMedicalResponse facility){
        setFacilityDetail(facility);
        this.services = new RealmList<>();
        for (ServiceDetailObject item: facility.getServices().getItems()) {
            Service service = new Service();
            service.setService(SEARCH_TEMP, item);
            this.services.add(service);
        }
    }

    public void setFacility(FacilityDetailEntranceResponse facility){
        setFacilityDetail(facility);
        this.services = new RealmList<>();
        for (ServiceDetailObject item: facility.getServices().getItems()) {
            Service service = new Service();
            service.setService(SEARCH_TEMP, item);
            this.services.add(service);
        }
    }

    @Override
    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String getAddress() {
        return address;
    }
    @Override
    public void setAddress(String address) {
        this.address = address;
    }
    @Override
    public String getRoadAddress() {
        return roadAddress;
    }
    @Override
    public void setRoadAddress(String roadAddress) {
        this.roadAddress = roadAddress;
    }
    @Override
    public String getHomepage() {
        return homepage;
    }
    @Override
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }
    @Override
    public String getContact() {
        return contact;
    }

    @Override
    public void setContact(String contact) {
        this.contact = contact;
    }
    @Override
    public LatLngFacility getLatLng() {
        return latLng;
    }
    @Override
    public void setLatLng(LatLngFacility latLng) {
        this.latLng = latLng;
    }
    @Override
    public String getThumbnail() {
        return thumbnail;
    }
    @Override
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public float getRatingAvg() {
        return ratingAvg;
    }
    @Override
    public void setRatingAvg(float ratingAvg) {
        this.ratingAvg = ratingAvg;
    }
    @Override
    public RealmList<Service> getServices() {
        return services;
    }
    @Override
    public void setServices(RealmList<Service> services) {
        this.services = services;
    }

    @Override
    public RealmList<Attach> getAttaches() {
        return attaches;
    }

    @Override
    public void setAttaches(RealmList<Attach> attaches) {
        this.attaches = attaches;
    }

    @Override
    public Long getFakeId() {
        return fakeId;
    }
    @Override
    public void setFakeId(Long fakeId) {
        this.fakeId = fakeId;
    }
    @Override
    public int getReviewCount() {
        return reviewCount;
    }
    @Override
    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    @Override
    public boolean isBookMarked() {
        return this.isBookmarked;
    }

    @Override
    public void setBookmarked(boolean bookmarked) {
        this.isBookmarked = bookmarked;
    }

    @Override
    public void deleteRealm(FacilityRealmModel realmModel) {
        RealmList<Service> services = realmModel.getServices();
        if(services != null){
            for(int j=0; j<services.size(); j++){
                Service service = services.get(j);
                if(service != null){
                    service.deleteRealm();
                }
            }
        }

        RealmList<Attach> attaches = realmModel.getAttaches();
        if(attaches != null){
            for(int j=0; j<attaches.size(); j++){
                Attach attach = attaches.get(j);
                if(attach != null){
                    attach.deleteFromRealm();
                }
            }
        }

        if(realmModel.getLatLng() != null){
            RealmObject.deleteFromRealm(realmModel.getLatLng());
        }

        RealmObject.deleteFromRealm(this);
    }

    @Override
    public boolean isModelSame(Object obj) {
        return obj instanceof FacilityRealmModel && this.fakeId.equals(((FacilityRealmModel) obj).getFakeId());
    }

    @Override
    public boolean isModelContentsSame(Object obj) {
        return false;
    }
}
