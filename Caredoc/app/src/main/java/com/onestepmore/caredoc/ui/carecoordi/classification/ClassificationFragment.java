package com.onestepmore.caredoc.ui.carecoordi.classification;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticDisease;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTool;
import com.onestepmore.caredoc.databinding.FClassificationBinding;
import com.onestepmore.caredoc.databinding.VChipGroupItemChoiceBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

public class ClassificationFragment extends BaseCoordiFragment<FClassificationBinding, ClassificationViewModel> implements
        ClassificationNavigator
{

    @Inject
    @Named("ClassificationFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private ClassificationViewModel mClassificationViewModel;

    private List<StaticDisease> mStaticDiseases;
    private List<StaticTool> mStaticTools;
    private CareCoordiActivity.UI_ITEM mSelectedUIItem;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_classification;
    }

    @Override
    public ClassificationViewModel getViewModel() {
        mClassificationViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ClassificationViewModel.class);
        return mClassificationViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClassificationViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getViewDataBinding().fClassificationChipGroup.setSingleSelection(false);
        mStaticDiseases = getViewModel().getRealm().where(StaticDisease.class).findAll();
        mStaticTools = getViewModel().getRealm().where(StaticTool.class).findAll();
    }

    private void initChipGroup(CareCoordiActivity.UI_ITEM uiItem) {
        if(mStaticDiseases == null || mStaticTools == null){
            return;
        }

        mSelectedUIItem = uiItem;

        getViewDataBinding().fClassificationChipGroup.removeAllViews();

        List<String> textArray = new ArrayList<>();
        if(mSelectedUIItem == CareCoordiActivity.UI_ITEM.DISEASE){
            for(StaticDisease disease : mStaticDiseases){
                textArray.add(disease.getTypeName());
            }
        }else if(mSelectedUIItem == CareCoordiActivity.UI_ITEM.TOOL){
            for(StaticTool tool : mStaticTools){
                if(!CommonUtils.checkNullAndEmpty(tool.getTypeName())){
                    textArray.add(tool.getTypeName());
                }else if(!CommonUtils.checkNullAndEmpty(tool.getModelName())){
                    textArray.add(tool.getModelName());
                }
            }
        }

        for (int i=0; i<textArray.size(); i++) {
            VChipGroupItemChoiceBinding binding = DataBindingUtil.inflate(
                    getLayoutInflater(),
                    R.layout.v_chip_group_item_choice,
                    getViewDataBinding().fClassificationChipGroup,
                    false);
            binding.vChipGroupItemChoiceChip.setText(textArray.get(i));
            binding.vChipGroupItemChoiceChip.setId(i);
            binding.vChipGroupItemChoiceChip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(mSelectedUIItem != null){
                        String typeCode = null;
                        String name = "";
                        if(mSelectedUIItem == CareCoordiActivity.UI_ITEM.DISEASE){
                            typeCode = mStaticDiseases.get(buttonView.getId()).getTypeCode();
                            name = buttonView.getText().toString();
                        }else if(mSelectedUIItem == CareCoordiActivity.UI_ITEM.TOOL){
                            typeCode = mStaticTools.get(buttonView.getId()).getTypeCode();
                            name = buttonView.getText().toString();
                        }

                        if(typeCode != null){
                            ((CareCoordiActivity)getBaseActivity()).checkClassificationItem(
                                    mSelectedUIItem,
                                    typeCode,
                                    name,
                                    isChecked);
                        }
                    }
                    AppLogger.i(getClass(), "id: " + buttonView.getId());
                }
            });
            getViewDataBinding().fClassificationChipGroup.addView(binding.getRoot());
        }
    }

    @Override
    public void setTitle(int titleResId) {
        UIUtils.setTextIncludeAnnotation(getBaseActivity(), titleResId, getViewDataBinding().fClassificationTitle);
    }

    @Override
    public void setStepData(StepData stepData) {
        if(stepData == null){
            return;
        }

        setTitle(CommonUtils.getCareCoordiTitleResId(stepData));
        initChipGroup(stepData.getUIItem());
    }
}
