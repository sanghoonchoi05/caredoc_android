package com.onestepmore.caredoc.data.model.api.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class CommentRemoveRequest {
    public static class PathParameter {
        @Expose
        @SerializedName("id")
        private long id;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }
    }
}
