package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.CommonResponse_Deprecated;
import com.onestepmore.caredoc.data.model.api.common.AddressResponse;
import com.onestepmore.caredoc.data.model.api.common.SuggestionResponse;
import com.onestepmore.caredoc.data.model.api.statics.DiseaseResponse;
import com.onestepmore.caredoc.data.model.api.statics.StaticDataResponse;
import com.onestepmore.caredoc.data.model.api.account.LoginRequest;
import com.onestepmore.caredoc.data.model.api.account.LoginResponse;
import com.onestepmore.caredoc.data.model.api.account.PasswordEmailRequest;
import com.onestepmore.caredoc.data.model.api.account.PasswordEmailResponse;
import com.onestepmore.caredoc.data.model.api.account.RegisterRequest;
import com.onestepmore.caredoc.data.model.api.account.RegisterResponse;
import com.onestepmore.caredoc.data.model.api.account.UserResponse;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkFacilityResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalCoordinateRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalCoordinateResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalJusoRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalJusoResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalMailgunRequest;
import com.onestepmore.caredoc.data.model.api.external.ExternalMailgunResponse;
import com.onestepmore.caredoc.data.model.api.external.ExternalNaverUserInfoResponse;
import com.onestepmore.caredoc.data.model.api.statics.SubjectResponse;
import com.onestepmore.caredoc.data.model.api.taker.MyTakerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public interface ApiHelper {
    ApiHeader getApiHeader();
    Flowable<LoginResponse> doServerLoginApiCall(LoginRequest.ServerLoginRequest request);
    Flowable<LoginResponse> doSocialLoginApiCall(LoginRequest.SocialLoginPathParameter pathParameter, JSONObject request);

    Flowable<UserResponse> doServerUserApiCall();
    Single<JSONObject> doServerLoginApiCallGetJson(LoginRequest.ServerLoginRequest request);
    Single<RegisterResponse> doServerRegisterApiCall(RegisterRequest request);
    void doServerLogoutApiCall(OkHttpResponseListener listener);
    Flowable<PasswordEmailResponse> doServerPasswordEmail(PasswordEmailRequest request);

    Flowable<CommonResponse_Deprecated> doServerCommonApiCall();
    Flowable<StaticDataResponse> doServerStaticDataApiCall();
    Flowable<DiseaseResponse> doServerDiseaseApiCall();
    Flowable<SubjectResponse> doServerSubjectApiCall();
    Flowable<JSONArray> doServerToolApiCall();
    Flowable<AddressResponse> doServerAddressApiCall(int sidoCode, int gugunCode);
    Flowable<List<SuggestionResponse>> doServerSuggestionApiCall(String keyword);

    Flowable<BookmarkFacilityResponse> getBookmarkApiCall();
    Flowable<MyTakerResponse> getMyTakerApiCall();

    Flowable<ExternalJusoResponse> getExternalJusoApiCall(ExternalJusoRequest.QueryParameter queryParameter);
    Flowable<ExternalCoordinateResponse> getExternalCoordinateApiCall(ExternalCoordinateRequest.QueryParameter queryParameter);
    Flowable<ExternalNaverUserInfoResponse> getExternalNaverUserInfo(String accessToken);
    Flowable<ExternalMailgunResponse> sendExternalMailgun(ExternalMailgunRequest.QueryParameter queryParameter);
}
