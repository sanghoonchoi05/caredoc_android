package com.onestepmore.caredoc.ui.main.search.filter.address;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.main.search.SearchViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AddressFilterFragmentModule {
    @Provides
    AddressFilterViewModel addressFilterViewModel(Application application, AppDataSource dataSource, Gson gson, SchedulerProvider schedulerProvider) {
        return new AddressFilterViewModel(application, dataSource, schedulerProvider, gson);
    }

    @Provides
    @Named("AddressFilterFragment")
    ViewModelProvider.Factory provideAddressFilterViewModelProvider(AddressFilterViewModel addressFilterViewModel) {
        return new ViewModelProviderFactory<>(addressFilterViewModel);
    }
}
