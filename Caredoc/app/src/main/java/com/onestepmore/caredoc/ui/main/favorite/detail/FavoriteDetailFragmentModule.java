package com.onestepmore.caredoc.ui.main.favorite.detail;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class FavoriteDetailFragmentModule {
    @Provides
    FavoriteDetailViewModel favoriteDetailViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new FavoriteDetailViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("FavoriteDetailFragment")
    ViewModelProvider.Factory provideFavoriteDetailViewModelProvider(FavoriteDetailViewModel favoriteDetailViewModel) {
        return new ViewModelProviderFactory<>(favoriteDetailViewModel);
    }
}
