package com.onestepmore.caredoc.ui.main.detail;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class FacilityActivityModule {
    @Provides
    FacilityViewModel facilityViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new FacilityViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideFacilityViewModelProvider(FacilityViewModel facilityViewModel) {
        return new ViewModelProviderFactory<>(facilityViewModel);
    }
}
