package com.onestepmore.caredoc.ui.carecoordi;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.DialogCareCoordiLoadingBinding;
import com.onestepmore.caredoc.ui.base.BaseDialog;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CareCoordiLoadingDialog extends BaseDialog {

    public static final String TAG = "care_coordi_loading_dialog";

    private Handler imageSwitchHandler;
    private int mIndex = 0;
    private DialogCareCoordiLoadingBinding mBinding;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()), R.layout.dialog_care_coordi_loading, null, false);

        mBinding.dialogCareCoordiImgSwitcher.setFactory(() -> {
            ImageView myView = new ImageView(getApplicationContext());
            myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            myView.setLayoutParams(new
                    ImageSwitcher.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            return myView;
        });

        Animation imgAnimationIn =  AnimationUtils.
                loadAnimation(getBaseActivity(),   R.anim.slide_left_in);
        mBinding.dialogCareCoordiImgSwitcher.setInAnimation(imgAnimationIn);

        Animation imgAnimationOut =  AnimationUtils.
                loadAnimation(getBaseActivity(),   R.anim.slide_left_out);
        mBinding.dialogCareCoordiImgSwitcher.setOutAnimation(imgAnimationOut);

        imageSwitchHandler = new Handler();
        imageSwitchHandler.post(runnableCode);

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(mBinding.getRoot());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);


        return dialog;
    }

    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {

            if(mIndex == 0){
                mBinding.dialogCareCoordiImgSwitcher.setImageResource(R.drawable.care_coordi_loading_1);
                mIndex++;
            }else if(mIndex == 1){
                mBinding.dialogCareCoordiImgSwitcher.setImageResource(R.drawable.care_coordi_loading_2);
                mIndex++;
            }else{
                mIndex = 0;
                mBinding.dialogCareCoordiImgSwitcher.setImageResource(R.drawable.care_coordi_loading_3);
            }

            imageSwitchHandler.postDelayed(this, 900);
        }
    };
    @Override
    public void onStop() {
        imageSwitchHandler.removeCallbacks(runnableCode);
        super.onStop();
    }
}
