package com.onestepmore.caredoc.ui.main.detail.photo;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface PhotoNavigator extends BaseNavigator {
    void onClose();
}
