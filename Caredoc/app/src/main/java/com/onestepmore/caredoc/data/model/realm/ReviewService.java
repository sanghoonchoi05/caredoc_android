package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.CommentObject;
import com.onestepmore.caredoc.data.model.api.object.RatingsObject;
import com.onestepmore.caredoc.data.model.api.object.ReviewServiceObject;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ReviewService extends RealmObject implements Parcelable {
    @PrimaryKey
    private long id;
    private float rating;
    private RealmList<String> labelList;
    private RealmList<Integer> ratingList;
    private String content;
    private long fakeId;
    private String serviceName;
    private String photoUrl;
    private String name;
    private String createdAt;
    private boolean isExposed;
    private boolean isOwn;

    public ReviewService(){}


    protected ReviewService(Parcel in) {
        id = in.readLong();
        rating = in.readFloat();
        in.readList(labelList, String.class.getClassLoader());
        ratingList = new RealmList<>();
        in.readList(ratingList, Integer.class.getClassLoader());
        content = in.readString();
        fakeId = in.readLong();
        serviceName = in.readString();
        photoUrl = in.readString();
        name = in.readString();
        createdAt = in.readString();
        isExposed = in.readByte() != 0;
        isOwn = in.readByte() != 0;
    }

    public static final Creator<ReviewService> CREATOR = new Creator<ReviewService>() {
        @Override
        public ReviewService createFromParcel(Parcel in) {
            return new ReviewService(in);
        }

        @Override
        public ReviewService[] newArray(int size) {
            return new ReviewService[size];
        }
    };

    public void setReview(ReviewServiceObject review){
        this.id = review.getId();
        this.rating = review.getRating();
        this.labelList = new RealmList<>();
        this.ratingList = new RealmList<>();
        for(RatingsObject.Item item : review.getRatings().getItems()){
            labelList.add(item.getLabel());
            ratingList.add(item.getRating());
        }
        this.content = review.getContent();
        this.fakeId = review.getReviewable().getFakeId();
        this.serviceName = review.getReviewable().getServiceName();
        this.photoUrl = review.getWriter().getPhotoUrl();
        this.name = review.getWriter().getName();
        this.createdAt = review.getCreatedAt();
        this.isExposed = review.isExposed();
        this.isOwn = review.isOwn();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public String getName() {
        return name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public RealmList<Integer> getRatingList() {
        return ratingList;
    }

    public long getFakeId() {
        return fakeId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public boolean isExposed() {
        return isExposed;
    }

    public RealmList<String> getLabelList() {
        return labelList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeFloat(rating);
        dest.writeList(labelList);
        dest.writeList(ratingList);
        dest.writeString(content);
        dest.writeLong(fakeId);
        dest.writeString(serviceName);
        dest.writeString(photoUrl);
        dest.writeString(name);
        dest.writeString(createdAt);
        dest.writeByte((byte) (isExposed ? 1 : 0));
        dest.writeByte((byte) (isOwn ? 1 : 0));
    }
}
