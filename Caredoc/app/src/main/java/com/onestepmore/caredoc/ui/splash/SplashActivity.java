package com.onestepmore.caredoc.ui.splash;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.nhn.android.naverlogin.OAuthLogin;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.databinding.ASplashBinding;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;

import static com.onestepmore.caredoc.AppConstants.DEEP_LINK_FACILITY_DETAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FORCE_UPDATE_POPUP_CLICK_UPDATE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.FORCE_UPDATE_POPUP;

public class SplashActivity extends BaseActivity<ASplashBinding, SplashViewModel> implements
        SplashNavigator
{
    public interface DynamicLinkCallback{
        void onComplete(String id);
    }

    private boolean isBlockBackKey = false;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private SplashViewModel mSplashViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_splash;
    }

    @Override
    public SplashViewModel getViewModel() {
        mSplashViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SplashViewModel.class);
        return mSplashViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSplashViewModel.setNavigator(this);

        UIUtils.setStatusBarColor(this, getWindow().getDecorView(), getResources().getColor(R.color.splash_bg), true);
        //checkGooglePlayServices();
        remoteConfigFetch();
    }

    @Override
    public void onBackPressed() {
        if(isBlockBackKey){
            return;
        }

        super.onBackPressed();
    }

    private void remoteConfigFetch(){
        isBlockBackKey = true;
        FirebaseManager.getInstance().remoteConfigFetch((successful) -> checkUpdate());
    }

    private void checkUpdate(){
        if(FirebaseManager.getInstance().isForceUpdate()){
            FirebaseManager.getInstance().logEvent(FirebaseEvent.Screen.FORCE_UPDATE_POPUP);
            AmplitudeManager.getInstance().logEvent(FORCE_UPDATE_POPUP);
            CommonYNDialog dialog = CommonYNDialog.newInstance(
                    getString(R.string.app_update_title),
                    FirebaseManager.getInstance().getForceUpdateMessage(),
                    getString(R.string.app_update_exit),
                    getString(R.string.app_update_ok)

            );
            dialog.setOnOkClickListener((view) -> {
                FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.FORCE_UPDATE_POPUP_CLICK_UPDATE);
                AmplitudeManager.getInstance().logEvent(FORCE_UPDATE_POPUP_CLICK_UPDATE);
                finish();
                FirebaseManager.getInstance().gotoMarket(this);
            });
            dialog.setOnCancelClickListener((view) -> finish());
            dialog.setCancelable(false);
            dialog.show(getSupportFragmentManager(), CommonYNDialog.class.getName());
        }
        else{
            checkApiStatus();
        }
    }

    private void checkApiStatus(){
        if(!FirebaseManager.getInstance().isApiStatus()){
            String msg = FirebaseManager.getInstance().getApiStatusMessage();
            if(CommonUtils.checkNullAndEmpty(msg)){
                msg = getString(R.string.warning_msg_api_status_desc);
            }
            showCommonDialog(
                    getString(R.string.warning_msg_api_status_title),
                    msg,
                    CommonYNDialog.class.getName(),
                    (view) -> finish(),
                    getString(R.string.common_confirm),
                    "",
                    true
            );
        }
        else{
            isBlockBackKey = false;
            initData();
        }
    }

    private void initData(){
        mSplashViewModel.setVersionName(BuildConfig.CAREDOC_FULL_VERSION);

		loadData();
    }

    private void loadData(){
        mSplashViewModel.saveStaticsData(getString(R.string.common_all));
        mSplashViewModel.loadStaticDataFromServer();
        mSplashViewModel.loadSubjectDataFromServer();
        mSplashViewModel.loadAddressDataFromServer(getString(R.string.common_nationwide), getString(R.string.common_nationwide));
        mSplashViewModel.loadDiseaseDataFromServer();
        //mSplashViewModel.loadToolDataFromServer();
    }

    /*private void fetchABText(){
        FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(BuildConfig.DEBUG).build());
        Map<String, Object> map = new HashMap<>();
        map.put("main_register_text", getString(R.string.a_start_register_button_text));
        map.put("main_care_coordi_text", getString(R.string.a_start_care_coordi_button_text));
        firebaseRemoteConfig.setDefaults(map);
        firebaseRemoteConfig.fetch(BuildConfig.DEBUG ? 0 : TimeUnit.HOURS.toSeconds(12)).addOnCompleteListener(
                (result)->{
                    if(result.isSuccessful()){
                        firebaseRemoteConfig.activateFetched();
                    }
                    loadData();
        });
	}*/

    private boolean checkGooglePlayServices() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (status != ConnectionResult.SUCCESS) {
            Dialog dialog = googleApiAvailability.getErrorDialog(this, status, -1);
            dialog.setOnDismissListener(dialogInterface -> finish());
            dialog.show();

            googleApiAvailability.showErrorNotification(this, status);

            return true;
        }

        return false;
    }

    public void handleIntent(DynamicLinkCallback callback){
        if(getIntent() != null){
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                            if(pendingDynamicLinkData != null){
                                Uri deepLink = pendingDynamicLinkData.getLink();
                                String path = deepLink.toString();
                                String facilityDetailUrl = DEEP_LINK_FACILITY_DETAIL;
                                if(path.startsWith(facilityDetailUrl)){
                                    String subString = path.substring(facilityDetailUrl.length());
                                    String [] split = subString.split("/");
                                    if(split.length > 0){
                                        callback.onComplete(split[0]);
                                        return;
                                    }
                                }
                            }

                            callback.onComplete("");
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            callback.onComplete("");
                        }
                    });
        }
        else{
            callback.onComplete("");
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    public void openMainActivity() {
        handleIntent(new DynamicLinkCallback() {
            @Override
            public void onComplete(String id) {
                BackStackManager.getInstance().removeAllFragment();
                Intent intent = newIntent(MainActivity.class);
                intent.putExtra("index", 0);
                if(!CommonUtils.checkNullAndEmpty(id)){
                    intent.putExtra("fakeId", Long.valueOf(id));
                }
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void loadComplete() {
        openMainActivity();
    }

    @Override
    public void onLogout() {
        LoginManager.getInstance().logOut();
        OAuthLogin.getInstance().logout(this);
        openMainActivity();
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }
}
