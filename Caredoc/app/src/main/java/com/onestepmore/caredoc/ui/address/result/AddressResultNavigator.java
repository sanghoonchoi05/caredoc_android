package com.onestepmore.caredoc.ui.address.result;

import com.onestepmore.caredoc.data.model.realm.Juso;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface AddressResultNavigator extends BaseNavigator {
    void onSearch();
    void onDelete();
    void onBack();
    void onItemClick(Juso juso);
    void onUpdateCoordinate(Juso juso);
}
