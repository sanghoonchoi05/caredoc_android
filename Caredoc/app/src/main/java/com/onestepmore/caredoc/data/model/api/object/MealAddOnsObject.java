package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class MealAddOnsObject extends ItemResponse<MealAddOnsObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("code")
        private String code;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("calculation_count")
        private int calculationCount;
        @Expose
        @SerializedName("general_meal_added")
        private int generalMealAdded;
        @Expose
        @SerializedName("treatment_meal_grade")
        private int treatmentMealGrade;

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public int getCalculationCount() {
            return calculationCount;
        }

        public void setCalculationCount(int calculationCount) {
            this.calculationCount = calculationCount;
        }

        public int getGeneralMealAdded() {
            return generalMealAdded;
        }

        public void setGeneralMealAdded(int generalMealAdded) {
            this.generalMealAdded = generalMealAdded;
        }

        public int getTreatmentMealGrade() {
            return treatmentMealGrade;
        }

        public void setTreatmentMealGrade(int treatmentMealGrade) {
            this.treatmentMealGrade = treatmentMealGrade;
        }
    }
}
