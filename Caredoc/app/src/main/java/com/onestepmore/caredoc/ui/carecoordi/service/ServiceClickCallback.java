package com.onestepmore.caredoc.ui.carecoordi.service;

import com.onestepmore.caredoc.data.model.realm.statics.StaticService;

public interface ServiceClickCallback {
    void onClick(StaticService service);
}
