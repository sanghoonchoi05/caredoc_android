package com.onestepmore.caredoc.ui.main.taker.add.address;

import android.app.Application;
import android.databinding.ObservableField;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TakerServiceAddressViewModel extends BaseViewModel<TakerServiceAddressNavigator> {

    public TakerServiceAddressViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }
}
