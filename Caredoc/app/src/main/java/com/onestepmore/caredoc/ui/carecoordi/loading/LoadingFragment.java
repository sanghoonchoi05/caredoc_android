package com.onestepmore.caredoc.ui.carecoordi.loading;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.FCareCoordiLoadingBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiLoadingDialog;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.ui.carecoordi.result.ResultViewModel;

import javax.inject.Inject;
import javax.inject.Named;

public class LoadingFragment extends BaseCoordiFragment<FCareCoordiLoadingBinding, LoadingViewModel> implements LoadingNavigator {

    @Inject
    @Named("LoadingFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private LoadingViewModel mLoadingViewModell;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_care_coordi_loading;
    }

    @Override
    public LoadingViewModel getViewModel() {
        mLoadingViewModell = ViewModelProviders.of(this, mViewModelFactory).get(LoadingViewModel.class);
        return mLoadingViewModell;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoadingViewModell.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setTitle(int titleResId) {

    }

    @Override
    public void setStepData(StepData stepData) {
        if(stepData == null){
            return;
        }

        CareCoordiLoadingDialog dialog = new CareCoordiLoadingDialog();
        dialog.show(getBaseActivity().getSupportFragmentManager(), CareCoordiLoadingDialog.TAG);

        ((CareCoordiActivity)getBaseActivity()).loadCareCoordiResult();
    }
}
