package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.FacilityCommentResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceSummaryResponse;
import com.onestepmore.caredoc.data.model.api.review.CommentRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.CommentSaveRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewSaveRequest;
import com.rx2androidnetworking.Rx2ANRequest;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class ReviewApiHelperImp implements ReviewApiHelper{
    private ApiHeader mApiHeader;

    @Inject
    public ReviewApiHelperImp(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public Flowable<ReviewFacilityServiceSummaryResponse> getReviewServiceSummaryApiCall(ReviewFacilityRequest.PathParameter pathParameter) {
        Rx2ANRequest.GetRequestBuilder builder = Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_REVIEW_SUMMARY_V2);
        builder = builder.addHeaders(mApiHeader.getProtectedApiHeader());

        return builder.addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(ReviewFacilityServiceSummaryResponse.class);
    }

    @Override
    public Flowable<ReviewFacilityServiceResponse> getReviewServiceApiCall(ReviewFacilityRequest.PathParameter pathParameter) {
        Rx2ANRequest.GetRequestBuilder builder = Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_REVIEW_V2);
        builder = builder.addHeaders(mApiHeader.getProtectedApiHeader());

        return builder.addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(ReviewFacilityServiceResponse.class);
    }

    @Override
    public Flowable<FacilityCommentResponse> getCommentApiCall(ReviewFacilityRequest.PathParameter pathParameter) {
        Rx2ANRequest.GetRequestBuilder builder = Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_COMMENT);
        builder = builder.addHeaders(mApiHeader.getProtectedApiHeader());

        return builder.addPathParameter(pathParameter)
                .build()
                .getObjectFlowable(FacilityCommentResponse.class);
    }

    @Override
    public void saveCommentApiCall(CommentSaveRequest request, CommentSaveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_COMMENT_SAVE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addBodyParameter(request)
                .addPathParameter(parameter)
                .build()
                .getAsOkHttpResponse(listener);
    }

    @Override
    public void removeCommentApiCall(CommentRemoveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_COMMENT_REMOVE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(parameter)
                .build()
                .getAsOkHttpResponse(listener);
    }

    @Override
    public void saveReviewApiCall(JSONObject jsonRequest, ReviewSaveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_REVIEW_V2_SAVE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addJSONObjectBody(jsonRequest)
                .addPathParameter(parameter)
                .build()
                .getAsOkHttpResponse(listener);
    }

    @Override
    public void removeReviewApiCall(ReviewRemoveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_REVIEW_V2_REMOVE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(parameter)
                .build()
                .getAsOkHttpResponse(listener);
    }
}
