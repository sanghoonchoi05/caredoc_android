package com.onestepmore.caredoc.ui.estimate.request;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class EstimateRequestStartActivityModule {
    @Provides
    EstimateRequestStartViewModel estimateRequestStartViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new EstimateRequestStartViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideEstimateRequestStartViewModelProvider(EstimateRequestStartViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
