package com.onestepmore.caredoc.ui.base;

public interface TransactionCallback {
    void onAdded();

    void onShow();

    void onHide();
}
