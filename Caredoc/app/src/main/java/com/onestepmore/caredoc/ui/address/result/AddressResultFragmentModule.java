package com.onestepmore.caredoc.ui.address.result;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AddressResultFragmentModule {
    @Provides
    AddressResultViewModel addressResultViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new AddressResultViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("AddressResultFragment")
    ViewModelProvider.Factory provideAddressResultViewModelProvider(AddressResultViewModel addressResultViewModel) {
        return new ViewModelProviderFactory<>(addressResultViewModel);
    }
}
