package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.ProtectorObject;
import com.onestepmore.caredoc.utils.DateUtils;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Protector extends RealmObject implements Parcelable {
    @PrimaryKey
    private long id;
    private String name;
    private String photoUrl;
    private Date emailVerifiedAt;
    private String contact;
    private boolean isAdmin;
    private String userType;

    public Protector(){}

    protected Protector(Parcel in) {
        id = in.readLong();
        name = in.readString();
        photoUrl = in.readString();
        emailVerifiedAt = (java.util.Date) in.readSerializable();
        contact = in.readString();
        isAdmin = in.readByte() != 0;
        userType = in.readString();
    }

    public void setProtector(ProtectorObject object){
        if(object == null){
            return;
        }

        this.id = object.getId();
        this.name = object.getName();
        this.photoUrl = object.getPhotoUrl();
        this.emailVerifiedAt = DateUtils.getDate(object.getEmailVerifiedAt());
        this.contact = object.getContact();
        this.isAdmin = object.isAdmin();
        this.userType = object.getUserType();
    }

    public static final Creator<Protector> CREATOR = new Creator<Protector>() {
        @Override
        public Protector createFromParcel(Parcel in) {
            return new Protector(in);
        }

        @Override
        public Protector[] newArray(int size) {
            return new Protector[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(photoUrl);
        dest.writeSerializable(emailVerifiedAt);
        dest.writeString(contact);
        dest.writeByte((byte) (isAdmin ? 1 : 0));
        dest.writeString(userType);
    }
}
