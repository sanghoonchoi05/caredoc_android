package com.onestepmore.caredoc.ui.main.search.filter.service;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.databinding.FServiceFilterBinding;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.search.ChangeFilterCallback;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.filter.GradeFilterCallback;
import com.onestepmore.caredoc.ui.main.search.filter.GradeFilterDialog;
import com.onestepmore.caredoc.ui.main.search.filter.ServiceFilterAdapter;
import com.onestepmore.caredoc.ui.main.search.filter.ServiceFilterCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import static com.onestepmore.caredoc.ui.main.search.filter.GradeFilterDialog.SERVICE_FILTER;

public class ServiceFilterFragment extends BaseFragment<FServiceFilterBinding, ServiceFilterViewModel> implements
        ServiceFilterNavigator,
        GradeFilterCallback,
        ServiceFilterCallback
{
    @Inject
    @Named("ServiceFilterFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private ServiceFilterViewModel mServiceFilterViewModel;
    private List<ServiceFilter> mCachedServiceFilter;

    private ChangeFilterCallback mChangeFilterCallback;

    private int mSelectedGradeFilterIndex = -1;

    public void setChangeFilterCallback(ChangeFilterCallback changeFilterCallback) {
        this.mChangeFilterCallback = changeFilterCallback;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_service_filter;
    }

    @Override
    public ServiceFilterViewModel getViewModel() {
        mServiceFilterViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ServiceFilterViewModel.class);
        return mServiceFilterViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mServiceFilterViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SearchFragment fragment = parentFragment();
        if(fragment == null){
            return;
        }

        mCachedServiceFilter = fragment.getSelectedServicesFilter();

        ServiceFilterAdapter adapter = new ServiceFilterAdapter();
        adapter.setCallback(this);
        getViewDataBinding().fServiceFilterRecyclerView.setAdapter(adapter);
        getViewDataBinding().fServiceFilterRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        adapter.addAll(mCachedServiceFilter);
    }

    @Override
    public void onServiceCheck(ServiceFilter service, boolean checked) {
        service.setChecked(checked);
        getViewDataBinding().setChecked(checked);
        if(!checked){
            for(ServiceFilter filter : mCachedServiceFilter){
                if(filter.isChecked()){
                    getViewDataBinding().setChecked(true);
                    break;
                }
            }
        }
    }

    @Override
    public void onServiceGradeClick(int position) {
        mSelectedGradeFilterIndex = position;

        GradeFilterDialog dialog = GradeFilterDialog.getInstance();
        dialog.setClickListener(this);
        Bundle bundle = new Bundle();
        bundle.putParcelable(SERVICE_FILTER, mCachedServiceFilter.get(position));

        dialog.setArguments(bundle);
        dialog.show(getBaseActivity().getSupportFragmentManager(), getClass().getName());
    }

    @Override
    public void onClose() {
        ((MainActivity)getBaseActivity()).showBottomTab(true);
        BackStackManager.getInstance().onBackPressed();
    }

    @Override
    public void onInit() {
        for(ServiceFilter filter : mCachedServiceFilter){
            filter.setGradeText(getString(R.string.common_all_grade));
            filter.setChecked(false);
        }

        getViewDataBinding().setChecked(false);
        getViewDataBinding().fServiceFilterRecyclerView.getAdapter().notifyItemRangeChanged(0, mCachedServiceFilter.size());

        notifyApplyFilter();
    }

    @Override
    public void onApplyFilter() {
        Map<String, MapValue<?>> map = new HashMap<>();
        for(ServiceFilter serviceFilter : mCachedServiceFilter){
            if(serviceFilter.isChecked()){
                map.put(AmplitudeParam.Key.VALUE, new MapValue<>(serviceFilter.getServiceName()));
            }
        }
        onClose();

        notifyApplyFilter();
    }

    private void notifyApplyFilter(){
        SearchFragment fragment = parentFragment();
        if(fragment != null && mChangeFilterCallback != null){
            mChangeFilterCallback.onChangedFilter(false);
        }
    }

    private SearchFragment parentFragment(){
        return (SearchFragment)getBaseActivity().getSupportFragmentManager().findFragmentByTag(SearchFragment.class.getName());
    }

    @Override
    public void onGradeFilterClick(int position, String text) {
        ServiceFilter serviceFilter = mCachedServiceFilter.get(mSelectedGradeFilterIndex);
        String gradeText = text;
        StaticCategory category = RealmDataAccessor.getInstance().getStaticCategory(serviceFilter.getId());

        if(position > 0){
            String [] splitText = text.split(" ");
            gradeText = splitText[0];

            int index = 0;
            for(String grade : category.getGrades()){
                if(index == position){
                    break;
                }
                serviceFilter.addGrade(grade);
                index++;
            }
        }
        else{
            for(String grade : category.getGrades()){
                serviceFilter.addGrade(grade);
            }
        }
        serviceFilter.setGradeText(gradeText);

        getViewDataBinding().fServiceFilterRecyclerView.getAdapter().notifyItemRangeChanged(0, mCachedServiceFilter.size());
    }

    @Override
    public void onGradeFilterClose() {

    }

    @Override
    public void onGradeFilterInit() {

    }
}
