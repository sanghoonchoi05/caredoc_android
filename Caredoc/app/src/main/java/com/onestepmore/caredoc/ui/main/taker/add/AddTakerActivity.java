package com.onestepmore.caredoc.ui.main.taker.add;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.myhexaville.smartimagepicker.ImagePicker;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.ServiceAddressRequest.Address;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTypes;
import com.onestepmore.caredoc.databinding.VStepMainBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.main.taker.add.address.TakerServiceAddressFragment;
import com.onestepmore.caredoc.ui.main.taker.add.basic.TakerBasicFragment;
import com.onestepmore.caredoc.ui.main.taker.add.care.TakerCareFragment;
import com.onestepmore.caredoc.ui.main.taker.add.disease.TakerDiseaseFragment;
import com.onestepmore.caredoc.ui.main.taker.add.food.TakerFoodFragment;
import com.onestepmore.caredoc.ui.main.taker.add.performance.TakerPerformanceFragment;
import com.onestepmore.caredoc.ui.main.taker.add.remark.TakerRemarkFragment;
import com.onestepmore.caredoc.ui.widgets.progress.ProgressNavigator;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import io.realm.RealmResults;

import static com.theartofdev.edmodo.cropper.CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE;

public class AddTakerActivity extends BaseActivity<VStepMainBinding, AddTakerViewModel> implements
        AddTakerNavigator,
        ProgressNavigator,
        HasSupportFragmentInjector,
        ViewPager.OnPageChangeListener
{
    public enum STEP{
        BASIC(0, false),
        ADDRESS(1, true),
        CARE(2, true),
        PERFORMANCE(3, true),
        DISEASE(4, true),
        FOOD(5, true),
        REMARK(6, true);

        private int step;
        private boolean canSkip;

        STEP(int step, boolean canSkip){
            this.step = step;
            this.canSkip = canSkip;
        }

        public int getStep() {
            return step;
        }

        public boolean isCanSkip() {
            return canSkip;
        }
    }

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private AddTakerViewModel mAddTakerViewModel;

    private FragmentManager mFragmentManager;
    private AddTakerPagerAdapter mPagerAdapter;

    private STEP mCurrentStep = STEP.BASIC;
    private STEP mPrevStep = STEP.BASIC;

    private boolean isModify = false;
    private long mTakerId;

    private AddTakerRequest.Basic mCachedBasic;
    private List<Address> mCachedAddresses;
    private AddTakerRequest.Care mCachedCare;
    private AddTakerRequest.Performances mCachedPerformances;
    private AddTakerRequest.Nutritions mCachedNutritions;
    private AddTakerRequest.Remarks mCachedRemarks;
    private List<String> mCachedDiseases;
    private File mCachedProfileImageFile;

    private ImagePicker mImagePicker;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.v_step_main;
    }

    @Override
    public AddTakerViewModel getViewModel() {
        mAddTakerViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AddTakerViewModel.class);
        return mAddTakerViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddTakerViewModel.setNavigator(this);

        int step = -1;
        if(getIntent() != null){
            mTakerId = getIntent().getLongExtra("takerId", 0);
            step = getIntent().getIntExtra("step", -1);
            if(mTakerId > 0){
                isModify = true;

                Taker taker = getViewModel().getRealm().where(Taker.class).equalTo("fakeId", mTakerId).findFirst();
                initParameter(taker);
            }
        }

        initPager(step);

        getViewDataBinding().vStepMainView.setShowExit(true);
        checkBottomButton(isModify);
    }

    private void initParameter(Taker taker){
        if(taker == null){
            return;
        }

        mCachedBasic = taker.getBasic();
        mCachedCare = taker.getCare();
        mCachedPerformances = taker.getPerformance();
        mCachedNutritions = taker.getNutritions();
        mCachedRemarks = taker.getRemarks();
        mCachedDiseases = taker.getDiseaseList();
        mCachedAddresses = taker.getAddressList();
    }

    private void initPager(int step){
        mFragmentManager = getSupportFragmentManager();
        mPagerAdapter = new AddTakerPagerAdapter(mFragmentManager);
        boolean isModify = false;
        if(step < 0){
            mPagerAdapter.pushAddTakerFragment(TakerBasicFragment.newInstance(R.string.add_taker_basic_info, isModify));
            mPagerAdapter.pushAddTakerFragment(TakerServiceAddressFragment.newInstance(R.string.add_taker_address, isModify));
            mPagerAdapter.pushAddTakerFragment(TakerCareFragment.newInstance(R.string.add_taker_care_info, isModify));
            mPagerAdapter.pushAddTakerFragment(TakerPerformanceFragment.newInstance(R.string.add_taker_performance, isModify));
            mPagerAdapter.pushAddTakerFragment(TakerDiseaseFragment.newInstance(R.string.add_taker_disease, isModify));
            mPagerAdapter.pushAddTakerFragment(TakerFoodFragment.newInstance(R.string.add_taker_food, isModify));
            mPagerAdapter.pushAddTakerFragment(TakerRemarkFragment.newInstance(R.string.add_taker_remark, isModify));
        }else{
            isModify = true;
            if(step == STEP.BASIC.getStep()){
                mPagerAdapter.pushAddTakerFragment(TakerBasicFragment.newInstance(R.string.add_taker_basic_info_modify, isModify));
            }else if(step == STEP.ADDRESS.getStep()){
                mPagerAdapter.pushAddTakerFragment(TakerServiceAddressFragment.newInstance(R.string.add_taker_address_modify, isModify));
            }else if(step == STEP.CARE.getStep()){
                mPagerAdapter.pushAddTakerFragment(TakerCareFragment.newInstance(R.string.add_taker_care_info_modify, isModify));
            }else if(step == STEP.PERFORMANCE.getStep()){
                mPagerAdapter.pushAddTakerFragment(TakerPerformanceFragment.newInstance(R.string.add_taker_performance_modify, isModify));
            }else if(step == STEP.DISEASE.getStep()){
                mPagerAdapter.pushAddTakerFragment(TakerDiseaseFragment.newInstance(R.string.add_taker_disease_modify, isModify));
            }else if(step == STEP.FOOD.getStep()){
                mPagerAdapter.pushAddTakerFragment(TakerFoodFragment.newInstance(R.string.add_taker_food_modify, isModify));
            }else if(step == STEP.REMARK.getStep()){
                mPagerAdapter.pushAddTakerFragment(TakerRemarkFragment.newInstance(R.string.add_taker_remark_modify, isModify));
            }
        }

        getViewDataBinding().vStepMainView.setNavigator(this);
        getViewDataBinding().vStepMainView.vStepPager.setAdapter(mPagerAdapter);
        getViewDataBinding().vStepMainView.vStepProgressBar.setViewPager(
                getViewDataBinding().vStepMainView.vStepPager, STEP.values().length, 500);
        getViewDataBinding().vStepMainView.vStepProgressBar.setCurrentStep(mCurrentStep.getStep());
        getViewDataBinding().vStepMainView.vStepPager.addOnPageChangeListener(this);

        if(mPagerAdapter.getCount() < 2){
            getViewDataBinding().vStepMainView.vStepProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleError(Throwable throwable) {
        hideLoading();
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        hideLoading();
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }


    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onNextButtonClick() {
        if(mCurrentStep == null){
            return;
        }

        if(mCurrentStep.getStep() == STEP.values().length - 1){
            addTaker();
        }else{
            if(isValidData()){
                int nextStep = mCurrentStep.getStep() + 1;
                getViewDataBinding().vStepMainView.vStepPager.setCurrentItem(nextStep, true);
            }else{
                showInvalidDataPopup();
            }
        }
    }

    @Override
    public void onPrevButtonClick() {
        int prevStep = mCurrentStep.getStep() - 1;

        if(prevStep == -1){
            onExit();
            return;
        }
        getViewDataBinding().vStepMainView.vStepPager.setCurrentItem(prevStep, true);
    }

    @Override
    public void onBackPressed() {
        onPrevButtonClick();
    }

    private void addTaker(){
        showLoading();
        getViewModel().addTaker(buildAddTakerRequest(), getCachedProfileImageFile());
    }

    @Override
    public void onModifyButtonClick() {
        showLoading();
        getViewModel().modifyTaker(buildAddTakerRequest(), mTakerId);
    }

    @Override
    public void onCompleteAddTaker() {
        hideLoading();
        finish();
    }

    @Override
    public void onCompleteModifyTaker() {
        hideLoading();
    }

    private AddTakerRequest buildAddTakerRequest(){
        return new AddTakerRequest.Builder()
                .addBasic(getCachedBasic())
                .addCare(getCachedCare())
                .addDiseases(getCachedDiseases())
                .addPerformances(getCachedPerformances())
                .addNutritions(getCachedNutritions())
                .addRemarks(getCachedRemarks())
                .addAddresses(getCachedAddresses())
                .build();
    }

    public String getTypeCode(String typeName, StaticTypes.TYPE_ENUM category){
        String typeCode = null;
        if(CommonUtils.checkNullAndEmpty(typeName)){
            return typeCode;
        }

        RealmResults<StaticTypes> staticTypes = getViewModel().getRealm()
                .where(StaticTypes.class)
                .equalTo("category", category.toString())
                .findAll();

        for(StaticTypes types : staticTypes){
            if(types.getTypeName().equals(typeName)){
                typeCode = types.getTypeCode();
                break;
            }
        }

        return typeCode;
    }

    public String getTypeName(String typeCode, StaticTypes.TYPE_ENUM category){
        RealmResults<StaticTypes> staticTypes = getViewModel().getRealm()
                .where(StaticTypes.class)
                .equalTo("category", category.toString())
                .findAll();

        String typeName = "";
        for(StaticTypes types : staticTypes){
            if(types.getTypeCode().equals(typeCode)){
                typeName = types.getTypeName();
                break;
            }
        }

        return typeName;
    }

    @Override
    public void onExit() {
        String title = getString(R.string.add_taker_exit_dialog_title);
        String desc = getString(R.string.add_taker_exit_dialog_desc);
        if (isModify) {
            title = getString(R.string.modify_taker_exit_dialog_title);
            desc = getString(R.string.modify_taker_exit_dialog_desc);
        }

        showCommonDialog(title, desc, "", (view) -> finish(), getString(R.string.common_exit), getString(R.string.common_return));
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        for(STEP step : STEP.values()){
            if(step.getStep() == i){
                mPrevStep = mCurrentStep;
                mCurrentStep = step;
                break;
            }
        }

        //FirebaseAnalyticsUtils.setCurrentScreen(getFirebaseAnalytics(), this, mCurrentStep);

        checkBottomButton(false);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    public void checkBottomButton(boolean modify){
        showBottomButton(false, false, false, false);

        if(modify){
            showBottomButton(false, false, false, true);
        }else{
            boolean canSkip = mCurrentStep.isCanSkip();
            if(canSkip || mPrevStep.getStep() > mCurrentStep.getStep()){
                showBottomButton(false, false, true, false);
            }

            if(mCurrentStep == STEP.BASIC){
                showBottomButton(true, false, false, false);
            }

            setBothNextButtonText(getString(R.string.action_next));
            if(mCurrentStep == STEP.REMARK){
                setBothNextButtonText(getString(R.string.common_complete));
            }
        }
    }

    public void showBottomButton(boolean showNext, boolean showPrev, boolean showBoth, boolean showModify){
        getViewDataBinding().vStepMainView.setShowNext(showNext);
        getViewDataBinding().vStepMainView.setShowPrev(showPrev);
        getViewDataBinding().vStepMainView.setShowBoth(showBoth);
        getViewDataBinding().vStepMainView.setShowModify(showModify);
    }

    public void setBothNextButtonText(String text){
        getViewDataBinding().vStepMainView.vStepBothButtonAnchor.setNextText(text);
    }

    private boolean isValidData(){
        boolean valid = true;
        if(mCurrentStep == STEP.BASIC){
            valid = getCachedBasic().isValid();
        }

        return valid;
    }

    public AddTakerRequest.Basic getCachedBasic() {
        if(mCachedBasic == null){
            mCachedBasic = new AddTakerRequest.Basic();
        }
        return mCachedBasic;
    }

    public AddTakerRequest.Care getCachedCare() {
        if(mCachedCare == null){
            mCachedCare = new AddTakerRequest.Care();
        }
        return mCachedCare;
    }

    public AddTakerRequest.Performances getCachedPerformances() {
        if(mCachedPerformances == null){
            mCachedPerformances = new AddTakerRequest.Performances();
        }
        return mCachedPerformances;
    }

    public AddTakerRequest.Nutritions getCachedNutritions() {
        if(mCachedNutritions == null){
            mCachedNutritions = new AddTakerRequest.Nutritions();
        }
        return mCachedNutritions;
    }

    public AddTakerRequest.Remarks getCachedRemarks() {
        if(mCachedRemarks == null){
            mCachedRemarks = new AddTakerRequest.Remarks();
        }
        return mCachedRemarks;
    }

    public List<String> getCachedDiseases() {
        if(mCachedDiseases == null){
            mCachedDiseases = new ArrayList<>();
        }
        return mCachedDiseases;
    }

    public List<Address> getCachedAddresses() {
        if(mCachedAddresses == null){
            mCachedAddresses = new ArrayList<>();
        }
        return mCachedAddresses;
    }

    public File getCachedProfileImageFile() {
        return mCachedProfileImageFile;
    }

    public void setCachedProfileImageFile(File cachedProfileImageFile) {
        this.mCachedProfileImageFile = cachedProfileImageFile;
    }

    public void showInvalidDataPopup(){
        if(mCurrentStep == STEP.BASIC){
            showCommonDialog(getString(R.string.warning_msg_taker_basic_info_title), getString(R.string.warning_msg_taker_basic_info_desc));
        }
    }

    public void showTakerPhoto(ImagePicker imagePicker) {
        mImagePicker = imagePicker;
        mImagePicker.choosePicture(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_CHOOSER_REQUEST_CODE || requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            mImagePicker.handleActivityResult(resultCode,requestCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImagePicker.handlePermission(requestCode, grantResults);
    }
}
