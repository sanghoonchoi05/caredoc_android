package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class EvaluationSourceObject {
    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("agency_name")
    private String agencyName;

    @Expose
    @SerializedName("title")
    private String title;

    public int getId() {
        return id;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public String getTitle() {
        return title;
    }
}
