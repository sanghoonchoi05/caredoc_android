package com.onestepmore.caredoc.data.model.api.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class ReviewFacilityRequest {
    public enum TYPE{
        facility("facility"),
        facility_service("facility-service");

        private String type;
        TYPE(String type){
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
    public static class PathParameter {
        @Expose
        @SerializedName("morphType")
        private String morphType;

        @Expose
        @SerializedName("morphId")
        private long morphId;

        public long getMorphId() {
            return morphId;
        }

        public void setMorphId(long morphId) {
            this.morphId = morphId;
        }

        public String getMorphType() {
            return morphType;
        }

        public void setMorphType(String morphType) {
            this.morphType = morphType;
        }
    }
}
