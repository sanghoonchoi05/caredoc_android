package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class MedicalSubjectObject extends ItemResponse<MedicalSubjectObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("doctor_count")
        private int doctorCount;

        public String getName() {
            return name;
        }

        public int getDoctorCount() {
            return doctorCount;
        }
    }
}
