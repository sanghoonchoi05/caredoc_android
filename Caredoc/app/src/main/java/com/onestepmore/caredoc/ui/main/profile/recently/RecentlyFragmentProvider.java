package com.onestepmore.caredoc.ui.main.profile.recently;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class RecentlyFragmentProvider {
    @ContributesAndroidInjector(modules = RecentlyFragmentModule.class)
    abstract RecentlyFragment provideRecentlyFragmentFactory();
}
