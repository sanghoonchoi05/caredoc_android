package com.onestepmore.caredoc.ui.carecoordi;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiRequest;
import com.onestepmore.caredoc.data.model.api.object.AggregatesObject;
import com.onestepmore.caredoc.data.model.api.object.CareCoordiSearchFilter;
import com.onestepmore.caredoc.data.model.realm.ServiceAddress;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.databinding.VStepMainBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseDialog;
import com.onestepmore.caredoc.ui.carecoordi.classification.ClassificationFragment;
import com.onestepmore.caredoc.ui.carecoordi.guide.GuideFragment;
import com.onestepmore.caredoc.ui.carecoordi.loading.LoadingFragment;
import com.onestepmore.caredoc.ui.carecoordi.result.ResultFragment;
import com.onestepmore.caredoc.ui.carecoordi.service.ServiceFragment;
import com.onestepmore.caredoc.ui.carecoordi.userinfo.UserInfoFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.ui.widgets.progress.ProgressNavigator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class CareCoordiActivity extends BaseActivity<VStepMainBinding, CareCoordiViewModel> implements
        CareCoordiNavigator,
        ProgressNavigator,
        HasSupportFragmentInjector,
        ViewPager.OnPageChangeListener
{
    public enum STEP{
        NEED_HELP(0, false),
        NEED_WHAT(1, false),
        NEED_WHERE(2, false),
        NEED_SERVICE(3, false),
        SELECT_SERVICE(4, false),
        SELECT_CLASSIFICATION(5, true),
        USER_INFO(6, true),
        RESULT(7, true);

        private int step;
        private boolean canSkip;

        STEP(int step, boolean canSkip){
            this.step = step;
            this.canSkip = canSkip;
        }

        public int getStep() {
            return step;
        }

        public boolean isCanSkip() {
            return canSkip;
        }
    }

    public enum UI_ITEM{
        NEED_HELP(0, EXAMPLE.KNOW, EXAMPLE.DONT_KNOW),
        NEED_WHAT(1, EXAMPLE.MEDICAL_CARE, EXAMPLE.TAKE_CARE, EXAMPLE.TOOLS),
        NEED_WHERE(2, EXAMPLE.HOME, EXAMPLE.FACILITY),
        HOME_CARE(3, EXAMPLE.CARE, EXAMPLE.NURSING, EXAMPLE.BATH),
        CARE_PERIOD(4, EXAMPLE.DAY_AND_NIGHT, EXAMPLE.SHORT_TIME, EXAMPLE.LONG_TIME),
        SERVICE(5),
        DISEASE(6),
        TOOL(7),
        USER_INFO(8),
        RESULT(9);

        private List<EXAMPLE> exampleList;
        private int index;
        UI_ITEM(int index, EXAMPLE ... examples){
            this.index = index;

            if(examples.length == 0){
                return;
            }

            exampleList = new ArrayList<>();
            exampleList.addAll(Arrays.asList(examples));
        }

        public List<EXAMPLE> getExampleList() {
            return exampleList;
        }

        public int getIndex() {
            return index;
        }
    }

    public enum EXAMPLE {
        KNOW(5, R.string.care_coordi_example_know),                   // 네, 직접 선택할게요.
        DONT_KNOW(1, R.string.care_coordi_example_dont_know),       // 잘 모르겠어요. 어떤 서비스를 찾을지 도와주세요.
        MEDICAL_CARE(6, R.string.care_coordi_example_medical_care),   // 치료가 필요합니다.
        TAKE_CARE(2, R.string.care_coordi_example_take_care),      // 돌봄이 필요합니다.
        TOOLS(7, R.string.care_coordi_example_tools),                    // 노인 용품이 필요합니다.
        HOME(3, R.string.care_coordi_example_home),                 // 집으로 오셨으면 해요.
        FACILITY(4, R.string.care_coordi_example_facility),       // 시설에 모시고 싶어요.
        CARE(8, R.string.care_coordi_example_care),                    // 요양이 필요해요.
        NURSING(8, R.string.care_coordi_example_nursing),              // 간호가 필요해요.
        BATH(8, R.string.care_coordi_example_bath),                    // 목욕이 필요해요.
        DAY_AND_NIGHT(8, R.string.care_coordi_example_day_and_night),  // 반나절(주/야간)동안 필요해요.
        SHORT_TIME(8, R.string.care_coordi_example_short_time),        // 단기간(2주이내)동안 필요해요.
        LONG_TIME(8, R.string.care_coordi_example_long_time);          // 장기간(2주이상)동안 필요해요.

        private int textResId;
        private int nextUIItemIndex;

        EXAMPLE(int nextUIItemIndex, int textResId){
            this.textResId = textResId;
            this.nextUIItemIndex = nextUIItemIndex;
        }

        public int getTextResId() {
            return textResId;
        }

        public int getNextUIItemIndex() {
            return nextUIItemIndex;
        }
    }

    public static final int RESULT_CODE_CareGuide_ACTIVITY_FINISH = 99;

    public LinkedList<StepData> mSelectedStepDataList = new LinkedList<>();

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private CareCoordiViewModel mCareCoordiViewModel;

    private FragmentManager mFragmentManager;
    private CareCoordiPagerAdapter mPagerAdapter;

    private CareCoordiSearchFilter.TakerInfo mCachedTakerInfo = new CareCoordiSearchFilter.TakerInfo("");
    private CareCoordiSearchFilter.Classification mCachedClassification = new CareCoordiSearchFilter.Classification();
    private List<CareCoordiSearchFilter.Service> mCachedServices = new ArrayList<>();

    private List<ServiceAddress> mCachedServiceAddress = new ArrayList<>();

    public List<ServiceAddress> getCachedServiceAddress() {
        return mCachedServiceAddress;
    }

    public boolean isExistCachedServiceAddress(){
        if(getCachedServiceAddress() == null || getCachedServiceAddress().size() == 0){
            return false;
        }

        return true;
    }

    public void setCachedService(CareCoordiSearchFilter.Service cachedService) {
        mCachedServices.clear();
        mCachedServices.add(cachedService);
    }

    public void setCachedCategory(CareCoordiSearchFilter.CATEGORY mCachedCategory) {
        this.mCachedCategory = mCachedCategory;
    }

    private CareCoordiSearchFilter.CATEGORY mCachedCategory = CareCoordiSearchFilter.CATEGORY.GERIATRIC_HOSPITAL;

    private STEP mCurrentStep = STEP.NEED_HELP;
    private STEP mPrevStep = STEP.NEED_HELP;
    private UI_ITEM mSelectedNextUIItem = UI_ITEM.NEED_WHAT;

    public void setSelectedNextUIItem(UI_ITEM selectedNextUIItem) {
        this.mSelectedNextUIItem = selectedNextUIItem;

        showBottomButton(false, false, true);
        if(mCurrentStep == STEP.NEED_HELP){
            showBottomButton(true, false, false);
        }
    }

    public LinkedList<StepData> getSelectedStepDataList() {
        return mSelectedStepDataList;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.v_step_main;
    }

    @Override
    public CareCoordiViewModel getViewModel() {
        mCareCoordiViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CareCoordiViewModel.class);
        return mCareCoordiViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCareCoordiViewModel.setNavigator(this);

        mFragmentManager = getSupportFragmentManager();

        initPager();
        initStepData();
        checkBottomButton();
        checkExitButton();
    }

    private void initPager(){
        mPagerAdapter = new CareCoordiPagerAdapter(mFragmentManager);
        mPagerAdapter.addCareCoordiFragment(new GuideFragment());
        mPagerAdapter.addCareCoordiFragment(new GuideFragment());
        mPagerAdapter.addCareCoordiFragment(new GuideFragment());
        mPagerAdapter.addCareCoordiFragment(new GuideFragment());
        mPagerAdapter.addCareCoordiFragment(new ServiceFragment());
        mPagerAdapter.addCareCoordiFragment(new ClassificationFragment());
        mPagerAdapter.addCareCoordiFragment(new UserInfoFragment());
        mPagerAdapter.addCareCoordiFragment(new LoadingFragment());

        getViewDataBinding().vStepMainView.setNavigator(this);

        getViewDataBinding().vStepMainView.vStepPager.setAdapter(mPagerAdapter);

        getViewDataBinding().vStepMainView.vStepProgressBar.setViewPager(
                getViewDataBinding().vStepMainView.vStepPager, STEP.values().length, 500);
        getViewDataBinding().vStepMainView.vStepProgressBar.setCurrentStep(mCurrentStep.getStep());
        getViewDataBinding().vStepMainView.vStepPager.addOnPageChangeListener(this);
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    public void onBackPressed() {
        onPrevButtonClick();
    }

    @Override
    public void onNextButtonClick() {
        if(mCurrentStep == null || mSelectedNextUIItem == null){
            return;
        }

        if(mCurrentStep.getStep() == STEP.values().length - 1){
            setResult(RESULT_CODE_CareGuide_ACTIVITY_FINISH);
            finish();
        }
        else{
            StepData nextStepData = pushNextStep();
            if(nextStepData != null){
                getViewDataBinding().vStepMainView.vStepPager.setCurrentItem(nextStepData.getStep().getStep(), true);
            }
        }
    }

    @Override
    public void onPrevButtonClick() {
        popStep();

        if(mCurrentStep == null){
            finish();
            return;
        }

        StepData currentStepData = mSelectedStepDataList.peek();
        if(currentStepData != null){
            getViewDataBinding().vStepMainView.vStepPager.setCurrentItem(currentStepData.getStep().getStep(), true);
        }
    }

    @Override
    public void onModifyButtonClick() {
        // not used.
    }

    public void checkBottomButton(){
        boolean canSkip = mCurrentStep.isCanSkip();

        showBottomButton(false, true, false);
        if(canSkip || mPrevStep.getStep() > mCurrentStep.getStep()){
            showBottomButton(false, false, true);
        }

        if(mCurrentStep == STEP.RESULT || mCurrentStep == STEP.NEED_HELP){
            showBottomButton(false, false, false);
        }
    }

    public void checkExitButton(){
        getViewDataBinding().vStepMainView.setShowExit(mCurrentStep.getStep() != STEP.RESULT.getStep());
    }

    public void showBottomButton(boolean showNext, boolean showPrev, boolean showBoth){
        getViewDataBinding().vStepMainView.setShowNext(showNext);
        getViewDataBinding().vStepMainView.setShowPrev(showPrev);
        getViewDataBinding().vStepMainView.setShowBoth(showBoth);
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int step) {
        checkBottomButton();
        checkExitButton();

        checkSelectedNextUIItem(mSelectedStepDataList.peek());

        //FirebaseAnalyticsUtils.setCurrentScreen(getFirebaseAnalytics(), this, mSelectedNextUIItem);

        if(mPagerAdapter != null){
            BaseCoordiFragment coordiFragment = mPagerAdapter.getItem(step);
            coordiFragment.setStepData(mSelectedStepDataList.peek());
        }
    }

    private void checkSelectedNextUIItem(StepData stepData){
        if(stepData == null){
            return;
        }

        if(stepData instanceof GuideData){
            List<ExampleData> exampleDataList = ((GuideData)stepData).getExampleDataList();
            for(ExampleData exampleData : exampleDataList){
                if(exampleData.isSelected()){
                    showBottomButton(false, false, true);
                    if(mCurrentStep == STEP.NEED_HELP){
                        showBottomButton(true, false, false);
                    }

                    for(CareCoordiActivity.UI_ITEM uiItem : CareCoordiActivity.UI_ITEM.values()){
                        if(exampleData.getExample().getNextUIItemIndex() == uiItem.getIndex()){
                            mSelectedNextUIItem = uiItem;
                            break;
                        }
                    }
                    break;
                }
            }
        }else{

        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    public void checkClassificationItem(UI_ITEM uiItem, String classificationItemCode, String classificationItemName, boolean isChecked){
        if(uiItem == UI_ITEM.DISEASE){
            if(isChecked){
                mCachedClassification.getDiseases().add(classificationItemCode);
                mCachedClassification.getDiseasesName().add(classificationItemName);
            }else{
                mCachedClassification.getDiseases().remove(classificationItemCode);
                mCachedClassification.getDiseasesName().remove(classificationItemName);
            }
        }else if(uiItem == UI_ITEM.TOOL){
            if(isChecked){
                mCachedClassification.getWelfareTools().add(classificationItemCode);
                mCachedClassification.getWelfareToolsName().add(classificationItemName);
            }else{
                mCachedClassification.getWelfareTools().remove(classificationItemCode);
                mCachedClassification.getWelfareToolsName().add(classificationItemName);
            }
        }
    }

    private void initStepData(){
        GuideData firstStepData = new GuideData();
        firstStepData.setStep(STEP.NEED_HELP);
        firstStepData.setUIItem(UI_ITEM.NEED_HELP);
        firstStepData.setExampleDataList(makeExampleList(UI_ITEM.NEED_HELP));
        mSelectedStepDataList.push(firstStepData);
    }

    private StepData pushNextStep(){
        StepData nextStepData = null;
        if(mCurrentStep == STEP.USER_INFO){
            nextStepData = new StepData();
            nextStepData.setStep(STEP.RESULT);
            nextStepData.setUIItem(UI_ITEM.RESULT);
        }else if(mCurrentStep == STEP.NEED_HELP){
            if(mSelectedNextUIItem == UI_ITEM.SERVICE){
                // 서비스를 안다.
                nextStepData = new StepData();
                nextStepData.setStep(STEP.SELECT_SERVICE);
                nextStepData.setUIItem(UI_ITEM.SERVICE);
            }else if(mSelectedNextUIItem == UI_ITEM.NEED_WHAT){
                // 서비스를 모른다.
                nextStepData = new GuideData();
                nextStepData.setStep(STEP.NEED_WHAT);
                nextStepData.setUIItem(UI_ITEM.NEED_WHAT);
                ((GuideData) nextStepData).setExampleDataList(makeExampleList(UI_ITEM.NEED_WHAT));
            }
        }else if(mCurrentStep == STEP.NEED_WHAT){
            if(mSelectedNextUIItem == UI_ITEM.DISEASE){
                // 치료다.
                nextStepData = new StepData();
                nextStepData.setStep(STEP.SELECT_CLASSIFICATION);
                nextStepData.setUIItem(UI_ITEM.DISEASE);
            }else if(mSelectedNextUIItem == UI_ITEM.NEED_WHERE){
                // 돌봄이다.
                nextStepData = new GuideData();
                nextStepData.setStep(STEP.NEED_WHERE);
                nextStepData.setUIItem(UI_ITEM.NEED_WHERE);
                ((GuideData) nextStepData).setExampleDataList(makeExampleList(UI_ITEM.NEED_WHERE));
            }else if(mSelectedNextUIItem == UI_ITEM.TOOL){
                // 복지용구다.
                nextStepData = new StepData();
                nextStepData.setStep(STEP.SELECT_CLASSIFICATION);
                nextStepData.setUIItem(UI_ITEM.TOOL);
            }
        }else if(mCurrentStep == STEP.NEED_WHERE){
            if(mSelectedNextUIItem == UI_ITEM.HOME_CARE){
                // 돌봄이다.
                nextStepData = new GuideData();
                nextStepData.setStep(STEP.NEED_SERVICE);
                nextStepData.setUIItem(UI_ITEM.HOME_CARE);
                ((GuideData) nextStepData).setExampleDataList(makeExampleList(UI_ITEM.HOME_CARE));
            }else if(mSelectedNextUIItem == UI_ITEM.CARE_PERIOD){
                // 복지용구다.
                nextStepData = new GuideData();
                nextStepData.setStep(STEP.NEED_SERVICE);
                nextStepData.setUIItem(UI_ITEM.CARE_PERIOD);
                ((GuideData) nextStepData).setExampleDataList(makeExampleList(UI_ITEM.CARE_PERIOD));
            }
        }else if(mCurrentStep == STEP.NEED_SERVICE){
            nextStepData = new StepData();
            nextStepData.setStep(STEP.USER_INFO);
            nextStepData.setUIItem(UI_ITEM.USER_INFO);
        }else if(mCurrentStep == STEP.SELECT_SERVICE){
            if(mSelectedNextUIItem == UI_ITEM.DISEASE){
                // 치료다.
                nextStepData = new StepData();
                nextStepData.setStep(STEP.SELECT_CLASSIFICATION);
                nextStepData.setUIItem(UI_ITEM.DISEASE);
            }else if(mSelectedNextUIItem == UI_ITEM.RESULT){
                // 돌봄이다.
                nextStepData = new StepData();
                nextStepData.setStep(STEP.USER_INFO);
                nextStepData.setUIItem(UI_ITEM.USER_INFO);
            }else if(mSelectedNextUIItem == UI_ITEM.TOOL){
                // 복지용구다.
                nextStepData = new StepData();
                nextStepData.setStep(STEP.SELECT_CLASSIFICATION);
                nextStepData.setUIItem(UI_ITEM.TOOL);
            }
        }else if(mCurrentStep == STEP.SELECT_CLASSIFICATION){
            nextStepData = new StepData();
            nextStepData.setStep(STEP.USER_INFO);
            nextStepData.setUIItem(UI_ITEM.USER_INFO);
        }else if(mCurrentStep == STEP.RESULT){

        }

        if(nextStepData != null){
            mPrevStep = mCurrentStep;
            mCurrentStep = nextStepData.getStep();
            mSelectedStepDataList.push(nextStepData);
        }

        return nextStepData;
    }

    private void popStep(){
        // 상세 분류 스텝이였다면
        if(mCurrentStep == STEP.SELECT_CLASSIFICATION){
            mCachedClassification.getDiseases().clear();
            mCachedClassification.getDiseasesName().clear();
            mCachedClassification.getWelfareTools().clear();
            mCachedClassification.getWelfareToolsName().clear();
        }

        mPrevStep = mCurrentStep;

        mSelectedStepDataList.pop();

        mCurrentStep = null;
        if(mSelectedStepDataList.peek() != null){
            mCurrentStep = mSelectedStepDataList.peek().getStep();
        }
    }

    private List<ExampleData> makeExampleList(UI_ITEM uiItem){
        List<ExampleData> exampleDataList = new ArrayList<>();
        if(uiItem.getExampleList() != null && uiItem.getExampleList().size() > 0){
            for(EXAMPLE example : uiItem.getExampleList()){
                ExampleData exampleData = new ExampleData();
                exampleData.setExample(example);
                exampleData.setSelected(false);
                exampleDataList.add(exampleData);
            }
        }
        return exampleDataList;
    }

    public void loadCareCoordiResult(){
        getViewModel().loadCareCoordiResult(getFilter());
    }

    public void loadCareCoordiResult(int skip){
        getViewModel().setSkip(skip);
        getViewModel().loadCareCoordiResult(getFilter());
    }

    public CareCoordiSearchFilter.TakerInfo getCachedTakerInfo() {
        return mCachedTakerInfo;
    }

    public void setTakerBirthInfo(String birth){
        mCachedTakerInfo.setBirth(birth);
    }

    public void setTakerLtiInfo(boolean lti){
        mCachedTakerInfo.setLti(lti);
    }

    public void selectStaticService(StaticService staticService){
        checkCategoryAndService(staticService);

        if(staticService.getServiceType().equals(StaticService.SERVICE_TYPE.HOSPITAL)){
            setSelectedNextUIItem(CareCoordiActivity.UI_ITEM.DISEASE);
        }else if(staticService.getServiceType().equals(StaticService.SERVICE_TYPE.WELFARE_TOOL)) {
            setSelectedNextUIItem(CareCoordiActivity.UI_ITEM.TOOL);
        }else{
            setSelectedNextUIItem(CareCoordiActivity.UI_ITEM.RESULT);
        }
    }

    public void selectExampleData(ExampleData exampleData){
        checkCategoryAndService(exampleData.getExample());

        for(CareCoordiActivity.UI_ITEM uiItem : CareCoordiActivity.UI_ITEM.values()){
            if(exampleData.getExample().getNextUIItemIndex() == uiItem.getIndex()){
                setSelectedNextUIItem(uiItem);
                break;
            }
        }
    }

    private void checkCategoryAndService(StaticService staticService){
        if(staticService.getServiceType().equals(StaticService.SERVICE_TYPE.HOSPITAL)){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.GERIATRIC_HOSPITAL);
        }else if(staticService.getServiceType().equals(StaticService.SERVICE_TYPE.NURSING_HOME) ||
                staticService.getServiceType().equals(StaticService.SERVICE_TYPE.NURSING_HOME_WHO_DEMENTIA)) {
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.NURSING_HOME);
        }else{
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.VISIT_HOME);
        }

        setCachedService(new CareCoordiSearchFilter.Service(staticService.getId()));
    }

    private void checkCategoryAndService(EXAMPLE example){
        if(example == EXAMPLE.MEDICAL_CARE){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.GERIATRIC_HOSPITAL);
            setCachedService(new CareCoordiSearchFilter.Service(1));
        }else if(example == EXAMPLE.TOOLS){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.VISIT_HOME);
            setCachedService(new CareCoordiSearchFilter.Service(9));
        }else if(example == EXAMPLE.CARE){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.VISIT_HOME);
            setCachedService(new CareCoordiSearchFilter.Service(4));
        }else if(example == EXAMPLE.NURSING){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.VISIT_HOME);
            setCachedService(new CareCoordiSearchFilter.Service(8));
        }else if(example == EXAMPLE.BATH){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.VISIT_HOME);
            setCachedService(new CareCoordiSearchFilter.Service(5));
        }else if(example == EXAMPLE.DAY_AND_NIGHT){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.VISIT_HOME);
            setCachedService(new CareCoordiSearchFilter.Service(6));
        }else if(example == EXAMPLE.SHORT_TIME){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.VISIT_HOME);
            setCachedService(new CareCoordiSearchFilter.Service(7));
        }else if(example == EXAMPLE.LONG_TIME){
            setCachedCategory(CareCoordiSearchFilter.CATEGORY.NURSING_HOME);
            setCachedService(new CareCoordiSearchFilter.Service(2));
        }
    }

    private CareCoordiSearchFilter getFilter(){
        return new CareCoordiSearchFilter.FilterBuilder()
                .addTakerInfo(mCachedTakerInfo)
                .addCategory(mCachedCategory.getName())
                .addClassifications(mCachedClassification)
                .addServices(mCachedServices)
                .build();
    }

    @Override
    public void onResult(AggregatesObject aggregatesObject, int totalCount){
        Fragment fragment = mFragmentManager.findFragmentByTag(CareCoordiLoadingDialog.TAG);
        if(fragment != null){
            if(fragment instanceof BaseDialog){
                ((BaseDialog)fragment).dismissDialog(CareCoordiLoadingDialog.TAG);
            }
        }

        Fragment resultFragment = mFragmentManager.findFragmentByTag(ResultFragment.class.getName());
        if(resultFragment == null){
            if(mCachedServices.size() > 0 && mCachedCategory != null){
                CareCoordiSearchFilter.Service service = mCachedServices.get(0);
                StaticService staticService = getViewModel().getRealm().where(StaticService.class)
                        .equalTo("id", service.getId())
                        .findFirst();
                if(staticService != null){

                    List<String> classificationList = mCachedClassification.getDiseasesName();
                    if(classificationList.size() == 0){
                        classificationList = mCachedClassification.getWelfareToolsName();
                    }

                    int price = 0;
                    if(aggregatesObject != null){
                        price = aggregatesObject.getExpensesAvg();
                    }

                    List<String> orderList = new ArrayList<>();
                    for(CareCoordiRequest.ORDER_BY orderby : CareCoordiRequest.ORDER_BY.values()){
                        if(orderby.isCommon()){
                            orderList.add(getString(orderby.getStrId()));
                        }
                    }
                    if(staticService.getServiceType().equals(StaticService.SERVICE_TYPE.HOSPITAL)){
                        orderList.add(getString(R.string.care_coordi_result_order_price_asc));
                        orderList.add(getString(R.string.care_coordi_result_order_price_desc));
                    }else if(staticService.getServiceType().equals(StaticService.SERVICE_TYPE.NURSING_HOME_WHO_DEMENTIA) ||
                            staticService.getServiceType().equals(StaticService.SERVICE_TYPE.PROTECT_WHO_DEMENTIA_FOR_DAY_AND_NIGHT)){
                        orderList.add(getString(R.string.care_coordi_result_order_scale_desc));
                        orderList.add(getString(R.string.care_coordi_result_order_scale_asc));
                    }

                    //TODO
                    BackStackManager.getInstance().addRootFragment(
                            this,
                            ResultFragment.newInstance(
                                    staticService.getTypeName(),
                                    "",
                                    classificationList,
                                    totalCount,
                                    price,
                                    orderList),
                            R.id.v_step_child_fragment_layout);
                }
            }
        }
    }

    @Override
    public void onExit() {
        showCommonDialog(
                getString(R.string.care_coordi_exit_dialog_title),
                getString(R.string.care_coordi_exit_dialog_desc),
                CommonYNDialog.class.getName(),
                (view) -> finish(),
                getString(R.string.common_return),
                getString(R.string.common_exit)
        );
    }

    public void setTaker(Taker taker){
        if(taker == null){
            return;
        }

        mCachedTakerInfo = taker.getTakerInfo();
        mCachedServiceAddress = taker.getServiceAddresses();

        if(mCachedServiceAddress != null && mCachedServiceAddress.size() > 0){
            ServiceAddress firstAddress = mCachedServiceAddress.get(0);
            if(firstAddress != null){
                mCachedTakerInfo.setAddressName(firstAddress.getAddressName());
                String currentPosition = firstAddress.getLatLng().getLatitude() + "," + firstAddress.getLatLng().getLongitude();
                getViewModel().setCurrentPosition(currentPosition);
            }
        }
    }
}
