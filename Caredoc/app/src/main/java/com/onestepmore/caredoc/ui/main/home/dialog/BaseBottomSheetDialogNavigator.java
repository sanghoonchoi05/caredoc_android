package com.onestepmore.caredoc.ui.main.home.dialog;

public interface BaseBottomSheetDialogNavigator {
    void onClose();
    void onItemClick(Object object);
}
