package com.onestepmore.caredoc.ui.main.search.filter;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.databinding.DialogGradeFilterBinding;
import com.onestepmore.caredoc.databinding.IDialogGradeFilterBinding;
import com.onestepmore.caredoc.ui.base.BaseBottomSheetDialog;
import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilter;

public class GradeFilterDialog extends BaseBottomSheetDialog{

    public static final String SERVICE_FILTER = "service_filter";

    public static GradeFilterDialog getInstance() {
        return new GradeFilterDialog();
    }

    String [] textList = null;

    GradeFilterCallback listener = null;
    DialogGradeFilterBinding mBinding;

    private ServiceFilter mCachedServiceFilter;

    public void setClickListener(GradeFilterCallback listener){
        this.listener = listener;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_grade_filter, container, false);

        Bundle bundle = getArguments();
        if(bundle != null){
            mCachedServiceFilter = bundle.getParcelable(SERVICE_FILTER);
            if(mCachedServiceFilter != null){
                StaticCategory category = RealmDataAccessor.getInstance().getStaticCategory(mCachedServiceFilter.getId());
                textList = new String[category.getGrades().size() + 1];

                for(int i=0; i<textList.length; i++){
                    if(i == 0){
                        textList[i] = getString(R.string.common_all_grade);
                    }
                    else if(i==1){
                        textList[i] = String.format(getResources().getString(R.string.grade_filter_list_formatted1), category.getGrades().get(i-1));
                    }else{
                        textList[i] = String.format(getResources().getString(R.string.grade_filter_list_formatted2), category.getGrades().get(0),category.getGrades().get(i-1));
                    }
                }

                RecyclerView recyclerView = mBinding.dialogBottomSheetRecyclerview;
                BottomSheetDialogAdapter adapter = new BottomSheetDialogAdapter();
                adapter.setTextList(textList);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                mBinding.dialogBottomSheetInit.setOnClickListener(this::onGradeFilterInit);
                mBinding.dialogBottomSheetCloseImg.setOnClickListener(this::onGradeFilterClose);
            }
        }

        return mBinding.getRoot();
    }

    public void onGradeFilterClick(int position, String text) {
        if(listener != null){
            listener.onGradeFilterClick(position, text);
        }
        dismiss();
    }

    public void onGradeFilterClose(View view) {
        if(listener != null){
            listener.onGradeFilterClose();
        }
        dismiss();
    }

    public void onGradeFilterInit(View view) {
        if(listener != null){
            listener.onGradeFilterInit();
        }
        dismiss();
    }

    public class BottomSheetDialogAdapter extends RecyclerView.Adapter<BottomSheetDialogAdapter.BottomSheetDialogViewHolder>
    {
        String [] mTextList;

        public void setTextList(final String [] textList){
            mTextList = textList;
            notifyItemRangeInserted(0, mTextList.length);
        }

        @NonNull
        @Override
        public BottomSheetDialogViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            IDialogGradeFilterBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_dialog_grade_filter, viewGroup, false);
            return new BottomSheetDialogViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull BottomSheetDialogViewHolder bottomSheetDialogViewHolder, int position) {
            bottomSheetDialogViewHolder.itemView.setSelected(false);
            bottomSheetDialogViewHolder.binding.setText(mTextList[position]);
            bottomSheetDialogViewHolder.binding.setPosition(position);
            bottomSheetDialogViewHolder.binding.executePendingBindings();
        }

        @Override
        public int getItemCount() {
            return mTextList == null ? 0 : mTextList.length;
        }

        class BottomSheetDialogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            final IDialogGradeFilterBinding binding;

            private BottomSheetDialogViewHolder(IDialogGradeFilterBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
                binding.iDialogGradeFilterLayout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                onGradeFilterClick(binding.getPosition(), binding.getText());
            }
        }
    }
}
