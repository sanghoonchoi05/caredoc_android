package com.onestepmore.caredoc.utils;

import android.content.Context;

import com.onestepmore.caredoc.ui.AppLogger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * @author Rajan Maurya
 *         On 06/07/17.
 */
public class DateUtils {

    public static final String LOG_TAG = DateUtils.class.getSimpleName();

    public static final String SCHEDULE_DATE_TIME_FORMAT = "M.dd";
    public static final String STANDARD_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String SUMMARY_DATE_TIME_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy MMM dd HH:mm:ss";
    public static final String OUTPUT_DATE_FORMAT = "yyyy MMM dd";
    public static final String INPUT_DATE_FORMAT = "yyyy-MM-dd'Z'";
    public static final String ACTIVITIES_DATE_FORMAT = "MMM dd, yyyy, HH:mm:ss a";
    public static final String BIRTH_DATE_FORMAT = "yyyy.MM.dd";

    public static final String FACILITY_DETAIL_TIME_INPUT_DATE_FORMAT = "HH:mm:ss";
    public static final String FACILITY_DETAIL_TIME_OUTPUT_DATE_FORMAT = "HH:mm";

    // 실제 만료날짜 보다 5일전에 리프레시 한다.(실제 만료날짜는 발급 후 15일)
    private static final long ANTEDATE_TOKEN_EXPIRE_TIME_FOR_DAY = 5L;

    public static String getTokenExpireDate(String remainTimeSec){
        return getTokenExpireDate(TimeUnit.SECONDS.toMillis(Long.parseLong(remainTimeSec)));
    }

    public static String getTokenExpireDate(long remainTimeSec){
        SimpleDateFormat format = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT, Locale.KOREA);

        long expireTimeMillis = System.currentTimeMillis() + remainTimeSec;
        Date date = new Date(expireTimeMillis);

        return format.format(date);
    }

    /**
     * Format date time string into "2013 Feb 28 13:24:56" format.
     *
     * @param dateString Standard Date Time String from server
     * @return String of Date time format 2013 Feb 28 13:24:56
     */
    public static String getDateTime(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT, Locale.KOREA);
        Date date = new Date();
        try {
            date = format.parse(dateString);
        } catch (ParseException e) {
            AppLogger.i(LOG_TAG, e.getLocalizedMessage());
        }
        format = new SimpleDateFormat(DATE_TIME_FORMAT, Locale.KOREA);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return format.format(calendar.getTime());
    }

    public static long getDateTime(String dateString, String inputFormat) {
        if(dateString == null || dateString.isEmpty())
            return 0;

        SimpleDateFormat format = new SimpleDateFormat(inputFormat, Locale.KOREA);
        Date date;
        try {
            date = format.parse(dateString);
            return date.getTime();
        } catch (ParseException e) {
            AppLogger.i(LOG_TAG, e.getLocalizedMessage());
        }

        return 0;
    }

    public static boolean isTokenExpired(String tokenExpiration) {
        if(tokenExpiration.isEmpty())
            return true;

        SimpleDateFormat format = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT, Locale.KOREA);
        Date date;
        try {
            date = format.parse(tokenExpiration);
            if (System.currentTimeMillis() > date.getTime() - TimeUnit.DAYS.toMillis(ANTEDATE_TOKEN_EXPIRE_TIME_FOR_DAY)) {
                return true;
            }
        } catch (ParseException e) {
            AppLogger.i(LOG_TAG, e.getLocalizedMessage());
        }
        return false;
    }

    public static String getDate(String dateString, String inputFormat, String outFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(inputFormat, Locale.KOREA);
        Date date = new Date();
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            AppLogger.i(LOG_TAG, e.getLocalizedMessage());
        }
        dateFormat = new SimpleDateFormat(outFormat, Locale.KOREA);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return dateFormat.format(calendar.getTime());
    }

    public static String getDateInUTC(Calendar time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT,
                Locale.KOREA);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(time.getTime());
    }

    public static String getCurrentDateStr() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(OUTPUT_DATE_FORMAT,
                Locale.KOREA);
        Calendar calendar = Calendar.getInstance();
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String convertServerDate(Calendar calendar) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(OUTPUT_DATE_FORMAT,
                Locale.KOREA);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getSummaryDateTimeFormat(Calendar calendar) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(SUMMARY_DATE_TIME_FORMAT,
                Locale.KOREA);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static Date getDate(String dateTime){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT,
                Locale.KOREA);
        Date date = null;
        try{
            date = simpleDateFormat.parse(dateTime);
        }catch (ParseException e){

        }

        return date;
    }

    public static Date getDate(String dateTime, String outputFormat){
        if(CommonUtils.checkNullAndEmpty(dateTime)){
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputFormat,
                Locale.KOREA);
        Date date = null;
        try{
            date = simpleDateFormat.parse(dateTime);
        }catch (ParseException e){
        }

        return date;
    }

    public static Date getDate(long dateTime){
        Date date = new Date(dateTime);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT,
                Locale.KOREA);

        try{
            date = simpleDateFormat.parse(simpleDateFormat.format(date));
        }catch (ParseException e){

        }

        return date;
    }

    public static String getStandardFormatTime(Date dateTime){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT,
                Locale.KOREA);

        return simpleDateFormat.format(dateTime);
    }

    public static String getDateFormatTime(Date dateTime, String outputFormat){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputFormat,
                Locale.KOREA);

        return simpleDateFormat.format(dateTime);
    }


    public static String getSummaryFormatTime(Date dateTime){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(SUMMARY_DATE_TIME_FORMAT,
                Locale.KOREA);

        return simpleDateFormat.format(dateTime);
    }

    public static Date getCurrentDate() {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STANDARD_DATE_TIME_FORMAT,
                Locale.KOREA);

        try{
            date = simpleDateFormat.parse(simpleDateFormat.format(date));
        }catch (ParseException e){

        }

        return date;
    }

    public static Calendar getCurrentCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.getCurrentDate());

        return calendar;
    }


    public static Date getSummaryDate(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat(SUMMARY_DATE_TIME_FORMAT, Locale.KOREA);
        String summaryDateString = getSummaryFormatTime(date);
        Date summaryDate = null;
        try {
            summaryDate = dateFormat.parse(summaryDateString);
        } catch (ParseException e) {
            AppLogger.i(LOG_TAG, e.getLocalizedMessage());
        }

        return summaryDate;
    }

    public static Date getCurrentSummaryDate(){
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(SUMMARY_DATE_TIME_FORMAT,
                Locale.KOREA);

        try{
            date = simpleDateFormat.parse(simpleDateFormat.format(date));
        }catch (ParseException e){

        }

        return date;
    }

    public static int getCalendarNumber(Date time, int field){
        int number = 0;
        if(time != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(time);
            number = cal.get(field);
        }
        return number;
    }
}
