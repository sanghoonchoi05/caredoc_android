package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityObject {
    @Expose
    @SerializedName("fake_id")
    private Long fakeId;

    @Expose
    @SerializedName("thumbnail")
    private String thumbnail;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("address")
    private String address;

    @Expose
    @SerializedName("road_address")
    private String roadAddress;

    @Expose
    @SerializedName("location")
    private String location;

    @Expose
    @SerializedName("foundation")
    private String foundation;

    @Expose
    @SerializedName("contact")
    private String contact;

    @Expose
    @SerializedName("homepage")
    private String homepage;

    @Expose
    @SerializedName("comments_count")
    private int commentsCount;

    @Expose
    @SerializedName("total_review_count")
    private int totalReviewCount;

    @Expose
    @SerializedName("total_rating")
    private float totalRating;

    @Expose
    @SerializedName("geo_location")
    private ApiCommonObject.GeoLocation geoLocation;

    @Expose
    @SerializedName("is_bookmarked")
    private boolean isBookmarked;

    @Expose
    @SerializedName("attaches")
    private AttachesObject attaches;

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getContact() {
        return contact;
    }

    public String getHomepage() {
        return homepage;
    }

    public ApiCommonObject.GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public Long getFakeId() {
        return fakeId;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public float getTotalRating() {
        return totalRating;
    }

    public String getRoadAddress() {
        return roadAddress;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public int getTotalReviewCount() {
        return totalReviewCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public String getFoundation() {
        return foundation;
    }

    public AttachesObject getAttaches() {
        return attaches;
    }

    public String getLocation() {
        return location;
    }

    public void setTotalRating(float totalRating) {
        this.totalRating = totalRating;
    }

    public void setTotalReviewCount(int totalReviewCount) {
        this.totalReviewCount = totalReviewCount;
    }
}
