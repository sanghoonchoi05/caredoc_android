package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;

import com.google.android.gms.maps.model.LatLng;
import com.onestepmore.caredoc.data.model.api.object.FacilitySimpleObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceSimpleObject;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class FacilitySearchMapList implements FacilityRealmModel {
    @PrimaryKey
    private Long fakeId;
    private String name;
    private String address;
    private String roadAddress;
    private String homepage;
    private String contact;
    private boolean isBookmarked;
    private LatLngFacility latLng;
    private String thumbnail;
    private float ratingAvg;
    private int reviewCount;
    private RealmList<Service> services;
    private String code;
    private boolean show;

    public FacilitySearchMapList() {
    }

    public FacilitySearchMapList(Parcel parcel) {
        this.fakeId = parcel.readLong();
        this.name = parcel.readString();
        this.address = parcel.readString();
        this.roadAddress = parcel.readString();
        this.homepage = parcel.readString();
        this.contact = parcel.readString();
        this.isBookmarked = parcel.readByte() != 0;
        this.latLng = parcel.readParcelable(LatLng.class.getClassLoader());
        this.thumbnail = parcel.readString();
        this.ratingAvg = parcel.readFloat();
        this.reviewCount = parcel.readInt();
        this.services = new RealmList<>();
        parcel.readList(this.services, Service.class.getClassLoader());
        this.code = parcel.readString();
        this.show = parcel.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(fakeId);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(roadAddress);
        dest.writeString(homepage);
        dest.writeString(contact);
        dest.writeByte((byte) (isBookmarked ? 1 : 0));
        dest.writeParcelable(latLng, flags);
        dest.writeString(thumbnail);
        dest.writeFloat(ratingAvg);
        dest.writeInt(reviewCount);
        dest.writeList(services);
        dest.writeString(code);
        dest.writeByte((byte) (show ? 1 : 0));
    }

    public static Creator<FacilitySearchMapList> CREATOR = new Creator<FacilitySearchMapList>() {

        @Override
        public FacilitySearchMapList createFromParcel(Parcel source) {
            return new FacilitySearchMapList(source);
        }

        @Override
        public FacilitySearchMapList[] newArray(int size) {
            return new FacilitySearchMapList[size];
        }

    };

    public void setFacility(FacilitySimpleObject facility, String code) {
        this.name = facility.getName();
        this.address = facility.getAddress();
        this.roadAddress = facility.getRoadAddress();
        this.homepage = facility.getHomepage();
        this.contact = facility.getContact();
        this.isBookmarked = facility.isBookmarked();
        this.latLng = new LatLngFacility();
        this.latLng.setLatLng(facility.getFakeId(), Service.TYPE.SEARCH_MAP, facility.getGeoLocation());
        this.fakeId = facility.getFakeId();
        this.thumbnail = facility.getThumbnail();
        this.ratingAvg = facility.getTotalRating();
        this.reviewCount = facility.getTotalReviewCount();
        this.services = new RealmList<>();
        for (ServiceSimpleObject item: facility.getServices().getItems()) {
            Service service = new Service();
            service.setService(Service.TYPE.SEARCH_MAP, item);
            this.services.add(service);
        }
        this.code = code;
        this.show = true;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getRoadAddress() {
        return roadAddress;
    }

    @Override
    public void setRoadAddress(String roadAddress) {
        this.roadAddress = roadAddress;
    }

    @Override
    public String getHomepage() {
        return homepage;
    }

    @Override
    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @Override
    public String getContact() {
        return contact;
    }

    @Override
    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public LatLngFacility getLatLng() {
        return latLng;
    }

    @Override
    public void setLatLng(LatLngFacility latLng) {
        this.latLng = latLng;
    }

    @Override
    public String getThumbnail() {
        return thumbnail;
    }

    @Override
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
    @Override
    public float getRatingAvg() {
        return ratingAvg;
    }

    @Override
    public void setRatingAvg(float ratingAvg) {
        this.ratingAvg = ratingAvg;
    }

    @Override
    public RealmList<Service> getServices() {
        return services;
    }

    @Override
    public void setServices(RealmList<Service> services) {
        this.services = services;
    }

    @Override
    public RealmList<Attach> getAttaches() {
        return null;
    }

    @Override
    public void setAttaches(RealmList<Attach> attaches) {

    }

    @Override
    public Long getFakeId() {
        return fakeId;
    }

    @Override
    public void setFakeId(Long fakeId) {
        this.fakeId = fakeId;
    }

    @Override
    public int getReviewCount() {
        return reviewCount;
    }

    @Override
    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    @Override
    public boolean isBookMarked() {
        return this.isBookmarked;
    }

    @Override
    public void setBookmarked(boolean bookmarked) {
        this.isBookmarked = bookmarked;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    @Override
    public void deleteRealm(FacilityRealmModel realmModel) {
        RealmList<Service> services = realmModel.getServices();
        if (services != null) {
            for (int j = 0; j < services.size(); j++) {
                Service service = services.get(j);
                if (service != null) {
                    service.deleteRealm();
                }
            }
        }

        if (realmModel.getLatLng() != null) {
            RealmObject.deleteFromRealm(realmModel.getLatLng());
        }

        RealmObject.deleteFromRealm(this);
    }

    @Override
    public boolean isModelSame(Object obj) {
        return obj instanceof FacilityRealmModel && this.fakeId.equals(((FacilityRealmModel) obj).getFakeId());
    }

    @Override
    public boolean isModelContentsSame(Object obj) {
        return false;
    }
}
