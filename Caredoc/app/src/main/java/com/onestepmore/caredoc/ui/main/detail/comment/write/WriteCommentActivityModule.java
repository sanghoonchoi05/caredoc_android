package com.onestepmore.caredoc.ui.main.detail.comment.write;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class WriteCommentActivityModule {
    @Provides
    WriteCommentViewModel writeCommentViewModel(Application application, AppDataSource dataSource,
                                               SchedulerProvider schedulerProvider, ReviewRepository reviewRepository) {
        return new WriteCommentViewModel(application, dataSource, schedulerProvider, reviewRepository);
    }

    @Provides
    ViewModelProvider.Factory provideWriteCommentViewModelProvider(WriteCommentViewModel writeCommentViewModel) {
        return new ViewModelProviderFactory<>(writeCommentViewModel);
    }
}
