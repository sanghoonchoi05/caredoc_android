package com.onestepmore.caredoc.ui.main.detail.comment.write;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Review;
import com.onestepmore.caredoc.databinding.ACommentBinding;
import com.onestepmore.caredoc.ui.base.BaseActivity;

import javax.inject.Inject;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.onestepmore.caredoc.ui.main.detail.FacilityDetailFragment.RESULT_CODE_WRITE_COMMENT;

public class WriteCommentActivity extends BaseActivity<ACommentBinding, WriteCommentViewModel> implements
        WriteCommentNavigator
{

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private WriteCommentViewModel mWriteCommentViewModel;

    private long mFacilityId = 0;
    private long mCommentId = 0;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_comment;
    }

    @Override
    public WriteCommentViewModel getViewModel() {
        mWriteCommentViewModel = ViewModelProviders.of(this, mViewModelFactory).get(WriteCommentViewModel.class);
        return mWriteCommentViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWriteCommentViewModel.setNavigator(this);

        setSupportActionBar(getViewDataBinding().aCommentToolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back_title);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Intent intent = getIntent();
        mFacilityId = intent.getLongExtra("facilityId", 0L);
        if(mFacilityId == 0){
            finish();
            return;
        }

        int defaultRating = 3;
        getViewDataBinding().aCommentRatingBar.setRating(defaultRating);
        getViewDataBinding().aCommentRatingNum.setText(String.format(getString(R.string.common_i_point_formatted),defaultRating));

        mCommentId = intent.getLongExtra("commentId", 0);
        if(mCommentId > 0){
            Review review = getViewModel().getRealm().where(Review.class).equalTo("id", mCommentId).findFirst();
            if(review != null){
                getViewDataBinding().aCommentRatingBar.setRating(review.getRating());
                getViewDataBinding().aCommentTextInput.setText(review.getContent());
            }
        }

        getViewDataBinding().aCommentRatingBar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                getViewDataBinding().aCommentRatingNum.setText(String.format(getString(R.string.common_i_point_formatted),(int)rating));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(mCommentId > 0){
            getMenuInflater().inflate(R.menu.write_comment, menu);
            LinearLayout layout = (LinearLayout)menu.findItem(R.id.write_comment_icon).getActionView();
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getViewModel().removeComment(mFacilityId);
                }
            });
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onRegisterComment() {
        int textLength = getViewDataBinding().aCommentTextInput.getText().length();
        if(textLength < 50 || getViewDataBinding().aCommentRatingBar.getRating() < 1) {
            String desc = "";
            if(textLength < 50){
                desc =  getString(R.string.warning_msg_comment_invalid_text);
            }else if(getViewDataBinding().aCommentRatingBar.getRating() < 1){
                desc =  getString(R.string.warning_msg_comment_invalid_star);
            }
            showCommonDialog(getString(R.string.warning_msg_comment_failed), desc);
            return;
        }

        getViewModel().registerComment(mFacilityId,
                (int)getViewDataBinding().aCommentRatingBar.getRating(),
                getViewDataBinding().aCommentTextInput.getText().toString()
                );
    }

    @Override
    public void onCompleteRegisterComment() {
        setResult(RESULT_CODE_WRITE_COMMENT);
        finish();
    }

    @Override
    public void onCompleteRemoveComment() {
        setResult(RESULT_CODE_WRITE_COMMENT);
        finish();
    }
}
