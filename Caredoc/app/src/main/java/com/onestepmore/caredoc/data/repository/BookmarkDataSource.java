package com.onestepmore.caredoc.data.repository;

import com.onestepmore.caredoc.data.remote.BookmarkApiHelper;

public interface BookmarkDataSource extends BookmarkApiHelper {
}
