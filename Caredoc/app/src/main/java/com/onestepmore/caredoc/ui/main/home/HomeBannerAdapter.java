package com.onestepmore.caredoc.ui.main.home;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.IHomeBannerBinding;
import com.onestepmore.caredoc.utils.UIUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeBannerAdapter extends RecyclerView.Adapter<HomeBannerAdapter.HomeBannerViewHolder>
{
    @NonNull
    private final List<String> mTopTextList;
    @NonNull
    private final List<String> mBottomArgsList;
    @NonNull
    private final List<Integer> mBottomTextIdList;
    @NonNull
    private final List<Integer> mBannerResourceList;
    @NonNull
    private final List<Integer> mBannerColorIdList;
    @NonNull
    private final WeakReference<Context> mContext;

    public HomeBannerAdapter(@NonNull Context context, @NonNull List<String> bottomArgsList){
        mContext = new WeakReference<>(context);
        mBottomArgsList = bottomArgsList;

        String [] topTextList = context.getResources().getStringArray(R.array.f_home_banner_top_text_list);;
        mTopTextList = Arrays.asList(topTextList);
        mBottomTextIdList = new ArrayList<>();
        mBottomTextIdList.add(R.string.f_home_banner_1_bottom_text);
        mBottomTextIdList.add(R.string.f_home_banner_2_bottom_text);
        mBottomTextIdList.add(R.string.f_home_banner_3_bottom_text);
        mBannerResourceList = new ArrayList<>();
        mBannerResourceList.add(R.drawable.ico_home_banner_1);
        mBannerResourceList.add(R.drawable.ico_home_banner_2);
        mBannerResourceList.add(R.drawable.ico_home_banner_3);
        mBannerColorIdList = new ArrayList<>();
        mBannerColorIdList.add(R.color.home_banner_1);
        mBannerColorIdList.add(R.color.home_banner_2);
        mBannerColorIdList.add(R.color.home_banner_3);
    }

    @NonNull
    @Override
    public HomeBannerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IHomeBannerBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_home_banner, viewGroup, false);
        return new HomeBannerViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeBannerViewHolder viewHolder, int position) {
        viewHolder.binding.setTopText(mTopTextList.get(position));
        UIUtils.setTextIncludeAnnotation(
                mContext.get(),
                mBottomTextIdList.get(position),
                viewHolder.binding.iHomeBannerBottomText,
                mBottomArgsList.get(position));
        viewHolder.binding.setBannerResourceId(mBannerResourceList.get(position));
        viewHolder.binding.setBannerColorId(mBannerColorIdList.get(position));
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mTopTextList.size();
    }

    class HomeBannerViewHolder extends RecyclerView.ViewHolder {
        final IHomeBannerBinding binding;

        private HomeBannerViewHolder(IHomeBannerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
