package com.onestepmore.caredoc.ui.main.profile.webview;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class WebViewActivityModule {
    @Provides
    WebViewViewModel webViewViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new WebViewViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideWebViewViewModelProvider(WebViewViewModel webViewViewModel) {
        return new ViewModelProviderFactory<>(webViewViewModel);
    }
}
