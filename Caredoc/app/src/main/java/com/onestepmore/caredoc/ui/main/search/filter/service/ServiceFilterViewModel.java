package com.onestepmore.caredoc.ui.main.search.filter.service;

import android.app.Application;
import android.databinding.ObservableField;

import com.google.gson.Gson;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import io.realm.Realm;

public class ServiceFilterViewModel extends BaseViewModel<ServiceFilterNavigator> {

    public ServiceFilterViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                                  Gson gson) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClose(){
        getNavigator().onClose();
    }

    public void onInit(){
        getNavigator().onInit();
    }

    public void onApply(){
        getNavigator().onApplyFilter();
    }
}
