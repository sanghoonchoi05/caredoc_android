package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class GradeDetailObject {
    @Expose
    @SerializedName("id")
    private long id;

    // 방밖으로 나오기 악화 환자분율_치매환자군(%)
    @Expose
    @SerializedName("leaving_room_dementia_patient")
    private Float leavingRoomDementiaPatient;

    // 방밖으로 나오기 악화 환자분율_치매환자제외군(%)
    @Expose
    @SerializedName("leaving_room_dementia_patient_excluded")
    private Float leavingRoomDementiaPatientExcluded;

    // 일상생활수행능력 감퇴환자 분율_치매환자군(%)
    @Expose
    @SerializedName("dementia_patient")
    private Float dementiaPatient;

    // 일상생활수행능력 감퇴환자 분율_치매환자제외군(%)
    @Expose
    @SerializedName("dementia_patient_excluded")
    private Float dementiaPatientExcluded;

    // 욕창이 악화된 환자분율_고위험군(%)
    @Expose
    @SerializedName("ulcers_worsened_high_risk_group")
    private Float ulcersWorsenedHighRiskGroup;

    // 욕창이 새로 생긴 환자분율_저위험군(%)
    @Expose
    @SerializedName("pressure_ulcers_low_risk_group")
    private Float pressureUlcersLowRiskGroup;

    // 욕창이 새로 생긴 환자분율_고위험군(%)
    @Expose
    @SerializedName("newly_developed_pressure_ulcers_high_risk_group")
    private Float newlyDevelopedPressureUlcersHighRiskGroup;

    // 욕창 개선 환자분율_고위험군(%)
    @Expose
    @SerializedName("improved_pressure_ulcers_high_risk_group")
    private Float improvedPressureUlcersHighRiskGroup;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Float getLeavingRoomDementiaPatient() {
        return leavingRoomDementiaPatient;
    }

    public void setLeavingRoomDementiaPatient(Float leavingRoomDementiaPatient) {
        this.leavingRoomDementiaPatient = leavingRoomDementiaPatient;
    }

    public Float getLeavingRoomDementiaPatientExcluded() {
        return leavingRoomDementiaPatientExcluded;
    }

    public void setLeavingRoomDementiaPatientExcluded(Float leavingRoomDementiaPatientExcluded) {
        this.leavingRoomDementiaPatientExcluded = leavingRoomDementiaPatientExcluded;
    }

    public Float getDementiaPatient() {
        return dementiaPatient;
    }

    public void setDementiaPatient(Float dementiaPatient) {
        this.dementiaPatient = dementiaPatient;
    }

    public Float getDementiaPatientExcluded() {
        return dementiaPatientExcluded;
    }

    public void setDementiaPatientExcluded(Float dementiaPatientExcluded) {
        this.dementiaPatientExcluded = dementiaPatientExcluded;
    }

    public Float getUlcersWorsenedHighRiskGroup() {
        return ulcersWorsenedHighRiskGroup;
    }

    public void setUlcersWorsenedHighRiskGroup(Float ulcersWorsenedHighRiskGroup) {
        this.ulcersWorsenedHighRiskGroup = ulcersWorsenedHighRiskGroup;
    }

    public Float getPressureUlcersLowRiskGroup() {
        return pressureUlcersLowRiskGroup;
    }

    public void setPressureUlcersLowRiskGroup(Float pressureUlcersLowRiskGroup) {
        this.pressureUlcersLowRiskGroup = pressureUlcersLowRiskGroup;
    }

    public Float getNewlyDevelopedPressureUlcersHighRiskGroup() {
        return newlyDevelopedPressureUlcersHighRiskGroup;
    }

    public void setNewlyDevelopedPressureUlcersHighRiskGroup(Float newlyDevelopedPressureUlcersHighRiskGroup) {
        this.newlyDevelopedPressureUlcersHighRiskGroup = newlyDevelopedPressureUlcersHighRiskGroup;
    }

    public Float getImprovedPressureUlcersHighRiskGroup() {
        return improvedPressureUlcersHighRiskGroup;
    }

    public void setImprovedPressureUlcersHighRiskGroup(Float improvedPressureUlcersHighRiskGroup) {
        this.improvedPressureUlcersHighRiskGroup = improvedPressureUlcersHighRiskGroup;
    }
}
