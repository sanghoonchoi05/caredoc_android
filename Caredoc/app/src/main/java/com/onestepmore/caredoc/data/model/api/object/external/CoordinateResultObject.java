package com.onestepmore.caredoc.data.model.api.object.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CoordinateResultObject {
    @Expose
    @SerializedName("common")
    public CommonObject common;

    @Expose
    @SerializedName("juso")
    public List<CoordinateObject> jusoList;

    public CommonObject getCommon() {
        return common;
    }

    public List<CoordinateObject> getJusoList() {
        return jusoList;
    }
}
