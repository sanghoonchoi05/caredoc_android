package com.onestepmore.caredoc.ui.widgets;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.DialogCommonYnBinding;
import com.onestepmore.caredoc.ui.base.BaseDialog;

public class CommonYNDialog extends BaseDialog{

    private View.OnClickListener okListener;
    private View.OnClickListener cancelListener;

    public static CommonYNDialog newInstance(String title, String desc, String cancelText, String okText) {
        Bundle args = new Bundle();
        CommonYNDialog dialog = new CommonYNDialog();
        args.putString("title", title);
        args.putString("desc", desc);
        args.putString("cancelText", cancelText);
        args.putString("okText", okText);
        dialog.setArguments(args);

        return dialog;

    }
    public void setOnOkClickListener(View.OnClickListener listener){
        this.okListener = listener;
    }
    public void setOnCancelClickListener(View.OnClickListener listener){
        this.cancelListener = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DialogCommonYnBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()), R.layout.dialog_common_yn, null, false);

        if(getArguments() != null){
            binding.setTitle(getArguments().getString("title"));
            binding.setDesc(getArguments().getString("desc"));
            binding.setCancelText(getArguments().getString("cancelText"));
            binding.setOkText(getArguments().getString("okText"));
        }

        // ok리스너가 null이거나 cancelText 공백이면 취소버튼은 숨기자
        if(okListener == null || binding.getCancelText().isEmpty()){
            binding.dialogCommonYnCancel.setVisibility(View.GONE);
        }

        binding.dialogCommonYnCancel.setOnClickListener((view) -> {
            if(cancelListener != null){
                cancelListener.onClick(view);
            }
            dismissDialog(getTag());
        });

        binding.dialogCommonYnButton.setOnClickListener((view) -> {
            dismissDialog(getTag());
            if(okListener != null){
                okListener.onClick(view);
            }
        });

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);


        return dialog;
    }
}
