package com.onestepmore.caredoc.ui.main.taker.add;

import android.app.Application;

import com.google.gson.Gson;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoResponse;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailResponse;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.TakerRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.reactivex.Flowable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class AddTakerViewModel extends BaseViewModel<AddTakerNavigator> {
    private final TakerRepository mTakerRepository;
    private final Gson mGson;

    public TakerRepository getTakerRepository() {
        return mTakerRepository;
    }

    public AddTakerViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                             Gson gson, TakerRepository takerRepository) {
        super(application, appDataSource, schedulerProvider);

        mTakerRepository = takerRepository;
        mGson = gson;
    }

    public void addTaker(AddTakerRequest request, File imageFile){
        JSONObject jsonObject = getRequestJSONObjectBody(request);

        if(jsonObject == null){
            return;
        }

        getCompositeDisposable().add(getTakerRepository()
                .addTakerApiCall(jsonObject)
                .flatMap(response -> getTakerRepository().getTakerDetailApiCall(new TakerDetailRequest.PathParameter(response.getFakeId())))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> saveTaker(response, imageFile), throwable ->
                        getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void modifyTaker(AddTakerRequest request, long takerId) {
        JSONObject jsonObject = getRequestJSONObjectBody(request);

        if(jsonObject == null){
            return;
        }

        getCompositeDisposable().add(getTakerRepository()
                .modifyTakerApiCall(jsonObject, new AddTakerRequest.PathParameter(takerId))
                .flatMap(response -> getTakerRepository().getTakerDetailApiCall(new TakerDetailRequest.PathParameter(takerId)))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> saveTaker(response, null), throwable ->
                        getDataManager().handleThrowable(throwable, getNavigator())));
    }

    private JSONObject getRequestJSONObjectBody(AddTakerRequest request){
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(mGson.toJson(request, AddTakerRequest.class));
        }catch (JSONException e){

        }

        return jsonObject;
    }

    public void saveTaker(TakerDetailResponse response, File imageFile) {
        if(response == null){
            return;
        }

        getRealm().executeTransaction((realm -> {
            Taker taker = new Taker();
            taker.setTaker(response);
            realm.copyToRealmOrUpdate(taker);

            if(imageFile != null){
                uploadPhoto(taker.getFakeId(), imageFile);
            }
            else{
                getNavigator().onCompleteAddTaker();
            }
        }));
    }

    public void uploadPhoto(long takerId, File imageFile) {
        getTakerRepository().addTakerPhotoApiCall(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onErrorPhotoUpload(e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    if(response.body() != null){
                        savePhotoUrl(mGson.fromJson(response.body().string(), AddTakerPhotoResponse.class), takerId);
                    }
                }
                else{
                    if (response.body() != null) {
                        onErrorPhotoUpload(response);
                    }
                }
            }
        }, takerId, imageFile);
    }

    private void onErrorPhotoUpload(String response){
        getCompositeDisposable().add(Flowable.fromCallable(() -> true)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    getDataManager().handleApiError(response, getNavigator());
                    getNavigator().onCompleteAddTaker();
                }));
    }

    private void onErrorPhotoUpload(Response response){
        getCompositeDisposable().add(Flowable.fromCallable(() -> true)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    getDataManager().handleApiError(response, getNavigator());
                    getNavigator().onCompleteAddTaker();
                }));
    }

    private void savePhotoUrl(AddTakerPhotoResponse response, long takerId) {
        if (response == null) {
            return;
        }

        getCompositeDisposable().add(Flowable.fromCallable(() -> true)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    getRealm().executeTransaction( (realm) -> {
                        Taker taker = getRealm().where(Taker.class).equalTo("fakeId", takerId).findFirst();
                        if(taker != null){
                            taker.setPhotoUrl(response.getFullThumbnailUrl());
                        }
                        realm.copyToRealmOrUpdate(taker);

                        getNavigator().onCompleteAddTaker();
                    });
                }));
    }
}
