package com.onestepmore.caredoc.ui.main.detail.comment;

public interface RemoveCommentListener {
    void removeComment(long commentId);
}
