package com.onestepmore.caredoc.data.repository;

import com.onestepmore.caredoc.data.remote.TakerApiHelper;

public interface TakerDataSource extends TakerApiHelper {
}
