package com.onestepmore.caredoc.ui.estimate.request;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.AEstimateRequestStartBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;

public class EstimateRequestStartActivity extends BaseActivity<AEstimateRequestStartBinding, EstimateRequestStartViewModel> implements 
        EstimateRequestStartNavigator
{

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private EstimateRequestStartViewModel mEstimateRequestStartViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_care_coordi_start;
    }

    @Override
    public EstimateRequestStartViewModel getViewModel() {
        mEstimateRequestStartViewModel = ViewModelProviders.of(this, mViewModelFactory).get(EstimateRequestStartViewModel.class);
        return mEstimateRequestStartViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEstimateRequestStartViewModel.setNavigator(this);

        UIUtils.setStatusBarColor(this, getWindow().getDecorView(), getResources().getColor(R.color.colorSubBg), true);
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onClose() {
        finish();
    }

    @Override
    public void onStartEstimateRequest() {
        finish();
        //Intent intent = newIntent(EstimateRequestActivity.class);
        //startActivityForResult(intent, 0);
    }
}
