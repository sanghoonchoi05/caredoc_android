package com.onestepmore.caredoc.ui.main.taker.add.performance;

import android.app.Application;
import android.view.View;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TakerPerformanceViewModel extends BaseViewModel<TakerPerformanceNavigator> {
    public TakerPerformanceViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClick(View view){
        getNavigator().onClick(view.getTag().toString());
    }
}
