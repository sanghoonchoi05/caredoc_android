package com.onestepmore.caredoc.ui.main.taker.detail;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.myhexaville.smartimagepicker.ImagePicker;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.realm.Taker;
import com.onestepmore.caredoc.databinding.ATakerDetailBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.NameDialog;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.SnackbarUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;

import io.realm.RealmResults;

public class TakerDetailActivity extends BaseActivity<ATakerDetailBinding, TakerDetailViewModel> implements TakerDetailNavigator {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private TakerDetailViewModel mTakerDetailViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_taker_detail;
    }

    RealmResults<Taker> mTakerResult;
    Taker mTaker;
    ImagePicker mImagePicker;

    @Override
    public TakerDetailViewModel getViewModel() {
        mTakerDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerDetailViewModel.class);
        return mTakerDetailViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerDetailViewModel.setNavigator(this);
        UIUtils.setStatusBarColor(this, getWindow().getDecorView(), getResources().getColor(R.color.colorPrimary), false);

        setSupportActionBar(getViewDataBinding().aTakerDetailToolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back_white_title);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        if(getIntent() != null){
            long takerId = getIntent().getLongExtra("takerId", 0);
            mTakerResult = getViewModel().getRealm().where(Taker.class).equalTo("fakeId", takerId).findAll();
            if(mTakerResult.size() > 0){
                mTaker = mTakerResult.get(0);
                getViewDataBinding().setTaker(mTaker);
                mTakerResult.addChangeListener((results) -> {
                    if(results.size() > 0){
                        mTaker = results.get(0);
                        getViewDataBinding().setTaker(mTaker);
                    }
                });
                getViewModel().getTakerDetail(takerId);
            }else{
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.taker_detail, menu);
        LinearLayout layout = (LinearLayout)menu.findItem(R.id.taker_detail_icon).getActionView();
        layout.setOnClickListener((view) -> showDeletePopup());

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDeletePopup(){
        showCommonDialog(
                getString(R.string.delete_taker_exit_dialog_title),
                getString(R.string.delete_taker_exit_dialog_desc),
                CommonYNDialog.class.getName(),
                (view) -> {
                    showLoading();
                    getViewModel().deleteTaker(mTaker.getFakeId());
                },
                getString(R.string.common_cancel),
                getString(R.string.common_delete)
        );
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        SnackbarUtils.showSnackbar(this, msg);
        //Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void showTakerModifyView(AddTakerActivity.STEP step) {
        Intent intent = newIntent(AddTakerActivity.class);
        intent.putExtra("takerId", mTaker.getFakeId());
        intent.putExtra("step", step.getStep());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImagePicker.handleActivityResult(resultCode,requestCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImagePicker.handlePermission(requestCode, grantResults);
    }

    @Override
    public void showTakerPhoto() {
        refreshImagePicker();
        mImagePicker.choosePicture(true);
    }

    @Override
    public void onDelete() {
        hideLoading();
        finish();
    }

    private void refreshImagePicker() {
        mImagePicker = new ImagePicker(this,
                null,
                imageUri -> getViewModel().uploadPhoto(mTaker.getFakeId(), mImagePicker.getImageFile()));
        mImagePicker.setWithImageCrop(1, 1);
    }

    @Override
    public void showNameDialog(){
        NameDialog dialog = NameDialog.newInstance(mTaker.getName());
        dialog.setOnNameInputListener(this::onNameOk);
        dialog.show(getSupportFragmentManager(), NameDialog.class.getName());
    }

    public void onNameOk(String name){
        getViewModel().modifyTakerName(buildAddTakerRequest(name), mTaker.getFakeId());
    }

    private AddTakerRequest buildAddTakerRequest(String name){
        if(mTaker == null || CommonUtils.checkNullAndEmpty(name)){
            return null;
        }

        AddTakerRequest.Basic basic = mTaker.getBasic();
        basic.setName(name);
        return new AddTakerRequest.Builder()
                .addBasic(basic)
                .addCare(mTaker.getCare())
                .addDiseases(mTaker.getDiseaseList())
                .addPerformances(mTaker.getPerformance())
                .addNutritions(mTaker.getNutritions())
                .addRemarks(mTaker.getRemarks())
                .build();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mTakerResult!=null){
            mTakerResult.removeAllChangeListeners();
        }
    }


}
