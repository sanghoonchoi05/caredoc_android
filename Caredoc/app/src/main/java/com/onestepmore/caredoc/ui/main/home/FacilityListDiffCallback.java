package com.onestepmore.caredoc.ui.main.home;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.onestepmore.caredoc.data.model.realm.ModelDiff;

import java.util.List;

public class FacilityListDiffCallback<T extends ModelDiff> extends DiffUtil.Callback {

    private final List<T> oldList;
    private final List<T> newList;

    public FacilityListDiffCallback(List<T> oldList, List<T> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).isModelSame(newList.get(newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).isModelContentsSame(newList.get(newItemPosition));
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're goging to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
