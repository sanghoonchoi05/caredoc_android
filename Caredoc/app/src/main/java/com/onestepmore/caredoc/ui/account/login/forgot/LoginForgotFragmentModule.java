package com.onestepmore.caredoc.ui.account.login.forgot;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginForgotFragmentModule {
    @Provides
    LoginForgotViewModel loginForgotViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new LoginForgotViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("LoginForgotFragment")
    ViewModelProvider.Factory provideLoginForgotViewModelProvider(LoginForgotViewModel loginForgotViewModel) {
        return new ViewModelProviderFactory<>(loginForgotViewModel);
    }
}
