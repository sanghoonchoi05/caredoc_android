package com.onestepmore.caredoc.ui.common;

public interface TextLayoutClickListener {
    void onClickFirstLayout();
    void onClickSecondLayout();
}
