package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoResponse;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerResponse;
import com.onestepmore.caredoc.data.model.api.taker.DeleteTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailResponse;

import org.json.JSONObject;

import java.io.File;

import io.reactivex.Flowable;
import okhttp3.Callback;

public interface TakerApiHelper {
    Flowable<AddTakerPhotoResponse> addTakerPhotoApiCall(AddTakerPhotoRequest.PathParameter pathParameter, File file);
    void addTakerPhotoApiCall(Callback responseCallback, long takerId, File file);
    Flowable<AddTakerResponse> addTakerApiCall(JSONObject jsonObject);
    Flowable<AddTakerResponse> modifyTakerApiCall(JSONObject jsonObject, AddTakerRequest.PathParameter pathParameter);
    Flowable<TakerDetailResponse> getTakerDetailApiCall(TakerDetailRequest.PathParameter pathParameter);
    void deleteTakerApiCall(OkHttpResponseListener listener, DeleteTakerRequest.PathParameter pathParameter);
}
