package com.onestepmore.caredoc.ui.main.taker.detail;

public interface ModifyTakerDetailListener {
    void onModifyTakerDetail();
}
