package com.onestepmore.caredoc.ui.support.rx;


import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;

import rx.Observable;

public final class SupportRxObserver {

    @CheckResult
    @NonNull
    public static Observable<Boolean> observableFieldBooleanChanges(@NonNull ObservableField<Boolean> observableField){
        return Observable.create(new SupportObservableFieldOnSubscribe<Boolean>(observableField));
    }

    @CheckResult
    @NonNull
    public static Observable<Boolean> observableBooleanChanges(@NonNull ObservableBoolean observableField){
        return Observable.create(new SupportObservableBooleanOnSubscribe(observableField));
    }

    @CheckResult
    @NonNull
    public static Observable<Boolean> observableCheckBoxChanges(@NonNull AppCompatCheckBox checkBox){
        return Observable.create(new SupportRxAppCompatCheckBox(checkBox));
    }
}
