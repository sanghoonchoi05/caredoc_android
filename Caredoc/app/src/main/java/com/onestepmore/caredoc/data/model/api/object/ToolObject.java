package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ToolObject {
    @Expose
    @SerializedName("type_code")
    private String typeCode;
    @Expose
    @SerializedName("category_code")
    private String categoryCode;
    @Expose
    @SerializedName("type_name")
    private String typeName;
    @Expose
    @SerializedName("model_name")
    private String modelName;
    @Expose
    @SerializedName("manufacturer")
    private String manufacturer;
    @Expose
    @SerializedName("usage")
    private String usage;
    @Expose
    @SerializedName("remark")
    private String remark;

    public String getTypeCode() {
        return typeCode;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getModelName() {
        return modelName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getUsage() {
        return usage;
    }

    public String getRemark() {
        return remark;
    }
}
