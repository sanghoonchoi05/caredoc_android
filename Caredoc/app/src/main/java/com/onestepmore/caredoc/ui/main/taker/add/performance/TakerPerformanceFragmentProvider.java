package com.onestepmore.caredoc.ui.main.taker.add.performance;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerPerformanceFragmentProvider {
    @ContributesAndroidInjector(modules = TakerPerformanceFragmentModule.class)
    abstract TakerPerformanceFragment provideTakerPerformanceFragmentFactory();
}
