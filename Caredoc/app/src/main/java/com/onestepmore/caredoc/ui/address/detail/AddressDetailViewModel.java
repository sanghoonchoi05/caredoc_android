package com.onestepmore.caredoc.ui.address.detail;

import android.app.Application;

import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class AddressDetailViewModel extends BaseViewModel<AddressDetailNavigator> {
    public AddressDetailViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClickComplete(JusoRealmModel juso){
        getNavigator().onSelectJuso(juso);
    }
}
