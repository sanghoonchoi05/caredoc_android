package com.onestepmore.caredoc.firebase;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.DateUtils;

import javax.inject.Singleton;

@Singleton
public class FirebaseManager {

    private static final String TRUE = "TRUE";

    private Gson mGson;
    private FirebaseAppUpdaterChecker mFirebaseAppUpdaterChecker;

    private static final long remoteConfigCacheInterval = 3600; // 1시간 동안 캐시한다
    public interface RemoteConfigOnCallback{
        void onComplete(boolean isSuccessful);
    }
    private static FirebaseManager mFirebaseManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private boolean mNeedShowEventPopup = false;
    private boolean mApiStatus = true;
    private String mApiStatusMessage = null;

    public static FirebaseManager newInstance(Context context){
        if(mFirebaseManager == null){
            mFirebaseManager = new FirebaseManager();
            mFirebaseManager.initFirebaseAnalytics(context);
            mFirebaseManager.initFirebaseRemoteConfig();
            mFirebaseManager.initGson();
        }

        return mFirebaseManager;
    }

    private void initGson(){
        mGson = new GsonBuilder()
                .setDateFormat(DateUtils.STANDARD_DATE_TIME_FORMAT)
                .excludeFieldsWithoutExposeAnnotation().create();
    }

    private void initFirebaseAnalytics(Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    private void initFirebaseRemoteConfig(){
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        if(mFirebaseRemoteConfig != null){
            FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                    .setDeveloperModeEnabled(BuildConfig.DEBUG) //false로 데이터를 캐싱합니다.
                    .setMinimumFetchIntervalInSeconds(remoteConfigCacheInterval)
                    .build();
            mFirebaseRemoteConfig.setConfigSettings(configSettings);
        }
    }

    public void remoteConfigFetch(RemoteConfigOnCallback listener){
        Task<Boolean> firebaseTask = mFirebaseRemoteConfig.fetchAndActivate();
        firebaseTask.addOnCompleteListener((task) -> {
            fetchAppInfo();
            fetchShowEventPopup();
            fetchApiStatus();
            listener.onComplete(task.isSuccessful());
        });
    }

    public String getConfigString(String key){
        return mFirebaseRemoteConfig.getString(key);
    }

    private void fetchAppInfo(){
        String appInfoJson = getConfigString("app_info");
        if(!CommonUtils.checkNullAndEmpty(appInfoJson)){
            mFirebaseAppUpdaterChecker = mGson.fromJson(appInfoJson, FirebaseAppUpdaterChecker.class);
            if(mFirebaseAppUpdaterChecker != null){
                mFirebaseAppUpdaterChecker.fatch();
            }
        }
    }

    private void fetchShowEventPopup(){
        String show = getConfigString("need_show_event_popup");
        mNeedShowEventPopup = show != null && show.equals(TRUE);
    }

    private void fetchApiStatus(){
        String apiStatus = getConfigString("api_status");
        mApiStatus = apiStatus != null && apiStatus.equals(TRUE);

        mApiStatusMessage = getConfigString("api_status_message");
    }

    public boolean isNeedShowEventPopup() {
        return mNeedShowEventPopup;
    }

    public boolean isApiStatus() {
        return mApiStatus;
    }

    public String getApiStatusMessage() {
        return mApiStatusMessage;
    }

    public boolean isForceUpdate(){
        if(mFirebaseAppUpdaterChecker != null){
            return mFirebaseAppUpdaterChecker.isForceUpdate();
        }

        return false;
    }

    public boolean isOptionalUpdate(){
        if(mFirebaseAppUpdaterChecker != null){
            return mFirebaseAppUpdaterChecker.isOptionalUpdate();
        }

        return false;
    }

    public String getForceUpdateMessage(){
        if(mFirebaseAppUpdaterChecker != null){
            return mFirebaseAppUpdaterChecker.getForceUpdateMessage();
        }

        return "";
    }

    public String getOptionalUpdateMessage(){
        if(mFirebaseAppUpdaterChecker != null){
            return mFirebaseAppUpdaterChecker.getOptionalUpdateMessage();
        }

        return "";
    }

    public void gotoMarket(Context context){
        if(mFirebaseAppUpdaterChecker != null){
            mFirebaseAppUpdaterChecker.gotoMarket(context);
        }
    }

    public static FirebaseManager getInstance(){
        return mFirebaseManager;
    }

    public void logEvent(BaseActivity baseActivity){
        String eventName = FirebaseEvent.getEventName(baseActivity);
        if(CommonUtils.checkNullAndEmpty(eventName)){
            return;
        }

        logEvent(eventName);
    }

    public void logEvent(BaseFragment baseFragment){
        String eventName = FirebaseEvent.getEventName(baseFragment);
        if(CommonUtils.checkNullAndEmpty(eventName)){
            return;
        }

        logEvent(eventName);
    }

    public void setCurrentScreen(BaseActivity baseActivity){
        if(baseActivity == null){
            return;
        }
        String screenName = FirebaseEvent.getScreenName(baseActivity);
        setCurrentScreen(baseActivity, screenName);
    }

    public void setCurrentScreen(BaseFragment baseFragment){
        if(baseFragment == null || baseFragment.getBaseActivity() == null){
            return;
        }
        String screenName = FirebaseEvent.getScreenName(baseFragment);
        setCurrentScreen(baseFragment.getBaseActivity(), screenName);
    }

    private void setCurrentScreen(Activity activity, String screenName){
        if(activity == null || CommonUtils.checkNullAndEmpty(screenName)){
            return;
        }

        mFirebaseAnalytics.setCurrentScreen(activity, screenName, null);
    }

    public void logEvent(String eventName){
        logEvent(eventName, null);
    }
    public void logEvent(String eventName, Parameter parameter){
        Bundle bundle = null;
        if(parameter != null){
            bundle = parameter.getBundle();
        }
        mFirebaseAnalytics.logEvent(eventName, bundle);
    }

    public static class Parameter{
        Bundle bundle = new Bundle();

        public Bundle getBundle() {
            return bundle;
        }
    }

    public static class Builder extends Parameter{
        public Builder addParam(String key, String value){
            bundle.putString(key, value);
            return this;
        }

        public Builder addParam(String key, int value){
            bundle.putInt(key, value);
            return this;
        }

        public Builder addParam(String key, long value){
            bundle.putLong(key, value);
            return this;
        }

        public Builder addParam(String key, float value){
            bundle.putFloat(key, value);
            return this;
        }

        public Parameter build(){
            Parameter parameter = new Parameter();
            parameter.bundle = this.bundle;
            return parameter;
        }
    }
}
