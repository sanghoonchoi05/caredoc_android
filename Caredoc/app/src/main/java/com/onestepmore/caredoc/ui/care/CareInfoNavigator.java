package com.onestepmore.caredoc.ui.care;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface CareInfoNavigator extends BaseNavigator {
    void onSendMail();
    void onSuccessSendMail();
}
