package com.onestepmore.caredoc.ui.main.search.dialog;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.ChipGroup;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.data.model.realm.Suggestion;
import com.onestepmore.caredoc.databinding.DialogSearchViewBinding;
import com.onestepmore.caredoc.databinding.ISuggestionBinding;
import com.onestepmore.caredoc.databinding.VFacilityServiceChipGroupItemBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseBottomSheetDialog2;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.CommonUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import rx.Subscription;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.SEARCH_KEYWORD_CLICK_LIST_ITEM;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Screen.SEARCH_KEYWORD;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.SEARCH_KEYWORD_SEARCH;

public class SearchDialog extends BaseBottomSheetDialog2<DialogSearchViewBinding, SearchDialogViewModel> implements
        SearchDialogCallback {

    @Inject
    @Named("SearchDialog")
    ViewModelProvider.Factory mViewModelFactory;
    private SearchDialogViewModel mSearchDialogViewModel;

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void showNeedLoginPopup() {

    }

    @Override
    public void showNeedLoginPopup(View.OnClickListener listener) {

    }

    @Override
    public void gotoAccountMainActivity() {

    }

    public interface OnSearchListener {
        void onSearch(Suggestion suggestion);
    }

    public static SearchDialog getInstance() {
        return new SearchDialog();
    }

    private Subscription mSubscription;
    private OnSearchListener mOnSearchListener;

    public void setOnSearchListener(OnSearchListener listener) {
        this.mOnSearchListener = listener;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.dialog_search_view;
    }

    @Override
    public SearchDialogViewModel getViewModel() {
        if (getBaseActivity() != null) {
            mSearchDialogViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchDialogViewModel.class);
        }
        return mSearchDialogViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSearchDialogViewModel.setNavigator(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        AmplitudeManager.getInstance().logEvent(SEARCH_KEYWORD);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setObserver();
        initListener();

        RecyclerView recyclerView = getViewDataBinding().dialogSearchViewRecyclerView;
        SearchDialogAdapter adapter = new SearchDialogAdapter(getViewModel().getSuggestion());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(), new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_facility));
        getViewDataBinding().dialogSearchViewRecyclerView.addItemDecoration(dividerItemDecoration);

        getViewDataBinding().getRoot().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();

                FrameLayout bottomSheet = (FrameLayout)
                        dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View view, int newState) {
                        AppLogger.i(getClass(), "Bottom Sheet newState: " + newState);
                        if (newState == BottomSheetBehavior.STATE_SETTLING) {
                            dismiss();
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View view, float v) {

                    }
                });
            }
        });

        getViewDataBinding().setEmpty(true);

        getViewDataBinding().dialogSearchViewEditLayout.vCommonAddressEditText.requestFocus();
        if(getDialog().getWindow() != null){
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }
    }

    @Override
    public void onSearch() {
        String text = getViewDataBinding().dialogSearchViewEditLayout.vCommonAddressEditText.getText().toString();
        if (CommonUtils.checkNullAndEmpty(text)) {
            CommonYNDialog dialog = CommonYNDialog.newInstance(
                    getString(R.string.warning_msg_search_title),
                    getString(R.string.warning_msg_search_empty_desc),
                    "",
                    getString(R.string.common_confirm)
            );
            dialog.show(getFragmentManager(), "");
            return;
        }

        Suggestion suggestion = getViewModel().getRealm().where(Suggestion.class).findFirst();
        if(suggestion != null){
            if (mOnSearchListener != null) {
                if(text.length() > 0){
                    AmplitudeManager.getInstance().logEvent(SEARCH_KEYWORD_SEARCH, AmplitudeParam.Key.VALUE, text);
                }

                Map<String, MapValue<?>> map = new HashMap<>();
                map.put(AmplitudeParam.Key.TYPE, new MapValue<>(suggestion.getType().toUpperCase()));
                map.put(AmplitudeParam.Key.VALUE, new MapValue<>(suggestion.getText()));
                AmplitudeManager.getInstance().logEvent(SEARCH_KEYWORD_CLICK_LIST_ITEM, map);

                mOnSearchListener.onSearch(suggestion);
                dismiss();
            }
        }
    }

    @Override
    public void onDelete() {
        getViewDataBinding().dialogSearchViewEditLayout.vCommonAddressEditText.setText("");
    }

    @Override
    public void onBack() {
        dismiss();
    }

    private void initListener() {
        getViewDataBinding().dialogSearchViewEditLayout.vCommonAddressEditText.setOnEditorActionListener((textView, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                onSearch();
                return true;
            }
            return false;
        });
    }

    private void setObserver() {
        getViewModel().getSuggestion().addChangeListener(suggestions -> getViewDataBinding().setEmpty(suggestions.size() == 0));

        rx.Observable<CharSequence> searchObserver = RxTextView.textChanges(getViewDataBinding().dialogSearchViewEditLayout.vCommonAddressEditText);
        mSubscription = searchObserver.subscribe(this::onTextChange);
    }

    private void onTextChange(CharSequence text) {
        if (text.toString().isEmpty()) {
            getViewModel().getDataManager().clearSuggestion();
        } else {
            getViewModel().loadSuggestionFromServer(text.toString());
        }
        getViewDataBinding().dialogSearchViewEditLayout.setText(text.toString());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public class SearchDialogAdapter extends RealmRecyclerViewAdapter<Suggestion, SearchDialogAdapter.SearchViewDialogViewHolder> implements
            OnSearchListener {

        public SearchDialogAdapter(OrderedRealmCollection<Suggestion> data) {
            super(data, true);
        }

        @NonNull
        @Override
        public SearchViewDialogViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            ISuggestionBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_suggestion, viewGroup, false);
            return new SearchViewDialogViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull SearchViewDialogViewHolder searchViewDialogViewHolder, int position) {
            if (getData() != null) {
                Suggestion suggestion = getData().get(position);
                searchViewDialogViewHolder.binding.setSuggestion(suggestion);
                searchViewDialogViewHolder.binding.setCallback(this);
                initChipGroup(searchViewDialogViewHolder.binding.iSuggestionChipGroup, suggestion.getServices());
                searchViewDialogViewHolder.binding.executePendingBindings();
            }
        }

        private void initChipGroup(ChipGroup chipGroup, List<Service> services) {
            chipGroup.removeAllViews();
            for (int i = 0; i < services.size(); i++) {
                Service service = services.get(i);
                if (service != null) {
                    VFacilityServiceChipGroupItemBinding binding = DataBindingUtil.inflate
                            (LayoutInflater.from(chipGroup.getContext()), R.layout.v_facility_service_chip_group_item, chipGroup, false);
                    String text = service.getServiceName();
                    String grade = service.getLatestGrade();
                    ColorStateList bgColor = getGradeColorStateList(grade);
                    ColorStateList strokeColor = bgColor;
                    ColorStateList textColor = getBaseActivity().getResources().getColorStateList(R.color.white);
                    binding.vFacilityServiceChip.setPaintFlags(0);
                    if(service.isClosed()){
                        bgColor = getBaseActivity().getResources().getColorStateList(R.color.colorClosedBg);
                        strokeColor = bgColor;
                        textColor = getBaseActivity().getResources().getColorStateList(R.color.colorClosedText);
                        binding.vFacilityServiceChip.setPaintFlags(binding.vFacilityServiceChip.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                    }
                    binding.vFacilityServiceChip.setChipBackgroundColor(bgColor);
                    binding.vFacilityServiceChip.setChipStrokeColor(strokeColor);
                    binding.vFacilityServiceChip.setTextColor(textColor);

                    if (grade != null) {
                        text = grade + " " + text;
                    }

                    binding.vFacilityServiceChip.setText(text);
                    chipGroup.addView(binding.vFacilityServiceChip);
                }
            }
        }

        private ColorStateList getGradeColorStateList(String gradeStr) {
            return CommonUtils.getColorStateListWithGrade(getBaseActivity(), gradeStr);
        }

        @Override
        public void onSearch(Suggestion suggestion) {
            dismiss();
            if (mOnSearchListener != null) {
                if(getViewDataBinding().dialogSearchViewEditLayout.vCommonAddressEditText.getText().length() > 0){
                    String searchText = getViewDataBinding().dialogSearchViewEditLayout.vCommonAddressEditText.getText().toString();
                    AmplitudeManager.getInstance().logEvent(SEARCH_KEYWORD_SEARCH, AmplitudeParam.Key.VALUE, searchText);
                }

                Map<String, MapValue<?>> map = new HashMap<>();
                map.put(AmplitudeParam.Key.TYPE, new MapValue<>(suggestion.getType().toUpperCase()));
                map.put(AmplitudeParam.Key.VALUE, new MapValue<>(suggestion.getText()));
                AmplitudeManager.getInstance().logEvent(SEARCH_KEYWORD_CLICK_LIST_ITEM, map);

                mOnSearchListener.onSearch(suggestion);
            }
        }

        class SearchViewDialogViewHolder extends RecyclerView.ViewHolder {
            final ISuggestionBinding binding;

            private SearchViewDialogViewHolder(ISuggestionBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}