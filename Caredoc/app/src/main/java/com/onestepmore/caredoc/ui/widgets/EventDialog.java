package com.onestepmore.caredoc.ui.widgets;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.Window;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.DialogEventBinding;
import com.onestepmore.caredoc.ui.base.BaseDialog;

public class EventDialog extends BaseDialog{
    public static EventDialog newInstance() {
        EventDialog dialog = new EventDialog();
        return dialog;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        DialogEventBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(getContext()), R.layout.dialog_event, null, false);


        binding.dialogEventImg.setOnClickListener((view) -> {
            dismissDialog(getTag());
        });

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getBaseActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        dialog.setCanceledOnTouchOutside(false);


        return dialog;
    }
}
