package com.onestepmore.caredoc.data.model.api.common;

import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.object.AddressObject;

public class AddressResponse extends ItemResponse<AddressObject> {
}
