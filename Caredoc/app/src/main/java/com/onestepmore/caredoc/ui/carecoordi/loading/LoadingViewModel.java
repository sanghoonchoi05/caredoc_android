package com.onestepmore.caredoc.ui.carecoordi.loading;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class LoadingViewModel extends BaseViewModel<LoadingNavigator> {
    public LoadingViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }
}