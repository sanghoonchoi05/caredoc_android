package com.onestepmore.caredoc.ui.main.taker.add.food;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerFoodFragmentModule {
    @Provides
    TakerFoodViewModel takerFoodViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerFoodViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerFoodFragment")
    ViewModelProvider.Factory provideTakerFoodViewModelProvider(TakerFoodViewModel takerFoodViewModel) {
        return new ViewModelProviderFactory<>(takerFoodViewModel);
    }
}
