package com.onestepmore.caredoc.ui.main.detail.review.write;

import android.app.Application;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.Gson;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewRemoveRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewSaveRequest;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Response;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.WRITE_REVIEW_COMPLETE;

public class WriteReviewViewModel extends BaseViewModel<WriteReviewNavigator> {

    private ReviewRepository mReviewRepository;
    private Gson mGson;

    public ReviewRepository getReviewRepository() {
        return mReviewRepository;
    }

    public WriteReviewViewModel(Application application, AppDataSource appDataSource,
                                SchedulerProvider schedulerProvider, Gson gson, ReviewRepository reviewRepository) {
        super(application, appDataSource, schedulerProvider);

        mReviewRepository = reviewRepository;
        mGson = gson;
    }

    public void onRegister(){
        getNavigator().onRegisterReview();
    }

    void registerReview(long serviceFakeId, ReviewSaveRequest request){
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(mGson.toJson(request, ReviewSaveRequest.class));

        }catch (JSONException e){
            return;
        }

        ReviewSaveRequest.PathParameter pathParameter = new ReviewSaveRequest.PathParameter();
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility_service.getType());
        pathParameter.setMorphId(serviceFakeId);

        getReviewRepository().saveReviewApiCall(jsonObject, pathParameter, new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    getNavigator().onCompleteRegisterReview();
                    AmplitudeManager.getInstance().logEvent(WRITE_REVIEW_COMPLETE);
                    FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.WRITE_USER_REVIEW_COMPLETE);
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }

    void removeReview(long id){
        ReviewRemoveRequest.PathParameter pathParameter = new ReviewRemoveRequest.PathParameter();
        pathParameter.setId(id);

        getReviewRepository().removeReviewApiCall(pathParameter, new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    getNavigator().onCompleteRemoveReview();
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }
}
