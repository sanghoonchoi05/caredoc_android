package com.onestepmore.caredoc.ui.account.account;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface AccountNavigator extends BaseNavigator {
    void showEmailRegisterView();
    void showLoginView();
    void showFacebookView();
    void showNaverView();
    void openMainActivity();
}
