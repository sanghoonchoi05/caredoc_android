package com.onestepmore.caredoc.data.model.api.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class CommentSaveRequest {
    @Expose
    @SerializedName("rating")
    private int rating;

    @Expose
    @SerializedName("content")
    private String content;

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static class PathParameter {
        @Expose
        @SerializedName("morphType")
        private String morphType;

        @Expose
        @SerializedName("morphId")
        private long morphId;

        public long getMorphId() {
            return morphId;
        }

        public void setMorphId(long morphId) {
            this.morphId = morphId;
        }

        public String getMorphType() {
            return morphType;
        }

        public void setMorphType(String morphType) {
            this.morphType = morphType;
        }
    }
}
