package com.onestepmore.caredoc.data.model.api.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.object.RatingsObject;
import com.onestepmore.caredoc.data.model.api.object.RatingsSummaryObject;
import com.onestepmore.caredoc.data.model.api.object.ReviewServiceObject;

import java.util.List;

public final class ReviewFacilityServiceSummaryResponse {
    @Expose
    @SerializedName("rating_avg")
    private float rating;

    @Expose
    @SerializedName("ratings")
    private List<RatingsSummaryObject.Item> ratings;

    @Expose
    @SerializedName("total")
    private int total;

    public float getRating() {
        return rating;
    }

    public int getTotal() {
        return total;
    }

    public List<RatingsSummaryObject.Item> getRatings() {
        return ratings;
    }
}
