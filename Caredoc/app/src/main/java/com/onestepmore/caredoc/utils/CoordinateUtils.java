package com.onestepmore.caredoc.utils;

import com.jhlabs.map.proj.Projection;
import com.jhlabs.map.proj.ProjectionFactory;

import java.awt.geom.Point2D;

public class CoordinateUtils {
    static String[] proj4_w = new String[] {
            "+proj=tmerc",
            "+lat_0=38",
            "+lon_0=127.5",
            "+ellps=GRS80",
            "+units=m",
            "+x_0=1000000",
            "+y_0=2000000",
            "+k=0.9996",
            "+no_defs"
    };

    public static double[] convertFromUTMKToWGS84(double x, double y){
        Projection proj = ProjectionFactory.fromPROJ4Specification(proj4_w);
        Point2D.Double src = new Point2D.Double(x, y);
        Point2D.Double dst = new Point2D.Double();
        proj.inverseTransform(src, dst);

        return new double[]{dst.y, dst.x};
    }
}
