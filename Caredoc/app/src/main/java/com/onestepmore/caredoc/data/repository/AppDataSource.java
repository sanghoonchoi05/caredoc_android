package com.onestepmore.caredoc.data.repository;

import android.support.annotation.NonNull;

import com.androidnetworking.error.ANError;
import com.onestepmore.caredoc.data.local.pref.PreferencesHelper;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkFacilityResponse;
import com.onestepmore.caredoc.data.model.api.taker.MyTakerResponse;
import com.onestepmore.caredoc.data.model.realm.LatLngAddress;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.data.remote.ApiHelper;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

import io.reactivex.Flowable;
import io.realm.Realm;
import okhttp3.Response;

public interface AppDataSource extends ApiHelper, PreferencesHelper {
    void onRegister();
    void onLogin();
    void onLogout();

    enum LoggedInMode {

        LOGGED_IN_MODE_LOGGED_OUT(0),
        LOGGED_IN_MODE_GOOGLE(1),
        LOGGED_IN_MODE_FB(2),
        LOGGED_IN_MODE_SERVER(3);

        private final int mType;

        LoggedInMode(int type) {
            mType = type;
        }

        public int getType() {
            return mType;
        }
    }

    LoggedInMode getLoggedInMode();
    void setLoggedInMode(LoggedInMode mode);
    boolean isLoggedIn();

    void handleApiError(ANError anError, @NonNull BaseNavigator navigator);
    void handleThrowable(Throwable throwable, @NonNull BaseNavigator navigator);
    void handleApiError(String jsonErrorBody, @NonNull BaseNavigator navigator);
    void handleApiError(Response response, @NonNull BaseNavigator navigator);

    Flowable<Boolean> loadUserInfoFromServer(Realm realm, String email, String password, String provider, String socialId, String socialToken);
    Flowable<Boolean> loadUserInfoFromServer(Realm realm, String email, String password);
    Flowable<Boolean> loadMyTakerFromServer(Realm realm);

    Flowable<Boolean> loadStaticData();
    Flowable<Boolean> loadStaticDisease();
    Flowable<Boolean> loadStaticSubject();

    Flowable<Boolean> loadStaticAddress(String wholeAddressName, String wholeAddressFullName);
    Flowable<Boolean> loadStaticAddress(String wholeAddressName, String wholeAddressFullName, String sidoCode, String gugunCode, double lat, double lng);

    Flowable<Boolean> loadSuggestion(String keyword);
    void clearSuggestion();

    void updateAccessToken(String accessToken, long expiresIn, String tokenType);
    void deleteAllFacility(Realm realm);
    void deleteAllTaker(Realm realm);
    void saveMyBookmark(Realm realm, BookmarkFacilityResponse response);
    void saveMyTaker(Realm realm, MyTakerResponse response);
    void deleteMyBookmark(Realm realm);
}
