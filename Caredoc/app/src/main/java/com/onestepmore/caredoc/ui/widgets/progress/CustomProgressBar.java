package com.onestepmore.caredoc.ui.widgets.progress;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

public class CustomProgressBar extends ProgressBar implements ViewPager.OnPageChangeListener{
    public interface OnStepChangeListener{
        void onStepChanged(int step);
    }
    private ViewPager pager;
    private int stepCount;
    private int currentStep;
    private int valuePerStep;
    private int previousStep;
    private int animProgress;
    private int animDuration;
    private OnStepChangeListener listener;

    // Running animations
    private AnimatorSet animatorSet;
    private ObjectAnimator barAnimator;

    public CustomProgressBar(Context context) {
        this(context, null);
    }

    public CustomProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.progressBarStyleHorizontal);
    }

    public CustomProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setMax(100);
        setSecondaryProgress(100);
    }

    public void setOnStepChangeListener(OnStepChangeListener listener){
        this.listener = listener;
    }

    public void setViewPager(ViewPager pager, int stepCount, int animDuration) {
        if (this.pager == pager) {
            return;
        }
        if (this.pager != null) {
            pager.removeOnPageChangeListener(this);
        }
        if (pager.getAdapter() == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }

        this.animDuration = animDuration;
        this.pager = pager;
        this.stepCount = stepCount;
        currentStep = 0;
        animProgress = 0;

        valuePerStep = getMax() / stepCount;

        pager.addOnPageChangeListener(this);
        setIndeterminate(false);
    }

    public void setCurrentStep(int currentStep) {
        if (currentStep < 0 || currentStep > stepCount) {
            throw new IllegalArgumentException("Invalid step value " + currentStep);
        }

        previousStep = this.currentStep;
        this.currentStep = currentStep;

        // Cancel any running animations
        if (animatorSet != null) {
            animatorSet.cancel();
        }

        animatorSet = null;

        int progressValue = 0;
        if(currentStep == stepCount - 1){
            progressValue = getMax();
        }else{
            progressValue = (currentStep + 1) * valuePerStep;
        }

        animatorSet = new AnimatorSet();
        barAnimator = ObjectAnimator.ofInt(CustomProgressBar.this, "animProgress", animProgress, progressValue);

        if (animatorSet != null) {
            // Max 500 ms for the animation
            barAnimator.setDuration(Math.min(400, animDuration));
            barAnimator.setInterpolator(new DecelerateInterpolator());
            animatorSet.play(barAnimator);
            animatorSet.start();
        }

        if(this.listener != null){
            this.listener.onStepChanged(currentStep);
        }
    }

    public void setAnimProgress(int animProgress) {
        this.animProgress = animProgress;
        setProgress(animProgress);
        invalidate();
    }

    public int getCurrentStep() {
        return currentStep;
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        setCurrentStep(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
