package com.onestepmore.caredoc.ui.widgets.chart;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

public class AxisValueFormatter implements IAxisValueFormatter
{
    private String [] mValues;
    public AxisValueFormatter(String [] values) {
        mValues = values;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        if(value < 0 ){
            return "";
        }

        int position = (int)value;
        return mValues.length > position ? mValues[position] : "";
    }
}
