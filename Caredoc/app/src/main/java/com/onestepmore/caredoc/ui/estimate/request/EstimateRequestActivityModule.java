package com.onestepmore.caredoc.ui.estimate.request;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class EstimateRequestActivityModule {
    @Provides
    EstimateRequestViewModel estimateRequestViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new EstimateRequestViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideEstimateRequestViewModelProvider(EstimateRequestViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
