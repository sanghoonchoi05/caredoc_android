package com.onestepmore.caredoc.ui.carecoordi;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface CareCoordiStartNavigator extends BaseNavigator {
    void onClose();
    void onStartCoordi();
}
