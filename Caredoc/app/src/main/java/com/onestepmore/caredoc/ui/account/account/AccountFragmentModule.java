package com.onestepmore.caredoc.ui.account.account;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountFragmentModule {
    @Provides
    AccountViewModel accountViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                      Gson gson) {
        return new AccountViewModel(application, dataSource, schedulerProvider, gson);
    }

    @Provides
    @Named("AccountFragment")
    ViewModelProvider.Factory provideAccountViewModelProvider(AccountViewModel accountViewModel) {
        return new ViewModelProviderFactory<>(accountViewModel);
    }
}
