package com.onestepmore.caredoc.ui.account.terms;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TermsFragmentProvider {
    @ContributesAndroidInjector(modules = TermsFragmentModule.class)
    abstract TermsFragment provideTermsFragmentFactory();
}
