package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilitySimpleObject extends FacilityObject{
    @Expose
    @SerializedName("services")
    private ServicesObject<ServiceSimpleObject> services;

    public ServicesObject<ServiceSimpleObject> getServices() {
        return services;
    }
}
