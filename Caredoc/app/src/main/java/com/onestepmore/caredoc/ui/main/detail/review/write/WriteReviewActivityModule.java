package com.onestepmore.caredoc.ui.main.detail.review.write;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class WriteReviewActivityModule {
    @Provides
    WriteReviewViewModel writeReviewViewModel(Application application, AppDataSource dataSource,
                                              SchedulerProvider schedulerProvider, Gson gson, ReviewRepository reviewRepository) {
        return new WriteReviewViewModel(application, dataSource, schedulerProvider, gson, reviewRepository);
    }

    @Provides
    ViewModelProvider.Factory provideWriteReviewViewModelProvider(WriteReviewViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
