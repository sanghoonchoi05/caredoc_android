package com.onestepmore.caredoc.ui.widgets;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.DialogBottomSheetBinding;
import com.onestepmore.caredoc.databinding.IDialogBottomSheetBinding;

public class BottomSheetDialog extends BottomSheetDialogFragment implements View.OnClickListener{

    private static final String BOTTOM_SHEET_DIALOG_LIST_KEY = "BOTTOM_SHEET_DIALOG_LIST_KEY";

    public static BottomSheetDialog getInstance(String[] textList) {
        Bundle bundle = new Bundle();
        bundle.putStringArray(BOTTOM_SHEET_DIALOG_LIST_KEY, textList);

        BottomSheetDialog dialog = new BottomSheetDialog();
        dialog.setArguments(bundle);
        return dialog;
    }

    String [] textList = null;

    BottomSheetDialogClickListener listener = null;
    DialogBottomSheetBinding mBinding;

    public void setClickListener(BottomSheetDialogClickListener listener){
        this.listener = listener;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if(bundle != null){
            textList = bundle.getStringArray(BOTTOM_SHEET_DIALOG_LIST_KEY);
        }

        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_bottom_sheet, container, false);

        RecyclerView recyclerView = mBinding.dialogBottomSheetRecyclerview;
        BottomSheetDialogAdapter adapter = new BottomSheetDialogAdapter();
        adapter.setTextList(textList);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mBinding.dialogBottomSheetCloseImg.setOnClickListener(this);
        mBinding.dialogBottomSheetInit.setOnClickListener(this);

        return mBinding.getRoot();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == mBinding.dialogBottomSheetInit.getId()){
            listener.onInit(getTag());
            dismiss();
        }else if(v.getId() == mBinding.dialogBottomSheetCloseImg.getId()){
            listener.onClose(getTag());
            dismiss();
        }

    }

    public class BottomSheetDialogAdapter extends RecyclerView.Adapter<BottomSheetDialogAdapter.BottomSheetDialogViewHolder> implements View.OnClickListener
    {
        String [] mTextList;

        public void setTextList(final String [] textList){
            mTextList = textList;
            notifyItemRangeInserted(0, mTextList.length);
        }

        @NonNull
        @Override
        public BottomSheetDialogViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            IDialogBottomSheetBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_dialog_bottom_sheet, viewGroup, false);
            return new BottomSheetDialogViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull BottomSheetDialogViewHolder bottomSheetDialogViewHolder, int position) {
            bottomSheetDialogViewHolder.binding.setText(mTextList[position]);
            bottomSheetDialogViewHolder.binding.setPosition(position);
            bottomSheetDialogViewHolder.itemView.setOnClickListener(this);
            bottomSheetDialogViewHolder.binding.executePendingBindings();
        }

        @Override
        public int getItemCount() {
            return mTextList == null ? 0 : mTextList.length;
        }

        @Override
        public void onClick(View v) {
            AppCompatTextView textView = (AppCompatTextView)v;
            if(textView!= null){
                listener.onClick(getTag(), textView.getText().toString());
                dismiss();
            }
        }

        class BottomSheetDialogViewHolder extends RecyclerView.ViewHolder {
            final IDialogBottomSheetBinding binding;

            private BottomSheetDialogViewHolder(IDialogBottomSheetBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}
