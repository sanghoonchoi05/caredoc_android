package com.onestepmore.caredoc.utils;

import android.app.Activity;
import android.support.design.widget.BottomSheetDialogFragment;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.onestepmore.caredoc.BuildConfig;
import com.onestepmore.caredoc.ui.account.account.AccountFragment;
import com.onestepmore.caredoc.ui.account.login.LoginFragment;
import com.onestepmore.caredoc.ui.account.login.forgot.LoginForgotFragment;
import com.onestepmore.caredoc.ui.account.register.email.EmailRegisterFragment;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.base.BaseBottomSheetDialog;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiStartActivity;
import com.onestepmore.caredoc.ui.carecoordi.loading.LoadingFragment;
import com.onestepmore.caredoc.ui.carecoordi.result.ResultFragment;
import com.onestepmore.caredoc.ui.main.detail.FacilityDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.comment.write.WriteCommentActivity;
import com.onestepmore.caredoc.ui.main.favorite.FavoriteFragment;
import com.onestepmore.caredoc.ui.main.favorite.detail.FavoriteDetailFragment;
import com.onestepmore.caredoc.ui.main.home.HomeFragment;
import com.onestepmore.caredoc.ui.main.profile.ProfileFragment;
import com.onestepmore.caredoc.ui.main.profile.collaboration.CollaborationFragment;
import com.onestepmore.caredoc.ui.main.profile.contact.ContactFragment;
import com.onestepmore.caredoc.ui.main.profile.recently.RecentlyFragment;
import com.onestepmore.caredoc.ui.main.profile.webview.WebViewActivity;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.filter.GradeFilterDialog;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.card.TakerCardFragment;
import com.onestepmore.caredoc.ui.main.taker.detail.TakerDetailActivity;
import com.onestepmore.caredoc.ui.splash.SplashActivity;
import com.onestepmore.caredoc.ui.start.StartActivity;

public class FirebaseAnalyticsUtils {

    public static void setCurrentScreen(FirebaseAnalytics firebaseAnalytics, BaseActivity activity){
        if(firebaseAnalytics == null || activity == null){
            return;
        }

        String screenName = getScreenName(activity);
        setCurrentScreen(firebaseAnalytics, activity, screenName);
    }

    public static void setCurrentScreen(FirebaseAnalytics firebaseAnalytics, BaseFragment fragment){
        if(firebaseAnalytics == null || fragment == null || fragment.getBaseActivity() == null){
            return;
        }

        String screenName = getScreenName(fragment);

        setCurrentScreen(firebaseAnalytics, fragment.getBaseActivity(), screenName);
    }

    public static void setCurrentScreen(FirebaseAnalytics firebaseAnalytics, BaseActivity activity, CareCoordiActivity.UI_ITEM uiItem){
        if(firebaseAnalytics == null || activity == null || uiItem == null){
            return;
        }

        String screenName = getCareCoordiUiItemName(uiItem);

        setCurrentScreen(firebaseAnalytics, activity, screenName);
    }

    public static void setCurrentScreen(FirebaseAnalytics firebaseAnalytics, BaseActivity activity, AddTakerActivity.STEP step){
        if(firebaseAnalytics == null || activity == null || step == null){
            return;
        }

        String screenName = getTakerStepName(step);

        setCurrentScreen(firebaseAnalytics, activity, screenName);
    }

    public static void setCurrentScreen(FirebaseAnalytics firebaseAnalytics, BaseBottomSheetDialog dialog){
        if(firebaseAnalytics == null || dialog == null){
            return;
        }

        String screenName = getScreenName(dialog);

        setCurrentScreen(firebaseAnalytics, dialog.getBaseActivity(), screenName);
    }

    private static void setCurrentScreen(FirebaseAnalytics firebaseAnalytics, Activity activity, String screenName){
        if(firebaseAnalytics == null || activity == null || CommonUtils.checkNullAndEmpty(screenName)){
            return;
        }

        if(!BuildConfig.DEBUG){
            firebaseAnalytics.setCurrentScreen(activity, screenName, null);
        }
    }

    private static String getScreenName(BaseActivity activity){
        String screenName = "";
        if(activity instanceof SplashActivity){
            screenName = "Splash";
        }
        else if(activity instanceof StartActivity){
            screenName = "Start";
        }
        else if(activity instanceof CareCoordiStartActivity){
            screenName = "CareCoordi_Main";
        }
        else if(activity instanceof WriteCommentActivity){
            screenName = "Review_Write";
        }
        else if(activity instanceof WebViewActivity){
            screenName = "WebView";
        }
        else if(activity instanceof TakerDetailActivity){
            screenName = "Taker_Detail";
        }

        return screenName;
    }

    private static String getScreenName(BaseFragment fragment){
        String screenName = "";
        if(fragment instanceof AccountFragment){
            screenName = "Account_Main";
        }
        else if(fragment instanceof EmailRegisterFragment){
            screenName = "Email_Register";
        }
        else if(fragment instanceof LoginFragment){
            screenName = "Email_Login";
        }
        else if(fragment instanceof LoginForgotFragment){
            screenName = "Email_ResetPassword";
        }
        else if(fragment instanceof HomeFragment){
            screenName = "Tab_Home";
        }
        else if(fragment instanceof SearchFragment){
            screenName = "Tab_Search";
        }
        else if(fragment instanceof FavoriteFragment){
            screenName = "Tab_Bookmark";
        }
        else if(fragment instanceof ProfileFragment){
            screenName = "Tab_Profile";
        }
        else if(fragment instanceof LoadingFragment){
            screenName = "CareCoordi_Loading";
        }
        else if(fragment instanceof ResultFragment){
            screenName = "CareCoordi_Result";
        }
        else if(fragment instanceof FacilityDetailFragment){
            screenName = "Facility_Detail";
        }
        else if(fragment instanceof FavoriteDetailFragment){
            screenName = "Bookmark_Detail";
        }
        else if(fragment instanceof CollaborationFragment){
            screenName = "Partner";
        }
        else if(fragment instanceof ContactFragment){
            screenName = "Contact";
        }
        else if(fragment instanceof RecentlyFragment){
            screenName = "Facility_Recently";
        }
        else if(fragment instanceof TakerCardFragment){
            screenName = "Taker_Card";
        }
        // Dialog 느낌의 Fragment의 경우 버튼 단위로 추적하자.
        /*else if(fragment instanceof AddressFilterFragment){
            screenName = "Filter_Address_Simple";
        }
        else if(fragment instanceof ServiceFilterFragment){
            screenName = "Filter_Service";
        }*/

        return screenName;
    }

    public static String getScreenName(BottomSheetDialogFragment dialog){
        String screenName = "";

        if(dialog instanceof GradeFilterDialog){
            screenName = "Filter_Facility_Grade";
        }

        return screenName;
    }

    private static String getCareCoordiUiItemName(CareCoordiActivity.UI_ITEM uiItem){
        String stepUiItemName = "";

        if(uiItem == CareCoordiActivity.UI_ITEM.USER_INFO){
            stepUiItemName = "CareCoordi_TakerInfo";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.NEED_HELP){
            stepUiItemName = "CareCoordi_Guide_Help";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.NEED_WHAT){
            stepUiItemName = "CareCoordi_Guide_What";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.NEED_WHERE){
            stepUiItemName = "CareCoordi_Guide_WHERE";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.HOME_CARE){
            stepUiItemName = "CareCoordi_Guide_Home_Care";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.CARE_PERIOD){
            stepUiItemName = "CareCoordi_Guide_Care_Period";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.SERVICE){
            stepUiItemName = "CareCoordi_Service";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.DISEASE){
            stepUiItemName = "CareCoordi_Classification_Disease";
        }
        else if(uiItem == CareCoordiActivity.UI_ITEM.TOOL){
            stepUiItemName = "CareCoordi_Classification_Tool";
        }

        return stepUiItemName;
    }

    private static String getTakerStepName(AddTakerActivity.STEP step){
        String stepName = "";

        if(step == AddTakerActivity.STEP.BASIC){
            stepName = "Taker_Add_Basic";
        }
        else if(step == AddTakerActivity.STEP.ADDRESS){
            stepName = "Taker_Add_Address";
        }
        else if(step == AddTakerActivity.STEP.CARE){
            stepName = "Taker_Add_Care";
        }
        else if(step == AddTakerActivity.STEP.PERFORMANCE){
            stepName = "Taker_Add_Performance";
        }
        else if(step == AddTakerActivity.STEP.DISEASE){
            stepName = "Taker_Add_Disease";
        }
        else if(step == AddTakerActivity.STEP.FOOD){
            stepName = "Taker_Add_Nutrition";
        }
        else if(step == AddTakerActivity.STEP.REMARK){
            stepName = "Taker_Add_Remark";
        }

        return stepName;
    }
}
