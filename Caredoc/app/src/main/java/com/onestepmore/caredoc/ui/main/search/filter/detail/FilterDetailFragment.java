package com.onestepmore.caredoc.ui.main.search.filter.detail;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.crystal.crystalrangeseekbar.interfaces.OnSeekbarChangeListener;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.onestepmore.caredoc.databinding.FFilterDetailBinding;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.filter.GradeFilterCallback;
import com.onestepmore.caredoc.ui.main.search.filter.GradeFilterDialog;
import com.onestepmore.caredoc.ui.main.search.filter.ServiceFilterAdapter;
import com.onestepmore.caredoc.ui.main.search.filter.ServiceFilterCallback;
import com.onestepmore.caredoc.ui.main.search.filter.service.ServiceFilter;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FILTER_DETAIL_CLICK_BACK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FILTER_DETAIL_CLICK_INIT;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.FILTER_DETAIL_APPLY_ORDER_BY;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.FILTER_DETAIL_APPLY_RATING;
import static com.onestepmore.caredoc.data.model.api.object.SearchFilter.DEFAULT_RATING;
import static com.onestepmore.caredoc.ui.main.search.filter.GradeFilterDialog.SERVICE_FILTER;

public class FilterDetailFragment extends BaseFragment<FFilterDetailBinding, FilterDetailViewModel> implements
        FilterDetailNavigator,
        GradeFilterCallback,
        ServiceFilterCallback
{
    @Inject
    @Named("FilterDetailFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private FilterDetailViewModel mFilterDetailViewModel;

    private List<ServiceFilter> mCachedServiceFilter;
    private Integer mCachedRating;
    private FacilitySearchRequest.ORDER_BY mCachedOrderBy;

    private boolean mChangedOrderBy = false;
    private boolean mChangedRating = false;
    private boolean mChangedGrade = false;

    private int mSelectedGradeFilterIndex = -1;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_filter_detail;
    }

    @Override
    public FilterDetailViewModel getViewModel() {
        mFilterDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(FilterDetailViewModel.class);
        return mFilterDetailViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFilterDetailViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fFilterDetailToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }

        SearchFragment fragment = parentFragment();
        if(fragment == null){
            return;
        }

        mCachedServiceFilter = fragment.getSelectedServicesFilter();
        mCachedRating = fragment.getSelectedRating();
        mCachedOrderBy = fragment.getOrderBy();

        if(mCachedServiceFilter == null){
            initServiceFilter();
        }

        initOrderBy();
        initRatingBar();
        initServiceFilterAdapter();

        getViewDataBinding().setChecked(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.filter_detail, menu);
        LinearLayout layout = (LinearLayout)menu.findItem(R.id.filter_detail_init).getActionView();
        layout.setOnClickListener((v) -> onInit());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AmplitudeManager.getInstance().logEvent(FILTER_DETAIL_CLICK_BACK);
                onClose();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initOrderBy(){
        selectOrderBy();
    }

    private void selectOrderBy(){
        getViewDataBinding().fFilterDetailSort.vFilterSortGradeLayout.setSelected(false);
        getViewDataBinding().fFilterDetailSort.vFilterSortReviewLayout.setSelected(false);
        getViewDataBinding().fFilterDetailSort.vFilterSortRatingLayout.setSelected(false);

        if(mCachedOrderBy == FacilitySearchRequest.ORDER_BY.grade){
            getViewDataBinding().fFilterDetailSort.vFilterSortGradeLayout.setSelected(true);
        }
        else if(mCachedOrderBy == FacilitySearchRequest.ORDER_BY.review){
            getViewDataBinding().fFilterDetailSort.vFilterSortReviewLayout.setSelected(true);
        }
        else if(mCachedOrderBy == FacilitySearchRequest.ORDER_BY.rating){
            getViewDataBinding().fFilterDetailSort.vFilterSortRatingLayout.setSelected(true);
        }
    }

    private void initRatingBar(){
        getViewDataBinding().fFilterDetailRating.vFilterRatingBar.setMinStartValue(mCachedRating).apply();
        getViewDataBinding().fFilterDetailRating.vFilterRatingBar.setOnSeekbarChangeListener(new OnSeekbarChangeListener() {
            @Override
            public void valueChanged(Number value) {
                mChangedRating = true;

                if(mCachedRating != value.intValue()){
                    getViewDataBinding().setChecked(true);
                }

                mCachedRating = value.intValue();
                parentFragment().setSelectedRating(mCachedRating);
                if(value.intValue() > 0){
                    UIUtils.setTextIncludeAnnotation(
                            getBaseActivity(),
                            R.string.f_filter_detail_rating_text,
                            getViewDataBinding().fFilterDetailRating.vFilterRatingText,
                            String.valueOf(value.intValue()));
                }
                else{
                    getViewDataBinding().fFilterDetailRating.vFilterRatingText.setText(R.string.f_filter_detail_rating_text_normal);
                }
            }
        });
    }

    private void initRating(){
        mCachedRating = DEFAULT_RATING;
        parentFragment().setSelectedRating(mCachedRating);
        getViewDataBinding().fFilterDetailRating.vFilterRatingBar.setMinStartValue(mCachedRating).apply();
    }

    private void initServiceFilter(){
        for(ServiceFilter filter : mCachedServiceFilter){
            filter.setGradeText(getString(R.string.common_all_grade));
            filter.setChecked(false);
        }

        RecyclerView.Adapter adapter = getViewDataBinding().fFilterDetailService.vServiceFilterRecyclerView.getAdapter();
        if(adapter != null){
            adapter.notifyItemRangeChanged(0, mCachedServiceFilter.size());
        }
    }

    private void initServiceFilterAdapter(){
        ServiceFilterAdapter adapter = new ServiceFilterAdapter();
        adapter.setCallback(this);
        getViewDataBinding().fFilterDetailService.vServiceFilterRecyclerView.setAdapter(adapter);
        getViewDataBinding().fFilterDetailService.vServiceFilterRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        adapter.addAll(mCachedServiceFilter);
    }

    @Override
    public void onServiceCheck(ServiceFilter service, boolean checked) {
        mChangedGrade = true;

        getViewDataBinding().setChecked(true);
        service.setChecked(checked);
    }

    @Override
    public void onServiceGradeClick(int position) {
        mChangedGrade = true;

        mSelectedGradeFilterIndex = position;

        GradeFilterDialog dialog = GradeFilterDialog.getInstance();
        dialog.setClickListener(this);
        Bundle bundle = new Bundle();
        bundle.putParcelable(SERVICE_FILTER, mCachedServiceFilter.get(position));

        dialog.setArguments(bundle);
        dialog.show(getBaseActivity().getSupportFragmentManager(), getClass().getName());
    }

    public void onClose() {
        ((MainActivity)getBaseActivity()).showBottomTab(true);
        BackStackManager.getInstance().onBackPressed();
    }

    public void onInit() {
        AmplitudeManager.getInstance().logEvent(FILTER_DETAIL_CLICK_INIT);

        getViewDataBinding().setChecked(false);

        initOrderBy();
        initRating();
        initServiceFilter();

        notifyApplyFilter();
    }

    @Override
    public void onApplyFilter() {
        onClose();

        if(mChangedOrderBy){
            String orderBy = AmplitudeParam.ORDER_BY_MODE.GRADE.toString();
            if(mCachedOrderBy == FacilitySearchRequest.ORDER_BY.rating){
                orderBy = AmplitudeParam.ORDER_BY_MODE.RATING.toString();
            }
            else if(mCachedOrderBy == FacilitySearchRequest.ORDER_BY.review){
                orderBy = AmplitudeParam.ORDER_BY_MODE.REVIEW.toString();
            }
            AmplitudeManager.getInstance().logEvent(FILTER_DETAIL_APPLY_ORDER_BY, AmplitudeParam.Key.MODE, orderBy);
        }

        if(mChangedRating){
            AmplitudeManager.getInstance().logEvent(FILTER_DETAIL_APPLY_RATING, AmplitudeParam.Key.VALUE, mCachedRating);
        }

        if(mChangedGrade){

        }

        notifyApplyFilter();
    }

    @Override
    public void onOrderByGradeClick() {
        mChangedOrderBy = true;

        mCachedOrderBy = FacilitySearchRequest.ORDER_BY.grade;
        checkOrderBy();
    }

    @Override
    public void onOrderByReviewClick() {
        mChangedOrderBy = true;

        mCachedOrderBy = FacilitySearchRequest.ORDER_BY.review;
        checkOrderBy();
    }

    @Override
    public void onOrderByRatingClick() {
        mChangedOrderBy = true;

        mCachedOrderBy = FacilitySearchRequest.ORDER_BY.rating;
        checkOrderBy();
    }


    private void checkOrderBy(){
        parentFragment().setOrderBy(mCachedOrderBy);
        selectOrderBy();
        getViewDataBinding().setChecked(true);
    }


    private SearchFragment parentFragment(){
        return (SearchFragment)getBaseActivity().getSupportFragmentManager().findFragmentByTag(SearchFragment.class.getName());
    }

    private void notifyApplyFilter(){
        SearchFragment fragment = parentFragment();
        if(fragment != null){
            fragment.onChangedFilter(false);
        }
    }

    @Override
    public void onGradeFilterClick(int position, String text) {

    }

    @Override
    public void onGradeFilterClose() {

    }

    @Override
    public void onGradeFilterInit() {

    }
}
