package com.onestepmore.caredoc.data.repository;

import android.content.Context;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.google.gson.Gson;
import com.onestepmore.caredoc.data.local.pref.PreferencesHelper;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerPhotoResponse;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerResponse;
import com.onestepmore.caredoc.data.model.api.taker.DeleteTakerRequest;
import com.onestepmore.caredoc.data.model.api.taker.MyTakerResponse;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailRequest;
import com.onestepmore.caredoc.data.model.api.taker.TakerDetailResponse;
import com.onestepmore.caredoc.data.remote.TakerApiHelper;

import org.json.JSONObject;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import okhttp3.Callback;

@Singleton
public class TakerRepository implements TakerDataSource {

    private TakerApiHelper mTakerApiHelper;
    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public TakerRepository(Context context, PreferencesHelper preferencesHelper, Gson gson,
                           TakerApiHelper takerApiHelper) {
        mTakerApiHelper = takerApiHelper;
        mPreferencesHelper = preferencesHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    @Override
    public Flowable<AddTakerPhotoResponse> addTakerPhotoApiCall(AddTakerPhotoRequest.PathParameter pathParameter, File file) {
        return mTakerApiHelper.addTakerPhotoApiCall(pathParameter, file);
    }

    @Override
    public void addTakerPhotoApiCall(Callback responseCallback, long takerId, File file) {
        mTakerApiHelper.addTakerPhotoApiCall(responseCallback, takerId, file);
    }

    @Override
    public Flowable<AddTakerResponse> addTakerApiCall(JSONObject jsonObject) {
        return mTakerApiHelper.addTakerApiCall(jsonObject);
    }

    @Override
    public Flowable<AddTakerResponse> modifyTakerApiCall(JSONObject jsonObject, AddTakerRequest.PathParameter pathParameter) {
        return mTakerApiHelper.modifyTakerApiCall(jsonObject, pathParameter);
    }

    @Override
    public Flowable<TakerDetailResponse> getTakerDetailApiCall(TakerDetailRequest.PathParameter pathParameter) {
        return mTakerApiHelper.getTakerDetailApiCall(pathParameter);
    }

    @Override
    public void deleteTakerApiCall(OkHttpResponseListener listener, DeleteTakerRequest.PathParameter pathParameter) {
        mTakerApiHelper.deleteTakerApiCall(listener, pathParameter);
    }
}
