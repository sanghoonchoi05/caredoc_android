package com.onestepmore.caredoc.ui.main.taker.add.care;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerCareFragmentModule {
    @Provides
    TakerCareViewModel takerCareViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerCareViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerCareFragment")
    ViewModelProvider.Factory provideTakerCareViewModelProvider(TakerCareViewModel takerCareViewModel) {
        return new ViewModelProviderFactory<>(takerCareViewModel);
    }
}
