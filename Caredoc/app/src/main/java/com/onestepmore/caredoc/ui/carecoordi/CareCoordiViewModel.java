package com.onestepmore.caredoc.ui.carecoordi;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiForHospitalResponse;
import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiForNursingHomeResponse;
import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiRequest;
import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiResponse;
import com.onestepmore.caredoc.data.model.api.object.AggregatesObject;
import com.onestepmore.caredoc.data.model.api.object.CareCoordiSearchFilter;
import com.onestepmore.caredoc.data.model.api.object.FacilityCareCoordiForHospitalObject;
import com.onestepmore.caredoc.data.model.api.object.FacilityCareCoordiForNursingHomeObject;
import com.onestepmore.caredoc.data.model.api.object.FacilityCareCoordiObject;
import com.onestepmore.caredoc.data.model.realm.FacilityCareCoordiList;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class CareCoordiViewModel extends BaseViewModel<CareCoordiNavigator> {

    private final Gson mGson;
    private int mSkip = 0;
    private CareCoordiRequest.ORDER_BY mOrderBy = CareCoordiRequest.ORDER_BY.grade_desc;
    private CareCoordiRequest.ORDER_DESC mOrderDesc = CareCoordiRequest.ORDER_DESC.desc;
    private String mCurrentPosition;

    public void setOrderBy(CareCoordiRequest.ORDER_BY orderBy) {
        this.mOrderBy = orderBy;
        this.mOrderDesc = CareCoordiRequest.ORDER_DESC.desc;
        if(!orderBy.desc()){
            this.mOrderDesc = CareCoordiRequest.ORDER_DESC.asc;
        }
    }

    public void setCurrentPosition(String currentPosition) {
        this.mCurrentPosition = currentPosition;
    }

    public void setSkip(int mSkip) {
        this.mSkip = mSkip;
    }

    private final FacilityRepository mFacilityRepository;

    public FacilityRepository getFacilityRepository() {
        return mFacilityRepository;
    }

    public CareCoordiViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                               Gson gson, FacilityRepository facilityRepository) {
        super(application, appDataSource, schedulerProvider);

        mFacilityRepository = facilityRepository;
        mGson = gson;

        mSkip = 0;
    }

    public void loadCareCoordiResult(CareCoordiSearchFilter filter){
        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject(mGson.toJson(filter));
            if(jsonObject.has("classifications")){
                JSONObject classificationsJsonObject = jsonObject.getJSONObject("classifications");
                JSONArray diseaseJsonArray = classificationsJsonObject.getJSONArray("diseases");
                JSONArray welfareToolsJsonArray = classificationsJsonObject.getJSONArray("welfareTools");
                if(diseaseJsonArray.length() == 0 && welfareToolsJsonArray.length() == 0){
                    jsonObject.put("classifications", new JSONObject());
                }
            }
        }catch (JSONException e){

        }

        if(jsonObject == null)
            return;

        CareCoordiRequest.QueryParameter queryParameter = new CareCoordiRequest.QueryParameter();
        queryParameter.setFilter(jsonObject.toString());
        queryParameter.setOrderBy(mOrderBy.getName());
        queryParameter.setOrderDesc(mOrderDesc.name());
        queryParameter.setCurrentPosition(mCurrentPosition);
        queryParameter.setSkip(mSkip);

        loadCareCoordiResult(queryParameter, filter.getCategory());
    }
    public void loadCareCoordiResult(CareCoordiRequest.QueryParameter parameter, String category){
        getCompositeDisposable().add(getFacilityRepository()
                .getFacilityCareCoordiListApiCall(parameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(jsonObject ->{
                    if(mSkip == 0){
                        clearFacilityCareCoordiRealm();
                    }

                    int size = 0;
                    if(category.equals(CareCoordiSearchFilter.CATEGORY.GERIATRIC_HOSPITAL.getName())){
                        CareCoordiForHospitalResponse response = convertToCareCoordiForHospitalResponse(jsonObject);
                        if(response != null){
                            size = response.getList().size();
                            saveCareCoordiForHospitalResponseRealm(response);
                        }
                    }else if(category.equals(CareCoordiSearchFilter.CATEGORY.NURSING_HOME.getName())){
                        CareCoordiForNursingHomeResponse response = convertToCareCoordiForNursingHomeResponse(jsonObject);
                        if(response != null){
                            size = response.getList().size();
                            saveCareCoordiForNursingHomeResponseRealm(response);
                        }
                    }else{
                        CareCoordiResponse response = convertToCareCoordiResponse(jsonObject);
                        if(response != null){
                            size = response.getList().size();
                            saveFacilityCareCoordiRealm(response);
                        }
                    }

                    if(mSkip == 0){
                        mSkip = size;
                    }
                    else{
                        mSkip += size;
                    }

                    //getNavigator().changeFacilityTotalCount(response.getTotal());
                }, throwable -> {
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private CareCoordiResponse convertToCareCoordiResponse(JSONObject jsonObject){
        CareCoordiResponse response = new CareCoordiResponse();
        response.setAggregates(getAggregatesObject(jsonObject));
        response.setTotal(getTotalCount(jsonObject));
        response.setList(getFacilityCareCoordiObject(jsonObject));

        return response;
    }

    private CareCoordiForHospitalResponse convertToCareCoordiForHospitalResponse(JSONObject jsonObject){
        CareCoordiForHospitalResponse response = new CareCoordiForHospitalResponse();
        response.setAggregates(getAggregatesObject(jsonObject));
        response.setTotal(getTotalCount(jsonObject));
        response.setList(getFacilityCareCoordiForHospitalObject(jsonObject));

        return response;
    }

    private CareCoordiForNursingHomeResponse convertToCareCoordiForNursingHomeResponse(JSONObject jsonObject){
        CareCoordiForNursingHomeResponse response = new CareCoordiForNursingHomeResponse();
        response.setAggregates(getAggregatesObject(jsonObject));
        response.setTotal(getTotalCount(jsonObject));
        response.setList(getFacilityCareCoordiForNursingHomeObject(jsonObject));

        return response;
    }

    private AggregatesObject getAggregatesObject(JSONObject jsonObject){
        AggregatesObject aggregatesObject = new AggregatesObject();
        try{
            JSONObject aggregatesJsonObject = jsonObject.getJSONObject("aggregates");
            if(aggregatesJsonObject.has("expenses_avg")){
                aggregatesObject.setExpensesAvg(aggregatesJsonObject.getInt("expenses_avg"));
            }

        }catch (JSONException e){

        }

        return aggregatesObject;
    }

    private int getTotalCount(JSONObject jsonObject){
        int totalCount = 0;
        try{
            if(jsonObject.has("total")){
                totalCount = jsonObject.getInt("total");
            }

        }catch (JSONException e){

        }

        return totalCount;
    }

    private List<FacilityCareCoordiObject> getFacilityCareCoordiObject(JSONObject jsonObject){
        List<FacilityCareCoordiObject> list = null;
        try{
            if(jsonObject.has("list")){
                Type listType = new TypeToken<List<FacilityCareCoordiObject>>(){}.getType();
                list = mGson.fromJson(jsonObject.getJSONArray("list").toString(),listType);
            }

        }catch (JSONException e){

        }

        return list;
    }

    private List<FacilityCareCoordiForHospitalObject> getFacilityCareCoordiForHospitalObject(JSONObject jsonObject){
        List<FacilityCareCoordiForHospitalObject> list = null;
        try{
            if(jsonObject.has("list")){
                Type listType = new TypeToken<List<FacilityCareCoordiForHospitalObject>>(){}.getType();
                list = mGson.fromJson(jsonObject.getJSONArray("list").toString(),listType);
            }

        }catch (JSONException e){

        }

        return list;
    }

    private List<FacilityCareCoordiForNursingHomeObject> getFacilityCareCoordiForNursingHomeObject(JSONObject jsonObject){
        List<FacilityCareCoordiForNursingHomeObject> list = null;
        try{
            if(jsonObject.has("list")){
                Type listType = new TypeToken<List<FacilityCareCoordiForNursingHomeObject>>(){}.getType();
                list = mGson.fromJson(jsonObject.getJSONArray("list").toString(),listType);
            }

        }catch (JSONException e){

        }

        return list;
    }

    private void saveFacilityCareCoordiRealm(CareCoordiResponse response){
        List<FacilityRealmModel> facilityRealmModelList = convertToFacilityRealmModelList(response);
        saveFacilityRealmModel(facilityRealmModelList);
        getNavigator().onResult(response.getAggregates(), response.getTotal());
    }

    private void saveCareCoordiForHospitalResponseRealm(CareCoordiForHospitalResponse response){
        List<FacilityRealmModel> facilityRealmModelList = convertToFacilityForHospitalRealmModelList(response);
        saveFacilityRealmModel(facilityRealmModelList);
        getNavigator().onResult(response.getAggregates(), response.getTotal());
    }

    private void saveCareCoordiForNursingHomeResponseRealm(CareCoordiForNursingHomeResponse response){
        List<FacilityRealmModel> facilityRealmModelList = convertToFacilityForNursingHomeRealmModelList(response);
        saveFacilityRealmModel(facilityRealmModelList);
        getNavigator().onResult(response.getAggregates(), response.getTotal());
    }

    private void saveFacilityRealmModel(List<FacilityRealmModel> facilityRealmModelList){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for(FacilityRealmModel facility : facilityRealmModelList){
                    realm.copyToRealmOrUpdate((FacilityCareCoordiList)facility);
                }
            }
        });
    }

    private List<FacilityRealmModel> convertToFacilityRealmModelList(CareCoordiResponse response){
        List<FacilityRealmModel> facilityRealmModelList = new ArrayList<>();
        for(FacilityCareCoordiObject facilityCareCoordiObject : response.getList()){
            FacilityCareCoordiList facility = new FacilityCareCoordiList();
            facility.setFacility(facilityCareCoordiObject);
            facilityRealmModelList.add(facility);
        }

        return facilityRealmModelList;
    }

    private List<FacilityRealmModel> convertToFacilityForHospitalRealmModelList(CareCoordiForHospitalResponse response){
        List<FacilityRealmModel> facilityRealmModelList = new ArrayList<>();
        for(FacilityCareCoordiForHospitalObject facilityCareCoordiForHospitalObject : response.getList()){
            FacilityCareCoordiList facility = new FacilityCareCoordiList();
            facility.setFacility(facilityCareCoordiForHospitalObject);
            facilityRealmModelList.add(facility);
        }

        return facilityRealmModelList;
    }

    private List<FacilityRealmModel> convertToFacilityForNursingHomeRealmModelList(CareCoordiForNursingHomeResponse response){
        List<FacilityRealmModel> facilityRealmModelList = new ArrayList<>();
        for(FacilityCareCoordiForNursingHomeObject facilityCareCoordiForNursingHomeObject : response.getList()){
            FacilityCareCoordiList facility = new FacilityCareCoordiList();
            facility.setFacility(facilityCareCoordiForNursingHomeObject);
            facilityRealmModelList.add(facility);
        }

        return facilityRealmModelList;
    }

    public void clearFacilityCareCoordiRealm(){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<FacilityCareCoordiList> realmResults = realm.where(FacilityCareCoordiList.class).findAll();
                if(realmResults != null){
                    realmResults.deleteAllFromRealm();
                }
            }
        });
    }
}
