package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class EvaluationObject {
    @Expose
    @SerializedName("service_type_id")
    private int serviceTypeId;

    @Expose
    @SerializedName("grade")
    private String grade;

    @Expose
    @SerializedName("year")
    private String year;

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public String getGrade() {
        return grade;
    }

    public String getYear() {
        return year;
    }
}
