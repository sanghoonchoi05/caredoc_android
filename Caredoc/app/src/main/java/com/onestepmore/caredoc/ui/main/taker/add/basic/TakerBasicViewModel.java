package com.onestepmore.caredoc.ui.main.taker.add.basic;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TakerBasicViewModel extends BaseViewModel<TakerBasicNavigator>{

    private final ObservableBoolean hide = new ObservableBoolean(true);

    public TakerBasicViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onRegisterPicture(){
        getNavigator().registerPicture();
    }


    public ObservableBoolean getHide() {
        return hide;
    }
}
