package com.onestepmore.caredoc.ui.main.detail.review.write;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface WriteReviewNavigator extends BaseNavigator {
    void onRegisterReview();
    void onCompleteRegisterReview();
    void onCompleteRemoveReview();
}
