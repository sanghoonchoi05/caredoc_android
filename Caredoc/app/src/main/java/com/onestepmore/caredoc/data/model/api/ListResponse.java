package com.onestepmore.caredoc.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListResponse<T> {
    @Expose
    @SerializedName("list")
    private List<T> list;

    @Expose
    @SerializedName("total")
    private int total;

    public int getTotal() {
        return total;
    }

    public List<T> getList() {
        return list;
    }
}
