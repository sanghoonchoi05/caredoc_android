package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class AddressObject {
    @Expose
    @SerializedName("b_code")
    private String bCode;
    @Expose
    @SerializedName("sido_code")
    private String sidoCode;
    @Expose
    @SerializedName("gugun_code")
    private String gugunCode;
    @Expose
    @SerializedName("dong_code")
    private String dongCode;
    @Expose
    @SerializedName("address_full_name")
    private String addressFullname;
    @Expose
    @SerializedName("address_name")
    private String addressName;

    @Expose
    @SerializedName("lat")
    private double lat;

    @Expose
    @SerializedName("lng")
    private double lng;

    public String getbCode() {
        return bCode;
    }

    public String getSidoCode() {
        return sidoCode;
    }

    public String getGugunCode() {
        return gugunCode;
    }

    public String getDongCode() {
        return dongCode;
    }

    public String getAddressFullname() {
        return addressFullname;
    }

    public String getAddressName() {
        return addressName;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
