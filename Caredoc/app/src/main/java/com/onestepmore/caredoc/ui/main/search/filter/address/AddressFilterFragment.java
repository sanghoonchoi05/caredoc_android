package com.onestepmore.caredoc.ui.main.search.filter.address;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.databinding.FAddressFilterBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;
import com.onestepmore.caredoc.ui.main.search.filter.address.list.AddressListFragment;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmQuery;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.FILTER_LOCATION_APPLY;

public class AddressFilterFragment extends BaseFragment<FAddressFilterBinding, AddressFilterViewModel> implements
        AddressFilterNavigator,
        BaseFragment.OnBackPressedListener,
        AppBarLayout.OnOffsetChangedListener
{
    @Inject
    @Named("AddressFilterFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private AddressFilterViewModel mAddressFilterViewModel;

    private StaticAddress mCachedStaticAddress;
    private StaticAddress mSelectedSido = null;
    private StaticAddress mSelectedGugun = null;
    private StaticAddress mSelectedDong = null;

    private boolean needApply = false;
    private boolean successInit = false;

    private int mPosition = 0;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_address_filter;
    }

    @Override
    public AddressFilterViewModel getViewModel() {
        mAddressFilterViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AddressFilterViewModel.class);
        return mAddressFilterViewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressFilterViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.filter_detail, menu);
        LinearLayout layout = (LinearLayout)menu.findItem(R.id.filter_detail_init).getActionView();
        layout.setOnClickListener((v) -> onInit());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClose();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fAddressFilterToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }
        getViewDataBinding().fAddressFilterAppBar.addOnOffsetChangedListener(this);

        SearchFragment fragment = parentFragment();
        if(fragment == null){
            return;
        }

        mCachedStaticAddress = fragment.getSelectedAddress();

        init();
    }

    private void init(){
        if(checkCachedStaticAddress()){
            initSelectedAddress();
            initPagerAdapter();
            setIndicator();

            successInit = true;
        }
    }

    private boolean checkCachedStaticAddress(){
        if(mCachedStaticAddress != null){
            if(mCachedStaticAddress.getSidoCode() != null){
                List<StaticAddress> gugunList = getNextStaticAddressFindAll(mCachedStaticAddress.getSidoCode() , null ,null);
                if(gugunList.size() == 0){
                    successInit = false;
                    StaticAddress sidoAddress = getStaticAddressFindFirst(mCachedStaticAddress.getSidoCode(), null, null);
                    getViewModel().loadAddressDataFromServer(getString(
                            R.string.common_all),
                            sidoAddress.getAddressFullname() + " " + getString(R.string.common_all),
                            sidoAddress);
                    return false;
                }
            }

            if(mCachedStaticAddress.getGugunCode() != null){
                List<StaticAddress> dongList = getNextStaticAddressFindAll(mCachedStaticAddress.getSidoCode() , mCachedStaticAddress.getGugunCode() ,null);
                if(dongList.size() == 0){
                    successInit = false;
                    StaticAddress gugunAddress = getStaticAddressFindFirst(mCachedStaticAddress.getSidoCode(), mCachedStaticAddress.getGugunCode(), null);
                    getViewModel().loadAddressDataFromServer(getString(
                            R.string.common_all),
                            gugunAddress.getAddressFullname() + " " + getString(R.string.common_all),
                            gugunAddress);
                    return false;
                }
            }
        }
        return true;
    }

    private void initSelectedAddress(){
        mSelectedSido = getStaticAddressFindFirst("0", null, null);

        if(mCachedStaticAddress != null){
            if(mCachedStaticAddress.getSidoCode() != null){
                mSelectedSido = getStaticAddressFindFirst(mCachedStaticAddress.getSidoCode(), null, null);
                if(!mSelectedSido.getSidoCode().equals("0")){
                    mPosition = 1;
                }
            }
        }

        if(mCachedStaticAddress != null){
            if(mCachedStaticAddress.getGugunCode() != null){
                mSelectedGugun = getStaticAddressFindFirst(mCachedStaticAddress.getSidoCode(), mCachedStaticAddress.getGugunCode(), null);
                if(!mSelectedGugun.getGugunCode().equals("0")){
                    mPosition = 2;
                }
            }
        }

        if(mCachedStaticAddress != null){
            if(mCachedStaticAddress.getDongCode() != null){
                mSelectedDong = getStaticAddressFindFirst(mCachedStaticAddress.getSidoCode(), mCachedStaticAddress.getGugunCode(), mCachedStaticAddress.getDongCode());
                mPosition = 2;
            }
        }
    }

    private void initPagerAdapter(){
        AddressFilterPagerAdapter adapter = new AddressFilterPagerAdapter(getChildFragmentManager());

        // 광역시도 화면
        AddressListFragment fragment = AddressListFragment.newInstance(
                getNextStaticAddressFindAll(null, null, null),
                mSelectedSido);
        fragment.setAddressListNavigator(this);
        adapter.addAddressListFragment(fragment, this);

        // 시군구 화면
        String sido = null;
        if(!mSelectedSido.getSidoCode().equals("0")){
            sido = mSelectedSido.getSidoCode();
        }
        fragment = AddressListFragment.newInstance(
                getNextStaticAddressFindAll(sido, null, null),
                mSelectedGugun);
        fragment.setAddressListNavigator(this);
        adapter.addAddressListFragment(fragment, this);

        // 동읍면 화면
        String gugun = null;
        if(mSelectedGugun != null && !mSelectedGugun.getGugunCode().equals("0")){
            gugun = mSelectedGugun.getGugunCode();
        }
        fragment = AddressListFragment.newInstance(
                getNextStaticAddressFindAll(sido, gugun, null),
                mSelectedDong);
        fragment.setAddressListNavigator(this);
        adapter.addAddressListFragment(fragment, this);

       getViewDataBinding().fAddressFilterViewPager.setAdapter(adapter);
       getViewDataBinding().fAddressFilterViewPager.setCurrentItem(mPosition, true);
    }

    private SearchFragment parentFragment(){
        return (SearchFragment)getBaseActivity().getSupportFragmentManager().findFragmentByTag(SearchFragment.class.getName());
    }

    @Override
    public void onLoadComplete() {
        if(successInit){
            onAddressFilterClick(mCachedStaticAddress);
        }
        else{
            init();
        }
    }

    @Override
    public void onAddressFilterClick(StaticAddress address) {
        if(address  == null){
            return;
        }

        mCachedStaticAddress = address;

        String sido = address.getSidoCode();
        String gugun = address.getGugunCode();
        String dong = address.getDongCode();

        final List<StaticAddress> realmResults = getNextStaticAddressFindAll(
                sido, gugun, dong);

        // 주소 코드가 없으면 서버에 요청
        if (realmResults.size() == 0) {
            getViewModel().loadAddressDataFromServer(getString(
                    R.string.common_all),
                    address.getAddressFullname() + " " + getString(R.string.common_all),
                    address);

            return;
        }

        setSelectAddress(mPosition, address);
        setArguments(mPosition);

        if (gugun == null) {
            // 광역시도 클릭 시
            mSelectedSido = address;
            initGugunAddress();

            if(!sido.equals("0")){
                mPosition = 1;
                setAddress(1, getNextStaticAddressFindAll(sido, null, null));
            }
        } else if (dong == null) {
            // 시군구 클릭 시
            mSelectedGugun = address;

            initDongAddress();

            if(!gugun.equals("0")){
                mPosition = 2;
                setAddress(2, getNextStaticAddressFindAll(sido, gugun, null));
            }
        } else {
            // 동면읍 클릭 시
            mPosition = 2;
            mSelectedDong = address;
        }

        setCachedStaticAddress();

        getViewDataBinding().fAddressFilterViewPager.setCurrentItem(mPosition, true);
        setIndicator();
    }

    @Override
    public void onAddressTextClick(int position) {
        mPosition = position;

        if(position == 1){
            initDongAddress();
        }
        else if(position == 0){
            initGugunAddress();
        }

        setCachedStaticAddress();
        getViewDataBinding().fAddressFilterViewPager.setCurrentItem(position, true);
        setIndicator();
    }

    private void initGugunAddress(){
        if(mCachedStaticAddress == null){
            return;
        }

        mSelectedGugun = getStaticAddressFindFirst(mCachedStaticAddress.getSidoCode(), "0", null);
        mSelectedDong = null;
    }

    private void initDongAddress(){
        if(mCachedStaticAddress == null){
            return;
        }

        mSelectedDong = getStaticAddressFindFirst(mCachedStaticAddress.getSidoCode(), mCachedStaticAddress.getGugunCode(), "0");
    }

    private void setCachedStaticAddress(){
        if(mSelectedDong != null){
            mCachedStaticAddress = mSelectedDong;
        }
        else if(mSelectedGugun != null){
            mCachedStaticAddress = mSelectedGugun;
        }
        else if(mSelectedSido != null){
            mCachedStaticAddress = mSelectedSido;
        }
}

    private void setIndicator(){
        getViewDataBinding().fAddressFilterIndicatorSido.setText("");
        getViewDataBinding().fAddressFilterIndicatorGugun.setText("");
        getViewDataBinding().fAddressFilterIndicatorDong.setText("");

        String sidoAddressName = mSelectedSido != null ? mSelectedSido.getAddressName() : "";
        String gugunAddressName = mSelectedGugun != null ? mSelectedGugun.getAddressName() : "";
        String dongAddressName = mSelectedDong != null ? mSelectedDong.getAddressName() : "";

        if(sidoAddressName.isEmpty()){
            sidoAddressName = mSelectedSido != null ? mSelectedSido.getAddressFullname() : "";
        }
        if(gugunAddressName.isEmpty()){
            gugunAddressName = mSelectedGugun != null ? mSelectedGugun.getAddressFullname() : "";
        }
        if(dongAddressName.isEmpty()){
            dongAddressName = mSelectedDong != null ? mSelectedDong.getAddressFullname() : "";
        }

        if(mPosition == 0){
            UIUtils.setTextIncludeAnnotation(
                    getBaseActivity(),
                    R.string.f_address_filter_indicator2,
                    getViewDataBinding().fAddressFilterIndicatorSido,
                    sidoAddressName);
        }
        else if(mPosition == 1){
            getViewDataBinding().fAddressFilterIndicatorSido.setText(getString(R.string.f_address_filter_indicator, sidoAddressName));
            UIUtils.setTextIncludeAnnotation(
                    getBaseActivity(),
                    R.string.f_address_filter_indicator2,
                    getViewDataBinding().fAddressFilterIndicatorGugun,
                    gugunAddressName);
        }
        else if(mPosition == 2){
            getViewDataBinding().fAddressFilterIndicatorSido.setText(getString(R.string.f_address_filter_indicator, sidoAddressName));
            getViewDataBinding().fAddressFilterIndicatorGugun.setText(getString(R.string.f_address_filter_indicator, gugunAddressName));
            UIUtils.setTextIncludeAnnotation(
                    getBaseActivity(),
                    R.string.f_address_filter_indicator2,
                    getViewDataBinding().fAddressFilterIndicatorDong,
                    dongAddressName);
        }
    }

    @Override
    public void onInit() {
        needApply = true;

        mSelectedSido = getStaticAddressFindFirst("0", null, null);;
        mSelectedGugun = null;
        mSelectedDong = null;

        mCachedStaticAddress = mSelectedSido;

        mPosition = 0;

        setAddress(mPosition, getNextStaticAddressFindAll(null, null, null));
        setArguments(mPosition);

        getViewDataBinding().fAddressFilterViewPager.setCurrentItem(mPosition, true);
        setIndicator();
    }

    private void setAddress(int position, List<StaticAddress> addressList){
        AddressListFragment fragment = getAddressListFragment(position);
        if(fragment != null){
            fragment.setAddress(addressList);
        }
    }

    private void setSelectAddress(int position, StaticAddress staticAddress){
        AddressListFragment fragment = getAddressListFragment(position);
        if(fragment != null){
            fragment.setSelectAddress(staticAddress);
        }
    }

    private void setArguments(int position){
        AddressListFragment fragment = getAddressListFragment(position);
        if(fragment != null){
            fragment.setArguments();
        }
    }

    @Override
    public void onApplyFilter() {
        onBack();

        SearchFragment fragment = parentFragment();
        if(fragment != null){
            Map<String, MapValue<?>> map = new HashMap<>();
            map.put(AmplitudeParam.Key.VALUE, new MapValue<>(mCachedStaticAddress.getAddressFullname()));
            map.put(AmplitudeParam.Key.MODE, new MapValue<>(fragment.getMode().toString()));
            AmplitudeManager.getInstance().logEvent(FILTER_LOCATION_APPLY, map);

            fragment.setSelectedAddress(mCachedStaticAddress);
            fragment.onChangedFilter(true);
        }
    }

    @Override
    public void onClose(){
        if(needApply){
            onApplyFilter();
        }
        else{
            onBack();
        }
    }

    public void onBack() {
        if(getBaseActivity() instanceof  MainActivity){
            ((MainActivity)getBaseActivity()).showBottomTab(true);
        }
        BackStackManager.getInstance().onBackPressed();
    }

    private AddressListFragment getAddressListFragment(int position){
        AddressFilterPagerAdapter adapter = (AddressFilterPagerAdapter)getViewDataBinding().fAddressFilterViewPager.getAdapter();
        if(adapter != null){
            return adapter.getAddressListFragment(position);
        }

        return null;
    }
    @Override
    public void doBack() {
        int position = mPosition - 1;
        if(position < 0){
            onClose();
        }
        else{
            onAddressTextClick(position);
        }
    }

    private List<StaticAddress> getNextStaticAddressFindAll(String sido, String gugun, String dong) {
        RealmQuery<StaticAddress> query = getViewModel().getRealm().where(StaticAddress.class);
        if (dong != null) {
            query = query.equalTo("sidoCode", sido)
                    .equalTo("gugunCode", gugun)
                    .equalTo("dongCode", dong);
        } else if (gugun != null) {
            query = query.equalTo("sidoCode", sido)
                    .equalTo("gugunCode", gugun)
                    .isNotNull("dongCode")
                    .distinct("dongCode");
        } else if( sido != null){
            query = query.equalTo("sidoCode", sido)
                    .isNotNull("gugunCode")
                    .isNull("dongCode");
        } else{
            query = query.isNull("gugunCode")
                    .isNull("dongCode");
        }

        return query.findAll();
    }

    private StaticAddress getStaticAddressFindFirst(String sido, String gugun, String dong) {
        RealmQuery<StaticAddress> query = getViewModel().getRealm().where(StaticAddress.class);
        query = query.equalTo("sidoCode", sido)
                .equalTo("gugunCode", gugun)
                .equalTo("dongCode", dong);

        return query.findFirst();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onScrollChange(NestedScrollView nestedScrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (nestedScrollView.canScrollVertically(-1)) {
                // Remove elevation
                getViewDataBinding().fAddressFilterAppBar.setElevation(10f);
            } else {
                getViewDataBinding().fAddressFilterAppBar.setElevation(0f);
            }
        }
    }

    // Firebase Crashlytics - AddressFilterFragment.java line 527
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if(verticalOffset == 0){
                getViewDataBinding().fAddressFilterAppBar.setElevation(0f);
            }
        }
    }
}
