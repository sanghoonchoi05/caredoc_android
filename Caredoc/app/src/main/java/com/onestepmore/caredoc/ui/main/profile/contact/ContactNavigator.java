package com.onestepmore.caredoc.ui.main.profile.contact;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface ContactNavigator extends BaseNavigator {
    void onTypeLayoutClick();
    void onSendMail();
    void onSuccessSendMail();
}
