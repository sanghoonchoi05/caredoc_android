package com.onestepmore.caredoc.utils;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.view.animation.Interpolator;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.maps.android.SphericalUtil;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.ui.AppLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapUtils {
    public static List<LatLng> getKiloMeterMarker(LatLng startPoint, LatLng endPoint, float markerDistance){
        List<LatLng> markerPointList = new ArrayList<>();

        // 시작점과 끝점과의 각도
        double heading = SphericalUtil.computeHeading(startPoint, endPoint);
        double totalDistance = SphericalUtil.computeDistanceBetween(startPoint, endPoint); // 시작점과 끝점까지의 거리
        float offsetMeter = markerDistance;

        // 찍어야할 킬로미터마커가 마지막점을 지났는지 체크
        while (offsetMeter <= totalDistance){
            // 다음 찍어야할 킬로미터마커의 LatLng를 구한다.
            LatLng markerPoint = SphericalUtil.computeOffset(startPoint, markerDistance, heading);
            markerPointList.add(markerPoint);

            offsetMeter += markerDistance;
            startPoint = markerPoint;
        }


        return markerPointList;
    }

    public static int getDiagonalHalfDistance(GoogleMap map) {
        double distance = 0D;
        if(map!=null){
            VisibleRegion visibleRegion = map.getProjection().getVisibleRegion();
            distance = SphericalUtil.computeDistanceBetween(visibleRegion.farLeft, map.getCameraPosition().target);
        }
        AppLogger.i("DiagonalHalfDistance: " + distance);

        return (int)distance;
    }

    /*public static Bitmap getBitmapFromKMView(View view, float km, int width, int height){
        if(view!=null){
            TextView kmText = view.findViewById(R.id.v_km_marker_text);
            String formattedStr = String.format(Locale.getDefault(), "%.0fK", km);

            float decimal = km % (int)km;
            if(decimal < 1){
                formattedStr = String.format(Locale.getDefault(), "%.1fK", km);
            }
            kmText.setText(formattedStr);

            return GraphicsUtils.createBitmapFromView(view, width, height);
        }

        return null;
    }*/

    public static float getKMMarkerDistanceWithCameraPosition(GoogleMap map){
        // 킬로미터마커
        // 1 = 1m
        int diagonalHalfDistance = MapUtils.getDiagonalHalfDistance(map);
        float kmMarkerDistRange = 200f;
        if(diagonalHalfDistance >= 5000){
            kmMarkerDistRange = 5000f;
        }
        else if(diagonalHalfDistance >= 500){
            kmMarkerDistRange = 1000f;
        }

        return kmMarkerDistRange;
    }

    public static float getKMMarkerDistanceWithTotalDistance(double distance){
        // 킬로미터마커
        // 1 = 1m
        float kmMarkerDistRange = 5000f;
        if(distance <= 1000){
            kmMarkerDistRange = 200f;
        }
        else if(distance <= 5000){
            kmMarkerDistRange = 1000f;
        }

        return kmMarkerDistRange;
    }

    public static void startDropMarkerAnimation(GoogleMap map, final Marker marker, long delayMillis) {
        marker.setVisible(false);

        final LatLng target = marker.getPosition();
        final long start = SystemClock.uptimeMillis() + delayMillis;

        Projection proj = map.getProjection();
        final long duration = 400L;
        Point startPoint = proj.toScreenLocation(target);
        startPoint.y = startPoint.y - 80;

        final LatLng startLatLng = proj.fromScreenLocation(startPoint);

        final Interpolator interpolator = new LinearOutSlowInInterpolator();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                marker.setVisible(true);
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * target.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * target.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later == 60 frames per second
                    handler.postDelayed(this, 16);
                }
            }
        }, delayMillis);
    }

    public static void setDarkPolygonWholeMap(GoogleMap map){
        if(map!=null){
            map.addPolygon(new PolygonOptions()
                    .add(new LatLng(85,90), new LatLng(85,0.1),
                            new LatLng(85,-90), new LatLng(85,-179.9),
                            new LatLng(0,-179.9), new LatLng(-85,-179.9),
                            new LatLng(-85,-90), new LatLng(-85,0.1),
                            new LatLng(-85,90), new LatLng(-85,179.9),
                            new LatLng(0,179.9), new LatLng(85,179.9))
                    .fillColor(Color.argb(150, 0, 0, 0))
                    .strokeColor(Color.TRANSPARENT));
        }
    }

    /**
     * Creates a List of LatLngs that form a rectangle with the given dimensions.
     */
    public static List<LatLng> createRectangle(LatLng center, double halfWidth, double halfHeight) {
        return Arrays.asList(new LatLng(center.latitude - halfHeight, center.longitude - halfWidth),
                new LatLng(center.latitude - halfHeight, center.longitude + halfWidth),
                new LatLng(center.latitude + halfHeight, center.longitude + halfWidth),
                new LatLng(center.latitude + halfHeight, center.longitude - halfWidth),
                new LatLng(center.latitude - halfHeight, center.longitude - halfWidth));
    }

    public static float getZoomValue(StaticAddress address){
        float zoom = 7f;

        if(address.getDongCode() != null){
            zoom = 13f;
        }else if(address.getGugunCode() != null){
            zoom = 12f;
        }else if(address.getSidoCode() != null){
            zoom = 10f;
        }

        return zoom;
    }
}
