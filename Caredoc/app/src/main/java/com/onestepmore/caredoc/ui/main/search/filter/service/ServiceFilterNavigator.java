package com.onestepmore.caredoc.ui.main.search.filter.service;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface ServiceFilterNavigator extends BaseNavigator {
    void onClose();
    void onInit();
    void onApplyFilter();
}
