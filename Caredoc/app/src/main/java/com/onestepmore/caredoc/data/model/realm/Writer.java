package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.WriterObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Writer extends RealmObject implements Parcelable {
    @PrimaryKey
    private long id;
    private String name;
    private String email;
    private String contact;
    private boolean emailVerifiedAt;
    private String photoUrl;

    public Writer(){}

    protected Writer(Parcel in) {
        id = in.readLong();
        name = in.readString();
        email = in.readString();
        contact = in.readString();
        emailVerifiedAt = in.readByte() != 0;
        photoUrl = in.readString();
    }

    public void setWriter(WriterObject writer){
        this.id = writer.getId();
        this.name = writer.getName();
        this.email = writer.getEmail();
        this.contact = writer.getContact();
        this.emailVerifiedAt = writer.isEmailVerifiedAt();
        this.photoUrl = writer.getPhotoUrl();
    }

    public static final Creator<Writer> CREATOR = new Creator<Writer>() {
        @Override
        public Writer createFromParcel(Parcel in) {
            return new Writer(in);
        }

        @Override
        public Writer[] newArray(int size) {
            return new Writer[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(contact);
        dest.writeByte((byte) (emailVerifiedAt ? 1 : 0));
        dest.writeString(photoUrl);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public boolean isEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(boolean emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
