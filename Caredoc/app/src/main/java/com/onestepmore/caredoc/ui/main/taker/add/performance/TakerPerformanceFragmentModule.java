package com.onestepmore.caredoc.ui.main.taker.add.performance;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class TakerPerformanceFragmentModule {
    @Provides
    TakerPerformanceViewModel takerPerformanceViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new TakerPerformanceViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("TakerPerformanceFragment")
    ViewModelProvider.Factory provideTakerPerformanceViewModelProvider(TakerPerformanceViewModel takerPerformanceViewModel) {
        return new ViewModelProviderFactory<>(takerPerformanceViewModel);
    }
}
