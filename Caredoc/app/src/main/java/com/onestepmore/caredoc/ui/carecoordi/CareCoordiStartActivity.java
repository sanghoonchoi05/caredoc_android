package com.onestepmore.caredoc.ui.carecoordi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.ACareCoordiStartBinding;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.utils.SnackbarUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;

public class CareCoordiStartActivity extends BaseActivity<ACareCoordiStartBinding, CareCoordiStartViewModel> implements
        CareCoordiStartNavigator,
        OnCompleteListener<Location>
{

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private CareCoordiStartViewModel mCareCoordiStartViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_care_coordi_start;
    }

    @Override
    public CareCoordiStartViewModel getViewModel() {
        mCareCoordiStartViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CareCoordiStartViewModel.class);
        return mCareCoordiStartViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCareCoordiStartViewModel.setNavigator(this);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        requestMyLocation();

        UIUtils.setStatusBarColor(this, getWindow().getDecorView(), getResources().getColor(R.color.colorSubBg), true);
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onClose() {
        finish();
    }

    @Override
    public void onStartCoordi() {
        finish();
        Intent intent = newIntent(CareCoordiActivity.class);
        startActivityForResult(intent, 0);
    }

    public void requestMyLocation(){
        checkPermission();

        if(getViewModel().getDataManager().isLocationPermissionGranted()){
            getDeviceLocation();
        }
    }

    @SuppressLint("MissingPermission")
    public void checkPermission() {
        if (checkLocationPermissions()) {
            getViewModel().getDataManager().setLocationPermissionGranted(true);
        } else {
            getViewModel().getDataManager().setLocationPermissionGranted(false);
            requestLocationPermissions();
        }
    }

    private boolean checkLocationPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestLocationPermissions() {
        // 사용자가 권한 요청을 한번 거절하면 shouldProvideRationale는 true로 온다.
        //
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) {
            SnackbarUtils.showSnackbar(this, getString(R.string.permission_location_rationale),
                    getString(android.R.string.ok), SnackbarRequestPermissionClickListener);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public View.OnClickListener SnackbarRequestPermissionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ActivityCompat.requestPermissions(CareCoordiStartActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    };

    public void getDeviceLocation() {
        try {
            if (mFusedLocationProviderClient != null) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this);
            }
        } catch (SecurityException e)  {
            AppLogger.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onComplete(@NonNull Task<Location> task) {
        if (task.isSuccessful() && task.getResult() !=null) {
            Location location = task.getResult();
            getViewModel().getDataManager().setLastKnownLocationLatitude(String.valueOf(location.getLatitude()));
            getViewModel().getDataManager().setLastKnownLocationLongitude(String.valueOf(location.getLongitude()));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        getViewModel().getDataManager().setLocationPermissionGranted(false);

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {

                SnackbarUtils.showSnackbar(this, getString(R.string.permission_user_canceled),
                        getString(android.R.string.ok), SnackbarRequestPermissionClickListener);

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getViewModel().getDataManager().setLocationPermissionGranted(true);
                requestMyLocation();

            } else {
                // Permission denied.
                SnackbarUtils.showSnackbar(this, getString(R.string.permission_location_rationale),
                        getString(android.R.string.ok), SnackbarRequestPermissionClickListener);
            }
        }
    }
}
