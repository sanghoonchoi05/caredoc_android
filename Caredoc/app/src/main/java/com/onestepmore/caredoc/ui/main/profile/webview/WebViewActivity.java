package com.onestepmore.caredoc.ui.main.profile.webview;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebChromeClient;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.AWebViewBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.utils.CommonUtils;

import javax.inject.Inject;

public class WebViewActivity extends BaseActivity<AWebViewBinding, WebViewViewModel> implements WebViewNavigator {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private WebViewViewModel mWebViewViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_web_view;
    }

    @Override
    public WebViewViewModel getViewModel() {
        mWebViewViewModel = ViewModelProviders.of(this, mViewModelFactory).get(WebViewViewModel.class);
        return mWebViewViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWebViewViewModel.setNavigator(this);

        String url = "";
        String title = "";
        Intent intent = getIntent();
        if(intent != null){
            url = intent.getStringExtra("url");
            title = intent.getStringExtra("title");
        }

        if(CommonUtils.checkNullAndEmpty(url) || CommonUtils.checkNullAndEmpty(title)){
            return;
        }

        setSupportActionBar(getViewDataBinding().aWebViewToolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setCustomView(R.layout.v_webview_action_bar);
            getSupportActionBar().getCustomView().findViewById(R.id.v_webview_action_bar_back_img).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            AppCompatTextView textView = (AppCompatTextView)getSupportActionBar().getCustomView().findViewById(R.id.v_webview_action_bar_title);
            textView.setText(title);
        }

        // TODO - shouldOverrideUrlLoading not called.
        getViewDataBinding().aWebView.getSettings().setJavaScriptEnabled(true);
        //getViewDataBinding().aWebView.getSettings().setDomStorageEnabled(false);
        //getViewDataBinding().aWebView.getSettings().setLoadWithOverviewMode(true);
        //getViewDataBinding().aWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        getViewDataBinding().aWebView.setWebViewClient(new MyWebViewClient());
        getViewDataBinding().aWebView.getSettings().setAppCacheEnabled(false);
        getViewDataBinding().aWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        getViewDataBinding().aWebView.loadUrl(url);
    }

    private class MyWebViewClient extends WebViewClient
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            AppLogger.i(getClass(), url);
        }
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return shouldOverrideUrlLoading(view, request.getUrl().toString());
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url + "?webview=1");
            return true;
        }
    }
    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }
}
