package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.CommentObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Review extends RealmObject implements Parcelable {
    @PrimaryKey
    private long id;
    private float rating;
    private String content;
    private String createdAt;
    private String updatedAt;
    private boolean isOwn;
    private Writer writer;

    public Review(){}

    protected Review(Parcel in) {
        id = in.readLong();
        rating = in.readFloat();
        content = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        isOwn = in.readByte() != 0;
        writer = in.readParcelable(Writer.class.getClassLoader());
    }

    public void setReview(CommentObject review){
        this.id = review.getId();
        this.rating = review.getRating() / 2;
        this.content = review.getContent();
        this.createdAt = review.getCreatedAt();
        this.updatedAt = review.getUpdatedAt();
        this.isOwn = review.isOwn();
        Writer writer = new Writer();
        writer.setWriter(review.getWriter());
        this.writer = writer;
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeFloat(rating);
        dest.writeString(content);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeByte((byte) (isOwn ? 1 : 0));
        dest.writeParcelable(writer, flags);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public void setOwn(boolean own) {
        isOwn = own;
    }

    public Writer getWriter() {
        return writer;
    }

    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
