package com.onestepmore.caredoc.ui.account.register.email;

import android.app.Application;
import android.databinding.ObservableBoolean;

import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.data.model.api.account.LoginRequest;
import com.onestepmore.caredoc.data.model.api.account.RegisterRequest;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.firebase.FirebaseParam;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import io.realm.Realm;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.Situation.EMAIL_REGISTER_COMPLETE_REGISTER;

public class EmailRegisterViewModel extends BaseViewModel<EmailRegisterNavigator> {
    private final ObservableBoolean emailCorrect = new ObservableBoolean(false);
    private final ObservableBoolean nameCorrect = new ObservableBoolean(false);
    private final ObservableBoolean passwordCorrect = new ObservableBoolean(false);
    private final ObservableBoolean passwordConfirmCorrect = new ObservableBoolean(false);

    private final ObservableBoolean possibleRegister = new ObservableBoolean(false);

    public void setPossibleRegister(boolean show){
        possibleRegister.set(show);
    }

    public void setEmailCorrect(boolean show){
        emailCorrect.set(show);
    }

    public void setNameCorrect(boolean show){
        nameCorrect.set(show);
    }

    public void setPasswordCorrect(boolean show){
        passwordCorrect.set(show);
    }

    public void setPasswordConfirmCorrect(boolean show){
        passwordConfirmCorrect.set(show);
    }

    public ObservableBoolean getEmailCorrect() {
        return emailCorrect;
    }

    public ObservableBoolean getNameCorrect() {
        return nameCorrect;
    }

    public ObservableBoolean getPasswordConfirmCorrect() {
        return passwordConfirmCorrect;
    }

    public ObservableBoolean getPasswordCorrect() {
        return passwordCorrect;
    }

    public EmailRegisterViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void register(final String email, final String name, final String password, final String passwordConfirmation) {
        if(!possibleRegister.get()){
            getNavigator().onRegisterFailed(
                    getEmailCorrect().get(),
                    getNameCorrect().get(),
                    getPasswordCorrect().get(),
                    getPasswordConfirmCorrect().get());
            return;
        }

        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerRegisterApiCall(new RegisterRequest(email, name, password, passwordConfirmation))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    User user = new User();
                    user.setUserId(response.getUserId());
                    user.setEmail(response.getEmail());
                    user.setName(response.getName());
                    user.setPassword(password);
                    user.setPhotoUrl(response.getPhotoUrl());
                    insertUser(user);

                    AmplitudeManager.getInstance().logEvent(EMAIL_REGISTER_COMPLETE_REGISTER);
                    FirebaseManager.getInstance().logEvent(FirebaseEvent.Situation.EMAIL_REGISTER_COMPLETE);
                    FirebaseManager.getInstance().logEvent(
                            FirebaseEvent.PredefinedEvent.SIGN_UP_COMPLETE,
                            new FirebaseManager.Builder()
                                    .addParam(FirebaseParam.PredefinedKey.METHOD, FirebaseParam.Value.EMAIL)
                                    .build());
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private void login(String email, String password){
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .doServerLoginApiCall(new LoginRequest.ServerLoginRequest(email, password))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    getDataManager().updateAccessToken(response.getToken(), response.getExpiresIn(), response.getTokenType());
                    getUserInfo(email, password);
                    //AppFirebaseAnalytics.getInstance().logEvent(AppFirebaseAnalytics.EVENT.EmailJoin);
                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private void getUserInfo(String email, String password){
        getCompositeDisposable().add(getDataManager()
                .loadUserInfoFromServer(getRealm(), email, password)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);
                    if(response){
                        getNavigator().openMainActivity();
                    }

                    User user = getRealm().where(User.class).findFirst();
                    if(getDataManager().isLoggedIn() && user != null){
                        AmplitudeManager.getInstance().setUserProperty(user);
                    }

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    private void insertUser(User user){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(user);
                login(user.getEmail(), user.getPassword());
            }
        });
    }

    public void onRegisterButtonClick(){
        getNavigator().onRegister();
    }
}
