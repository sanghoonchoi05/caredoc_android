package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;

import com.onestepmore.caredoc.data.model.api.object.external.JusoObject;

import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Juso implements JusoRealmModel {
    @PrimaryKey
    private String id;
    private String roadAddress;
    private String jibunAddress;
    private String roadCode;
    private String zipCode;
    private String buildingName;
    private String detail;
    private String dongCode;
    private String buildingMainNo;
    private String buildingSubNo;
    private String isUnderground;
    private String entX;
    private String entY;
    private double lat;
    private double lng;

    public Juso() {}

    protected Juso(Parcel in) {
        id = in.readString();
        roadAddress = in.readString();
        jibunAddress = in.readString();
        roadCode = in.readString();
        zipCode = in.readString();
        buildingName = in.readString();
        detail = in.readString();
        dongCode = in.readString();
        buildingMainNo = in.readString();
        buildingSubNo = in.readString();
        isUnderground = in.readString();
        entX = in.readString();
        entY = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
    }

    public static final Creator<Juso> CREATOR = new Creator<Juso>() {
        @Override
        public Juso createFromParcel(Parcel in) {
            return new Juso(in);
        }

        @Override
        public Juso[] newArray(int size) {
            return new Juso[size];
        }
    };

    public void setJuso(JusoObject juso){
        this.id = juso.getBdMgtSn();
        this.roadCode = juso.getRnMgtSn();
        this.roadAddress = juso.getRoadAddrPart1();
        this.jibunAddress = juso.getJibunAddr();
        this.zipCode = juso.getZipNo();
        this.buildingName = juso.getBdNm();
        this.dongCode = juso.getAdmCd();
        this.buildingMainNo = juso.getBuldMnnm();
        this.buildingSubNo = juso.getBuldSlno();
        this.isUnderground = juso.getUdrtYn();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(roadAddress);
        dest.writeString(jibunAddress);
        dest.writeString(roadCode);
        dest.writeString(zipCode);
        dest.writeString(buildingName);
        dest.writeString(detail);
        dest.writeString(dongCode);
        dest.writeString(buildingMainNo);
        dest.writeString(buildingSubNo);
        dest.writeString(isUnderground);
        dest.writeString(entX);
        dest.writeString(entY);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }

    @Override
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    @Override
    public String getRoadAddress() {
        return roadAddress;
    }
    @Override
    public void setRoadAddress(String roadAddress) {
        this.roadAddress = roadAddress;
    }
    @Override
    public String getJibunAddress() {
        return jibunAddress;
    }
    @Override
    public void setJibunAddress(String jibunAddress) {
        this.jibunAddress = jibunAddress;
    }
    @Override
    public String getRoadCode() {
        return roadCode;
    }
    @Override
    public void setRoadCode(String roadCode) {
        this.roadCode = roadCode;
    }
    @Override
    public String getZipCode() {
        return zipCode;
    }
    @Override
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    @Override
    public String getBuildingName() {
        return buildingName;
    }
    @Override
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }
    @Override
    public String getDetail() {
        return detail;
    }
    @Override
    public void setDetail(String detail) {
        this.detail = detail;
    }
    @Override
    public String getDongCode() {
        return dongCode;
    }
    @Override
    public void setDongCode(String dongCode) {
        this.dongCode = dongCode;
    }
    @Override
    public String getBuildingMainNo() {
        return buildingMainNo;
    }
    @Override
    public void setBuildingMainNo(String buildingMainNo) {
        this.buildingMainNo = buildingMainNo;
    }
    @Override
    public String getBuildingSubNo() {
        return buildingSubNo;
    }
    @Override
    public void setBuildingSubNo(String buildingSubNo) {
        this.buildingSubNo = buildingSubNo;
    }
    @Override
    public String isUnderground() {
        return isUnderground;
    }
    @Override
    public void setUnderground(String  underground) {
        isUnderground = underground;
    }
    @Override
    public String getEntX() {
        return entX;
    }
    @Override
    public void setEntX(String entX) {
        this.entX = entX;
    }
    @Override
    public String getEntY() {
        return entY;
    }
    @Override
    public void setEntY(String entY) {
        this.entY = entY;
    }
    @Override
    public double getLat() {
        return lat;
    }
    @Override
    public double getLng() {
        return lng;
    }
    @Override
    public void setLat(double lat) {
        this.lat = lat;
    }
    @Override
    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public boolean isModelSame(Object obj) {
        return false;
    }

    @Override
    public boolean isModelContentsSame(Object obj) {
        return false;
    }
}
