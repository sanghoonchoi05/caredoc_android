package com.onestepmore.caredoc.ui.main.detail;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.object.RatingsSummaryObject;
import com.onestepmore.caredoc.databinding.IRatingBarBinding;

import java.util.List;

public class RatingBarAdapter extends RecyclerView.Adapter<RatingBarAdapter.RatingBarViewHolder> {
    private List<RatingsSummaryObject.Item> mRatingList;

    public RatingBarAdapter(@NonNull List<RatingsSummaryObject.Item> ratingList) {
        setRatingList(ratingList);
    }

    public void setRatingList(@NonNull List<RatingsSummaryObject.Item> ratingList){
        mRatingList = ratingList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RatingBarViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IRatingBarBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_rating_bar, viewGroup, false);
        return new RatingBarViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingBarViewHolder viewHolder, int position) {
        final RatingsSummaryObject.Item item = mRatingList.get(position);
        viewHolder.binding.setLabel(item.getLabel());
        viewHolder.binding.setPoint(item.getValue());
        viewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mRatingList.size();
    }

    class RatingBarViewHolder extends RecyclerView.ViewHolder {
        final IRatingBarBinding binding;

        private RatingBarViewHolder(IRatingBarBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
