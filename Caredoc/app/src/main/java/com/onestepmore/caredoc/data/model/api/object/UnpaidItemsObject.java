package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class UnpaidItemsObject extends ItemResponse<UnpaidItemsObject.Item> {
    public enum TYPE{
        UPI_1("UPI-1"),
        UPI_2("UPI-2"),
        UPI_3("UPI-3"),
        UPI_4("UPI-4"),
        UPI_5("UPI-5"),
        UPI_6("UPI-6"),
        UPI_99("UPI-77");

        private String type;
        TYPE(String type){
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }
    public final class Item{
        @Expose
        @SerializedName("code")
        private String code;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("amount")
        private int amount;

        @Expose
        @SerializedName("base")
        private String base;

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public int getAmount() {
            return amount;
        }

        public String getBase() {
            return base;
        }
    }
}
