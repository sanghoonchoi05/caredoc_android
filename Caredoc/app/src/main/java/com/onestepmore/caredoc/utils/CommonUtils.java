/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.utils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.SpannedString;
import android.text.TextUtils;
import android.util.Patterns;

import com.google.android.gms.maps.model.LatLng;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.data.model.realm.Suggestion;
import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.ui.main.search.SearchFragment;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CommonUtils {

    private static boolean touchBlock = false;

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static void setTouchBlock(boolean block) {
        touchBlock = block;
    }

    public static boolean isTouchBlock() {
        return touchBlock;
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        }

        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isNameValid(String name) {
        if (TextUtils.isEmpty(name)) {
            return false;
        }

        return name.length() > 1 && name.length() < 21;
    }

    public static boolean checkNullAndEmpty(String text) {
        return text == null || text.isEmpty();
    }

    public static boolean checkNullAndNumber(Float text) {
        return text == null || text.isNaN();
    }

    public static Float plusFloat(Float first, Float second) {
        if(!checkNullAndNumber(first) && !checkNullAndNumber(second)){
            return UnitUtils.getFirstDecimal(first + second);
        }

        if(!checkNullAndNumber(first)){
            return first;
        }

        if(!checkNullAndNumber(second)){
            return second;
        }

        return null;
    }

    public static boolean isPasswordValid(String password) {
        if (TextUtils.isEmpty(password)) {
            return false;
        } else if (password.length() < 8) {
            return false;
        } else if(password.equals(password.toUpperCase())){
            return false;
        } else if(!isContainsNumber(password)){
            return false;
        }

        return true;
    }

    public static boolean isContainsNumber(String s) {
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher(s);

        return m.find();
    }

    public static boolean isEmpty(CharSequence sequence) {
        return sequence.length() != 0;
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static Drawable getCategoryIconForList(Context context, final int serviceTypeId) {
        int drawableId = R.drawable.ico_type_alzheimer;
        if(serviceTypeId == StaticService.SERVICE_TYPE.HOSPITAL.getId()){
            drawableId = R.drawable.ico_type_hospital;
        }
        else if(serviceTypeId == StaticService.SERVICE_TYPE.NURSING_HOME.getId()){
            drawableId = R.drawable.ico_type_long_care;
        }
        else if(serviceTypeId == StaticService.SERVICE_TYPE.VISIT_BATH.getId() ||
                serviceTypeId == StaticService.SERVICE_TYPE.VISIT_CARE.getId() ||
                serviceTypeId == StaticService.SERVICE_TYPE.VISIT_NURSING.getId() ||
                serviceTypeId == StaticService.SERVICE_TYPE.PROTECT_FOR_DAY_AND_NIGHT.getId() ||
                serviceTypeId == StaticService.SERVICE_TYPE.PROTECT_FOR_SHORT_TERM.getId() ||
                serviceTypeId == StaticService.SERVICE_TYPE.WELFARE_TOOL.getId()){
            drawableId = R.drawable.ico_type_home_care;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static Drawable getCategoryWhiteIcon(Context context, String categoryCode) {
        int drawableId = R.drawable.ico_type_white_alzheimer;
        if (categoryCode != null) {
            switch (categoryCode) {
                case "FC-01":
                    drawableId = R.drawable.ico_type_white_hospital;
                    break;
                case "FC-02":
                    drawableId = R.drawable.ico_type_white_home;
                    break;
                case "FC-03":
                    drawableId = R.drawable.ico_type_white_home_care;
                    break;
                default:
                    drawableId = R.drawable.ico_type_white_alzheimer;
                    break;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static Drawable getCategoryIconBig(Context context, String categoryCode) {
        int drawableId = R.drawable.ico_selectcate_alzheimer;
        if (categoryCode != null) {
            switch (categoryCode) {
                case "FC-01":
                    drawableId = R.drawable.ico_selectcate_hospital;
                    break;
                case "FC-02":
                    drawableId = R.drawable.ico_selectcate_home_care;
                    break;
                case "FC-03":
                    drawableId = R.drawable.ico_selectcate_home;
                    break;
                default:
                    drawableId = R.drawable.ico_selectcate_alzheimer;
                    break;
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static int getServiceIconResourceId(StaticService service) {
        int drawableId = 0;
        if (service != null) {
            switch (service.getId()) {
                case 1:
                    drawableId = R.drawable.img_medical;
                    break;
                case 2:
                    drawableId = R.drawable.img_entrance;
                    break;
                case 3:
                    drawableId = R.drawable.img_entrance_dementia;
                    break;
                case 4:
                    drawableId = R.drawable.img_visit_care;
                    break;
                case 5:
                    drawableId = R.drawable.img_visit_bath;
                    break;
                case 6:
                    drawableId = R.drawable.img_care_center_day_and_night;
                    break;
                case 7:
                    drawableId = R.drawable.img_care_center_short_term;
                    break;
                case 8:
                    drawableId = R.drawable.img_visit_nursing;
                    break;
                case 9:
                    drawableId = R.drawable.img_welfare;
                    break;
                case 10:
                    drawableId = R.drawable.img_care_center_day_and_night_demetia;
                    break;
                default:
                    drawableId = R.drawable.img_medical;
                    break;
            }
        }

        return drawableId;
    }

    public static int getSearchCategoryIdIconResourceId(StaticCategory category) {
        int drawableId = 0;
        if (category != null) {
            switch (category.getId()) {
                case 1:
                    drawableId = R.drawable.img_medical;
                    break;
                case 2:
                    drawableId = R.drawable.img_entrance;
                    break;
                case 3:
                    drawableId = R.drawable.img_entrance_dementia;
                    break;
                case 4:
                    drawableId = R.drawable.img_visit_care;
                    break;
                case 6:
                    drawableId = R.drawable.img_care_center_day_and_night;
                    break;
                default:
                    drawableId = R.drawable.img_medical;
                    break;
            }
        }

        return drawableId;
    }

    public static Drawable getServiceIcon(Context context, StaticService service) {
        int drawableId = getServiceIconResourceId(service);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static Drawable getCategoryIconForTab(Context context, String categoryEnum) {
        int drawableId = R.drawable.ico_type_alzheimer_tab;
        if (categoryEnum != null) {
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static Drawable getOvalDrawableWithGrade(Context context, String grade, boolean isClosed) {
        int drawableId = R.drawable.oval_list_no_grade;

        if(grade != null){
            if (grade.equals("E") || grade.equals("5")) {
                drawableId = R.drawable.oval_list_grade_5;
            } else if (grade.equals("D") || grade.equals("4")) {
                drawableId = R.drawable.oval_list_grade_4;
            } else if (grade.equals("C") || grade.equals("3")) {
                drawableId = R.drawable.oval_list_grade_3;
            } else if (grade.equals("B") || grade.equals("2")) {
                drawableId = R.drawable.oval_list_grade_2;
            } else if (grade.equals("A") || grade.equals("1")) {
                drawableId = R.drawable.oval_list_grade_1;
            }
        }

        if (isClosed) {
            drawableId = R.drawable.oval_list_closed_grade;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static Drawable getGradeMarkerDrawableWithGradePoint(Context context) {
        int drawableId = R.drawable.pin_grade_none_map;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(drawableId, null);
        } else {
            return context.getResources().getDrawable(drawableId);
        }
    }

    public static Integer getColorWithGrade(Context context, String grade) {
        Integer color = ContextCompat.getColor(context, R.color.no_grade);

        if(grade == null){
            return color;
        }

        if (grade.equals("E") || grade.equals("5")) {
            color = ContextCompat.getColor(context, R.color.grade_5);
        } else if (grade.equals("D") || grade.equals("4")) {
            color = ContextCompat.getColor(context, R.color.grade_4);
        } else if (grade.equals("C") || grade.equals("3")) {
            color = ContextCompat.getColor(context, R.color.grade_3);
        } else if (grade.equals("B") || grade.equals("2")) {
            color = ContextCompat.getColor(context, R.color.grade_2);
        } else if (grade.equals("A") || grade.equals("1")) {
            color = ContextCompat.getColor(context, R.color.grade_1);
        }

        return color;
    }

    public static ColorStateList getColorStateListWithGrade(Context context, String grade) {
        ColorStateList color = context.getResources().getColorStateList(R.color.no_grade);

        if(grade == null){
            return color;
        }

        if (grade.equals("E") || grade.equals("5")) {
            color = context.getResources().getColorStateList(R.color.grade_5);
        } else if (grade.equals("D") || grade.equals("4")) {
            color = context.getResources().getColorStateList(R.color.grade_4);
        } else if (grade.equals("C") || grade.equals("3")) {
            color = context.getResources().getColorStateList(R.color.grade_3);
        } else if (grade.equals("B") || grade.equals("2")) {
            color = context.getResources().getColorStateList(R.color.grade_2);
        } else if (grade.equals("A") || grade.equals("1")) {
            color = context.getResources().getColorStateList(R.color.grade_1);
        }

        return color;
    }

    public static LatLng getKoreaCenterPosition(){
        return new LatLng(36.613071, 127.930777);
    }

    public static int getCareCoordiTitleResId(StepData stepData){
        int titleResId = R.string.care_coordi_user_info_title;

        if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.NEED_HELP){
            titleResId = R.string.care_coordi_need_help_title;
        }else if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.NEED_WHAT){
            titleResId = R.string.care_coordi_need_what_title;
        }else if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.NEED_WHERE){
            titleResId = R.string.care_coordi_need_where_title;
        }else if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.HOME_CARE){
            titleResId = R.string.care_coordi_need_service_title;
        }else if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.CARE_PERIOD){
            titleResId = R.string.care_coordi_need_period_title;
        }else if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.SERVICE){
            titleResId = R.string.care_coordi_need_service_title;
        }else if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.DISEASE){
            titleResId = R.string.care_coordi_disease_title;
        }else if(stepData.getUIItem() == CareCoordiActivity.UI_ITEM.TOOL){
            titleResId = R.string.care_coordi_tool_title;
        }

        return titleResId;
    }

    public static boolean isIncludeLastConsonant(char word){
        boolean include = false;

        char comVal = (char) (word-0xAC00);
        if (comVal <= 11172){
            // 한글일경우
            // 유니코드 표에 맞추어 초성 중성 종성을 분리합니다..
            char cho = (char) ((((comVal - (comVal % 28)) / 28) / 21) + 0x1100);
            char jung = (char) ((((comVal - (comVal % 28)) / 28) % 21) + 0x1161);
            char jong = (char) ((comVal % 28) + 0x11a7);

            if(jong!=4519){
                include = true;
            }
        } else {
            // 한글이 아닐경우
        }

        return include;
    }

    public static CharSequence getText(Context context, int id, Object... args) {
        for(int i = 0; i < args.length; ++i)
            args[i] = args[i] instanceof String? TextUtils.htmlEncode((String)args[i]) : args[i];
        return Html.fromHtml(String.format(Html.toHtml(new SpannedString(context.getText(id))), args));
    }

    public static String getFilterName(Context context, SearchFragment.FILTER_UI filter){
        String name = "";
        switch (filter){
            case location:
                name = context.getString(R.string.f_search_location_filter);
                break;
            case service:
                name = context.getString(R.string.f_search_service_filter);
                break;
            case rating:
                name = context.getString(R.string.f_search_rating_filter);
                break;
        }

        return  name;
    }

    public static StaticAddress getStaticAddress(Suggestion suggestion){
        String code = suggestion.getValue();
        String sido = null;
        String gugun = null;
        String dong = null;

        if(code.length() == 5){
            dong = "0";
        }else if(code.length() == 2){
            gugun = "0";
        }

        if(code.length() == 8){
            dong = code.substring(5);
            code = code.substring(0, 5);
        }

        if(code.length() == 5){
            gugun = code.substring(2, 5);
            code = code.substring(0, 2);
        }

        if(code.length() == 2){
            sido = code;
        }

        StaticAddress staticAddress = new StaticAddress();
        staticAddress.setSidoCode(sido);
        staticAddress.setGugunCode(gugun);
        staticAddress.setDongCode(dong);
        staticAddress.setAddressFullname(suggestion.getText());

        return staticAddress;
    }

    public static boolean isAllClosed(FacilityRealmModel facility){
        boolean allClosed = true;
        if(facility != null){
            for(Service service : facility.getServices()){
                if(!service.isClosed()){
                    allClosed = false;
                    break;
                }
            }
        }

        return allClosed;
    }
}
