package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkFacilityResponse;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkRemoveRequest;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkSaveRequest;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchResponse;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;

@Singleton
public class BookmarkApiHelperImp implements BookmarkApiHelper{
    private ApiHeader mApiHeader;

    @Inject
    public BookmarkApiHelperImp(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public Flowable<BookmarkFacilityResponse> getBookmarkApiCall() {
        return Rx2AndroidNetworking.get(ApiEndPoint.ENDPOINT_GET_BOOKMARK)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .build()
                .getObjectFlowable(BookmarkFacilityResponse.class);
    }

    @Override
    public void saveBookmarkApiCall(BookmarkSaveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        Rx2AndroidNetworking.post(ApiEndPoint.ENDPOINT_BOOKMARK_SAVE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(parameter)
                .build()
                .getAsOkHttpResponse(listener);
    }

    @Override
    public void removeBookmarkApiCall(BookmarkRemoveRequest.PathParameter parameter, OkHttpResponseListener listener) {
        Rx2AndroidNetworking.delete(ApiEndPoint.ENDPOINT_BOOKMARK_REMOVE)
                .addHeaders(mApiHeader.getProtectedApiHeader())
                .addPathParameter(parameter)
                .build()
                .getAsOkHttpResponse(listener);
    }
}
