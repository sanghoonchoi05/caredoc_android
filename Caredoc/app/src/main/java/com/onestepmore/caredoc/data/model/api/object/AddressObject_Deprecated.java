package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public final class AddressObject_Deprecated {
    @Expose
    @SerializedName("b_code")
    private String bCode;
    @Expose
    @SerializedName("sido_code")
    private int sidoCode;
    @Expose
    @SerializedName("gugun_code")
    private int gugunCode;
    @Expose
    @SerializedName("dong_code")
    private int dongCode;
    @Expose
    @SerializedName("address_full_name")
    private String addressFullname;
    @Expose
    @SerializedName("address_name")
    private String addressName;

    @Expose
    @SerializedName("geo_location")
    private ApiCommonObject.GeoLocation geoLocation;

    public String getbCode() {
        return bCode;
    }

    public int getSidoCode() {
        return sidoCode;
    }

    public int getGugunCode() {
        return gugunCode;
    }

    public int getDongCode() {
        return dongCode;
    }

    public String getAddressFullname() {
        return addressFullname;
    }

    public String getAddressName() {
        return addressName;
    }

    public ApiCommonObject.GeoLocation getGeoLocation() {
        return geoLocation;
    }
}
