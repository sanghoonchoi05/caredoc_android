package com.onestepmore.caredoc.ui.account.terms;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface TermsNavigator extends BaseNavigator {
    void onTermsCheck(int position, boolean checked);
    void onTermsClick(int position);
    void onAgree();
}
