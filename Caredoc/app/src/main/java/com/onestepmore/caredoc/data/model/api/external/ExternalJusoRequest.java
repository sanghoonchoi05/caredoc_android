package com.onestepmore.caredoc.data.model.api.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExternalJusoRequest {
    public static class QueryParameter {
        @Expose
        @SerializedName("confmKey")
        private String confmKey;

        @Expose
        @SerializedName("currentPage")
        private int currentPage;

        @Expose
        @SerializedName("countPerPage")
        private int countPerPage;

        @Expose
        @SerializedName("keyword")
        private String keyword;

        @Expose
        @SerializedName("resultType")
        private String resultType;

        public String getConfmKey() {
            return confmKey;
        }

        public void setConfmKey(String confmKey) {
            this.confmKey = confmKey;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getCountPerPage() {
            return countPerPage;
        }

        public void setCountPerPage(int countPerPage) {
            this.countPerPage = countPerPage;
        }

        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }

        public String getResultType() {
            return resultType;
        }

        public void setResultType(String resultType) {
            this.resultType = resultType;
        }
    }
}
