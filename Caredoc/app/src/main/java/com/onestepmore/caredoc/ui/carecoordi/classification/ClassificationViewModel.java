package com.onestepmore.caredoc.ui.carecoordi.classification;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class ClassificationViewModel extends BaseViewModel<ClassificationNavigator> {
    public ClassificationViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }
}
