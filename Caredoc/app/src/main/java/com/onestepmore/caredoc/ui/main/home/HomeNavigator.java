package com.onestepmore.caredoc.ui.main.home;

import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface HomeNavigator extends BaseNavigator {
    void onCareCoordiButtonClick();
    void onRequestEstimate();
    void onSearch();
}
