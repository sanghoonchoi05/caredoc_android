package com.onestepmore.caredoc.ui.main.home;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.databinding.ICareFacilityHorizontalBinding;

import java.util.List;

public class CareFacilityNewAdapter extends RecyclerView.Adapter<CareFacilityNewAdapter.CareFacilityNewViewHolder> {
    private List< ? extends FacilitySearchList> mFacilityList = null;

    @Nullable
    private final HomeNavigator mHomeNavigator;
    private String mRatePointFormat;
    private String mReviewCountFormat;

    public CareFacilityNewAdapter(@Nullable HomeNavigator homeNavigator){
        mHomeNavigator = homeNavigator;
    }

    public void setFacilityList(final List< ? extends FacilitySearchList> facilityList){
        mFacilityList = facilityList;
        notifyItemRangeInserted(0, mFacilityList.size());
    }

    public void setRatePointFormat(String ratePointFormat) {
        this.mRatePointFormat = ratePointFormat;
    }

    public void setReviewCountFormat(String reviewCountFormat) {
        this.mReviewCountFormat = reviewCountFormat;
    }

    @NonNull
    @Override
    public CareFacilityNewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ICareFacilityHorizontalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_care_facility_horizontal, viewGroup, false);
        return new CareFacilityNewViewHolder(binding, mHomeNavigator);
    }

    @Override
    public void onBindViewHolder(@NonNull CareFacilityNewViewHolder facilityViewHolder, int position) {
        /*float ratePoint = Math.round(mFacilityList.get(position).rate*10)/10.f;
        String ratePointStr = String.format(mRatePointFormat, String.valueOf(ratePoint));
        String reviewCountStr = String.format(mReviewCountFormat, mFacilityList.get(position).reviewCount);
        facilityViewHolder.binding.iCareFacilityNewRatingBar.setRating(ratePoint);
        facilityViewHolder.binding.setRatePoint(ratePointStr);
        facilityViewHolder.binding.setReviewCount(reviewCountStr);
        facilityViewHolder.binding.setFacility(mFacilityList.get(position));*/
        facilityViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mFacilityList == null ? 0 : mFacilityList.size();
    }

    static class CareFacilityNewViewHolder extends RecyclerView.ViewHolder {
        final ICareFacilityHorizontalBinding binding;

        private CareFacilityNewViewHolder(ICareFacilityHorizontalBinding binding, HomeNavigator listener) {
            super(binding.getRoot());
            this.binding = binding;
            //binding.setCallback(listener);
        }
    }
}
