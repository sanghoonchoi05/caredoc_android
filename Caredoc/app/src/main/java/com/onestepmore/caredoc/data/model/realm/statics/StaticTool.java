package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.ToolObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@Deprecated
public class StaticTool extends RealmObject implements Parcelable {
    @PrimaryKey
    private String typeCode;
    private String categoryCode;
    private String typeName;
    private String modelName;
    private String manufacturer;
    private String usage;
    private String remark;

    public StaticTool(){}

    protected StaticTool(Parcel in) {
        typeCode = in.readString();
        categoryCode = in.readString();
        typeName = in.readString();
        modelName = in.readString();
        manufacturer = in.readString();
        usage = in.readString();
        remark = in.readString();
    }

    public void setStaticTool(ToolObject tool){
        this.typeCode = tool.getTypeCode();
        this.categoryCode = tool.getCategoryCode();
        this.typeName = tool.getTypeName();
        this.modelName = tool.getModelName();
        this.manufacturer = tool.getManufacturer();
        this.usage = tool.getUsage();
        this.remark = tool.getRemark();
    }

    public static final Creator<StaticTool> CREATOR = new Creator<StaticTool>() {
        @Override
        public StaticTool createFromParcel(Parcel in) {
            return new StaticTool(in);
        }

        @Override
        public StaticTool[] newArray(int size) {
            return new StaticTool[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeCode);
        dest.writeString(categoryCode);
        dest.writeString(typeName);
        dest.writeString(modelName);
        dest.writeString(manufacturer);
        dest.writeString(usage);
        dest.writeString(remark);
    }

    public String getTypeCode() {
        return typeCode;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getModelName() {
        return modelName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getUsage() {
        return usage;
    }

    public String getRemark() {
        return remark;
    }
}
