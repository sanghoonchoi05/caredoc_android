package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class ProgramsObject extends ItemResponse<ProgramsObject.Item> {
    public final class Item{
        @Expose
        @SerializedName("code")
        private String code;

        @Expose
        @SerializedName("name")
        private String name;

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
}
