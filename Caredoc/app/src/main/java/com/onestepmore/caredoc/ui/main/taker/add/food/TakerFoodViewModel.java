package com.onestepmore.caredoc.ui.main.taker.add.food;

import android.app.Application;
import android.databinding.ObservableField;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TakerFoodViewModel extends BaseViewModel<TakerFoodNavigator> {

    public TakerFoodViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }
}
