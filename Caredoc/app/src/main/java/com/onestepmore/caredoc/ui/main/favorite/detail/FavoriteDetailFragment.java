package com.onestepmore.caredoc.ui.main.favorite.detail;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Bookmark;
import com.onestepmore.caredoc.data.model.realm.BookmarkFacilityList;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.databinding.FFavoriteDetailBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;

public class FavoriteDetailFragment extends BaseFragment<FFavoriteDetailBinding, FavoriteDetailViewModel> implements FavoriteDetailNavigator {

    public static FavoriteDetailFragment newInstance(StaticCategory category) {

        Bundle args = new Bundle();

        FavoriteDetailFragment fragment = new FavoriteDetailFragment();
        args.putParcelable("category", category);
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    @Named("FavoriteDetailFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private FavoriteDetailViewModel mFavoriteDetailViewModel;
    private RealmResults<BookmarkFacilityList> mBookmarkFacilityList;
    private String mCategoryName;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_favorite_detail;
    }

    @Override
    public FavoriteDetailViewModel getViewModel() {
        mFavoriteDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(FavoriteDetailViewModel.class);
        return mFavoriteDetailViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFavoriteDetailViewModel.setNavigator(this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setSupportToolBar(getViewDataBinding().fFavoriteDetailToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_title);
        }

        Bundle bundle = getArguments();
        StaticCategory category = null;
        if(bundle != null){
            category = bundle.getParcelable("category");
            if(category != null){
                if(category.getQueryServiceTypeIds().size() == 0){
                    getViewDataBinding().setName(category.getText());
                }else{
                    getViewDataBinding().setName(String.format(getString(R.string.f_favorite_desc_5), category.getText()));
                }
                mCategoryName = category.getText();
            }
        }

        if(category == null){
            return;
        }

        // 시설 리스트 UI 구성
        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(),new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_facility));
        getViewDataBinding().fFavoriteDetailRecyclerView.addItemDecoration(dividerItemDecoration);


        RealmResults<Bookmark> bookmarkRealmResults = getViewModel().getRealm().where(Bookmark.class).equalTo("show", true).findAll();
        List<Bookmark> bookmarks = new ArrayList<>();

        // 전체
        if(category.getQueryServiceTypeIds().size() == 0){
            bookmarks.addAll(bookmarkRealmResults);
        }
        else {
            // 북마크에 저장된 저장된 서치카테고리 비교
            for (Bookmark bookmark : bookmarkRealmResults) {
                for (String searchText : bookmark.getSearchCategoryList()) {
                    if (category.getText().equals(searchText)) {
                        bookmarks.add(bookmark);
                        break;
                    }
                }
            }
        }

        int count = 0;
        if(bookmarks.size() > 0){
            RealmQuery<BookmarkFacilityList> query = getViewModel().getRealm().where(BookmarkFacilityList.class);
            for(int i=0; i<bookmarks.size(); i++){
                Bookmark bookmark = bookmarks.get(i);
                if(bookmark != null){
                    query = query.equalTo("fakeId", bookmark.getFakeId());
                }
                if(i < bookmarks.size() - 1){
                    query = query.or();
                }
            }
            mBookmarkFacilityList = query.findAll();
            mBookmarkFacilityList.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<BookmarkFacilityList>>() {
                @Override
                public void onChange(RealmResults<BookmarkFacilityList> bookmarkFacilityLists, OrderedCollectionChangeSet changeSet) {
                    onChangeBookmarkCount(bookmarkFacilityLists.size());
                }
            });

            FavoriteFacilityAdapter searchAdapter = new FavoriteFacilityAdapter(mBookmarkFacilityList);
            searchAdapter.setMainNavigator(((MainActivity)getBaseActivity()));
            getViewDataBinding().fFavoriteDetailRecyclerView.setAdapter(searchAdapter);
            getViewDataBinding().fFavoriteDetailRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

            count = mBookmarkFacilityList.size();
        }

        onChangeBookmarkCount(count);
    }

    private void onChangeBookmarkCount(int count){
        UIUtils.setTextIncludeAnnotation(
                getBaseActivity(),
                R.string.f_favorite_desc_1,
                getViewDataBinding().fFavoriteDetailDesc,
                String.valueOf(count));
        getViewDataBinding().setEmpty(count == 0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BackStackManager.getInstance().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * OnDestroy we unbind from the model and controller.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mBookmarkFacilityList!=null){
            mBookmarkFacilityList.removeAllChangeListeners();
        }
    }
}
