package com.onestepmore.caredoc.ui.main.search.map;

import android.app.Application;
import android.arch.lifecycle.MediatorLiveData;
import android.databinding.ObservableBoolean;

import com.google.gson.Gson;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapRequest;
import com.onestepmore.caredoc.data.model.api.object.FacilitySearchMapObject;
import com.onestepmore.caredoc.data.model.api.object.SearchCodeFilter;
import com.onestepmore.caredoc.data.model.api.object.SearchMapFilter;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchMapList;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import java.util.List;

import io.realm.RealmResults;

public class SearchMapViewModel extends BaseViewModel<SearchMapNavigator> {

    private final Gson mGson;
    private FacilityRepository mFacilityRepository;

    private final ObservableBoolean mTouchedMap = new ObservableBoolean(false);
    private MediatorLiveData<List<FacilitySearchMapObject>> mObservableClusterObj;
    private RealmResults<FacilitySearchMapList> mFacilitySearchMapList;

    public FacilityRepository getFacilityRepository() {
        return mFacilityRepository;
    }

    public RealmResults<FacilitySearchMapList> getFacilitySearchMapList() {
        return mFacilitySearchMapList;
    }

    public Gson getGson() {
        return mGson;
    }

    public MediatorLiveData<List<FacilitySearchMapObject>> getObservableClusterObj() {
        return mObservableClusterObj;
    }

    public SearchMapViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                              Gson gson, FacilityRepository facilityRepository) {
        super(application, appDataSource, schedulerProvider);

        mFacilityRepository = facilityRepository;
        mGson = gson;

        mObservableClusterObj = new MediatorLiveData<>();
        mObservableClusterObj.setValue(null);

        init();
    }

    private void init(){
        clearFacilityRealm();

        if(mFacilitySearchMapList == null){
            mFacilitySearchMapList = getFacilityRepository().getFacilitySearchMapList(true);
        }
    }

    void loadFacilitySearchClusterFromServer(SearchMapFilter filter){
        FacilitySearchMapRequest.QueryParameter queryParameter = new FacilitySearchMapRequest.QueryParameter();
        queryParameter.setFilter(getGson().toJson(filter));
        loadFacilitySearchClusterFromServer(queryParameter);
    }

    private void loadFacilitySearchClusterFromServer(FacilitySearchMapRequest.QueryParameter queryParameter){
        getCompositeDisposable().add(getFacilityRepository()
                .getFacilitySearchClusterApiCall(queryParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> mObservableClusterObj.setValue(response.getItems()),
                        throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    void loadFacilitySearchCodeFromServer(SearchCodeFilter filter){
        FacilitySearchMapRequest.QueryParameter queryParameter = new FacilitySearchMapRequest.QueryParameter();
        queryParameter.setFilter(getGson().toJson(filter));
        loadFacilitySearchCodeFromServer(queryParameter, filter.getCode());
    }

    private void loadFacilitySearchCodeFromServer(FacilitySearchMapRequest.QueryParameter queryParameter, String code){
        getCompositeDisposable().add(getFacilityRepository()
                .loadFacilitySearchCode(queryParameter, code)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {},
                        throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    public void onList(){
        getNavigator().onList();
    }

    @Override
    protected void onCleared() {
        unbindRealm();
        super.onCleared();
    }

    private void unbindRealm() {
        if(mFacilitySearchMapList!=null){
            mFacilitySearchMapList.removeAllChangeListeners();
        }

        clearFacilityRealm();
    }

    public void onMyLocation(){
        getNavigator().onMyLocation();
    }

    void clearFacilityRealm(){
        getFacilityRepository().clearFacilityRealm(FacilitySearchMapList.class);
    }

    List<FacilitySearchMapList> getFacilityList(boolean show){
        return getFacilityRepository().getFacilitySearchMapList(show);
    }

    List<FacilitySearchMapList> getFacilityList(String code){
        return getFacilityRepository().getFacilitySearchMapList(code);
    }
}
