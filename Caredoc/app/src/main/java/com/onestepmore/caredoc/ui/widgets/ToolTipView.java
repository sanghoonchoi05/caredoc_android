package com.onestepmore.caredoc.ui.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.ui.AppLogger;

public class ToolTipView extends RelativeLayout implements
        ViewTreeObserver.OnScrollChangedListener, AppBarLayout.OnOffsetChangedListener, View.OnClickListener
{
    public interface OnCloseListener{
        void onCloseTooltip();
    }

    private AppCompatTextView mTextView;
    private WindowManager.LayoutParams mLayoutParams;
    private WindowManager mWindowManager;
    private View mAnchorView;
    private OnCloseListener mCloseListener;
    private Rect mTmpDisplayFrame = new Rect();

    public void setCloseListener(OnCloseListener listener){
        mCloseListener = listener;
    }

    public ToolTipView(Context context) {
        super(context);
        init(null, 0);
    }

    public ToolTipView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ToolTipView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    public void init(AttributeSet attrs, int defStyle){
        View v = LayoutInflater.from(getContext()).inflate(R.layout.popup_tool_tip, this, false);
        addView(v);
        mTextView = v.findViewById(R.id.popup_tool_tip_text);
        v.findViewById(R.id.popup_tool_tip_root).setOnClickListener(this);

        mLayoutParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_PANEL,//항상 최 상위. 터치 이벤트 받을 수 있음.
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,  //포커스를 가지지 않음
                PixelFormat.TRANSLUCENT);
        mLayoutParams.gravity = Gravity.START | Gravity.TOP;
        mLayoutParams.windowAnimations = R.style.Tooltip;
    }

    public void setTooltipText(String text){
        mTextView.setText(text);
    }

    public void setAnchorView(View anchorView){
        this.mAnchorView = anchorView;
        mAnchorView.getViewTreeObserver().addOnScrollChangedListener(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    @Override
    public void onScrollChanged() {
        AppLogger.i(getClass(), "onScrollChanged");
        updatePosition();
        updateViewLayout();
    }

    private void updatePosition(){
        if(mAnchorView == null || getRootView() == null){
            return;
        }
        int[] tmpAnchorPos = new int[2];
        int[] tmpTooltipPos = new int[2];
        getRootView().getWindowVisibleDisplayFrame(mTmpDisplayFrame);
        getRootView().getLocationOnScreen(tmpTooltipPos);

        mAnchorView.getLocationOnScreen(tmpAnchorPos);
        final int spec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        getRootView().measure(spec, spec);

        float offset_y = getResources().getDimensionPixelOffset(R.dimen.custom_tooltip_top_margin);
        float offset_x = getResources().getDimensionPixelOffset(R.dimen.custom_tooltip_horizontal_margin);

        mLayoutParams.y = tmpAnchorPos[1] + mAnchorView.getHeight() + (int)offset_y;

        /*int below_y = tmpAnchorPos[1] + mAnchorView.getHeight() + (int)offset_y;
        if(tmpTooltipPos[1] != 0 && tmpAnchorPos[1] > tmpTooltipPos[1]){
            mLayoutParams.y = tmpAnchorPos[1] - (int)offset_y - getRootView().getMeasuredHeight();
        }else{
            mLayoutParams.y = tmpAnchorPos[1] + mAnchorView.getHeight() + (int)offset_y;
        }*/

        mLayoutParams.x = (int)offset_x;


        AppLogger.i(getClass(), "Tooltip y: " + mLayoutParams.y);
    }

    public void addViewWindow(WindowManager windowManager){
        if(windowManager == null){
            return;
        }

        updatePosition();
        mWindowManager = windowManager;
        mWindowManager.addView(getRootView(), mLayoutParams);
    }
    private void updateViewLayout(){
        if(mWindowManager == null){
            return;
        }
        mWindowManager.updateViewLayout(getRootView(), mLayoutParams);
    }

    public void removeViewWindow(){
        if(mWindowManager == null){
            return;
        }

        mWindowManager.removeViewImmediate(getRootView());

        handleListener();
        removeScrollListener();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        AppLogger.i(getClass(), "onOffsetChanged");
        updatePosition();
        updateViewLayout();
    }

    @Override
    public void onClick(View v) {
        removeViewWindow();
    }

    public void handleListener(){
        if(mCloseListener != null){
            mCloseListener.onCloseTooltip();
            mCloseListener = null;
        }
    }

    public void removeScrollListener(){
        if(mAnchorView != null){
            mAnchorView.getViewTreeObserver().removeOnScrollChangedListener(this);
        }
    }
}
