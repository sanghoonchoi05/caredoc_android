/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.ui;

import timber.log.Timber;

/**
 * Created by amitshekhar on 07/07/17.
 */

public final class AppLogger {

    private AppLogger() {
        // This utility class is not publicly instantiable
    }

    public static void setTag(String tag){
        Timber.tag(tag);
    }

    public static void d(String s, Object... objects) {
        Timber.d(s, objects);
    }

    public static void d(Class<?> c, String s, Object... objects) {
        Timber.tag(getTagWithClass(c)).d(s, objects);
    }

    public static void d(Throwable throwable, String s, Object... objects) {
        Timber.d(throwable, s, objects);
    }

    public static void e(String s, Object... objects) {
        Timber.e(s, objects);
    }

    public static void e(Throwable throwable, String s, Object... objects) {
        Timber.e(throwable, s, objects);
    }

    public static void e(Class<?> c, String s, Object... objects) {
        Timber.tag(getTagWithClass(c)).e(s, objects);
    }

    public static void e(Class<?> c, Throwable throwable, String s, Object... objects) {
        Timber.tag(getTagWithClass(c)).e(throwable, s, objects);
    }

    public static void i(Class<?> c, String s, Object... ojbects){
        Timber.tag(getTagWithClass(c)).i(s, ojbects);
    }

    public static void i(String s, Object... objects) {
        Timber.i(s, objects);
    }

    public static void i(Class<?> c, Throwable throwable, String s, Object... objects) {
        Timber.tag(getTagWithClass(c)).i(throwable, s, objects);
    }

    public static void i(Throwable throwable, String s, Object... objects) {
        Timber.i(throwable, s, objects);
    }

    public static void init() {
        //if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        //}
    }

    public static void w(String s, Object... objects) {
        Timber.w(s, objects);
    }

    public static void w(Throwable throwable, String s, Object... objects) {
        Timber.w(throwable, s, objects);
    }

    private static String getTagWithClass(Class<?> c){
        Class<?> enclosingClass = c.getEnclosingClass();
        String tag = c.getName();
        if(enclosingClass!=null){
            tag = enclosingClass.getName();
        }

        return tag;
    }
}
