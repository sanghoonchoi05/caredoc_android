package com.onestepmore.caredoc.ui.account.register.email;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface EmailRegisterNavigator extends BaseNavigator {
    void openMainActivity();
    void onRegister();
    void onRegisterFailed(boolean email, boolean name, boolean password, boolean passwordConfirm);
}
