package com.onestepmore.caredoc.ui.main.taker.add.care;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.ui.common.BirthClickListener;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TakerCareViewModel extends BaseViewModel<TakerCareNavigator> {

    private final ObservableBoolean hide = new ObservableBoolean(true);

    public TakerCareViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public ObservableBoolean getHide() {
        return hide;
    }
}
