package com.onestepmore.caredoc.data.model.api.taker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.object.TakerObject;

import java.util.List;

public class MyTakerResponse extends ItemResponse<TakerObject> {
}
