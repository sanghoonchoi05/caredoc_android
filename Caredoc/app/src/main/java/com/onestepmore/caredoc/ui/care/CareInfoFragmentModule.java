package com.onestepmore.caredoc.ui.care;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class CareInfoFragmentModule {
    @Provides
    CareInfoViewModel careInfoViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new CareInfoViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("CareInfoFragment")
    ViewModelProvider.Factory provideCareInfoViewModelProvider(CareInfoViewModel careInfoViewModel) {
        return new ViewModelProviderFactory<>(careInfoViewModel);
    }
}
