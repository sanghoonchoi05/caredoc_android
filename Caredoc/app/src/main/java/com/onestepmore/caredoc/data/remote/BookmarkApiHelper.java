package com.onestepmore.caredoc.data.remote;

import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkFacilityResponse;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkRemoveRequest;
import com.onestepmore.caredoc.data.model.api.bookmark.BookmarkSaveRequest;

import io.reactivex.Flowable;

public interface BookmarkApiHelper {
    Flowable<BookmarkFacilityResponse> getBookmarkApiCall();
    void saveBookmarkApiCall(BookmarkSaveRequest.PathParameter parameter, OkHttpResponseListener listener);
    void removeBookmarkApiCall(BookmarkRemoveRequest.PathParameter parameter, OkHttpResponseListener listener);
}
