package com.onestepmore.caredoc.ui.main.detail.photo;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class PhotoViewModel extends BaseViewModel<PhotoNavigator> {

    public PhotoViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClose(){
        getNavigator().onClose();
    }
}
