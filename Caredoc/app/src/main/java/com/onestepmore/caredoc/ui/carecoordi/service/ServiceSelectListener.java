package com.onestepmore.caredoc.ui.carecoordi.service;

import android.view.View;

import com.onestepmore.caredoc.data.model.realm.statics.StaticService;

public interface ServiceSelectListener {
    void onSelect(View view, StaticService service);
}
