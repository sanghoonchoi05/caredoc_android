package com.onestepmore.caredoc.ui.main.taker.card;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerCardFragmentProvider {
    @ContributesAndroidInjector(modules = TakerCardFragmentModule.class)
    abstract TakerCardFragment provideTakerCardFragmentFactory();
}
