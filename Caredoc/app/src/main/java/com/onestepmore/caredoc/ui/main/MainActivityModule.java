package com.onestepmore.caredoc.ui.main;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {
    @Provides
    MainViewModel mainViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                FacilityRepository facilityRepository) {
        return new MainViewModel(application, dataSource, schedulerProvider, facilityRepository);
    }

    @Provides
    ViewModelProvider.Factory provideMainViewModelProvider(MainViewModel mainViewModel) {
        return new ViewModelProviderFactory<>(mainViewModel);
    }
}
