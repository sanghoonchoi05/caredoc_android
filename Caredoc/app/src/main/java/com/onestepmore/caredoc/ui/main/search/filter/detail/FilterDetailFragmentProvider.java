package com.onestepmore.caredoc.ui.main.search.filter.detail;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FilterDetailFragmentProvider {
    @ContributesAndroidInjector(modules = FilterDetailFragmentModule.class)
    abstract FilterDetailFragment provideFilterDetailFragmentFactory();
}
