package com.onestepmore.caredoc.ui.account.login.forgot;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.User;
import com.onestepmore.caredoc.databinding.FLoginForgotBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.base.BaseFragment;

import javax.inject.Inject;
import javax.inject.Named;

public class LoginForgotFragment extends BaseFragment<FLoginForgotBinding, LoginForgotViewModel> implements LoginForgotNavigator {

    @Inject
    @Named("LoginForgotFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private LoginForgotViewModel mLoginForgotViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_login_forgot;
    }

    @Override
    public LoginForgotViewModel getViewModel() {
        mLoginForgotViewModel = ViewModelProviders.of(this, mViewModelFactory).get(LoginForgotViewModel.class);
        return mLoginForgotViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginForgotViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = getViewModel().getRealm().where(User.class).findFirst();
        if(user!=null){
            getViewDataBinding().fLoginEmailEdit.setText(user.getEmail());
        }

        getViewModel().setEditTextChangeObserver(getViewDataBinding().fLoginEmailEdit);
    }

    @Override
    public void onComplete() {
        showCommonDialog(getString(R.string.success_msg_send_password_email_title), getString(R.string.success_msg_send_password_email_desc));
    }

    @Override
    public void invalidEmail() {
        showCommonDialog(getString(R.string.warning_msg_send_password_email_title), getString(R.string.warning_msg_send_password_email_desc));
    }
}
