package com.onestepmore.caredoc.ui.main.detail.hospital;

import android.app.Application;
import android.databinding.BindingAdapter;
import android.databinding.ObservableField;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.OkHttpResponseListener;
import com.onestepmore.caredoc.data.model.api.object.CommentObject;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.CommentRemoveRequest;
import com.onestepmore.caredoc.data.model.realm.Review;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.Response;

public class HospitalDetailViewModel extends BaseViewModel<HospitalDetailNavigator> {
    private ReviewRepository mReviewRepository;

    public ReviewRepository getReviewRepository() {
        return mReviewRepository;
    }


    public ObservableField<String> getLunchWeekdays() {
        return lunchWeekdays;
    }

    public void setLunchWeekdays(String lunchWeekdays) {
        this.lunchWeekdays.set(lunchWeekdays);
    }

    public ObservableField<String> getLunchSat() {
        return lunchSat;
    }

    public void setLunchSat(String lunchSat) {
        this.lunchSat.set(lunchSat);
    }

    public ObservableField<String> getLunchSun() {
        return lunchSun;
    }

    public void setLunchSun(String lunchSun) {
        this.lunchSun.set(lunchSun);
    }

    public ObservableField<String> getReceptionWeekdays() {
        return receptionWeekdays;
    }

    public void setReceptionWeekdays(String receptionWeekdays) {
        this.receptionWeekdays.set(receptionWeekdays);
    }

    public ObservableField<String> getReceptionSat() {
        return receptionSat;
    }

    public void setReceptionSat(String receptionSat) {
        this.receptionSat.set(receptionSat);
    }

    public ObservableField<String> getReceptionSun() {
        return receptionSun;
    }

    public void setReceptionSun(String receptionSun) {
        this.receptionSun.set(receptionSun);
    }

    public ObservableField<String> getClosedAll() {
        return closedAll;
    }

    public void setClosedAll(String closedAll) {
        this.closedAll.set(closedAll);
    }

    public ObservableField<String> getClosedEtc() {
        return closedEtc;
    }

    public void setClosedEtc(String closedEtc) {
        this.closedEtc.set(closedEtc);
    }

    private ObservableField<String> lunchWeekdays = new ObservableField<>();
    private ObservableField<String> lunchSat = new ObservableField<>();
    private ObservableField<String> lunchSun = new ObservableField<>();
    private ObservableField<String> receptionWeekdays = new ObservableField<>();
    private ObservableField<String> receptionSat = new ObservableField<>();
    private ObservableField<String> receptionSun = new ObservableField<>();
    private ObservableField<String> closedAll = new ObservableField<>();
    private ObservableField<String> closedEtc = new ObservableField<>();


    private ObservableField<String> selfPer = new ObservableField<>();
    private ObservableField<String> needHelpPer = new ObservableField<>();
    private ObservableField<String> needFullHelpPer = new ObservableField<>();

    private ObservableField<String> newlyUlcersPer = new ObservableField<>();
    private ObservableField<String> reduceUlcers = new ObservableField<>();
    private ObservableField<String> worseUlcers = new ObservableField<>();

    private ObservableField<String> worseOutRoomPer = new ObservableField<>();
    private ObservableField<String> worseActivityPer = new ObservableField<>();


    public HospitalDetailViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                                   ReviewRepository reviewRepository) {
        super(application, appDataSource, schedulerProvider);

        mReviewRepository = reviewRepository;
    }


    public ObservableField<String> getSelfPer() {
        return selfPer;
    }

    public void setSelfPer(String selfPer) {
        this.selfPer.set(selfPer);
    }

    public ObservableField<String> getNeedHelpPer() {
        return needHelpPer;
    }

    public void setNeedHelpPer(String needHelpPer) {
        this.needHelpPer.set(needHelpPer);
    }

    public ObservableField<String> getNeedFullHelpPer() {
        return needFullHelpPer;
    }

    public void setNeedFullHelpPer(String needFullHelpPer) {
        this.needFullHelpPer.set(needFullHelpPer);
    }

    @BindingAdapter("textVisibility")
    public static void setTextVisibility(View view, String text){
        view.setVisibility(View.VISIBLE);
        if(text == null || text.isEmpty()){
            view.setVisibility(View.GONE);
        }
    }

    public ObservableField<String> getNewlyUlcersPer() {
        return newlyUlcersPer;
    }

    public void setNewlyUlcersPer(String newlyUlcersPer) {
        this.newlyUlcersPer.set(newlyUlcersPer);
    }

    public ObservableField<String> getReduceUlcers() {
        return reduceUlcers;
    }

    public void setReduceUlcers(String reduceUlcers) {
        this.reduceUlcers.set(reduceUlcers);
    }

    public ObservableField<String> getWorseUlcers() {
        return worseUlcers;
    }

    public void setWorseUlcers(String worseUlcers) {
        this.worseUlcers.set(worseUlcers);
    }

    public ObservableField<String> getWorseOutRoomPer() {
        return worseOutRoomPer;
    }

    public void setWorseOutRoomPer(String worseOutRoomPer) {
        this.worseOutRoomPer.set(worseOutRoomPer);
    }

    public ObservableField<String> getWorseActivityPer() {
        return worseActivityPer;
    }

    public void setWorseActivityPer(String worseActivityPer) {
        this.worseActivityPer.set(worseActivityPer);
    }

    void loadReviewServiceSummaryFromServer(long serviceFakeId) {
        //setIsLoading(true);

        ReviewFacilityRequest.PathParameter pathParameter = new ReviewFacilityRequest.PathParameter();
        pathParameter.setMorphId(serviceFakeId);
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility_service.getType());
        getCompositeDisposable().add(getReviewRepository()
                .getReviewServiceSummaryApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);

                    getNavigator().onLoadReviewServiceSummary(response);

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void loadCommentFacilityFromServer(long id){
        deleteReview();
        //setIsLoading(true);

        ReviewFacilityRequest.PathParameter pathParameter = new ReviewFacilityRequest.PathParameter();
        pathParameter.setMorphId(id);
        pathParameter.setMorphType(ReviewFacilityRequest.TYPE.facility.getType());
        getCompositeDisposable().add(getReviewRepository()
                .getCommentApiCall(pathParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(response -> {
                    setIsLoading(false);

                    saveReview(response.getItems());
                    getNavigator().onLoadCommentFacility(response);

                }, throwable -> {
                    setIsLoading(false);
                    getDataManager().handleThrowable(throwable, getNavigator());
                }));
    }

    public void removeComment(final long id, final long facilityId){
        CommentRemoveRequest.PathParameter pathParameter = new CommentRemoveRequest.PathParameter();
        pathParameter.setId(id);

        getReviewRepository().removeCommentApiCall(pathParameter, new OkHttpResponseListener() {
            @Override
            public void onResponse(Response response) {
                if (response.isSuccessful()) {
                    setIsLoading(false);
                    loadCommentFacilityFromServer(facilityId);
                } else {
                    setIsLoading(false);
                    getDataManager().handleApiError(response, getNavigator());
                }
            }

            @Override
            public void onError(ANError anError) {
                getDataManager().handleApiError(anError, getNavigator());
            }
        });
    }

    public void deleteReview(){
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Review> reviews = realm.where(Review.class).findAll();
                if(reviews != null){
                    while(reviews.size() > 0){
                        Review review = reviews.get(reviews.size()-1);
                        if(review != null){
                            if(review.getWriter() != null){
                                review.getWriter().deleteFromRealm();
                            }
                            review.deleteFromRealm();
                        }
                    }
                }
            }
        });
    }

    private void saveReview(List<CommentObject> commentObjects){
        if(commentObjects != null && commentObjects.size() > 0){
            List<Review> reviewList = new ArrayList<>();
            for(CommentObject commentObject : commentObjects){
                Review review = new Review();
                review.setReview(commentObject);
                reviewList.add(review);
            }

            getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for(Review review : reviewList){
                        realm.copyToRealmOrUpdate(review);
                    }
                }
            });
        }
    }

    public void onCall(View view){
        getNavigator().onShowPhoneDial();
    }

    public void onWrite(View view){
        getNavigator().onWrite();
    }

    public void onGoHomepage(View view){
        getNavigator().onGoHomepage();
    }
}
