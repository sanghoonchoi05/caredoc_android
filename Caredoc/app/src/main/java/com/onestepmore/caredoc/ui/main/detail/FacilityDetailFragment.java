package com.onestepmore.caredoc.ui.main.detail;

import android.Manifest;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.ChipGroup;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailEntranceResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailMedicalResponse;
import com.onestepmore.caredoc.data.model.api.object.ServiceEntranceObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceMedicalObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceObject;
import com.onestepmore.caredoc.data.model.realm.Attach;
import com.onestepmore.caredoc.data.model.realm.Bookmark;
import com.onestepmore.caredoc.data.model.realm.BookmarkFacilityList;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.FacilityRecentlyList;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.databinding.FFacilityDetailBinding;
import com.onestepmore.caredoc.databinding.IFacilityDetailPhotoBinding;
import com.onestepmore.caredoc.databinding.VFacilityDetailFavoriteMenuBinding;
import com.onestepmore.caredoc.databinding.VFacilityDetailServiceTabBinding;
import com.onestepmore.caredoc.databinding.VFacilityDetailShareMenuBinding;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.firebase.FirebaseParam;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.detail.base.BaseFacilityDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.comment.write.WriteCommentActivity;
import com.onestepmore.caredoc.ui.main.detail.homecare.HomecareDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.hospital.HospitalDetailFragment;
import com.onestepmore.caredoc.ui.main.detail.photo.PhotoActivity;
import com.onestepmore.caredoc.ui.main.detail.review.ReviewActivity;
import com.onestepmore.caredoc.ui.main.detail.review.write.WriteReviewActivity;
import com.onestepmore.caredoc.ui.widgets.StartPagerSnapHelper;
import com.onestepmore.caredoc.ui.widgets.ToolTipView;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.onestepmore.caredoc.AppConstants.DEEP_LINK_DOMAIN_URI_PREFIX;
import static com.onestepmore.caredoc.AppConstants.DEEP_LINK_FACILITY_DETAIL;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_AUTH_FOR_REVIEW;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_BACK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_BOOKMARK_REMOVE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_BOOKMARK_SAVE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_CAROUSEL_INDICATOR;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_HOMEPAGE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_MAP;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_SHARE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_SWITCH_SERVICE;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_TOOLTIP;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_VIEW_REVIEW;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_WRITE_COMMENT;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.FACILITY_DETAIL_CLICK_WRITE_REVIEW;
import static com.onestepmore.caredoc.ui.main.detail.FacilityActivity.EXTRA_FACILITY;
import static com.onestepmore.caredoc.ui.main.detail.FacilityActivity.EXTRA_SERVICE_ID;

public class FacilityDetailFragment extends BaseFragment<FFacilityDetailBinding, FacilityDetailViewModel> implements
        FacilityDetailNavigator,
        ChipGroup.OnCheckedChangeListener,
        TabLayout.BaseOnTabSelectedListener,
        ToolTipView.OnCloseListener
{
    private static final int REQUEST_CODE_WRITE_COMMENT = 1;
    private static final int REQUEST_CODE_PERMISSION_CALL = 2;
    private static final int REQUEST_CODE_SHOW_REVIEW = 3;
    private static final int REQUEST_CODE_WRITE_REVIEW = 4;
    private static final int REQUEST_CODE_PHOTO_FULL_SCREEN = 5;

    public static final int RESULT_CODE_WRITE_COMMENT = 1;
    public static final int RESULT_CODE_WRITE_REVIEW = 2;
    public static final int RESULT_CODE_REMOVE_REVIEW = 3;

    public static FacilityDetailFragment newInstance(FacilityRealmModel facility, int serviceId) {

        Bundle args = new Bundle();

        FacilityDetailFragment fragment = new FacilityDetailFragment();
        args.putParcelable(EXTRA_FACILITY, facility);
        args.putInt(EXTRA_SERVICE_ID, serviceId);
        fragment.setArguments(args);
        return fragment;
    }
    public static FacilityDetailFragment newInstance(long fakeId, String key) {

        Bundle args = new Bundle();

        FacilityDetailFragment fragment = new FacilityDetailFragment();
        args.putLong(key, fakeId);
        fragment.setArguments(args);
        return fragment;
    }

    private FacilityRealmModel mFacility;
    private int mTouchServiceId = 0;
    private RealmResults<Bookmark> mBookmark;

    @Inject
    @Named("FacilityDetailFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private FacilityDetailViewModel mFacilityDetailViewModel;
    VFacilityDetailFavoriteMenuBinding mFavoriteMenuBinding;

    private ToolTipView mToolTipView;
    private WindowManager mWindowManager;

    private int mPhotoPosition = 0;
    private int mPhotoListCount = 0;

    private boolean isHospital = false;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_facility_detail;
    }

    @Override
    public FacilityDetailViewModel getViewModel() {
        mFacilityDetailViewModel = ViewModelProviders.of(this, mViewModelFactory).get(FacilityDetailViewModel.class);
        return mFacilityDetailViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFacilityDetailViewModel.setNavigator(this);

        mWindowManager = (WindowManager) getBaseActivity().getSystemService(Context.WINDOW_SERVICE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.facility_detail, menu);

        mFavoriteMenuBinding =  DataBindingUtil.bind(menu.findItem(R.id.facility_detail_favorite).getActionView());
        changeBookmark(checkBookmark());
        mFavoriteMenuBinding.vFacilityDetailFavoriteMenuLayout.setOnClickListener((v) -> onFavorite());

        VFacilityDetailShareMenuBinding binding = DataBindingUtil.bind(menu.findItem(R.id.facility_detail_share).getActionView());
        if(binding != null){
            binding.vFacilityDetailShareMenuLayout.setOnClickListener((v) -> onShare());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_BACK);
                onClose();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UIUtils.setStatusBarTranslucent(getBaseActivity(), getView(), true);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mFacility = bundle.getParcelable(EXTRA_FACILITY);
            mTouchServiceId = bundle.getInt(EXTRA_SERVICE_ID);
        }

        if(mFacility == null){
            return;
        }

        setHasOptionsMenu(true);

        setSupportToolBar(getViewDataBinding().fFacilityDetailToolbar);
        ActionBar actionBar = getBaseActivity().getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setHomeAsUpIndicator(R.drawable.btn_back_white_title);
        }

        setFacilityData();
    }

    public void setFacilityData(){
        saveFacilityRecently();

        getViewDataBinding().setFacility(mFacility);

        setAttaches(mFacility.getAttaches());

        observerBookmark();

        if(mFacility.getServices().size() > 0){
            if(CommonUtils.isAllClosed(mFacility)){
                getViewDataBinding().setIsClosed(true);
            }
            else{
                getViewDataBinding().setIsClosed(false);
                Service service = mFacility.getServices().get(0);
                if(service != null){
                    if(service.getServiceCategory().equals(ServiceObject.CATEGORY.GERIATRIC_HOSPITAL.getName())){
                        isHospital = true;
                        getViewModel().loadFacilityHospitalDetailFromServer(mFacility.getFakeId());
                    }
                    else{
                        isHospital = false;
                        getViewModel().loadFacilityHomecareDetailFromServer(mFacility.getFakeId());
                    }
                }
            }
        }
    }

    @Override
    public void onLoadHospitalDetail(FacilityDetailMedicalResponse response){
        initServicePager(getFacilityDetailFragment(response));
    }

    @Override
    public void onLoadHomeCareDetail(FacilityDetailEntranceResponse response){
        initServicePager(getFacilityDetailFragment(response));
    }

    @Override
    public void onPrevPhoto() {
        if(mPhotoPosition == 0){
            return;
        }

        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_CAROUSEL_INDICATOR, AmplitudeParam.Key.VALUE, AmplitudeParam.DIRECTION.LEFT.toString());

        mPhotoPosition--;
        getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoRecyclerView.smoothScrollToPosition(mPhotoPosition);
    }

    @Override
    public void onNextPhoto() {
        if(mPhotoPosition == mPhotoListCount - 1){
            return;
        }

        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_CAROUSEL_INDICATOR, AmplitudeParam.Key.VALUE, AmplitudeParam.DIRECTION.RIGHT.toString());

        mPhotoPosition++;
        getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoRecyclerView.smoothScrollToPosition(mPhotoPosition);
    }

    private List<BaseFacilityDetailFragment> getFacilityDetailFragment(FacilityDetailMedicalResponse response){
        List<BaseFacilityDetailFragment> baseFacilityDetailFragmentList = new ArrayList<>();
        for (ServiceMedicalObject serviceMedicalObject : response.getServices().getItems()) {
            HospitalDetailFragment fragment = new HospitalDetailFragment();
            fragment.setServiceDetailObject(serviceMedicalObject);
            fragment.setFacilityDetailObject(response);
            fragment.setFacilityDetailNavigator(this);
            baseFacilityDetailFragmentList.add(fragment);
        }

        return baseFacilityDetailFragmentList;
    }

    private List<BaseFacilityDetailFragment> getFacilityDetailFragment(FacilityDetailEntranceResponse response){
        List<BaseFacilityDetailFragment> baseFacilityDetailFragmentList = new ArrayList<>();
        for (ServiceEntranceObject serviceEntranceObject : response.getServices().getItems()) {
            HomecareDetailFragment fragment = new HomecareDetailFragment();
            fragment.setServiceDetailObject(serviceEntranceObject);
            fragment.setFacilityDetailObject(response);
            fragment.setFacilityDetailNavigator(this);
            baseFacilityDetailFragmentList.add(fragment);
        }

        return baseFacilityDetailFragmentList;
    }

    private void initServicePager(List<BaseFacilityDetailFragment> baseFacilityDetailFragmentList){
        FacilityDetailPagerAdapter adapter = new FacilityDetailPagerAdapter(getBaseActivity().getSupportFragmentManager());
        TabLayout tabLayout = getViewDataBinding().fFacilityDetailTab;
        int index = 0;
        int position = 0;
        for(BaseFacilityDetailFragment baseFacilityDetailFragment : baseFacilityDetailFragmentList){
            if(mTouchServiceId > 0 && baseFacilityDetailFragment.getServiceDetailObject().getServiceId() == mTouchServiceId){
                position = index;
            }
            adapter.addFacilityDetailFragment(baseFacilityDetailFragment, this);
            index++;
        }

        getViewDataBinding().fFacilityDetailPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(getViewDataBinding().fFacilityDetailPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i, getBaseActivity()));
        }

        getViewDataBinding().fFacilityDetailPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(this);

        getViewDataBinding().fFacilityDetailPager.setCurrentItem(position);
        //setTabSelected(position);
    }

    private void setAttaches(List<Attach> attaches){
        if(getViewDataBinding() == null || attaches == null){
            return;
        }

        PhotoAdapter adapter = new PhotoAdapter();
        getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManagerCenter = new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.HORIZONTAL, false);
        getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoRecyclerView.setLayoutManager(layoutManagerCenter);

        if(attaches.size()==0){
            getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoIndicatorLayout.setVisibility(View.GONE);
        }else{
            String [] urls = new String[attaches.size()];
            for(int i=0; i<urls.length; i++){
                urls[i] = attaches.get(i).getUrl();
            }

            mPhotoListCount = urls.length;
            setPhotoIndicatorText();
            adapter.setImgList(urls);
        }

        SnapHelper snapHelperCenter = new StartPagerSnapHelper(0);
        snapHelperCenter.attachToRecyclerView(getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoRecyclerView);

        getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                int snapPosition = RecyclerView.NO_POSITION;
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if(layoutManager!=null){
                    View snapView = snapHelperCenter.findSnapView(layoutManager);
                    if(snapView!=null){
                        snapPosition = layoutManager.getPosition(snapView);
                        onChangeSnapPosition(snapPosition);
                    }
                }

                AppLogger.i(getClass(), "Scrolled x: " + dx);
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void setPhotoIndicatorText(){
        getViewDataBinding().fFacilityDetailTop.vFacilityDetailPhotoIndicatorText.setText(String.format(getString(R.string.common_slash_num2), mPhotoPosition+1, mPhotoListCount));
    }

    private void onChangeSnapPosition(int snapPosition){
        if(mPhotoPosition == snapPosition){
            return;
        }
        mPhotoPosition = snapPosition;
        setPhotoIndicatorText();
    }

    @Override
    public void onChangeBookmark(){
        boolean isBookmarked = false;
        if(!checkBookmark()) {
            AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_BOOKMARK_SAVE);
            FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.FACILITY_CLICK_BOOKMARK_SAVE);
            isBookmarked = true;
            showToastSave();
        }
        else{
            AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_BOOKMARK_REMOVE);
        }

        changeBookmark(isBookmarked);
    }

    private boolean checkBookmark(){
        if(mFacility == null){
            return false;
        }
        return getViewModel().getRealm().where(Bookmark.class)
                .equalTo("fakeId", mFacility.getFakeId())
                .findFirst() != null;
    }


    private void changeBookmark(boolean isBookmarked){
        if(mFavoriteMenuBinding == null){
            return;
        }

        if(isBookmarked){
            mFavoriteMenuBinding.setIcon(ActivityCompat.getDrawable(getBaseActivity(), R.drawable.ico_detail_heart_on));
        }
        else{
            mFavoriteMenuBinding.setIcon(ActivityCompat.getDrawable(getBaseActivity(), R.drawable.ico_detail_heart_off));
        }
    }

    private void observerBookmark(){
        if(mFacility != null){
            mBookmark = getViewModel().getRealm().where(Bookmark.class).equalTo("fakeId", mFacility.getFakeId()).findAll();
            mBookmark.addChangeListener(new RealmChangeListener<RealmResults<Bookmark>>() {
                @Override
                public void onChange(@NonNull RealmResults<Bookmark> bookmarks) {
                    changeBookmark(bookmarks.size() > 0);
                }
            });

            changeBookmark(mBookmark.size() > 0);
        }
    }

    private void saveFacilityRecently(){
        if(mFacility != null){
            getViewModel().getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<FacilityRecentlyList> recentlyListRealmResults = getViewModel().getRealm().where(FacilityRecentlyList.class).sort("date",Sort.DESCENDING).findAll();
                    FacilityRecentlyList recently = getViewModel().getRealm().where(FacilityRecentlyList.class).equalTo("fakeId", mFacility.getFakeId()).findFirst();
                    if(recently != null){
                        //recently.deleteRealm(recently);
                        RealmObject.deleteFromRealm(recently);
                    }else if(recentlyListRealmResults != null && recentlyListRealmResults.size() >= 15){
                        FacilityRecentlyList oldFacility = recentlyListRealmResults.get(recentlyListRealmResults.size()-1);
                        RealmObject.deleteFromRealm(oldFacility);
                    }

                    FacilityRecentlyList facility = new FacilityRecentlyList();
                    facility.setFacility(mFacility);
                    realm.copyToRealmOrUpdate(facility);
                }
            });
        }
    }


    @Override
    public void onCheckedChanged(ChipGroup chipGroup, int checkedId) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        setTabSelected(tab.getPosition());

        if(getViewDataBinding().fFacilityDetailPager.getCurrentItem() == tab.getPosition()){
            return;
        }

        if(mFacility.getServices().size() > 0){
            Service service = mFacility.getServices().get(tab.getPosition());
            if(service != null){
                AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_SWITCH_SERVICE, AmplitudeParam.Key.VALUE, service.getServiceName());
            }
        }

        getViewDataBinding().fFacilityDetailPager.setCurrentItem(tab.getPosition(), true);
        removeTooltip();
    }

    private void setTabSelected(int position){
        for(int i=0; i<getViewDataBinding().fFacilityDetailTab.getTabCount(); i++){
            TabLayout.Tab tab = getViewDataBinding().fFacilityDetailTab.getTabAt(i);
            if(tab == null || tab.getCustomView() == null){
                continue;
            }

            View view = tab.getCustomView();
            VFacilityDetailServiceTabBinding binding = DataBindingUtil.getBinding(view);
            if(binding == null){
                continue;
            }

            boolean isClosed = false;
            Service service = mFacility.getServices().size() > i ? mFacility.getServices().get(i) : null;
            if(service != null){
                isClosed = service.isClosed();
            }

            boolean isSelect = false;
            Typeface typeface = ResourcesCompat.getFont(getBaseActivity(), R.font.noto_sans_kr_demi_light_hestia);
            Drawable drawable = CommonUtils.getOvalDrawableWithGrade(getBaseActivity(), null, isClosed);
            if(position == i){
                isSelect = true;
                typeface = ResourcesCompat.getFont(getBaseActivity(), R.font.noto_sans_kr_bold_hestia);
                drawable = CommonUtils.getOvalDrawableWithGrade(getBaseActivity(), binding.getGrade(), isClosed);
            }

            binding.vFacilityDetailServiceTabTitle.setSelected(isSelect);
            binding.vFacilityDetailServiceTabTitle.setTypeface(typeface);
            binding.setGradeIcon(drawable);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClose() {
        BackStackManager.getInstance().onBackPressed();
    }

    @Override
    public void onShowPhoneDial() {
        if(mFacility == null){
            return;
        }

        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.FACILITY_CLICK_CALL);
        int permissionCheck = ActivityCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.CALL_PHONE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(
                    new String[]{Manifest.permission.CALL_PHONE},
                    REQUEST_CODE_PERMISSION_CALL);
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            callIntent.setData(Uri.parse("tel:"+mFacility.getContact()));
            getBaseActivity().startActivity(callIntent);
        }
    }

    @Override
    public void onShowHomepage() {
        if(mFacility == null || CommonUtils.checkNullAndEmpty(mFacility.getHomepage())){
            return;
        }

        String homepage = mFacility.getHomepage();
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_HOMEPAGE);
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.FACILITY_CLICK_HOMEPAGE);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        if(!homepage.startsWith("http")){
            homepage = "http://" + homepage;
        }
        browserIntent.setData(Uri.parse(homepage));
        try{
            getBaseActivity().startActivity(browserIntent);
        }catch (ActivityNotFoundException e){

        }
    }

    @Override
    public void onShare() {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_SHARE);
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.FACILITY_CLICK_SHARE);
        FirebaseManager.getInstance().logEvent(
                FirebaseEvent.PredefinedEvent.SHARE,
                new FirebaseManager.Builder()
                        .addParam(FirebaseParam.PredefinedKey.CONTENT_TYPE, FirebaseParam.Value.FACILITY)
                        .addParam(FirebaseParam.PredefinedKey.ITEM_ID, mFacility.getFakeId()));
        createDynamicLink();
    }

    public void createDynamicLink(){
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(getFacilityDetailDeepLink())
                .setDomainUriPrefix(DEEP_LINK_DOMAIN_URI_PREFIX)
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder().build())
                .buildShortDynamicLink()
                .addOnCompleteListener(getBaseActivity(), new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            try {
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, shortLink.toString());
                                sendIntent.setType("text/plain");
                                startActivity(Intent.createChooser(sendIntent, "Share"));
                            } catch (ActivityNotFoundException ignored) {
                            }
                        } else {
                            // Error
                            // ...
                        }
                    }
                });
    }

    private Uri getFacilityDetailDeepLink() {
        return Uri.parse(DEEP_LINK_FACILITY_DETAIL + mFacility.getFakeId().toString() + "/" + mFacility.getName());
    }

    @Override
    public void onSaveFailedForGuest() {
        showCommonDialog(getString(R.string.warning_msg_favorite_failed),getString(R.string.warning_msg_favorite_failed_guest));
    }

    @Override
    public void onWriteComment() {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_WRITE_COMMENT);
        if(!getViewModel().getDataManager().isLoggedIn()){
            showNeedLoginPopup();
            return;
        }

        Intent intent = getBaseActivity().newIntent(WriteCommentActivity.class);
        intent.putExtra("facilityId", mFacility.getFakeId());
        //TODO - 수정하기 가능한 시점에 오픈
        /*RealmResults<Review> reviews = getViewModel().getRealm().where(Review.class).equalTo("isOwn", true).findAll();
        if(reviews.size() > 0){
            Review review = reviews.get(0);
            if(review != null){
                intent.putExtra("commentId", review.getServiceId());
            }
        }*/
        startActivityForResult(intent, REQUEST_CODE_WRITE_COMMENT);
    }

    public void gotoAccountMainActivityForReview(View view){
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_AUTH_FOR_REVIEW);

        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.NEED_LOGIN_POPUP_CLICK_START);
        Intent intent = getBaseActivity().newIntent(AccountMainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRemoveComment(long id) {

    }

    @Override
    public void onShowTooltip(View anchorView, String title, String text) {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_TOOLTIP, AmplitudeParam.Key.VALUE, title);

        removeTooltip();

        mToolTipView = new ToolTipView(getBaseActivity());
        mToolTipView.setAnchorView(anchorView);
        mToolTipView.addViewWindow(mWindowManager);
        mToolTipView.setTooltipText(text);
        mToolTipView.setCloseListener(this);
        getViewDataBinding().fFacilityDetailAppBar.addOnOffsetChangedListener(mToolTipView);
    }

    @Override
    public void onShowReview(long serviceFakeId) {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_VIEW_REVIEW);

        Intent intent = getBaseActivity().newIntent(ReviewActivity.class);
        intent.putExtra("serviceFakeId", serviceFakeId);
        startActivityForResult(intent, REQUEST_CODE_SHOW_REVIEW);
    }

    @Override
    public void onWriteReview(long serviceFakeId, int serviceId) {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_WRITE_REVIEW);
        if(!getViewModel().getDataManager().isLoggedIn()){
            showNeedLoginPopup(this::gotoAccountMainActivityForReview);
            return;
        }

        Intent intent = getBaseActivity().newIntent(WriteReviewActivity.class);
        intent.putExtra("serviceFakeId", serviceFakeId);
        intent.putExtra("serviceId", serviceId);
        startActivityForResult(intent, REQUEST_CODE_WRITE_REVIEW);
    }

    @Override
    public void onRemoveReview(long serviceFakeId) {

    }

    @Override
    public void onShowGoogleMap() {
        AmplitudeManager.getInstance().logEvent(FACILITY_DETAIL_CLICK_MAP);

        if(mFacility.getLatLng() == null){
            return;
        }

        String uri = "http://maps.google.com/maps?q=loc:" + mFacility.getLatLng().getLatitude() + "," + mFacility.getLatLng().getLongitude();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        try{
            getBaseActivity().startActivity(intent);
        }catch (ActivityNotFoundException ex){
            Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            getBaseActivity().startActivity(unrestrictedIntent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CODE_PERMISSION_CALL){
            if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                onShowPhoneDial();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_CODE_WRITE_COMMENT ||
                resultCode == RESULT_CODE_WRITE_REVIEW ||
                resultCode == RESULT_CODE_REMOVE_REVIEW){
            FacilityDetailPagerAdapter adapter = (FacilityDetailPagerAdapter) getViewDataBinding().fFacilityDetailPager.getAdapter();
            if(adapter != null){
                for(int i=0; i<adapter.getCount(); i++){
                    BaseFacilityDetailFragment fragment = (BaseFacilityDetailFragment)adapter.getItem(i);
                    if(!fragment.isAdded()){
                        continue;
                    }

                    if(resultCode == RESULT_CODE_WRITE_COMMENT){
                        fragment.refreshComment();
                    }
                    else{
                        fragment.refreshReview();
                    }
                }
            }
        }
    }

    private void showToastSave(){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.v_facility_save, null, false);

        Toast toast = new Toast(getBaseActivity());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();

    }

    public void onFavorite() {
        if (mFacility == null) {
            return;
        }

        BookmarkFacilityList bookmark = getViewModel().getRealm().where(BookmarkFacilityList.class).equalTo("fakeId", mFacility.getFakeId()).findFirst();
        if (bookmark != null) {
            getViewModel().onRemoveFacility(mFacility.getFakeId());
        } else {
            getViewModel().onSaveFacility(mFacility.getFakeId());
        }
    }

    @Override
    public void onDetach() {
        if (mToolTipView != null) {
            mToolTipView.removeViewWindow();
        }

        super.onDetach();
    }

    /**
     * OnDestroy we unbind from the model and controller.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mBookmark!=null){
            mBookmark.removeAllChangeListeners();
        }
    }

    @Override
    public void onCloseTooltip() {
        if(mToolTipView!=null){
            getViewDataBinding().fFacilityDetailAppBar.removeOnOffsetChangedListener(mToolTipView);
            mToolTipView = null;
        }
    }

    private void removeTooltip(){
        if (mToolTipView != null) {
            mToolTipView.removeViewWindow();
        }
    }

    private void onShowPhotoFullScreen(){
        Intent intent = getBaseActivity().newIntent(PhotoActivity.class);
        intent.putExtra(PhotoActivity.EXTRA_FACILITY, mFacility);
        startActivityForResult(intent, REQUEST_CODE_PHOTO_FULL_SCREEN);
    }

    public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

        @Nullable
        private String [] mImgUrls;

        public void setImgList(final String [] imgUrls){
            mImgUrls = imgUrls;
            notifyItemRangeInserted(0, mImgUrls.length);
        }

        @NonNull
        @Override
        public PhotoAdapter.PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            IFacilityDetailPhotoBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_detail_photo, viewGroup, false);
            return new PhotoAdapter.PhotoViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull PhotoAdapter.PhotoViewHolder facilityViewHolder, int position) {
            if(mImgUrls != null){
                Glide.with(facilityViewHolder.itemView.getContext()).load(mImgUrls[position]).into(facilityViewHolder.binding.iFacilityDetailPhoto);
                facilityViewHolder.binding.setImgUrl(mImgUrls[position]);
                facilityViewHolder.binding.executePendingBindings();
            }
        }

        @Override
        public int getItemCount() {
            return mImgUrls == null ? 0 : mImgUrls.length;
        }

        class PhotoViewHolder extends RecyclerView.ViewHolder {
            final IFacilityDetailPhotoBinding binding;

            private PhotoViewHolder(IFacilityDetailPhotoBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
                this.binding.iFacilityDetailPhoto.setOnClickListener((view) -> onShowPhotoFullScreen());
            }
        }
    }
}
