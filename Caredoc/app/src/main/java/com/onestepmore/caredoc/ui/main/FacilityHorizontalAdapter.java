package com.onestepmore.caredoc.ui.main;

import android.content.Context;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.chip.ChipGroup;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.databinding.IFacilityHorizontalBinding;
import com.onestepmore.caredoc.databinding.VFacilityServiceChipGroupItemBinding;
import com.onestepmore.caredoc.utils.CommonUtils;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public abstract class FacilityHorizontalAdapter<T extends FacilityRealmModel, S extends FacilityHorizontalAdapter.FacilityHorizontalViewHolder>
        extends RealmRecyclerViewAdapter<T, S>{

    @NonNull
    private MainNavigator mMainNavigator;
    private MainNavigator.FROM mFrom;

    public FacilityHorizontalAdapter(@NonNull OrderedRealmCollection<T> data, MainNavigator.FROM from) {
        super(data, true, false);
        mFrom = from;
    }

    public void setMainNavigator(@NonNull MainNavigator mainNavigator) {
        mMainNavigator = mainNavigator;
    }


    @Override
    public void onBindViewHolder(@NonNull S s, int position) {
        OrderedRealmCollection data = getData();
        if (data != null) {
            if(s.binding instanceof IFacilityHorizontalBinding){
                final FacilityRealmModel facility = getData().get(position);
                IFacilityHorizontalBinding binding = (IFacilityHorizontalBinding)s.binding;
                binding.setFacility(facility);
                binding.setFrom(mFrom);
                binding.iFacilityHorizontalImg.setIsClosed(CommonUtils.isAllClosed(facility));
                initChipGroup(binding.iFacilityHorizontalChipGroup, facility, binding.getRoot().getContext());
                binding.setCallback(mMainNavigator);
                binding.executePendingBindings();
            }
        }
    }

    private ColorStateList getGradeColorStateList(String gradeStr, Context context) {
        return CommonUtils.getColorStateListWithGrade(context, gradeStr);
    }

    private void initChipGroup(ChipGroup chipGroup, FacilityRealmModel facility, Context context) {
        chipGroup.removeAllViews();
        for (int i = 0; i < facility.getServices().size(); i++) {
            Service service = facility.getServices().get(i);
            if (service != null) {
                VFacilityServiceChipGroupItemBinding binding = DataBindingUtil.inflate
                        (LayoutInflater.from(chipGroup.getContext()), R.layout.v_facility_service_chip_group_item, chipGroup, false);
                String text = service.getServiceName();
                String grade = service.getLatestGrade();
                ColorStateList bgColor = getGradeColorStateList(grade, context);
                ColorStateList strokeColor = bgColor;
                ColorStateList textColor = context.getResources().getColorStateList(R.color.white);
                binding.vFacilityServiceChip.setPaintFlags(0);
                if(service.isClosed()){
                    bgColor = context.getResources().getColorStateList(R.color.colorClosedBg);
                    strokeColor = bgColor;
                    textColor = context.getResources().getColorStateList(R.color.colorClosedText);
                    binding.vFacilityServiceChip.setPaintFlags(binding.vFacilityServiceChip.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
                }
                binding.vFacilityServiceChip.setChipBackgroundColor(bgColor);
                binding.vFacilityServiceChip.setChipStrokeColor(strokeColor);
                binding.vFacilityServiceChip.setTextColor(textColor);
                binding.vFacilityServiceChip.setOnClickListener(view -> mMainNavigator.onFacilityDetailClick(facility, mFrom, service));

                if (grade != null) {
                    text = grade + " " + text;
                }

                binding.vFacilityServiceChip.setText(text);
                chipGroup.addView(binding.vFacilityServiceChip);
            }
        }
    }

    public class FacilityHorizontalViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        public FacilityHorizontalViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
