package com.onestepmore.caredoc.ui.carecoordi;

import java.util.List;

public class GuideData extends StepData{
    private List<ExampleData> exampleDataList;

    public List<ExampleData> getExampleDataList() {
        return exampleDataList;
    }

    public void setExampleDataList(List<ExampleData> exampleDataList) {
        this.exampleDataList = exampleDataList;
    }
}
