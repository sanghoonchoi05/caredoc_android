/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc.ui.base;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableBoolean;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.realm.Realm;
import rx.Observable;
import rx.Subscription;

public abstract class BaseViewModel<N> extends AndroidViewModel {

    private final AppDataSource mAppDataSource;

    private final ObservableBoolean mIsLoading = new ObservableBoolean(false);

    private final SchedulerProvider mSchedulerProvider;

    private CompositeDisposable mCompositeDisposable;

    private WeakReference<N> mNavigator;

    private List<Subscription> mSubscriptionList = new ArrayList<>();

    private Realm mRealm;

    public BaseViewModel(Application application, AppDataSource appDataSource,
                         SchedulerProvider schedulerProvider) {
        super(application);
        this.mAppDataSource = appDataSource;
        this.mSchedulerProvider = schedulerProvider;
        this.mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        unsubscribe();
        mCompositeDisposable.dispose();
        if(mRealm != null){
            mRealm.close();
        }
        super.onCleared();
    }

    private void unsubscribe(){
        if(mSubscriptionList.size() > 0){
            for(Subscription subscription: mSubscriptionList){
                if(!subscription.isUnsubscribed()){
                    subscription.unsubscribe();
                }
            }
            mSubscriptionList.clear();
        }
    }

    public void setLoadingObserver(Observable<Boolean> observer){
        mSubscriptionList.add(observer.subscribe(this::changedLoading));
    }

    private void changedLoading(Boolean changed){
        mIsLoading.set(changed);
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public AppDataSource getDataManager() {
        return mAppDataSource;
    }

    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.set(isLoading);
    }

    public N getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public void addSubscription(Subscription subscription){
        this.mSubscriptionList.add(subscription);
    }

    public Realm getRealm() {
        if(mRealm == null){
            mRealm = Realm.getDefaultInstance();
        }
        return mRealm;
    }
}
