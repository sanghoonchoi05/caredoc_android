package com.onestepmore.caredoc.ui.carecoordi.guide;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.FGuideBinding;
import com.onestepmore.caredoc.databinding.IGuideBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.carecoordi.BaseCoordiFragment;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity;
import com.onestepmore.caredoc.ui.carecoordi.ExampleData;
import com.onestepmore.caredoc.ui.carecoordi.GuideData;
import com.onestepmore.caredoc.ui.carecoordi.StepData;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

public class GuideFragment extends BaseCoordiFragment<FGuideBinding, GuideViewModel> implements
        GuideNavigator
{
    public interface ExampleClickListener{
        void onExampleClick(View view, ExampleData example);
    }

    @Inject
    @Named("GuideFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private GuideViewModel mGuideViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_guide;
    }

    @Override
    public GuideViewModel getViewModel() {
        mGuideViewModel = ViewModelProviders.of(this, mViewModelFactory).get(GuideViewModel.class);
        return mGuideViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGuideViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int spacingInPixels = getBaseActivity().getResources().getDimensionPixelSize(R.dimen.care_coordi_example_list_space);
        getViewDataBinding().fGuideRecyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels, 0));
        getViewDataBinding().fGuideRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        StepData stepData =((CareCoordiActivity)getBaseActivity()).getSelectedStepDataList().getFirst();
        if(stepData != null && stepData.getStep() == CareCoordiActivity.STEP.NEED_HELP){
            setStepData(stepData);
        }
    }

    @Override
    public void setTitle(int titleResId){
        UIUtils.setTextIncludeAnnotation(getBaseActivity(), titleResId, getViewDataBinding().fGuideTitle);
    }

    @Override
    public void setStepData(StepData stepData) {
        if(stepData == null){
            return;
        }

        if(stepData instanceof GuideData){
            GuideData guideData = (GuideData)stepData;
            setTitle(CommonUtils.getCareCoordiTitleResId(stepData));

            getViewDataBinding().setShowFootNote(false);
            if(guideData.getStep() == CareCoordiActivity.STEP.NEED_HELP){
                getViewDataBinding().setShowFootNote(true);
            }

            GuideAdapter adapter = new GuideAdapter(new ExampleClickListener() {
                @Override
                public void onExampleClick(View view, ExampleData exampleData) {
                    for(int i=0; i<getViewDataBinding().fGuideRecyclerView.getChildCount(); i++){
                        View childView = getViewDataBinding().fGuideRecyclerView.getChildAt(i);
                        if(childView != view){
                            childView.setSelected(false);
                        }
                    }

                    for(ExampleData data : guideData.getExampleDataList()){
                        if(data.getExample() != exampleData.getExample()){
                            data.setSelected(false);
                        }
                    }

                    ((CareCoordiActivity)getBaseActivity()).selectExampleData(exampleData);
                }
            });
            adapter.setExampleList(guideData.getExampleDataList());

            getViewDataBinding().fGuideRecyclerView.setAdapter(adapter);
        }
    }

    public class GuideAdapter extends RecyclerView.Adapter<GuideAdapter.GuideViewHolder>
    {
        List<ExampleData> mExampleList;

        @Nullable
        private final ExampleClickListener mListener;

        public GuideAdapter(@Nullable final ExampleClickListener listener){
            mListener = listener;
        }

        public void setExampleList(final List<ExampleData> exampleList){
            mExampleList = exampleList;
            notifyItemRangeInserted(0, mExampleList.size());
        }

        @NonNull
        @Override
        public GuideViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
            IGuideBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_guide, viewGroup, false);
            return new GuideViewHolder(binding, mListener);
        }

        @Override
        public void onBindViewHolder(@NonNull GuideViewHolder guideViewHolder, int position) {
            guideViewHolder.setExample(mExampleList.get(position));
            guideViewHolder.binding.setText(getString(mExampleList.get(position).getExample().getTextResId()));
            guideViewHolder.binding.executePendingBindings();
        }

        @Override
        public int getItemCount() {
            return mExampleList == null ? 0 : mExampleList.size();
        }

        class GuideViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            final IGuideBinding binding;
            final View view;
            final ExampleClickListener listener;
            private ExampleData example;

            private GuideViewHolder(IGuideBinding binding, ExampleClickListener listener) {
                super(binding.getRoot());
                this.binding = binding;
                this.view = binding.getRoot();
                this.listener = listener;
                this.view.setOnClickListener(this);
            }

            public void setExample(ExampleData example) {
                this.example = example;
                this.view.setSelected(example.isSelected());
            }

            @Override
            public void onClick(View v) {
                if(view.isSelected()){
                    return;
                }

                view.setSelected(true);

                if(listener != null){
                    example.setSelected(true);
                    listener.onExampleClick(v, example);
                }
            }
        }
    }
}
