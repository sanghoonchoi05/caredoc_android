package com.onestepmore.caredoc.ui.start;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.databinding.AStartBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.account.AccountMainActivity;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.carecoordi.CareCoordiStartActivity;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.main.MainActivity;

import javax.inject.Inject;

import static com.onestepmore.caredoc.ui.carecoordi.CareCoordiActivity.RESULT_CODE_CareGuide_ACTIVITY_FINISH;

public class StartActivity extends BaseActivity<AStartBinding, StartViewModel> implements StartNavigator {

    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private StartViewModel mStartViewModel;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_start;
    }

    @Override
    public StartViewModel getViewModel() {
        mStartViewModel = ViewModelProviders.of(this, mViewModelFactory).get(StartViewModel.class);
        return mStartViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStartViewModel.setNavigator(this);

        BackStackManager.getInstance().setFragmentSlideAnimation();

        getViewDataBinding().aStartRegisterStartButtonText.setText(FirebaseRemoteConfig.getInstance().getString("main_register_text"));
        getViewDataBinding().aStartCareCoordiStartButtonText.setText(FirebaseRemoteConfig.getInstance().getString("main_care_coordi_text"));
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void openAccountMainActivity() {
        Intent intent = newIntent(AccountMainActivity.class);
        startActivity(intent);
    }

    @Override
    public void openCareCoordiActivity() {
        Intent intent = newIntent(CareCoordiStartActivity.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public void openMainActivity(int index) {
        BackStackManager.getInstance().removeAllFragment();
        Intent intent = newIntent(MainActivity.class);
        intent.putExtra("index", index);
        startActivity(intent);
        finish();
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_CODE_CareGuide_ACTIVITY_FINISH){
            openMainActivity(1);
        }
    }
}
