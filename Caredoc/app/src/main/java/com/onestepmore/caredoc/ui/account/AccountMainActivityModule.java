package com.onestepmore.caredoc.ui.account;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class AccountMainActivityModule {
    @Provides
    AccountMainViewModel accountMainViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new AccountMainViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideAccountMainViewModelProvider(AccountMainViewModel accountMainViewModel) {
        return new ViewModelProviderFactory<>(accountMainViewModel);
    }
}
