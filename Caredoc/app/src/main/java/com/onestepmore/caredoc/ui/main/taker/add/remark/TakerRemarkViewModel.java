package com.onestepmore.caredoc.ui.main.taker.add.remark;

import android.app.Application;
import android.databinding.ObservableField;
import android.view.View;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TakerRemarkViewModel extends BaseViewModel<TakerRemarkNavigator> {

    public TakerRemarkViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClick(View view){
        getNavigator().onClick(view.getTag().toString());
    }
}
