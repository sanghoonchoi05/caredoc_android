package com.onestepmore.caredoc.ui.main.search.list;

import android.app.Application;
import android.databinding.ObservableBoolean;

import com.google.gson.Gson;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchRequest;
import com.onestepmore.caredoc.data.model.api.object.SearchFilter;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import io.realm.RealmResults;

public class SearchListViewModel extends BaseViewModel<SearchListNavigator> {

    private final ObservableBoolean mEmpty = new ObservableBoolean(false);
    private final ObservableBoolean mNoMoreList = new ObservableBoolean(false);
    private final Gson mGson;
    private final FacilityRepository mFacilityRepository;
    private RealmResults<FacilitySearchList> mFacilitySearchList;

    private int mSkip = 0;
    private int mTake = 10;

    public FacilityRepository getFacilityRepository() {
        return mFacilityRepository;
    }
    public RealmResults<FacilitySearchList> getFacilitySearchList() {
        return mFacilitySearchList;
    }
    public Gson getGson() {
        return mGson;
    }
    public ObservableBoolean getEmpty() {
        return mEmpty;
    }
    public void setEmpty(boolean empty){
        mEmpty.set(empty);
    }
    public ObservableBoolean getNoMoreList() {
        return mNoMoreList;
    }
    public void setNoMoreList(boolean value){
        mNoMoreList.set(value);
    }
    public void setSkip(int mSkip) {
        this.mSkip = mSkip;
    }

    public int getSkip() {
        return mSkip;
    }

    public int getTake() {
        return mTake;
    }

    public SearchListViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider,
                               Gson gson, FacilityRepository facilityRepository) {
        super(application, appDataSource, schedulerProvider);

        mGson = gson;
        mFacilityRepository = facilityRepository;

        init();
    }

    private void init(){
        clearFacilityRealm();

        if(mFacilitySearchList == null){
            mFacilitySearchList = getFacilityRepository().getFacilitySearchList();
        }
    }

    public void loadFacilitySearchListFromServer(SearchFilter filter, FacilitySearchRequest.ORDER_BY orderBy){
        loadFacilitySearchListFromServer(getFacilitySearchRequestQueryParameter(filter, orderBy));
    }

    private FacilitySearchRequest.QueryParameter getFacilitySearchRequestQueryParameter(SearchFilter filter, FacilitySearchRequest.ORDER_BY orderBy){
        FacilitySearchRequest.QueryParameter queryParameter = new FacilitySearchRequest.QueryParameter();
        queryParameter.setFilter(mGson.toJson(filter));
        queryParameter.setSkip(mSkip);
        queryParameter.setTake(mTake);
        queryParameter.setOrderBy(orderBy.getName());

        return queryParameter;
    }

    private void loadFacilitySearchListFromServer(FacilitySearchRequest.QueryParameter queryParameter){
        getCompositeDisposable().add(getFacilityRepository()
                .loadFacilitySearchList(queryParameter)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(size ->{
                    getFacilityRepository().fetchFacilityTotalCount();
                    mNoMoreList.set(false);
                    if(size == 0){
                        // 요청했으나 더이상 리스트가 없는 경우
                        mNoMoreList.set(true);
                    }else{
                        // 요청한 수보다 리스트수가 적은 경우
                        if(mTake > size){
                            mNoMoreList.set(true);
                        }
                    }
                }, throwable -> getDataManager().handleThrowable(throwable, getNavigator())));
    }

    void clearFacilityRealm(){
        getFacilityRepository().clearFacilityRealm(FacilitySearchList.class);
    }

    @Override
    protected void onCleared() {
        clearFacilityRealm();
        super.onCleared();
    }

    public void onMap(){
        getNavigator().onMap();
    }
}
