package com.onestepmore.caredoc.ui.carecoordi.result;

import android.app.Application;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;

import com.onestepmore.caredoc.data.model.api.carecoordi.CareCoordiRequest;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.firebase.FirebaseEvent;
import com.onestepmore.caredoc.firebase.FirebaseManager;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class ResultViewModel extends BaseViewModel<ResultNavigator> {
    private final ObservableBoolean mEmpty = new ObservableBoolean(false);
    private final ObservableBoolean mNoMoreList = new ObservableBoolean(false);

    public ResultViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public ObservableBoolean getEmpty() {
        return mEmpty;
    }

    public ObservableBoolean getNoMoreList() {
        return mNoMoreList;
    }

    public void setEmpty(boolean empty){
        mEmpty.set(empty);
    }

    public void setNoMoreList(boolean noMoreList){
        mNoMoreList.set(noMoreList);
    }

    public void onCareCoordiRetryButtonClick(){
        FirebaseManager.getInstance().logEvent(FirebaseEvent.ClickAction.RESULT_CARECOORDI_CLICK_RETRY);
        getNavigator().onCareCoordiRetryButtonClick();
    }

    public void onOrderButtonClick(){
        getNavigator().onOrderButtonClick();
    }

    public void onExit(){
        getNavigator().onExit();
    }
}
