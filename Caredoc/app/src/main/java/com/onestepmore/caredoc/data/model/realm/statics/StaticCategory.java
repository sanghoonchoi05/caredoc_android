package com.onestepmore.caredoc.data.model.realm.statics;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.SearchCategoriesObject;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class StaticCategory extends RealmObject implements Parcelable {
    @PrimaryKey
    private String text;
    private int id;
    private RealmList<String> grades;
    private RealmList<Integer> queryServiceTypeIds;

    public StaticCategory(){}

    protected StaticCategory(Parcel in) {
        text = in.readString();
        id = in.readInt();
        this.grades = new RealmList<>();
        in.readList(this.grades, String.class.getClassLoader());
        this.queryServiceTypeIds = new RealmList<>();
        in.readList(this.queryServiceTypeIds, Integer.class.getClassLoader());
    }

    public void setCategory(SearchCategoriesObject.Item category){
        this.text = category.getText();
        this.id = category.getId();
        if(category.getGrades() != null && category.getGrades().size() > 0){
            this.grades = new RealmList<>();
            this.grades.addAll(category.getGrades());
        }
        if(category.getQuery().getServiceTypeIds() != null && category.getQuery().getServiceTypeIds().size() > 0){
            this.queryServiceTypeIds = new RealmList<>();
            this.queryServiceTypeIds.addAll(category.getQuery().getServiceTypeIds());
        }
    }

    public static final Creator<StaticCategory> CREATOR = new Creator<StaticCategory>() {
        @Override
        public StaticCategory createFromParcel(Parcel in) {
            return new StaticCategory(in);
        }

        @Override
        public StaticCategory[] newArray(int size) {
            return new StaticCategory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeInt(id);
        dest.writeList(grades);
        dest.writeList(queryServiceTypeIds);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RealmList<String> getGrades() {
        return grades;
    }

    public void setGrades(RealmList<String> grades) {
        this.grades = grades;
    }

    public RealmList<Integer> getQueryServiceTypeIds() {
        return queryServiceTypeIds;
    }

    public void setQueryServiceTypeIds(RealmList<Integer> queryServiceTypeIds) {
        this.queryServiceTypeIds = queryServiceTypeIds;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        StaticCategory staticCategory = (StaticCategory) object;
        if (text != null ? !text.equals(staticCategory.text) : staticCategory.text != null) {
            return false;
        }

        if (grades != null ? !grades.equals(staticCategory.grades) : staticCategory.grades != null) {
            return false;
        }

        return queryServiceTypeIds != null ? queryServiceTypeIds.equals(staticCategory.queryServiceTypeIds) : staticCategory.queryServiceTypeIds == null;

    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (grades != null ? grades.hashCode() : 0);
        result = 31 * result + (queryServiceTypeIds != null ? queryServiceTypeIds.hashCode() : 0);
        return result;
    }

    public boolean isHospital(){
        if(queryServiceTypeIds == null){
            return false;
        }

        for(Integer serviceId : queryServiceTypeIds){
            if(serviceId == StaticService.SERVICE_TYPE.HOSPITAL.getId()){
                return true;
            }
        }

        return false;
    }
}
