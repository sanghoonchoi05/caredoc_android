package com.onestepmore.caredoc.data.model.api.external;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.object.external.JusoResultObject;

public class ExternalJusoResponse {
    @Expose
    @SerializedName("results")
    private JusoResultObject results;

    public JusoResultObject getResults() {
        return results;
    }
}
