package com.onestepmore.caredoc.ui.main.detail.comment.write;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface WriteCommentNavigator extends BaseNavigator {
    void onRegisterComment();
    void onCompleteRegisterComment();
    void onCompleteRemoveComment();
}
