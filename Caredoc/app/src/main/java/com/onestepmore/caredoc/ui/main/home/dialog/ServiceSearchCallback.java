package com.onestepmore.caredoc.ui.main.home.dialog;

import com.onestepmore.caredoc.data.model.realm.statics.StaticService;

import java.util.List;

public interface ServiceSearchCallback {
    void onSearchService(StaticService staticService);
    void onServiceSearchClose();
    void onApply();
}
