package com.onestepmore.caredoc.data.remote;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.di.ApiInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Credentials;

@Singleton
public class ApiHeader {
    private ProtectedApiHeader mProtectedApiHeader;

    private PublicApiHeader mPublicApiHeader;

    @Inject
    public ApiHeader(PublicApiHeader publicApiHeader, ProtectedApiHeader protectedApiHeader) {
        mPublicApiHeader = publicApiHeader;
        mProtectedApiHeader = protectedApiHeader;
    }

    public ProtectedApiHeader getProtectedApiHeader() {
        return mProtectedApiHeader;
    }

    public PublicApiHeader getPublicApiHeader() {
        return mPublicApiHeader;
    }

    public static class ProtectedApiHeader {

        @Expose
        @SerializedName("Authorization")
        private String mAccessToken;

        @Expose
        @SerializedName("CAREDOC-API-KEY")
        private String mApiKey;

        public ProtectedApiHeader(String apiKey, String accessToken) {
            this.mApiKey = apiKey;
            this.mAccessToken = accessToken;
        }

        public void setAccessToken(String accessToken) {
            mAccessToken = accessToken;
        }

        public String getApiKey() {
            return mApiKey;
        }

        public String getAccessToken() {
            return mAccessToken;
        }

        public void setApiKey(String apiKey) {
            mApiKey = apiKey;
        }
    }

    public static class PublicApiHeader {

        @Expose
        @SerializedName("CAREDOC-API-KEY")
        private String mApiKey;

        @Inject
        public PublicApiHeader(@ApiInfo String apiKey) {
            mApiKey = apiKey;
        }

        public String getApiKey() {
            return mApiKey;
        }

        public void setApiKey(String apiKey) {
            mApiKey = apiKey;
        }
    }

    public static final class ExternalProtectedApiHeader {

        @Expose
        @SerializedName("Authorization")
        private String mAccessToken;

        public ExternalProtectedApiHeader(String accessToken) {
            this.mAccessToken = accessToken;
        }

        public String getAccessToken() {
            return mAccessToken;
        }
    }

    public static final class ExternalMailgunProtectedApiHeader {

        @Expose
        @SerializedName("Authorization")
        private String mAccessToken;

        public ExternalMailgunProtectedApiHeader() {
            this.mAccessToken = Credentials.basic("api", "226386d754dc6f39ba1c2527dcd622c5-059e099e-9173ebbf");
        }

        public String getAccessToken() {
            return mAccessToken;
        }
    }
}
