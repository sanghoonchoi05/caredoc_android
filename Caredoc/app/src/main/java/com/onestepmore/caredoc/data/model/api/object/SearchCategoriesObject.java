package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

import java.util.List;

public final class SearchCategoriesObject extends ItemResponse<SearchCategoriesObject.Item> {
    public final class Item{

        @Expose
        @SerializedName("id")
        private int id;

        @Expose
        @SerializedName("text")
        private String text;

        @Expose
        @SerializedName("grades")
        private List<String> grades;

        @Expose
        @SerializedName("query")
        private Query query;

        public int getId() {
            return id;
        }

        public String getText() {
            return text;
        }

        public List<String> getGrades() {
            return grades;
        }

        public Query getQuery() {
            return query;
        }

        public final class Query{
            @Expose
            @SerializedName("service_type_id")
            private List<Integer> serviceTypeIds;

            public List<Integer> getServiceTypeIds() {
                return serviceTypeIds;
            }
        }
    }
}
