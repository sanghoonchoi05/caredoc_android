package com.onestepmore.caredoc.ui.main.favorite.detail;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.BookmarkFacilityList;
import com.onestepmore.caredoc.databinding.IFacilityVerticalBinding;
import com.onestepmore.caredoc.ui.main.FacilityVerticalAdapter;
import com.onestepmore.caredoc.ui.main.MainNavigator;

import io.realm.OrderedRealmCollection;

public class FavoriteFacilityAdapter extends FacilityVerticalAdapter<BookmarkFacilityList, FacilityVerticalAdapter.FacilityVerticalViewHolder> {

    public FavoriteFacilityAdapter(@Nullable OrderedRealmCollection<BookmarkFacilityList> data) {
        super(data, MainNavigator.FROM.BOOKMARK_DETAIL);
    }

    @NonNull
    @Override
    public FacilityVerticalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFacilityVerticalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_vertical, viewGroup, false);
        return new FacilityVerticalViewHolder(binding);
    }
}