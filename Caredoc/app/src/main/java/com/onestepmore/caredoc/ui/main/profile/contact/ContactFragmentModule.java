package com.onestepmore.caredoc.ui.main.profile.contact;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ContactFragmentModule {
    @Provides
    ContactViewModel contactViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new ContactViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("ContactFragment")
    ViewModelProvider.Factory provideContactViewModelProvider(ContactViewModel contactViewModel) {
        return new ViewModelProviderFactory<>(contactViewModel);
    }
}
