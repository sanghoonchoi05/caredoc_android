package com.onestepmore.caredoc.ui.main.search.filter.detail;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class FilterDetailFragmentModule {
    @Provides
    FilterDetailViewModel filterDetailViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new FilterDetailViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("FilterDetailFragment")
    ViewModelProvider.Factory provideFilterDetailViewModelProvider(FilterDetailViewModel viewModel) {
        return new ViewModelProviderFactory<>(viewModel);
    }
}
