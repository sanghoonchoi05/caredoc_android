package com.onestepmore.caredoc.ui.carecoordi;

import android.databinding.ViewDataBinding;

import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.base.BaseViewModel;

public abstract class BaseCoordiFragment<T extends ViewDataBinding,V extends BaseViewModel> extends BaseFragment<T,V> {
    public abstract void setTitle(int titleResId);
    public abstract void setStepData(StepData stepData);
    public void onExit(){
        ((CareCoordiActivity)getBaseActivity()).onExit();
    }
}
