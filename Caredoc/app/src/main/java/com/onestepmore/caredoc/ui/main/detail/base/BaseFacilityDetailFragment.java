package com.onestepmore.caredoc.ui.main.detail.base;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.onestepmore.caredoc.data.model.api.object.FacilityDetailObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceDetailObject;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.ui.main.detail.FacilityDetailNavigator;
import com.onestepmore.caredoc.utils.UIUtils;

public abstract class BaseFacilityDetailFragment<
        T extends ViewDataBinding,
        V extends BaseViewModel,
        S extends ServiceDetailObject> extends BaseFragment<T,V> {
    private S mServiceDetailObject;
    public void setServiceDetailObject(S serviceDetailObject){
        mServiceDetailObject = serviceDetailObject;
    }
    public S getServiceDetailObject(){
        return mServiceDetailObject;
    }

    private FacilityDetailObject mFacilityDetailObject;
    public void setFacilityDetailObject(FacilityDetailObject facilityDetailObject){
        mFacilityDetailObject = facilityDetailObject;
    }
    public FacilityDetailObject getFacilityDetailObject(){
        return mFacilityDetailObject;
    }

    private FacilityDetailNavigator mFacilityDetailNavigator;
    public void setFacilityDetailNavigator(FacilityDetailNavigator navigator){
        mFacilityDetailNavigator = navigator;
    }
    public FacilityDetailNavigator getFacilityDetailNavigator(){
        return mFacilityDetailNavigator;
    }

    public abstract void refreshComment();
    public abstract void refreshReview();

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        UIUtils.setStatusBarTranslucent(getBaseActivity(), getView(), true);
    }

    @Override
    public void onShow() {
        UIUtils.setStatusBarTranslucent(getBaseActivity(), getView(), true);
    }
}
