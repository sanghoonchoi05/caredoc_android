package com.onestepmore.caredoc.ui.common;

public interface AddressDetailListener {
    void onDeleteAddress();
    void onSearchAddress();
}
