package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityCareCoordiObject extends FacilitySimpleObject {
    @Expose
    @SerializedName("distance")
    private double distance;

    public double getDistance() {
        return distance;
    }
}
