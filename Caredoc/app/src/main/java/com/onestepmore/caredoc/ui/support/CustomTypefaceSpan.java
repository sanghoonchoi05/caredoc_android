package com.onestepmore.caredoc.ui.support;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.ColorInt;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

public class CustomTypefaceSpan extends TypefaceSpan {

    private final Typeface mNewType;
    private final int mColor;

    public CustomTypefaceSpan(String family, Typeface type, @ColorInt int color) {
        super(family);
        mNewType = type;
        mColor = color;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, mNewType, mColor);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, mNewType, mColor);
    }

    private static void applyCustomTypeFace(Paint paint, Typeface tf, @ColorInt int color) {
        int oldStyle;
        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        int fake = oldStyle & ~tf.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        if(color != 0){
            paint.setColor(color);
        }
        paint.setTypeface(tf);
    }
}
