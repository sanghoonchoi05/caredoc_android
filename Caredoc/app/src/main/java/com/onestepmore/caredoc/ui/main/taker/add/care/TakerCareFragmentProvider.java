package com.onestepmore.caredoc.ui.main.taker.add.care;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerCareFragmentProvider {
    @ContributesAndroidInjector(modules = TakerCareFragmentModule.class)
    abstract TakerCareFragment provideTakerCareFragmentFactory();
}
