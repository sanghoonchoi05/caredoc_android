package com.onestepmore.caredoc.ui.address.result;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.Juso;
import com.onestepmore.caredoc.data.model.realm.JusoRealmModel;
import com.onestepmore.caredoc.databinding.FAddressResultBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.address.AddressMainActivity;
import com.onestepmore.caredoc.ui.address.detail.AddressDetailFragment;
import com.onestepmore.caredoc.ui.base.BaseFragment;
import com.onestepmore.caredoc.ui.fragmentmanager.BackStackManager;
import com.onestepmore.caredoc.ui.support.CustomDividerItemDecoration;
import com.onestepmore.caredoc.utils.CommonUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmResults;

public class AddressResultFragment extends BaseFragment<FAddressResultBinding, AddressResultViewModel> implements
        AddressResultNavigator,
        NestedScrollView.OnScrollChangeListener
{
    public static AddressResultFragment newInstance(String searchText){
        Bundle bundle = new Bundle();
        bundle.putString("searchText", searchText);
        AddressResultFragment fragment = new AddressResultFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Inject
    @Named("AddressResultFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private AddressResultViewModel mAddressResultViewModel;

    private RealmResults<Juso> mJusoResults;
    private String mSearchText;
    private int mNestedScrollViewY = 0;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_address_result;
    }

    @Override
    public AddressResultViewModel getViewModel() {
        mAddressResultViewModel = ViewModelProviders.of(this, mViewModelFactory).get(AddressResultViewModel.class);
        return mAddressResultViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressResultViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            mSearchText = getArguments().getString("searchText");
            getViewDataBinding().setSearchText(mSearchText);
            getViewModel().clearJuso();
            getViewModel().getJuso(mSearchText);
        }

        setTextObserver();
        initAdapter();
        initListener();
    }

    private void initListener(){
        getViewDataBinding().fAddressResultEditLayout.vCommonAddressEditText.setOnEditorActionListener((textView, actionId, event) ->{
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                onSearch();
                return true;
            }
            return false;
        });
    }

    private void setTextObserver(){
        mJusoResults = getViewModel().getRealm().where(Juso.class).findAll();
        mJusoResults.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Juso>>() {
            @Override
            public void onChange(RealmResults<Juso> results, OrderedCollectionChangeSet changeSet) {
                AppLogger.i(getClass(), "AddressResultFragment realm Juso size: " + results.size());
            }
        });

        rx.Observable<CharSequence> searchObserver = RxTextView.textChanges(getViewDataBinding().fAddressResultEditLayout.vCommonAddressEditText);
        getViewModel().addSubscription(searchObserver.skip(1).subscribe((text) -> getViewDataBinding().setSearchText(text.toString())));
    }

    private void initAdapter(){
        AddressResultAdapter resultAdapter = new AddressResultAdapter(mJusoResults, this);
        getViewDataBinding().fAddressResultRecyclerView.setAdapter(resultAdapter);
        getViewDataBinding().fAddressResultRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));

        CustomDividerItemDecoration dividerItemDecoration =
                new CustomDividerItemDecoration(getBaseActivity(),new LinearLayoutManager(getBaseActivity()).getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_facility));
        getViewDataBinding().fAddressResultRecyclerView.addItemDecoration(dividerItemDecoration);
        getViewDataBinding().fAddressResultRecyclerView.setItemAnimator(null);
        getViewDataBinding().fAddressResultScrollView.setOnScrollChangeListener(this);
    }

    @Override
    public void onSearch() {
        String searchText = getViewDataBinding().getSearchText();

        if(CommonUtils.checkNullAndEmpty(searchText)){
            showCommonDialog(getString(R.string.warning_msg_address_search_title), getString(R.string.warning_msg_address_search_empty_desc));
            return;
        }

        mSearchText = searchText;
        getViewModel().clearJuso();
        getViewModel().getJuso(mSearchText, 1);
    }

    @Override
    public void onDelete() {
        getViewDataBinding().setSearchText("");
    }

    @Override
    public void onBack() {
        BackStackManager.getInstance().onBackPressed();
    }

    @Override
    public void onItemClick(Juso juso) {
        getViewModel().getCoordinate(juso);
    }

    @Override
    public void onUpdateCoordinate(Juso juso){
        if(getBaseActivity() instanceof AddressMainActivity){
            if(((AddressMainActivity)getBaseActivity()).isNeedDetail()){
                onShowAddressDetailView(juso);
            }
            else{
                ((AddressMainActivity)getBaseActivity()).onSelectJuso(juso);
            }
        }
    }

    public void onShowAddressDetailView(Juso juso) {
        BackStackManager.getInstance().addChildFragment(
                AddressDetailFragment.newInstance(juso),
                R.id.a_address_main_fragment_layout);
    }

    @Override
    public void onScrollChange(NestedScrollView nestedScrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        mNestedScrollViewY = scrollY;

        if (!nestedScrollView.canScrollVertically(1)) {
            getViewDataBinding().fAddressResultRecyclerView.stopScroll();
            getViewDataBinding().fAddressResultRecyclerView.stopNestedScroll();

            getViewModel().getJuso(mSearchText);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindRealm();
    }

    private void unbindRealm() {
        if(mJusoResults!=null){
            getViewModel().getRealm().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    mJusoResults.deleteAllFromRealm();
                }
            });
            mJusoResults.removeAllChangeListeners();
        }
    }
}
