package com.onestepmore.caredoc.ui.carecoordi.guide;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class GuideFragmentProvider {
    @ContributesAndroidInjector(modules = GuideFragmentModule.class)
    abstract GuideFragment provideGuideFragmentFactory();
}
