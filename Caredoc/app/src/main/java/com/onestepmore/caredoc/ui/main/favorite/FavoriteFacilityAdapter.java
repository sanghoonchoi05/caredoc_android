package com.onestepmore.caredoc.ui.main.favorite;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.databinding.IFavoriteFacilityBinding;

import io.realm.RealmResults;

public class FavoriteFacilityAdapter extends RecyclerView.Adapter<FavoriteFacilityAdapter.FavoriteFacilityViewHolder>
{
    private RealmResults<StaticCategory> mCategoryList = null;
    private int [] mCountList = null;

    @Nullable
    private final FavoriteNavigator mNavigator;

    public FavoriteFacilityAdapter(@Nullable FavoriteNavigator navigator){
        mNavigator = navigator;
    }

    public void setCategoryList(final RealmResults<StaticCategory> categoryList){
        mCategoryList = categoryList;
        notifyDataSetChanged();
    }

    public void setCountList(int [] countList){
        mCountList = countList;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public FavoriteFacilityViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        IFavoriteFacilityBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_favorite_facility, viewGroup, false);
        return new FavoriteFacilityViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteFacilityViewHolder favoriteFacilityViewHolder, int position) {
        StaticCategory category = mCategoryList.get(position);
        if(category != null){
            favoriteFacilityViewHolder.binding.setCategory(category);
            //favoriteFacilityViewHolder.binding.setIcon(CommonUtils.getCategoryIconForList(mContext, category.getCode()));
        }
        favoriteFacilityViewHolder.binding.setNavigator(mNavigator);
        favoriteFacilityViewHolder.binding.setCount(mCountList[position]);
        favoriteFacilityViewHolder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCategoryList == null ? 0 : mCategoryList.size();
    }

    class FavoriteFacilityViewHolder extends RecyclerView.ViewHolder {
        final IFavoriteFacilityBinding binding;

        private FavoriteFacilityViewHolder(IFavoriteFacilityBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}