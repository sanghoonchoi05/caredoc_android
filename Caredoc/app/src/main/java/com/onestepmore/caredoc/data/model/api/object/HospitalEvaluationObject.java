package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HospitalEvaluationObject {
    // 욕창악화
    @Expose
    @SerializedName("ulcers_worsened_high_risk_group")
    private float ulcersWorsened;

    // 욕창발생
    @Expose
    @SerializedName("newly_developed_pressure_ulcers_high_risk_group")
    private float newlyUlcers;

    // 욕창개선
    @Expose
    @SerializedName("improved_pressure_ulcers_high_risk_group")
    private float improvedUlcers;

    public float getUlcersWorsened() {
        return ulcersWorsened;
    }

    public void setUlcersWorsened(float ulcersWorsened) {
        this.ulcersWorsened = ulcersWorsened;
    }

    public float getNewlyUlcers() {
        return newlyUlcers;
    }

    public void setNewlyUlcers(float newlyUlcers) {
        this.newlyUlcers = newlyUlcers;
    }

    public float getImprovedUlcers() {
        return improvedUlcers;
    }

    public void setImprovedUlcers(float improvedUlcers) {
        this.improvedUlcers = improvedUlcers;
    }
}
