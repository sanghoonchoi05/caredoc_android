package com.onestepmore.caredoc.ui.main.taker.add.disease;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class TakerDiseaseViewModel extends BaseViewModel<TakerDiseaseNavigator> {
    public TakerDiseaseViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }
}
