package com.onestepmore.caredoc.ui.estimate.request;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class EstimateRequestPagerAdapter extends FragmentPagerAdapter {
    private List<BaseEstimateRequestFragment> estimateRequestFragmentList = new ArrayList<>();
    private Context context;

    public EstimateRequestPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void pushEstimateRequestFragmentListFragment(BaseEstimateRequestFragment fragment){
        estimateRequestFragmentList.add(fragment);
    }
    @Override
    public int getCount() {
        return estimateRequestFragmentList.size();
    }

    @Override
    public BaseEstimateRequestFragment getItem(int position) {
        return estimateRequestFragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }
}
