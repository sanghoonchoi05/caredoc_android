package com.onestepmore.caredoc.ui.widgets;

public interface BottomSheetDialogClickListener {
    void onClick(String tag, String text);
    void onClose(String tag);
    void onInit(String tag);
}
