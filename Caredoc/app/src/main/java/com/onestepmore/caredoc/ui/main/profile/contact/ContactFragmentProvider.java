package com.onestepmore.caredoc.ui.main.profile.contact;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ContactFragmentProvider {
    @ContributesAndroidInjector(modules = ContactFragmentModule.class)
    abstract ContactFragment provideContactFragmentFactory();
}
