package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcel;
import android.os.Parcelable;

import com.onestepmore.caredoc.data.model.api.object.ServiceAddressObject;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ServiceAddress extends RealmObject implements Parcelable {
    @PrimaryKey
    private long id;
    private String addressName;
    private String addressCode;
    private String addressDetail;
    private String roadAddressName;
    private String roadAddressCode;
    private LatLngTaker latLng;

    public ServiceAddress(){}

    protected ServiceAddress(Parcel in) {
        id = in.readLong();
        addressName = in.readString();
        addressCode = in.readString();
        addressDetail = in.readString();
        roadAddressName = in.readString();
        roadAddressCode = in.readString();
        latLng = in.readParcelable(LatLngTaker.class.getClassLoader());
    }

    public static final Creator<ServiceAddress> CREATOR = new Creator<ServiceAddress>() {
        @Override
        public ServiceAddress createFromParcel(Parcel in) {
            return new ServiceAddress(in);
        }

        @Override
        public ServiceAddress[] newArray(int size) {
            return new ServiceAddress[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(addressName);
        dest.writeString(addressCode);
        dest.writeString(addressDetail);
        dest.writeString(roadAddressName);
        dest.writeString(roadAddressCode);
        dest.writeParcelable(latLng, flags);
    }

    public void setServiceAddress(ServiceAddressObject.Data serviceAddressData){
        this.id = serviceAddressData.getId();
        this.addressName = serviceAddressData.getAddressName();
        this.addressCode = serviceAddressData.getAddressCode();
        this.addressDetail = serviceAddressData.getAddressDetail();
        this.roadAddressName = serviceAddressData.getRoadAddressName();
        this.roadAddressCode = serviceAddressData.getRoadAddressCode();
        if (serviceAddressData.getGeoLocation() != null) {
            List<Double> coordinates = serviceAddressData.getGeoLocation().getCoordinates();
            if (coordinates != null && coordinates.size() > 1) {
                this.latLng = new LatLngTaker();
                this.latLng.setId(String.valueOf(serviceAddressData.getId()));
                this.latLng.setLatitude(serviceAddressData.getGeoLocation().getCoordinates().get(1));
                this.latLng.setLongitude(serviceAddressData.getGeoLocation().getCoordinates().get(0));
            }
        }
    }

    public long getId() {
        return id;
    }

    public String getAddressName() {
        return addressName;
    }

    public String getAddressCode() {
        return addressCode;
    }

    public String getRoadAddressName() {
        return roadAddressName;
    }

    public String getRoadAddressCode() {
        return roadAddressCode;
    }

    public LatLngTaker getLatLng() {
        return latLng;
    }

    public String getAddressDetail() {
        return addressDetail;
    }
}
