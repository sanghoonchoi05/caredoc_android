package com.onestepmore.caredoc.ui.main.detail.homecare;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.data.repository.ReviewRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class HomecareDetailFragmentModule {
    @Provides
    HomecareDetailViewModel homecareDetailViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                                    Gson gson, ReviewRepository reviewRepository) {
        return new HomecareDetailViewModel(application, dataSource, schedulerProvider, reviewRepository);
    }

    @Provides
    @Named("HomecareDetailFragment")
    ViewModelProvider.Factory provideHomecareDetailViewModelProvider(HomecareDetailViewModel homecareDetailViewModel) {
        return new ViewModelProviderFactory<>(homecareDetailViewModel);
    }
}
