package com.onestepmore.caredoc.ui.account.account;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AccountFragmentProvider {
    @ContributesAndroidInjector(modules = AccountFragmentModule.class)
    abstract AccountFragment provideAccountFragmentFactory();
}
