package com.onestepmore.caredoc.ui.main.search.filter.address;

import android.support.v4.widget.NestedScrollView;

import com.onestepmore.caredoc.data.model.realm.statics.StaticAddress;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface AddressFilterNavigator extends BaseNavigator {
    void onLoadComplete();
    void onAddressFilterClick(StaticAddress address);
    void onAddressTextClick(int position);
    void onInit();
    void onClose();
    void onApplyFilter();
    void onScrollChange(NestedScrollView nestedScrollView, int scrollX, int scrollY, int oldScrollX, int oldScrollY);
}
