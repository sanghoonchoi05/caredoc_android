package com.onestepmore.caredoc.data.model.api.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HospitalDetailObject {
    @Expose
    @SerializedName("id")
    private long id;
    @Expose
    @SerializedName("external_id")
    private long externalId;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("address")
    private String address;
    @Expose
    @SerializedName("contact")
    private String contact;
    @Expose
    @SerializedName("post")
    private String post;
    @Expose
    @SerializedName("post_serial_number")
    private String postSerialNumber;
    @Expose
    @SerializedName("foundation_type_code")
    private String foundationTypeCcode;
    @Expose
    @SerializedName("emergency_room_count")
    private int emergencyRoomCount;
    @Expose
    @SerializedName("physical_therapy_room_count")
    private int physicalTherapyRoomCount;
    @Expose
    @SerializedName("operating_room_count")
    private int operatingRoomCount;
    @Expose
    @SerializedName("advanced_bed_count")
    private int advanced_bed_count;
    @Expose
    @SerializedName("general_bed_count")
    private int generalBedCount;
    @Expose
    @SerializedName("funeral")
    private int funeral;
    @Expose
    @SerializedName("location")
    private String location;
    @Expose
    @SerializedName("volunteer_activity")
    private String volunteerActivity;
    @Expose
    @SerializedName("religious_activity")
    private String religiousActivity;
    @Expose
    @SerializedName("caring_form")
    private String caringForm;
    @Expose
    @SerializedName("chemist_count")
    private int chemistCount;
    @Expose
    @SerializedName("social_worker_count")
    private int socialWorkerCount;
    @Expose
    @SerializedName("physical_therapist_count")
    private int physicalTherapistCount;
    @Expose
    @SerializedName("occupational_therapist_count")
    private int occupationalTherapistCount;
    @Expose
    @SerializedName("person_per_doctor")
    private float personPerDoctor;
    @Expose
    @SerializedName("person_per_nurse")
    private float personPerNurse;
    @Expose
    @SerializedName("person_per_nursing_staff")
    private float personPerNursingStaff;
    @Expose
    @SerializedName("first_patient_disease")
    private String firstPatientDisease;
    @Expose
    @SerializedName("second_patient_disease")
    private String secondPatientDisease;
    @Expose
    @SerializedName("third_patient_disease")
    private String thirdPatientDisease;
    @Expose
    @SerializedName("patient_disease")
    private int patientDisease;
    @Expose
    @SerializedName("patient_self_help")
    private String patientSelfHelp;
    @Expose
    @SerializedName("patient_need_help")
    private String patientNeedHelp;
    @Expose
    @SerializedName("patient_need_full_help")
    private String patientNeedFullHelp;
    @Expose
    @SerializedName("nutritionist_count")
    private int nutritionistCount;
    @Expose
    @SerializedName("cook_count")
    private int cookCount;
    @Expose
    @SerializedName("medical_expenses")
    private int medicalExpenses;
    @Expose
    @SerializedName("emergency_day")
    private int emergencyDay;
    @Expose
    @SerializedName("emergency_day_contact")
    private String emergencyDayContact;
    @Expose
    @SerializedName("emergency_night")
    private int emergencyNight;
    @Expose
    @SerializedName("emergency_night_contact")
    private String emergencyNightContact;
    @Expose
    @SerializedName("geo_location")
    private ApiCommonObject.GeoLocation geoLocation;

    @Expose
    @SerializedName("foundation_type")
    private FoundationTypeObject foundationType;

    @Expose
    @SerializedName("evaluations")
    private List<HospitalEvaluationObject> evaluations;

    @Expose
    @SerializedName("meals")
    private List<MealAddOnsObject> meals;

    @Expose
    @SerializedName("nursings")
    private List<NursingAddOnsObject> nursings;

    @Expose
    @SerializedName("traffics")
    private List<TrafficObject> traffis;

    @Expose
    @SerializedName("workers")
    private List<WorkerObject> workers;

    @Expose
    @SerializedName("grades")
    private List<GradeDetailObject> grades;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getExternalId() {
        return externalId;
    }

    public void setExternalId(long externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPostSerialNumber() {
        return postSerialNumber;
    }

    public void setPostSerialNumber(String postSerialNumber) {
        this.postSerialNumber = postSerialNumber;
    }

    public String getFoundationTypeCcode() {
        return foundationTypeCcode;
    }

    public void setFoundationTypeCcode(String foundationTypeCcode) {
        this.foundationTypeCcode = foundationTypeCcode;
    }

    public int getEmergencyRoomCount() {
        return emergencyRoomCount;
    }

    public void setEmergencyRoomCount(int emergencyRoomCount) {
        this.emergencyRoomCount = emergencyRoomCount;
    }

    public int getPhysicalTherapyRoomCount() {
        return physicalTherapyRoomCount;
    }

    public void setPhysicalTherapyRoomCount(int physicalTherapyRoomCount) {
        this.physicalTherapyRoomCount = physicalTherapyRoomCount;
    }

    public int getOperatingRoomCount() {
        return operatingRoomCount;
    }

    public void setOperatingRoomCount(int operatingRoomCount) {
        this.operatingRoomCount = operatingRoomCount;
    }

    public int getAdvanced_bed_count() {
        return advanced_bed_count;
    }

    public void setAdvanced_bed_count(int advanced_bed_count) {
        this.advanced_bed_count = advanced_bed_count;
    }

    public int getGeneralBedCount() {
        return generalBedCount;
    }

    public void setGeneralBedCount(int generalBedCount) {
        this.generalBedCount = generalBedCount;
    }

    public int getFuneral() {
        return funeral;
    }

    public void setFuneral(int funeral) {
        this.funeral = funeral;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getVolunteerActivity() {
        return volunteerActivity;
    }

    public void setVolunteerActivity(String volunteerActivity) {
        this.volunteerActivity = volunteerActivity;
    }

    public String getReligiousActivity() {
        return religiousActivity;
    }

    public void setReligiousActivity(String religiousActivity) {
        this.religiousActivity = religiousActivity;
    }

    public String getCaringForm() {
        return caringForm;
    }

    public void setCaringForm(String caringForm) {
        this.caringForm = caringForm;
    }

    public int getChemistCount() {
        return chemistCount;
    }

    public void setChemistCount(int chemistCount) {
        this.chemistCount = chemistCount;
    }

    public int getSocialWorkerCount() {
        return socialWorkerCount;
    }

    public void setSocialWorkerCount(int socialWorkerCount) {
        this.socialWorkerCount = socialWorkerCount;
    }

    public int getPhysicalTherapistCount() {
        return physicalTherapistCount;
    }

    public void setPhysicalTherapistCount(int physicalTherapistCount) {
        this.physicalTherapistCount = physicalTherapistCount;
    }

    public int getOccupationalTherapistCount() {
        return occupationalTherapistCount;
    }

    public void setOccupationalTherapistCount(int occupationalTherapistCount) {
        this.occupationalTherapistCount = occupationalTherapistCount;
    }

    public float getPersonPerDoctor() {
        return personPerDoctor;
    }

    public void setPersonPerDoctor(float personPerDoctor) {
        this.personPerDoctor = personPerDoctor;
    }

    public float getPersonPerNurse() {
        return personPerNurse;
    }

    public void setPersonPerNurse(float personPerNurse) {
        this.personPerNurse = personPerNurse;
    }

    public float getPersonPerNursingStaff() {
        return personPerNursingStaff;
    }

    public void setPersonPerNursingStaff(float personPerNursingStaff) {
        this.personPerNursingStaff = personPerNursingStaff;
    }

    public String getFirstPatientDisease() {
        return firstPatientDisease;
    }

    public void setFirstPatientDisease(String firstPatientDisease) {
        this.firstPatientDisease = firstPatientDisease;
    }

    public String getSecondPatientDisease() {
        return secondPatientDisease;
    }

    public void setSecondPatientDisease(String secondPatientDisease) {
        this.secondPatientDisease = secondPatientDisease;
    }

    public String getThirdPatientDisease() {
        return thirdPatientDisease;
    }

    public void setThirdPatientDisease(String thirdPatientDisease) {
        this.thirdPatientDisease = thirdPatientDisease;
    }

    public int getPatientDisease() {
        return patientDisease;
    }

    public void setPatientDisease(int patientDisease) {
        this.patientDisease = patientDisease;
    }

    public String getPatientSelfHelp() {
        return patientSelfHelp;
    }

    public void setPatientSelfHelp(String patientSelfHelp) {
        this.patientSelfHelp = patientSelfHelp;
    }

    public String getPatientNeedHelp() {
        return patientNeedHelp;
    }

    public void setPatientNeedHelp(String patientNeedHelp) {
        this.patientNeedHelp = patientNeedHelp;
    }

    public String getPatientNeedFullHelp() {
        return patientNeedFullHelp;
    }

    public void setPatientNeedFullHelp(String patientNeedFullHelp) {
        this.patientNeedFullHelp = patientNeedFullHelp;
    }

    public int getNutritionistCount() {
        return nutritionistCount;
    }

    public void setNutritionistCount(int nutritionistCount) {
        this.nutritionistCount = nutritionistCount;
    }

    public int getCookCount() {
        return cookCount;
    }

    public void setCookCount(int cookCount) {
        this.cookCount = cookCount;
    }

    public int getMedicalExpenses() {
        return medicalExpenses;
    }

    public void setMedicalExpenses(int medicalExpenses) {
        this.medicalExpenses = medicalExpenses;
    }

    public int getEmergencyDay() {
        return emergencyDay;
    }

    public void setEmergencyDay(int emergencyDay) {
        this.emergencyDay = emergencyDay;
    }

    public String getEmergencyDayContact() {
        return emergencyDayContact;
    }

    public void setEmergencyDayContact(String emergencyDayContact) {
        this.emergencyDayContact = emergencyDayContact;
    }

    public int getEmergencyNight() {
        return emergencyNight;
    }

    public void setEmergencyNight(int emergencyNight) {
        this.emergencyNight = emergencyNight;
    }

    public String getEmergencyNightContact() {
        return emergencyNightContact;
    }

    public void setEmergencyNightContact(String emergencyNightContact) {
        this.emergencyNightContact = emergencyNightContact;
    }

    public ApiCommonObject.GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(ApiCommonObject.GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public FoundationTypeObject getFoundationType() {
        return foundationType;
    }

    public void setFoundationType(FoundationTypeObject foundationType) {
        this.foundationType = foundationType;
    }

    public List<HospitalEvaluationObject> getEvaluations() {
        return evaluations;
    }

    public void setEvaluations(List<HospitalEvaluationObject> evaluations) {
        this.evaluations = evaluations;
    }

    public List<MealAddOnsObject> getMeals() {
        return meals;
    }

    public void setMeals(List<MealAddOnsObject> meals) {
        this.meals = meals;
    }

    public List<NursingAddOnsObject> getNursings() {
        return nursings;
    }

    public void setNursings(List<NursingAddOnsObject> nursings) {
        this.nursings = nursings;
    }

    public List<TrafficObject> getTraffis() {
        return traffis;
    }

    public void setTraffis(List<TrafficObject> traffis) {
        this.traffis = traffis;
    }

    public List<WorkerObject> getWorkers() {
        return workers;
    }

    public void setWorkers(List<WorkerObject> workers) {
        this.workers = workers;
    }

    public List<GradeDetailObject> getGrades() {
        return grades;
    }

    public void setGrades(List<GradeDetailObject> grades) {
        this.grades = grades;
    }
}
