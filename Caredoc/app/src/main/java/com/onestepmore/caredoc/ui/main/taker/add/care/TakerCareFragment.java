package com.onestepmore.caredoc.ui.main.taker.add.care;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;
import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.api.taker.AddTakerRequest;
import com.onestepmore.caredoc.data.model.realm.statics.StaticLtiGrades;
import com.onestepmore.caredoc.data.model.realm.statics.StaticTypes;
import com.onestepmore.caredoc.databinding.FTakerCareBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.ui.common.TextLayoutClickListener;
import com.onestepmore.caredoc.ui.main.taker.add.AddTakerActivity;
import com.onestepmore.caredoc.ui.main.taker.add.BaseAddTakerFragment;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialog;
import com.onestepmore.caredoc.ui.widgets.BottomSheetDialogClickListener;
import com.onestepmore.caredoc.utils.UIUtils;

import javax.inject.Inject;
import javax.inject.Named;

import io.realm.RealmResults;

public class TakerCareFragment extends BaseAddTakerFragment<FTakerCareBinding, TakerCareViewModel> implements
        TakerCareNavigator,
        BottomSheetDialogClickListener
{
    private static final String BOTTOM_SHEET_DIALOG_LTI_GRADE_TAG = "BOTTOM_SHEET_DIALOG_LTI_GRADE_TAG";

    public static TakerCareFragment newInstance(int titleResId, boolean isModify){
        TakerCareFragment fragment = new TakerCareFragment();
        fragment.setArguments(getBundle(titleResId, isModify));

        return  fragment;
    }

    @Inject
    @Named("TakerCareFragment")
    ViewModelProvider.Factory mViewModelFactory;
    private TakerCareViewModel mTakerCareViewModel;
    private AddTakerRequest.Care mCachedCare;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.f_taker_care;
    }

    @Override
    public TakerCareViewModel getViewModel() {
        mTakerCareViewModel = ViewModelProviders.of(this, mViewModelFactory).get(TakerCareViewModel.class);
        return mTakerCareViewModel;
    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(getBaseActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
        AppLogger.e(getClass(), throwable, throwable.toString());
    }

    @Override
    public void networkError(String msg) {
        Toast.makeText(getBaseActivity(), msg, Toast.LENGTH_SHORT).show();
        AppLogger.i(getClass(), msg);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTakerCareViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null){
            setModify(getArguments().getBoolean("isModify"));
        }

        mCachedCare = ((AddTakerActivity)getBaseActivity()).getCachedCare();
        if(isModify() && mCachedCare != null){
            getViewDataBinding().setCare(mCachedCare);
        }

        initTitle(getViewDataBinding().fTakerCareTitle);
        initListener();
        setTextObserver();
    }

    private void initListener(){
        getViewDataBinding().fTakerCareLtiGradeLayout.setListener(new TextLayoutClickListener() {
            @Override
            public void onClickFirstLayout() {
                RealmResults<StaticLtiGrades> staticLtiGradesRealmResults = getViewModel().getRealm().where(StaticLtiGrades.class).findAll();
                if(staticLtiGradesRealmResults != null){
                    String [] textList = new String[staticLtiGradesRealmResults.size()];
                    for(int i=0; i<staticLtiGradesRealmResults.size(); i++){
                        textList[i] = staticLtiGradesRealmResults.get(i).getGradeName();
                    }
                    showBottomSheetDialog(textList, BOTTOM_SHEET_DIALOG_LTI_GRADE_TAG);
                }
            }

            @Override
            public void onClickSecondLayout() {

            }
        });
    }

    private void setTextObserver(){
        // mCachedCare데이터가 레이아웃에 set이 되기 전이기 때문에
        // TextInputEditText textChanges의 첫 Emit는 빈 문자열수밖에 없다.
        // 그러므로 첫 Emit은 skip.
        rx.Observable<CharSequence> heightObserver = RxTextView.textChanges(getViewDataBinding().fTakerCareHwLayout.vCommonTextLayout2FirstText);
        getViewModel().addSubscription(heightObserver.skip(1).subscribe((text) -> mCachedCare.setHeight(text.toString())));

        rx.Observable<CharSequence> weightObserver = RxTextView.textChanges(getViewDataBinding().fTakerCareHwLayout.vCommonTextLayout2SecondText);
        getViewModel().addSubscription(weightObserver.skip(1).subscribe((text) -> mCachedCare.setWeight(text.toString())));

        rx.Observable<CharSequence> ltiNumberObserver = RxTextView.textChanges(getViewDataBinding().fTakerCareLtiNumberLayout.vCommonTextLayout3FirstText);
        getViewModel().addSubscription(ltiNumberObserver.skip(1).subscribe((text) -> mCachedCare.setLtiNumber(text.toString())));
    }

    public void showBottomSheetDialog(String [] textList, String tag){
        BottomSheetDialog dialog = BottomSheetDialog.getInstance(textList);
        dialog.setClickListener(this);
        dialog.show(getBaseActivity().getSupportFragmentManager(), tag);
    }

    @Override
    public void onClick(String tag, String text) {
        if(mCachedCare == null){
            return;
        }

        StaticLtiGrades grades = getViewModel().getRealm().where(StaticLtiGrades.class).equalTo("gradeName", text).findFirst();
        mCachedCare.setLtiGrade(String.valueOf(grades.getGrade()));

        refreshCareInfo();
    }

    @Override
    public void onClose(String tag) {

    }

    @Override
    public void onInit(String tag) {
        if(mCachedCare == null){
            return;
        }

        mCachedCare.setLtiGrade(null);

        refreshCareInfo();
    }

    private void refreshCareInfo(){
        getViewDataBinding().setCare(mCachedCare);
    }
}
