/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.onestepmore.caredoc;


public final class AppConstants {

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;

    public static final String DB_NAME = "caredoc.db";

    public static final long NULL_INDEX = -1L;

    public static final String PREF_NAME = "caredoc_pref";

    public static final String STATICS_LTI_GRADES = "statics/lti_grades.json";

    public static final String SENTRY_DSN = "https://e13136edda8f4d268b9ce5b47f35847c@sentry.onestepmore.co.kr/5";

    public static final String CHANNEL_PLUGIN_KEY = "77e9f00e-9caa-4cae-a63f-d694c06e3ef7";

    public static final String DEEP_LINK_DOMAIN_URI_PREFIX = "https://caredoc.page.link";
    public static final String DEEP_LINK_FACILITY_DETAIL = BuildConfig.BASE_URL + "/facility/";
    private AppConstants() {
        // This utility class is not publicly instantiable
    }
}
