package com.onestepmore.caredoc.ui.main.search.map;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchMapFragmentModule {
    @Provides
    SearchMapViewModel searchMapViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                          Gson gson, FacilityRepository facilityRepository) {
        return new SearchMapViewModel(application, dataSource, schedulerProvider, gson, facilityRepository);
    }

    @Provides
    @Named("SearchMapFragment")
    ViewModelProvider.Factory provideSearchMapViewModelProvider(SearchMapViewModel searchMapViewModel) {
        return new ViewModelProviderFactory<>(searchMapViewModel);
    }
}
