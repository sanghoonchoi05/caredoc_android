package com.onestepmore.caredoc.ui.address.result;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AddressResultFragmentProvider {
    @ContributesAndroidInjector(modules = AddressResultFragmentModule.class)
    abstract AddressResultFragment provideAddressResultFragmentFactory();
}
