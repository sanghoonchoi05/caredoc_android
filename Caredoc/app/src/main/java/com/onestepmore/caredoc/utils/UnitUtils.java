package com.onestepmore.caredoc.utils;

import java.text.DecimalFormat;
import java.util.Locale;

public class UnitUtils {

    public static DecimalFormat getThousandCommnaFormat(){
        return new DecimalFormat("###,###");
    }
    public static String thousandComma(int number){
        return getThousandCommnaFormat().format(number);
    }

    public static String thousandComma(float number){
        return getThousandCommnaFormat().format(number);
    }

    public static String getFirstDecimalStr(float ratingAvg){
        return String.format(Locale.getDefault(), "%.1f", ratingAvg);
    }

    public static float getFirstDecimal(float ratingAvg){
        String rating = String.format(Locale.getDefault(), "%.1f", ratingAvg);
        return Float.valueOf(rating);
    }
}
