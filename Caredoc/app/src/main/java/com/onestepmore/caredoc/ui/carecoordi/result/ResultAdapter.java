package com.onestepmore.caredoc.ui.carecoordi.result;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.data.model.realm.FacilityCareCoordiList;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.RealmDataAccessor;
import com.onestepmore.caredoc.databinding.ICareCoordiFacilityVerticalBinding;
import com.onestepmore.caredoc.databinding.IFacilityVerticalFooterBinding;
import com.onestepmore.caredoc.ui.AppLogger;
import com.onestepmore.caredoc.utils.CommonUtils;
import com.onestepmore.caredoc.utils.UnitUtils;

import java.lang.ref.WeakReference;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class ResultAdapter extends RealmRecyclerViewAdapter<FacilityCareCoordiList, ResultAdapter.ResultViewHolder> {

    protected static final int FOOTER_VIEW = 1;

    @Nullable
    protected ResultNavigator mResultNavigator;
    private String mRatePointReviewCountFormat;
    private WeakReference<Context> mContext;
    private boolean mShowFooter = true;

    public ResultAdapter(@Nullable OrderedRealmCollection<FacilityCareCoordiList> data, Context context) {
        super(data, true, false);
        mContext = new WeakReference<>(context);
        mRatePointReviewCountFormat = mContext.get().getResources().getString(R.string.common_rate_point_review_count);
    }

    public void setResultNavigator(@Nullable ResultNavigator resultNavigator) {
        mResultNavigator = resultNavigator;
    }

    public void setShowFooter(boolean showFooter) {
        this.mShowFooter = showFooter;
        if(getData() != null){
            notifyItemChanged(getData().size());
        }
    }

    public boolean isShowFooter() {
        return mShowFooter;
    }

    @Override
    public int getItemViewType(int position) {
        if(isShowFooter() && getData() != null && position == getData().size()){
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if(getData() == null || !getData().isValid()){
            return 0;
        }

        int addCount = 0;
        if(isShowFooter()){
            addCount = 1;
        }

        return getData().size() + addCount;
    }

    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == FOOTER_VIEW) {
            IFacilityVerticalFooterBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_facility_vertical_footer, viewGroup, false);

            return new ResultViewHolder(binding);
        }

        ICareCoordiFacilityVerticalBinding binding = DataBindingUtil.inflate
                (LayoutInflater.from(viewGroup.getContext()), R.layout.i_care_coordi_facility_vertical, viewGroup, false);
        return new ResultViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultViewHolder holder, int position) {
        OrderedRealmCollection data = getData();
        if (data != null) {
            if(holder.binding instanceof ICareCoordiFacilityVerticalBinding){
                AppLogger.i(getClass(),"search List position: " + position);
                final FacilityRealmModel facility = getData().get(position);
                ICareCoordiFacilityVerticalBinding binding = (ICareCoordiFacilityVerticalBinding)holder.binding;
                binding.iCareCoordiFacilityRatingBar.setVisibility(View.GONE);
                binding.iCareCoordiFacilityRatePoint.setVisibility(View.GONE);
                if (facility.getRatingAvg() > 0) {
                    binding.iCareCoordiFacilityRatingBar.setVisibility(View.VISIBLE);
                    binding.iCareCoordiFacilityRatePoint.setVisibility(View.VISIBLE);
                }

                String ratePointStr = String.format(mRatePointReviewCountFormat,
                        String.valueOf(facility.getRatingAvg()),
                        facility.getReviewCount());
                binding.setRateReviewText(ratePointStr);
                binding.setFacility(facility);
                //binding.setIcon(CommonUtils.getCategoryIconForList(mContext.get(), facility.getType().getCategoryCode()));
                binding.setCallback(mResultNavigator);

                binding.iCareCoordiFacilityPriceDesc.setSelected(false);
                //binding.iCareCoordiFacilityPriceLayout.setSelected(false);
                binding.iCareCoordiFacilityPrice.setSelected(false);
                binding.setPrice("");
                /*if(facility.getType().getCategoryCode().equals("FC-01")){
                    binding.setPrice(mContext.get().getString(R.string.common_no_info));
                    int price = ((FacilityCareCoordiList)facility).getMedicalExpenses();
                    if(price > 0){
                        binding.iCareCoordiFacilityPriceDesc.setSelected(true);
                        binding.iCareCoordiFacilityPrice.setSelected(true);
                        //binding.iCareCoordiFacilityPriceLayout.setSelected(true);
                        price = price * 30;
                        binding.setPrice(mContext.get().getString(R.string.i_care_coordi_facility_price, String.format("%s", UnitUtils.thousandComma(price))));
                    }
                }*/

                binding.iCareCoordiFacilityScaleDesc.setSelected(false);
                binding.iCareCoordiFacilityScale.setSelected(false);
                binding.setScale("");
                /*if(facility.getType().getCategoryCode().equals("FC-02")){
                    binding.setScale(mContext.get().getString(R.string.common_no_info));
                    int scaleCount = ((FacilityCareCoordiList)facility).getScaleCount();
                    if(scaleCount > 0){
                        binding.iCareCoordiFacilityScaleDesc.setSelected(true);
                        binding.iCareCoordiFacilityScale.setSelected(true);
                        binding.setScale(mContext.get().getString(R.string.i_care_coordi_facility_scale, scaleCount));
                    }
                }*/

                double distance = ((FacilityCareCoordiList)facility).getDistance();
                if(distance > 1){
                    binding.setDistance(mContext.get().getString(R.string.common_kilometer, UnitUtils.getFirstDecimalStr((float)distance)));
                }else{
                    int meter = (int)(distance * 1000);
                    binding.setDistance(mContext.get().getString(R.string.common_meter, meter));
                }
                binding.executePendingBindings();
            }else if(holder.binding instanceof IFacilityVerticalFooterBinding){
                IFacilityVerticalFooterBinding binding = (IFacilityVerticalFooterBinding)holder.binding;
                binding.executePendingBindings();
            }
        }
    }

    private int getGradeColor(String gradeStr) {
        return CommonUtils.getColorWithGrade(mContext.get(), gradeStr);
    }

    public class ResultViewHolder extends RecyclerView.ViewHolder {
        final ViewDataBinding binding;

        public ResultViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    /*@BindingAdapter("visibilityWithEvalType")
    public static void setTextVisibility(View view, String evalType){
        if(evalType.equals(FacilityRealmModel.EVAL_TYPE.facility.toString())){
            view.setVisibility(View.VISIBLE);
        }else{
            view.setVisibility(View.GONE);
        }
    }*/
}
