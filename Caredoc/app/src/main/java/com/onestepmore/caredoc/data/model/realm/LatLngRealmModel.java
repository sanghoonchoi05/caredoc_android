package com.onestepmore.caredoc.data.model.realm;

import android.os.Parcelable;

import io.realm.RealmModel;

public interface LatLngRealmModel extends RealmModel, Parcelable {
    void setLatitude(double latitude);

    void setLongitude(double longitude);

    String getId();

    void setId(String id);

    double getLatitude();

    double getLongitude();
}
