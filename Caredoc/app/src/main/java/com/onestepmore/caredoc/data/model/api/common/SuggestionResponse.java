package com.onestepmore.caredoc.data.model.api.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.BasicResponse;
import com.onestepmore.caredoc.data.model.api.object.ServiceSimpleObject;
import com.onestepmore.caredoc.data.model.api.object.ServicesObject;

public final class SuggestionResponse {
    public enum TYPE{
        location,
        facility;
    }
    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("text")
    private String text;

    @Expose
    @SerializedName("value")
    private String value;

    @Expose
    @SerializedName("location")
    private BasicResponse.Location location;

    @Expose
    @SerializedName("slug")
    private String slug;

    @Expose
    @SerializedName("extra_data")
    private ExtraData extraData;


    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }

    public BasicResponse.Location getLocation() {
        return location;
    }

    public TYPE getTypeEnum(){
        TYPE typeEnum = null;
        for(TYPE type : TYPE.values()){
            if(this.type.equals(type.toString())){
                typeEnum = type;
                break;
            }
        }

        return typeEnum;
    }

    public ExtraData getExtraData() {
        return extraData;
    }

    public static final class ExtraData{
        @Expose
        @SerializedName("address")
        private String address;

        @Expose
        @SerializedName("services")
        private ServicesObject<ServiceSimpleObject> services;

        public String getAddress() {
            return address;
        }

        public ServicesObject<ServiceSimpleObject> getServices() {
            return services;
        }
    }
}
