package com.onestepmore.caredoc.ui.main.taker.add.basic;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface TakerBasicNavigator extends BaseNavigator {
    void registerPicture();
}
