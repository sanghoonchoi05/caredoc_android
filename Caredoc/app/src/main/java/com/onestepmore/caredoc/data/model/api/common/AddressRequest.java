package com.onestepmore.caredoc.data.model.api.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressRequest {
    public static class QueryParameter {
        @Expose
        @SerializedName("sidoCode")
        private int sidoCode;

        @Expose
        @SerializedName("gugunCode")
        private int gugunCode;

        public int getSidoCode() {
            return sidoCode;
        }

        public void setSidoCode(int sidoCode) {
            this.sidoCode = sidoCode;
        }

        public int getGugunCode() {
            return gugunCode;
        }

        public void setGugunCode(int gugunCode) {
            this.gugunCode = gugunCode;
        }
    }
}
