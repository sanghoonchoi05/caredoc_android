package com.onestepmore.caredoc.ui.main.taker.add;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.google.gson.Gson;
import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.data.repository.TakerRepository;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class AddTakerActivityModule {
    @Provides
    AddTakerViewModel addTakerViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider,
                                        Gson gson, TakerRepository takerRepository) {
        return new AddTakerViewModel(application, dataSource, schedulerProvider, gson, takerRepository);
    }

    @Provides
    ViewModelProvider.Factory provideAddTakerViewModelProvider(AddTakerViewModel addTakerViewModel) {
        return new ViewModelProviderFactory<>(addTakerViewModel);
    }
}
