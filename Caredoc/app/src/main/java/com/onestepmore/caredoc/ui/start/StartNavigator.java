package com.onestepmore.caredoc.ui.start;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface StartNavigator extends BaseNavigator {
    void openAccountMainActivity();
    void openCareCoordiActivity();
    void openMainActivity(int index);
}
