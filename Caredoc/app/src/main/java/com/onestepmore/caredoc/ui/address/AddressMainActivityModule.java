package com.onestepmore.caredoc.ui.address;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class AddressMainActivityModule {
    @Provides
    AddressMainViewModel addressMainViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new AddressMainViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    ViewModelProvider.Factory provideAddressMainViewModelProvider(AddressMainViewModel addressMainViewModel) {
        return new ViewModelProviderFactory<>(addressMainViewModel);
    }
}
