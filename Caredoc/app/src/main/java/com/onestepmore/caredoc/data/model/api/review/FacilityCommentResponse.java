package com.onestepmore.caredoc.data.model.api.review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.object.CommentObject;

public final class FacilityCommentResponse extends ItemResponse<CommentObject> {
    @Expose
    @SerializedName("rating_average")
    private float ratingAverage;

    public float getRatingAverage() {
        return ratingAverage;
    }
}
