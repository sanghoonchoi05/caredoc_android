package com.onestepmore.caredoc.ui.main.taker.add.address;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class TakerServiceAddressFragmentProvider {
    @ContributesAndroidInjector(modules = TakerServiceAddressFragmentModule.class)
    abstract TakerServiceAddressFragment provideTakerServiceAddressFragmentFactory();
}
