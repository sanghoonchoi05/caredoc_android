package com.onestepmore.caredoc.ui.estimate.request;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class EstimateRequestStartViewModel extends BaseViewModel<EstimateRequestStartNavigator> {

    public EstimateRequestStartViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onClose(){
        getNavigator().onClose();
    }

    public void onStart(){
        getNavigator().onStartEstimateRequest();
    }
}
