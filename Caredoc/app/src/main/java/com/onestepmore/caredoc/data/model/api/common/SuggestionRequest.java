package com.onestepmore.caredoc.data.model.api.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuggestionRequest {
    public static class QueryParameter {
        @Expose
        @SerializedName("keyword")
        private String keyword;

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }
    }
}
