package com.onestepmore.caredoc.ui.main.detail.review;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.onestepmore.caredoc.BR;
import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.amplitude.AmplitudeManager;
import com.onestepmore.caredoc.amplitude.AmplitudeParam;
import com.onestepmore.caredoc.amplitude.MapValue;
import com.onestepmore.caredoc.data.model.api.object.RatingsSummaryObject;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceResponse;
import com.onestepmore.caredoc.data.model.realm.ReviewService;
import com.onestepmore.caredoc.databinding.AUserReviewBinding;
import com.onestepmore.caredoc.databinding.IReviewBinding;
import com.onestepmore.caredoc.ui.base.BaseActivity;
import com.onestepmore.caredoc.ui.main.detail.RatingBarAdapter;
import com.onestepmore.caredoc.ui.support.SpacesItemDecoration;
import com.onestepmore.caredoc.ui.widgets.CommonYNDialog;
import com.onestepmore.caredoc.utils.UIUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.VIEW_REVIEW_CLICK_BACK;
import static com.onestepmore.caredoc.amplitude.AmplitudeEvent.ClickAction.VIEW_REVIEW_CLICK_BACK_KEY;
import static com.onestepmore.caredoc.ui.main.detail.FacilityDetailFragment.RESULT_CODE_REMOVE_REVIEW;

public class ReviewActivity extends BaseActivity<AUserReviewBinding, ReviewViewModel> implements
        ReviewNavigator,
        RemoveReviewListener {
    @Inject
    ViewModelProvider.Factory mViewModelFactory;
    private ReviewViewModel mReviewViewModel;

    private long mServiceFakeId;
    private long mStartTime;
    private int mTotalReviewCount;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.a_user_review;
    }

    @Override
    public ReviewViewModel getViewModel() {
        mReviewViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ReviewViewModel.class);
        return mReviewViewModel;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReviewViewModel.setNavigator(this);

        mStartTime = System.currentTimeMillis();

        setSupportActionBar(getViewDataBinding().aUserReviewToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.btn_back_title);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        Intent intent = getIntent();
        mServiceFakeId = intent.getLongExtra("serviceFakeId", 0L);
        if (mServiceFakeId == 0) {
            finish();
            return;
        }

        getViewModel().loadReviewServiceFromServer(mServiceFakeId);
        initReviewList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                back(VIEW_REVIEW_CLICK_BACK);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        back(VIEW_REVIEW_CLICK_BACK_KEY);
    }

    private void back(String eventName){
        int sec = (int)((System.currentTimeMillis() - mStartTime) / 1000);
        Map<String, MapValue<?>> map = new HashMap<>();
        map.put(AmplitudeParam.Key.COUNT, new MapValue<>(mTotalReviewCount));
        map.put(AmplitudeParam.Key.SECOND, new MapValue<>(sec));
        AmplitudeManager.getInstance().logEvent(eventName, map);
        finish();
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void refresh() {

    }

    @Override
    public void onBack(String tag) {

    }

    @Override
    public void onLoadReviewService(ReviewFacilityServiceResponse response) {
        mTotalReviewCount = response.getTotal();

        UIUtils.setTextIncludeAnnotation(
                getBaseContext(),
                R.string.a_review_desc,
                getViewDataBinding().aUserReviewDesc,
                String.valueOf(response.getTotal()));
    }

    @Override
    public void onCompleteRemoveReview() {
        setResult(RESULT_CODE_REMOVE_REVIEW);
        getViewModel().loadReviewServiceFromServer(mServiceFakeId);
    }

    private void initReviewList() {
        RealmResults<ReviewService> reviewRealmResults = getViewModel().getRealm().where(ReviewService.class)
                .sort("isOwn", Sort.DESCENDING).findAll();

        ReviewAdapter adapter = new ReviewAdapter(reviewRealmResults, this);
        getViewDataBinding().aUserReviewRecyclerView.setAdapter(adapter);
        getViewDataBinding().aUserReviewRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        int spacingInPixels = getBaseContext().getResources().getDimensionPixelSize(R.dimen.i_review_item_space);
        SpacesItemDecoration decoration = new SpacesItemDecoration(spacingInPixels, 0, true);
        getViewDataBinding().aUserReviewRecyclerView.addItemDecoration(decoration);
    }

    @Override
    public void removeReview(long reviewId) {
        showCommonDialog(
                getString(R.string.warning_msg_remove_review_title),
                getString(R.string.warning_msg_remove_review_desc),
                CommonYNDialog.class.getName(),
                (view) -> getViewModel().removeReview(reviewId),
                getString(R.string.common_delete),
                getString(R.string.common_cancel)
        );
    }

    public class ReviewAdapter extends RealmRecyclerViewAdapter<ReviewService, ReviewAdapter.ReviewViewHolder> {

        private RemoveReviewListener mRemoveListener;

        public ReviewAdapter(OrderedRealmCollection<ReviewService> data, RemoveReviewListener listener) {
            super(data, true);
            mRemoveListener = listener;
        }

        @NonNull
        @Override
        public ReviewAdapter.ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            IReviewBinding binding = DataBindingUtil.inflate
                    (LayoutInflater.from(viewGroup.getContext()), R.layout.i_review, viewGroup, false);
            return new ReviewAdapter.ReviewViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(@NonNull ReviewAdapter.ReviewViewHolder reviewViewHolder, int position) {
            OrderedRealmCollection data = getData();
            if (data != null) {
                final ReviewService review = getData().get(position);
                reviewViewHolder.binding.iReviewContentLayout.setSelected(review.isOwn());
                reviewViewHolder.binding.setReview(review);
                reviewViewHolder.binding.iReviewRemove.setOnClickListener((view) -> mRemoveListener.removeReview(review.getId()));
                initRatingBar(reviewViewHolder.binding.iReviewRatingBarRecyclerView, review);
                reviewViewHolder.binding.executePendingBindings();
            }
        }

        private void initRatingBar(RecyclerView recyclerView, ReviewService reviewService) {
            List<RatingsSummaryObject.Item> ratings = new ArrayList<>();
            for (int i = 0; i < reviewService.getLabelList().size(); i++) {
                RatingsSummaryObject.Item item = new RatingsSummaryObject.Item();
                item.setLabel(reviewService.getLabelList().get(i));
                item.setValue(reviewService.getRatingList().get(i));
                ratings.add(item);
            }

            RatingBarAdapter adapter = new RatingBarAdapter(ratings);
            recyclerView.setAdapter(adapter);
            GridLayoutManager manager = new GridLayoutManager(getBaseContext(), 2);
            recyclerView.setLayoutManager(manager);

            int verticalSpacingInPixels = getResources().getDimensionPixelSize(R.dimen.i_user_review_rating_bar_item_space);
            recyclerView.addItemDecoration(new SpacesItemDecoration(verticalSpacingInPixels, 0));
        }

        class ReviewViewHolder extends RecyclerView.ViewHolder {
            final IReviewBinding binding;

            private ReviewViewHolder(IReviewBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}
