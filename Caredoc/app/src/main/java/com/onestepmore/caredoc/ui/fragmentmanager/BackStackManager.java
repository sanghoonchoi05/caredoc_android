package com.onestepmore.caredoc.ui.fragmentmanager;

import android.support.annotation.AnimRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.onestepmore.caredoc.R;
import com.onestepmore.caredoc.ui.base.BaseActivity;

import java.util.LinkedList;
import java.util.ListIterator;
import java.util.UUID;

public class BackStackManager {

    public static BackStackManager mInstance;
    //region Members
    private Fragment mMenuFragment;
    /** Reference to the made up backstack */
    private final LinkedList<BackStack> mBackStacks;

    @AnimRes private int rootSwitchEnterAnim;
    @AnimRes private int rootSwitchExitAnim;
    @AnimRes private int rootEnterAnim;
    @AnimRes private int rootExitAnim;
    @AnimRes private int rootBackEnterAnim;
    @AnimRes private int rootBackExitAnim;
    @AnimRes private int childEnterAnim;
    @AnimRes private int childExitAnim;
    @AnimRes private int childBackEnterAnim;
    @AnimRes private int childBackExitAnim;

    public void setRootSwitchAnim(@AnimRes int rootSwitchEnterAnim, @AnimRes int rootSwitchExitAnim) {
        this.rootSwitchEnterAnim = rootSwitchEnterAnim;
        this.rootSwitchExitAnim = rootSwitchExitAnim;
    }

    public void setRootAnim(@AnimRes int rootEnterAnim, @AnimRes int rootExitAnim) {
        this.rootEnterAnim = rootEnterAnim;
        this.rootExitAnim = rootExitAnim;
    }

    public void setChildAnim(@AnimRes int childEnterAnim, @AnimRes int childExitAnim) {
        this.childEnterAnim = childEnterAnim;
        this.childExitAnim = childExitAnim;
    }

    public void setRootBackAnim(@AnimRes int rootBackEnterAnim, @AnimRes int rootBackExitAnim) {
        this.rootBackEnterAnim = rootBackEnterAnim;
        this.rootBackExitAnim = rootBackExitAnim;
    }

    public void setChildBackAnim(@AnimRes int childBackEnterAnim, @AnimRes int childBackExitAnim) {
        this.childBackEnterAnim = childBackEnterAnim;
        this.childBackExitAnim = childBackExitAnim;
    }

    //endregion

    public static BackStackManager getInstance() {
        if(mInstance == null){
            mInstance = new BackStackManager();
        }
        return mInstance;
    }

    //region Constructors
    public BackStackManager() {
        mBackStacks = new LinkedList<>();
    }
    //endregion


    /*public void replaceMenuFragment(@NonNull final Fragment fragment,
                                    final int layoutId, @AnimRes final int enterAni, @AnimRes final int exitAni) {

        if(mMenuFragment == fragment){
            return;
        }
        else{
            removeAllBackStack();
        }

        mMenuFragment = fragment;
        mFragmentManager.replaceMenuFragment(fragment, layoutId);
    }*/

    public void removeAllFragment(){
        removeAllBackStack();
    }

    /** When adding a new root fragment
     * IMPORTANT: Activity his holding the reference to the root. */
    public void addRootFragment(@NonNull final BaseActivity baseActivity, @NonNull final Fragment fragment,
                                final int layoutId) {
        addRootFragment(baseActivity, fragment, layoutId, false);
    }

    public void addRootFragment(@NonNull final BaseActivity baseActivity, @NonNull final Fragment fragment,
                                final int layoutId, boolean alwaysShowRoot) {
        if (!isAdded(fragment)) {
            addRoot(baseActivity, fragment, layoutId, alwaysShowRoot);
        }
        else if (isAdded(fragment) && isCurrent(fragment)) {
            refreshCurrentRoot();
        }
        else {
            switchRoot(fragment);
        }
    }

    /** When activity is calling onBackPressed */
    public void onBackPressed() {
        onBackPressed(0,0);
    }

    /** When activity is calling onBackPressed */
    public void onBackPressed(@AnimRes final int enterAni, @AnimRes final int exitAni) {
        final BackStack current = mBackStacks.peekLast();
        if(current != null){
            final String uuid = current.pop();

            if (uuid == null) {
                removeRoot(current);
            }
            else {
                current.mRootActivity.onBack(uuid);
                current.mFragmentManager.removeChild(current, uuid, rootBackEnterAnim, childBackEnterAnim, childBackExitAnim);

            }
        }
    }

    /** Adding child fragment */
    public void addChildFragment(@NonNull final Fragment fragment,
                                 final int layoutId) {

        final String uuid = UUID.randomUUID().toString();
        //final String uuid = fragment.getClass().getName();
        final BackStack backStack = mBackStacks.peekLast();

        backStack.push(uuid);

        backStack.mFragmentManager.addChildFragment(fragment, layoutId, uuid, childEnterAnim, childExitAnim);

        if(backStack.isAlwaysShowRoot()){
            backStack.mFragmentManager.showFragment(backStack.mRootFragment, 0, 0);
        }
    }

    /** Remove root */
    private void removeRoot(@NonNull final BackStack backStack) {
        mBackStacks.remove(backStack);

        //After removing. Call close app listener if the backstack is empty
        if (mBackStacks.isEmpty()) {
            backStack.mFragmentManager.removeRoot(backStack, rootExitAnim);
            backStack.mRootActivity.onFinish();
        }
        //Change root since the old one is out
        else {
            BackStack newRoot = mBackStacks.peekLast();
            if(newRoot.mRootActivity != backStack.mRootActivity){
                backStack.mRootActivity.onFinish();
            }

            backStack.mFragmentManager.switchFragment(
                    newRoot.mFragmentManager.getLastFragment(),
                    backStack.mFragmentManager.getLastFragment(),
                    rootSwitchEnterAnim,
                    rootSwitchExitAnim);

            if(newRoot.isAlwaysShowRoot()){
                newRoot.mFragmentManager.showFragment(newRoot.mRootFragment, rootEnterAnim, rootExitAnim);
            }

            if(backStack.isAlwaysShowRoot()){
                backStack.mFragmentManager.hideFragment(backStack.mRootFragment);
            }

            newRoot.mRootActivity.onBack(newRoot.mRootFragment.getTag());
        }
    }

    /** Adding root fragment */
    private void addRoot(@NonNull final BaseActivity baseActivity, @NonNull final Fragment fragment, final int layoutId, boolean alwaysShowRoot) {
        final BackStack currentBackStack = mBackStacks.peekLast();
        if(currentBackStack != null){
            currentBackStack.mFragmentManager.hideLastFragment();
            if(currentBackStack.isAlwaysShowRoot()){
                currentBackStack.mFragmentManager.hideFragment(currentBackStack.mRootFragment);
            }
        }

        //Create a new backstack and add it to the list
        final BackStack backStack = new BackStack(
                baseActivity,
                new BackStackFragmentManager(baseActivity.getSupportFragmentManager()),
                fragment,
                alwaysShowRoot);
        mBackStacks.offerLast(backStack);
        backStack.mFragmentManager.addFragment(fragment, layoutId, rootEnterAnim, rootExitAnim);
    }

    /** Switch root internally in the made up backstack */
    private void switchRoot(@NonNull final Fragment rootFragment) {
        final BackStack currentBackStack = mBackStacks.peekLast();

        for (int i = 0; i < mBackStacks.size(); i++) {
            BackStack backStack = mBackStacks.get(i);
            if (backStack.mRootFragment == rootFragment) {
                mBackStacks.remove(i);
                mBackStacks.offerLast(backStack);

                // 루트 프래그먼트 스위치 시 바뀔 프래그먼트의 마지막 프래그먼트로
                currentBackStack.mFragmentManager.switchFragment(
                        backStack.mFragmentManager.getLastFragment(),
                        currentBackStack.mFragmentManager.getLastFragment(),
                        rootSwitchEnterAnim,
                        rootSwitchExitAnim);

                if(backStack.isAlwaysShowRoot()){
                    backStack.mFragmentManager.showFragment(backStack.mRootFragment, rootEnterAnim, rootExitAnim);
                }

                if(currentBackStack.isAlwaysShowRoot()){
                    currentBackStack.mFragmentManager.hideFragment(currentBackStack.mRootFragment);
                }
                break;
            }
        }
    }

    /** Let listener know to call refresh */
    private void refreshCurrentRoot() {
        final BackStack backStack = mBackStacks.peekLast();
        backStack.mRootActivity.refresh();
    }

    /** Convenience method */
    private boolean isAdded(@NonNull final Fragment fragment) {
        for (BackStack backStack : mBackStacks) {
            if (backStack.mRootFragment == fragment) {
                return true;
            }
        }
        return false;
    }

    /** Convenience method */
    private boolean isCurrent(@NonNull final Fragment fragment) {
        final BackStack backStack = mBackStacks.peekLast();
        return backStack.mRootFragment == fragment;
    }

    public Fragment getCurrentFragment(){
        final BackStack backStack = mBackStacks.peekLast();
        if(backStack != null){
            return backStack.mFragmentManager.getLastFragment();
        }
        else{
            return mMenuFragment;
        }
    }

    public BackStack getCurrentBackStack(){
        return mBackStacks.peekLast();
    }

    public int getBackStackCount() { return mBackStacks.size(); }

    public Fragment getAddedRootFragment(String tag){
        for (BackStack backStack : mBackStacks) {
            Fragment fragment = backStack.mRootFragment;
            if (fragment.getTag().equals(tag)) {
                return fragment;
            }
        }

        return null;
    }

    public void removeAllBackStack(){
        for(BackStack backStack : mBackStacks){
            backStack.removeChildAll();
            backStack.mFragmentManager.removeAllFragment(true);
            backStack = null;
        }

        mBackStacks.clear();
    }

    public void removeBackStackInCurrentActivity(){
        if(mBackStacks.size() == 0){
            return;
        }
        BackStack curBackStack = mBackStacks.peekLast();
        curBackStack.mFragmentManager.removeAllChild(curBackStack);
        removeRoot(curBackStack);
    }

    public void setFragmentFadeAnimation(){
        BackStackManager.getInstance().setRootSwitchAnim(R.anim.fade_in, R.anim.fade_out);
        BackStackManager.getInstance().setRootAnim(R.anim.fade_in, R.anim.fade_out);
        BackStackManager.getInstance().setRootBackAnim(R.anim.fade_in, R.anim.fade_out);
        BackStackManager.getInstance().setChildAnim(R.anim.fade_in, R.anim.fade_out);
        BackStackManager.getInstance().setChildBackAnim(R.anim.fade_in, R.anim.fade_out);
    }

    public void setFragmentSlideAnimation(){
        BackStackManager.getInstance().setRootSwitchAnim(R.anim.fade_in, R.anim.fade_out);
        BackStackManager.getInstance().setRootAnim(R.anim.slide_left_in, R.anim.slide_left_out);
        BackStackManager.getInstance().setRootBackAnim(R.anim.slide_right_in, R.anim.slide_right_out);
        BackStackManager.getInstance().setChildAnim(R.anim.slide_left_in, R.anim.slide_left_out);
        BackStackManager.getInstance().setChildBackAnim(R.anim.slide_right_in, R.anim.slide_right_out);
    }
    //endregion
}
