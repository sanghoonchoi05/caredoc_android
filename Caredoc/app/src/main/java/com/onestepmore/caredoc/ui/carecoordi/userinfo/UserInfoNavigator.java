package com.onestepmore.caredoc.ui.carecoordi.userinfo;

import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface UserInfoNavigator extends BaseNavigator {
    void onShowTakerCardView();
    void onNewAddress();
}
