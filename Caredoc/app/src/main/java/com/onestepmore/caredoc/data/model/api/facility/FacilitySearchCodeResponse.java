package com.onestepmore.caredoc.data.model.api.facility;

import com.onestepmore.caredoc.data.model.api.ItemResponse;
import com.onestepmore.caredoc.data.model.api.ListResponse;
import com.onestepmore.caredoc.data.model.api.object.FacilitySimpleObject;

public final class FacilitySearchCodeResponse extends ItemResponse<FacilitySimpleObject> {
}
