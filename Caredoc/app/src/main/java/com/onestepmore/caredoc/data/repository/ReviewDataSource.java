package com.onestepmore.caredoc.data.repository;

import com.onestepmore.caredoc.data.remote.ReviewApiHelper;

public interface ReviewDataSource extends ReviewApiHelper {
}
