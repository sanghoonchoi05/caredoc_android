package com.onestepmore.caredoc.ui.carecoordi.service;

import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;

import com.onestepmore.caredoc.ViewModelProviderFactory;
import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceFragmentModule {
    @Provides
    ServiceViewModel serviceViewModel(Application application, AppDataSource dataSource, SchedulerProvider schedulerProvider) {
        return new ServiceViewModel(application, dataSource, schedulerProvider);
    }

    @Provides
    @Named("ServiceFragment")
    ViewModelProvider.Factory provideServiceViewModelProvider(ServiceViewModel serviceViewModel) {
        return new ViewModelProviderFactory<>(serviceViewModel);
    }
}
