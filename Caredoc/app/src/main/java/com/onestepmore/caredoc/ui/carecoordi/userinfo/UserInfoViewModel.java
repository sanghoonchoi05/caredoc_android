package com.onestepmore.caredoc.ui.carecoordi.userinfo;

import android.app.Application;
import android.databinding.ObservableField;

import com.onestepmore.caredoc.data.repository.AppDataSource;
import com.onestepmore.caredoc.ui.base.BaseViewModel;
import com.onestepmore.caredoc.utils.rx.SchedulerProvider;

public class UserInfoViewModel extends BaseViewModel<UserInfoNavigator>{

    public UserInfoViewModel(Application application, AppDataSource appDataSource, SchedulerProvider schedulerProvider) {
        super(application, appDataSource, schedulerProvider);
    }

    public void onLoadTaker(){
        getNavigator().onShowTakerCardView();
    }

    public void onNewAddress(){
        getNavigator().onNewAddress();
    }
}
