package com.onestepmore.caredoc.data.model.api.statics;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.onestepmore.caredoc.data.model.api.ItemResponse;

public final class SubjectResponse extends ItemResponse<SubjectResponse.Item> {
    public final class Item{
        @Expose
        @SerializedName("type_code")
        private String typeCode;
        @Expose
        @SerializedName("type_name")
        private String typeName;

        public String getTypeCode() {
            return typeCode;
        }

        public String getTypeName() {
            return typeName;
        }
    }
}
