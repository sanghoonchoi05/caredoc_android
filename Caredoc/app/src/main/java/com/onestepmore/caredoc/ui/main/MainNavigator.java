package com.onestepmore.caredoc.ui.main;

import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.Service;
import com.onestepmore.caredoc.ui.base.BaseNavigator;

public interface MainNavigator extends BaseNavigator {
    enum FROM{
        SEARCH_FACILITY_LIST,
        SEARCH_FACILITY_MAP,
        BOOKMARK_DETAIL,
        PROFILE_RECENTLY,
        RECENTLY,
    }

    enum TOUCH{
        SERVICE,
        PICTURE,
        ITEM
    }
    void onFacilityDetailClick(FacilityRealmModel facility, FROM from, TOUCH touch);
    void onFacilityDetailClick(FacilityRealmModel facility, FROM from, Service touchService);
    void showFacilityDetail(FacilityRealmModel facility);
    void showChannelTalk();
}
