package com.onestepmore.caredoc.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailEntranceResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilityDetailMedicalResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchMapResponse;
import com.onestepmore.caredoc.data.model.api.facility.FacilitySearchResponse;
import com.onestepmore.caredoc.data.model.api.object.FacilitySearchMapObject;
import com.onestepmore.caredoc.data.model.api.object.SearchFilter;
import com.onestepmore.caredoc.data.model.api.object.SearchMapFilter;
import com.onestepmore.caredoc.data.model.api.object.ServiceEntranceObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceMedicalObject;
import com.onestepmore.caredoc.data.model.api.object.ServiceObject;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchTemp;
import com.onestepmore.caredoc.utils.DateUtils;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import io.restassured.mapper.ObjectMapperType;

import static com.onestepmore.caredoc.BasicApp.UNRELIABLE_INTEGER_FACTORY;
import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_FACILITY_DETAIL;
import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_FACILITY_SEARCH;
import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_FACILITY_SEARCH_MAP;
import static io.restassured.RestAssured.given;

public class FacilityApiTest extends ApiTest {

    private Gson mGson;

    @Before
    @Override
    public void setUp() throws Exception {
        mGson = new GsonBuilder()
                .setDateFormat(DateUtils.STANDARD_DATE_TIME_FORMAT)
                .registerTypeAdapterFactory(UNRELIABLE_INTEGER_FACTORY)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();
    }

    @After
    @Override
    public void tearDown() throws Exception {
    }

    /**
     * 시설찾기 리스트 API 테스트
     */
    @Test
    public void testFacilitySearchListApi() {
        List<SearchFilter.Service> serviceList = new ArrayList<>();
        SearchFilter.Service service = new SearchFilter.Service();
        service.setGrades(null);
        service.setId(1);
        serviceList.add(service);

        SearchFilter.SearchFilterBuilder filter = new SearchFilter.SearchFilterBuilder()
                .addAddressCode(null)
                .addServices(serviceList)
                .addRating(0);

        FacilitySearchResponse response = given()
                .queryParam("filter", mGson.toJson(filter.build()))
                .queryParam("orderBy", "grade")
                .queryParam("skip", 0)
                .queryParam("take", 10)
                .when()
                .get(ENDPOINT_FACILITY_SEARCH).as(FacilitySearchResponse.class, ObjectMapperType.GSON);

        Assert.assertNotNull(response.getItems());
        Assert.assertEquals(response.getItems().size(), 10);

        FacilitySearchList facility = new FacilitySearchList();
        facility.setFacility(response.getItems().get(0));
    }

    /**
     * 시설찾기 맵 API 테스트
     */
    @Test
    public void testFacilitySearchMapApi() {
        List<SearchMapFilter.Service> serviceList = new ArrayList<>();
        SearchMapFilter.Service service = new SearchMapFilter.Service();
        service.setGrades(null);
        service.setId(1);
        serviceList.add(service);

        SearchMapFilter.SearchMapFilterBuilder filter = new SearchMapFilter.SearchMapFilterBuilder()
                .addAddressCode(null)
                .addServices(serviceList)
                .addSwNe("37.06116351637433,126.84082629687498", "37.94253233419054,127.39014270312498")
                .addZoom(14)
                .addSearchMode("map")
                .addRating(0);

        FacilitySearchMapResponse response = given()
                .queryParam("filter", mGson.toJson(filter.build()))
                .when()
                .get(ENDPOINT_FACILITY_SEARCH_MAP).as(FacilitySearchMapResponse.class, ObjectMapperType.GSON);

        Assert.assertNotNull(response.getItems());
        Assert.assertTrue(response.getItems().size() > 0);
        Assert.assertEquals(response.getItems().get(0).getGroupBy(), FacilitySearchMapObject.GROUP_BY.MARKER.getText());
        Assert.assertTrue(response.getItems().get(0).getItems().size() > 0);
    }

    /**
     * 시설상세 요양병원 API 테스트
     * ex) 대구명성요양병원
     */
    @Test
    public void testFacilityMedicalDetailApi() {
        FacilityDetailMedicalResponse response = getFacilityDetailResponse(FacilityDetailMedicalResponse.class, 1743964097);

        Assert.assertNotNull(response);

        FacilitySearchTemp facilitySearchTemp = new FacilitySearchTemp();
        facilitySearchTemp.setFacility(response);

        Assert.assertTrue(response.getServices().getItems().size() > 0);
        for(ServiceMedicalObject service : response.getServices().getItems()){
            Assert.assertEquals(service.getServiceCategory(), ServiceObject.CATEGORY.GERIATRIC_HOSPITAL.getName());
            Assert.assertNotNull(service);
            Assert.assertNotNull(service.getData());
            Assert.assertNotNull(service.getData().getMedicalSubjectObject());
            Assert.assertNotNull(service.getData().getPrograms());
            Assert.assertNotNull(service.getData().getSubs());
            Assert.assertNotNull(service.getData().getEquipment());
            Assert.assertNotNull(service.getData().getEmployees());
            Assert.assertNotNull(service.getData().getTimeTables());
            Assert.assertNotNull(service.getData().getEvaluationsMedicalObject());
            Assert.assertNotNull(service.getData().getMealAddOnsObject());
            Assert.assertNotNull(service.getData().getNursingAddOnsObject());
        }
    }

    /**
     * 시설상세 요양원 API 테스트
     * ex) 덕인노인전문요양원
     */
    @Test
    public void testFacilityEntranceDetailApi() {
        FacilityDetailEntranceResponse response = getFacilityDetailResponse(FacilityDetailEntranceResponse.class, 544266281);

        Assert.assertNotNull(response);

        FacilitySearchTemp facilitySearchTemp = new FacilitySearchTemp();
        facilitySearchTemp.setFacility(response);

        Assert.assertTrue(response.getServices().getItems().size() > 0);
        for(ServiceEntranceObject service : response.getServices().getItems()){
            Assert.assertEquals(service.getServiceCategory(), ServiceObject.CATEGORY.NURSING_HOME.getName());
            Assert.assertNotNull(service);
            Assert.assertNotNull(service.getData());
            Assert.assertNotNull(service.getData().getUnpaidItems());
            Assert.assertNotNull(service.getData().getPrograms());
            Assert.assertNotNull(service.getData().getSubs());
            Assert.assertNotNull(service.getData().getEquipment());
            Assert.assertNotNull(service.getData().getEmployees());
            Assert.assertNotNull(service.getData().getTimeTables());
        }
    }

    /**
     * 시설상세 재가 API 테스트
     * ex) 상주우석노인복지센터
     */
    @Test
    public void testFacilityVisitCareDetailApi() {
        FacilityDetailEntranceResponse response = getFacilityDetailResponse(FacilityDetailEntranceResponse.class,1906474306);

        Assert.assertNotNull(response);
        FacilitySearchTemp facilitySearchTemp = new FacilitySearchTemp();
        facilitySearchTemp.setFacility(response);

        Assert.assertTrue(response.getServices().getItems().size() > 0);
        for(ServiceEntranceObject service : response.getServices().getItems()){
            Assert.assertEquals(service.getServiceCategory(), ServiceObject.CATEGORY.VISIT_HOME.getName());
            Assert.assertNotNull(service);
            Assert.assertNotNull(service.getData());
            Assert.assertNotNull(service.getData().getUnpaidItems());
            Assert.assertNotNull(service.getData().getSubs());
            Assert.assertNotNull(service.getData().getEquipment());
            Assert.assertNotNull(service.getData().getEmployees());
            Assert.assertNotNull(service.getData().getTimeTables());
        }
    }

    private <T> T getFacilityDetailResponse(Class<T> cls, long id) {
        return given()
                .pathParam("id", id)
                .when()
                .get(ENDPOINT_FACILITY_DETAIL).as(cls, ObjectMapperType.GSON);
    }
}