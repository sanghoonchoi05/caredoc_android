package com.onestepmore.caredoc.api;

import com.google.gson.reflect.TypeToken;
import com.onestepmore.caredoc.data.model.api.common.AddressResponse;
import com.onestepmore.caredoc.data.model.api.common.SuggestionResponse;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import io.restassured.mapper.ObjectMapperType;

import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_SERVER_ADDRESS_V2;
import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_SERVER_SUGGESTION;
import static io.restassured.RestAssured.given;

public class CommonApiTest extends ApiTest {

    @Before
    @Override
    public void setUp() throws Exception {
    }

    @After
    @Override
    public void tearDown() throws Exception {
    }


    @Test
    public void testAddressV2Api(){
        AddressResponse response = given()
                .queryParam("sidoCode", 44)
                .queryParam("gugunCode", 0)
                .when()
                .get(ENDPOINT_SERVER_ADDRESS_V2)
                .as(AddressResponse.class, ObjectMapperType.GSON);

        Assert.assertTrue(response.getItems().size() > 0);
    }

    @Test
    public void testSuggestionApi(){
        List<SuggestionResponse> response = given()
                .queryParam("keyword", "방이")
                .queryParam("service[]", 1)
                .when()
                .get(ENDPOINT_SERVER_SUGGESTION)
                .as(new TypeToken<List<SuggestionResponse>>(){}.getType(), ObjectMapperType.GSON);
        Assert.assertTrue(response.size() > 0);
    }
}