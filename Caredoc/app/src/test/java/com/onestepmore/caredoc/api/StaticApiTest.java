package com.onestepmore.caredoc.api;

import com.onestepmore.caredoc.data.model.api.object.SearchCategoriesObject;
import com.onestepmore.caredoc.data.model.api.object.StaticServiceObject;
import com.onestepmore.caredoc.data.model.api.statics.DiseaseResponse;
import com.onestepmore.caredoc.data.model.api.statics.StaticDataResponse;
import com.onestepmore.caredoc.data.model.api.statics.SubjectResponse;
import com.onestepmore.caredoc.data.model.realm.statics.StaticCategory;
import com.onestepmore.caredoc.data.model.realm.statics.StaticService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import io.restassured.mapper.ObjectMapperType;

import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_SERVER_DISEASE;
import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_SERVER_STATICS;
import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_SERVER_SUBJECT;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

public class StaticApiTest extends ApiTest {

    @Before
    @Override
    public void setUp() throws Exception {
    }

    @After
    @Override
    public void tearDown() throws Exception {
    }


    @Test
    public void testStaticApi(){
        StaticDataResponse response = given()
                .when()
                .get(ENDPOINT_SERVER_STATICS).as(StaticDataResponse.class, ObjectMapperType.GSON);

        Assert.assertNotNull(response.getAggregates());
        Assert.assertNotNull(response.getCategories());
        Assert.assertNotNull(response.getServices());

        Assert.assertTrue(response.getCategories().getItems().size() > 0);
        Assert.assertTrue(response.getServices().getItems().size() > 0);
        Assert.assertTrue(response.getAggregates().getFacilityTotal() > 0);

        checkCategory(response.getCategories());
        checkService(response.getServices().getItems());
    }

    private void checkCategory(SearchCategoriesObject searchCategoriesObject){
        // 중복된 카테고리가 있는 지 체크
        List<String> categoryNameList = new ArrayList<>();

        // Realm Data Model 체크
        for(SearchCategoriesObject.Item object : searchCategoriesObject.getItems()){
            Assert.assertNotNull(object.getText());
            Assert.assertNotNull(object.getGrades());
            Assert.assertTrue(object.getGrades().size() > 0);
            Assert.assertNotNull(object.getQuery());
            Assert.assertNotNull(object.getQuery().getServiceTypeIds());
            Assert.assertTrue(object.getQuery().getServiceTypeIds().size() > 0);
            StaticCategory category = new StaticCategory();
            category.setCategory(object);
            categoryNameList.add(category.getText());
        }

        Assert.assertThat(searchCategoriesObject.getItems().size(), is(categoryNameList.size()));

        // 중복 id는 중복된 값을 저장하지 않는 HashSet으로 검사하자.
        Assert.assertEquals(new HashSet<>(categoryNameList).size(), categoryNameList.size());
    }

    private void checkService(List<StaticServiceObject> serviceObjectList){
        Assert.assertEquals(StaticService.SERVICE_TYPE.values().length - 1, serviceObjectList.size());
        for(StaticServiceObject service : serviceObjectList){
            StaticService.SERVICE_TYPE type = Arrays.stream(StaticService.SERVICE_TYPE.values())
                    .filter(serviceType -> service.getId() == service.getId())
                    .findAny()
                    .orElse(null);

            Assert.assertNotNull(type);
        }
    }

    @Test
    public void testMedicalDiseaseApi(){
        DiseaseResponse response = given()
                .when()
                .get(ENDPOINT_SERVER_DISEASE).as(DiseaseResponse.class, ObjectMapperType.GSON);

        //.get(ENDPOINT_SERVER_DISEASE).as(new TypeToken<List<DiseaseResponse>>(){}.getType(), ObjectMapperType.GSON);

        Assert.assertNotNull(response);
        Assert.assertTrue(response.getItems().size() > 0);
    }

    @Test
    public void testMedicalSubjectApi(){
        SubjectResponse response = given()
                .when()
                .get(ENDPOINT_SERVER_SUBJECT).as(SubjectResponse.class, ObjectMapperType.GSON);

        Assert.assertNotNull(response);
        Assert.assertTrue(response.getItems().size() > 0);
    }
}