package com.onestepmore.caredoc.api;

import com.onestepmore.caredoc.data.model.api.object.ReviewServiceObject;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityServiceSummaryResponse;
import com.onestepmore.caredoc.data.model.realm.ReviewService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.restassured.mapper.ObjectMapperType;

import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_GET_REVIEW_SUMMARY_V2;
import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_GET_REVIEW_V2;
import static io.restassured.RestAssured.given;

public class ReviewApiTest extends ApiTest {

    @Before
    @Override
    public void setUp() throws Exception {
    }

    @After
    @Override
    public void tearDown() throws Exception {
    }

    /**
     * 후기 목록 API 테스트
     */
    @Test
    public void testReviewListApi() {
        ReviewFacilityServiceResponse response = given()
                .pathParam("morphType", ReviewFacilityRequest.TYPE.facility_service.getType())
                .pathParam("morphId", 167753729)
                .when()
                .get(ENDPOINT_GET_REVIEW_V2).as(ReviewFacilityServiceResponse.class, ObjectMapperType.GSON);

        Assert.assertTrue(response.getItems().size() > 0);
        for(ReviewServiceObject review : response.getItems()){
            Assert.assertNotNull(review.getRatings());
            Assert.assertTrue(review.getRatings().getItems().size() > 0);
            ReviewService reviewService = new ReviewService();
            reviewService.setReview(review);
        }
    }

    /**
     * 후기 목록 API 테스트
     */
    @Test
    public void testReviewSummeryListApi() {
        ReviewFacilityServiceSummaryResponse response = given()
                .pathParam("morphType", ReviewFacilityRequest.TYPE.facility_service.getType())
                .pathParam("morphId", 167753729)
                .when()
                .get(ENDPOINT_GET_REVIEW_SUMMARY_V2).as(ReviewFacilityServiceSummaryResponse.class, ObjectMapperType.GSON);

        Assert.assertNotNull(response.getRatings());
        Assert.assertTrue(response.getRatings().size() > 0);
    }
}