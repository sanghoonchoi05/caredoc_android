package com.onestepmore.caredoc.ui.main.home;

import android.app.Application;

import com.onestepmore.caredoc.data.repository.AppDataRepository;
import com.onestepmore.caredoc.data.repository.FacilityRepository;
import com.onestepmore.caredoc.utils.rx.TestSchedulerProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.schedulers.TestScheduler;

@RunWith(MockitoJUnitRunner.class)
public class HomeViewModelTest {
    @Mock
    private FacilityRepository mFacilityRepository;

    @Mock
    private AppDataRepository mAppDataRepository;

    @Mock
    private Application mApplication;

    @Mock
    private HomeNavigator mHomeNavigator;

    private HomeViewModel mHomeViewModel;
    private TestScheduler mTestScheduler;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mTestScheduler = new TestScheduler();
        TestSchedulerProvider testSchedulerProvider = new TestSchedulerProvider(mTestScheduler);

        mHomeViewModel = new HomeViewModel(
                mApplication,
                mAppDataRepository,
                testSchedulerProvider,
                mFacilityRepository
        );

        mHomeViewModel.setNavigator(mHomeNavigator);
    }

    @After
    public void tearDown() throws Exception {
        mTestScheduler = null;
        mHomeViewModel = null;
    }

    @Test
    public void fetchValidDataShouldLoadIntoView() {
        /*FacilitySearchResponse response = new FacilitySearchResponse();
        doReturn(Flowable.just(response))
                .when(mFacilityRepository)
                .getFacilitySearchMainApiCall();*/

        /*mHomeViewModel.loadFacilitySearchMainFromServer();
        mTestScheduler.triggerActions();

        verify(mHomeNavigator, never()).handleError(any());
        verify(mHomeNavigator, never()).networkError(anyString());*/
    }

    @Test
    public void testLoadFacilitySearchMainFromServer(){
        /*given().header("CAREDOC-API-KEY", BuildConfig.API_KEY).when();
        FacilitySearchResponse response = get(ENDPOINT_FACILITY_SEARCH_MAIN).as(FacilitySearchResponse.class, ObjectMapperType.GSON);

        Assert.assertNotNull(response.getItems());
        Assert.assertTrue(response.getItems().size() > 0);

        FacilitySearchTemp facility = new FacilitySearchTemp();
        facility.setFacility(response.getItems().get(0));*/
    }
}