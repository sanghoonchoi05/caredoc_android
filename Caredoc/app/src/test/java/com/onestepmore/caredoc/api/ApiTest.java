package com.onestepmore.caredoc.api;

import com.onestepmore.caredoc.BuildConfig;

import org.junit.After;
import org.junit.Before;

import java.util.HashMap;
import java.util.Map;

import io.restassured.authentication.AuthenticationScheme;
import io.restassured.internal.http.HTTPBuilder;

public abstract class ApiTest implements AuthenticationScheme {

    @Before
    protected abstract void setUp() throws Exception;

    @After
    protected abstract void tearDown() throws Exception;

    @Override
    public void authenticate(HTTPBuilder httpBuilder) {
        Map<String, String> map = new HashMap<>();
        map.put("CAREDOC-API-KEY", BuildConfig.API_KEY);
        httpBuilder.setHeaders(map);
    }
}