package com.onestepmore.caredoc.api;

import com.onestepmore.caredoc.data.model.api.object.CommentObject;
import com.onestepmore.caredoc.data.model.api.review.FacilityCommentResponse;
import com.onestepmore.caredoc.data.model.api.review.ReviewFacilityRequest;
import com.onestepmore.caredoc.data.model.realm.Review;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import io.restassured.mapper.ObjectMapperType;

import static com.onestepmore.caredoc.data.remote.ApiEndPoint.ENDPOINT_GET_COMMENT;
import static io.restassured.RestAssured.given;

public class CommentApiTest extends ApiTest {

    @Before
    @Override
    public void setUp() throws Exception {
    }

    @After
    @Override
    public void tearDown() throws Exception {
    }

    /**
     * 댓글 목록 API 테스트
     */
    @Test
    public void testCommentListApi() {
        FacilityCommentResponse response = given()
                .pathParam("morphType", ReviewFacilityRequest.TYPE.facility.getType())
                .pathParam("morphId", 1699363414)
                .when()
                .get(ENDPOINT_GET_COMMENT).as(FacilityCommentResponse.class, ObjectMapperType.GSON);

        Assert.assertNotNull(response.getItems());
        for(CommentObject comment : response.getItems()){
            Review review = new Review();
            review.setReview(comment);
        }
    }
}