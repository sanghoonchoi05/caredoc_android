package com.onestepmore.caredoc;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.maps.model.LatLng;
import com.onestepmore.caredoc.data.model.realm.FacilityRealmModel;
import com.onestepmore.caredoc.data.model.realm.FacilitySearchList;
import com.onestepmore.caredoc.ui.main.detail.FacilityActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.onestepmore.caredoc.ui.main.detail.FacilityActivity.EXTRA_FACILITY;

@RunWith(AndroidJUnit4.class)
public class FacilityActivityTest{

    FacilityRealmModel facility = null;

    @Rule
    public ActivityTestRule<FacilityActivity> mActivityRule =
            new ActivityTestRule<FacilityActivity>(FacilityActivity.class, true, false);

    @Before
    public void init(){
        facility = new FacilitySearchList();
        facility.setFakeId(1596703421L);
        facility.setName("북부노인주간보호센터");
        facility.setAddress("경기 남양주시 오남읍 오남리 346-2");
        //facility.category = "요양병원";
        //facility.address = "서울 송파구 방이동";
        //facility.rate = 3.5f;
        //facility.reviewCount = 107;
        //facility.latLng = new LatLng(37.721476, 128.5650602);
    }

    @Test
    public void sendIntent(){
        Context context = InstrumentationRegistry.getInstrumentation().getContext();
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.putExtra(EXTRA_FACILITY, facility);
        mActivityRule.launchActivity(intent);
    }
}
